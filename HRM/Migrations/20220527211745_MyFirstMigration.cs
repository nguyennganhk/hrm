﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HRM.Migrations
{
    public partial class MyFirstMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "WF");

            migrationBuilder.EnsureSchema(
                name: "Sale");

            migrationBuilder.EnsureSchema(
                name: "eim");

            migrationBuilder.EnsureSchema(
                name: "Project");

            migrationBuilder.EnsureSchema(
                name: "HR");

            migrationBuilder.CreateTable(
                name: "AnhSanPhamViews",
                columns: table => new
                {
                    RowGuid = table.Column<Guid>(nullable: false),
                    Title = table.Column<string>(nullable: true),
                    Title1 = table.Column<string>(nullable: true),
                    Title2 = table.Column<string>(nullable: true),
                    Title3 = table.Column<string>(nullable: true),
                    Title4 = table.Column<string>(nullable: true),
                    Title5 = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AnhSanPhamViews", x => x.RowGuid);
                });

            migrationBuilder.CreateTable(
                name: "Auto",
                columns: table => new
                {
                    AliasId = table.Column<string>(nullable: false),
                    IsEdit = table.Column<bool>(nullable: true),
                    IsNumberAuto = table.Column<bool>(nullable: true),
                    Prefixs = table.Column<string>(nullable: true),
                    ModuleId = table.Column<string>(nullable: true),
                    VoucherType = table.Column<string>(nullable: true),
                    IsEditPOV = table.Column<bool>(nullable: true),
                    IsNumberAutoPOV = table.Column<bool>(nullable: true),
                    IsEditINVE = table.Column<bool>(nullable: true),
                    IsNumberAutoINVE = table.Column<bool>(nullable: true),
                    IsEditDEBT = table.Column<bool>(nullable: true),
                    IsNumberAutoDEBT = table.Column<bool>(nullable: true),
                    IsEditCA = table.Column<bool>(nullable: true),
                    IsNumberAutoCA = table.Column<bool>(nullable: true),
                    IsEditBA = table.Column<bool>(nullable: true),
                    IsNumberAutoBA = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Auto", x => x.AliasId);
                });

            migrationBuilder.CreateTable(
                name: "DashboardView1",
                columns: table => new
                {
                    Stt = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Title = table.Column<string>(nullable: true),
                    GTDH = table.Column<decimal>(nullable: true),
                    GTGDH = table.Column<decimal>(nullable: true),
                    CN = table.Column<decimal>(nullable: true),
                    NL = table.Column<decimal>(nullable: true),
                    SLDHPT = table.Column<int>(nullable: true),
                    SLDHPS = table.Column<int>(nullable: true),
                    BHBT = table.Column<int>(nullable: true),
                    SLDHBHBT = table.Column<int>(nullable: true),
                    SLDHHSPL = table.Column<int>(nullable: true),
                    Tamp = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DashboardView1", x => x.Stt);
                });

            migrationBuilder.CreateTable(
                name: "DepartmentsJtableViews",
                columns: table => new
                {
                    DepartmentGuid = table.Column<Guid>(nullable: false),
                    DepartmentId = table.Column<string>(nullable: true),
                    DepartmentName = table.Column<string>(nullable: true),
                    ParentId = table.Column<Guid>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    OrderId = table.Column<int>(nullable: true),
                    IsActive = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DepartmentsJtableViews", x => x.DepartmentGuid);
                });

            migrationBuilder.CreateTable(
                name: "DepartmentsTreeview",
                columns: table => new
                {
                    DepartmentGuid = table.Column<Guid>(nullable: false),
                    DepartmentId = table.Column<string>(nullable: true),
                    DepartmentName = table.Column<string>(nullable: true),
                    ParentId = table.Column<Guid>(nullable: true),
                    IsActive = table.Column<bool>(nullable: true),
                    NumberEmployee = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DepartmentsTreeview", x => x.DepartmentGuid);
                });

            migrationBuilder.CreateTable(
                name: "DepartmentsViewItems",
                columns: table => new
                {
                    DepartmentGuid = table.Column<Guid>(nullable: false),
                    DepartmentId = table.Column<string>(nullable: true),
                    DepartmentName = table.Column<string>(nullable: true),
                    ManagerGuid = table.Column<Guid>(nullable: true),
                    ManagerID = table.Column<string>(nullable: true),
                    ManagerName = table.Column<string>(nullable: true),
                    ParentId = table.Column<Guid>(nullable: true),
                    ParentName = table.Column<string>(nullable: true),
                    OrganizationGuid = table.Column<Guid>(nullable: true),
                    OrganizationName = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Content = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    Fax = table.Column<string>(nullable: true),
                    OrderId = table.Column<int>(nullable: true),
                    IsActive = table.Column<bool>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: true),
                    ModifiedBy = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DepartmentsViewItems", x => x.DepartmentGuid);
                });

            migrationBuilder.CreateTable(
                name: "EmployeesExport",
                columns: table => new
                {
                    EmployeeGuid = table.Column<Guid>(nullable: false),
                    LastName = table.Column<string>(nullable: true),
                    EmployeeID = table.Column<string>(nullable: true),
                    FullName = table.Column<string>(nullable: true),
                    StatusOfWork = table.Column<string>(nullable: true),
                    DepartmentID = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    PhotoTitle = table.Column<string>(nullable: true),
                    JobTitlesName = table.Column<string>(nullable: true),
                    StartDate = table.Column<DateTime>(nullable: true),
                    CurrentAddress = table.Column<string>(nullable: true),
                    EndDate = table.Column<DateTime>(nullable: true),
                    ContractExpired = table.Column<int>(nullable: true),
                    HireDate = table.Column<DateTime>(nullable: true),
                    IssueIddate = table.Column<DateTime>(nullable: true),
                    LoginName = table.Column<string>(nullable: true),
                    IssueIdby = table.Column<string>(nullable: true),
                    Gender = table.Column<string>(nullable: true),
                    Identification = table.Column<string>(nullable: true),
                    DepartmentName = table.Column<string>(nullable: true),
                    ProfestionaName = table.Column<string>(nullable: true),
                    BirthDate = table.Column<DateTime>(nullable: true),
                    WorkEmail = table.Column<string>(nullable: true),
                    TaxCode = table.Column<string>(nullable: true),
                    BankAccount = table.Column<string>(nullable: true),
                    BankName = table.Column<string>(nullable: true),
                    OrganizationName = table.Column<string>(nullable: true),
                    GenitiveId = table.Column<string>(nullable: true),
                    IsActive = table.Column<bool>(nullable: true),
                    ContractType = table.Column<string>(nullable: true),
                    ReasonName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmployeesExport", x => x.EmployeeGuid);
                });

            migrationBuilder.CreateTable(
                name: "EmployeesJtableView",
                columns: table => new
                {
                    EmployeeGuid = table.Column<Guid>(nullable: false),
                    LastName = table.Column<string>(nullable: true),
                    EmployeeID = table.Column<string>(nullable: true),
                    FullName = table.Column<string>(nullable: true),
                    StatusOfWork = table.Column<string>(nullable: true),
                    DepartmentID = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    PhotoTitle = table.Column<string>(nullable: true),
                    JobTitlesName = table.Column<string>(nullable: true),
                    StartDate = table.Column<DateTime>(nullable: true),
                    EndDate = table.Column<DateTime>(nullable: true),
                    ContractExpired = table.Column<int>(nullable: true),
                    HireDate = table.Column<DateTime>(nullable: true),
                    LoginName = table.Column<string>(nullable: true),
                    Gender = table.Column<string>(nullable: true),
                    Identification = table.Column<string>(nullable: true),
                    DepartmentName = table.Column<string>(nullable: true),
                    ProfestionaName = table.Column<string>(nullable: true),
                    ShiftName = table.Column<string>(nullable: true),
                    ShiftStartTime = table.Column<TimeSpan>(nullable: true),
                    ShiftEndTime = table.Column<TimeSpan>(nullable: true),
                    CheckApplyLeaves = table.Column<int>(nullable: true),
                    CandidateGuid = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmployeesJtableView", x => x.EmployeeGuid);
                });

            migrationBuilder.CreateTable(
                name: "EmployeesView",
                columns: table => new
                {
                    EmployeeGuid = table.Column<Guid>(nullable: false),
                    CandidateName = table.Column<string>(nullable: true),
                    EmployeeId = table.Column<string>(nullable: true),
                    DepartmentName = table.Column<string>(nullable: true),
                    Title = table.Column<string>(nullable: true),
                    FullName = table.Column<string>(nullable: true),
                    FirstName = table.Column<string>(nullable: true),
                    MiddleName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    Identification = table.Column<string>(nullable: true),
                    IssueIddate = table.Column<DateTime>(nullable: true),
                    IssueIdby = table.Column<string>(nullable: true),
                    LoginId = table.Column<string>(nullable: true),
                    LoginName = table.Column<string>(nullable: true),
                    JobTitleName = table.Column<string>(nullable: true),
                    ProfestionaName = table.Column<string>(nullable: true),
                    SpecializesId = table.Column<string>(nullable: true),
                    BirthDate = table.Column<DateTime>(nullable: true),
                    PlaceOfBirth = table.Column<string>(nullable: true),
                    MaritalStatus = table.Column<string>(nullable: true),
                    Gender = table.Column<string>(nullable: true),
                    HireDate = table.Column<DateTime>(nullable: true),
                    SalaryPayBy = table.Column<string>(nullable: true),
                    SalaryMethod = table.Column<string>(nullable: true),
                    PermanentAddress = table.Column<string>(nullable: true),
                    CurrentAddress = table.Column<string>(nullable: true),
                    CountryName = table.Column<string>(nullable: true),
                    ProvinceName = table.Column<string>(nullable: true),
                    DistrictName = table.Column<string>(nullable: true),
                    WardName = table.Column<string>(nullable: true),
                    CountryId = table.Column<string>(nullable: true),
                    ProvinceId = table.Column<string>(nullable: true),
                    DistrictId = table.Column<string>(nullable: true),
                    WardId = table.Column<string>(nullable: true),
                    HomeNumber = table.Column<string>(nullable: true),
                    EthnicName = table.Column<string>(nullable: true),
                    HomePhone = table.Column<string>(nullable: true),
                    HomeMobile = table.Column<string>(nullable: true),
                    HomeEmail = table.Column<string>(nullable: true),
                    WorkPhone = table.Column<string>(nullable: true),
                    WorkMobile = table.Column<string>(nullable: true),
                    WorkEmail = table.Column<string>(nullable: true),
                    EducationName = table.Column<string>(nullable: true),
                    ImagePath = table.Column<string>(nullable: true),
                    Photo = table.Column<byte[]>(nullable: true),
                    TaxCode = table.Column<string>(nullable: true),
                    BankName = table.Column<string>(nullable: true),
                    BankAccount = table.Column<string>(nullable: true),
                    StatusOfWork = table.Column<string>(nullable: true),
                    EndDateWork = table.Column<DateTime>(nullable: true),
                    Note = table.Column<string>(nullable: true),
                    StartDate = table.Column<DateTime>(nullable: true),
                    EndDate = table.Column<DateTime>(nullable: true),
                    ProbationPeriod = table.Column<int>(nullable: true),
                    Uniform = table.Column<string>(nullable: true),
                    IsConcurrently = table.Column<string>(nullable: true),
                    TimeKeepingId = table.Column<string>(nullable: true),
                    IsUseCar = table.Column<bool>(nullable: true),
                    CarId = table.Column<string>(nullable: true),
                    IsActive = table.Column<bool>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: true),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    GenitiveId = table.Column<string>(nullable: true),
                    SalaryTierId = table.Column<string>(nullable: true),
                    RankName = table.Column<string>(nullable: true),
                    ReasonLeaveId = table.Column<int>(nullable: true),
                    ManagerName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmployeesView", x => x.EmployeeGuid);
                });

            migrationBuilder.CreateTable(
                name: "EmployeesViewLogin",
                columns: table => new
                {
                    EmployeeGuid = table.Column<Guid>(nullable: false),
                    EmployeeId = table.Column<string>(nullable: true),
                    LoginId = table.Column<string>(nullable: true),
                    LoginName = table.Column<string>(nullable: true),
                    FullName = table.Column<string>(nullable: true),
                    WorkPhone = table.Column<string>(nullable: true),
                    DepartmentId = table.Column<string>(nullable: true),
                    DepartmentGuid = table.Column<Guid>(nullable: true),
                    DepartmentName = table.Column<string>(nullable: true),
                    OrganizationGuid = table.Column<Guid>(nullable: false),
                    OrganizationName = table.Column<string>(nullable: true),
                    JobTitleId = table.Column<int>(nullable: true),
                    JobTitleName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmployeesViewLogin", x => x.EmployeeGuid);
                });

            migrationBuilder.CreateTable(
                name: "Object_Combobox",
                columns: table => new
                {
                    value = table.Column<string>(nullable: false),
                    text = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Object_Combobox", x => x.value);
                });

            migrationBuilder.CreateTable(
                name: "Object_Jexcel",
                columns: table => new
                {
                    id = table.Column<string>(nullable: false),
                    name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Object_Jexcel", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "ObjectNew_Combobox",
                columns: table => new
                {
                    value = table.Column<string>(nullable: false),
                    text = table.Column<string>(nullable: true),
                    valueMore = table.Column<string>(nullable: true),
                    valueText = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ObjectNew_Combobox", x => x.value);
                });

            migrationBuilder.CreateTable(
                name: "PhotoView",
                columns: table => new
                {
                    EmployeeGuid = table.Column<Guid>(nullable: false),
                    ImagePath = table.Column<string>(nullable: true),
                    Thumb = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PhotoView", x => x.EmployeeGuid);
                });

            migrationBuilder.CreateTable(
                name: "ProjectManageJtableView",
                columns: table => new
                {
                    ProjectDetailGuidC2 = table.Column<Guid>(nullable: false),
                    ProjectDetailGuidC1 = table.Column<Guid>(nullable: true),
                    ProjectGuid = table.Column<Guid>(nullable: true),
                    FullName = table.Column<string>(nullable: true),
                    TitleClueGuid = table.Column<string>(nullable: true),
                    TitleProjectCode = table.Column<string>(nullable: true),
                    TitleKPI236 = table.Column<string>(nullable: true),
                    TitlePlan = table.Column<string>(nullable: true),
                    TitleKPI37 = table.Column<string>(nullable: true),
                    Notec1 = table.Column<string>(nullable: true),
                    Notec2 = table.Column<string>(nullable: true),
                    StartDate = table.Column<DateTime>(nullable: true),
                    EndDate = table.Column<DateTime>(nullable: true),
                    CountExpired = table.Column<int>(nullable: true),
                    Status = table.Column<int>(nullable: true),
                    Approver = table.Column<int>(nullable: true),
                    ExpiredStatus = table.Column<int>(nullable: true),
                    DepartmentName = table.Column<string>(nullable: true),
                    GHC2 = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectManageJtableView", x => x.ProjectDetailGuidC2);
                });

            migrationBuilder.CreateTable(
                name: "ProjectManageResultView2",
                columns: table => new
                {
                    Stt = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FullName = table.Column<string>(nullable: true),
                    SLDA = table.Column<int>(nullable: true),
                    SLCVc1 = table.Column<int>(nullable: true),
                    HT = table.Column<int>(nullable: true),
                    KHT = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectManageResultView2", x => x.Stt);
                });

            migrationBuilder.CreateTable(
                name: "ProjectManageResultView3",
                columns: table => new
                {
                    Stt = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ClueName = table.Column<string>(nullable: true),
                    TenDuan = table.Column<string>(nullable: true),
                    Notec1 = table.Column<string>(nullable: true),
                    Note = table.Column<string>(nullable: true),
                    PLCQKQ = table.Column<string>(nullable: true),
                    ObjectiveName = table.Column<string>(nullable: true),
                    Reason = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectManageResultView3", x => x.Stt);
                });

            migrationBuilder.CreateTable(
                name: "ProjectManageView1",
                columns: table => new
                {
                    Stt = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FullName = table.Column<string>(nullable: true),
                    KH1 = table.Column<int>(nullable: true),
                    KH2 = table.Column<int>(nullable: true),
                    KH3 = table.Column<int>(nullable: true),
                    KH4 = table.Column<int>(nullable: true),
                    KH5 = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectManageView1", x => x.Stt);
                });

            migrationBuilder.CreateTable(
                name: "ProjectManageView3",
                columns: table => new
                {
                    Stt = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    KH = table.Column<string>(nullable: true),
                    NhomKH = table.Column<string>(nullable: true),
                    SLDA = table.Column<int>(nullable: true),
                    Tamp = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectManageView3", x => x.Stt);
                });

            migrationBuilder.CreateTable(
                name: "ProjectManageView4",
                columns: table => new
                {
                    Stt = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Tenban = table.Column<string>(nullable: true),
                    SLCV = table.Column<int>(nullable: true),
                    SLDA = table.Column<int>(nullable: true),
                    Tamp = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectManageView4", x => x.Stt);
                });

            migrationBuilder.CreateTable(
                name: "ProjectManageView5",
                columns: table => new
                {
                    Stt = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FullName = table.Column<string>(nullable: true),
                    CPTC = table.Column<decimal>(nullable: true),
                    CPNB = table.Column<decimal>(nullable: true),
                    Tamp = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectManageView5", x => x.Stt);
                });

            migrationBuilder.CreateTable(
                name: "ProjectManageView6",
                columns: table => new
                {
                    Stt = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FullName = table.Column<string>(nullable: true),
                    Daumoi = table.Column<string>(nullable: true),
                    TenDA = table.Column<string>(nullable: true),
                    Kpi37 = table.Column<string>(nullable: true),
                    Kpi236 = table.Column<string>(nullable: true),
                    Notec1 = table.Column<string>(nullable: true),
                    StartDate = table.Column<DateTime>(nullable: true),
                    EndDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectManageView6", x => x.Stt);
                });

            migrationBuilder.CreateTable(
                name: "ProjectReportView1",
                columns: table => new
                {
                    Stt = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    KHDK = table.Column<string>(nullable: true),
                    NhomDK = table.Column<string>(nullable: true),
                    SLDADK = table.Column<int>(nullable: true),
                    KHCK = table.Column<string>(nullable: true),
                    NhomCK = table.Column<string>(nullable: true),
                    SLDACK = table.Column<int>(nullable: true),
                    Note = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectReportView1", x => x.Stt);
                });

            migrationBuilder.CreateTable(
                name: "ProjectReportView2",
                columns: table => new
                {
                    Stt = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    KH = table.Column<string>(nullable: true),
                    SLCV = table.Column<int>(nullable: true),
                    HT = table.Column<int>(nullable: true),
                    KHT = table.Column<int>(nullable: true),
                    PTHT = table.Column<decimal>(nullable: true),
                    PTKHT = table.Column<decimal>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectReportView2", x => x.Stt);
                });

            migrationBuilder.CreateTable(
                name: "ProjectReportView3",
                columns: table => new
                {
                    Stt = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Note = table.Column<string>(nullable: true),
                    SLDA = table.Column<int>(nullable: true),
                    HT = table.Column<int>(nullable: true),
                    KHT = table.Column<int>(nullable: true),
                    PTHT = table.Column<decimal>(nullable: true),
                    PTKHT = table.Column<decimal>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectReportView3", x => x.Stt);
                });

            migrationBuilder.CreateTable(
                name: "ProjectReportView5",
                columns: table => new
                {
                    Stt = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Tenban = table.Column<string>(nullable: true),
                    SLCV = table.Column<int>(nullable: true),
                    SLPhHT = table.Column<int>(nullable: true),
                    SLPhKHT = table.Column<int>(nullable: true),
                    PTHT = table.Column<decimal>(nullable: true),
                    PTKHT = table.Column<decimal>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectReportView5", x => x.Stt);
                });

            migrationBuilder.CreateTable(
                name: "ProjectReportView6",
                columns: table => new
                {
                    Stt = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    PLCQKQ = table.Column<string>(nullable: true),
                    CVKHT = table.Column<int>(nullable: true),
                    DG = table.Column<decimal>(nullable: true),
                    Type = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectReportView6", x => x.Stt);
                });

            migrationBuilder.CreateTable(
                name: "ReportViewSheet1",
                columns: table => new
                {
                    Stt = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Maduan = table.Column<string>(nullable: true),
                    Tenduan = table.Column<string>(nullable: true),
                    Cluec1 = table.Column<string>(nullable: true),
                    PlanName = table.Column<string>(nullable: true),
                    GroupName = table.Column<string>(nullable: true),
                    TypePlan = table.Column<string>(nullable: true),
                    Amount = table.Column<decimal>(nullable: true),
                    AmountPackage = table.Column<decimal>(nullable: true),
                    AmountLiabilities = table.Column<decimal>(nullable: true),
                    AmountFuel = table.Column<decimal>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReportViewSheet1", x => x.Stt);
                });

            migrationBuilder.CreateTable(
                name: "ReportViewSheet2",
                columns: table => new
                {
                    Stt = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Maduan = table.Column<string>(nullable: true),
                    Tenduan = table.Column<string>(nullable: true),
                    Notekpi37 = table.Column<string>(nullable: true),
                    StartDate = table.Column<DateTime>(nullable: true),
                    EndDate = table.Column<DateTime>(nullable: true),
                    Notec1 = table.Column<string>(nullable: true),
                    Note = table.Column<string>(nullable: true),
                    StartDatec1 = table.Column<DateTime>(nullable: true),
                    EndDatec1 = table.Column<DateTime>(nullable: true),
                    GTGN = table.Column<decimal>(nullable: true),
                    Notec2 = table.Column<string>(nullable: true),
                    StartDatec2 = table.Column<DateTime>(nullable: true),
                    EndDatec2 = table.Column<DateTime>(nullable: true),
                    CPTC = table.Column<decimal>(nullable: true),
                    TMTC = table.Column<string>(nullable: true),
                    CPNB = table.Column<decimal>(nullable: true),
                    TMNB = table.Column<string>(nullable: true),
                    BKT = table.Column<string>(nullable: true),
                    NotePHKT = table.Column<string>(nullable: true),
                    EndDateKT = table.Column<DateTime>(nullable: true),
                    BTMQT = table.Column<string>(nullable: true),
                    NotePHTMQT = table.Column<string>(nullable: true),
                    EndDateTMQT = table.Column<DateTime>(nullable: true),
                    NoteBTCKT = table.Column<string>(nullable: true),
                    EndDateBTCKT = table.Column<DateTime>(nullable: true),
                    NameKH = table.Column<string>(nullable: true),
                    NoteKH = table.Column<string>(nullable: true),
                    EndDateKH = table.Column<DateTime>(nullable: true),
                    PLCQNN = table.Column<string>(nullable: true),
                    NameCQNN = table.Column<string>(nullable: true),
                    NotePHCQNN = table.Column<string>(nullable: true),
                    EndDateCQNN = table.Column<DateTime>(nullable: true),
                    NameTP = table.Column<string>(nullable: true),
                    NotePHTP = table.Column<string>(nullable: true),
                    EndDateTP = table.Column<DateTime>(nullable: true),
                    PLTV = table.Column<string>(nullable: true),
                    NameTV = table.Column<string>(nullable: true),
                    NotePHTV = table.Column<string>(nullable: true),
                    EndDateTV = table.Column<DateTime>(nullable: true),
                    NameKB = table.Column<string>(nullable: true),
                    NotePHKB = table.Column<string>(nullable: true),
                    EndDateKB = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReportViewSheet2", x => x.Stt);
                });

            migrationBuilder.CreateTable(
                name: "ReportViewSheet3",
                columns: table => new
                {
                    Stt = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Maduan = table.Column<string>(nullable: true),
                    Tenduan = table.Column<string>(nullable: true),
                    Notekpi37 = table.Column<string>(nullable: true),
                    StartDate = table.Column<DateTime>(nullable: true),
                    EndDate = table.Column<DateTime>(nullable: true),
                    Notec1 = table.Column<string>(nullable: true),
                    Note = table.Column<string>(nullable: true),
                    StartDatec1 = table.Column<DateTime>(nullable: true),
                    EndDatec1 = table.Column<DateTime>(nullable: true),
                    GTGN = table.Column<decimal>(nullable: true),
                    Notec2 = table.Column<string>(nullable: true),
                    StartDatec2 = table.Column<DateTime>(nullable: true),
                    EndDatec2 = table.Column<DateTime>(nullable: true),
                    GHc2 = table.Column<DateTime>(nullable: true),
                    PLCQKQ = table.Column<string>(nullable: true),
                    ObjectiveName = table.Column<string>(nullable: true),
                    Reason = table.Column<string>(nullable: true),
                    CPTC = table.Column<decimal>(nullable: true),
                    TMTC = table.Column<string>(nullable: true),
                    CPNB = table.Column<decimal>(nullable: true),
                    TMNB = table.Column<string>(nullable: true),
                    BKT = table.Column<string>(nullable: true),
                    NotePHKT = table.Column<string>(nullable: true),
                    EndDateKT = table.Column<DateTime>(nullable: true),
                    BTMQT = table.Column<string>(nullable: true),
                    NotePHTMQT = table.Column<string>(nullable: true),
                    EndDateTMQT = table.Column<DateTime>(nullable: true),
                    NoteBTCKT = table.Column<string>(nullable: true),
                    EndDateBTCKT = table.Column<DateTime>(nullable: true),
                    NameKH = table.Column<string>(nullable: true),
                    NoteKH = table.Column<string>(nullable: true),
                    EndDateKH = table.Column<DateTime>(nullable: true),
                    PLCQNN = table.Column<string>(nullable: true),
                    NameCQNN = table.Column<string>(nullable: true),
                    NotePHCQNN = table.Column<string>(nullable: true),
                    EndDateCQNN = table.Column<DateTime>(nullable: true),
                    NameTP = table.Column<string>(nullable: true),
                    NotePHTP = table.Column<string>(nullable: true),
                    EndDateTP = table.Column<DateTime>(nullable: true),
                    PLTV = table.Column<string>(nullable: true),
                    NameTV = table.Column<string>(nullable: true),
                    NotePHTV = table.Column<string>(nullable: true),
                    EndDateTV = table.Column<DateTime>(nullable: true),
                    NameKB = table.Column<string>(nullable: true),
                    NotePHKB = table.Column<string>(nullable: true),
                    EndDateKB = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReportViewSheet3", x => x.Stt);
                });

            migrationBuilder.CreateTable(
                name: "ReportViewSheet4",
                columns: table => new
                {
                    Stt = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    PLCQKQ = table.Column<string>(nullable: true),
                    CVKHT = table.Column<int>(nullable: true),
                    DG = table.Column<decimal>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReportViewSheet4", x => x.Stt);
                });

            migrationBuilder.CreateTable(
                name: "SanPhamViews",
                columns: table => new
                {
                    RowGuid = table.Column<Guid>(nullable: false),
                    MaSanPham = table.Column<string>(nullable: true),
                    TenSanPham = table.Column<string>(nullable: true),
                    DonGia = table.Column<decimal>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    TenHang = table.Column<string>(nullable: true),
                    TenNhom = table.Column<string>(nullable: true),
                    Giamgia = table.Column<int>(nullable: true),
                    MetaTitle = table.Column<string>(nullable: true),
                    Location = table.Column<bool>(nullable: true),
                    Title1 = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SanPhamViews", x => x.RowGuid);
                });

            migrationBuilder.CreateTable(
                name: "SQLCOMMANDS",
                columns: table => new
                {
                    Error = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SQLCOMMANDS", x => x.Error);
                });

            migrationBuilder.CreateTable(
                name: "ESActions",
                schema: "eim",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Title = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Ord = table.Column<int>(nullable: true),
                    Seq = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ESActions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ESResources",
                schema: "eim",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Title = table.Column<string>(maxLength: 255, nullable: true),
                    Code = table.Column<string>(maxLength: 255, nullable: true),
                    ParentId = table.Column<int>(nullable: true),
                    GroupResourceId = table.Column<string>(maxLength: 50, nullable: false),
                    Ord = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ESResources", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ESUserPrivileges",
                schema: "eim",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<string>(nullable: true),
                    PrivilegeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ESUserPrivileges", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Candidates",
                schema: "HR",
                columns: table => new
                {
                    CandidateGuid = table.Column<Guid>(nullable: false, defaultValueSql: "(newid())"),
                    OrganizationGuid = table.Column<Guid>(nullable: false),
                    CandidateID = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    RecruitmentGuid = table.Column<Guid>(nullable: true),
                    RecruitmentID = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    DepartmentGuid = table.Column<Guid>(nullable: true),
                    DepartmentID = table.Column<string>(unicode: false, maxLength: 20, nullable: true),
                    FullName = table.Column<string>(maxLength: 60, nullable: false),
                    FirstName = table.Column<string>(maxLength: 20, nullable: true),
                    MiddleName = table.Column<string>(maxLength: 20, nullable: true),
                    LastName = table.Column<string>(maxLength: 20, nullable: true),
                    Identification = table.Column<string>(unicode: false, maxLength: 20, nullable: true),
                    IssueDate = table.Column<DateTime>(type: "date", nullable: true),
                    BirthDate = table.Column<DateTime>(type: "date", nullable: false),
                    PlaceOfBirth = table.Column<string>(maxLength: 255, nullable: true),
                    MaritalStatus = table.Column<string>(unicode: false, maxLength: 1, nullable: true),
                    Gender = table.Column<string>(unicode: false, maxLength: 1, nullable: false),
                    PermanentAddress = table.Column<string>(maxLength: 255, nullable: true),
                    CurrentAddress = table.Column<string>(maxLength: 255, nullable: true),
                    CountryID = table.Column<string>(unicode: false, maxLength: 20, nullable: true, defaultValueSql: "('VN')"),
                    CountryName = table.Column<string>(maxLength: 50, nullable: true, defaultValueSql: "(N'Việt Nam')"),
                    ProvinceID = table.Column<string>(unicode: false, maxLength: 20, nullable: true),
                    ProvinceName = table.Column<string>(maxLength: 50, nullable: true),
                    DistrictID = table.Column<string>(unicode: false, maxLength: 20, nullable: true),
                    DistrictName = table.Column<string>(maxLength: 50, nullable: true),
                    WardID = table.Column<string>(unicode: false, maxLength: 20, nullable: true),
                    WardName = table.Column<string>(maxLength: 20, nullable: true),
                    HomeNumber = table.Column<string>(maxLength: 50, nullable: true),
                    EthnicID = table.Column<int>(nullable: true),
                    EthnicName = table.Column<string>(maxLength: 10, nullable: true),
                    HomePhone = table.Column<string>(unicode: false, maxLength: 15, nullable: true, defaultValueSql: "(N'()')"),
                    HomeMobile = table.Column<string>(unicode: false, maxLength: 20, nullable: true),
                    HomeEmail = table.Column<string>(maxLength: 50, nullable: true),
                    WorkPhone = table.Column<string>(unicode: false, maxLength: 15, nullable: true),
                    WorkMobile = table.Column<string>(unicode: false, maxLength: 20, nullable: true),
                    WorkEmail = table.Column<string>(maxLength: 50, nullable: true),
                    EducationID = table.Column<int>(nullable: true),
                    EducationName = table.Column<string>(maxLength: 20, nullable: true),
                    ImagePath = table.Column<string>(maxLength: 255, nullable: true),
                    Photo = table.Column<byte[]>(nullable: true),
                    TaxCode = table.Column<string>(unicode: false, maxLength: 20, nullable: true),
                    ApplyDate = table.Column<DateTime>(type: "date", nullable: true),
                    Channel = table.Column<string>(maxLength: 50, nullable: true),
                    Note = table.Column<string>(maxLength: 255, nullable: true),
                    AssignedTo = table.Column<string>(maxLength: 500, nullable: true),
                    Headship = table.Column<string>(maxLength: 500, nullable: true),
                    Approver = table.Column<string>(maxLength: 500, nullable: true),
                    Action = table.Column<string>(maxLength: 255, nullable: true),
                    Request = table.Column<string>(maxLength: 255, nullable: true),
                    FilterResult = table.Column<string>(unicode: false, maxLength: 1, nullable: true),
                    ExamResult = table.Column<string>(unicode: false, maxLength: 1, nullable: true),
                    Status = table.Column<string>(unicode: false, maxLength: 1, nullable: true),
                    Specialized = table.Column<string>(maxLength: 100, nullable: true),
                    RejectReason = table.Column<string>(maxLength: 250, nullable: true),
                    Referrer = table.Column<string>(maxLength: 50, nullable: true),
                    StartDate = table.Column<DateTime>(type: "date", nullable: true),
                    OldSalary = table.Column<decimal>(type: "money", nullable: true),
                    DesiredSalary = table.Column<decimal>(type: "money", nullable: true),
                    ReasonNotInterview = table.Column<string>(maxLength: 255, nullable: true),
                    Experience = table.Column<string>(maxLength: 255, nullable: true),
                    InterviewDate = table.Column<DateTime>(type: "date", nullable: true),
                    InterviewStaff = table.Column<string>(maxLength: 50, nullable: true),
                    IsDraft = table.Column<bool>(nullable: true, defaultValueSql: "((0))"),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: true, defaultValueSql: "(getdate())"),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    ModifiedBy = table.Column<string>(maxLength: 50, nullable: true),
                    ModifiedDate = table.Column<DateTime>(type: "datetime", nullable: true, defaultValueSql: "(getdate())"),
                    WorkFlowGuid = table.Column<Guid>(nullable: true),
                    IsLocked = table.Column<bool>(nullable: true),
                    PositionId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Candidates", x => x.CandidateGuid);
                });

            migrationBuilder.CreateTable(
                name: "Contracts",
                schema: "HR",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ContractGuid = table.Column<Guid>(nullable: false, defaultValueSql: "(newid())"),
                    ContractNo = table.Column<string>(unicode: false, maxLength: 20, nullable: true),
                    Title = table.Column<string>(maxLength: 255, nullable: true),
                    EmployeeGuid = table.Column<Guid>(nullable: true),
                    EmployeeID = table.Column<string>(unicode: false, maxLength: 20, nullable: true),
                    DepartmentGuid = table.Column<Guid>(nullable: true),
                    DepartmentID = table.Column<string>(unicode: false, maxLength: 20, nullable: true),
                    OrganizationGuid = table.Column<Guid>(nullable: true),
                    Salary = table.Column<decimal>(type: "money", nullable: true),
                    ContractDate = table.Column<DateTime>(type: "date", nullable: true),
                    ContractExpired = table.Column<DateTime>(type: "date", nullable: true),
                    ContractType = table.Column<int>(nullable: true),
                    Description = table.Column<string>(maxLength: 255, nullable: true),
                    Body = table.Column<string>(nullable: true),
                    IsActive = table.Column<bool>(nullable: true, defaultValueSql: "((1))"),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ModifiedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    ModifiedBy = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Contracts", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Educations",
                schema: "HR",
                columns: table => new
                {
                    EducationID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    EducationName = table.Column<string>(maxLength: 255, nullable: false),
                    OrganizationGuid = table.Column<Guid>(nullable: false),
                    Description = table.Column<string>(maxLength: 255, nullable: true),
                    IsActive = table.Column<bool>(nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: true, defaultValueSql: "(getdate())"),
                    ModifiedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    ModifiedBy = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Educations", x => x.EducationID);
                });

            migrationBuilder.CreateTable(
                name: "JobTitles",
                schema: "HR",
                columns: table => new
                {
                    JobTitleID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CategoryId = table.Column<string>(nullable: true),
                    JobTitleName = table.Column<string>(maxLength: 255, nullable: false),
                    OrganizationGuid = table.Column<Guid>(nullable: false),
                    Description = table.Column<string>(maxLength: 255, nullable: true),
                    IsActive = table.Column<bool>(nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: true, defaultValueSql: "(getdate())"),
                    ModifiedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    ModifiedBy = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JobTitles", x => x.JobTitleID);
                });

            migrationBuilder.CreateTable(
                name: "Organizations",
                schema: "HR",
                columns: table => new
                {
                    OrganizationGuid = table.Column<Guid>(nullable: false, defaultValueSql: "(newid())"),
                    OrganizationID = table.Column<string>(unicode: false, maxLength: 20, nullable: true),
                    ParentID = table.Column<Guid>(nullable: true),
                    OrganizationName = table.Column<string>(maxLength: 250, nullable: false),
                    CountryID = table.Column<string>(maxLength: 20, nullable: true),
                    CountyName = table.Column<string>(maxLength: 50, nullable: true),
                    Description = table.Column<string>(maxLength: 250, nullable: true),
                    SortOrder = table.Column<int>(nullable: true),
                    IsDependentBrach = table.Column<bool>(nullable: true),
                    Address = table.Column<string>(maxLength: 250, nullable: true),
                    Tel = table.Column<string>(unicode: false, maxLength: 250, nullable: true),
                    Fax = table.Column<string>(unicode: false, maxLength: 250, nullable: true),
                    Web = table.Column<string>(unicode: false, maxLength: 250, nullable: true),
                    Email = table.Column<string>(unicode: false, maxLength: 250, nullable: true),
                    TaxCode = table.Column<string>(unicode: false, maxLength: 25, nullable: true),
                    IsActive = table.Column<bool>(nullable: true, defaultValueSql: "((1))"),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: true, defaultValueSql: "(getdate())"),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    ModifiedDate = table.Column<DateTime>(type: "datetime", nullable: true, defaultValueSql: "(getdate())"),
                    ModifiedBy = table.Column<string>(maxLength: 50, nullable: true),
                    Representative = table.Column<string>(maxLength: 50, nullable: true),
                    Photo = table.Column<byte[]>(nullable: true),
                    ImagePath = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Organizations", x => x.OrganizationGuid);
                });

            migrationBuilder.CreateTable(
                name: "Profestionals",
                schema: "HR",
                columns: table => new
                {
                    ProfestionalId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ProfestionaName = table.Column<string>(maxLength: 50, nullable: false),
                    OrganizationGuid = table.Column<Guid>(nullable: false),
                    Description = table.Column<string>(maxLength: 100, nullable: true),
                    IsActive = table.Column<bool>(nullable: true, defaultValueSql: "((1))"),
                    ModifiedDate = table.Column<DateTime>(type: "datetime", nullable: true, defaultValueSql: "(getdate())"),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: true, defaultValueSql: "(getdate())"),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    ModifiedBy = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Profestionals", x => x.ProfestionalId);
                });

            migrationBuilder.CreateTable(
                name: "Rank",
                schema: "HR",
                columns: table => new
                {
                    CategoryID = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    CategoryName = table.Column<string>(maxLength: 255, nullable: false),
                    OrganizationGuid = table.Column<Guid>(nullable: true),
                    Description = table.Column<string>(maxLength: 255, nullable: true),
                    OrderID = table.Column<int>(nullable: true),
                    IsActive = table.Column<bool>(nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: true, defaultValueSql: "(getdate())"),
                    ModifiedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    ModifiedBy = table.Column<string>(maxLength: 50, nullable: true),
                    PercentSalary = table.Column<decimal>(type: "smallmoney", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Rank", x => x.CategoryID);
                });

            migrationBuilder.CreateTable(
                name: "ShiftOfEmployees",
                schema: "HR",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ShiftID = table.Column<int>(nullable: false),
                    EmployeeGuid = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ShiftOfEmployees", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Shifts",
                schema: "HR",
                columns: table => new
                {
                    ShiftID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ShiftName = table.Column<string>(maxLength: 20, nullable: false),
                    OrganizationGuid = table.Column<Guid>(nullable: true),
                    StartTime = table.Column<TimeSpan>(nullable: true),
                    EndTime = table.Column<TimeSpan>(nullable: true),
                    Allowance = table.Column<decimal>(type: "money", nullable: true),
                    Description = table.Column<string>(maxLength: 100, nullable: true),
                    IsActive = table.Column<bool>(nullable: true, defaultValueSql: "((1))"),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ModifiedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    ModifiedBy = table.Column<string>(maxLength: 50, nullable: true, defaultValueSql: "(getdate())"),
                    Type = table.Column<string>(nullable: true),
                    Rested = table.Column<int>(nullable: true),
                    ShiftCode = table.Column<string>(nullable: true),
                    WorkDay = table.Column<double>(nullable: true),
                    WorkWeekend = table.Column<double>(nullable: true),
                    WorkHoliday = table.Column<double>(nullable: true),
                    OvertimeDay = table.Column<double>(nullable: true),
                    OvertimeWeekend = table.Column<double>(nullable: true),
                    OvertimeHoliday = table.Column<double>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Shifts", x => x.ShiftID);
                });

            migrationBuilder.CreateTable(
                name: "Specializes",
                schema: "HR",
                columns: table => new
                {
                    CategoryID = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    ProfestionalD = table.Column<int>(unicode: false, maxLength: 20, nullable: false),
                    OrganizationName = table.Column<string>(nullable: true),
                    OrganizationGuid = table.Column<Guid>(nullable: true),
                    CategoryName = table.Column<string>(maxLength: 100, nullable: false),
                    Description = table.Column<string>(maxLength: 250, nullable: true),
                    OrderID = table.Column<int>(nullable: true),
                    IsActive = table.Column<bool>(nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: true, defaultValueSql: "(getdate())"),
                    ModifiedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ModifiedBy = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Specializes", x => x.CategoryID);
                });

            migrationBuilder.CreateTable(
                name: "BCV12",
                schema: "Project",
                columns: table => new
                {
                    BCV12Guid = table.Column<Guid>(nullable: false, defaultValueSql: "(newid())"),
                    Code = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    Title = table.Column<string>(maxLength: 255, nullable: true),
                    Description = table.Column<string>(maxLength: 255, nullable: true),
                    IsActive = table.Column<bool>(nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ModifiedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    ModifiedBy = table.Column<string>(maxLength: 50, nullable: true),
                    SortOrder = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BCV12", x => x.BCV12Guid);
                });

            migrationBuilder.CreateTable(
                name: "BCV24",
                schema: "Project",
                columns: table => new
                {
                    BCV24Guid = table.Column<Guid>(nullable: false, defaultValueSql: "(newid())"),
                    Code = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    Title = table.Column<string>(maxLength: 255, nullable: true),
                    Description = table.Column<string>(maxLength: 255, nullable: true),
                    IsActive = table.Column<bool>(nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ModifiedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    ModifiedBy = table.Column<string>(maxLength: 50, nullable: true),
                    SortOrder = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BCV24", x => x.BCV24Guid);
                });

            migrationBuilder.CreateTable(
                name: "ClassifyKT",
                schema: "Project",
                columns: table => new
                {
                    ClassifyKTGuid = table.Column<Guid>(nullable: false, defaultValueSql: "(newid())"),
                    Code = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    Title = table.Column<string>(maxLength: 255, nullable: true),
                    Description = table.Column<string>(maxLength: 255, nullable: true),
                    IsActive = table.Column<bool>(nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ModifiedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    ModifiedBy = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClassifyKT", x => x.ClassifyKTGuid);
                });

            migrationBuilder.CreateTable(
                name: "ClassifyTMQT",
                schema: "Project",
                columns: table => new
                {
                    ClassifyTMQTGuid = table.Column<Guid>(nullable: false, defaultValueSql: "(newid())"),
                    Code = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    Title = table.Column<string>(maxLength: 255, nullable: true),
                    Description = table.Column<string>(maxLength: 255, nullable: true),
                    IsActive = table.Column<bool>(nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ModifiedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    ModifiedBy = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClassifyTMQT", x => x.ClassifyTMQTGuid);
                });

            migrationBuilder.CreateTable(
                name: "ClassifyTV",
                schema: "Project",
                columns: table => new
                {
                    ClassifyTVGuid = table.Column<Guid>(nullable: false, defaultValueSql: "(newid())"),
                    Code = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    Title = table.Column<string>(maxLength: 255, nullable: true),
                    Description = table.Column<string>(maxLength: 255, nullable: true),
                    IsActive = table.Column<bool>(nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ModifiedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    ModifiedBy = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClassifyTV", x => x.ClassifyTVGuid);
                });

            migrationBuilder.CreateTable(
                name: "Clue",
                schema: "Project",
                columns: table => new
                {
                    ClueGuid = table.Column<Guid>(nullable: false),
                    Title = table.Column<string>(maxLength: 255, nullable: true),
                    ParentId = table.Column<Guid>(nullable: true),
                    OrderId = table.Column<int>(nullable: true),
                    ClueId = table.Column<string>(nullable: true),
                    Description = table.Column<string>(maxLength: 255, nullable: true),
                    IsActive = table.Column<bool>(nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ModifiedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    ModifiedBy = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Clue", x => x.ClueGuid);
                });

            migrationBuilder.CreateTable(
                name: "Combination",
                schema: "Project",
                columns: table => new
                {
                    CombinationGuid = table.Column<Guid>(nullable: false, defaultValueSql: "(newid())"),
                    ProjectCodeGuid = table.Column<Guid>(nullable: true),
                    Title = table.Column<string>(maxLength: 255, nullable: true),
                    KPI37Guid = table.Column<Guid>(nullable: true),
                    KPI236Guid = table.Column<Guid>(nullable: true),
                    Body = table.Column<string>(maxLength: 500, nullable: true),
                    Description = table.Column<string>(maxLength: 500, nullable: true),
                    TypeCombination = table.Column<bool>(nullable: true),
                    Type = table.Column<int>(nullable: true),
                    IsActive = table.Column<bool>(nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ModifiedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    ModifiedBy = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Combination", x => x.CombinationGuid);
                });

            migrationBuilder.CreateTable(
                name: "FieldClassification",
                schema: "Project",
                columns: table => new
                {
                    FieldClassificationGuid = table.Column<Guid>(nullable: false, defaultValueSql: "(newid())"),
                    Code = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    Title = table.Column<string>(maxLength: 255, nullable: true),
                    Description = table.Column<string>(maxLength: 255, nullable: true),
                    IsActive = table.Column<bool>(nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ModifiedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    ModifiedBy = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FieldClassification", x => x.FieldClassificationGuid);
                });

            migrationBuilder.CreateTable(
                name: "GroupPlan",
                schema: "Project",
                columns: table => new
                {
                    GroupPlanGuid = table.Column<Guid>(nullable: false, defaultValueSql: "(newid())"),
                    Code = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    Title = table.Column<string>(maxLength: 255, nullable: true),
                    Description = table.Column<string>(maxLength: 255, nullable: true),
                    Type = table.Column<bool>(nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ModifiedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    ModifiedBy = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GroupPlan", x => x.GroupPlanGuid);
                });

            migrationBuilder.CreateTable(
                name: "KPI236",
                schema: "Project",
                columns: table => new
                {
                    KPI236Guid = table.Column<Guid>(nullable: false, defaultValueSql: "(newid())"),
                    Code = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    Title = table.Column<string>(maxLength: 255, nullable: true),
                    Description = table.Column<string>(maxLength: 255, nullable: true),
                    IsActive = table.Column<bool>(nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ModifiedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    ModifiedBy = table.Column<string>(maxLength: 50, nullable: true),
                    SortOrder = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_KPI236", x => x.KPI236Guid);
                });

            migrationBuilder.CreateTable(
                name: "KPI37",
                schema: "Project",
                columns: table => new
                {
                    KPI37Guid = table.Column<Guid>(nullable: false, defaultValueSql: "(newid())"),
                    Code = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    Title = table.Column<string>(maxLength: 255, nullable: true),
                    Description = table.Column<string>(maxLength: 255, nullable: true),
                    IsActive = table.Column<bool>(nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ModifiedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    ModifiedBy = table.Column<string>(maxLength: 50, nullable: true),
                    SortOrder = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_KPI37", x => x.KPI37Guid);
                });

            migrationBuilder.CreateTable(
                name: "Objective",
                schema: "Project",
                columns: table => new
                {
                    ObjectiveGuid = table.Column<Guid>(nullable: false, defaultValueSql: "(newid())"),
                    CodeParrent = table.Column<string>(nullable: true),
                    Code = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    Title = table.Column<string>(maxLength: 255, nullable: true),
                    Description = table.Column<string>(maxLength: 255, nullable: true),
                    IsActive = table.Column<bool>(nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ModifiedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    ModifiedBy = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Objective", x => x.ObjectiveGuid);
                });

            migrationBuilder.CreateTable(
                name: "Partner",
                schema: "Project",
                columns: table => new
                {
                    PartnerGuid = table.Column<Guid>(nullable: false, defaultValueSql: "(newid())"),
                    Code = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    Title = table.Column<string>(maxLength: 255, nullable: true),
                    Description = table.Column<string>(maxLength: 255, nullable: true),
                    IsActive = table.Column<bool>(nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ModifiedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    ModifiedBy = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Partner", x => x.PartnerGuid);
                });

            migrationBuilder.CreateTable(
                name: "ProjectCode",
                schema: "Project",
                columns: table => new
                {
                    ProjectCodeGuid = table.Column<Guid>(nullable: false, defaultValueSql: "(newid())"),
                    LocalCode = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    CompanyCode = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    Title = table.Column<string>(maxLength: 255, nullable: true),
                    Description = table.Column<string>(maxLength: 255, nullable: true),
                    IsActive = table.Column<bool>(nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ModifiedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    ModifiedBy = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectCode", x => x.ProjectCodeGuid);
                });

            migrationBuilder.CreateTable(
                name: "ProjectCombination",
                schema: "Project",
                columns: table => new
                {
                    ProjectCombinationGuid = table.Column<Guid>(nullable: false),
                    ProjectGuid = table.Column<Guid>(nullable: true),
                    LoginName = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ModifiedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    ModifiedBy = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectCombination", x => x.ProjectCombinationGuid);
                });

            migrationBuilder.CreateTable(
                name: "ProjectManage",
                schema: "Project",
                columns: table => new
                {
                    ProjectGuid = table.Column<Guid>(nullable: false, defaultValueSql: "(newid())"),
                    Title = table.Column<string>(nullable: true),
                    ProjectCodeGuid = table.Column<Guid>(nullable: true),
                    GroupGuid = table.Column<Guid>(nullable: true),
                    PlanGuid = table.Column<Guid>(nullable: true),
                    TypePlan = table.Column<int>(nullable: true),
                    FieldClassificationGuid = table.Column<Guid>(nullable: true),
                    ClueGuid = table.Column<Guid>(nullable: true),
                    ClueGuid2 = table.Column<Guid>(nullable: true),
                    ClueGuid3 = table.Column<Guid>(nullable: true),
                    ClueGuid4 = table.Column<Guid>(nullable: true),
                    ProjectNameGuid = table.Column<Guid>(nullable: true),
                    Amount = table.Column<decimal>(nullable: true),
                    AmountPackage = table.Column<decimal>(nullable: true),
                    AmountLiabilities = table.Column<decimal>(nullable: true),
                    AmountFuel = table.Column<decimal>(nullable: true),
                    BKT = table.Column<string>(maxLength: 255, nullable: true),
                    IsDelete = table.Column<bool>(nullable: true),
                    Approver = table.Column<int>(nullable: true),
                    Approve2 = table.Column<int>(nullable: true),
                    LoginName = table.Column<string>(nullable: true),
                    ExpiredDate = table.Column<DateTime>(nullable: true),
                    DepartmentId = table.Column<string>(nullable: true),
                    ContractNo = table.Column<string>(nullable: true),
                    ExpiredContract = table.Column<DateTime>(nullable: true),
                    ExpiredGuarantee = table.Column<DateTime>(nullable: true),
                    Status = table.Column<int>(nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ModifiedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    ModifiedBy = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectManage", x => x.ProjectGuid);
                });

            migrationBuilder.CreateTable(
                name: "ProjectManageDetailC1",
                schema: "Project",
                columns: table => new
                {
                    ProjectDetailGuidC1 = table.Column<Guid>(nullable: false, defaultValueSql: "(newid())"),
                    ProjectGuid = table.Column<Guid>(nullable: true),
                    KPI37ID = table.Column<Guid>(nullable: true),
                    KPI236ID = table.Column<Guid>(nullable: true),
                    Note = table.Column<string>(maxLength: 250, nullable: true),
                    StartDate = table.Column<DateTime>(type: "date", nullable: true),
                    EndDate = table.Column<DateTime>(type: "date", nullable: true),
                    GTGN = table.Column<decimal>(type: "decimal(18, 0)", nullable: true),
                    Cost = table.Column<decimal>(type: "decimal(18, 0)", nullable: true),
                    Demonstration = table.Column<string>(maxLength: 250, nullable: true),
                    CostNB = table.Column<decimal>(type: "decimal(18, 0)", nullable: true),
                    DemonstrationNB = table.Column<string>(maxLength: 250, nullable: true),
                    LoginName = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ModifiedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    ModifiedBy = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectManageDetailC1", x => x.ProjectDetailGuidC1);
                });

            migrationBuilder.CreateTable(
                name: "ProjectManageDetailC2",
                schema: "Project",
                columns: table => new
                {
                    ProjectDetailGuidC2 = table.Column<Guid>(nullable: false, defaultValueSql: "(newid())"),
                    ProjectGuid = table.Column<Guid>(nullable: true),
                    ProjectDetailGuidC1 = table.Column<Guid>(nullable: true),
                    Note = table.Column<string>(maxLength: 250, nullable: true),
                    StartDate = table.Column<DateTime>(type: "date", nullable: true),
                    EndDate = table.Column<DateTime>(type: "date", nullable: true),
                    BKT = table.Column<string>(nullable: true),
                    NotePHKT = table.Column<string>(maxLength: 250, nullable: true),
                    EndDateKT = table.Column<DateTime>(type: "date", nullable: true),
                    BTMQT = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    NotePHTMQT = table.Column<string>(maxLength: 250, nullable: false),
                    EndDateTMQT = table.Column<DateTime>(type: "date", nullable: true),
                    NoteBTCKT = table.Column<string>(maxLength: 250, nullable: true),
                    EndDateBTCKT = table.Column<DateTime>(type: "date", nullable: true),
                    NameKH = table.Column<string>(maxLength: 250, nullable: false),
                    NoteKH = table.Column<string>(maxLength: 250, nullable: true),
                    EndDateKH = table.Column<DateTime>(type: "date", nullable: true),
                    ClassifyCQNN = table.Column<Guid>(unicode: false, maxLength: 50, nullable: true),
                    NameCQNN = table.Column<string>(maxLength: 250, nullable: false),
                    NotePHCQNN = table.Column<string>(maxLength: 250, nullable: true),
                    EndDateCQNN = table.Column<DateTime>(type: "date", nullable: true),
                    NameTP = table.Column<string>(maxLength: 250, nullable: false),
                    NotePHTP = table.Column<string>(maxLength: 250, nullable: true),
                    EndDateTP = table.Column<DateTime>(type: "date", nullable: true),
                    ClassifyTV = table.Column<Guid>(unicode: false, maxLength: 50, nullable: true),
                    NameTV = table.Column<string>(maxLength: 250, nullable: true),
                    NotePHTV = table.Column<string>(maxLength: 250, nullable: true),
                    EndDateTV = table.Column<DateTime>(type: "date", nullable: true),
                    NameKB = table.Column<Guid>(maxLength: 250, nullable: true),
                    NotePHKB = table.Column<string>(maxLength: 250, nullable: true),
                    EndDateKB = table.Column<DateTime>(type: "date", nullable: true),
                    Approver = table.Column<int>(nullable: true),
                    Approver2 = table.Column<int>(nullable: true),
                    Status = table.Column<int>(nullable: true),
                    ExpiredDate = table.Column<DateTime>(nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ModifiedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    ModifiedBy = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectManageDetailC2", x => x.ProjectDetailGuidC2);
                });

            migrationBuilder.CreateTable(
                name: "ProjectManageExpired",
                schema: "Project",
                columns: table => new
                {
                    ProjectManageExpiredGuid = table.Column<Guid>(nullable: false, defaultValueSql: "(newid())"),
                    ProjectGuid = table.Column<Guid>(nullable: true),
                    ProjectDetailGuidC2 = table.Column<Guid>(nullable: true),
                    ExpiredDate = table.Column<DateTime>(nullable: true),
                    ObjectiveGuid = table.Column<Guid>(nullable: true),
                    Reason = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ModifiedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    ModifiedBy = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectManageExpired", x => x.ProjectManageExpiredGuid);
                });

            migrationBuilder.CreateTable(
                name: "ProjectManageGroup",
                schema: "Project",
                columns: table => new
                {
                    ProjectManageGroupGuid = table.Column<Guid>(nullable: false, defaultValueSql: "(newid())"),
                    ProjectGuid = table.Column<Guid>(nullable: true),
                    GroupPlanGuid = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectManageGroup", x => x.ProjectManageGroupGuid);
                });

            migrationBuilder.CreateTable(
                name: "ProjectName",
                schema: "Project",
                columns: table => new
                {
                    ProjectNameGuid = table.Column<Guid>(nullable: false, defaultValueSql: "(newid())"),
                    ProjectCodeID = table.Column<int>(nullable: true),
                    Title = table.Column<string>(maxLength: 255, nullable: true),
                    Description = table.Column<string>(maxLength: 255, nullable: true),
                    IsActive = table.Column<bool>(nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ModifiedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    ModifiedBy = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectName", x => x.ProjectNameGuid);
                });

            migrationBuilder.CreateTable(
                name: "StateAgencies",
                schema: "Project",
                columns: table => new
                {
                    StateAgenciesGuid = table.Column<Guid>(nullable: false, defaultValueSql: "(newid())"),
                    Code = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    Title = table.Column<string>(maxLength: 255, nullable: true),
                    Description = table.Column<string>(maxLength: 255, nullable: true),
                    IsActive = table.Column<bool>(nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ModifiedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    ModifiedBy = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StateAgencies", x => x.StateAgenciesGuid);
                });

            migrationBuilder.CreateTable(
                name: "Treasury",
                schema: "Project",
                columns: table => new
                {
                    TreasuryGuid = table.Column<Guid>(nullable: false, defaultValueSql: "(newid())"),
                    Code = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    Title = table.Column<string>(maxLength: 255, nullable: true),
                    Description = table.Column<string>(maxLength: 255, nullable: true),
                    IsActive = table.Column<bool>(nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ModifiedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    ModifiedBy = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Treasury", x => x.TreasuryGuid);
                });

            migrationBuilder.CreateTable(
                name: "AnhSanPham",
                schema: "Sale",
                columns: table => new
                {
                    RowGuid = table.Column<Guid>(nullable: false),
                    SanPhamGuid = table.Column<Guid>(nullable: true),
                    Title = table.Column<string>(nullable: true),
                    Title1 = table.Column<string>(nullable: true),
                    Title2 = table.Column<string>(nullable: true),
                    Title3 = table.Column<string>(nullable: true),
                    Title4 = table.Column<string>(nullable: true),
                    Title5 = table.Column<string>(nullable: true),
                    FileType = table.Column<string>(nullable: true),
                    Image1 = table.Column<byte[]>(nullable: true),
                    Image2 = table.Column<byte[]>(nullable: true),
                    Image3 = table.Column<byte[]>(nullable: true),
                    Image4 = table.Column<byte[]>(nullable: true),
                    Image5 = table.Column<byte[]>(nullable: true),
                    Location = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AnhSanPham", x => x.RowGuid);
                });

            migrationBuilder.CreateTable(
                name: "GroupSanPham",
                schema: "Sale",
                columns: table => new
                {
                    RowGuid = table.Column<Guid>(nullable: false),
                    TenNhom = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Location = table.Column<int>(nullable: true),
                    Status = table.Column<bool>(nullable: true),
                    MetaTitle = table.Column<string>(nullable: true),
                    MetaDescription = table.Column<string>(nullable: true),
                    MetaKeyWord = table.Column<string>(nullable: true),
                    StatusShowTenNhom = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GroupSanPham", x => x.RowGuid);
                });

            migrationBuilder.CreateTable(
                name: "HangSanXuat",
                schema: "Sale",
                columns: table => new
                {
                    RowGuid = table.Column<Guid>(nullable: false),
                    TenHang = table.Column<string>(nullable: true),
                    NgayThanhLap = table.Column<DateTime>(nullable: true),
                    Note = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    ModifiedBy = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HangSanXuat", x => x.RowGuid);
                });

            migrationBuilder.CreateTable(
                name: "Activities",
                schema: "WF",
                columns: table => new
                {
                    ActivityGuid = table.Column<Guid>(nullable: false, defaultValueSql: "(newid())"),
                    ConditionGuid = table.Column<Guid>(nullable: true),
                    ConditionGuidOfStep = table.Column<Guid>(nullable: true),
                    WorkflowGuid = table.Column<Guid>(nullable: true),
                    ActivityName = table.Column<string>(maxLength: 255, nullable: true),
                    IsActive = table.Column<bool>(nullable: true),
                    ParentId = table.Column<Guid>(nullable: true),
                    Location = table.Column<Guid>(nullable: true),
                    StartStep = table.Column<bool>(nullable: true),
                    IsValid = table.Column<bool>(nullable: true),
                    ActivityUid = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    ModifiedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ModifiedBy = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Activities", x => x.ActivityGuid);
                });

            migrationBuilder.CreateTable(
                name: "Conditions",
                schema: "WF",
                columns: table => new
                {
                    ConditionGuid = table.Column<Guid>(nullable: false, defaultValueSql: "(newid())"),
                    WorkflowGuid = table.Column<Guid>(nullable: false),
                    ConditionData = table.Column<string>(nullable: true),
                    ConditionTrue = table.Column<string>(nullable: true),
                    ConditionFalse = table.Column<string>(nullable: true),
                    LocationGuid = table.Column<Guid>(nullable: true),
                    WorkflowType = table.Column<bool>(nullable: true),
                    StepCallback = table.Column<string>(nullable: true),
                    StepContinue = table.Column<string>(nullable: true),
                    ConditionUid = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    ModifiedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ModifiedBy = table.Column<string>(maxLength: 50, nullable: true),
                    WFStatusId = table.Column<string>(nullable: true),
                    WFStatusBackId = table.Column<string>(nullable: true),
                    WFConditionId = table.Column<string>(nullable: true),
                    WFConditionBackId = table.Column<string>(nullable: true),
                    PersonMain = table.Column<string>(nullable: true),
                    PersonSupport = table.Column<string>(nullable: true),
                    PersonFollow = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Conditions", x => x.ConditionGuid);
                });

            migrationBuilder.CreateTable(
                name: "Workflows",
                schema: "WF",
                columns: table => new
                {
                    WorkflowGuid = table.Column<Guid>(nullable: false, defaultValueSql: "(newid())"),
                    WorkflowID = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    WorkflowName = table.Column<string>(maxLength: 255, nullable: false),
                    OrganizationGuid = table.Column<Guid>(nullable: true),
                    ModuleID = table.Column<string>(unicode: false, maxLength: 10, nullable: true),
                    ModuleName = table.Column<string>(maxLength: 50, nullable: true),
                    WorkFlowData = table.Column<string>(nullable: true),
                    TableGuid = table.Column<Guid>(nullable: true),
                    TableName = table.Column<string>(maxLength: 50, nullable: true),
                    IsActive = table.Column<bool>(nullable: true, defaultValueSql: "((1))"),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    ModifiedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ModifiedBy = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Workflows", x => x.WorkflowGuid);
                });

            migrationBuilder.CreateTable(
                name: "WorkFlowSetting",
                schema: "WF",
                columns: table => new
                {
                    RowGuid = table.Column<Guid>(nullable: false, defaultValueSql: "(newid())"),
                    WorkFlowGuid = table.Column<Guid>(nullable: true),
                    WorkFlowName = table.Column<string>(maxLength: 250, nullable: true),
                    TicketID = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    TicketName = table.Column<string>(maxLength: 250, nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: true, defaultValueSql: "(getdate())"),
                    ModifiedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    ModifiedBy = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkFlowSetting", x => x.RowGuid);
                });

            migrationBuilder.CreateTable(
                name: "WorkFlowStatus",
                schema: "WF",
                columns: table => new
                {
                    CategoryID = table.Column<string>(unicode: false, maxLength: 2, nullable: false),
                    CategoryName = table.Column<string>(maxLength: 100, nullable: true),
                    Description = table.Column<string>(maxLength: 250, nullable: true),
                    OrderID = table.Column<int>(nullable: true),
                    LocationID = table.Column<string>(unicode: false, maxLength: 20, nullable: true),
                    IsActive = table.Column<bool>(nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: true, defaultValueSql: "(getdate())"),
                    ModifiedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ModifiedBy = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkFlowStatus", x => x.CategoryID);
                });

            migrationBuilder.CreateTable(
                name: "ESPrivileges",
                schema: "eim",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ActionId = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    ResourceId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ESPrivileges", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ESPrivileges_ESResources",
                        column: x => x.ResourceId,
                        principalSchema: "eim",
                        principalTable: "ESResources",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ESResAttributes",
                schema: "eim",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Key = table.Column<string>(maxLength: 50, nullable: true),
                    Value = table.Column<string>(maxLength: 255, nullable: true),
                    ResourceId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ESResAttributes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ESResAttributes_ESResources",
                        column: x => x.ResourceId,
                        principalSchema: "eim",
                        principalTable: "ESResources",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Departments",
                schema: "HR",
                columns: table => new
                {
                    DepartmentGuid = table.Column<Guid>(nullable: false),
                    OrganizationGuid = table.Column<Guid>(nullable: true),
                    OrganizationName = table.Column<string>(nullable: true),
                    ParentID = table.Column<Guid>(nullable: true),
                    DepartmentID = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    DepartmentName = table.Column<string>(maxLength: 50, nullable: false),
                    Description = table.Column<string>(maxLength: 255, nullable: true),
                    Content = table.Column<string>(type: "ntext", nullable: true),
                    EnglishName = table.Column<string>(maxLength: 255, nullable: true),
                    Address = table.Column<string>(maxLength: 255, nullable: true),
                    Email = table.Column<string>(maxLength: 255, nullable: true),
                    Phone = table.Column<string>(maxLength: 50, nullable: true),
                    Fax = table.Column<string>(maxLength: 50, nullable: true),
                    OrderID = table.Column<int>(nullable: true),
                    IsActive = table.Column<bool>(nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: true, defaultValueSql: "(getdate())"),
                    ModifiedDate = table.Column<DateTime>(type: "datetime", nullable: true, defaultValueSql: "(getdate())"),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    ModifiedBy = table.Column<string>(maxLength: 50, nullable: true),
                    ManagerGuid = table.Column<Guid>(nullable: true),
                    ManagerID = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    ManagerName = table.Column<string>(maxLength: 250, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Departments", x => x.DepartmentGuid);
                    table.ForeignKey(
                        name: "FK_Departments_Organizations_OrganizationGuid",
                        column: x => x.OrganizationGuid,
                        principalSchema: "HR",
                        principalTable: "Organizations",
                        principalColumn: "OrganizationGuid",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Employees",
                schema: "HR",
                columns: table => new
                {
                    EmployeeGuid = table.Column<Guid>(nullable: false, defaultValueSql: "(newid())"),
                    CandidateGuid = table.Column<Guid>(nullable: true),
                    CandidateID = table.Column<string>(unicode: false, maxLength: 20, nullable: true),
                    EmployeeID = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    OrganizationGuid = table.Column<Guid>(nullable: false),
                    OrganizationID = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    DepartmentGuid = table.Column<Guid>(nullable: true),
                    DepartmentID = table.Column<string>(unicode: false, maxLength: 20, nullable: true),
                    Title = table.Column<string>(maxLength: 8, nullable: true),
                    FullName = table.Column<string>(maxLength: 60, nullable: true),
                    FirstName = table.Column<string>(maxLength: 20, nullable: true),
                    MiddleName = table.Column<string>(maxLength: 20, nullable: true),
                    LastName = table.Column<string>(maxLength: 20, nullable: true),
                    Identification = table.Column<string>(unicode: false, maxLength: 20, nullable: true),
                    IssueIDDate = table.Column<DateTime>(type: "date", nullable: true),
                    IssueIDBy = table.Column<string>(maxLength: 50, nullable: true),
                    LoginID = table.Column<string>(maxLength: 50, nullable: true),
                    LoginName = table.Column<string>(maxLength: 50, nullable: true),
                    JobTitleID = table.Column<int>(nullable: true),
                    JobTitleName = table.Column<string>(maxLength: 50, nullable: true),
                    ProfestionalD = table.Column<int>(nullable: true),
                    SpecializesId = table.Column<string>(nullable: true),
                    ProfestionaName = table.Column<string>(maxLength: 50, nullable: true),
                    BirthDate = table.Column<DateTime>(type: "date", nullable: true),
                    PlaceOfBirth = table.Column<string>(maxLength: 100, nullable: true),
                    MaritalStatus = table.Column<string>(unicode: false, maxLength: 1, nullable: true),
                    Gender = table.Column<string>(unicode: false, maxLength: 1, nullable: true),
                    HireDate = table.Column<DateTime>(type: "date", nullable: true),
                    SalaryPayBy = table.Column<string>(unicode: false, maxLength: 1, nullable: true, defaultValueSql: "('M')"),
                    SalaryMethod = table.Column<string>(unicode: false, maxLength: 2, nullable: true),
                    PermanentAddress = table.Column<string>(maxLength: 255, nullable: true),
                    CurrentAddress = table.Column<string>(maxLength: 255, nullable: true),
                    CountryID = table.Column<string>(unicode: false, maxLength: 20, nullable: true, defaultValueSql: "('VN')"),
                    CountryName = table.Column<string>(maxLength: 50, nullable: true, defaultValueSql: "(N'Việt Nam')"),
                    ProvinceID = table.Column<string>(unicode: false, maxLength: 20, nullable: true),
                    ProvinceName = table.Column<string>(maxLength: 50, nullable: true),
                    DistrictID = table.Column<string>(unicode: false, maxLength: 20, nullable: true),
                    DistrictName = table.Column<string>(maxLength: 50, nullable: true),
                    WardID = table.Column<string>(unicode: false, maxLength: 20, nullable: true),
                    WardName = table.Column<string>(maxLength: 20, nullable: true),
                    HomeNumber = table.Column<string>(maxLength: 50, nullable: true),
                    EthnicID = table.Column<int>(nullable: true),
                    EthnicName = table.Column<string>(maxLength: 10, nullable: true),
                    HomePhone = table.Column<string>(unicode: false, maxLength: 15, nullable: true, defaultValueSql: "(N'()')"),
                    HomeMobile = table.Column<string>(unicode: false, maxLength: 20, nullable: true),
                    HomeEmail = table.Column<string>(maxLength: 50, nullable: true),
                    WorkPhone = table.Column<string>(unicode: false, maxLength: 15, nullable: true),
                    WorkMobile = table.Column<string>(unicode: false, maxLength: 20, nullable: true),
                    WorkEmail = table.Column<string>(maxLength: 50, nullable: true),
                    EducationID = table.Column<int>(nullable: true),
                    EducationName = table.Column<string>(maxLength: 20, nullable: true),
                    ImagePath = table.Column<string>(maxLength: 255, nullable: true),
                    Photo = table.Column<byte[]>(nullable: true),
                    TaxCode = table.Column<string>(unicode: false, maxLength: 20, nullable: true),
                    BankCode = table.Column<string>(unicode: false, maxLength: 20, nullable: true),
                    BankName = table.Column<string>(maxLength: 255, nullable: true),
                    BankAccount = table.Column<string>(nullable: true),
                    StatusOfWork = table.Column<string>(unicode: false, maxLength: 2, nullable: true),
                    EndDateWork = table.Column<DateTime>(type: "date", nullable: true),
                    Note = table.Column<string>(maxLength: 255, nullable: true),
                    StartDate = table.Column<DateTime>(type: "date", nullable: true),
                    EndDate = table.Column<DateTime>(type: "date", nullable: true),
                    ProbationPeriod = table.Column<int>(nullable: true),
                    Uniform = table.Column<string>(unicode: false, maxLength: 1, nullable: true),
                    IsConcurrently = table.Column<string>(unicode: false, maxLength: 1, nullable: true, defaultValueSql: "('N')"),
                    TimeKeepingID = table.Column<string>(unicode: false, maxLength: 20, nullable: true),
                    IsUseCar = table.Column<bool>(nullable: true),
                    CarID = table.Column<string>(unicode: false, maxLength: 10, nullable: true),
                    IsActive = table.Column<bool>(nullable: true, defaultValueSql: "((1))"),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: true, defaultValueSql: "(getdate())"),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    ModifiedBy = table.Column<string>(maxLength: 50, nullable: true),
                    ModifiedDate = table.Column<DateTime>(type: "datetime", nullable: true, defaultValueSql: "(getdate())"),
                    GenitiveId = table.Column<string>(nullable: true),
                    SalaryTierId = table.Column<string>(nullable: true),
                    RankId = table.Column<string>(nullable: true),
                    ReasonLeaveId = table.Column<int>(nullable: true),
                    ManagerId = table.Column<Guid>(nullable: true),
                    Thumb = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employees", x => x.EmployeeGuid);
                    table.ForeignKey(
                        name: "FK_Employees_Organizations_OrganizationGuid",
                        column: x => x.OrganizationGuid,
                        principalSchema: "HR",
                        principalTable: "Organizations",
                        principalColumn: "OrganizationGuid",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProjectManageDetail",
                schema: "Project",
                columns: table => new
                {
                    ProjectDetailGuid = table.Column<Guid>(nullable: false, defaultValueSql: "(newid())"),
                    ProjectGuid = table.Column<Guid>(nullable: true),
                    BCV12ID = table.Column<Guid>(nullable: true),
                    BCV24ID = table.Column<Guid>(nullable: true),
                    KPI37ID = table.Column<Guid>(nullable: true),
                    StartDate = table.Column<DateTime>(nullable: true),
                    EndDate = table.Column<DateTime>(nullable: true),
                    Cost = table.Column<decimal>(nullable: true),
                    Demonstration = table.Column<string>(nullable: true),
                    CostNb = table.Column<decimal>(nullable: true),
                    DemonstrationNb = table.Column<string>(nullable: true),
                    Note = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ModifiedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    ModifiedBy = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectManageDetail", x => x.ProjectDetailGuid);
                    table.ForeignKey(
                        name: "FK_ProjectManageDetail_ProjectManage",
                        column: x => x.ProjectGuid,
                        principalSchema: "Project",
                        principalTable: "ProjectManage",
                        principalColumn: "ProjectGuid",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SanPham",
                schema: "Sale",
                columns: table => new
                {
                    RowGuid = table.Column<Guid>(nullable: false, defaultValueSql: "(newid())"),
                    MaSanPham = table.Column<string>(nullable: true),
                    TenSanPham = table.Column<string>(nullable: true),
                    HangSanXuatGuid = table.Column<Guid>(nullable: true),
                    Chitiet = table.Column<string>(nullable: true),
                    DonGia = table.Column<decimal>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    GroupSanPhamGuid = table.Column<Guid>(nullable: true),
                    Giamgia = table.Column<int>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: true),
                    ModifiedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    MetaTitle = table.Column<string>(nullable: true),
                    MetaDescription = table.Column<string>(nullable: true),
                    MetaKeyWord = table.Column<string>(nullable: true),
                    Location = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SanPham", x => x.RowGuid);
                    table.ForeignKey(
                        name: "FK_SanPham_HangSanXuat_HangSanXuatGuid",
                        column: x => x.HangSanXuatGuid,
                        principalSchema: "Sale",
                        principalTable: "HangSanXuat",
                        principalColumn: "RowGuid",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Concurrently",
                schema: "HR",
                columns: table => new
                {
                    ConcurrentlyGuid = table.Column<Guid>(nullable: false, defaultValueSql: "(newid())"),
                    EmployeeGuid = table.Column<Guid>(nullable: false),
                    EmployeeID = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    OrganizationGuid = table.Column<Guid>(nullable: false),
                    DepartmentGuid = table.Column<Guid>(nullable: false),
                    DepartmentID = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    DepartmentName = table.Column<string>(maxLength: 255, nullable: false),
                    JobTitleID = table.Column<int>(nullable: true),
                    JobTitleName = table.Column<string>(maxLength: 50, nullable: true),
                    StartDate = table.Column<DateTime>(type: "date", nullable: true),
                    EndDate = table.Column<DateTime>(type: "date", nullable: true),
                    IsActive = table.Column<bool>(nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: true, defaultValueSql: "(getdate())"),
                    ModifiedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    ModifiedBy = table.Column<string>(maxLength: 50, nullable: true),
                    DepartmentGuid1 = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Concurrently", x => x.ConcurrentlyGuid);
                    table.ForeignKey(
                        name: "FK_Concurrently_Departments_DepartmentGuid1",
                        column: x => x.DepartmentGuid1,
                        principalSchema: "HR",
                        principalTable: "Departments",
                        principalColumn: "DepartmentGuid",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Concurrently_JobTitles_JobTitleID",
                        column: x => x.JobTitleID,
                        principalSchema: "HR",
                        principalTable: "JobTitles",
                        principalColumn: "JobTitleID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DatHang",
                schema: "Sale",
                columns: table => new
                {
                    DatHangId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    MaSanPham = table.Column<int>(nullable: false),
                    HoTen = table.Column<string>(maxLength: 250, nullable: true),
                    SDT = table.Column<string>(maxLength: 15, nullable: true),
                    Email = table.Column<string>(maxLength: 50, nullable: true),
                    SoLuong = table.Column<int>(nullable: false),
                    ThanhTien = table.Column<decimal>(type: "money", nullable: true),
                    Status = table.Column<bool>(nullable: true),
                    SanPhamRowGuid = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DatHang", x => x.DatHangId);
                    table.ForeignKey(
                        name: "FK_DatHang_SanPham_SanPhamRowGuid",
                        column: x => x.SanPhamRowGuid,
                        principalSchema: "Sale",
                        principalTable: "SanPham",
                        principalColumn: "RowGuid",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ESPrivileges_ResourceId",
                schema: "eim",
                table: "ESPrivileges",
                column: "ResourceId");

            migrationBuilder.CreateIndex(
                name: "IX_ESResAttributes_ResourceId",
                schema: "eim",
                table: "ESResAttributes",
                column: "ResourceId");

            migrationBuilder.CreateIndex(
                name: "UC_CandidateID",
                schema: "HR",
                table: "Candidates",
                column: "CandidateID",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Concurrently_DepartmentGuid1",
                schema: "HR",
                table: "Concurrently",
                column: "DepartmentGuid1");

            migrationBuilder.CreateIndex(
                name: "IX_Concurrently_JobTitleID",
                schema: "HR",
                table: "Concurrently",
                column: "JobTitleID");

            migrationBuilder.CreateIndex(
                name: "UC_DepartmentID",
                schema: "HR",
                table: "Departments",
                column: "DepartmentID",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Departments_OrganizationGuid",
                schema: "HR",
                table: "Departments",
                column: "OrganizationGuid");

            migrationBuilder.CreateIndex(
                name: "IX_Employees_OrganizationGuid",
                schema: "HR",
                table: "Employees",
                column: "OrganizationGuid");

            migrationBuilder.CreateIndex(
                name: "IX_ProjectManageDetail_ProjectGuid",
                schema: "Project",
                table: "ProjectManageDetail",
                column: "ProjectGuid");

            migrationBuilder.CreateIndex(
                name: "IX_DatHang_SanPhamRowGuid",
                schema: "Sale",
                table: "DatHang",
                column: "SanPhamRowGuid");

            migrationBuilder.CreateIndex(
                name: "IX_SanPham_HangSanXuatGuid",
                schema: "Sale",
                table: "SanPham",
                column: "HangSanXuatGuid");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AnhSanPhamViews");

            migrationBuilder.DropTable(
                name: "Auto");

            migrationBuilder.DropTable(
                name: "DashboardView1");

            migrationBuilder.DropTable(
                name: "DepartmentsJtableViews");

            migrationBuilder.DropTable(
                name: "DepartmentsTreeview");

            migrationBuilder.DropTable(
                name: "DepartmentsViewItems");

            migrationBuilder.DropTable(
                name: "EmployeesExport");

            migrationBuilder.DropTable(
                name: "EmployeesJtableView");

            migrationBuilder.DropTable(
                name: "EmployeesView");

            migrationBuilder.DropTable(
                name: "EmployeesViewLogin");

            migrationBuilder.DropTable(
                name: "Object_Combobox");

            migrationBuilder.DropTable(
                name: "Object_Jexcel");

            migrationBuilder.DropTable(
                name: "ObjectNew_Combobox");

            migrationBuilder.DropTable(
                name: "PhotoView");

            migrationBuilder.DropTable(
                name: "ProjectManageJtableView");

            migrationBuilder.DropTable(
                name: "ProjectManageResultView2");

            migrationBuilder.DropTable(
                name: "ProjectManageResultView3");

            migrationBuilder.DropTable(
                name: "ProjectManageView1");

            migrationBuilder.DropTable(
                name: "ProjectManageView3");

            migrationBuilder.DropTable(
                name: "ProjectManageView4");

            migrationBuilder.DropTable(
                name: "ProjectManageView5");

            migrationBuilder.DropTable(
                name: "ProjectManageView6");

            migrationBuilder.DropTable(
                name: "ProjectReportView1");

            migrationBuilder.DropTable(
                name: "ProjectReportView2");

            migrationBuilder.DropTable(
                name: "ProjectReportView3");

            migrationBuilder.DropTable(
                name: "ProjectReportView5");

            migrationBuilder.DropTable(
                name: "ProjectReportView6");

            migrationBuilder.DropTable(
                name: "ReportViewSheet1");

            migrationBuilder.DropTable(
                name: "ReportViewSheet2");

            migrationBuilder.DropTable(
                name: "ReportViewSheet3");

            migrationBuilder.DropTable(
                name: "ReportViewSheet4");

            migrationBuilder.DropTable(
                name: "SanPhamViews");

            migrationBuilder.DropTable(
                name: "SQLCOMMANDS");

            migrationBuilder.DropTable(
                name: "ESActions",
                schema: "eim");

            migrationBuilder.DropTable(
                name: "ESPrivileges",
                schema: "eim");

            migrationBuilder.DropTable(
                name: "ESResAttributes",
                schema: "eim");

            migrationBuilder.DropTable(
                name: "ESUserPrivileges",
                schema: "eim");

            migrationBuilder.DropTable(
                name: "Candidates",
                schema: "HR");

            migrationBuilder.DropTable(
                name: "Concurrently",
                schema: "HR");

            migrationBuilder.DropTable(
                name: "Contracts",
                schema: "HR");

            migrationBuilder.DropTable(
                name: "Educations",
                schema: "HR");

            migrationBuilder.DropTable(
                name: "Employees",
                schema: "HR");

            migrationBuilder.DropTable(
                name: "Profestionals",
                schema: "HR");

            migrationBuilder.DropTable(
                name: "Rank",
                schema: "HR");

            migrationBuilder.DropTable(
                name: "ShiftOfEmployees",
                schema: "HR");

            migrationBuilder.DropTable(
                name: "Shifts",
                schema: "HR");

            migrationBuilder.DropTable(
                name: "Specializes",
                schema: "HR");

            migrationBuilder.DropTable(
                name: "BCV12",
                schema: "Project");

            migrationBuilder.DropTable(
                name: "BCV24",
                schema: "Project");

            migrationBuilder.DropTable(
                name: "ClassifyKT",
                schema: "Project");

            migrationBuilder.DropTable(
                name: "ClassifyTMQT",
                schema: "Project");

            migrationBuilder.DropTable(
                name: "ClassifyTV",
                schema: "Project");

            migrationBuilder.DropTable(
                name: "Clue",
                schema: "Project");

            migrationBuilder.DropTable(
                name: "Combination",
                schema: "Project");

            migrationBuilder.DropTable(
                name: "FieldClassification",
                schema: "Project");

            migrationBuilder.DropTable(
                name: "GroupPlan",
                schema: "Project");

            migrationBuilder.DropTable(
                name: "KPI236",
                schema: "Project");

            migrationBuilder.DropTable(
                name: "KPI37",
                schema: "Project");

            migrationBuilder.DropTable(
                name: "Objective",
                schema: "Project");

            migrationBuilder.DropTable(
                name: "Partner",
                schema: "Project");

            migrationBuilder.DropTable(
                name: "ProjectCode",
                schema: "Project");

            migrationBuilder.DropTable(
                name: "ProjectCombination",
                schema: "Project");

            migrationBuilder.DropTable(
                name: "ProjectManageDetail",
                schema: "Project");

            migrationBuilder.DropTable(
                name: "ProjectManageDetailC1",
                schema: "Project");

            migrationBuilder.DropTable(
                name: "ProjectManageDetailC2",
                schema: "Project");

            migrationBuilder.DropTable(
                name: "ProjectManageExpired",
                schema: "Project");

            migrationBuilder.DropTable(
                name: "ProjectManageGroup",
                schema: "Project");

            migrationBuilder.DropTable(
                name: "ProjectName",
                schema: "Project");

            migrationBuilder.DropTable(
                name: "StateAgencies",
                schema: "Project");

            migrationBuilder.DropTable(
                name: "Treasury",
                schema: "Project");

            migrationBuilder.DropTable(
                name: "AnhSanPham",
                schema: "Sale");

            migrationBuilder.DropTable(
                name: "DatHang",
                schema: "Sale");

            migrationBuilder.DropTable(
                name: "GroupSanPham",
                schema: "Sale");

            migrationBuilder.DropTable(
                name: "Activities",
                schema: "WF");

            migrationBuilder.DropTable(
                name: "Conditions",
                schema: "WF");

            migrationBuilder.DropTable(
                name: "Workflows",
                schema: "WF");

            migrationBuilder.DropTable(
                name: "WorkFlowSetting",
                schema: "WF");

            migrationBuilder.DropTable(
                name: "WorkFlowStatus",
                schema: "WF");

            migrationBuilder.DropTable(
                name: "ESResources",
                schema: "eim");

            migrationBuilder.DropTable(
                name: "Departments",
                schema: "HR");

            migrationBuilder.DropTable(
                name: "JobTitles",
                schema: "HR");

            migrationBuilder.DropTable(
                name: "ProjectManage",
                schema: "Project");

            migrationBuilder.DropTable(
                name: "SanPham",
                schema: "Sale");

            migrationBuilder.DropTable(
                name: "Organizations",
                schema: "HR");

            migrationBuilder.DropTable(
                name: "HangSanXuat",
                schema: "Sale");
        }
    }
}
