﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis.Options;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using HRM.Models;
using Newtonsoft.Json.Serialization;
using Microsoft.Extensions.Caching.SqlServer;
using Aspose.Pdf.Forms;
namespace HRM
{
    public class AppSettings
    {
        public string UrlStatic { get; set; }
        public string DBName_HR { get; set; }
    }
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //Add cookie
            var appSettings = Configuration.GetSection("AppSettings");
            var connectionString = Configuration.GetConnectionString("HRMConnection");
            services.Configure<AppSettings>(appSettings);
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie(options =>
                {
                    options.LoginPath = "/Account/login";
                    options.Cookie.Name = "AshProgHelpCookie";
                }
            );
            services.AddIdentity<AppUser, AppRole>(options =>
            {
                options.User.RequireUniqueEmail = true;
                options.Password.RequiredLength = 6;
                options.Password.RequireLowercase = false;
                options.Password.RequireUppercase = false;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireDigit = false;
            }).AddEntityFrameworkStores<IdentityAppContext>()
            .AddDefaultTokenProviders();

            services.AddDbContext<IdentityAppContext>(cfg =>
            {
                cfg.UseSqlServer(connectionString);
            });
            services.AddDbContextPool<HRMDBContext>(_ =>
            {
                _.UseSqlServer(connectionString, options =>
                {
                    options.CommandTimeout(1800); // 3 minutes
                });
            });
            // Giữ nguyên obj khi truy vấn lên server
            services.AddMvc().AddJsonOptions(options => options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);
            services.AddMvc().AddJsonOptions(options => options.SerializerSettings.ContractResolver = new DefaultContractResolver());
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });
            var environment = services.BuildServiceProvider().GetRequiredService<IHostingEnvironment>();


            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }
            // start cấu hình để trong WebContext gọi được store
            var appSettings = Configuration.GetSection("AppSettings");
            var connectionString = Configuration.GetConnectionString("HRMConnection");
            var cache = new SqlServerCache(new SqlServerCacheOptions()
            {
                ConnectionString = connectionString,
                SchemaName = "dbo",
                TableName = "AspNetSessions"
            });
            System.Web.WebContext.Configure(app.ApplicationServices.
              GetRequiredService<IHttpContextAccessor>(), appSettings, cache, Configuration
          );
            // end cấu hình để trong WebContext gọi được store

            app.UseHttpsRedirection();
            app.UseStaticFiles();


            app.UseAuthentication();
            app.UseCookiePolicy();
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                  name: "areas",
                  template: "{area:exists}/{controller=Account}/{action=Login}/{id?}"
                );
            });
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Account}/{action=Login}/{id?}");
            });
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Account}/{action=Login}",
                    defaults: new { area = "Hr" }, constraints: new { area = "Hr" });
            });
        }
    }
}
