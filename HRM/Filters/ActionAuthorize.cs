﻿using HRM.Models;
using IdentityModel.Client;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HRM.Filters
{
    public class ActionAuthorize : ActionFilterAttribute, IActionFilter
    {
        public List<string> Actions { get; set; }
        public string Resource { get; set; }
        public ActionAuthorize(params EAction[] actions)
        {
            Actions = new List<string>();
            foreach (var item in actions)
            {
                Actions.Add(item.Value());
            }
            Actions.Distinct();
        }

        [Obsolete]
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            try
            {
                var dataExp = WebContext.Exp;
                if ((dataExp - DateTime.Now).TotalMinutes < 1)
                {
                    DiscoveryResponse disco = DiscoveryClient.GetAsync(WebContext.AppSetting("Authority")).Result;
                    if (disco.IsError) throw new Exception(disco.Error);

                    var tokenClient = GetTokenClient(disco);

                    var rt = WebContext.UserRefreshToken;
                    var tokenResult = tokenClient.RequestRefreshTokenAsync(rt).Result;

                    if (!tokenResult.IsError)
                    {
                        var oldIdToken = WebContext.UserAccessToken;
                        var newAccessToken = tokenResult.AccessToken;
                        var newRefreshToken = tokenResult.RefreshToken;

                        var tokens = new List<AuthenticationToken>
                            {
                                new AuthenticationToken {Name = OpenIdConnectParameterNames.IdToken, Value = oldIdToken},
                                new AuthenticationToken
                                {
                                    Name = OpenIdConnectParameterNames.AccessToken,
                                    Value = newAccessToken
                                },
                                new AuthenticationToken
                                {
                                    Name = OpenIdConnectParameterNames.RefreshToken,
                                    Value = newRefreshToken
                                }
                            };
                        context.HttpContext.Authentication.SignOutAsync(WebContext.AppSetting("AuthenticationScheme"));
                        context.HttpContext.Authentication.SignOutAsync("OIDC");
                    }
                }
            }
            catch (Exception)
            {
                base.OnActionExecuting(context);
            }
        }

        [Obsolete]
        private static TokenClient GetTokenClient(DiscoveryResponse disco)
        {
            return new TokenClient(disco.TokenEndpoint, WebContext.AppSetting("ClientId"), WebContext.AppSetting("ClientSecret"));
        }

        public override void OnActionExecuted(ActionExecutedContext context)
        {

            var result = context.Result as ViewResult;

            var permApp = WebContext.CheckPermissionApp();
            if (permApp != 2)
            {
                if (!context.HttpContext.Request.IsAjaxRequest())
                {
                    context.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                    {
                        controller = "Home",
                        action = "AppDenied",
                        code = permApp
                    }));
                }
                else
                {
                    context.Result = new JsonResult(new { Error = true, ErrorCode = 403, ErrorType = "Application" });
                }
            }
            else
            {
                var controller = context.ActionDescriptor.RouteValues["controller"];
                if (string.IsNullOrEmpty(Resource)) Resource = controller;
                var nav = WebContext.Navigation;
                var resources = nav.Where(x => x.Code.Equals(Resource));
                var auth = false;
                try
                {
                    if (resources != null && resources.Count() > 0)
                    {
                        var res = resources.First();
                        var res_parent = nav.Where(x => res.ParentId.HasValue && x.Id == res.ParentId.Value);
                        var Privileges = WebContext.GetPermission(res.Id);
                        auth = Privileges.Any(x => Actions.Contains(x));
                        if (result != null)
                        {
                            result.ViewData["Page-Title"] = res.Title;
                            result.ViewData["Page-Url"] = res.Url;
                            result.ViewData["Page-Action"] = string.Join(",", Actions);
                            result.ViewData["Title"] = res.Title;
                            result.ViewData["ResourceCode"] = Resource;
                            if (res_parent != null)
                            {
                                result.ViewData["Parent-Page-Title"] = res_parent.First().Title;
                                result.ViewData["Parent-Page-Url"] = res_parent.First().Url;
                            }
                        }
                        if (!context.HttpContext.Request.IsAjaxRequest())
                        {
                            var css = "";
                            var act = new List<string>();
                            foreach (var p in Privileges)

                            {
                                css += "*[data-action='" + p + "']{display:inline-block;} ";
                                act.Add(p);
                            }
                            result.ViewData["CSS-PERMISSION"] = css;
                            result.ViewData["ACTION-PERMISSION"] = string.Join(",", act);
                        }

                    }
                }
                catch (Exception ex)
                { }
                if (!auth)
                {
                    if (!context.HttpContext.Request.IsAjaxRequest())
                    {
                        context.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                        {
                            controller = "Home",
                            action = "ResDenied",
                        }));
                    }
                    else
                    {
                        context.Result = new JsonResult(new { Error = true, ErrorCode = 403, ErrorType = "RES" });
                    }
                }
            }
            base.OnActionExecuted(context);
        }
    }
    public static partial class ExtensionMethods
    {
        public static bool IsAjaxRequest(this Microsoft.AspNetCore.Http.HttpRequest request)
        {
            if (request == null)
                throw new ArgumentNullException("request");

            if (request.Headers != null)
                return request.Headers["X-Requested-With"] == "XMLHttpRequest";
            return false;
        }
    }
}
