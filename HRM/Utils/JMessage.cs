﻿
using OfficeOpenXml.FormulaParsing.Excel.Functions.Information;
using System;
using System.Collections.Generic;

namespace HRM.Utils
{
    public class JListError
    {
        /// <summary>
        /// Có lỗi hay không có lỗi
        /// </summary>
        public bool Error { get; set; }
        /// <summary>
        /// Thông báo
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// Đối tượng attach kèm theo thông báo
        /// </summary>
        public object Object { get; set; }
        public object Data { get; set; }
        public JListError()
        {

        }
        public JListError(string title, bool error, object obj, object data)
        {
            Title = title; Error = error; Object = obj; Data = data;
        }
    }
    public class ListError
    {
        /// <summary>
        /// Chức năng
        /// </summary>
        public string ModuleId { get; set; }
        /// <summary>
        /// Số chứng từ
        /// </summary>
        public string VoucherNo { get; set; }
        /// <summary>
        /// Có lỗi hay không có lỗi
        /// </summary>
        public bool Error { get; set; }
        /// <summary>
        /// Thông báo
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// Đối tượng attach kèm theo thông báo
        /// </summary>
        public object Object { get; set; }
        public ListError()
        {

        }
        public ListError(string title, bool error, object obj)
        {
            Title = title; Error = error; Object = obj;
        }
    }
    /// <summary>
    /// Đối tượng đóng gói các thông báo khi thêm, sửa, xóa...
    /// </summary>
    /// <modified>
    /// Author				    created date					comments
    /// LongND					05/07/2013				        Tạo mới
    ///</modified>
    ///
    [Serializable]
    public class JMessage
    {
        /// <summary>
        /// ID của bản ghi được thêm, sửa, xóa
        /// </summary>
        public int ID { get; set; }
        /// <summary>
        /// Thông báo
        /// </summary>
        public string Title { get; set; }
        public string TitleError { get; set; }
        public object Data { get; set; }


        /// <summary>
        /// Có lỗi hay không có lỗi
        /// </summary>
        public bool Error { get; set; }
        /// <summary>
        /// Đối tượng attach kèm theo thông báo
        /// </summary>
        public object Object { get; set; }
        public int? WorkFlowsOrder { set; get; }

        public JMessage(int id, string title, bool error, object obj, int orderid)
        {
            ID = id; Title = title; Error = error; Object = obj; WorkFlowsOrder = orderid;
        }
        public JMessage(int id, string title, string titleerror, int? workFlowsOrder, bool error, object obj)
        {
            JListError = new List<JListError>();
            ListError = new List<ListError>();
            ID = id; Title = title; TitleError = titleerror; WorkFlowsOrder = workFlowsOrder; Error = error; Object = obj;
        }
        public JMessage(int id, string title, string titleerror, int? workFlowsOrder, bool error, object obj, object data)
        {
            JListError = new List<JListError>();
            ListError = new List<ListError>();
            ID = id; Title = title; TitleError = titleerror; WorkFlowsOrder = workFlowsOrder; Error = error; Object = obj; Data = data;
        }
        public JMessage()
        {
            JListError = new List<JListError>();
            ListError = new List<ListError>();
        }
        public List<JListError> JListError { get; set; }
        public List<ListError> ListError { get; set; }
    }
}
