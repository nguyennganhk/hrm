﻿using Microsoft.AspNetCore.Mvc;
using System.IO;
using System.Web;
namespace HRM.Utils
{
    public class JAttachment
    {
        public int ID { set; get; }
        public string DepartmentID { set; get; }
        public int ContractID { set; get; }
        public int InsuranceID { set; get; }
        public string CertificateID { set; get; }
        public int SalaryID { set; get; }
        public int RewardDisciplineID { set; get; }
        public string FileName { set; get; }
        public string FileNameRoot { set; get; }
    }
}
