﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace HRM.Utils
{
    public class TempSub
    {
        public int[] IdI { set; get; }
        public string[] IdS { set; get; }
        public DateTime? Date { get; set; }
    }
    public class Order
    {
        public int Column { get; set; }
        public string Dir { get; set; }
    }
    public class Search
    {
        public string value { get; set; }
        public string regex { get; set; }
    }
    public class Column
    {
        public string Data { get; set; }
        public string Name { get; set; }
        public bool Searchable { get; set; }
        public bool Orderable { get; set; }
        public Search Search { get; set; }
    }
    public class Dynamic
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
    
    public class JTableModel
    {

        public int Draw { get; set; }
        public List<Column> Columns { get; set; }
        public List<Order> Order { get; set; }
        public int Start { get; set; }
        public int Length { get; set; }
        public List<Search> searchs { get; set; }
        public Search search { get; set; }
        public IDictionary<string, object> Dynamic { get; set; }
        public string QueryOrderBy
        {
            get
            {
                try
                {
                    var lstSort = new List<string>();
                    foreach (var item in GetSortedColumns())
                    {
                        var sdir = "";
                        sdir = item.Direction.ToString().Equals(SortingDirection.Ascending.ToString()) ? "" : "DESC";
                        lstSort.Add(string.Format("{0} {1}", item.PropertyName, sdir));

                    }
                    return string.Join(",", lstSort);
                }
                catch (Exception)
                {

                    return "";
                }
            }



        }
        public int CurrentPage
        {
            get
            {
                var d = (double)((double)Start / (double)Length);
                return (int)Math.Ceiling(d) + 1;
            }
        }
        private IEnumerable<SortedColumn> GetSortedColumns()
        {
            if (!Order.Any())
            {
                return new ReadOnlyCollection<SortedColumn>(new List<SortedColumn>());
            }
            var sortedColumns = Order.Select(item => new SortedColumn(string.IsNullOrEmpty(Columns[item.Column].Name) ? Columns[item.Column].Data : Columns[item.Column].Name, item.Dir)).ToList();
            return sortedColumns.AsReadOnly();
        }
    }
    public class Auto
    {
        [Key]
        public string AliasId { get; set; }
        public bool? IsEdit { get; set; }
        public bool? IsNumberAuto { get; set; }
        public string Prefixs { get; set; }
        public string ModuleId { get; set; }
        public string VoucherType { get; set; }
        //Phiếu nhập
        public bool? IsEditPOV { get; set; }
        public bool? IsNumberAutoPOV { get; set; }
        //Phiếu nhập
        public bool? IsEditINVE { get; set; }
        public bool? IsNumberAutoINVE { get; set; }
        //Chứng từ công nợ
        public bool? IsEditDEBT { get; set; }
        public bool? IsNumberAutoDEBT { get; set; }
        //Phiếu chi
        public bool? IsEditCA { get; set; }
        public bool? IsNumberAutoCA { get; set; }
        //Uỷ nhiệm chi
        public bool? IsEditBA { get; set; }
        public bool? IsNumberAutoBA { get; set; }
    }
    public class SortedColumn
    {
        private const string Ascending = "asc";
        public SortedColumn(string propertyName, string sortingDirection)
        {
            PropertyName = propertyName;
            Direction = sortingDirection.Equals(Ascending) ? SortingDirection.Ascending : SortingDirection.Descending;
        }
        public string PropertyName { get; private set; }
        public SortingDirection Direction { get; private set; }
        public override int GetHashCode()
        {
            var directionHashCode = Direction.GetHashCode();
            return PropertyName != null ? PropertyName.GetHashCode() + directionHashCode : directionHashCode;
        }
        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }
            if (GetType() != obj.GetType())
            {
                return false;
            }
            var other = (SortedColumn)obj;
            if (other.Direction != Direction)
            {
                return false;
            }
            return other.PropertyName == PropertyName;
        }
    }
    public enum SortingDirection
    {
        Ascending,
        Descending
    }
    public static class Fun_Table_Filter
    {
        /// <summary>
        /// Get data filter
        /// </summary>
        /// <param name="o">data list input</param>
        /// <returns></returns>
        public static void Set_ColumnFilter(List<_filterData> o, SqlParameter p)
        {
            DataTable _rs = new DataTable();
            _rs.Columns.Add("Type");
            _rs.Columns.Add("Condition");
            _rs.Columns.Add("Key");
            _rs.Columns.Add("ValueInput");
            if (o != null)
            {
                foreach (var _i in o)
                {
                    _rs.Rows.Add(
                        _i.Type,
                        _i.Condition,
                        _i.Key,
                        _i.Value
                        );
                }
            }
            p.TypeName = "[dbo].[FilterTable]";
            p.Value = _rs;
        }
    }
    public class _filterData
    {
        public string Type { get; set; }
        //Condition : =, <=, >=, <>
        public string Condition { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
    }
    public class JTableSearch : JTableModel
    {        
         public string DepartmentId { set; get; }
        public string Title { set; get; }
        public string Keyword { set; get; }
        public string LoginName { set; get; }
        public string Permisstion { set; get; }
        public List<int?> lstInt { set; get; }
        public string Years { set; get; }
        public List<string> lstString { set; get; }
        public List<string> lstHangSanXuatGuid { set; get; }
        public List<string> lstGroupSanPhamGuid { set; get; }
        public bool? IsActive { set; get; }
        public DateTime? StartDate { set; get; }
        public DateTime? EndDate { set; get; }
        public string Type { set; get; }
        public Guid? GroupSanPhamGuid { set; get; }
        
    }
}
