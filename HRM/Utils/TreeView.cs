﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HRM.Utils
{
    public class TreeView
    {
        public int? Id { get; set; }
        public string Title { get; set; }
        public Guid? DepartmentGuid { get; set; }
        public Guid? GroupGuid { get; set; }
        public Guid? ParentIDGroup { get; set; }
        public string GroupName { get; set; }
        public string DepartmentId { get; set; }
        public Guid? SaleGroupGuid { get; set; }
        public string SaleGroupName { get; set; }
        public string DepartmentName { get; set; }
        public int? ParentId { get; set; }
        public int? Order { get; set; }
        public bool HasChild { get; set; }
        public string BusinessID { set; get; }
        public string ReportDepartmentId { set; get; }
        public string ReportBlockId { set; get; }
        public string AgencyId { get; set; }
        public string AccountId { get; set; }
        public string AccountName { get; set; }
        public string ProductGroupId { get; set; }
        public string CategoryId { get; set; }
        public int? OrderId { get; set; }
        public string ParentCode { get; set; }
        public Guid OrganizationGuid { get; set; }
        public string OrganizationId { get; set; }
        public string OrganizationName { get; set; }
        public int? JobTitleId { get; set; }
        public string JobTitleName { get; set; }
        public int? CriteriaId { get; set; }
        public string CriteriaName { get; set; }
        public Guid? PlanGuid { get; set; }
        public string AccountNature { get; set; }
        public Guid? CategoryOfTaskGuid { get; set; }
        public Guid? AddressBookGuid { set; get; }
        public Guid? ClueGuid { get; set; }
    }
}
