﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HRM.Utils
{
    public static class ActionPermission
    {
        public static String Add { get { return "Add"; } }
        public static String Update { get { return "Update"; } }
        public static String Delete { get { return "Delete"; } }
        public static String View { get { return "View"; } }
        public static String Approval { get { return "Approval"; } }
        public static String Comment { get { return "Comment"; } }
        public static String Forward { get { return "Forward"; } }
    }
    public class DepartmenEn
    {
        public string DepartmentID { set; get; }
    }
}
