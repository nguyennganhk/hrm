﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace HRM.Utils
{
    public static class Utilities
    {
        public static byte[] StreamToByteArray(Stream stream)
        {
            if (stream is MemoryStream)
            {
                return ((MemoryStream)stream).ToArray();
            }
            else
            {
                // Jon Skeet's accepted answer 
                return ReadFully(stream);
            }
        }
        public static byte[] ReadFully(this Stream input)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                input.CopyTo(ms);
                return ms.ToArray();
            }
        }
        public static string ExportExcel(ExcelPackage stream, string WebRootPath, string FileName)
        {
            var streamagain = new MemoryStream(stream.GetAsByteArray());
            MemoryStream storeStream = new MemoryStream();

            storeStream.SetLength(streamagain.Length);
            streamagain.Read(storeStream.GetBuffer(), 0, (int)streamagain.Length);

            storeStream.Flush();
            streamagain.Close();

            SaveMemoryStream(storeStream, WebRootPath + "/data/Export/" + FileName + ".xlsx");
            storeStream.Close();
            return "/data/Export/" + FileName + ".xlsx";
        }
        public class Auto
        {
            public string AliasId { get; set; }
            public bool? IsEdit { get; set; }
            public bool? IsNumberAuto { get; set; }
            public string Prefixs { get; set; }
            public string ModuleId { get; set; }
            public string VoucherType { get; set; }
            //Phiếu nhập
            public bool? IsEditPOV { get; set; }
            public bool? IsNumberAutoPOV { get; set; }
            //Phiếu nhập
            public bool? IsEditINVE { get; set; }
            public bool? IsNumberAutoINVE { get; set; }
            //Chứng từ công nợ
            public bool? IsEditDEBT { get; set; }
            public bool? IsNumberAutoDEBT { get; set; }
            //Phiếu chi
            public bool? IsEditCA { get; set; }
            public bool? IsNumberAutoCA { get; set; }
            //Uỷ nhiệm chi
            public bool? IsEditBA { get; set; }
            public bool? IsNumberAutoBA { get; set; }
        }
        public static void SaveMemoryStream(MemoryStream ms, string fileName)
        {
            var outStream = File.OpenWrite(fileName);
            ms.WriteTo(outStream);
            outStream.Flush();
            outStream.Close();
        }
        //hàm định dạng tiền tệ
        public static string FormatCurrency(decimal? input)
        {
            NumberFormatInfo nfi;

            // pretend we're in Vietnam
            Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo("vi-VN");
            nfi = CultureInfo.CurrentCulture.NumberFormat.Clone() as NumberFormatInfo;
            nfi.CurrencySymbol = "";
            var number = Convert.ToDouble(input);
            return (number).ToString("c", nfi);         
        }
        //hàm định dạng tiền tệ
        public static string FormatCurrency2(decimal? input)
        {          
            var info = CultureInfo.GetCultureInfo("vi-VN");
            return string.Format(CultureInfo.InvariantCulture, "{0:N0}", input);
        }

    }
}
