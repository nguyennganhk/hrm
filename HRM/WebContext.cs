﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System.Net.Http;
using Newtonsoft.Json;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Caching.SqlServer;
using Microsoft.Extensions.Primitives;
using System.Security.Claims;
using System.Security.Cryptography;
using System.IO;
using System.Text;
using HRM.Models;
using System.Data.SqlClient;
using System.Data;
using Microsoft.EntityFrameworkCore;
using OfficeOpenXml.FormulaParsing.Utilities;
using DocumentFormat.OpenXml.InkML;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System.Net.Http;
namespace System.Web
{

    public static class WebContext
    {
        private static IHttpContextAccessor m_httpContextAccessor;
        private static IConfigurationSection m_appSettings;
        private static SqlServerCache _cache;
        public static IConfiguration _Config;
        public static void Configure(IHttpContextAccessor httpContextAccessor, IConfigurationSection appSettings, SqlServerCache cache, IConfiguration Configuration)
        {
            m_httpContextAccessor = httpContextAccessor;
            m_appSettings = appSettings;
            _cache = cache;
            _Config = Configuration;
        }


        private const string SIL_PASSWORD = "IsoleteStoragePassword";
        private const string DB_PASSWORD = "DatabasePassword";
        private const string SIL_SALT = "IsoleteStorageSalt";
        private const string DB_SALT = "DatabaseSalt";
        public static string EncryptString(string input, bool forDatabase = false)
        {
            string password = forDatabase ? DB_PASSWORD : SIL_PASSWORD;
            string salt = forDatabase ? DB_SALT : SIL_SALT;

            return EncryptString(input, password, salt);
        }

        public static string EncryptString(string input, string password, string salt)
        {
            try
            {
                // Our symmetric encryption algorithm
                using (Aes aes = new AesManaged())
                {
                    // We're using the PBKDF2 standard for password-based key generation
                    Rfc2898DeriveBytes deriveBytes = new Rfc2898DeriveBytes(password, Encoding.UTF8.GetBytes(salt));
                    // Setting parameters
                    aes.BlockSize = aes.LegalBlockSizes[0].MaxSize;
                    aes.KeySize = aes.LegalKeySizes[0].MaxSize;
                    aes.Key = deriveBytes.GetBytes(aes.KeySize / 8);
                    aes.IV = deriveBytes.GetBytes(aes.BlockSize / 8);

                    using (MemoryStream encryptionStream = new MemoryStream())
                    {
                        using (CryptoStream encrypt = new CryptoStream(encryptionStream, aes.CreateEncryptor(), CryptoStreamMode.Write))
                        {
                            byte[] utfData = UTF8Encoding.UTF8.GetBytes(input);
                            encrypt.Write(utfData, 0, utfData.Length);
                            encrypt.FlushFinalBlock();
                        }
                        return Convert.ToBase64String(encryptionStream.ToArray());
                    }
                }
            }
            catch
            {
                return string.Empty;
            }
        }
        public static string ClientIP
        {
            get
            {
                return GetRequestIP(true);
            }
        }
        public static string GetRequestIP(bool tryUseXForwardHeader = true)
        {
            string ip = null;
            if (tryUseXForwardHeader)
                ip = GetHeaderValueAs<string>("X-Forwarded-For").SplitCsv().FirstOrDefault();
            if (ip.IsNullOrWhitespace() && m_httpContextAccessor.HttpContext?.Connection?.RemoteIpAddress != null)
                ip = m_httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();

            if (ip.IsNullOrWhitespace())
                ip = GetHeaderValueAs<string>("REMOTE_ADDR");
            if (ip.IsNullOrWhitespace())
                throw new Exception("Unable to determine caller's IP.");

            return ip;
        }


        public static string GetAppSetting(string key)
        {
            try
            {
                return m_appSettings.GetValue<string>(key);
            }
            catch (Exception)
            {

                return "";
            }
        }
        public static string UrlStatic
        {
            get
            {
                return "/urlstatic"; /*m_appSettings.GetValue<string>("UrlStatic");*/
            }
        }
        public static string AppSetting(string key)
        {
            return m_appSettings.GetValue<string>(key);
        }
        public static string Application
        {
            get
            {
                return m_appSettings.GetValue<string>("Application");
            }
        }
        public static string UserAccessToken
        {
            get
            {
                var accessToken = m_httpContextAccessor.HttpContext.GetTokenAsync("access_token").Result;
                return accessToken;
            }
        }
        public static string UserRefreshToken
        {
            get
            {
                var accessToken = m_httpContextAccessor.HttpContext.GetTokenAsync("refresh_token").Result;
                return accessToken;
            }
        }
        public static string SessionId
        {
            get
            {
                return GetClaim("sid");
            }
        }
        public static string GetClaim(string key)
        {
            try
            {
                var result = new List<string>();
                foreach (var claim in Current.User.Claims)
                {
                    if (claim.Type.Equals(key))
                    {
                        result.Add(claim.Value);
                    }
                }
                return string.Join(";", result);
            }
            catch (Exception)
            {

                return "";
            }
        }
        public static bool SetClaim(string key, string value)
        {
            try
            {
                var cp = Current.User.Identities.First();
                 var result = new List<string>();
                foreach (var claim in Current.User.Claims)
                {
                    if (claim.Type.Equals(key))
                    {
                        cp.RemoveClaim(claim); break;
                    }
                }
                cp.AddClaim(new Claim(key, value));
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }
        public static DateTime Exp
        {
            get
            {
                DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
                dtDateTime = dtDateTime.AddSeconds(Convert.ToDouble(GetClaim("exp"))).ToLocalTime();
                return dtDateTime;
            }
        }
        public static DateTime Nbf
        {
            get
            {
                DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
                dtDateTime = dtDateTime.AddSeconds(Convert.ToDouble(GetClaim("nbf"))).ToLocalTime();
                return dtDateTime;
            }
        }
        public static DateTime Iat
        {
            get
            {
                DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
                dtDateTime = dtDateTime.AddSeconds(Convert.ToDouble(GetClaim("iat"))).ToLocalTime();
                return dtDateTime;
            }
        }
        public static DateTime Auth_Time
        {
            get
            {
                DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
                dtDateTime = dtDateTime.AddSeconds(Convert.ToDouble(GetClaim("auth_time"))).ToLocalTime();
                return dtDateTime;
            }
        }

        public static string UserName
        {
            get
            {       
                return Current.User.Identity.Name;
                
            }
        }
        public static HttpContext Current
        {
            get
            {
                return m_httpContextAccessor.HttpContext;
            }
        }

        public static List<Resource> GetResources(string groupkey)
        {
            try
            {
                var optionsBuilder = new DbContextOptionsBuilder<HRMDBContext>();
                var connectionString = _Config.GetConnectionString("HRMConnection");
                optionsBuilder
               .UseSqlServer(connectionString, providerOptions => providerOptions.CommandTimeout(60))
                       .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);

                using (var context = new HRMDBContext(optionsBuilder.Options))
                {
                    var b = context.Resource.OrderBy(x=>x.Ord).ToList();
                    return b;
                }
                //string sql = "[eim].[SP_HRM_GetResources] @GroupResourceId";
                //var GroupResourceId = new SqlParameter("@GroupResourceId", SqlDbType.VarChar); GroupResourceId.Value = groupkey;               
                //var data = _context.Resource.FromSql(sql, GroupResourceId).ToList();             
                //return data;
            }
            catch (Exception ex)
            {
                return new List<Resource>();
            }
        }
        //public static List<Resource> GetStatus(string groupkey)
        //{
        //    try
        //    {
        //        var key = "Resources@" + UserName + "@" + Application + "@" + groupkey;
        //        var cacheEntryOptions = new DistributedCacheEntryOptions().SetSlidingExpiration(TimeSpan.FromSeconds(15));
        //        var content = _cache.GetString(key);
        //        if (string.IsNullOrEmpty(content))
        //        {
        //            var client = new HttpClient();
        //            client.SetBearerToken(UserAccessToken);
        //            content = client.GetStringAsync(m_appSettings.GetValue<string>("Api") + "/api/Permission/Resources/" + groupkey).Result;
        //            _cache.SetString(key, content, cacheEntryOptions);

        //        }
        //        var data = JsonConvert.DeserializeObject<List<Resource>>(content);
        //        return data;
        //    }
        //    catch (Exception ex)
        //    {
        //        return new List<Resource>();
        //    }
        //}
        //public static List<Resource> ResNavigation
        //{
        //    get
        //    {
        //        var res = GetResources(m_appSettings.GetValue<string>("Navigation"));
        //        return res;
        //    }
        //}
        public static List<NavigationView> GetNavigation(string key)
        {          
            var optionsBuilder = new DbContextOptionsBuilder<HRMDBContext>();
            var connectionString = _Config.GetConnectionString("HRMConnection");
            optionsBuilder
           .UseSqlServer(connectionString, providerOptions => providerOptions.CommandTimeout(60))
                   .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
            var _context = new HRMDBContext(optionsBuilder.Options);
            var result = new List<NavigationView>();
            var lstPrivilegeId = _context.ESUserPrivileges.Where(x => x.UserId == Current.User.Identity.Name).Select(x=>x.PrivilegeId).ToList();
            var lstResource = _context.Privilege.Where(x => lstPrivilegeId.Contains(x.Id) && x.ActionId == "ACCESS").Select(x => x.ResourceId).Distinct().ToList();
            var res = _context.Resource.OrderBy(x=>x.Ord).Where(x=> lstResource.Contains(x.Id)).ToList();
            foreach (var r in res)
            {
                var item = new NavigationView()
                {
                    Id = r.Id,
                    Title = r.Title,
                    Code = r.Code,
                    Ord = r.Ord,
                    ParentId = r.ParentId
                };
                try
                {
                    item.Icon = _context.Attribute.Where(x =>x.ResourceId == r.Id && x.Key.Equals("ICON")).First().Value;
                }
                catch (Exception)
                { }
                try
                {
                    item.Url = _context.Attribute.Where(x => x.ResourceId == r.Id && x.Key.Equals("URL")).First().Value;
                }
                catch (Exception)
                { }
                try
                {
                    item.HasChild = res.Any(x => x.ParentId.HasValue && x.ParentId.Value == item.Id);
                }
                catch (Exception)
                { }

                result.Add(item);
            }
            return result;
        }
        public class ObjPermiss
        {
            public string Action { get; set; }
            public string Permistion { get; set; }
        }
        public static ObjPermiss GetPermission()
        {
            var url = m_httpContextAccessor.HttpContext.Request.Path.Value;

            var obj = new ObjPermiss();
            var optionsBuilder = new DbContextOptionsBuilder<HRMDBContext>();
            var connectionString = _Config.GetConnectionString("HRMConnection");
            optionsBuilder
           .UseSqlServer(connectionString, providerOptions => providerOptions.CommandTimeout(60))
                   .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
            var _context = new HRMDBContext(optionsBuilder.Options);
            var ResourceId = _context.Attribute.Where(x => x.Key.Equals("URL") && x.Value == url).Select(x=>x.ResourceId).FirstOrDefault();

            var result = new List<NavigationView>();
            var lstPrivilegeId = _context.ESUserPrivileges.Where(x => x.UserId == Current.User.Identity.Name).Select(x => x.PrivilegeId).ToList();
            var lstActionId = _context.Privilege.Where(x => lstPrivilegeId.Contains(x.Id) && x.ResourceId == ResourceId).Select(x => x.ActionId).Distinct().ToList();
            var lstAction = ""; var lstPermiss = "";
            for (var i=0;i< lstActionId.Count;i++)
            {   if(i==(lstActionId.Count -1))
                {
                    lstPermiss += lstActionId[i];
                }
                else
                {
                    lstPermiss += lstActionId[i] + ",";
                }
                lstAction += "*[data-action = '" + lstActionId[i] + "']{ display: inline-block; }";
            }
            obj.Action = lstAction;
            obj.Permistion = lstPermiss;
            return obj;
        }
        public static List<NavigationView> Navigation
        {
            get
            {
                var result = new List<NavigationView>();
                var res = GetResources(m_appSettings.GetValue<string>("Navigation"));
                foreach (var r in res)
                {
                    var item = new NavigationView()
                    {
                        Id = r.Id,
                        Title = r.Title,
                        Code = r.Code,
                        Ord = r.Ord,
                        ParentId = r.ParentId
                    };
                    try
                    {
                        if(r.Attributes.Count >0)
                        {
                            item.Icon = r.Attributes.Where(x => x.Key.Equals("ICON")).First().Value;
                        }
                        else
                        {
                            item.Icon = "";
                        }
                    }
                    catch (Exception)
                    { }
                    try
                    {
                        if (r.Attributes.Count > 0)
                        {
                            item.Url = r.Attributes.Where(x => x.Key.Equals("URL")).First().Value;
                    }
                        else
                    {
                        item.Icon = "";
                    }
                }
                    catch (Exception)
                    { }
                    try
                    {
                        item.HasChild = res.Any(x => x.ParentId.HasValue && x.ParentId.Value == item.Id);
                    }
                    catch (Exception)
                    { }

                    result.Add(item);
                }

                return result;
            }
        }
        public static List<string> GetPermission(int resourceId)
        {

            try
            {                
                    var optionsBuilder = new DbContextOptionsBuilder<HRMDBContext>();
                    var connectionString = _Config.GetConnectionString("HRMConnection");
                    optionsBuilder
                   .UseSqlServer(connectionString, providerOptions => providerOptions.CommandTimeout(60))
                           .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
                    var _context = new HRMDBContext(optionsBuilder.Options);
                    var lstPrivilegeId = _context.ESUserPrivileges.Where(x => x.UserId == Current.User.Identity.Name).Select(x => x.PrivilegeId).ToList();
                    var lstActionId = _context.Privilege.Where(x => lstPrivilegeId.Contains(x.Id) && x.ResourceId == resourceId).Select(x => x.ActionId).Distinct().ToList();

                    return lstActionId;
            }
            catch (Exception ex)
            {
                return null;
            }

        }
        public static List<string> CheckPermission(string module)
        {
            var act = new List<string>();
            var nav = Navigation;
            var resources = nav.Where(x => x.Code.Equals(module));
            if (resources != null && resources.Count() > 0)
            {
                var res = resources.First();
                var optionsBuilder = new DbContextOptionsBuilder<HRMDBContext>();
                var connectionString = _Config.GetConnectionString("HRMConnection");
                optionsBuilder
               .UseSqlServer(connectionString, providerOptions => providerOptions.CommandTimeout(60))
                       .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
                var _context = new HRMDBContext(optionsBuilder.Options);
                var lstPrivilegeId = _context.ESUserPrivileges.Where(x => x.UserId == Current.User.Identity.Name).Select(x => x.PrivilegeId).ToList();
                var lstActionId = _context.Privilege.Where(x => lstPrivilegeId.Contains(x.Id) && x.ResourceId == res.Id).Select(x => x.ActionId).Distinct().ToList();

                foreach (var p in lstActionId)
                {
                    act.Add(p);
                }
            }
            return act;
        }
        //public static List<string> CheckPermission1(string module)
        //{
        //    var act = new List<string>();
        //    var nav = Navigation;
        //    var resources = nav.Where(x => x.Code.Equals(module));
        //    if (resources != null && resources.Count() > 0)
        //    {
        //        var res = resources.First();
        //        var perm = GetPermission(res.Id);
        //        foreach (var p in perm.Privileges)
        //        {
        //            act.Add(p.ActionId);
        //        }
        //    }
        //    return act;
        //}
        public static int CheckPermissionApp()
        {

            try
            {
                var url = m_httpContextAccessor.HttpContext.Request.Path.Value;

            var obj = new ObjPermiss();
            var optionsBuilder = new DbContextOptionsBuilder<HRMDBContext>();
            var connectionString = _Config.GetConnectionString("HRMConnection");
            optionsBuilder
           .UseSqlServer(connectionString, providerOptions => providerOptions.CommandTimeout(60))
                   .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
            var _context = new HRMDBContext(optionsBuilder.Options);
            var ResourceId = _context.Attribute.Where(x => x.Key.Equals("URL") && x.Value == url).Select(x=>x.ResourceId).FirstOrDefault();
                var data = _context.Resource.Where(x => x.Id == ResourceId).ToList();
                if (data.Count == 0)
                {
                    return 0;
                }
                else if (data.Count == 1)
                {
                   return 2; 
                }
                return 0;
            }
            catch (Exception ex)
            {
                return -1;
            }

        }
        //----------------------------------------
        private static T GetHeaderValueAs<T>(string headerName)
        {
            StringValues values;

            if (m_httpContextAccessor.HttpContext?.Request?.Headers?.TryGetValue(headerName, out values) ?? false)
            {
                string rawValues = values.ToString();

                if (!string.IsNullOrEmpty(rawValues))
                    return (T)Convert.ChangeType(values.ToString(), typeof(T));
            }
            return default(T);
        }
        private static List<string> SplitCsv(this string csvList, bool nullOrWhitespaceInputReturnsNull = false)
        {
            if (string.IsNullOrWhiteSpace(csvList))
                return nullOrWhitespaceInputReturnsNull ? null : new List<string>();

            return csvList
                .TrimEnd(',')
                .Split(',')
                .AsEnumerable<string>()
                .Select(s => s.Trim())
                .ToList();
        }
        private static bool IsNullOrWhitespace(this string s)
        {
            return String.IsNullOrWhiteSpace(s);
        }
        public static string GuidEmployee
        {
            get
            {
                return GetClaim("sub");
            }
        }
    }


}