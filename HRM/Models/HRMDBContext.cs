﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations.Schema;
using HRM.Models.Custom.Hr;
using HRM.Utils;
using ES_MODEL.Task;
using ES_MODEL.Task.Custom;

namespace HRM.Models
{
    public partial class HRMDBContext : DbContext
    {
        public HRMDBContext(DbContextOptions<HRMDBContext> options) : base(options)
        {

        }
        // Cấu hình entity Custom
        public virtual DbSet<AnnouncementsJtableView> AnnouncementsJtableView { get; set; }
        public virtual DbSet<RatingTasksView> RatingTasksView { get; set; }
        public virtual DbSet<RatingTasks_BieuDo> RatingTasks_BieuDo { get; set; }


        public virtual DbSet<ListEmpAnnoucementGetByid> ListEmpAnnoucementGetByid { get; set; }
        public virtual DbSet<CustomerView> CustomerView { get; set; }
        public virtual DbSet<GroupsView> GroupsView { get; set; }
        public virtual DbSet<ProductionMaterialsViews> ProductionMaterialsViews { get; set; }
        public virtual DbSet<ProductDeparmentsViews> ProductDeparmentsViews { get; set; }
        public virtual DbSet<AccountantsViews> AccountantsViews { get; set; }
        public virtual DbSet<AccountantsOthersViews> AccountantsOthersViews { get; set; }
        public virtual DbSet<AccountantsManagementViews> AccountantsManagementViews { get; set; }
        public virtual DbSet<AccountantsDebtViews> AccountantsDebtViews { get; set; }



        //--------------------------------------Attachments -----------------------------------------------------
        public virtual DbSet<OrderAttachmentsDownloadView> AttachmentsDownloadView { get; set; }
        public virtual DbSet<OrderAttachmentsView> AttachmentsView { get; set; }
        public virtual DbSet<AttachmentsView> OpenAttachmentsView { get; set; }
        public virtual DbSet<SanPhamViews> SanPhamViews { get; set; }
        public virtual DbSet<TaskLabelCustoms> TaskLabelCustoms { get; set; }
        public virtual DbSet<TaskAttachmentViews> TaskAttachmentViews { get; set; }
        public virtual DbSet<CheckPermission> CheckPermissions { get; set; }
        public virtual DbSet<ActivitiesCustom> ActivitiesCustoms { get; set; }
        public virtual DbSet<GetTaskComment> GetTaskComments { get; set; }
        public virtual DbSet<TaskAttachbyType> TaskAttachbyTypes { get; set; }
        public virtual DbSet<LoadCheckList> LoadCheckLists { get; set; }
        public virtual DbSet<ViewJtableLables> ViewJtableLable { get; set; }


        public virtual DbSet<GetTasks> GetTasks { get; set; }
        public virtual DbSet<GetItem> GetItems { get; set; }
        public virtual DbSet<ES_MODEL.HR.Custom.WorkFlowStatusJtableView> WorkFlowStatusJtableView { get; set; }
        public virtual DbSet<ES_MODEL.HR.Custom.Hr.WorkFlowSettingJTableView> WorkFlowSettingJTableView { get; set; }
        public virtual DbSet<ProjectManageResultView2> ProjectManageResultView2 { get; set; }
        public virtual DbSet<ProjectManageResultView3> ProjectManageResultView3 { get; set; }

        public virtual DbSet<ProjectManageJtableView> ProjectManageJtableView { get; set; }
        public virtual DbSet<DashboardView1> DashboardView1 { get; set; }
        public virtual DbSet<CountDocuments> CountDocuments { get; set; }


        // báo cáo phòng ban
        public virtual DbSet<ProjectManageView1> ProjectManageView1 { get; set; }
        public virtual DbSet<ProjectManageView3> ProjectManageView3 { get; set; }
        public virtual DbSet<ProjectManageView4> ProjectManageView4 { get; set; }
        public virtual DbSet<ProjectManageView5> ProjectManageView5 { get; set; }
        public virtual DbSet<ProjectManageView6> ProjectManageView6 { get; set; }
        // view báo cáo kết quả
        public virtual DbSet<ProjectReportView1> ProjectReportView1 { get; set; }
        public virtual DbSet<ProjectReportView2> ProjectReportView2 { get; set; }
        public virtual DbSet<ProjectReportView3> ProjectReportView3 { get; set; }
        public virtual DbSet<ProjectReportView5> ProjectReportView5 { get; set; }
        public virtual DbSet<ProjectReportView6> ProjectReportView6 { get; set; }
        // xuất Excel báo cáo kq
        public virtual DbSet<ReportViewSheet1> ReportViewSheet1 { get; set; }
        public virtual DbSet<ReportViewSheet2> ReportViewSheet2 { get; set; }
        public virtual DbSet<ReportViewSheet3> ReportViewSheet3 { get; set; }
        public virtual DbSet<ReportViewSheet4> ReportViewSheet4 { get; set; }

        public virtual DbSet<DepartmentsJtableView> DepartmentsJtableViews { get; set; }
        public virtual DbSet<DepartmentsTreeview> DepartmentsTreeview { get; set; }
        public virtual DbSet<DepartmentsViewItem> DepartmentsViewItems { get; set; }
        public virtual DbSet<SQLCOMMAND> SQLCOMMANDS { get; set; }
        public virtual DbSet<PhotoView> PhotoView { get; set; }
        public virtual DbSet<EmployeesJtableView> EmployeesJtableView { get; set; }
        public virtual DbSet<EmployeesViewLogin> EmployeesViewLogin { get; set; }
        public virtual DbSet<Object_Jexcel> Object_Jexcel { get; set; }
        public virtual DbSet<Object_Combobox> Object_Combobox { get; set; }
        public virtual DbSet<ObjectNew_Combobox> ObjectNew_Combobox { get; set; }
        public virtual DbSet<EmployeesView> EmployeesView { get; set; }
        public virtual DbSet<EmployeesExport> EmployeesExport { get; set; }
        public virtual DbSet<Auto> Auto { get; set; }
        public virtual DbSet<ProductionManagerViews> ProductionManagerViews { get; set; }
        public virtual DbSet<HRM.Models.SQLCOMMANDMessage> SQLCOMMANDMessage { get; set; }
        public virtual DbSet<HRM.Models.SQLConvertLoginName> SQLConvertLoginName { get; set; }
        public virtual DbSet<HRM.Models.Custom.Hr.Object_Table> Object_Table { get; set; }
        public virtual DbSet<HRM.Models.Custom.Hr.Object_Table5> Object_Table5 { get; set; }

        public virtual DbSet<OrderAttachmentsView> OrderAttachmentsView { get; set; }
        public virtual DbSet<AttachmentsDownloadView> AttachmentsDownloadView_ { get; set; }
        public virtual DbSet<CommentView> CommentView { get; set; }
        // Cấu hình entity gốc 

        //Bán hàng
        public virtual DbSet<ProductionManager> ProductionManager { get; set; }
        public virtual DbSet<ProductionManagerDetails> ProductionManagerDetails { get; set; }


        public virtual DbSet<ProductionManagerComments> ProductionManagerComments { get; set; }
        public virtual DbSet<ProductionManagerProcess> ProductionManagerProcess { get; set; }
        public virtual DbSet<ProductionManagerComments_DOC> ProductionManagerComments_DOC { get; set; }
        public virtual DbSet<Announcements> Announcements { get; set; }
        public virtual DbSet<AnnouncementAttachmentComments> AnnouncementAttachmentComments { get; set; }

        public virtual DbSet<ProductDeparments> ProductDeparments { get; set; }
        public virtual DbSet<ProductDeparments_DOC> ProductDeparments_DOC { get; set; }
        public virtual DbSet<ProductDeparmentsComments> ProductDeparmentsComments { get; set; }
        public virtual DbSet<ProductDeparmentsComments_DOC> ProductDeparmentsComments_DOC { get; set; }
        public virtual DbSet<ProductDeparmentDetails> ProductDeparmentDetails { get; set; }

        public virtual DbSet<Accountants> Accountants { get; set; }
        public virtual DbSet<Accountants_DOC> Accountants_DOC { get; set; }
        public virtual DbSet<AccountantsComments> AccountantsComments { get; set; }
        public virtual DbSet<AccountantsComments_DOC> AccountantsComments_DOC { get; set; }
        public virtual DbSet<AccountantsDetails> AccountantsDetails { get; set; }


        public virtual DbSet<ProductMaterialDetails> ProductMaterialDetails { get; set; }
        public virtual DbSet<ProductLogisticDetails> ProductLogisticDetails { get; set; }
        public virtual DbSet<ProductKSCDetails> ProductKSCDetails { get; set; }

        public virtual DbSet<PaymentOrders> PaymentOrders { get; set; }
        public virtual DbSet<ExportDocuments> ExportDocuments { get; set; }


        // Tast
        public virtual DbSet<TaskActivities> TaskActivities { get; set; }
        public virtual DbSet<TaskAttachments> TaskAttachments { get; set; }
        public virtual DbSet<TaskCheckLists> TaskCheckLists { get; set; }
        public virtual DbSet<TaskCommentAttachments> TaskCommentAttachments { get; set; }
        public virtual DbSet<TaskComments> TaskComments { get; set; }
        public virtual DbSet<TaskOfUsers> TaskOfUsers { get; set; }
        public virtual DbSet<TaskOptions> TaskOptions { get; set; }
        public virtual DbSet<TaskPermissions> TaskPermissions { get; set; }

        public virtual DbSet<Customers> Customers { get; set; }
        public virtual DbSet<Tasks> Tasks { get; set; }
        public virtual DbSet<TasksVersions> TasksVersions { get; set; }
        public virtual DbSet<Lables> Lables { get; set; }
        public virtual DbSet<CategoryOfTasks> CategoryOfTasks { get; set; }
        public virtual DbSet<TaskOfUserOfTaskSteps> TaskOfUserOfTaskSteps { get; set; }
        public virtual DbSet<TaskProcess> TaskProcess { get; set; }
        public virtual DbSet<TaskOfLabels> TaskOfLabels { get; set; }
        public virtual DbSet<CategoryOfProcess> CategoryOfProcess { get; set; }
        public virtual DbSet<CalendarAttachments> CalendarAttachments { get; set; }
        public virtual DbSet<CalendarComments> CalendarComments { get; set; }
        public virtual DbSet<CalendarOfUsers> CalendarOfUsers { get; set; }
        public virtual DbSet<Calendars> Calendars { get; set; }
        public virtual DbSet<CategoryShare> CategoryShare { get; set; }
        public virtual DbSet<TaskStepBySteps> TaskStepBySteps { get; set; }
        public virtual DbSet<TaskStepStatus> TaskStepStatus { get; set; }
        public virtual DbSet<Employees> Employees { get; set; }
        public virtual DbSet<Contracts> Contracts { get; set; }
        public virtual DbSet<Organizations> Organizations { get; set; }
        public virtual DbSet<Departments> Departments { get; set; }
        public virtual DbSet<ShiftOfEmployees> ShiftOfEmployees { get; set; }
        public virtual DbSet<Shifts> Shifts { get; set; }
        public virtual DbSet<Rank> Rank { get; set; }
        public virtual DbSet<Specializes> Specializes { get; set; }
        public virtual DbSet<Candidates> Candidates { get; set; }
        public virtual DbSet<Educations> Educations { get; set; }
        public virtual DbSet<JobTitles> JobTitles { get; set; }
        public virtual DbSet<Profestionals> Profestionals { get; set; }
        public virtual DbSet<Concurrently> Concurrently { get; set; }
        public virtual DbSet<Workflows> Workflows { get; set; }
        public virtual DbSet<WorkFlowSetting> WorkFlowSetting { get; set; }
        public virtual DbSet<WorkFlowStatus> WorkFlowStatus { get; set; }
        public virtual DbSet<Conditions> Conditions { get; set; }
        public virtual DbSet<Activities> Activities { get; set; }
        public virtual DbSet<CategoryColumns> CategoryColumns { get; set; }
        public virtual DbSet<CategoryTables> CategoryTables { get; set; }
        public virtual DbSet<CashFlows> CashFlows { get; set; }
        public virtual DbSet<CashFlowsTotals> CashFlowsTotals { get; set; }
        public virtual DbSet<ProductDeparmentsProcess> ProductDeparmentsProcess { get; set; }


        public virtual DbSet<Units> Units { get; set; }
        public virtual DbSet<Items> Items { get; set; }
        public virtual DbSet<ProductionMaterials> ProductionMaterials { get; set; }
        public virtual DbSet<ProductionMaterialDetails> ProductionMaterialDetails { get; set; }
        public virtual DbSet<Logistics> Logistics { get; set; }
        public virtual DbSet<LogisticsDetails> LogisticsDetails { get; set; }
        public virtual DbSet<AccountantsDebt> AccountantsDebt { get; set; }
        public virtual DbSet<AccountantsOthers> AccountantsOthers { get; set; }
        public virtual DbSet<AccountantsManagement> AccountantsManagement { get; set; }


        // Cấu hình entity EIM   
        public virtual DbSet<Resource> Resource { get; set; }
        public virtual DbSet<Attribute> Attribute { get; set; }
        public virtual DbSet<Privilege> Privilege { get; set; }
        public virtual DbSet<ESUserPrivileges> ESUserPrivileges { get; set; }
        public virtual DbSet<ESActions> ESActions { get; set; }
        public virtual DbSet<ListEmpAnnouncement> ListEmpAnnouncement { get; set; }



        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Customers>(entity =>
            {
                entity.HasKey(e => e.CustomerGuid);

                entity.ToTable("Customers", "Sale");

                entity.Property(e => e.CustomerGuid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<ProductDeparmentsProcess>(entity =>
            {
                entity.HasKey(e => e.RowGuid);

                entity.ToTable("ProductDeparmentsProcess", "Sale");

                entity.Property(e => e.RowGuid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<CashFlows>(entity =>
            {
                entity.HasKey(e => e.RowGuid);

                entity.ToTable("CashFlows", "Sale");

                entity.Property(e => e.RowGuid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<ProductionMaterials>(entity =>
            {
                entity.HasKey(e => e.RowGuid);

                entity.ToTable("ProductionMaterials", "Sale");

                entity.Property(e => e.RowGuid).HasDefaultValueSql("(newid())");
            });
            modelBuilder.Entity<ProductionManagerDetails>(entity =>
            {
                entity.HasKey(e => e.RowGuid);

                entity.ToTable("ProductionManagerDetails", "Sale");

                entity.Property(e => e.RowGuid).HasDefaultValueSql("(newid())");
            });
            modelBuilder.Entity<ProductionMaterialDetails>(entity =>
            {
                entity.HasKey(e => e.RowGuid);

                entity.ToTable("ProductionMaterialDetails", "Sale");

                entity.Property(e => e.RowGuid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<ProductDeparments>(entity =>
            {
                entity.HasKey(e => e.RowGuid);

                entity.ToTable("ProductDeparments", "Sale");

                entity.Property(e => e.RowGuid).HasDefaultValueSql("(newid())");
            });
            modelBuilder.Entity<Accountants>(entity =>
            {
                entity.HasKey(e => e.RowGuid);

                entity.ToTable("Accountants", "Sale");

                entity.Property(e => e.RowGuid).HasDefaultValueSql("(newid())");
            });
            modelBuilder.Entity<AccountantsDetails>(entity =>
            {
                entity.HasKey(e => e.RowGuid);

                entity.ToTable("AccountantsDetails", "Sale");

                entity.Property(e => e.RowGuid).HasDefaultValueSql("(newid())");
            });
            modelBuilder.Entity<Accountants_DOC>(entity =>
            {
                entity.HasKey(e => e.AttachmentGuid);

                entity.ToTable("Accountants_DOC", "Sale");

                entity.Property(e => e.AttachmentGuid).HasDefaultValueSql("(newid())");
            });
            modelBuilder.Entity<AccountantsComments>(entity =>
            {
                entity.HasKey(e => e.CommentGuid);

                entity.ToTable("AccountantsComments", "Sale");

                entity.Property(e => e.CommentGuid).HasDefaultValueSql("(newid())");
            });
            modelBuilder.Entity<AccountantsComments_DOC>(entity =>
            {
                entity.HasKey(e => e.AttachmentGuid);

                entity.ToTable("AccountantsComments_DOC", "Sale");

                entity.Property(e => e.AttachmentGuid).HasDefaultValueSql("(newid())");
            });
            modelBuilder.Entity<PaymentOrders>(entity =>
            {
                entity.HasKey(e => e.RowGuid);

                entity.ToTable("PaymentOrders", "Sale");

                entity.Property(e => e.RowGuid).HasDefaultValueSql("(newid())");
            });
            modelBuilder.Entity<ExportDocuments>(entity =>
            {
                entity.HasKey(e => e.RowGuid);

                entity.ToTable("ExportDocuments", "Sale");

                entity.Property(e => e.RowGuid).HasDefaultValueSql("(newid())");
            });
            modelBuilder.Entity<ProductDeparmentDetails>(entity =>
            {
                entity.HasKey(e => e.RowGuid);

                entity.ToTable("ProductDeparmentDetails", "Sale");

                entity.Property(e => e.RowGuid).HasDefaultValueSql("(newid())");
            });
            modelBuilder.Entity<ProductMaterialDetails>(entity =>
            {
                entity.HasKey(e => e.RowGuid);

                entity.ToTable("ProductMaterialDetails", "Sale");

                entity.Property(e => e.RowGuid).HasDefaultValueSql("(newid())");
            });
            modelBuilder.Entity<ProductLogisticDetails>(entity =>
            {
                entity.HasKey(e => e.RowGuid);

                entity.ToTable("ProductLogisticDetails", "Sale");

                entity.Property(e => e.RowGuid).HasDefaultValueSql("(newid())");
            });
            modelBuilder.Entity<ProductKSCDetails>(entity =>
            {
                entity.HasKey(e => e.RowGuid);

                entity.ToTable("ProductKSCDetails", "Sale");

                entity.Property(e => e.RowGuid).HasDefaultValueSql("(newid())");
            });
            modelBuilder.Entity<ProductDeparments_DOC>(entity =>
            {
                entity.HasKey(e => e.AttachmentGuid);

                entity.ToTable("ProductDeparments_DOC", "Sale");

                entity.Property(e => e.AttachmentGuid).HasDefaultValueSql("(newid())");
            });
            modelBuilder.Entity<ProductDeparmentsComments>(entity =>
            {
                entity.HasKey(e => e.CommentGuid);

                entity.ToTable("ProductDeparmentsComments", "Sale");

                entity.Property(e => e.CommentGuid).HasDefaultValueSql("(newid())");
            });
            modelBuilder.Entity<ProductDeparmentsComments_DOC>(entity =>
            {
                entity.HasKey(e => e.AttachmentGuid);

                entity.ToTable("ProductDeparmentsComments_DOC", "Sale");

                entity.Property(e => e.AttachmentGuid).HasDefaultValueSql("(newid())");
            });



            modelBuilder.Entity<Logistics>(entity =>
            {
                entity.HasKey(e => e.RowGuid);

                entity.ToTable("Logistics", "Sale");

                entity.Property(e => e.RowGuid).HasDefaultValueSql("(newid())");
            });
            modelBuilder.Entity<AccountantsManagement>(entity =>
            {
                entity.HasKey(e => e.RowGuid);

                entity.ToTable("AccountantsManagement", "Sale");

                entity.Property(e => e.RowGuid).HasDefaultValueSql("(newid())");
            });
            modelBuilder.Entity<AccountantsOthers>(entity =>
            {
                entity.HasKey(e => e.RowGuid);

                entity.ToTable("AccountantsOthers", "Sale");

                entity.Property(e => e.RowGuid).HasDefaultValueSql("(newid())");
            });
            modelBuilder.Entity<AccountantsDebt>(entity =>
            {
                entity.HasKey(e => e.RowGuid);

                entity.ToTable("AccountantsDebt", "Sale");

                entity.Property(e => e.RowGuid).HasDefaultValueSql("(newid())");
            });
            modelBuilder.Entity<LogisticsDetails>(entity =>
            {
                entity.HasKey(e => e.RowGuid);

                entity.ToTable("LogisticsDetails", "Sale");

                entity.Property(e => e.RowGuid).HasDefaultValueSql("(newid())");
            });


            modelBuilder.Entity<Items>(entity =>
            {
                entity.HasKey(e => e.RowGuid);

                entity.ToTable("Items", "Sale");

                entity.Property(e => e.RowGuid).HasDefaultValueSql("(newid())");
            });
            modelBuilder.Entity<Units>(entity =>
            {
                entity.HasKey(e => e.UnitGuid);

                entity.ToTable("Units", "Common");

                entity.Property(e => e.UnitGuid).HasDefaultValueSql("(newid())");
            });
            modelBuilder.Entity<TaskStepStatus>(entity =>
            {
                entity.HasKey(e => e.RecordGuid);

                entity.ToTable("TaskStepStatus", "Cooperation");

                entity.Property(e => e.RecordGuid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Code).HasMaxLength(250);

                entity.Property(e => e.ColorBackground)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ColorText)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Name).HasMaxLength(250);

                entity.HasOne(d => d.TaskStepByStepGu)
                    .WithMany(p => p.TaskStepStatus)
                    .HasForeignKey(d => d.TaskStepByStepGuid)
                    .HasConstraintName("FK_TaskStepStatus_TaskStepBySteps");
            });
            modelBuilder.Entity<TaskOptions>(entity =>
            {
                entity.HasKey(e => e.TaskOptionGuid);

                entity.ToTable("TaskOptions", "Cooperation");

                entity.Property(e => e.TaskOptionGuid).ValueGeneratedNever();

                entity.Property(e => e.CategoryTeamId)
                    .HasColumnName("CategoryTeamID")
                    .HasMaxLength(50);

                entity.Property(e => e.HoursWorked).HasColumnType("smallmoney");

                entity.Property(e => e.OrderNumber).HasMaxLength(50);

                entity.Property(e => e.PlanStartDate).HasColumnType("date");

                entity.Property(e => e.ProductCode).HasMaxLength(50);

                entity.Property(e => e.ProductName).HasMaxLength(255);

                entity.Property(e => e.ProjectItemId).HasColumnName("ProjectItemID");

                entity.Property(e => e.QuotationNo).HasMaxLength(50);

                entity.Property(e => e.RequestTiketDetailId).HasColumnName("RequestTiketDetailID");

                entity.Property(e => e.TaskRequestDetailId).HasColumnName("TaskRequestDetailID");

                entity.Property(e => e.TaskRequestNo).HasMaxLength(50);

                entity.Property(e => e.WarrantyCost).HasColumnType("money");

                entity.Property(e => e.WarrantyTicketNo).HasMaxLength(50);

                entity.HasOne(d => d.TaskGu)
                    .WithMany(p => p.TaskOptions)
                    .HasForeignKey(d => d.TaskGuid)
                    .HasConstraintName("FK_TaskOption_Tasks");
            });

            modelBuilder.Entity<TaskPermissions>(entity =>
            {
                entity.HasKey(e => e.PermissionGuid);

                entity.ToTable("TaskPermissions", "Cooperation");

                entity.Property(e => e.PermissionGuid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Alias).HasMaxLength(50);

                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Description).HasMaxLength(255);

                entity.Property(e => e.LocationId).HasColumnName("LocationID");

                entity.Property(e => e.ModifiedBy).HasMaxLength(50);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.PermissionId)
                    .IsRequired()
                    .HasColumnName("PermissionID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PermissionName).HasMaxLength(255);
            });

            modelBuilder.Entity<Tasks>(entity =>
            {
                entity.HasKey(e => e.TaskGuid);

                entity.ToTable("Tasks", "Cooperation");

                entity.Property(e => e.TaskGuid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Approval).HasMaxLength(250);

                entity.Property(e => e.ApprovalStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.CompleteDate).HasColumnType("datetime");

                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.CustomerId)
                    .HasColumnName("CustomerID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DepartmentGuid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.DepartmentId)
                    .HasColumnName("DepartmentID")
                    .HasMaxLength(50);

                entity.Property(e => e.DepartmentName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Description).HasMaxLength(255);

                entity.Property(e => e.EmployeeId)
                    .HasColumnName("EmployeeID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.EmployeeName).HasMaxLength(50);

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.IsApproval)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('N')");

                entity.Property(e => e.Kpiscore).HasColumnName("KPIScore");

                entity.Property(e => e.LoginName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedBy).HasMaxLength(50);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Note).HasMaxLength(255);

                entity.Property(e => e.ParentId).HasColumnName("ParentID");

                entity.Property(e => e.PercentComplete).HasDefaultValueSql("((0))");

                entity.Property(e => e.Priority)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('N')");

                entity.Property(e => e.ProjectId)
                    .HasColumnName("ProjectID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.SecurityLevel)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('N')");

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.Property(e => e.StatusCompleted)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Subject)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.TaskContent).HasColumnType("ntext");

                entity.Property(e => e.TaskLevel)
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.TaskNumber)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.TimeReminder).HasColumnType("datetime");

                entity.HasOne(d => d.CategoryOfTaskGu)
                    .WithMany(p => p.Tasks)
                    .HasForeignKey(d => d.CategoryOfTaskGuid)
                    .HasConstraintName("FK_Tasks_CategoryOfTasks");

                entity.HasOne(d => d.Parent)
                    .WithMany(p => p.InverseParent)
                    .HasForeignKey(d => d.ParentId)
                    .HasConstraintName("FK_Tasks_Tasks");
            });

            modelBuilder.Entity<TasksVersions>(entity =>
            {
                entity.HasKey(e => e.TaskVersionId);

                entity.ToTable("TasksVersions", "Cooperation");

                entity.Property(e => e.TaskVersionId).HasColumnName("TaskVersionID");

                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Description).IsRequired();

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).HasMaxLength(50);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.TaskGuid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.TaskVersionGuid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Title).HasMaxLength(50);
            });
            modelBuilder.Entity<TaskProcess>(entity =>
            {
                entity.HasKey(e => e.RowGuid);

                entity.ToTable("TaskProcess", "Cooperation");

                entity.Property(e => e.RowGuid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Class).HasMaxLength(250);

                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.ModifiedBy).HasMaxLength(50);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Title).HasMaxLength(250);
            });
            modelBuilder.Entity<TaskOfProcess>(entity =>
            {
                entity.HasKey(e => e.RowGuid);

                entity.ToTable("TaskOfProcess", "Cooperation");

                entity.Property(e => e.RowGuid).HasDefaultValueSql("(newid())");

                entity.HasOne(d => d.TaskGu)
                    .WithMany(p => p.TaskOfProcess)
                    .HasForeignKey(d => d.TaskGuid)
                    .HasConstraintName("FK_TaskOfProcess_Tasks");

                entity.HasOne(d => d.TaskProcessGu)
                    .WithMany(p => p.TaskOfProcess)
                    .HasForeignKey(d => d.TaskProcessGuid)
                    .HasConstraintName("FK_TaskOfProcess_TaskProcess");
            });
            modelBuilder.Entity<CategoryOfProcess>(entity =>
            {
                entity.HasKey(e => e.RowGuid);

                entity.ToTable("CategoryOfProcess", "Cooperation");

                entity.Property(e => e.RowGuid).HasDefaultValueSql("(newid())");

                entity.HasOne(d => d.CategoryOfTaskGu)
                    .WithMany(p => p.CategoryOfProcess)
                    .HasForeignKey(d => d.CategoryOfTaskGuid)
                    .HasConstraintName("FK_CategoryOfProcess_CategoryOfTasks");

                entity.HasOne(d => d.TaskProcessGu)
                    .WithMany(p => p.CategoryOfProcess)
                    .HasForeignKey(d => d.TaskProcessGuid)
                    .HasConstraintName("FK_CategoryOfProcess_TaskProcess");
            });

            modelBuilder.Entity<Recurrence>(entity =>
            {
                entity.HasKey(e => e.RowGuid);

                entity.ToTable("Recurrence", "Cooperation");

                entity.Property(e => e.RowGuid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.IsRender).HasColumnType("datetime");

                entity.Property(e => e.LoginName).HasMaxLength(50);

                entity.Property(e => e.ModifiedBy).HasMaxLength(50);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Name).HasMaxLength(250);

                entity.Property(e => e.OptionMore).HasMaxLength(50);

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.Property(e => e.Type).HasMaxLength(50);
            });

            modelBuilder.Entity<RecurrenceConfigs>(entity =>
            {
                entity.HasKey(e => e.RowGuid);

                entity.ToTable("RecurrenceConfigs", "Cooperation");

                entity.Property(e => e.RowGuid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Option).HasMaxLength(20);

                entity.Property(e => e.Postion).HasColumnType("datetime");

                entity.Property(e => e.WeekChoice).HasMaxLength(25);

                entity.HasOne(d => d.RecurrenceGu)
                    .WithMany(p => p.RecurrenceConfigs)
                    .HasForeignKey(d => d.RecurrenceGuid)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_RecurrenceConfigs_Recurrence");
            });

            modelBuilder.Entity<CategoryOfProcess>(entity =>
            {
                entity.HasKey(e => e.RowGuid);

                entity.ToTable("CategoryOfProcess", "Cooperation");

                entity.Property(e => e.RowGuid).HasDefaultValueSql("(newid())");

                entity.HasOne(d => d.CategoryOfTaskGu)
                    .WithMany(p => p.CategoryOfProcess)
                    .HasForeignKey(d => d.CategoryOfTaskGuid)
                    .HasConstraintName("FK_CategoryOfProcess_CategoryOfTasks");

                entity.HasOne(d => d.TaskProcessGu)
                    .WithMany(p => p.CategoryOfProcess)
                    .HasForeignKey(d => d.TaskProcessGuid)
                    .HasConstraintName("FK_CategoryOfProcess_TaskProcess");
            });
            modelBuilder.Entity<TaskOfLabels>(entity =>
            {
                entity.HasKey(e => e.RowGuid);

                entity.ToTable("TaskOfLabels", "Cooperation");

                entity.Property(e => e.RowGuid).HasDefaultValueSql("(newid())");

                entity.HasOne(d => d.Label)
                    .WithMany(p => p.TaskOfLabels)
                    .HasForeignKey(d => d.LabelId)
                    .HasConstraintName("FK_TaskOfLabel_Lables");

                entity.HasOne(d => d.TaskGu)
                    .WithMany(p => p.TaskOfLabels)
                    .HasForeignKey(d => d.TaskGuid)
                    .HasConstraintName("FK_TaskOfLabel_Tasks");
            });
            modelBuilder.Entity<TaskPermissions>(entity =>
            {
                entity.HasKey(e => e.PermissionGuid);

                entity.ToTable("TaskPermissions", "Cooperation");

                entity.Property(e => e.PermissionGuid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Alias).HasMaxLength(50);

                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Description).HasMaxLength(255);

                entity.Property(e => e.LocationId).HasColumnName("LocationID");

                entity.Property(e => e.ModifiedBy).HasMaxLength(50);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.PermissionId)
                    .IsRequired()
                    .HasColumnName("PermissionID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PermissionName).HasMaxLength(255);
            });

            modelBuilder.Entity<TaskOptions>(entity =>
            {
                entity.HasKey(e => e.TaskOptionGuid);

                entity.ToTable("TaskOptions", "Cooperation");

                entity.Property(e => e.TaskOptionGuid).ValueGeneratedNever();

                entity.Property(e => e.CategoryTeamId)
                    .HasColumnName("CategoryTeamID")
                    .HasMaxLength(50);

                entity.Property(e => e.HoursWorked).HasColumnType("smallmoney");

                entity.Property(e => e.OrderNumber).HasMaxLength(50);

                entity.Property(e => e.PlanStartDate).HasColumnType("date");

                entity.Property(e => e.ProductCode).HasMaxLength(50);

                entity.Property(e => e.ProductName).HasMaxLength(255);

                entity.Property(e => e.ProjectItemId).HasColumnName("ProjectItemID");

                entity.Property(e => e.QuotationNo).HasMaxLength(50);

                entity.Property(e => e.RequestTiketDetailId).HasColumnName("RequestTiketDetailID");

                entity.Property(e => e.TaskRequestDetailId).HasColumnName("TaskRequestDetailID");

                entity.Property(e => e.TaskRequestNo).HasMaxLength(50);

                entity.Property(e => e.WarrantyCost).HasColumnType("money");

                entity.Property(e => e.WarrantyTicketNo).HasMaxLength(50);

                entity.HasOne(d => d.TaskGu)
                    .WithMany(p => p.TaskOptions)
                    .HasForeignKey(d => d.TaskGuid)
                    .HasConstraintName("FK_TaskOption_Tasks");
            });
            modelBuilder.Entity<TaskOfUsers>(entity =>
            {
                entity.HasKey(e => e.TaskOfUserGuid);

                entity.ToTable("TaskOfUsers", "Cooperation");

                entity.Property(e => e.TaskOfUserGuid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.ActualEndDate).HasColumnType("datetime");

                entity.Property(e => e.ActualStartDate).HasColumnType("datetime");

                entity.Property(e => e.CompleteDate).HasColumnType("datetime");

                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.DepartmentGuid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.DepartmentId)
                    .HasColumnName("DepartmentID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.DepartmentName).HasMaxLength(500);

                entity.Property(e => e.DirectLink).HasMaxLength(255);

                entity.Property(e => e.EmployeeGuid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.EmployeeId)
                    .HasColumnName("EmployeeID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.EmployeeName).HasMaxLength(50);

                entity.Property(e => e.IsAcceptTask)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsCompleted)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IsLeader)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsOwner)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsRecallAccept)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Kpiscore).HasColumnName("KPIScore");

                entity.Property(e => e.LoginName).HasMaxLength(255);

                entity.Property(e => e.ModifiedBy).HasMaxLength(50);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Note).HasMaxLength(255);

                entity.Property(e => e.PermissionId).HasColumnName("PermissionID");

                entity.Property(e => e.ProcessAccept).HasMaxLength(5);

                entity.Property(e => e.ReadDate).HasColumnType("datetime");

                entity.Property(e => e.StatusCompleted)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.TaskLevel)
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.TimeReminder).HasColumnType("datetime");

                entity.HasOne(d => d.Permission)
                    .WithMany(p => p.TaskOfUsers)
                    .HasForeignKey(d => d.PermissionId)
                    .HasConstraintName("FK_TaskOfUsers_TaskPermissions");

                entity.HasOne(d => d.TaskGu)
                    .WithMany(p => p.TaskOfUsers)
                    .HasForeignKey(d => d.TaskGuid)
                    .HasConstraintName("FK_TaskOfUsers_Tasks");
            });
            modelBuilder.Entity<TaskActivities>(entity =>
            {
                entity.HasKey(e => e.TaskActivityGuid);

                entity.ToTable("TaskActivities", "Cooperation");

                entity.Property(e => e.TaskActivityGuid).ValueGeneratedNever();

                entity.Property(e => e.ActivityContent).IsRequired();

                entity.Property(e => e.ActivityId)
                    .IsRequired()
                    .HasColumnName("ActivityID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.EmployeeId)
                    .IsRequired()
                    .HasColumnName("EmployeeID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.EmployeeName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.ModifiedBy).HasMaxLength(50);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Subject)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.TaskOfUserGuid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.HasOne(d => d.Tasks)
                    .WithMany(p => p.TaskActivities)
                    .HasForeignKey(d => d.TaskGuid)
                    .HasConstraintName("FK_TaskActivities_Tasks");
            });

            modelBuilder.Entity<TaskAttachments>(entity =>
            {
                entity.HasKey(e => e.AttachmentGuid);

                entity.ToTable("TaskAttachments", "Cooperation");

                entity.Property(e => e.AttachmentGuid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Attachment).IsRequired();

                entity.Property(e => e.CloudPath).HasMaxLength(500);

                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.FileExtension)
                    .IsRequired()
                    .HasMaxLength(8);

                entity.Property(e => e.FileName)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.ModifiedBy).HasMaxLength(50);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.ModuleId)
                    .HasColumnName("ModuleID")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.PrivateLevel)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.RecordGuid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.SyncTime).HasColumnType("datetime");

                entity.Property(e => e.Title).HasMaxLength(255);

                entity.Property(e => e.UrlExternal).HasMaxLength(250);

                entity.HasOne(d => d.TaskGu)
                   .WithMany(p => p.TaskAttachments)
                   .HasForeignKey(d => d.RecordGuid)
                   .HasConstraintName("FK_TaskOption_TaskAttachments");
            });

            modelBuilder.Entity<TaskCheckLists>(entity =>
            {
                entity.HasKey(e => e.CheckListGuid);

                entity.ToTable("TaskCheckLists", "Cooperation");

                entity.Property(e => e.CheckListGuid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.CheckListTitle).HasMaxLength(255);

                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedBy).HasMaxLength(50);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.HasOne(d => d.TaskGu)
                    .WithMany(p => p.TaskCheckLists)
                    .HasForeignKey(d => d.TaskGuid)
                    .HasConstraintName("FK_TaskCheckLists_Tasks");
            });

            modelBuilder.Entity<TaskCommentAttachments>(entity =>
            {
                entity.HasKey(e => e.AttachmentGuid);

                entity.ToTable("TaskCommentAttachments", "Cooperation");

                entity.Property(e => e.AttachmentGuid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Attachment).IsRequired();

                entity.Property(e => e.CloudPath).HasMaxLength(500);

                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.FileExtension)
                    .IsRequired()
                    .HasMaxLength(8);

                entity.Property(e => e.FileName)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.ModifiedBy).HasMaxLength(50);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.ModuleId)
                    .HasColumnName("ModuleID")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.PrivateLevel)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.RecordGuid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.SyncTime).HasColumnType("datetime");

                entity.Property(e => e.Title).HasMaxLength(255);

                entity.Property(e => e.UrlExternal).HasMaxLength(250);

                entity.HasOne(d => d.TaskComments)
                   .WithMany(p => p.TaskCommentAttachments)
                   .HasForeignKey(d => d.RecordGuid)
                   .HasConstraintName("FK_TaskCommentAttachments_TaskComments");
            });
            modelBuilder.Entity<TaskComments>(entity =>
            {
                entity.HasKey(e => e.CommentGuid);

                entity.ToTable("TaskComments", "Cooperation");

                entity.Property(e => e.CommentGuid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.AvatarUrl).HasMaxLength(255);

                entity.Property(e => e.Comment).IsRequired();

                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.DepartmentId)
                    .HasColumnName("DepartmentID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.DepartmentName).HasMaxLength(50);

                entity.Property(e => e.EmployeeId)
                    .IsRequired()
                    .HasColumnName("EmployeeID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.EmployeeName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.ModifiedBy).HasMaxLength(50);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.RecordGuid).HasDefaultValueSql("(newid())");

                entity.HasOne(d => d.Tasks)
                    .WithMany(p => p.TaskComments)
                    .HasForeignKey(d => d.RecordGuid)
                    .HasConstraintName("FK_TaskComments_Tasks");

                entity.HasOne(d => d.Parent)
                    .WithMany(p => p.InverseParent)
                    .HasForeignKey(d => d.ParentGuid)
                    .HasConstraintName("FK_TaskComments_TaskComments");
            });
            modelBuilder.Entity<TaskOfLabels>(entity =>
            {
                entity.HasKey(e => e.RowGuid);

                entity.ToTable("TaskOfLabels", "Cooperation");

                entity.Property(e => e.RowGuid).HasDefaultValueSql("(newid())");

                entity.HasOne(d => d.Label)
                    .WithMany(p => p.TaskOfLabels)
                    .HasForeignKey(d => d.LabelId)
                    .HasConstraintName("FK_TaskOfLabel_Lables");

                entity.HasOne(d => d.TaskGu)
                    .WithMany(p => p.TaskOfLabels)
                    .HasForeignKey(d => d.TaskGuid)
                    .HasConstraintName("FK_TaskOfLabel_Tasks");
            });
            modelBuilder.Entity<Recurrence>(entity =>
            {
                entity.HasKey(e => e.RowGuid);

                entity.ToTable("Recurrence", "Cooperation");

                entity.Property(e => e.RowGuid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.IsRender).HasColumnType("datetime");

                entity.Property(e => e.LoginName).HasMaxLength(50);

                entity.Property(e => e.ModifiedBy).HasMaxLength(50);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Name).HasMaxLength(250);

                entity.Property(e => e.OptionMore).HasMaxLength(50);

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.Property(e => e.Type).HasMaxLength(50);
            });
            modelBuilder.Entity<TaskOfProcess>(entity =>
            {
                entity.HasKey(e => e.RowGuid);

                entity.ToTable("TaskOfProcess", "Cooperation");

                entity.Property(e => e.RowGuid).HasDefaultValueSql("(newid())");

                entity.HasOne(d => d.TaskGu)
                    .WithMany(p => p.TaskOfProcess)
                    .HasForeignKey(d => d.TaskGuid)
                    .HasConstraintName("FK_TaskOfProcess_Tasks");

                entity.HasOne(d => d.TaskProcessGu)
                    .WithMany(p => p.TaskOfProcess)
                    .HasForeignKey(d => d.TaskProcessGuid)
                    .HasConstraintName("FK_TaskOfProcess_TaskProcess");
            });
            modelBuilder.Entity<TaskOfProfiles>(entity =>
            {
                entity.HasKey(e => e.TaskOfProfiles1);

                entity.ToTable("TaskOfProfiles", "Cooperation");

                entity.Property(e => e.TaskOfProfiles1)
                    .HasColumnName("TaskOfProfiles")
                    .HasDefaultValueSql("(newid())");

                entity.HasOne(d => d.ProfileGu)
                    .WithMany(p => p.TaskOfProfiles)
                    .HasForeignKey(d => d.ProfileGuid)
                    .HasConstraintName("FK_TaskOfProfiles_Profiles");

                entity.HasOne(d => d.TaskOfUserGu)
                    .WithMany(p => p.TaskOfProfiles)
                    .HasForeignKey(d => d.TaskOfUserGuid)
                    .HasConstraintName("FK_TaskOfProfiles_TaskOfUsers");
            });
            modelBuilder.Entity<RecurrenceConfigs>(entity =>
            {
                entity.HasKey(e => e.RowGuid);

                entity.ToTable("RecurrenceConfigs", "Cooperation");

                entity.Property(e => e.RowGuid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Option).HasMaxLength(20);

                entity.Property(e => e.Postion).HasColumnType("datetime");

                entity.Property(e => e.WeekChoice).HasMaxLength(25);

                entity.HasOne(d => d.RecurrenceGu)
                    .WithMany(p => p.RecurrenceConfigs)
                    .HasForeignKey(d => d.RecurrenceGuid)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_RecurrenceConfigs_Recurrence");
            });

            modelBuilder.Entity<TaskOfRecurrences>(entity =>
            {
                entity.HasKey(e => e.RowGuid);

                entity.ToTable("TaskOfRecurrences", "Cooperation");

                entity.Property(e => e.RowGuid).HasDefaultValueSql("(newid())");

                entity.HasOne(d => d.RecurrenceGu)
                    .WithMany(p => p.TaskOfRecurrences)
                    .HasForeignKey(d => d.RecurrenceGuid)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_TaskOfRecurrences_Recurrence");

                entity.HasOne(d => d.TaskGu)
                    .WithMany(p => p.TaskOfRecurrences)
                    .HasForeignKey(d => d.TaskGuid)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_TaskOfRecurrences_Tasks");
            });
            modelBuilder.Entity<Lables>(entity =>
            {
                entity.HasKey(e => e.LableId);

                entity.ToTable("Lables", "Cooperation");

                entity.Property(e => e.LableId)
                    .HasColumnName("LableID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description).HasMaxLength(255);

                entity.Property(e => e.LableName).HasMaxLength(10);

                entity.Property(e => e.ModifiedBy).HasMaxLength(50);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.OrderId).HasColumnName("OrderID");
            });
            modelBuilder.Entity<CategoryOfTasks>(entity =>
            {
                entity.HasKey(e => e.CategoryOfTaskGuid);

                entity.ToTable("CategoryOfTasks", "Cooperation");

                entity.Property(e => e.CategoryOfTaskGuid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Alias)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Class).HasMaxLength(50);

                entity.Property(e => e.ClassText).HasMaxLength(50);

                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.DepartmentGuid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.DepartmentId)
                    .HasColumnName("DepartmentID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.DepartmentName).HasMaxLength(50);

                entity.Property(e => e.Description).HasMaxLength(255);

                entity.Property(e => e.EmployeeGuid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.EmployeeId)
                    .HasColumnName("EmployeeID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.EmployeeName).HasMaxLength(50);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsPublic)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.LoginName).HasMaxLength(50);

                entity.Property(e => e.ModifiedBy).HasMaxLength(50);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.ParentId).HasColumnName("ParentID");

                entity.Property(e => e.ProjectId)
                    .HasColumnName("ProjectID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ProjectName).HasMaxLength(255);

                entity.Property(e => e.TaskLevel)
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.Title).HasMaxLength(255);

                entity.Property(e => e.Type)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.TypeStep)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.HasOne(d => d.Parent)
                    .WithMany(p => p.InverseParent)
                    .HasForeignKey(d => d.ParentId)
                    .HasConstraintName("FK_CategoryOfTasks_CategoryOfTasks");
            });
            modelBuilder.Entity<CategoryShare>(entity =>
            {
                entity.HasKey(e => e.CategoryShareGuid);

                entity.ToTable("CategoryShare", "Cooperation");

                entity.Property(e => e.CategoryShareGuid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.DepartmentId)
                    .HasColumnName("DepartmentID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.DepartmentName).HasMaxLength(50);

                entity.HasOne(d => d.CategoryOfTaskGu)
                    .WithMany(p => p.CategoryShare)
                    .HasForeignKey(d => d.CategoryOfTaskGuid)
                    .HasConstraintName("FK_CategoryShare_CategoryOfTasks");
            });
            modelBuilder.Entity<TaskStepBySteps>(entity =>
            {
                entity.HasKey(e => e.RecordGuid);

                entity.ToTable("TaskStepBySteps", "Cooperation");

                entity.Property(e => e.RecordGuid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Code).HasMaxLength(250);

                entity.Property(e => e.ColorBackground)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ColorText)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedBy).HasMaxLength(50);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Name).HasMaxLength(250);

                entity.HasOne(d => d.CategoryOfTaskGu)
                    .WithMany(p => p.TaskStepBySteps)
                    .HasForeignKey(d => d.CategoryOfTaskGuid)
                    .HasConstraintName("FK_TaskStepBySteps_CategoryOfTasks");
            });

            modelBuilder.Entity<TaskOfUserOfTaskSteps>(entity =>
            {
                entity.HasKey(e => e.RecordGuid);

                entity.ToTable("TaskOfUserOfTaskSteps", "Cooperation");

                entity.Property(e => e.RecordGuid).HasDefaultValueSql("(newid())");

                entity.HasOne(d => d.TaskOfUserGu)
                    .WithMany(p => p.TaskOfUserOfTaskSteps)
                    .HasForeignKey(d => d.TaskOfUserGuid)
                    .HasConstraintName("FK_TaskOfUserOfTaskSteps_TaskOfUsers");

                entity.HasOne(d => d.TaskStepByStepGu)
                    .WithMany(p => p.TaskOfUserOfTaskSteps)
                    .HasForeignKey(d => d.TaskStepByStepGuid)
                    .HasConstraintName("FK_TaskOfUserOfTaskSteps_TaskStepBySteps");

                entity.HasOne(d => d.TaskStepStatusNavigation)
                    .WithMany(p => p.TaskOfUserOfTaskSteps)
                    .HasForeignKey(d => d.TaskStepStatus)
                    .HasConstraintName("FK_TaskOfUserOfTaskSteps_TaskStepStatus");
            });
            modelBuilder.Entity<TaskOfTaskSteps>(entity =>
            {
                entity.HasKey(e => e.RecordGuid);

                entity.ToTable("TaskOfTaskSteps", "Cooperation");

                entity.Property(e => e.RecordGuid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.PermissionByLogin).HasMaxLength(50);

                entity.Property(e => e.PermissionByName).HasMaxLength(50);

                entity.HasOne(d => d.TaskGu)
                    .WithMany(p => p.TaskOfTaskSteps)
                    .HasForeignKey(d => d.TaskGuid)
                    .HasConstraintName("FK_TaskOfTaskSteps_Tasks");

                entity.HasOne(d => d.TaskStepByStepGu)
                    .WithMany(p => p.TaskOfTaskSteps)
                    .HasForeignKey(d => d.TaskStepByStepGuid)
                    .HasConstraintName("FK_TaskOfTaskSteps_TaskStepBySteps");
            });

            modelBuilder.Entity<CategoryOfProcess>(entity =>
            {
                entity.HasKey(e => e.RowGuid);

                entity.ToTable("CategoryOfProcess", "Cooperation");

                entity.Property(e => e.RowGuid).HasDefaultValueSql("(newid())");

                entity.HasOne(d => d.CategoryOfTaskGu)
                    .WithMany(p => p.CategoryOfProcess)
                    .HasForeignKey(d => d.CategoryOfTaskGuid)
                    .HasConstraintName("FK_CategoryOfProcess_CategoryOfTasks");

                entity.HasOne(d => d.TaskProcessGu)
                    .WithMany(p => p.CategoryOfProcess)
                    .HasForeignKey(d => d.TaskProcessGuid)
                    .HasConstraintName("FK_CategoryOfProcess_TaskProcess");
            });
            modelBuilder.Entity<CalendarAttachments>(entity =>
            {
                entity.HasKey(e => e.AttachmentGuid);

                entity.ToTable("CalendarAttachments", "Cooperation");

                entity.Property(e => e.AttachmentGuid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Attachment).IsRequired();

                entity.Property(e => e.CloudPath).HasMaxLength(500);

                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.FileExtension)
                    .IsRequired()
                    .HasMaxLength(8);

                entity.Property(e => e.FileName)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.ModifiedBy).HasMaxLength(50);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.ModuleId)
                    .HasColumnName("ModuleID")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.PrivateLevel)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.RecordGuid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.SyncTime).HasColumnType("datetime");

                entity.Property(e => e.Title).HasMaxLength(255);

                entity.Property(e => e.UrlExternal).HasMaxLength(250);
            });

            modelBuilder.Entity<CalendarComments>(entity =>
            {
                entity.HasKey(e => e.CommentGuid);

                entity.ToTable("CalendarComments", "Cooperation");

                entity.Property(e => e.CommentGuid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.AvatarUrl).HasMaxLength(255);

                entity.Property(e => e.Comment).IsRequired();

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.DepartmentId)
                    .HasColumnName("DepartmentID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.DepartmentName).HasMaxLength(50);

                entity.Property(e => e.EmployeeId)
                    .IsRequired()
                    .HasColumnName("EmployeeID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.EmployeeName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.ModifiedBy).HasMaxLength(50);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.RecordGuid).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<CalendarOfUsers>(entity =>
            {
                entity.HasKey(e => e.CalendarOfUsersGuid);

                entity.ToTable("CalendarOfUsers", "Cooperation");

                entity.Property(e => e.CalendarOfUsersGuid).HasDefaultValueSql("(newid())");

                entity.HasOne(d => d.CalendarGu)
                    .WithMany(p => p.CalendarOfUsers)
                    .HasForeignKey(d => d.CalendarGuid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CalendarOfUsers_Calendars");

                entity.HasOne(d => d.TaskOfUsersGu)
                    .WithMany(p => p.CalendarOfUsers)
                    .HasForeignKey(d => d.TaskOfUsersGuid)
                    .HasConstraintName("FK_CalendarOfUsers_TaskOfUsers");
            });

            modelBuilder.Entity<Calendars>(entity =>
            {
                entity.HasKey(e => e.CalendarGuid);

                entity.ToTable("Calendars", "Cooperation");

                entity.Property(e => e.CalendarGuid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.CalenderContent).IsRequired();

                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.DepartmentGuid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.DepartmentName)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.EmployeeGuid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.EmployeeId)
                    .HasColumnName("EmployeeID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.EmployeeName).HasMaxLength(50);

                entity.Property(e => e.EndTime).HasColumnType("datetime");

                entity.Property(e => e.Location).HasMaxLength(255);

                entity.Property(e => e.ModifiedBy).HasMaxLength(50);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.StartTime).HasColumnType("datetime");

                entity.Property(e => e.Title).HasMaxLength(255);

                entity.Property(e => e.LoginName).HasMaxLength(50);
            });
            modelBuilder.Entity<Profiles>(entity =>
            {
                entity.HasKey(e => e.ProfileGuid);

                entity.ToTable("Profiles", "Cooperation");

                entity.Property(e => e.ProfileGuid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedBy).HasMaxLength(50);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Title).HasMaxLength(50);

                entity.HasOne(d => d.Parent)
                    .WithMany(p => p.InverseParent)
                    .HasForeignKey(d => d.ParentId)
                    .HasConstraintName("FK_Profiles_Profiles");
            });


            modelBuilder.Entity<ProductionManager>(entity =>
            {
                entity.HasKey(e => e.RowGuid);
                entity.ToTable("ProductionManager", "Sale");
                entity.Property(e => e.RowGuid).HasDefaultValueSql("(newid())");
            });
            modelBuilder.Entity<ListEmpAnnouncement>(entity =>
            {
                entity.HasKey(e => e.RecordGuid);
                entity.ToTable("ListEmpAnnouncement", "HR");
                entity.Property(e => e.RecordGuid).HasDefaultValueSql("(newid())");
            });



            modelBuilder.Entity<Announcements>(entity =>
            {
                entity.HasKey(e => e.AnnouncementGuid);
                entity.ToTable("Announcements", "HR");
                entity.Property(e => e.AnnouncementGuid).HasDefaultValueSql("(newid())");

            });

            modelBuilder.Entity<ProductionManagerComments_DOC>(entity =>
            {
                entity.HasKey(e => e.AttachmentGuid);
                entity.ToTable("ProductionManagerComments_DOC", "Sale");
                entity.Property(e => e.AttachmentGuid).HasDefaultValueSql("(newid())");

            });
            modelBuilder.Entity<AnnouncementAttachmentComments>(entity =>
            {
                entity.HasKey(e => e.AttachmentGuid);
                entity.ToTable("AnnouncementAttachmentComments", "Hr");
                entity.Property(e => e.AttachmentGuid).HasDefaultValueSql("(newid())");

            });

            modelBuilder.Entity<ProductionManagerProcess>(entity =>
            {
                entity.HasKey(e => e.RowGuid);
                entity.ToTable("ProductionManagerProcess", "Sale");
                entity.Property(e => e.RowGuid).HasDefaultValueSql("(newid())");

            });
            modelBuilder.Entity<ProductionManagerComments>(entity =>
            {
                entity.HasKey(e => e.CommentGuid);
                entity.ToTable("ProductionManagerComments", "Sale");
                entity.Property(e => e.CommentGuid).HasColumnName("RowGuid");

            });

            modelBuilder.Entity<CategoryColumns>(entity =>
            {
                entity.HasKey(e => e.ColumnGuid);

                entity.ToTable("CategoryColumns", "WF");

                entity.Property(e => e.ColumnGuid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.ColumnId)
                    .HasColumnName("ColumnID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ColumnName).HasMaxLength(255);

                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.DataType)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedBy).HasMaxLength(50);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.TableId)
                    .HasColumnName("TableID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TableName).HasMaxLength(255);

                entity.HasOne(d => d.TableGu)
                    .WithMany(p => p.CategoryColumns)
                    .HasForeignKey(d => d.TableGuid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CategoryColumns_CategoryTables");
            });

            modelBuilder.Entity<CategoryTables>(entity =>
            {
                entity.HasKey(e => e.TableGuid);

                entity.ToTable("CategoryTables", "WF");

                entity.Property(e => e.TableGuid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).HasMaxLength(50);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.ModuleId)
                    .HasColumnName("ModuleID")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ModuleName).HasMaxLength(50);

                entity.Property(e => e.TableId)
                    .HasColumnName("TableID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TableName).HasMaxLength(255);

                entity.Property(e => e.TableType)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('F')");
            });

            modelBuilder.Entity<Conditions>(entity =>
            {
                entity.HasKey(e => e.ConditionGuid);

                entity.ToTable("Conditions", "WF");

                entity.Property(e => e.ConditionGuid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedBy).HasMaxLength(50);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");


            });
            modelBuilder.Entity<Workflows>(entity =>
            {
                entity.HasKey(e => e.WorkflowGuid);

                entity.ToTable("Workflows", "WF");

                entity.Property(e => e.WorkflowGuid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).HasMaxLength(50);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.ModuleId)
                    .HasColumnName("ModuleID")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ModuleName).HasMaxLength(50);

                entity.Property(e => e.TableName).HasMaxLength(50);

                entity.Property(e => e.WorkflowId)
                    .IsRequired()
                    .HasColumnName("WorkflowID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.WorkflowName)
                    .IsRequired()
                    .HasMaxLength(255);


            });
            modelBuilder.Entity<Activities>(entity =>
            {
                entity.HasKey(e => e.ActivityGuid);

                entity.ToTable("Activities", "WF");

                entity.Property(e => e.ActivityGuid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.ActivityName).HasMaxLength(255);

                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedBy).HasMaxLength(50);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

            });
            modelBuilder.Entity<WorkFlowSetting>(entity =>
            {
                entity.HasKey(e => e.RowGuid);

                entity.ToTable("WorkFlowSetting", "WF");

                entity.Property(e => e.RowGuid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedBy).HasMaxLength(50);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.TicketId)
                    .HasColumnName("TicketID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TicketName).HasMaxLength(250);

                entity.Property(e => e.WorkFlowName).HasMaxLength(250);
            });
            modelBuilder.Entity<WorkFlowStatus>(entity =>
            {
                entity.HasKey(e => e.CategoryId);

                entity.ToTable("WorkFlowStatus", "WF");

                entity.Property(e => e.CategoryId)
                    .HasColumnName("CategoryID")
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CategoryName).HasMaxLength(100);

                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description).HasMaxLength(250);

                entity.Property(e => e.LocationId)
                    .HasColumnName("LocationID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedBy).HasMaxLength(50);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.OrderId).HasColumnName("OrderID");
            });
            modelBuilder.Entity<Concurrently>(entity =>
            {
                entity.HasKey(e => e.ConcurrentlyGuid);

                entity.ToTable("Concurrently", "HR");

                entity.Property(e => e.ConcurrentlyGuid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DepartmentId)
                    .IsRequired()
                    .HasColumnName("DepartmentID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.DepartmentName)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.EmployeeId)
                    .IsRequired()
                    .HasColumnName("EmployeeID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.EndDate).HasColumnType("date");

                entity.Property(e => e.JobTitleId).HasColumnName("JobTitleID");

                entity.Property(e => e.JobTitleName).HasMaxLength(50);

                entity.Property(e => e.ModifiedBy).HasMaxLength(50);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.StartDate).HasColumnType("date");


            });
            modelBuilder.Entity<Profestionals>(entity =>
            {
                entity.HasKey(e => e.ProfestionalId);

                entity.ToTable("Profestionals", "HR");

                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description).HasMaxLength(100);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).HasMaxLength(50);

                entity.Property(e => e.ModifiedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ProfestionaName)
                    .IsRequired()
                    .HasMaxLength(50);
            });
            modelBuilder.Entity<JobTitles>(entity =>
            {
                entity.HasKey(e => e.JobTitleId);

                entity.ToTable("JobTitles", "HR");

                entity.Property(e => e.JobTitleId)
                    .HasColumnName("JobTitleID");

                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description).HasMaxLength(255);

                entity.Property(e => e.JobTitleName)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.ModifiedBy).HasMaxLength(50);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");
            });
            modelBuilder.Entity<Educations>(entity =>
            {
                entity.HasKey(e => e.EducationId);

                entity.ToTable("Educations", "HR");

                entity.Property(e => e.EducationId).HasColumnName("EducationID");

                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description).HasMaxLength(255);

                entity.Property(e => e.EducationName)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.ModifiedBy).HasMaxLength(50);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");
            });
            modelBuilder.Entity<Rank>(entity =>
            {
                entity.HasKey(e => e.CategoryId);

                entity.ToTable("Rank", "HR");

                entity.Property(e => e.CategoryId)
                    .HasColumnName("CategoryID")
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CategoryName)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description).HasMaxLength(255);

                entity.Property(e => e.ModifiedBy).HasMaxLength(50);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");
                entity.Property(e => e.OrderId).HasColumnName("OrderID");
                entity.Property(e => e.PercentSalary).HasColumnType("smallmoney");
            });
            modelBuilder.Entity<Candidates>(entity =>
            {
                entity.HasKey(e => e.CandidateGuid);

                entity.ToTable("Candidates", "HR");

                entity.HasIndex(e => e.CandidateId)
                    .HasName("UC_CandidateID")
                    .IsUnique();

                entity.Property(e => e.CandidateGuid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Action).HasMaxLength(255);

                entity.Property(e => e.ApplyDate).HasColumnType("date");

                entity.Property(e => e.Approver).HasMaxLength(500);

                entity.Property(e => e.AssignedTo).HasMaxLength(500);

                entity.Property(e => e.BirthDate).HasColumnType("date");

                entity.Property(e => e.CandidateId)
                    .IsRequired()
                    .HasColumnName("CandidateID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Channel).HasMaxLength(50);

                entity.Property(e => e.CountryId)
                    .HasColumnName("CountryID")
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('VN')");

                entity.Property(e => e.CountryName)
                    .HasMaxLength(50)
                    .HasDefaultValueSql("(N'Việt Nam')");

                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CurrentAddress).HasMaxLength(255);

                entity.Property(e => e.DepartmentId)
                    .HasColumnName("DepartmentID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.DesiredSalary).HasColumnType("money");

                entity.Property(e => e.DistrictId)
                    .HasColumnName("DistrictID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.DistrictName).HasMaxLength(50);

                entity.Property(e => e.EducationId).HasColumnName("EducationID");

                entity.Property(e => e.EducationName).HasMaxLength(20);

                entity.Property(e => e.EthnicId).HasColumnName("EthnicID");

                entity.Property(e => e.EthnicName).HasMaxLength(10);

                entity.Property(e => e.ExamResult)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Experience).HasMaxLength(255);

                entity.Property(e => e.FilterResult)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.FirstName).HasMaxLength(20);

                entity.Property(e => e.FullName)
                    .IsRequired()
                    .HasMaxLength(60);

                entity.Property(e => e.Gender)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Headship).HasMaxLength(500);

                entity.Property(e => e.HomeEmail).HasMaxLength(50);

                entity.Property(e => e.HomeMobile)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.HomeNumber).HasMaxLength(50);

                entity.Property(e => e.HomePhone)
                    .HasMaxLength(15)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(N'()')");

                entity.Property(e => e.Identification)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ImagePath).HasMaxLength(255);

                entity.Property(e => e.InterviewDate).HasColumnType("date");

                entity.Property(e => e.InterviewStaff).HasMaxLength(50);

                entity.Property(e => e.IsDraft).HasDefaultValueSql("((0))");

                entity.Property(e => e.IssueDate).HasColumnType("date");

                entity.Property(e => e.LastName).HasMaxLength(20);

                entity.Property(e => e.MaritalStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.MiddleName).HasMaxLength(20);

                entity.Property(e => e.ModifiedBy).HasMaxLength(50);

                entity.Property(e => e.ModifiedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Note).HasMaxLength(255);

                entity.Property(e => e.OldSalary).HasColumnType("money");

                entity.Property(e => e.PermanentAddress).HasMaxLength(255);

                entity.Property(e => e.PlaceOfBirth).HasMaxLength(255);

                entity.Property(e => e.ProvinceId)
                    .HasColumnName("ProvinceID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ProvinceName).HasMaxLength(50);

                entity.Property(e => e.ReasonNotInterview).HasMaxLength(255);

                entity.Property(e => e.RecruitmentId)
                    .IsRequired()
                    .HasColumnName("RecruitmentID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Referrer).HasMaxLength(50);

                entity.Property(e => e.RejectReason).HasMaxLength(250);

                entity.Property(e => e.Request).HasMaxLength(255);

                entity.Property(e => e.Specialized).HasMaxLength(100);

                entity.Property(e => e.StartDate).HasColumnType("date");

                entity.Property(e => e.Status)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.TaxCode)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.WardId)
                    .HasColumnName("WardID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.WardName).HasMaxLength(20);

                entity.Property(e => e.WorkEmail).HasMaxLength(50);

                entity.Property(e => e.WorkMobile)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.WorkPhone)
                    .HasMaxLength(15)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<ShiftOfEmployees>(entity =>
            {
                entity.ToTable("ShiftOfEmployees", "HR");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ShiftId).HasColumnName("ShiftID");
            });

            modelBuilder.Entity<Shifts>(entity =>
            {
                entity.HasKey(e => e.ShiftId);

                entity.ToTable("Shifts", "HR");

                entity.Property(e => e.ShiftId).HasColumnName("ShiftID");

                entity.Property(e => e.Allowance).HasColumnType("money");

                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Description).HasMaxLength(100);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy)
                    .HasMaxLength(50)
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.ShiftName)
                    .IsRequired()
                    .HasMaxLength(20);
            });
            modelBuilder.Entity<Specializes>(entity =>
            {
                entity.HasKey(e => e.CategoryId);

                entity.ToTable("Specializes", "HR");

                entity.Property(e => e.CategoryId)
                    .HasColumnName("CategoryID")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CategoryName)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description).HasMaxLength(250);

                entity.Property(e => e.ModifiedBy).HasMaxLength(50);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.OrderId).HasColumnName("OrderID");

                entity.Property(e => e.ProfestionalD)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<Employees>(entity =>
            {
                entity.HasKey(e => e.EmployeeGuid);

                entity.ToTable("Employees", "HR");

                entity.Property(e => e.EmployeeGuid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.BankCode)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.BankName).HasMaxLength(255);

                entity.Property(e => e.BirthDate).HasColumnType("date");

                entity.Property(e => e.CandidateId)
                    .HasColumnName("CandidateID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CarId)
                    .HasColumnName("CarID")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.CountryId)
                    .HasColumnName("CountryID")
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('VN')");

                entity.Property(e => e.CountryName)
                    .HasMaxLength(50)
                    .HasDefaultValueSql("(N'Việt Nam')");

                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CurrentAddress).HasMaxLength(255);

                entity.Property(e => e.DepartmentId)
                    .HasColumnName("DepartmentID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.DistrictId)
                    .HasColumnName("DistrictID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.DistrictName).HasMaxLength(50);

                entity.Property(e => e.EducationId).HasColumnName("EducationID");

                entity.Property(e => e.EducationName).HasMaxLength(20);

                entity.Property(e => e.EmployeeId)
                    .IsRequired()
                    .HasColumnName("EmployeeID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.EndDate).HasColumnType("date");

                entity.Property(e => e.EndDateWork).HasColumnType("date");

                entity.Property(e => e.EthnicId).HasColumnName("EthnicID");

                entity.Property(e => e.EthnicName).HasMaxLength(10);

                entity.Property(e => e.FirstName).HasMaxLength(20);

                entity.Property(e => e.FullName).HasMaxLength(60);

                entity.Property(e => e.Gender)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.HireDate).HasColumnType("date");

                entity.Property(e => e.HomeEmail).HasMaxLength(50);

                entity.Property(e => e.HomeMobile)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.HomeNumber).HasMaxLength(50);

                entity.Property(e => e.HomePhone)
                    .HasMaxLength(15)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(N'()')");

                entity.Property(e => e.Identification)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ImagePath).HasMaxLength(255);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsConcurrently)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('N')");

                entity.Property(e => e.IssueIdby)
                    .HasColumnName("IssueIDBy")
                    .HasMaxLength(50);

                entity.Property(e => e.IssueIddate)
                    .HasColumnName("IssueIDDate")
                    .HasColumnType("date");

                entity.Property(e => e.JobTitleId).HasColumnName("JobTitleID");

                entity.Property(e => e.JobTitleName).HasMaxLength(50);

                entity.Property(e => e.LastName).HasMaxLength(20);

                entity.Property(e => e.LoginId)
                    .HasColumnName("LoginID")
                    .HasMaxLength(50);

                entity.Property(e => e.LoginName).HasMaxLength(50);

                entity.Property(e => e.MaritalStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.MiddleName).HasMaxLength(20);

                entity.Property(e => e.ModifiedBy).HasMaxLength(50);

                entity.Property(e => e.ModifiedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Note).HasMaxLength(255);

                entity.Property(e => e.OrganizationId)
                    .IsRequired()
                    .HasColumnName("OrganizationID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PermanentAddress).HasMaxLength(255);

                entity.Property(e => e.PlaceOfBirth).HasMaxLength(100);

                entity.Property(e => e.ProfestionaName).HasMaxLength(50);

                entity.Property(e => e.ProvinceId)
                    .HasColumnName("ProvinceID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ProvinceName).HasMaxLength(50);

                entity.Property(e => e.SalaryMethod)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.SalaryPayBy)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('M')");

                entity.Property(e => e.StartDate).HasColumnType("date");

                entity.Property(e => e.StatusOfWork)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.TaxCode)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.TimeKeepingId)
                    .HasColumnName("TimeKeepingID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Title).HasMaxLength(8);

                entity.Property(e => e.Uniform)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.WardId)
                    .HasColumnName("WardID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.WardName).HasMaxLength(20);

                entity.Property(e => e.WorkEmail).HasMaxLength(50);

                entity.Property(e => e.WorkMobile)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.WorkPhone)
                    .HasMaxLength(15)
                    .IsUnicode(false);


            });
            modelBuilder.Entity<Contracts>(entity =>
            {
                entity.HasKey(e => e.Id);

                entity.ToTable("Contracts", "HR");

                entity.Property(e => e.ContractGuid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.ContractDate).HasColumnType("date");

                entity.Property(e => e.ContractExpired).HasColumnType("date");

                entity.Property(e => e.ContractNo)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.DepartmentId)
                    .HasColumnName("DepartmentID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Description).HasMaxLength(255);

                entity.Property(e => e.EmployeeId)
                    .HasColumnName("EmployeeID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).HasMaxLength(50);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Salary).HasColumnType("money");

                entity.Property(e => e.Title).HasMaxLength(255);
            });
            modelBuilder.Entity<Privilege>(entity =>
            {
                entity.ToTable("ESPrivileges", "eim");

                entity.Property(e => e.ActionId)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Resource)
                    .WithMany(p => p.Privileges)
                    .HasForeignKey(d => d.ResourceId)
                    .HasConstraintName("FK_ESPrivileges_ESResources");
            });

            modelBuilder.Entity<Attribute>(entity =>
            {
                entity.ToTable("ESResAttributes", "eim");

                entity.Property(e => e.Key).HasMaxLength(50);

                entity.Property(e => e.Value).HasMaxLength(255);

                entity.HasOne(d => d.Resource)
                    .WithMany(p => p.Attributes)
                    .HasForeignKey(d => d.ResourceId)
                    .HasConstraintName("FK_ESResAttributes_ESResources");
            });

            modelBuilder.Entity<Resource>(entity =>
            {
                entity.ToTable("ESResources", "eim");

                entity.Property(e => e.Code).HasMaxLength(255);

                entity.Property(e => e.GroupResourceId)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Title).HasMaxLength(255);
            });
            modelBuilder.Entity<ESUserPrivileges>(entity =>
            {
                entity.HasKey(e => e.Id);

                entity.ToTable("ESUserPrivileges", "eim");

            });
            modelBuilder.Entity<ESActions>(entity =>
            {
                entity.ToTable("ESActions", "eim");
            });

            modelBuilder.Entity<Organizations>(entity =>
            {
                entity.HasKey(e => e.OrganizationGuid);

                entity.ToTable("Organizations", "HR");

                entity.Property(e => e.OrganizationGuid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Address).HasMaxLength(250);

                entity.Property(e => e.CountryId)
                    .HasColumnName("CountryID")
                    .HasMaxLength(20);

                entity.Property(e => e.CountyName).HasMaxLength(50);

                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description).HasMaxLength(250);

                entity.Property(e => e.Email)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Fax)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).HasMaxLength(50);

                entity.Property(e => e.ModifiedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.OrganizationId)
                    .HasColumnName("OrganizationID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.OrganizationName)
                    .IsRequired()
                    .HasMaxLength(250);

                entity.Property(e => e.ParentId).HasColumnName("ParentID");

                entity.Property(e => e.Representative).HasMaxLength(50);

                entity.Property(e => e.TaxCode)
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.Tel)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Web)
                    .HasMaxLength(250)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<Departments>(entity =>
            {
                entity.HasKey(e => e.DepartmentGuid);

                entity.ToTable("Departments", "HR");

                entity.HasIndex(e => e.DepartmentId)
                    .HasName("UC_DepartmentID")
                    .IsUnique();

                entity.Property(e => e.DepartmentGuid).ValueGeneratedNever();

                entity.Property(e => e.Address).HasMaxLength(255);

                entity.Property(e => e.Content).HasColumnType("ntext");

                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DepartmentId)
                    .IsRequired()
                    .HasColumnName("DepartmentID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.DepartmentName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Description).HasMaxLength(255);

                entity.Property(e => e.Email).HasMaxLength(255);

                entity.Property(e => e.EnglishName).HasMaxLength(255);

                entity.Property(e => e.Fax).HasMaxLength(50);
                entity.Property(e => e.ManagerId)
                    .HasColumnName("ManagerID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ManagerName).HasMaxLength(250);

                entity.Property(e => e.ModifiedBy).HasMaxLength(50);

                entity.Property(e => e.ModifiedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.OrderId).HasColumnName("OrderID");

                entity.Property(e => e.ParentId).HasColumnName("ParentID");

                entity.Property(e => e.Phone).HasMaxLength(50);

            });
        }
    }
}