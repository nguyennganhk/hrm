﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HRM.Models
{
    public partial class Customers
    {
        [Key]
        public Guid CustomerGuid { get; set; }
        public string CustomerId { get; set; }
        public Guid OrganizationGuid { get; set; }
        public Guid? ParentId { get; set; }
        public Guid? DepartmentGuid { get; set; }
        public string DepartmentName { get; set; }
        public bool? IsBussiness { get; set; }
        public bool? IsCustomer { get; set; }
        public string CustomerName { get; set; }
        public string LeadName { get; set; }
        
        public string Representative { get; set; }
        public string Idnumber { get; set; }
        public string BirthPlace { get; set; }
        public DateTime? IssueIddate { get; set; }
        public string IssueIdby { get; set; }
        public string JobTitleId { get; set; }
        public string JobTitle { get; set; }
        public string CountryId { get; set; }
        public string CountryName { get; set; }
        public int? TerritoryId { get; set; }
        public string TerritoryName { get; set; }
        public string ProvinceId { get; set; }
        public string ProvinceName { get; set; }
        public string DistrictId { get; set; }
        public string DistrictIname { get; set; }
        public string Wards { get; set; }
        public string Address { get; set; }
        public string PermanentAddress { get; set; }
        public string WorkEmail { get; set; }
        public string WorkPhone { get; set; }
        public string WorkMobile { get; set; }
        public string WorkFax { get; set; }
        public string Website { get; set; }
        public string TaxCode { get; set; }
        public string WorkPlace { get; set; }
        public int? ActivityFieldId { get; set; }
        public string ActivityFieldName { get; set; }
        public DateTime? BirthDate { get; set; }
        public double? Capital { get; set; }
        public int? StaffNumber { get; set; }
        public string SourceType { get; set; }
        public byte[] Image { get; set; }
        public string ImageUrl { get; set; }
        public string Gender { get; set; }
        public string MaritalStatus { get; set; }
        public bool? HaveChildren { get; set; }
        public byte? SuccessRate { get; set; }
        public string RateByLevel { get; set; }
        public int? RateByScore { get; set; }
        public string Status { get; set; }
        public string IsActive { get; set; }
        public bool? IsLocked { get; set; }
        public string CompaignId { get; set; }
        public string CompaignName { get; set; }
        public int? ResourceId { get; set; }
        public string ResourceName { get; set; }
        public string ResourceDetail { get; set; }
        public int? BusinessTypeId { get; set; }
        public string BusinessTypeName { get; set; }
        public string SortTitle { get; set; }
        public string Description { get; set; }
        public int? CategoryId { get; set; }
        public decimal? AmountSalesForecas { get; set; }
        public decimal? AmountSales { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Utmsource { get; set; }
        public string Utmcampaign { get; set; }
        public string Utmmedium { get; set; }
        public string Utmcontent { get; set; }
        public string Utmterm { get; set; }
        public string LinkForm { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
        public Guid? CompaignGuid { get; set; }
        public bool? IsLeads { get; set; }
        public bool? IsProvider { get; set; }
        public string OrganizationNameInvoices { get; set; }
        public string AddressInvoices { get; set; }
        public DateTime? StartDatePP { get; set; }
        public string Monitor { get; set; }
        public string Method { get; set; }
        public string Type { get; set; }
        public Guid? PolicyGuid { get; set; }
        public string PolicyId { get; set; }
        public string PolicyName { get; set; }
        public Guid? PolicyGroupGuid { get; set; }
        public string PolicyGroupId { get; set; }
        public string PolicyGroupName { get; set; }
        public string Note { get; set; }
        public string IsProvince { get; set; }
        public Guid? CustomerInvoiceGuid { get; set; }
        public int? DebtDay { get; set; }
      
    }
    public partial class CustomerView
    {
        public CustomerView()
        {

        }
        [Key]
        public Guid CustomerGuid { get; set; }
        public string WorkPhone { get; set; }
        public string CustomerName { get; set; }
        public string CustomerId { get; set; }
        public string WorkEmail { get; set; }
        //public Guid EmployeeGuid { get; set; }
        //public string EmployeeId { get; set; }
        //public string UserName { get; set; }
        //public string FullName { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string TypeName { get; set; }
        public string GroupName { get; set; }
        public string CreatedDateString
        {
            get
            {
                if (CreatedDate != null)
                {
                    return (string)Convert.ToDateTime(CreatedDate).ToString("dd/MM/yyyy");
                }
                else
                {
                    return "";
                }
            }
        }
        public byte[] Image { get; set; }
        public int CountQuotation { get; set; }
        public int CountOrder { get; set; }
        public int CountComment { get; set; }
        public double? Capital { get; set; }
    }
    public class GroupsView
    {
        public GroupsView()
        {

        }
        [Key]
        public Guid GroupGuid { get; set; }
        public string GroupName { get; set; }
        public Guid? SaleGroupGuid { get; set; }
        public string SaleGroupName { get; set; }
        public Guid? ParentId { get; set; }
        public int? OrderId { get; set; }
        public bool IsActive { get; set; }
    }
}
