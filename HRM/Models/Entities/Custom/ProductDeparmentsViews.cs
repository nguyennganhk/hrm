﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace HRM.Models.Custom.Hr
{
    public partial  class ProductDeparmentsViews
    {
        [Key]
        public Guid RowGuid { get; set; }
        public string OrderID { get; set; }
       
        public DateTime? DeliveryDate { get; set; }
        public string DeliveryDateString
        {
            get
            {
                if (DeliveryDate != null)
                {
                    return (string)Convert.ToDateTime(DeliveryDate).ToString("dd/MM/yyyy");
                }
                else
                {
                    return "";
                }
            }
        }
        public string CustomerID { get; set; }
        public string CustomerName { get; set; }
        public string EmployeeName { get; set; }
        public string OrderStatus { get; set; }
        public int? CountComment { get; set; }
    }
}