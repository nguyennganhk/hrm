﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ES_MODEL.HR.Custom
{
    public partial class TimeKeepingByHandCustom
    {
        public Guid? TimeKeepingGuid { get; set; }
        public int? Year { get; set; }
        public int? Month { get; set; }
        public DateTime? Date { get; set; }
        public int? Week { get; set; }
        [Key]
        public Guid? EmployeeGuid { get; set; }
        public string EmployeeName { get; set; }
        public int? JobTitleId { get; set; }
        public string JobTitleName { get; set; }
        public Guid? DepartmentGuid { get; set; }
        public string DepartmentName { get; set; }
        public string DayOfWeek { get; set; }
        public string Holiday { get; set; }
        public bool? IsLocked { get; set; }
        public string Note { get; set; }
        public string Day1 { get; set; }
        public string Day2 { get; set; }
        public string Day3 { get; set; }
        public string Day4 { get; set; }
        public string Day5 { get; set; }
        public string Day6 { get; set; }
        public string Day7 { get; set; }
        public string Day8 { get; set; }
        public string Day9 { get; set; }
        public string Day10 { get; set; }
        public string Day11 { get; set; }
        public string Day12 { get; set; }
        public string Day13 { get; set; }
        public string Day14 { get; set; }
        public string Day15 { get; set; }
        public string Day16 { get; set; }
        public string Day17 { get; set; }
        public string Day18 { get; set; }
        public string Day19 { get; set; }
        public string Day20 { get; set; }
        public string Day21 { get; set; }
        public string Day22 { get; set; }
        public string Day23 { get; set; }
        public string Day24 { get; set; }
        public string Day25 { get; set; }
        public string Day26 { get; set; }
        public string Day27 { get; set; }
        public string Day28 { get; set; }
        public string Day29 { get; set; }
        public string Day30 { get; set; }
        public string Day31 { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
        public int? SortOrder { get; set; }
    }
    public partial class TimeKeepingByHandItemView
    {
        public Guid? TimeKeepingGuid { get; set; }
        public int? Year { get; set; }
        public int? Month { get; set; }
        public DateTime? Date { get; set; }
        public int? Week { get; set; }
        [Key]
        public Guid EmployeeGuid { get; set; }
        public string EmployeeName { get; set; }
        public int? JobTitleId { get; set; }
        public string JobTitleName { get; set; }
        public Guid? DepartmentGuid { get; set; }
        public string DepartmentName { get; set; }
        public string Holiday { get; set; }
        public bool? IsLocked { get; set; }
        public string Note { get; set; }
        public string Day1 { get; set; }
        public string Day2 { get; set; }
        public string Day3 { get; set; }
        public string Day4 { get; set; }
        public string Day5 { get; set; }
        public string Day6 { get; set; }
        public string Day7 { get; set; }
        public string Day8 { get; set; }
        public string Day9 { get; set; }
        public string Day10 { get; set; }
        public string Day11 { get; set; }
        public string Day12 { get; set; }
        public string Day13 { get; set; }
        public string Day14 { get; set; }
        public string Day15 { get; set; }
        public string Day16 { get; set; }
        public string Day17 { get; set; }
        public string Day18 { get; set; }
        public string Day19 { get; set; }
        public string Day20 { get; set; }
        public string Day21 { get; set; }
        public string Day22 { get; set; }
        public string Day23 { get; set; }
        public string Day24 { get; set; }
        public string Day25 { get; set; }
        public string Day26 { get; set; }
        public string Day27 { get; set; }
        public string Day28 { get; set; }
        public string Day29 { get; set; }
        public string Day30 { get; set; }
        public string Day31 { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
    }
    public partial class TimeKeepingsListReport
    {
        [Key]
        public string EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public double? Day1 { get; set; }
        public double? Day2 { get; set; }
        public double? Day3 { get; set; }
        public double? Day4 { get; set; }
        public double? Day5 { get; set; }
        public double? Day6 { get; set; }
        public double? Day7 { get; set; }
        public double? Day8 { get; set; }
        public double? Day9 { get; set; }
        public double? Day10 { get; set; }
        public double? Day11 { get; set; }
        public double? Day12 { get; set; }
        public double? Day13 { get; set; }
        public double? Day14 { get; set; }
        public double? Day15 { get; set; }
        public double? Day16 { get; set; }
        public double? Day17 { get; set; }
        public double? Day18 { get; set; }
        public double? Day19 { get; set; }
        public double? Day20 { get; set; }
        public double? Day21 { get; set; }
        public double? Day22 { get; set; }
        public double? Day23 { get; set; }
        public double? Day24 { get; set; }
        public double? Day25 { get; set; }
        public double? Day26 { get; set; }
        public double? Day27 { get; set; }
        public double? Day28 { get; set; }
        public double? Day29 { get; set; }
        public double? Day30 { get; set; }
        public double? Day31 { get; set; }
        public double? HoursWork { get; set; }
        public double? WorkedDay { get; set; }
        public int? NumOfTimesLate { get; set; }
        public int? NumOfTimesEarly { get; set; }

    }
    public partial class TimeKeepingsListReportDetails
    {
        [Key]
        public Guid? TimeKeepingGuid { get; set; }
        public string EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public Guid? DepartmentGuid { get; set; }
        public int? Year { get; set; }
        public int? Month { get; set; }
        public string DepartmentName { get; set; }
        public int? DateCheck { get; set; }
        public string DayOfWeek { get; set; }
        public string CheckIn { get; set; }
        public string CheckOut { get; set; }
        public double? MinutesLate { get; set; }
        public double? MinutesEarly { get; set; }
        public double? WorkedDay { get; set; }
        public double? HoursWork { get; set; }
        public int? ShiftId { get; set; }
        public string ShiftName { get; set; }

    }
    public partial class TimeKeepingsListReportGeneral
    {
        [Key]
        public Guid? EmployeeGuid { get; set; }
        public string EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public string DepartmentName { get; set; }
        public double? WorkedDay { get; set; }
        public double? HoursWork { get; set; }
        public double? MinutesLate { get; set; }
        public double? MinutesEarly { get; set; }
        public int? TotalAL { get; set; }
        public int? TotalNL { get; set; }
        public int? TotalKL { get; set; }

    }
}
