﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HRM.Models.Custom.Hr
{
    public partial class SanPhamViews
    {
        [Key]
        public Guid RowGuid { get; set; }
        public string MaSanPham { get; set; }

        public string TenSanPham { get; set; }
        public decimal? DonGia { get; set; }
        public string Description { get; set; }
        public string TenHang { get; set; }
        public string TenNhom { get; set; }
        public int? Giamgia { get; set; }
        public string MetaTitle { get; set; }      
        public bool? Location { get; set; }
        public string Title1 { get; set; }
    }
    
}
