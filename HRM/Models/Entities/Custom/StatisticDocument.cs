﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ES_MODEL.HR.Custom
{
    public partial class StatisticDocument
    {
        [Key]
        public Guid RowGuid { get; set;}
        public int? Notstart { get; set;}
        public int? Process { get; set;}
        public int? Complete { get; set;}
        public int? Follow { get; set;}
        public int? Assigned { get; set;}
        public int? Waitsomeone { get; set;}
    }

    public partial class CountDocument
    {
        [Key]
        public Guid RowGuid { get; set; }
        public string Name { get; set; }
        public int Value { get; set; }
    }
    public partial class CountViewHome
    {
        [Key]
        public int CNHienco { get; set; }
        public int? CNNghi { get; set; }
        public int? LDThoivu { get; set; }
        public int? CNTangca { get; set; }
        public int? YCSX { set; get; }
        public int? TongYCSX { set; get; }
        public int? YCSXHuy { set; get; }
        public int? YCSXChoXN { set; get; }
        public int? BOMPending { set; get; }
        public int? DanhSachSP { set; get; }
        public int? TongDNM { set; get; }
    }
    public class ObjectNewData
    {
        [Key]
        public int ID { get; set; }
        public string value { get; set; }
        public string text { get; set; }
        public string valueMore { get; set; }
        public string valueText { get; set; }
    }
}
