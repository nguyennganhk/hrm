﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HRM.Models.Entities.Custom.Project
{
    public partial class CluesViewItem
    {
        [Key]
        public Guid ClueGuid { get; set; }
        public string ClueId { get; set; }
        public string ClueName { get; set; }
        public Guid? ManagerGuid { get; set; }
        public string ManagerID { get; set; }
        public string ManagerName { get; set; }
        public Guid? ParentId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool? IsActive { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public Guid? OrganizationGuid { get; set; }
        public string OrganizationName { get; set; }
        public int? OrderId { get; set; }
    }
}
