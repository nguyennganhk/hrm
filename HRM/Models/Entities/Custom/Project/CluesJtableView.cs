﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HRM.Models.Entities.Custom.Project
{
    public partial class CluesJtableView
    {
        [Key]
        public Guid ClueGuid { get; set; }
        public Guid? ParentId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool? IsActive { get; set; }
    }
}
