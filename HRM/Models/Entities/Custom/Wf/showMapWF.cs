﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ES_MODEL.HR.Custom.Wf
{
    public partial class showMapWF
    {
        [Key]
        public string id { get; set; }
        public string text { get; set; }
        public string status { get; set; } // 0: off; 1: onl
    }
}
