﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ES_MODEL.HR.Custom.Wf
{
    public partial class WorkFlowStatus
    {
        public string CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string Description { get; set; }
        public int? OrderId { get; set; }
        public string LocationId { get; set; }
        public bool? IsActive { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
    }
}
