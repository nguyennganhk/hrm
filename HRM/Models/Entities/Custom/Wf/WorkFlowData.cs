﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ES_MODEL.HR.Custom.Wf
{
    public partial class WorkFlowData
    {
        [Key]
        public string id { get; set; }
        public string type { get; set; }
        public string text { get; set; }
        public string x { get; set; }
        public string y { get; set; }
        public string fill { get; set; }
        public string stroke { get; set; }
        public string extraLinesStroke { get; set; }
        public string width { get; set; }
        public string height { get; set; }
        public string strokeWidth { get; set; }
        public string fontColor { get; set; }
        public string fontSize { get; set; }
        public string textAlign { get; set; }
        public string lineHeight { get; set; }
        public string fontStyle { get; set; }
        public string textVerticalAlign { get; set; }
        public string angle { get; set; }
        public string strokeType { get; set; }
    }
}
