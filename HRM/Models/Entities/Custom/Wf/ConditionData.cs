﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ES_MODEL.HR.Custom.Wf
{
    public partial class ConditionData
    {
        [Key]
        public Guid ColumnGuid { get; set; }
        public string Condition { get; set; }
        public string ConditionValue { get; set; }
        public string AndOr { get; set; }
    }
}
