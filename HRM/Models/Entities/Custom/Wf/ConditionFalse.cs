﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ES_MODEL.HR.Custom.Wf
{
    public partial class ConditionFalse1
    {
        [Key]
        public string Activity { get; set; }
        public string ApproveType { get; set; }
        public string[] Approver { get; set; }
        public string StepCallback { get; set; }
    }
}
