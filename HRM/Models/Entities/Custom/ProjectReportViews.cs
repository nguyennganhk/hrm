﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace HRM.Models.Custom.Hr
{
    public partial  class ProjectReportView1
    {
        [Key]
        public int Stt { get; set; }
        public string KHDK { get; set; }
        public string NhomDK { get; set; }
        public int? SLDADK { get; set; }
        public string KHCK { get; set; }
        public string NhomCK { get; set; }
        public int? SLDACK { get; set; }
        public string Note { get; set; }      
    }
    public partial class ProjectReportView2
    {
        [Key]
        public int Stt { get; set; }     
        public string KH { get; set; }
        public int? SLCV { get; set; }
        public int? HT { get; set; }
        public int? KHT { get; set; }
        public Decimal? PTHT { get; set; }
        public Decimal? PTKHT { get; set; }
    }
    public partial class ProjectReportView3
    {
        [Key]
        public int Stt { get; set; }
        public string Note { get; set; }
        public int? SLDA { get; set; }
        public int? HT { get; set; }
        public int? KHT { get; set; }
        public Decimal? PTHT { get; set; }
        public Decimal? PTKHT { get; set; }
    }
    public partial class ProjectReportView5
    {
        [Key]
        public int Stt { get; set; }
        public string Tenban { get; set; }
        public int? SLCV { get; set; }
        public int? SLPhHT { get; set; }
        public int? SLPhKHT { get; set; }
        public Decimal? PTHT { get; set; }
        public Decimal? PTKHT { get; set; }
    }
    public partial class ProjectReportView6
    {
        [Key]
        public int Stt { get; set; }
        public string PLCQKQ { get; set; }
        public int? CVKHT { get; set; }
        public Decimal? DG { get; set; }
        public int? Type { get; set; }
        
    }
   
    public partial class ReportViewSheet1
    {
        [Key]
        public int Stt { get; set; }
        public string Maduan { get; set; }
        public string Tenduan { get; set; }
        public string Cluec1 { get; set; }
        public string PlanName { get; set; }
        public string GroupName { get; set; }
        public string TypePlan { get; set; }
        public Decimal? Amount { get; set; }
        public Decimal? AmountPackage { get; set; }
        public Decimal? AmountLiabilities { get; set; }
        public Decimal? AmountFuel { get; set; }       
      

    }
    public partial class ReportViewSheet2
    {      
        [Key]
        public int Stt { get; set; }
        public string Maduan { get; set; }
        public string Tenduan { get; set; }
        public string Notekpi37 { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Notec1 { get; set; }
        public string Note { get; set; }
        public DateTime? StartDatec1 { get; set; }
        public DateTime? EndDatec1 { get; set; }
        public Decimal? GTGN { get; set; }
        public string Notec2 { get; set; }
        public DateTime? StartDatec2 { get; set; }
        public DateTime? EndDatec2 { get; set; }      
        public Decimal? CPTC { get; set; }
        public string TMTC { get; set; }
        public Decimal? CPNB { get; set; }
        public string TMNB { get; set; }
        public string BKT { get; set; }
        public string NotePHKT { get; set; }
        public DateTime? EndDateKT { get; set; }
        public string BTMQT { get; set; }
        public string NotePHTMQT { get; set; }
        public DateTime? EndDateTMQT { get; set; }
        public string NoteBTCKT { get; set; }
        public DateTime? EndDateBTCKT { get; set; }
        public string NameKH { get; set; }
        public string NoteKH { get; set; }
        public DateTime? EndDateKH { get; set; }
        public string PLCQNN { get; set; }// phân loại cơ quan nhà nước
        public string NameCQNN { get; set; }
        public string NotePHCQNN { get; set; }
        public DateTime? EndDateCQNN { get; set; }
        public string NameTP { get; set; }
        public string NotePHTP { get; set; }
        public DateTime? EndDateTP { get; set; }
        public string PLTV { get; set; } // phân loại TV
        public string NameTV { get; set; }
        public string NotePHTV { get; set; }
        public DateTime? EndDateTV { get; set; }
        public string NameKB { get; set; }
        public string NotePHKB { get; set; }
        public DateTime? EndDateKB { get; set; }
    }
    public partial class ReportViewSheet3
    {
        [Key]
        public int Stt { get; set; }
        public string Maduan { get; set; }
        public string Tenduan { get; set; }
        public string Notekpi37 { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Notec1 { get; set; }
        public string Note { get; set; }
        public DateTime? StartDatec1 { get; set; }
        public DateTime? EndDatec1 { get; set; }
        public Decimal? GTGN { get; set; }
        public string Notec2 { get; set; }
        public DateTime? StartDatec2 { get; set; }
        public DateTime? EndDatec2 { get; set; }
        public DateTime? GHc2 { get; set; }
        public string PLCQKQ { get; set; } // phân loại chủ quan khách quan
        public string ObjectiveName { get; set; } // đơn vị gây lỗi
        public string Reason { get; set; } // nguyên nhân
        public Decimal? CPTC { get; set; }
        public string TMTC { get; set; }
        public Decimal? CPNB { get; set; }
        public string TMNB { get; set; }
        public string BKT { get; set; }
        public string NotePHKT { get; set; }
        public DateTime? EndDateKT { get; set; }
        public string BTMQT { get; set; }
        public string NotePHTMQT { get; set; }
        public DateTime? EndDateTMQT { get; set; }
        public string NoteBTCKT { get; set; }
        public DateTime? EndDateBTCKT { get; set; }
        public string NameKH { get; set; }
        public string NoteKH { get; set; }
        public DateTime? EndDateKH { get; set; }
        public string PLCQNN { get; set; }// phân loại cơ quan nhà nước
        public string NameCQNN { get; set; }
        public string NotePHCQNN { get; set; }
        public DateTime? EndDateCQNN { get; set; }
        public string NameTP { get; set; }
        public string NotePHTP { get; set; }
        public DateTime? EndDateTP { get; set; }
        public string PLTV { get; set; } // phân loại TV
        public string NameTV { get; set; }
        public string NotePHTV { get; set; }
        public DateTime? EndDateTV { get; set; }
        public string NameKB { get; set; }
        public string NotePHKB { get; set; }
        public DateTime? EndDateKB { get; set; }
    }
    public partial class ReportViewSheet4
    {
        [Key]
        public int Stt { get; set; }
        public string PLCQKQ { get; set; }
        public int? CVKHT { get; set; }
        public Decimal? DG { get; set; }

    }
  
}