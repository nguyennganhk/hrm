﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ES_MODEL.HR.Custom
{
    public partial class WorkFlowStatusJtableView
    {
        [Key]
        public string CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string Description { get; set; }
        public string LocationId { get; set; }
        public int? OrderId { get; set; }
        public bool? IsActive { get; set; }
    }
}
