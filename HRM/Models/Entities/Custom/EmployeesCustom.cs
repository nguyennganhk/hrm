﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HRM.Models.Custom.Hr
{
    public partial class EmployeesViewLogin
    {
        [Key]
        public Guid EmployeeGuid { get; set; }
        public string EmployeeId { get; set; }
        public string LoginId { get; set; }
        public string LoginName { get; set; }
        public string FullName { get; set; }
        public string WorkPhone { get; set; }
        public string DepartmentId { get; set; }
        public Guid? DepartmentGuid { get; set; }
        public string DepartmentName { get; set; }
        public Guid OrganizationGuid { get; set; }
        public string OrganizationName { get; set; }
        public int? JobTitleId { get; set; }
        public string JobTitleName { set; get; }
    }
    public partial class EmployeesListView
    {        
        public Guid OrganizationGuid { get; set; }
        [Key]
        public Guid EmployeeGuid { get; set; }
        public string EmployeeId { get; set; }
        public string FullName { set; get; }
    }
    public partial class EmployeesBySearch
    {
        [Key]
        public Guid EmployeeGuid { get; set; }
        public string EmployeeId { get; set; }
        public string FullName { set; get; }
        public string LoginName { set; get; }
        public int? JobTitleId { set; get; }
        public string JobTitle { set; get; }
    }
    public partial class EmployeesBySearchOut
    {
        [Key]
        public Guid EmployeeGuid { get; set; }
        public string EmployeeId { get; set; }
        public string FullName { set; get; }
        public string LoginName { set; get; }
        public int? JobTitleId { set; get; }
        public string JobTitle { set; get; }
        public string Alias { set; get; }
    }
}
