﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace HRM.Models.Custom.Hr
{
    public partial  class ResignationFormsJtableView
    {
        [Key]
        public Guid ResignationFormGuid { get; set; }
        public string ResignationFormID { get; set; }
        public string Title { get; set; }
        public Guid? OrganizationGuid { get; set; }
        public string EmployeeName { get; set; }
        public string DepartmentName { get; set; }
        public string StatusWF { get; set; }
        public string Status { get; set; }
        public string ReasonName { get; set; }
        public int? Permisstion { get; set; }
        public int CountComment { get; set; }
        public string HRComment { get; set; }
        public DateTime? StartDate { get; set; }
        public string StartDateString
        {
            get
            {
                if (StartDate != null)
                {
                    return (string)Convert.ToDateTime(StartDate).ToString("dd/MM/yyyy");
                }
                else
                {
                    return "";
                }
            }
        }
    }
}