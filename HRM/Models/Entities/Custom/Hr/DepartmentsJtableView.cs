﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace HRM.Models.Custom.Hr
{
    public partial class DepartmentsJtableView
    {
        [Key]
        public Guid DepartmentGuid { get; set; }
        public string DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public Guid? ParentId { get; set; }
        public string Description { get; set; }
        public int? OrderId { get; set; }
        public bool? IsActive { get; set; }
    }
}
