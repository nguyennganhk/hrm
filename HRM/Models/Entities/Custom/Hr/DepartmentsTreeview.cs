﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Text;

namespace HRM.Models.Custom.Hr
{
    public class DepartmentsTreeview
    {
        [Key]
        public Guid DepartmentGuid { get; set; }     
        public string DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public Guid? ParentId { get; set; }
        public bool? IsActive { get; set; }
        public int? NumberEmployee { get; set; }
    }



}
