﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ES_MODEL.HR.Custom.Hr
{
    public partial class CategoryColumns
    {
        [Key]
        public Guid ColumnGuid { get; set; }
        public Guid TableGuid { get; set; }
        public string TableId { get; set; }
        public string TableName { get; set; }
        public string ColumnId { get; set; }
        public string ColumnName { get; set; }
        public string DataType { get; set; }
        public string ConditionOfColumn { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }

        public CategoryTables TableGu { get; set; }
    }
}
