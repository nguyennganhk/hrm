﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HRM.Models.Custom.Hr
{
    public partial class DepartmentsViewItem
    {
        [Key]
        public Guid? DepartmentGuid { get; set; }
        public string DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public Guid? ManagerGuid { get; set; }
        public string ManagerID { get; set; }
        public string ManagerName { get; set; }
        public Guid? ParentId { get; set; }
        public string ParentName { get; set; }
        public Guid? OrganizationGuid { get; set; }
        public string OrganizationName { get; set; }
        public string Description { get; set; }
        public string Content { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public int? OrderId { get; set; }
        public bool? IsActive { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
    }
}
