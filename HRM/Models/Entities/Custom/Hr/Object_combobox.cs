﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Text;

namespace HRM.Models.Custom.Hr
{
    public class Object_Combobox
    {
        [Key]
        public string value { get; set; }     
        public string text { get; set; }      
       
    }
    public class ObjectNew_Combobox
    {
        [Key]
        public string value { get; set; }
        public string text { get; set; }
        public string valueMore { get; set; }
        public string valueText { get; set; }

    }
    public class ObjectNewData
    {
        [Key]
        public int ID { get; set; }
        public string value { get; set; }
        public string text { get; set; }
        public string valueMore { get; set; }
        public string valueText { get; set; }

    }
    public class Object_Table
    {
        [Key]
        public string Table1 { get; set; }
        public string Table2 { get; set; }
        public string Table3 { get; set; }      

    }
    public class Object_Table5
    {
        [Key]
        public string Table1 { get; set; }
        public string Table2 { get; set; }
        public string Table3 { get; set; }
        public string Table4 { get; set; }
        public string Table5 { get; set; }
    }
}
