﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace HRM.Models.Custom.Hr
{
    public class EmployeesExport
    {
        [Key]
        public Guid EmployeeGuid { get; set; }

        public string LastName { get; set; }
        public string EmployeeID { get; set; }
        public string FullName { get; set; }
        public string StatusOfWork { get; set; }
        public string StatusOfWorkString
           
        {
            get
            {
                if (StatusOfWork == "OM")
                {
                    return "Nhân viên chính thức";
                }
                else if(StatusOfWork == "PE")
                {
                    return "Thử việc";
                }
                else if (StatusOfWork == "PT")
                {
                    return "Part time";
                }
                else if (StatusOfWork == "ML")
                {
                    return "Nghỉ thai sản";
                }
                else if (StatusOfWork == "LJ")
                {
                    return "Nghỉ việc";
                }
                else
                {
                    return "";
                }
            }
        }
        public string DepartmentID { get; set; }
        public string Phone { get; set; }
        public string PhotoTitle { get; set; }
        public string JobTitlesName { get; set; }
        public DateTime? StartDate { get; set; }
        public string CurrentAddress { get; set; }
        public string StartDateString
        {
            get
            {
                if (StartDate != null)
                {
                    return (string)Convert.ToDateTime(StartDate).ToString("dd/MM/yyyy");
                }
                else
                {
                    return "";
                }
            }
        }
        public DateTime? EndDate { get; set; }
        public string EndDateString
        {
            get
            {
                if (EndDate != null)
                {
                    return (string)Convert.ToDateTime(EndDate).ToString("dd/MM/yyyy");
                }
                else
                {
                    return "";
                }
            }
        }
        public int? ContractExpired { get; set; }

        public DateTime? HireDate { get; set; }
        public string HireDateString
        {
            get
            {
                if (HireDate != null)
                {
                    return (string)Convert.ToDateTime(HireDate).ToString("dd/MM/yyyy");
                }
                else
                {
                    return "";
                }
            }
        }
        public DateTime? IssueIddate { get; set; }
        public string IssueIddateString
        {
            get
            {
                if (IssueIddate != null)
                {
                    return (string)Convert.ToDateTime(IssueIddate).ToString("dd/MM/yyyy");
                }
                else
                {
                    return "";
                }
            }
        }
        public string LoginName { get; set; }
        public string IssueIdby { get; set; }
        public string Gender { get; set; }
        public string GenderString
        {
            get
            {
                if (Gender == "M")
                {
                    return "Nam";
                }
                else if(Gender == "F")
                {
                    return "Nữ";
                }
                else
                {
                    return "";
                }
            }
        }
        public string Identification { get; set; }
        public string DepartmentName { get; set; }
        public string ProfestionaName { get; set; }
        public DateTime? BirthDate { get; set; }
        public string BirthDateString
        {
            get
            {
                if (BirthDate != null)
                {
                    return (string)Convert.ToDateTime(BirthDate).ToString("dd/MM/yyyy");
                }
                else
                {
                    return "";
                }
            }
        }
        public string WorkEmail { get; set; }
        public string TaxCode { get; set; }
        public string BankAccount { get; set; }
        public string BankName { get; set; }
        public string OrganizationName { get; set; }
        public string GenitiveId { get; set; }
        public bool? IsActive { get; set; }
        public string ContractType { get; set; }
        public string ContractTypeString

        {
            get
            {
                if (ContractType == "Y")
                {
                    return "Xác định thời hạn";
                }
                else if (ContractType == "N")
                {
                    return "Không xác định thời hạn";
                }
                else if (ContractType == "T")
                {
                    return "Hợp đồng thời vụ";
                }
                else if (ContractType == "K")
                {
                    return "Khác";
                }
                else
                {
                    return "";
                }
            }
        }
        public string ReasonName { get; set; }
    }
}
