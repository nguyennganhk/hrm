﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ES_MODEL.HR.Custom.Hr
{
    public partial class WorkFlowSettingJTableView
    {
        [Key]
        public Guid RowGuid { get; set; }
        public Guid? WorkFlowGuid { get; set; }
        public string WorkFlowName { get; set; }
        public string TicketId { get; set; }
        public string TicketName { get; set; }
    }
}
