﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Text;

namespace HRM.Models.Custom.Hr
{
    public class EmployeesView
    {
        [Key]
        public Guid EmployeeGuid { get; set; }        
        public string CandidateName { get; set; }
        public string EmployeeId { get; set; }      
        public string DepartmentName { get; set; }
        public string Title { get; set; }
        public string FullName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Identification { get; set; }
        public DateTime? IssueIddate { get; set; }
        public string IssueIdby { get; set; }
        public string LoginId { get; set; }
        public string LoginName { get; set; }       
        public string JobTitleName { get; set; }      
        public string ProfestionaName { get; set; }
        public string SpecializesId { get; set; }
        public DateTime? BirthDate { get; set; }
        public string PlaceOfBirth { get; set; }
        public string MaritalStatus { get; set; }
        public string Gender { get; set; }
        public DateTime? HireDate { get; set; }
        public string SalaryPayBy { get; set; }
        public string SalaryMethod { get; set; }
        public string PermanentAddress { get; set; }
        public string CurrentAddress { get; set; }    
        public string CountryName { get; set; }      
        public string ProvinceName { get; set; }    
        public string DistrictName { get; set; }
        public string WardName { get; set; }
        public string CountryId { get; set; }
        public string ProvinceId { get; set; }
        public string DistrictId { get; set; }
        public string WardId { get; set; }
        public string HomeNumber { get; set; }        
        public string EthnicName { get; set; }
        public string HomePhone { get; set; }
        public string HomeMobile { get; set; }
        public string HomeEmail { get; set; }
        public string WorkPhone { get; set; }
        public string WorkMobile { get; set; }
        public string WorkEmail { get; set; }    
        public string EducationName { get; set; }
        public string ImagePath { get; set; }
        public byte[] Photo { get; set; }
        public string TaxCode { get; set; }    
        public string BankName { get; set; }
        public string BankAccount { get; set; }
        public string StatusOfWork { get; set; }
        public DateTime? EndDateWork { get; set; }
        public string Note { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? ProbationPeriod { get; set; }
        public string Uniform { get; set; }
        public string IsConcurrently { get; set; }
        public string TimeKeepingId { get; set; }
        public bool? IsUseCar { get; set; }
        public string CarId { get; set; }
        public bool? IsActive { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string GenitiveId { get; set; }
        public string SalaryTierId { get; set; }
        public string RankName { get; set; }
        public int? ReasonLeaveId { get; set; }
        public string ManagerName { get; set; }
        
    }
    public class EmployeesViews
    {
        [Key]
        public Guid EmployeeGuid { get; set; }
        public Guid? DepartmentGuid { get; set; }
        public string DepartmentId { get; set; }
        public string EmployeeId { get; set; }
        public string LoginName { get; set; }
        public string FullName { get; set; }
        public string LookupId { get; set; }
        public string LookupValue { get; set; }
        public int? JobTitleId { get; set; }
        public string JobTitleName { get; set; }
    }
}
