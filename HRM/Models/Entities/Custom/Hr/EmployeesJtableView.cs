﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Text;

namespace HRM.Models.Custom.Hr
{
    public class EmployeesJtableView
    {
        [Key]
        public Guid EmployeeGuid { get; set; }    
        public string LastName { get; set; }
        public string EmployeeID { get; set; }
        public string FullName { get; set; }
        public string StatusOfWork { get; set; }
        public string DepartmentID { get; set; }
        public string Phone { get; set; }
        public string PhotoTitle { get; set; }
        public string JobTitlesName { get; set; }
        public DateTime? StartDate { get; set; }
        public string StartDateString
        {
            get
            {
                if (StartDate != null)
                {
                    return (string)Convert.ToDateTime(StartDate).ToString("dd/MM/yyyy");
                }
                else
                {
                    return "";
                }
            }
        }
        public DateTime? EndDate { get; set; }
        public string EndDateString
        {
            get
            {
                if (EndDate != null)
                {
                    return (string)Convert.ToDateTime(EndDate).ToString("dd/MM/yyyy");
                }
                else
                {
                    return "";
                }
            }
        }
        public int? ContractExpired { get; set; }
       
        public DateTime? HireDate { get; set; }
        public string HireDateString
        {
            get
            {
                if (HireDate != null)
                {
                    return (string)Convert.ToDateTime(HireDate).ToString("dd/MM/yyyy");
                }
                else
                {
                    return "";
                }
            }
        }
        public string LoginName { get; set; }
        public string Gender { get; set; }
        public string Identification { get; set; }
        public string DepartmentName { get; set; }
        public string ProfestionaName { get; set; }
        public string ShiftName { get; set; }
        public TimeSpan? ShiftStartTime { get; set; }
        public TimeSpan? ShiftEndTime { get; set; }
        public int? CheckApplyLeaves { get; set; }
        public Guid? CandidateGuid { get; set; }
    }
    public class CheckEmployeeView
    {
        [Key]
        public Guid EmployeeGuid { get; set; }      
        public string EmployeeID { get; set; }
        public string FullName { get; set; }      
        public string ShiftId { get; set; }
        public string ShiftName { get; set; }
        public TimeSpan? ShiftStartTime { get; set; }
        public TimeSpan? ShiftEndTime { get; set; }
        public string ShiftNameAttenId { get; set; }
        public string ShiftNameAtten { get; set; }
        public TimeSpan? ShiftStartTimeAtten { get; set; }
        public TimeSpan? ShiftEndTimeAtten { get; set; }
        public DateTime? Date { get; set; }
        public string DateString
        {
            get
            {
                if (Date != null)
                {
                    return (string)Convert.ToDateTime(Date).ToString("dd/MM/yyyy");
                }
                else
                {
                    return "";
                }
            }
        }
    }
    public class PhotoView
    {
        [Key]
        public Guid EmployeeGuid { get; set; }
        public string ImagePath { get; set; }
        public byte[] Thumb { get; set; }

    }
}
