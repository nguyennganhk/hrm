﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HRM.Models.Custom.Hr
{
    public partial class ProjectManageView1
    {
        [Key]
        public int Stt { get; set; }
        public string FullName { get; set; }
        public int? KH1 { get; set; }
        public int? KH2 { get; set; }
        public int? KH3 { get; set; }
        public int? KH4 { get; set; }
        public int? KH5 { get; set; }

    }
    public partial class ProjectManageView3
    {
        [Key]
        public int Stt { get; set; }
        public string KH { get; set; }
        public string NhomKH { get; set; }
        public int? SLDA { get; set; }
        public int? Tamp { get; set; } // dùng để đánh giấu kế hoạch hay nhóm kế hoạch

    }
    public partial class ProjectManageView4
    {
        [Key]
        public int Stt { get; set; }
        public string Tenban { get; set; }
        public int? SLCV { get; set; }
        public int? SLDA { get; set; }
        public int? Tamp { get; set; }
    }
    public partial class ProjectManageView5
    {
        [Key]
        public int Stt { get; set; }
        public string FullName { get; set; }
        public Decimal? CPTC { get; set; }
        public Decimal? CPNB { get; set; }
        public int? Tamp { get; set; }
    }
    public partial class ProjectManageView6
    {
        [Key]
        public int Stt { get; set; }
        public string FullName { get; set; }
        public string Daumoi { get; set; }
        public string TenDA { get; set; }
        public string Kpi37 { get; set; }
        public string Kpi236 { get; set; }
        public string Notec1 { get; set; }
        public DateTime? StartDate { get; set; }
        public string StartDateString
        {
            get
            {
                if (StartDate != null)
                {
                    return (string)Convert.ToDateTime(StartDate).ToString("dd/MM/yyyy");
                }
                else
                {
                    return "";
                }
            }
        }
        public DateTime? EndDate { get; set; }
        public string EndDateString
        {
            get
            {
                if (StartDate != null)
                {
                    return (string)Convert.ToDateTime(EndDate).ToString("dd/MM/yyyy");
                }
                else
                {
                    return "";
                }
            }
        }
    }
    public partial class ProjectManageResultView2
    {
        [Key]
        public int Stt { get; set; }
        public string FullName { get; set; }
        public int? SLDA { get; set; }
        public int? SLCVc1 { get; set; }
        public int? HT { get; set; }
        public int? KHT { get; set; }
    }
    public partial class ProjectManageResultView3
    {
        [Key]
        public int Stt { get; set; }
        public string ClueName { get; set; }
        public string TenDuan { get; set; }
        public string Notec1 { get; set; }
        public string Note { get; set; }
        public string PLCQKQ { get; set; } // phân loại chủ quan khách quan
        public string ObjectiveName { get; set; } // đơn vị gây lỗi
        public string Reason { get; set; } // nguyên nhân
    }
    public partial class DashboardView1
    {
        [Key]
        public int Stt { get; set; }
        public string Title { get; set; }
        public Decimal? GTDH { get; set; } 
        public Decimal? GTGDH { get; set; }
        public Decimal? CN { get; set; }
        public Decimal? NL { get; set; }
        public int? SLDHPT { get; set; }
        public int? SLDHPS { get; set; }
        public int? BHBT { get; set; }
        public int? SLDHBHBT { get; set; }
        public int? SLDHHSPL { get; set; }
        public int? Tamp { get; set; }
    }
    public partial class CountDocuments
    {
        [Key]
        public Guid RowGuid { get; set; }
        public string Name { get; set; }
        public int Value { get; set; }
    }
}
