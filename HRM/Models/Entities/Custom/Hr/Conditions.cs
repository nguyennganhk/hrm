﻿using System;
using System.Collections.Generic;

namespace ES_MODEL.HR.Custom.Hr
{
    public partial class Conditions
    {
        public Guid ConditionGuid { get; set; }
        public Guid WorkflowGuid { get; set; }
        public string ConditionData { get; set; }
        public string ConditionTrue { get; set; }
        public string ConditionFalse { get; set; }
        public Guid? LocationGuid { get; set; }
        public bool? WorkflowType { get; set; }
        public string StepCallback { get; set; }
        public string StepContinue { get; set; }
        public string ConditionUid { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string WfstatusId { get; set; }
        public string WfstatusBackId { get; set; }
        public string WfconditionId { get; set; }
        public string WfconditionBackId { get; set; }
        public string PersonMain { get; set; }
        public string PersonSupport { get; set; }
        public string PersonFollow { get; set; }
    }
}
