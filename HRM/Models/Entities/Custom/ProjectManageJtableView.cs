﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace HRM.Models.Custom.Hr
{
    public partial  class ProjectManageJtableView
    {
        [Key]
        public Guid ProjectDetailGuidC2 { get; set; }
        public Guid? ProjectDetailGuidC1 { get; set; }
        public Guid? ProjectGuid { get; set; }       
        public string FullName { get; set; }
        public string TitleClueGuid { get; set; }
        public string TitleProjectCode { get; set; }
        public string TitleKPI236 { get; set; }
        public string TitlePlan { get; set; }      
        public string TitleKPI37 { get; set; }
        public string Notec1 { get; set; }
        public string Notec2 { get; set; }
        public DateTime? StartDate { get; set; }
        public string StartDateString
        {
            get
            {
                if (StartDate != null)
                {
                    return (string)Convert.ToDateTime(StartDate).ToString("dd/MM/yyyy");
                }
                else
                {
                    return "";
                }
            }
        }
        public DateTime? EndDate { get; set; }
        public string EndDateString
        {
            get
            {
                if (EndDate != null)
                {
                    return (string)Convert.ToDateTime(EndDate).ToString("dd/MM/yyyy");
                }
                else
                {
                    return "";
                }
            }
        }
        public int? CountExpired { get; set; }
        public int? Status { get; set; }
        public int? Approver { get; set; }
        public int? ExpiredStatus { get; set; }
        public string DepartmentName { get; set; }
        public DateTime? GHC2 { get; set; }
        public string GHC2String
        {
            get
            {
                if (GHC2 != null)
                {
                    return (string)Convert.ToDateTime(GHC2).ToString("dd/MM/yyyy");
                }
                else
                {
                    return "";
                }
            }
        }
        
    }
   
}