﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HRM.Models
{
    public class BaseItem
    {
    }
    public class SQLCOMMAND
    {
        [Key]
        public string Error { get; set; }
    }
    public class SQLCOMMAND_V2
    {
        [Key]
        public string Error { get; set; }
        public string Module { set; get; }
    }
    public class SQLCOMMAND_FUNC
    {
        [Key]
        public string Value { get; set; }
    }
    public class SQLCOMMAND2
    {
        [Key]
        public string value { get; set; }
    }
    public class SQLCOMMAND_INVENTORY
    {
        [Key]
        public string Error { get; set; }
        public decimal? CapitalMoney { get; set; }
        public decimal? CapitalPrice { get; set; }
    }
    public class SQLConvertLoginName
    {
        [Key]
        public string LoginName { get; set; }
    }
    public class SQLCOMMANDMessage
    {
        [Key]
        public string Error { get; set; }
        public string LstApprover { get; set; }
        public string Status { get; set; }
    }
    public partial class AttachmentsDownloadView
    {
        [Key]
        public Guid AttachmentGuid { get; set; }
        public string FileName { get; set; }
        public byte[] Attachment { get; set; }
    }
    public partial class OrderAttachmentsView
    {
        [Key]
        public Guid AttachmentGuid { get; set; }
        public string Title { get; set; }
        public string FileName { get; set; }
        //thanhtt
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
    }
    public partial class OrderAttachmentsDownloadView
    {
        [Key]
        public Guid AttachmentGuid { get; set; }
        public string FileName { get; set; }
        public byte[] Attachment { get; set; }
    }

    public partial class AttachmentsView
    {
        [Key]
        public Guid AttachmentGuid { get; set; }
        public string Title { get; set; }
        public string FileName { get; set; }
        public string ModuleId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public bool? IsReadonly { get; set; }
    }
    public partial class CommentView
    {
        [Key]
        public Guid CommentGuid { get; set; }
        public string Comment { get; set; }
        public string LoginName { get; set; }
        public Guid EmployeeGuid { get; set; }
        public string EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public string DepartmentName { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string CommentAttachments { get; set; }
    }
}
