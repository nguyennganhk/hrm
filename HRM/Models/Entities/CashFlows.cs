﻿using DocumentFormat.OpenXml.Office2013.PowerPoint.Roaming;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRM.Models
{ 
    public partial class CashFlows
    {

        public Guid RowGuid { get; set; }       
        public DateTime? DateHD { get; set; }
        public string DateHDString
        {
            get
            {
                if (DateHD != null)
                {
                    return (string)Convert.ToDateTime(DateHD).ToString("dd/MM/yyyy");
                }
                else
                {
                    return "";
                }
            }
        }
        public string Types { get; set; }
        public string TypesDT { get; set; }      
        public string Description { get; set; }
        public decimal? AmountOc { get; set; }
          
        public string Note { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }

    public partial class CashFlowsTotals
    {

        public int Id { get; set; }
        public decimal? TotalDTVaoDK { get; set; }
        public decimal? TotalDTRaDK { get; set; }
        public decimal? TotalDTDK { get; set; }
        public decimal? TotalSDTCDK { get; set; }
        public decimal? TotalTTTDK { get; set; }
        public decimal? TotalDTVaoTT { get; set; }
        public decimal? TotalDTRaTT { get; set; }
        public decimal? TotalDTTT { get; set; }
        public decimal? TotalSDTCTT { get; set; }
        public decimal? TotalTTTTT { get; set; }
    }
}
