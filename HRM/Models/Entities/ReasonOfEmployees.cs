﻿using System;
using System.Collections.Generic;

namespace ES_MODEL.HR
{
    public partial class ReasonOfEmployees
    {
        public int Id { get; set; }
        public int ReasonLeaveWorkId { get; set; }
        public Guid EmployeeGuid { get; set; }
        public int? OrderId { get; set; }
        public string Comment { get; set; }
    }
}
