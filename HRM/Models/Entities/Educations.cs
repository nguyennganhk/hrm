﻿using System;
using System.Collections.Generic;

namespace HRM.Models
{
    public partial class Educations
    {
        public int EducationId { get; set; }
        public string EducationName { get; set; }
        public Guid OrganizationGuid { get; set; }
        public string Description { get; set; }
        public bool? IsActive { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
    }
}
