﻿using System;
using System.Collections.Generic;

namespace ES_MODEL.HR
{
    public partial class SalaryTier
    {
        public string SalaryTierId { get; set; }
        public string SalaryTierName { get; set; }
        public Guid? OrganizationGuid { get; set; }
        public string Description { get; set; }
        public bool? IsActive { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
    }
}
