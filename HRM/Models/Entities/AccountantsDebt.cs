﻿using DocumentFormat.OpenXml.Office2013.PowerPoint.Roaming;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRM.Models
{ 
    public partial class AccountantsDebt
    {

        public Guid RowGuid { get; set; }
        public Guid OrganizationGuid { get; set; }
        public string CodeID { get; set; }      
        public DateTime? DateVB { get; set; }
        public string DateVBString
        {
            get
            {
                if (DateVB != null)
                {
                    return (string)Convert.ToDateTime(DateVB).ToString("dd/MM/yyyy");
                }
                else
                {
                    return "";
                }
            }
        }
        public string Title { get; set; }
        public string UnitID { get; set; }
        public string UnitName { get; set; }
        public decimal? Quantity { get; set; }
        public string StatusOrder { get; set; }
        public string EmployeeID { get; set; }
        public string EmployeeName { get; set; }
        public string Note { get; set; }     
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }

    }
    public partial class AccountantsDebtViews
    {
        [Key]
        public Guid RowGuid { get; set; }
        public string CodeID { get; set; }
        public DateTime? DateVB { get; set; }
        public string DateVBString
        {
            get
            {
                if (DateVB != null)
                {
                    return (string)Convert.ToDateTime(DateVB).ToString("dd/MM/yyyy");
                }
                else
                {
                    return "";
                }
            }
        }
        public string Title { get; set; }
        public string UnitID { get; set; }
        public string UnitName { get; set; }
        public decimal? Quantity { get; set; }
        public string StatusOrder { get; set; }
        public string EmployeeID { get; set; }
        public string EmployeeName { get; set; }
       
       
    }



}
