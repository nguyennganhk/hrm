﻿using DocumentFormat.OpenXml.Office2013.PowerPoint.Roaming;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRM.Models
{ 
    public partial class ExportDocuments
    {

        public Guid RowGuid { get; set; }
        public Guid? OrganizationGuid { get; set; }
        public string CodeID { get; set; }
        public string Title { get; set; }    
        public string ExportMarket { get; set; }
        public string ExportForm { get; set; }
        public string ExportFile { get; set; }
        public string Agencies { get; set; }
        public string EmployeeName { get; set; }
       
        public string Note { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }


}
