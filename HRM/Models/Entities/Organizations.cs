﻿using System;
using System.Collections.Generic;

namespace HRM.Models
{
    public partial class Organizations
    {
        public Organizations()
        {
            Employees = new HashSet<Employees>();
            Departments = new HashSet<Departments>();
        }
        public Guid OrganizationGuid { get; set; }
        public string OrganizationId { get; set; }
        public Guid? ParentId { get; set; }
        public string OrganizationName { get; set; }
        public string CountryId { get; set; }
        public string CountyName { get; set; }
        public string Description { get; set; }
        public int? SortOrder { get; set; }
        public bool? IsDependentBrach { get; set; }
        public string Address { get; set; }
        public string Tel { get; set; }
        public string Fax { get; set; }
        public string Web { get; set; }
        public string Email { get; set; }
        public string TaxCode { get; set; }
        public bool? IsActive { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string Representative { get; set; }
        public byte[] Photo { get; set; }
        public string ImagePath { get; set; }
        
        public ICollection<Employees> Employees { get; set; }
        public ICollection<Departments> Departments { get; set; }
    }
}
