﻿using System;
using System.Collections.Generic;

namespace HRM.Models
{
    public partial class Conditions
    {
        

        public Guid ConditionGuid { get; set; }
        public Guid WorkflowGuid { get; set; }
        public string ConditionData { get; set; }
        public string ConditionTrue { get; set; }
        public string ConditionFalse { get; set; }
        public Guid? LocationGuid { get; set; }
        public bool? WorkflowType { get; set; }
        public string StepCallback { get; set; }
        public string StepContinue { get; set; }
        public string ConditionUid { get; set; }     
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string WFStatusId { get; set; }
        public string WFStatusBackId { get; set; }
        public string WFConditionId { get; set; }
        public string WFConditionBackId { get; set; }
        public string PersonMain { get; set; }
        public string PersonSupport { get; set; }
        public string PersonFollow { get; set; }
        
    }
}
