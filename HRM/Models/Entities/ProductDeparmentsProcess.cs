﻿using DocumentFormat.OpenXml.Office2013.PowerPoint.Roaming;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRM.Models
{ 
    public partial class ProductDeparmentsProcess
    {

        public Guid RowGuid { get; set; }
        public string ProductID { get; set; }      
        public string ProductName { get; set; }
        public string Packing { get; set; }
        public string ProductProcess { get; set; }
        public string Preservation { get; set; }
        public string Note { get; set; }
        public string EmployeeName { get; set; }       
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }

    }
   
}
