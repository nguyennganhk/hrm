﻿using System;
using System.Collections.Generic;

namespace HRM.Models
{
    public partial class Concurrently
    {
        public Guid ConcurrentlyGuid { get; set; }
        public Guid EmployeeGuid { get; set; }
        public string EmployeeId { get; set; }
        public Guid OrganizationGuid { get; set; }
        public Guid DepartmentGuid { get; set; }
        public string DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public int? JobTitleId { get; set; }
        public string JobTitleName { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool? IsActive { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }

        public Departments Department { get; set; }
        public JobTitles JobTitle { get; set; }
    }
}
