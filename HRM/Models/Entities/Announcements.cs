﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace HRM.Models
{
    public partial class Announcements
    {
        [Key]
        public Guid AnnouncementGuid { get; set; }
        public Guid OrganizationGuid { get; set; }
        public Guid? DepartmentGuid { get; set; }
        public string DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public Guid EmployeeGuid { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Contents { get; set; }
        public string ExternalUrl { get; set; }
        public string IsPublic { get; set; }
        public string IsActive { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? DueDate { get; set; }
        
        public string Status { get; set; }
        public string Ispriority { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
    }
    public partial class AnnouncementsJtableView
    {
        [Key]
        public Guid AnnouncementGuid { get; set; }
        public Guid OrganizationGuid { get; set; }
        public string DepartmentName { get; set; }
        public string Title { get; set; }
        public string IsPublic { get; set; }
        public int? PermissEdit { get; set; }
        public DateTime? StartDate { get; set; }
        public string StartDateString
        {
            get
            {
                if (StartDate != null)
                {
                    return (string)Convert.ToDateTime(StartDate).ToString("dd/MM/yyyy");
                }
                else
                {
                    return "";
                }
            }
        }
        public DateTime? EndDate { get; set; }
        public string EndDateString
        {
            get
            {
                if (EndDate != null)
                {
                    return (string)Convert.ToDateTime(EndDate).ToString("dd/MM/yyyy");
                }
                else
                {
                    return "";
                }
            }
        }
        public string Status { get; set; }
        public string Ispriority { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedDateString
        {
            get
            {
                if (CreatedDate != null)
                {
                    return (string)Convert.ToDateTime(CreatedDate).ToString("dd/MM/yyyy");
                }
                else
                {
                    return "";
                }
            }
        }
        public string FullName { get; set; }
        public int? CountComment { get; set; }
        public bool? IsRead { get; set; }
    }
    public partial class ListEmpAnnoucementGetByid
    {
        [Key]
        public Guid? RecordGuid { get; set; }
        public Guid? EmployeeGuid { get; set; }
        public string EmployeeName { get; set; }
        public Guid? AnnouncementGuid { get; set; }
    }
    public partial class ListEmpAnnouncement
    {
        public Guid? RecordGuid { get; set; }
        public Guid? EmployeeGuid { get; set; }
        public string EmployeeName { get; set; }
        public Guid? AnnouncementGuid { get; set; }
    }
}
