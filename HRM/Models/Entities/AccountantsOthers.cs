﻿using DocumentFormat.OpenXml.Office2013.PowerPoint.Roaming;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRM.Models
{ 
    public partial class AccountantsOthers
    {

        public Guid RowGuid { get; set; }
        public Guid OrganizationGuid { get; set; }
        public string CodeID { get; set; }
        public string OrderID { get; set; }
        public string Title { get; set; }
        public string UnitID { get; set; }
        public string UnitName { get; set; }

        public decimal? Quantity { get; set; }
        public decimal? UnitPrice { get; set; }
        public decimal? AmountOc { get; set; }
        public string EmployeeID { get; set; }
        public string EmployeeApprove { get; set; }
        public string EmployeeName { get; set; }
       
        public string Note { get; set; }
      
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public DateTime? DatePS { get; set; }
        public string DatePSString
        {
            get
            {
                if (DatePS != null)
                {
                    return (string)Convert.ToDateTime(DatePS).ToString("dd/MM/yyyy");
                }
                else
                {
                    return "";
                }
            }
        }


    }
    public partial class AccountantsOthersViews
    {
        [Key]
        public Guid RowGuid { get; set; }
        public string CodeID { get; set; }
        public string OrderID { get; set; }
        public string Title { get; set; }
        public string UnitID { get; set; }
        public string UnitName { get; set; }

        public decimal? Quantity { get; set; }
        public decimal? UnitPrice { get; set; }
        public decimal? AmountOc { get; set; }
        public string EmployeeID { get; set; }
        public string EmployeeApprove { get; set; }
        public string EmployeeName { get; set; }

        public string Note { get; set; }


    }



}
