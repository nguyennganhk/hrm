﻿using System;
using System.Collections.Generic;

namespace HRM.Models
{
    public partial class ShiftOfEmployees
    {
        public int Id { get; set; }
        public int ShiftId { get; set; }
        public Guid EmployeeGuid { get; set; }
    }
}
