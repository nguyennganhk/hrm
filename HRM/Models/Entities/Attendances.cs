﻿using System;
using System.Collections.Generic;

namespace HRM.Models
{
    public partial class Attendances
    {
        public Guid RowGuid { get; set; }
        public Guid EmployeeGuid { get; set; }
        public string EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public Guid DepartmentGuid { get; set; }
        public string DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public DateTime Date { get; set; }
        public TimeSpan StartTime { get; set; }
        public TimeSpan EndTime { get; set; }
        public int? ShiftId { get; set; }
        public string ShiftName { get; set; }
        public string Status { get; set; }
        public bool? IsUpdated { get; set; }
        public bool? Hrconfirm { get; set; }
        public string ImageUrl { get; set; }
        public string AttendanceBy { get; set; }
        public string Comment { get; set; }
        public string JobTitle { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public bool? CheckAprove { get; set; }
    }
}
