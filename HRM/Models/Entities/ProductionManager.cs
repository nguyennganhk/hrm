﻿using DocumentFormat.OpenXml.Office2013.PowerPoint.Roaming;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRM.Models
{ 
    public partial class ProductionManager
    {

        public Guid RowGuid { get; set; }
        public Guid OrganizationGuid { get; set; }
        public string CodeID { get; set; }
        public DateTime? DateSX { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public string Title { get; set; }
        public string EmployeeID { get; set; }
        public string EmployeeName { get; set; }
        public string CustomerID { get; set; }
        public string CustomerName { get; set; }

        public decimal? TotalMoney { get; set; }
        public string Status { get; set; }
        public string IsApprove { get; set; }
        public bool? IsAccountant { get; set; }
        public bool? IsManager { get; set; }

        public string ConditionValue { get; set; }
        public Guid? WorkFlowGuid { get; set; }
        public string WorkFlowsContent { get; set; }
        public Guid? WorkFlowCurrent { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }       
        public string Note { get; set; }
      

    }
    public partial class ProductionManagerViews
    {
        [Key]
        public Guid RowGuid { get; set; }
        public string CodeID { get; set; }
        public DateTime? DateSX { get; set; }
        public string DateSXString
        {
            get
            {
                if (DateSX != null)
                {
                    return (string)Convert.ToDateTime(DateSX).ToString("dd/MM/yyyy");
                }
                else
                {
                    return "";
                }
            }
        }
        public DateTime? EffectiveDate { get; set; }
        public string EffectiveDatetring
        {
            get
            {
                if (EffectiveDate != null)
                {
                    return (string)Convert.ToDateTime(EffectiveDate).ToString("dd/MM/yyyy");
                }
                else
                {
                    return "";
                }
            }
        }
        public string Title { get; set; }
        public string EmployeeID { get; set; }
        public string EmployeeName { get; set; }
        public string CustomerID { get; set; }
        public string CustomerName { get; set; }
        public decimal? TotalMoney { get; set; }       
        public string IsApprove { get; set; }
        public string StatusWF { get; set; }
        public int? Permisstion { get; set; }
        public int? CountComment { get; set; }
       
    }



}
