﻿using DocumentFormat.OpenXml.Office2013.PowerPoint.Roaming;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRM.Models
{ 
    public partial class Accountants
    {

        public Guid RowGuid { get; set; }
        public Guid OrganizationGuid { get; set; }
        public string CodeID { get; set; }      
        public string StatusAcc { get; set; }
        public DateTime? DateSX { get; set; }
        public string EmployeeRequire { get; set; }
        public string EmployeeID { get; set; }
        public string EmployeeName { get; set; }
        public string Note { get; set; }      
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }

    }
    public partial class AccountantsViews
    {
        [Key]
        public Guid RowGuid { get; set; }
        public string CodeID { get; set; }
        public string StatusAcc { get; set; }        
        public DateTime? DateSX { get; set; }
        public string DateSXString
        {
            get
            {
                if (DateSX != null)
                {
                    return (string)Convert.ToDateTime(DateSX).ToString("dd/MM/yyyy");
                }
                else
                {
                    return "";
                }
            }
        }
        public string EmployeeRequire { get; set; }
        public string EmployeeID { get; set; }
        public string EmployeeName { get; set; }
        public string Note { get; set; }       
        public int? CountComment { get; set; }     
    }



}
