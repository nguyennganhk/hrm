﻿using System;
using System.Collections.Generic;

namespace HRM.Models
{
    public partial class Workflows
    {     
        public Guid WorkflowGuid { get; set; }
        public string WorkflowId { get; set; }
        public string WorkflowName { get; set; }
        public Guid? OrganizationGuid { get; set; }
        public string ModuleId { get; set; }
        public string ModuleName { get; set; }
        public string WorkFlowData { get; set; }
        public Guid? TableGuid { get; set; }
        public string TableName { get; set; }
        public bool? IsActive { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }

       
    }
}
