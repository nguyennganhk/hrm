﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HRM.Models
{
    public partial class Profestionals
    {
        [Key]
        public int ProfestionalId { get; set; }
        public string ProfestionaName { get; set; }
        public Guid OrganizationGuid { get; set; }
        public string Description { get; set; }
        public bool? IsActive { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
    }
}
