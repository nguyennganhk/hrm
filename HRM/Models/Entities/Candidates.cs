﻿using System;
using System.Collections.Generic;

namespace HRM.Models
{
    public partial class Candidates
    {
        public Guid CandidateGuid { get; set; }
        public Guid OrganizationGuid { get; set; }
        public string CandidateId { get; set; }
        public Guid? RecruitmentGuid { get; set; }
        public string RecruitmentId { get; set; }
        public Guid? DepartmentGuid { get; set; }
        public string DepartmentId { get; set; }
        public string FullName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Identification { get; set; }
        public DateTime? IssueDate { get; set; }
        public DateTime BirthDate { get; set; }
        public string PlaceOfBirth { get; set; }
        public string MaritalStatus { get; set; }
        public string Gender { get; set; }
        public string PermanentAddress { get; set; }
        public string CurrentAddress { get; set; }
        public string CountryId { get; set; }
        public string CountryName { get; set; }
        public string ProvinceId { get; set; }
        public string ProvinceName { get; set; }
        public string DistrictId { get; set; }
        public string DistrictName { get; set; }
        public string WardId { get; set; }
        public string WardName { get; set; }
        public string HomeNumber { get; set; }
        public int? EthnicId { get; set; }
        public string EthnicName { get; set; }
        public string HomePhone { get; set; }
        public string HomeMobile { get; set; }
        public string HomeEmail { get; set; }
        public string WorkPhone { get; set; }
        public string WorkMobile { get; set; }
        public string WorkEmail { get; set; }
        public int? EducationId { get; set; }
        public string EducationName { get; set; }
        public string ImagePath { get; set; }
        public byte[] Photo { get; set; }
        public string TaxCode { get; set; }
        public DateTime? ApplyDate { get; set; }
        public string Channel { get; set; }
        public string Note { get; set; }
        public string AssignedTo { get; set; }
        public string Headship { get; set; }
        public string Approver { get; set; }
        public string Action { get; set; }
        public string Request { get; set; }
        public string FilterResult { get; set; }
        public string ExamResult { get; set; }
        public string Status { get; set; }
        public string Specialized { get; set; }
        public string RejectReason { get; set; }
        public string Referrer { get; set; }
        public DateTime? StartDate { get; set; }
        public decimal? OldSalary { get; set; }
        public decimal? DesiredSalary { get; set; }
        public string ReasonNotInterview { get; set; }
        public string Experience { get; set; }
        public DateTime? InterviewDate { get; set; }
        public string InterviewStaff { get; set; }
        public bool? IsDraft { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? WorkFlowGuid { get; set; }
        public bool? IsLocked { get; set; }
        public int? PositionId { get; set; }
    }
}
