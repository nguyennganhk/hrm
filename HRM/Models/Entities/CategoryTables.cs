﻿using System;
using System.Collections.Generic;

namespace HRM.Models
{
    public partial class CategoryTables
    {
        public CategoryTables()
        {
            CategoryColumns = new HashSet<CategoryColumns>();
            Workflows = new HashSet<Workflows>();
        }

        public Guid TableGuid { get; set; }
        public Guid? OrganizationGuid { get; set; }
        public string TableId { get; set; }
        public string TableName { get; set; }
        public string TableType { get; set; }
        public string ModuleId { get; set; }
        public string ModuleName { get; set; }
        public bool? IsActive { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }

        public ICollection<CategoryColumns> CategoryColumns { get; set; }
        public ICollection<Workflows> Workflows { get; set; }
    }
}
