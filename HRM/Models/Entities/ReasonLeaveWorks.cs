﻿using System;
using System.Collections.Generic;

namespace ES_MODEL.HR
{
    public partial class ReasonLeaveWorks
    {
        public int ReasonLeaveWorkId { get; set; }
        public string ReasonName { get; set; }
        public string Description { get; set; }
        public bool? IsActive { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
