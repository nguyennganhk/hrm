﻿using DocumentFormat.OpenXml.Office2013.PowerPoint.Roaming;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRM.Models
{ 
    public partial class ProductionMaterialDetails
    {

        public Guid RowGuid { get; set; }
        public Guid? RecordGuid { get; set; }
        public string ItemID { get; set; }
        public string ItemName { get; set; }
        public string Title { get; set; }
        public string UnitID { get; set; }
        public string UnitName { get; set; }

        public decimal? Quantity { get; set; }
        public decimal? UnitPrice { get; set; }
        public decimal? AmountOc { get; set; }
        public decimal? VATPercent { get; set; }
        public decimal? VATAmountOC { get; set; }
        public decimal? AmountNoVAT { get; set; }
        public decimal? AmountVAT { get; set; }
        public string Note { get; set; }
        public string Packings { get; set; }
        public string ProductSpecificate { get; set; }
        public string SupplierID { get; set; }
        public string SupplierName { get; set; }
        public DateTime? ImportDate { get; set; }
        public string PaymentsType { get; set; }
        public string PlaceOfDelivery { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? SortOrder { get; set; }

    }

}
