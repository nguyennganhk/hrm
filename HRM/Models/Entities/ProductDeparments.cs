﻿using DocumentFormat.OpenXml.Office2013.PowerPoint.Roaming;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRM.Models
{ 
    public partial class ProductDeparments
    {

        public Guid RowGuid { get; set; }
        public Guid OrganizationGuid { get; set; }
        public string OrderID { get; set; }      
        public string CustomerID { get; set; }
        public string CustomerName { get; set; }
        public DateTime? DeliveryDate { get; set; }
        public string Feedback { get; set; }
        public string EmployeeID { get; set; }
        public string EmployeeName { get; set; }
        public string Note { get; set; }

        public string ShippingStatus { get; set; }
        public string OrderStatus { get; set; }
        public string EmployeeKSC { get; set; }  
        public string NoteKSC { get; set; }      
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }

    }
    public partial class ProductDeparmentViews
    {
        [Key]
        public Guid RowGuid { get; set; }
        public string OrderID { get; set; }
        public string CustomerID { get; set; }
        public string CustomerName { get; set; }
        
        public DateTime? DeliveryDate { get; set; }
        public string DeliveryDateString
        {
            get
            {
                if (DeliveryDate != null)
                {
                    return (string)Convert.ToDateTime(DeliveryDate).ToString("dd/MM/yyyy");
                }
                else
                {
                    return "";
                }
            }
        }
        public string Feedback { get; set; }
        public string EmployeeID { get; set; }
        public string EmployeeName { get; set; }
        public string Note { get; set; }       
        public int? CountComment { get; set; }     
    }



}
