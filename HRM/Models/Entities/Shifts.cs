﻿using System;
using System.Collections.Generic;

namespace HRM.Models
{
    public partial class Shifts
    {
        public int ShiftId { get; set; }
        public string ShiftName { get; set; }
        public Guid? OrganizationGuid { get; set; }
        public TimeSpan? StartTime { get; set; }
        public TimeSpan? EndTime { get; set; }
        public decimal? Allowance { get; set; }
        public string Description { get; set; }
        public bool? IsActive { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public string Type { get; set; }
        public int? Rested { get; set; }
        public string ShiftCode { get; set; }
        public double? WorkDay { get; set; }
        public double? WorkWeekend { get; set; }
        public double? WorkHoliday { get; set; }
        public double? OvertimeDay { get; set; }
        public double? OvertimeWeekend { get; set; }
        public double? OvertimeHoliday { get; set; }
    }
}
