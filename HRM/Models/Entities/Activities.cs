﻿using System;
using System.Collections.Generic;

namespace HRM.Models
{
    public partial class Activities
    {
            public Guid ActivityGuid { get; set; }
            public Guid? ConditionGuid { get; set; }
            public Guid? ConditionGuidOfStep { get; set; }
            public Guid? WorkflowGuid { get; set; }
            public string ActivityName { get; set; }
            public bool? IsActive { get; set; }
            public Guid? ParentId { get; set; }
            public Guid? Location { get; set; }
            public bool? StartStep { get; set; }
            public bool? IsValid { get; set; }
            public string ActivityUid { get; set; }
            public DateTime? CreatedDate { get; set; }
            public string CreatedBy { get; set; }
            public DateTime? ModifiedDate { get; set; }
            public string ModifiedBy { get; set; }

           
    }
}
