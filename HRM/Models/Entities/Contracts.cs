﻿using System;
using System.Collections.Generic;

namespace HRM.Models
{
    public partial class Contracts
    {
        public int Id { get; set; }
        public Guid ContractGuid { get; set; }
        public string ContractNo { get; set; }
        public string Title { get; set; }
        public Guid? EmployeeGuid { get; set; }
        public string EmployeeId { get; set; }
        public Guid? DepartmentGuid { get; set; }
        public string DepartmentId { get; set; }
        public Guid? OrganizationGuid { get; set; }
        public decimal? Salary { get; set; }
        public DateTime? ContractDate { get; set; }
        public string ContractDateString
        {
            get
            {
                if (ContractDate != null)
                {
                    return (string)Convert.ToDateTime(ContractDate).ToString("dd/MM/yyyy");
                }
                else
                {
                    return "";
                }
            }
        }
        public DateTime? ContractExpired { get; set; }
        public string ContractExpiredString
        {
            get
            {
                if (ContractExpired != null)
                {
                    return (string)Convert.ToDateTime(ContractExpired).ToString("dd/MM/yyyy");
                }
                else
                {
                    return "";
                }
            }
        }
        public int? ContractType { get; set; }
        public string Description { get; set; }
        public string Body { get; set; }
        public bool? IsActive { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
    }
}
