﻿using System;
using System.Collections.Generic;

namespace HRM.Models
{
    public partial class Rewards
    {
        public Guid RewardGuid { get; set; }
        public string Type { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public Guid? EmployeeGuid { get; set; }
        public string EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public Guid OrganizationGuid { get; set; }
        public Guid? DepartmentGuid { get; set; }
        public string DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public DateTime? Date { get; set; }
        public string RewardLevel { get; set; }
        public string Forms { get; set; }
        public string Reason { get; set; }
        public bool? IsLocked { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
    }
}
