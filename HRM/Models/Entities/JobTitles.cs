﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HRM.Models
{
    public partial class JobTitles
    {
        public JobTitles()
        {
            Concurrently = new HashSet<Concurrently>();
        }
        public int JobTitleId { get; set; }
        public string CategoryId { get; set; }
        public string JobTitleName { get; set; }
        public Guid OrganizationGuid { get; set; }
        public string Description { get; set; }
        public bool? IsActive { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }

        public ICollection<Concurrently> Concurrently { get; set; }
    }
}
