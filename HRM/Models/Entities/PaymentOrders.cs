﻿using DocumentFormat.OpenXml.Office2013.PowerPoint.Roaming;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRM.Models
{ 
    public partial class PaymentOrders
    {

        public Guid RowGuid { get; set; }
        public Guid? OrganizationGuid { get; set; }
        public Guid? RecordGuid { get; set; }      
        public DateTime? PaymentDate { get; set; }
        public decimal? PaymentMoney { get; set; }
        public decimal? PaymentPercent { get; set; }      
        public string MoneyType { get; set; }
        public string UNC { get; set; }
      
        public int? SortOrder { get; set; }
        public string Note { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }


}
