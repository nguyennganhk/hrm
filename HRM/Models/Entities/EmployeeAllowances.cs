﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRM.Models
{
    public partial class EmployeeAllowances
    {
        [Key]
        public int AllowanceId { get; set; }
        public Guid AllowanceGuid { get; set; }
        public string AllowanceName { get; set; }
        public Guid EmployeeGuid { get; set; }
        public string EmployeeId { get; set; }
        public Guid? DepartmentGuid { get; set; }
        public string DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public decimal? Value { get; set; }
        public bool? Type { get; set; }
        public bool? IsActive { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
       
    }
}
