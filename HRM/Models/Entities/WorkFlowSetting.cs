﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HRM.Models
{
    public partial class WorkFlowSetting
    {
        public Guid RowGuid { get; set; }
        public Guid? WorkFlowGuid { get; set; }
        public string WorkFlowName { get; set; }
        public string TicketId { get; set; }
        public string TicketName { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
    }
}
