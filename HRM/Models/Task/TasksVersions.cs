﻿using System;
using System.Collections.Generic;

namespace ES_MODEL.Task
{
    public partial class TasksVersions
    {
        public Guid TaskVersionGuid { get; set; }
        public int TaskVersionId { get; set; }
        public Guid TaskGuid { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int VersionNumber { get; set; }
        public bool? IsActive { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
