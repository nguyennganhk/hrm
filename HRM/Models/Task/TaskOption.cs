﻿using System;
using System.Collections.Generic;

namespace ES_MODEL.Task
{
    public partial class TaskOptions
    {
        public Guid TaskOptionGuid { get; set; }
        public int? ProjectItemId { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public string CategoryTeamId { get; set; }
        public decimal? HoursWorked { get; set; }
        public bool? IsTaskReport { get; set; }
        public int? RequestTiketDetailId { get; set; }
        public int? QuantityRequired { get; set; }
        public bool? IsIssueExist { get; set; }
        public int? TaskRequestDetailId { get; set; }
        public DateTime? PlanStartDate { get; set; }
        public int? MemberNumber { get; set; }
        public int? PlanMemberNumber { get; set; }
        public int? PlanQuantityRequired { get; set; }
        public bool? IsConfirm { get; set; }
        public string WarrantyTicketNo { get; set; }
        public decimal? WarrantyCost { get; set; }
        public Guid TaskGuid { get; set; }
        public string OrderNumber { get; set; }
        public string QuotationNo { get; set; }
        public string TaskRequestNo { get; set; }

        public Tasks TaskGu { get; set; }
    }
}
