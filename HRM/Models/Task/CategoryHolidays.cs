﻿using System;
using System.Collections.Generic;

namespace ES_MODEL.Task
{
    public partial class CategoryHolidays
    {
        public int Id { get; set; }
        public Guid? OrganizationGuid { get; set; }
        public string Title { get; set; }
        public DateTime? Date { get; set; }
        public int? TotalHoliday { get; set; }
        public string HolidayType { get; set; }
        public string Description { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
