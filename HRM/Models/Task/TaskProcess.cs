﻿using System;
using System.Collections.Generic;

namespace ES_MODEL.Task
{
    public partial class TaskProcess
    {
        public TaskProcess()
        {
            CategoryOfProcess = new HashSet<CategoryOfProcess>();
            TaskOfProcess = new HashSet<TaskOfProcess>();
        }

        public Guid RowGuid { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string CreatedBy { get; set; }
        public bool? IsActive { get; set; }
        public Guid? OrganizationGuid { get; set; }
        public string Class { get; set; }

        public ICollection<CategoryOfProcess> CategoryOfProcess { get; set; }
        public ICollection<TaskOfProcess> TaskOfProcess { get; set; }
    }
}
