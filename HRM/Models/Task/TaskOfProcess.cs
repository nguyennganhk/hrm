﻿using System;
using System.Collections.Generic;

namespace ES_MODEL.Task
{
    public partial class TaskOfProcess
    {
        public Guid RowGuid { get; set; }
        public Guid? TaskGuid { get; set; }
        public Guid? TaskProcessGuid { get; set; }
        public bool? IsActive { get; set; }
        public int? OrderId { get; set; }

        public Tasks TaskGu { get; set; }
        public TaskProcess TaskProcessGu { get; set; }
    }
}
