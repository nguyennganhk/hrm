﻿using System;
using System.Collections.Generic;

namespace ES_MODEL.Task
{
    public partial class Profiles
    {
        public Profiles()
        {
            InverseParent = new HashSet<Profiles>();
            TaskOfProfiles = new HashSet<TaskOfProfiles>();
        }

        public Guid ProfileGuid { get; set; }
        public Guid? ParentId { get; set; }
        public string Title { get; set; }
        public string LoginName { get; set; }
        public Guid? EmployeeGuid { get; set; }
        public Guid? OrganizationGuid { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
        public bool? IsPublic { get; set; }

        public Profiles Parent { get; set; }
        public ICollection<Profiles> InverseParent { get; set; }
        public ICollection<TaskOfProfiles> TaskOfProfiles { get; set; }
    }
}
