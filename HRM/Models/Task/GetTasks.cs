﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;
using System.Text;

namespace ES_MODEL.Task.Custom
{
    public partial class GetTasks : _GetTasks
    {
        public List<EmpJoinCustom> EmpJoin
        {
            get
            {
                if (this._EmpJoin != null && this._EmpJoin != "")
                {
                    return JsonConvert.DeserializeObject<List<EmpJoinCustom>>(this._EmpJoin);
                }
                return new List<EmpJoinCustom>();
            }
        }
        public List<CheckListsCustom> CheckLists
        {
            get
            {
                if (this._CheckLists != null && this._CheckLists != "")
                {
                    return JsonConvert.DeserializeObject<List<CheckListsCustom>>(this._CheckLists);
                }
                return new List<CheckListsCustom>();
            }
        }
        public EmpPermissionCustom EmpPermissionCustom
        {
            get
            {
                if (this._EmpPermissionCustom != null && this._EmpPermissionCustom != "")
                {
                    return JsonConvert.DeserializeObject<List<EmpPermissionCustom>>(this._EmpPermissionCustom)[0];
                }
                return null;
            }
        }
        public TaskStepByStepNow TaskStepByStepNow
        {
            get
            {
                if (this._TaskStepByStepNow != null && this._TaskStepByStepNow != "")
                {
                    return JsonConvert.DeserializeObject<List<TaskStepByStepNow>>(this._TaskStepByStepNow)[0];
                }
                return null;
            }
        }
    }
    public class EmpJoinCustom
    {
        [Key]
        public Guid RecordGuid { get; set; }
        public Guid? EmployeeGuid { get; set; }
        public string IsOwner { get; set; }
        public string LoginName { get; set; }
        public string EmployeeId { get; set; }
        public string DepartmentId { get; set; }
        public string EmployeeName { get; set; }
        public string DepartmentName { get; set; }
        public float? PercentComplete { get; set; }
        public string StatusCompleted { get; set; }
        public string ProcessAccept { get; set; }
    }
    public class TaskStepByStepNow
    {
        [Key]
        public Guid TaskStepByStepGuid { get; set; }
    }
    public class EmpPermissionCustom
    {
        public string Alias { get; set; }
        public bool? IsAdd { get; set; }
        public bool? IsUpdate { get; set; }
        public bool? IsDelete { get; set; }
        public bool? IsApproval { get; set; }
        public bool? IsForward { get; set; }
        public bool? IsComment { get; set; }
        public bool? IsView { get; set; }
        public bool? IsCheck { get; set; }
        public bool? IsFullControl { get; set; }
        public int? LocationId { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDisable { get; set; }
    }
    public class CheckListsCustom
    {
        [Key]
        public Guid RecordGuid { get; set; }
        public Guid? CheckListGuid { get; set; }
        public bool? Status { get; set; }
        public string CheckListTitle { get; set; }
    }
    public partial class _GetTasks
    {
        [Key]
        public Guid TaskGuid { get; set; }
        public string Subject { get; set; }
        public Guid? TaskOfUserGuid { get; set; }
        public string TaskContent { get; set; }
        public string Priority { get; set; }
        public Guid? ParentId { get; set; }
        public int? CheckChild { get; set; }
        public string _EmpJoin { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public double? PercentComplete { get; set; }
        public double? PercentCompleteTaskOfUser { get; set; }
        public int? CountComment { get; set; }
        public string StatusCompleted { get; set; }
        public string StatusCompletedTaskOfUser { get; set; }
        public string StatusIsOwnerTaskOfUser { get; set; }
        public DateTime? TimeReminderTaskOfUser { get; set; }
        
        public string CreatedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? IsHasCalendar { get; set; }
        public bool? IsRead { get; set; }
        public bool? IsFollow { get; set; }
        public int? CountAtt { get; set; }
        public string LoginName { get; set; }
        public Guid? CategoryOfTaskGuid { get; set; }
        public string _CheckLists { get; set; }
        public string _EmpPermissionCustom { get; set; }
        public Guid? TaskProcess { get; set; }
        public string DepartmentName { get; set; }
        public string CategoryName { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string IsAcceptTask { get; set; }
        public bool? IsArchive { get; set; }
        public bool? ProcessDelete { get; set; }
        public bool? IsRecall { get; set; }
        public string _TaskStepByStepNow { get; set; }
    }
    public partial class LoadCheckList
    {
        [Key]
        public Guid CheckListGuid { get; set; }
        public string CheckListTitle { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public bool? Status { get; set; }
        public int? OrderId { get; set; }
    }
    public partial class TaskAttachmentViews
    {
        [Key]
        public Guid AttachmentGuid { get; set; }
        public string Title { get; set; }
        public string FileName { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? FileSize { get; set; }
        public string FileExtension { get; set; }
    }
    public partial class CheckPermission
    {
        [Key]
        public Guid? TaskOfUserGuid { get; set; }
        public Guid? TaskGuid { get; set; }
        public string EmployeeId { get; set; }
        public CheckPermission_Permission Permission
        {
            get
            {
                if (this._Permission == null || this._Permission == "")
                {
                    return null;
                }
                return JsonConvert.DeserializeObject<List<CheckPermission_Permission>>(this._Permission)[0];
            }
        }
        public string _Permission { get; set; }
        public string IsCompleted { get; set; }
        public bool? IsRead { get; set; }
    }
    public partial class CheckPermission_Permission
    {
        [Key]
        public Guid PermissionGuid { get; set; }
        public bool? IsAdd { get; set; }
        public bool? IsUpdate { get; set; }
        public bool? IsDelete { get; set; }
        public bool? IsApproval { get; set; }
        public bool? IsForward { get; set; }
        public bool? IsComment { get; set; }
        public bool? IsView { get; set; }
        public bool? IsFullControl { get; set; }
        public bool? IsCheck { get; set; }
        public bool? IsDisable { get; set; }
        public bool? IsRecall { get; set; }
    }
    public class ActivitiesCustom
    {
        [Key]
        public Guid TaskActivityGuid { get; set; }
        public string ActivityId { get; set; }
        public string Subject { get; set; }
        public string ActivityContent { get; set; }
        public string EmployeeName { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
    }
    public class TaskProcessCustomView
    {
        [Key]
        public Guid RowGuid { get; set; }
        public Guid? TaskOfProcessGuid { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string CreatedBy { get; set; }
        public bool? IsActive { get; set; }
        public int? OrderId { get; set; }
        public string Class { get; set; }
        public string TaskMapProcess { get; set; }
    }
    public class TaskAttachbyType
    {
        [Key]
        public Guid RowGuid { get; set; }
        public string TypeExtension { get; set; }
        public string Value { get; set; }
    }
}
