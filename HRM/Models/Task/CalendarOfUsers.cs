﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ES_MODEL.Task
{
    public partial class CalendarOfUsers
    {
        public Guid CalendarOfUsersGuid { get; set; }
        public Guid TaskOfUsersGuid { get; set; }
        public Guid CalendarGuid { get; set; }

        public Calendars CalendarGu { get; set; }
        public TaskOfUsers TaskOfUsersGu { get; set; }
    }
}
