﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ES_MODEL.Task
{
    public partial class CategoryShare
    {
        public Guid CategoryShareGuid { get; set; }
        public Guid? DepartmentGuid { get; set; }
        public string DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public Guid? OrganizationGuid { get; set; }
        public Guid? CategoryOfTaskGuid { get; set; }

        public CategoryOfTasks CategoryOfTaskGu { get; set; }
    }
}
