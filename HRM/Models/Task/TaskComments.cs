﻿using System;
using System.Collections.Generic;

namespace ES_MODEL.Task
{
    public partial class TaskComments
    {
        public TaskComments()
        {
            InverseParent = new HashSet<TaskComments>();
            TaskCommentAttachments = new HashSet<TaskCommentAttachments>();
        }
        public Guid CommentGuid { get; set; }
        public Guid? ParentGuid { get; set; }
        public Guid RecordGuid { get; set; }
        public Guid OrganizationGuid { get; set; }
        public string Comment { get; set; }
        public Guid EmployeeGuid { get; set; }
        public string EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public string LoginName { get; set; }
        public Guid? DepartmentGuid { get; set; }
        public string DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public string AvatarUrl { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string DataAttachment { get; set; }
        public string Type { get; set; }

        public Tasks Tasks { get; set; }
        public TaskComments Parent { get; set; }
        public ICollection<TaskComments> InverseParent { get; set; }
        public ICollection<TaskCommentAttachments> TaskCommentAttachments { get; set; }
    }
}
