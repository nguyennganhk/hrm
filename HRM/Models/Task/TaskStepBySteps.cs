﻿using System;
using System.Collections.Generic;

namespace ES_MODEL.Task
{
    public partial class TaskStepBySteps
    {
        public TaskStepBySteps()
        {
            TaskOfTaskSteps = new HashSet<TaskOfTaskSteps>();
            TaskOfUserOfTaskSteps = new HashSet<TaskOfUserOfTaskSteps>();
            TaskStepStatus = new HashSet<TaskStepStatus>();
        }

        public Guid RecordGuid { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public Guid? Guid { get; set; }
        public string ColorText { get; set; }
        public string ColorBackground { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public Guid? OrganizationGuid { get; set; }
        public Guid? CategoryOfTaskGuid { get; set; }
        public int? Sort { get; set; }

        public CategoryOfTasks CategoryOfTaskGu { get; set; }
        public ICollection<TaskOfTaskSteps> TaskOfTaskSteps { get; set; }
        public ICollection<TaskOfUserOfTaskSteps> TaskOfUserOfTaskSteps { get; set; }
        public ICollection<TaskStepStatus> TaskStepStatus { get; set; }
    }
}
