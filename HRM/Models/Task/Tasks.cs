﻿using ES_MODEL.Task.Custom;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ES_MODEL.Task
{
    public partial class Tasks
    {
        public Tasks()
        {
            InverseParent = new HashSet<Tasks>();
            TaskCheckLists = new HashSet<TaskCheckLists>();
            TaskOfLabels = new HashSet<TaskOfLabels>();
            TaskOfProcess = new HashSet<TaskOfProcess>();
            TaskOfRecurrences = new HashSet<TaskOfRecurrences>();
            TaskOfUsers = new HashSet<TaskOfUsers>();
            TaskOptions = new HashSet<TaskOptions>();
            #region custom
            TaskAttachments = new HashSet<TaskAttachments>();
            TaskComments = new HashSet<TaskComments>();
            TaskActivities = new HashSet<TaskActivities>();
            #endregion
        }

        public Guid TaskGuid { get; set; }
        public Guid? ParentId { get; set; }
        public Guid? ProjectGuid { get; set; }
        public string ProjectId { get; set; }
        public Guid OrganizationGuid { get; set; }
        public Guid DepartmentGuid { get; set; }
        public string DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public Guid? EmployeeGuid { get; set; }
        public string EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public Guid? CustomerGuid { get; set; }
        public string CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string Subject { get; set; }
        public string TaskNumber { get; set; }
        public string TaskContent { get; set; }
        public DateTime? StartDate { get; set; }
        public TimeSpan? StartTime { get; set; }
        public DateTime? EndDate { get; set; }
        public TimeSpan? EndTime { get; set; }
        public DateTime? CompleteDate { get; set; }
        public string Priority { get; set; }
        public string SecurityLevel { get; set; }
        public double? PercentComplete { get; set; }
        public DateTime? TimeReminder { get; set; }
        public string StatusCompleted { get; set; }
        public string Description { get; set; }
        public string Note { get; set; }
        public string Approval { get; set; }
        public string IsApproval { get; set; }
        public string ApprovalStatus { get; set; }
        public bool? IsRecall { get; set; }
        public string TaskLevel { get; set; }
        public double? Mark { get; set; }
        public double? Kpiscore { get; set; }
        public Guid? CategoryOfTaskGuid { get; set; }
        public int? IsActive { get; set; }
        public bool? IsDraft { get; set; }
        public bool? IsLocked { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string LoginName { get; set; }
        public bool? IsArchive { get; set; }
        public Guid? TaskProcess { get; set; }
        public int? AttachCount { get; set; }
        public Guid? RecordGuid { get; set; }
        public string CompaignId { get; set; }
        public bool? ProcessDelete { get; set; }
        public DateTime? FinishDay { get; set; }

        public CategoryOfTasks CategoryOfTaskGu { get; set; }
        public Tasks Parent { get; set; }
        public ICollection<Tasks> InverseParent { get; set; }
        public ICollection<TaskCheckLists> TaskCheckLists { get; set; }
        public ICollection<TaskOfLabels> TaskOfLabels { get; set; }
        public ICollection<TaskOfProcess> TaskOfProcess { get; set; }
        public ICollection<TaskOfRecurrences> TaskOfRecurrences { get; set; }
        public ICollection<TaskOfUsers> TaskOfUsers { get; set; }
        public ICollection<TaskOptions> TaskOptions { get; set; }
        public ICollection<TaskOfTaskSteps> TaskOfTaskSteps { get; set; }
        #region custom 
        public ICollection<TaskComments> TaskComments { get; set; }
        public ICollection<TaskAttachments> TaskAttachments { get; set; }
        public ICollection<TaskActivities> TaskActivities { get; set; }
        #endregion
    }
    public class CommentPaging
    {
        public Guid RecordGuid { get; set; }
        public int Take { get; set; }
        public int Skip { get; set; }
        public List<string> GroupUser { get; set; }
    }
    public class TaskCommentAttachmentsView
    {
        [Key]
        public Guid AttachmentGuid { get; set; }
        public string FileExtension { get; set; }
        public Guid RecordGuid { get; set; }
        public string FileName { get; set; }
    }
    public class TaskCommentView
    {
        [Key]
        public Guid CommentGuid { get; set; }
        public Guid ParentGuid { get; set; }
        public string Comment { get; set; }
        public string EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public Guid? EmployeeGuid { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string AvatarUrl { get; set; }
        public int? AttCount { get; set; }
        public ICollection<TaskCommentAttachmentsView> TaskCommentAttachments { get; set; }
    }
    public partial class TaskLabelCustoms
    {
        [Key]
        public int LabelId { get; set; }
        public string LabelName { get; set; }
        public string Class { get; set; }
    }
    public partial class RatingTasksView
    {
        [Key]
        public Guid TaskGuid { get; set; }
        public string TaskNumber { get; set; }
        public string Subject { get; set; }       
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string StatusCompleted { get; set; }
        public string Approval { get; set; }
        public string ApprovalStar { get; set; }
    }
    public partial class RatingTasks_BieuDo
    {
        [Key]      
        public string EmployeeIdRS { get; set; }
        public string EmployeeName { get; set; }
        public decimal? SaoDGRS { get; set; }      
    }
}
