﻿using System;
using System.Collections.Generic;

namespace ES_MODEL.Task
{
    public partial class TaskOfRecurrences
    {
        public Guid RowGuid { get; set; }
        public Guid? TaskGuid { get; set; }
        public Guid? RecurrenceGuid { get; set; }

        public Recurrence RecurrenceGu { get; set; }
        public Tasks TaskGu { get; set; }
    }
}
