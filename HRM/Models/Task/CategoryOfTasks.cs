﻿using System;
using System.Collections.Generic;

namespace ES_MODEL.Task
{
    public partial class CategoryOfTasks
    {
        public CategoryOfTasks()
        {
            CategoryShare = new HashSet<CategoryShare>();
            CategoryOfProcess = new HashSet<CategoryOfProcess>();
            InverseParent = new HashSet<CategoryOfTasks>();
            TaskStepBySteps = new HashSet<TaskStepBySteps>();
            Tasks = new HashSet<Tasks>();
        }

        public Guid CategoryOfTaskGuid { get; set; }
        public Guid? ParentId { get; set; }
        public Guid? ProjectGuid { get; set; }
        public string ProjectId { get; set; }
        public string ProjectName { get; set; }
        public Guid OrganizationGuid { get; set; }
        public Guid? DepartmentGuid { get; set; }
        public string DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public Guid? EmployeeGuid { get; set; }
        public string EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public bool? IsPublic { get; set; }
        public string Alias { get; set; }
        public string TaskLevel { get; set; }
        public double? Mark { get; set; }
        public bool? IsActive { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public string Class { get; set; }
        public int? OrderId { get; set; }
        public string ClassText { get; set; }
        public string LoginName { get; set; }
        public bool? IsDefault { get; set; }
        public string TypeStep { get; set; }

        public CategoryOfTasks Parent { get; set; }
        public ICollection<CategoryOfProcess> CategoryOfProcess { get; set; }
        public ICollection<CategoryShare> CategoryShare { get; set; }
        public ICollection<CategoryOfTasks> InverseParent { get; set; }
        public ICollection<TaskStepBySteps> TaskStepBySteps { get; set; }
        public ICollection<Tasks> Tasks { get; set; }
    }
}
