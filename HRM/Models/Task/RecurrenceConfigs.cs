﻿using System;
using System.Collections.Generic;

namespace ES_MODEL.Task
{
    public partial class RecurrenceConfigs
    {
        public Guid RowGuid { get; set; }
        public int? Day { get; set; }
        public int? Month { get; set; }
        public int? Year { get; set; }
        public int? Week { get; set; }
        public Guid? RecurrenceGuid { get; set; }
        public DateTime? Postion { get; set; }
        public string WeekChoice { get; set; }
        public string Option { get; set; }

        public Recurrence RecurrenceGu { get; set; }
    }
}
