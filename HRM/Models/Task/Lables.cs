﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ES_MODEL.Task
{
    public partial class Lables
    {
        public Lables()
        {
            TaskOfLabels = new HashSet<TaskOfLabels>();
        }

        public int LableId { get; set; }
        public string LableName { get; set; }
        public string Description { get; set; }
        public Guid OrganizationGuid { get; set; }
        public int? OrderId { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string CreatedBy { get; set; }
        public string Class { get; set; }

        public ICollection<TaskOfLabels> TaskOfLabels { get; set; }
    }
    public partial class ViewJtableLables
    {
        [Key]
        public int LableId { get; set; }
        public string LableName { get; set; }
        public string Description { get; set; }
        public string Class { get; set; }
        public int? OrderId { get; set; }
    }




}
