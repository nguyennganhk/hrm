﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ES_MODEL.Task.Custom
{
    public partial class GetItem
    {
        [Key]
        public Guid TaskGuid { get; set; }
        public string Subject { get; set; }
        public string TaskContent { get; set; }
        public string TaskNumber { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Priority { get; set; }
        public double? PercentComplete { get; set; }
        public DateTime? TimeReminder { get; set; }
        public string StatusCompleted { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? IsActive { get; set; }
        public Guid? CategoryOfTaskGuid { get; set; }
        public string Description { get; set; }
        public string Note { get; set; }
        public Guid? ParentId { get; set; }
        public Guid? ProfileGuid { get; set; }
        public List<GetItem_TaskOfUser> TaskOfUsers
        {
            get
            {
                if (this._TaskOfUsers == null || this._TaskOfUsers == "[{}]")
                {
                    return new List<GetItem_TaskOfUser>();
                }
                return JsonConvert.DeserializeObject<List<GetItem_TaskOfUser>>(this._TaskOfUsers);
            }
        }
        public string _TaskOfUsers { get; set; }
        public DateTime? CompleteDate { get; set; }
        public string GroupTitle { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public List<GetItem_CheckLists> CheckList
        {
            get
            {
                if (this._CheckList == null || this._CheckList == "[{}]")
                {
                    return new List<GetItem_CheckLists>();
                }
                return JsonConvert.DeserializeObject<List<GetItem_CheckLists>>(this._CheckList);
            }
        }
        public string _CheckList { get; set; }
        public double? PercentCompleteTaskOfUser { get; set; }
        public string StatusCompletedTaskOfUser { get; set; }
        public string StatusIsOwnerTaskOfUser { get; set; }
        public DateTime? TimeReminderTaskOfUser { get; set; }

        public string IsAcceptTask { get; set; }
        public bool? IsCreated { get; set; }
        public string IsRecallAccept { get; set; }
        public bool? IsRecall { get; set; }
        public bool? IsFollow { get; set; }
        public List<GetItem_Labels> Labels
        {
            get
            {
                if (this._Labels == null || this._Labels == "[{}]")
                {
                    return new List<GetItem_Labels>();
                }
                return JsonConvert.DeserializeObject<List<GetItem_Labels>>(this._Labels);
            }
        }
        public string _Labels { get; set; }
        public int? AttachCount { get; set; }
        public int? CountComment { get; set; }
        public string _EmpPermissionCustom { get; set; }
        public EmpPermissionCustom EmpPermissionCustom
        {
            get
            {
                if (this._EmpPermissionCustom != null && this._EmpPermissionCustom != "" || this._EmpPermissionCustom == "[{}]")
                {
                    return JsonConvert.DeserializeObject<List<EmpPermissionCustom>>(this._EmpPermissionCustom)[0];
                }
                return null;
            }
        }
        public bool? ProcessDelete { get; set; }
        public string ProcessDeleteOfUser { get; set; }
        public Guid? TaskOfUserGuid { get; set; }
    }

    public partial class GetItem_Permission
    {
        [Key]
        public Guid PermissionGuid { get; set; }
        public string PermissionId { get; set; }
        public string PermissionName { get; set; }
        public string Alias { get; set; }
        public bool? IsAdd { get; set; }
        public bool? IsUpdate { get; set; }
        public bool? IsDelete { get; set; }
        public bool? IsApproval { get; set; }
        public bool? IsForward { get; set; }
        public bool? IsComment { get; set; }
        public bool? IsView { get; set; }
        public bool? IsCheck { get; set; }
        public bool? IsFullControl { get; set; }
        public string Description { get; set; }
        public int? LocationId { get; set; }
    }

    public partial class GetItem_CheckLists
    {
        [Key]
        public Guid? CheckListGuid { get; set; }
        public string CheckListTitle { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public bool? Status { get; set; }
        public int? OrderId { get; set; }
    }
    public partial class GetItem_Labels
    {
        [Key]
        public int? LabelId { get; set; }
        public string LabelName { get; set; }
        public string Class { get; set; }
    }
    public partial class GetItem_TaskOfUser
    {
        [Key]
        public Guid? TaskOfUserGuid { get; set; }
        public string IsOwner { get; set; }
        public Guid? TaskGuid { get; set; }
        public string EmployeeId { get; set; }
        public string LoginName { get; set; }
        public string FullName { get; set; }
        public bool? IsRead { get; set; }
        public string IsCompleted { get; set; }
        public DateTime? ReadDate { get; set; }
        public string DirectLink { get; set; }
        public GetItem_Permission Permission
        {
            get
            {
                if (this._Permission == null || this._Permission.Count > 0)
                {
                    return null;
                }
                return this._Permission[0];
            }
        }
        public List<GetItem_Permission> _Permission { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? EmployeeGuid { get; set; }
        public double? PercentComplete { get; set; }
        public string DepartmentName { get; set; }
        public string StatusCompleted { get; set; }
        public string ProcessAccept { get; set; }
        public string ProcessAcceptComment { get; set; }
        public string ProcessDelete { get; set; }
        public DateTime? TimeReminder { get; set; }
        public string Note { get; set; }
    }
}
