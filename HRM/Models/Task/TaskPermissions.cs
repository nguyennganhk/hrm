﻿using System;
using System.Collections.Generic;

namespace ES_MODEL.Task
{
    public partial class TaskPermissions
    {
        public TaskPermissions()
        {
            TaskOfUsers = new HashSet<TaskOfUsers>();
        }

        public Guid PermissionGuid { get; set; }
        public string PermissionId { get; set; }
        public string PermissionName { get; set; }
        public string Alias { get; set; }
        public bool? IsAdd { get; set; }
        public bool? IsUpdate { get; set; }
        public bool? IsDelete { get; set; }
        public bool? IsApproval { get; set; }
        public bool? IsForward { get; set; }
        public bool? IsComment { get; set; }
        public bool? IsView { get; set; }
        public bool? IsCheck { get; set; }
        public bool? IsFullControl { get; set; }
        public string Description { get; set; }
        public int? LocationId { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDisable { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public ICollection<TaskOfUsers> TaskOfUsers { get; set; }
    }
}
