﻿using System;
using System.Collections.Generic;

namespace ES_MODEL.Task
{
    public partial class Recurrence
    {
        public Recurrence()
        {
            RecurrenceConfigs = new HashSet<RecurrenceConfigs>();
            TaskOfRecurrences = new HashSet<TaskOfRecurrences>();
        }

        public Guid RowGuid { get; set; }
        public Guid OrganizationGuid { get; set; }
        public string Name { get; set; }
        public string LoginName { get; set; }
        public string Type { get; set; }
        public DateTime? IsRender { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? DayRegenerate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public string OptionMore { get; set; }
        public int? CountRender { get; set; }

        public ICollection<RecurrenceConfigs> RecurrenceConfigs { get; set; }
        public ICollection<TaskOfRecurrences> TaskOfRecurrences { get; set; }
    }
}
