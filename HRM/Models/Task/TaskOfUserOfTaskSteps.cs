﻿using System;
using System.Collections.Generic;

namespace ES_MODEL.Task
{
    public partial class TaskOfUserOfTaskSteps
    {
        public Guid RecordGuid { get; set; }
        public Guid? TaskStepByStepGuid { get; set; }
        public Guid? TaskOfUserGuid { get; set; }
        public Guid? TaskStepStatus { get; set; }

        public TaskOfUsers TaskOfUserGu { get; set; }
        public TaskStepBySteps TaskStepByStepGu { get; set; }
        public TaskStepStatus TaskStepStatusNavigation { get; set; }
    }
}
