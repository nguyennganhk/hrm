﻿using System;
using System.Collections.Generic;

namespace ES_MODEL.Task
{
    public partial class TasksComments
    {
        public TasksComments()
        {
            TaskCommentAttachments = new HashSet<TaskCommentAttachments>();
        }

        public Guid TaskCommentGuid { get; set; }
        public int TaskCommentId { get; set; }
        public Guid? TaskGuid { get; set; }
        public string Body { get; set; }
        public bool? IsRead { get; set; }
        public bool? IsUpdated { get; set; }
        public DateTime? UpdateTime { get; set; }
        public DateTime? ReadTime { get; set; }
        public Guid? EmployeeGuid { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeId { get; set; }
        public string Email { get; set; }
        public string AvatarUrl { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }

        public Tasks TaskGu { get; set; }
        public ICollection<TaskCommentAttachments> TaskCommentAttachments { get; set; }
    }
}
