﻿using System;
using System.Collections.Generic;

namespace ES_MODEL.Task
{
    public partial class TaskOfTaskSteps
    {
        public Guid RecordGuid { get; set; }
        public Guid? TaskGuid { get; set; }
        public Guid? TaskStepByStepGuid { get; set; }
        public string PermissionByName { get; set; }
        public string PermissionByLogin { get; set; }
        public Guid? PermissionByGuid { get; set; }
        public bool? IsActive { get; set; }

        public Tasks TaskGu { get; set; }
        public TaskStepBySteps TaskStepByStepGu { get; set; }
    }
}
