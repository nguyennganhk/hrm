﻿using System;
using System.Collections.Generic;

namespace ES_MODEL.Task
{
    public partial class TaskActivities
    {
        public Guid TaskActivityGuid { get; set; }
        public Guid TaskOfUserGuid { get; set; }
        public Guid TaskGuid { get; set; }
        public Guid EmployeeGuid { get; set; }
        public Guid OrganizationGuid { get; set; }
        public string EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public string LoginName { get; set; }
        public string Subject { get; set; }
        public string ActivityId { get; set; }
        public string ActivityContent { get; set; }
        public string Type { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public Tasks Tasks { get; set; }
    }
}
