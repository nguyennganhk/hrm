﻿using System;
using System.Collections.Generic;

namespace ES_MODEL.Task
{
    public partial class TaskOfLabels
    {
        public Guid RowGuid { get; set; }
        public Guid? TaskGuid { get; set; }
        public int? LabelId { get; set; }

        public Lables Label { get; set; }
        public Tasks TaskGu { get; set; }
    }
}
