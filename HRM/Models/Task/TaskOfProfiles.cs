﻿using System;
using System.Collections.Generic;

namespace ES_MODEL.Task
{
    public partial class TaskOfProfiles
    {
        public Guid TaskOfProfiles1 { get; set; }
        public Guid? ProfileGuid { get; set; }
        public Guid? TaskOfUserGuid { get; set; }

        public Profiles ProfileGu { get; set; }
        public TaskOfUsers TaskOfUserGu { get; set; }
    }
}
