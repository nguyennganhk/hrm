﻿using System;
using System.Collections.Generic;

namespace ES_MODEL.Task
{
    public partial class TaskStepStatus
    {
        public TaskStepStatus()
        {
            TaskOfUserOfTaskSteps = new HashSet<TaskOfUserOfTaskSteps>();
        }

        public Guid RecordGuid { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public Guid? TaskStepByStepGuid { get; set; }
        public string ColorBackground { get; set; }
        public string ColorText { get; set; }
        public int? Sort { get; set; }

        public TaskStepBySteps TaskStepByStepGu { get; set; }
        public ICollection<TaskOfUserOfTaskSteps> TaskOfUserOfTaskSteps { get; set; }
    }
}
