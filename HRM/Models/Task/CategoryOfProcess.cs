﻿using System;
using System.Collections.Generic;

namespace ES_MODEL.Task
{
    public partial class CategoryOfProcess
    {
        public Guid RowGuid { get; set; }
        public Guid? CategoryOfTaskGuid { get; set; }
        public Guid? TaskProcessGuid { get; set; }
        public int? OrderId { get; set; }

        public CategoryOfTasks CategoryOfTaskGu { get; set; }
        public TaskProcess TaskProcessGu { get; set; }
    }
}
