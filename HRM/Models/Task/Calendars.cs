﻿using System;
using System.Collections.Generic;

namespace ES_MODEL.Task
{
    public partial class Calendars
    {
        public Calendars()
        {
            CalendarOfUsers = new HashSet<CalendarOfUsers>();
        }

        public Guid CalendarGuid { get; set; }
        public Guid? TaskOfUserGuid { get; set; }
        public Guid? TaskGuid { get; set; }
        public Guid? OrganizationGuid { get; set; }
        public Guid DepartmentGuid { get; set; }
        public string DepartmentName { get; set; }
        public string Title { get; set; }
        public Guid? EmployeeGuid { get; set; }
        public string EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public string LoginName { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public string Location { get; set; }
        public string CalenderContent { get; set; }
        public bool? IsConflict { get; set; }
        public bool? IsPublic { get; set; }
        public bool? IsActive { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string CalenderType { get; set; }
        public string CalenderRange { get; set; }
        public string Class { get; set; }
        public bool? CheckNotify { get; set; }
        public DateTime? TimeNotify { get; set; }
        public bool? HadNotify { get; set; }
        public Guid? CalendarGroupGuid { get; set; }

        public ICollection<CalendarOfUsers> CalendarOfUsers { get; set; }
    }
}
