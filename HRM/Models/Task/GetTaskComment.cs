﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace ES_MODEL.Task.Custom
{
    public partial class GetTaskComment
    {
        [Key]
        public Guid CommentGuid { get; set; }
        public bool? IsSelf { get; set; }
        public Guid? RecordGuid { get; set; }
        public int? CountCommentTask { get; set; }
        public string AvatarUrl { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeId { get; set; }
        public Guid? EmployeeGuid { get; set; }
        public string TaskCommentAttachments { get; set; }
        public Guid? ParentGuid { get; set; }
        public string Comment { get; set; }
        public string Type { get; set; }
        public string LoginName { get; set; }
        public List<string> UserOfTask
        {
            get
            {
                if (this._UserOfTask == null || this._UserOfTask == "")
                {
                    return new List<string>();
                }
                var _obj = JsonConvert.DeserializeObject<List<GetTaskComment_UserOfTask>>(this._UserOfTask);
                return _obj.Select(x => x.LoginName).ToList();
            }
        }
        public string _UserOfTask { get; set; }
        public List<_GetTaskComment> TaskCommentChild
        {
            get
            {
                if (this._TaskComments == null || this._TaskComments == "")
                {
                    return null;
                }
                return JsonConvert.DeserializeObject<List<_GetTaskComment>>(this._TaskComments);
            }
        }
        public string _TaskComments { get; set; }
    }
    public partial class GetTaskComment_UserOfTask
    {
        [Key]
        public string LoginName { get; set; }
    }
    public partial class _GetTaskComment
    {
        [Key]
        public Guid CommentGuid { get; set; }
        public bool? IsSelf { get; set; }
        public string AvatarUrl { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeId { get; set; }
        public Guid? EmployeeGuid { get; set; }
        public string TaskCommentAttachments { get; set; }
        public Guid? ParentGuid { get; set; }
        public string Comment { get; set; }
        public string Type { get; set; }
        public string LoginName { get; set; }
    }
}
