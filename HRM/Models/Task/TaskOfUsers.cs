﻿using System;
using System.Collections.Generic;

namespace ES_MODEL.Task
{
    public partial class TaskOfUsers
    {
        public TaskOfUsers()
        {
            CalendarOfUsers = new HashSet<CalendarOfUsers>();
            TaskOfProfiles = new HashSet<TaskOfProfiles>();
            TaskOfUserOfTaskSteps = new HashSet<TaskOfUserOfTaskSteps>();
        }

        public Guid TaskOfUserGuid { get; set; }
        public Guid TaskGuid { get; set; }
        public Guid OrganizationGuid { get; set; }
        public Guid DepartmentGuid { get; set; }
        public string DepartmentId { get; set; }
        public Guid? EmployeeGuid { get; set; }
        public string EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public string LoginName { get; set; }
        public bool? IsRead { get; set; }
        public DateTime? ReadDate { get; set; }
        public string IsCompleted { get; set; }
        public string DirectLink { get; set; }
        public Guid? PermissionId { get; set; }
        public string IsOwner { get; set; }
        public string IsLeader { get; set; }
        public string IsAcceptTask { get; set; }
        public double? PercentComplete { get; set; }
        public DateTime? TimeReminder { get; set; }
        public string StatusCompleted { get; set; }
        public string Note { get; set; }
        public DateTime? CompleteDate { get; set; }
        public string IsRecallAccept { get; set; }
        public string TaskLevel { get; set; }
        public double? Mark { get; set; }
        public double? Kpiscore { get; set; }
        public bool? IsLocked { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? IsFollowed { get; set; }
        public DateTime? ActualStartDate { get; set; }
        public DateTime? ActualEndDate { get; set; }
        public Guid? TaskProcess { get; set; }
        public Guid? RecordGuid { get; set; }
        public string DepartmentName { get; set; }
        public string ProcessAccept { get; set; }
        public string ProcessAcceptComment { get; set; }
        public string ProcessDelete { get; set; }
        public DateTime? FinishDay { get; set; }
        public DateTime? ProcessAcceptDate { get; set; }
        public double? ProcessAcceptPercent { get; set; }

        public TaskPermissions Permission { get; set; }
        public Tasks TaskGu { get; set; }
        public ICollection<CalendarOfUsers> CalendarOfUsers { get; set; }
        public ICollection<TaskOfProfiles> TaskOfProfiles { get; set; }
        public ICollection<TaskOfUserOfTaskSteps> TaskOfUserOfTaskSteps { get; set; }
    }
}
