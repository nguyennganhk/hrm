﻿using System;
using System.Collections.Generic;

namespace ES_MODEL.Task
{
    public partial class TaskCheckLists
    {
        public Guid CheckListGuid { get; set; }
        public Guid? TaskGuid { get; set; }
        public string CheckListTitle { get; set; }
        public int? OrderId { get; set; }
        public bool? Status { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public Tasks TaskGu { get; set; }
    }
}
