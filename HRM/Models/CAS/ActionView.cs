﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HRM.Models
{
    public class ESActions
    {
        [Key]
        public  string Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int? Ord { get; set; }
        public int? Seq { get; set; } 
    }
}


 