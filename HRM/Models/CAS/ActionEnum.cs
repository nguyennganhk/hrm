﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HRM.Models
{
    public enum EAction
    {
        ACCESS,
        LISTVIEW,
        OPEN,
        ADD,
        EDIT,
        DELETE,
        SORT,
        APPROVE,
        REJECT,
        DOWNLOAD,
        UPLOAD,
        EXPORT,
        IMPORT,
        FORWARD,
        SET_PASSWORD,
        ADD_FOLDER,
        EDIT_FOLDER,
        DELETE_FOLDER,
        LOCKUP,
        UNLOCK,
        COMMENT,
        ADD_OTHER,
        EDIT_OTHER,
        DELETE_OTHER ,
        SYNCHRONIZED,
        START_JOBS,
        END_JOBS,
        ADD_ACCVOUCHER,
        ADD_CURVOUCHER,
        ADD_DEBTVOUCHER,
        ADD_INVEVOUCHER,
        ADD_INVOICEVOUCHER,
        ADD_FULLVOUCHER,
        ACCOUNT_ADMIN,
         FULLCONTROL
    }
    public static partial class ExtensionMethods
    {
        public static string Value(this EAction e)
        {
            switch (e)
            {
                case EAction.ACCESS:
                    return "ACCESS";
                case EAction.LISTVIEW:
                    return "LISTVIEW";
                case EAction.OPEN:
                    return "OPEN";
                case EAction.ADD:
                    return "ADD";
                case EAction.EDIT:
                    return "EDIT";
                case EAction.DELETE:
                    return "DELETE";
                case EAction.SORT:
                    return "SORT";
                case EAction.APPROVE:
                    return "APPROVE";
                case EAction.REJECT:
                    return "REJECT";
                case EAction.DOWNLOAD:
                    return "DOWNLOAD";
                case EAction.UPLOAD:
                    return "UPLOAD";
                case EAction.EXPORT:
                    return "EXPORT";
                case EAction.IMPORT:
                    return "IMPORT";
                case EAction.FORWARD:
                    return "FORWARD";
                case EAction.SET_PASSWORD:
                    return "SET_PASSWORD";
                case EAction.ADD_FOLDER:
                    return "ADD_FOLDER";
                case EAction.EDIT_FOLDER:
                    return "EDIT_FOLDER";
                case EAction.DELETE_FOLDER:
                    return "DELETE_FOLDER";
                case EAction.LOCKUP:
                    return "LOCK";
                case EAction.UNLOCK:
                    return "UNLOCK";
                case EAction.COMMENT:
                    return "COMMENT";
                case EAction.ADD_OTHER:
                    return "ADD_OTHER";
                case EAction.EDIT_OTHER:
                    return "EDIT_OTHER";
                case EAction.DELETE_OTHER:
                    return "DELETE_OTHER";
                case EAction.SYNCHRONIZED:
                    return "SYNCHRONIZED";
                case EAction.START_JOBS:
                    return "START_JOBS";
                case EAction.END_JOBS:
                    return "END_JOBS";
                case EAction.ADD_ACCVOUCHER:
                    return "ADD_ACCVOUCHER";
                case EAction.ADD_CURVOUCHER:
                    return "ADD_CURVOUCHER";
                case EAction.ADD_DEBTVOUCHER:
                    return "ADD_DEBTVOUCHER";
                case EAction.ADD_INVEVOUCHER:
                    return "ADD_INVEVOUCHER";
                case EAction.ADD_INVOICEVOUCHER:
                    return "ADD_INVOICEVOUCHER";
                case EAction.ADD_FULLVOUCHER:
                    return "ADD_FULLVOUCHER";
                case EAction.ACCOUNT_ADMIN:
                    return "ACCOUNT_ADMIN";
                case EAction.FULLCONTROL:
                    return "FULLCONTROL";
                    
            }
            return "";
        }
    }
}
