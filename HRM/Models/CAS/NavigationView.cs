﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HRM.Models
{
    public class NavigationView
    {
        public NavigationView()
        {
            HasChild = false;
        }
        public int Id { get; set; }
        public string Title { get; set; }
        public string Code { get; set; }
        public string Icon { get; set; }
        public string Url { get; set; }
        public int? ParentId { get; set; }
        public int? Ord { get; set; }
        public bool HasChild { get; set; }
    }
}
