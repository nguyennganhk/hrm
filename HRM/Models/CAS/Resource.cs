﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace HRM.Models
{
    public partial class Privilege
    {
               
        [Key]
        public int Id { get; set; }
        public string ActionId { get; set; }
        public int? ResourceId { get; set; }
        public Resource Resource { get; set; }
    }
    public partial class Attribute
    {
        [Key]
        public int Id { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
        public int? ResourceId { get; set; }      
        public Resource Resource { get; set; }
    }
    public class Resource
    {
        public Resource()
        {
            Privileges = new HashSet<Privilege>();
            Attributes = new HashSet<Attribute>();
        } 
        [Key]
        public int Id { get; set; }
        public string Title { get; set; }
        public string Code { get; set; }
        public int? ParentId { get; set; }
        public string GroupResourceId { get; set; }        
        public int? Ord { get; set; }

        public virtual ICollection<Privilege> Privileges { get; set; }
        public virtual ICollection<Attribute> Attributes { get; set; } 
    }
    public class ESUserPrivileges
    {
           
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        
         public int Id { get; set; }
        public string UserId { get; set; }
        public int PrivilegeId { get; set; }
       
     
        
    }
}
