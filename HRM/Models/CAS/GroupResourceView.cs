﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HRM.Models
{
    
    public class GroupResourceView
    {
        public GroupResourceView() { }
        public int? Seq { get; set; }
        public string Id { get; set; }
        public string Title { get; set; }
        public List<int> Applications { get; set; }

    }
}
