﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HRM.Models
{
    public class ApplicationView
    {
        public ApplicationView() { } 

            public int Id { get; set; }
            public string Title { get; set; }
            public string Code { get; set; }
            public int? Status { get; set; }
            public int? Ord { get; set; }
            public bool Active { get; set; }
            public string Description { get; set; }

    }
    
    public class AppPermisisonView
    {
        public AppPermisisonView()
        {
            Users = new List<string>();
            Roles = new List<string>();
            GroupUsers = new List<int>(); 
            Orgs = new List<int>();
            Endpoints = new List<int>();
        }
        public List<string> Users { get; set; }
        public List<string> Roles { get; set; }
          public List<int> GroupUsers { get; set; }
        public List<int> Orgs { get; set; }
        public List<int> Endpoints { get; set; }
        public List<ApplicationView> Applications { get; set; }
    }
}
