﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HRM.Models
{
    public class PrivilegeView
    {
        public List<int> Resources { get; set; }
        public List<ESActions> Actions { get; set; }
    }
}
