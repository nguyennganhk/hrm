﻿using System;
using System.Collections.Generic;

namespace HRM.Models
{
    public partial class Esorganizations
    {
        public Esorganizations()
        {
            AspNetUsers = new HashSet<AspNetUsers>();
            InverseParent = new HashSet<Esorganizations>();
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public int? ParentId { get; set; }
        public int? Ord { get; set; }
        public string Address { get; set; }

        public Esorganizations Parent { get; set; }
        public ICollection<AspNetUsers> AspNetUsers { get; set; }
        public ICollection<Esorganizations> InverseParent { get; set; }
    }
}
