﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HRM.Models
{
    public class PermissionView
    {


        
        public PermissionView()

        {
            Objects = new List<string>();
            Resources = new List<int>();
            Actions = new List<ESActions>();
        }
        public int ObjectType { get; set; }
        public List<string> Objects { get; set; }
        public List<int> Resources { get; set; }
        public List<ESActions> Actions { get; set; }
    
    }
}
