﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HRM.Models
{
    public class UserViewModel
    {
        public string Id { set; get; }
        public string UserName { set; get; }
        public string Password { set; get; }
        public string Email { set; get; }
        public string PhoneNumber { set; get; }
     
        public string FamilyName { set; get; }
        public string GivenName { set; get; }
        public int? OrgId { set; get; }

        public bool LockoutEnabled { get; set; }
        public  DateTimeOffset? LockoutEnd { get; set; }
        public  bool TwoFactorEnabled { get; set; }
        public  bool PhoneNumberConfirmed { get; set; }
        public  bool EmailConfirmed { get; set; }


         public List<string> Roles { get; set; }
        public List<int> Groups { get; set; }

       
    }
}
