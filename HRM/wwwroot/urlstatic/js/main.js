﻿(function ($) {
    'use strict';

    // -------------------------------------------------------------------------------------------------
    // NAVBAR SEARCH ................ Navbar search
    // SIDEBAR NAV .................. Sidebar nav
    // -------------------------------------------------------------------------------------------------

    $(document).ready(function () {
        initSidebarScrollbar();
        navbarSearch();
        sidebarNav();
        select2();
        flatpickr();
        tooltips();
        sidebarCollapse();
    });

    // -------------------------------------------------------------------------------------------------
    // NAVBAR SEARCH
    // -------------------------------------------------------------------------------------------------

    function navbarSearch() {
        $('.navbar-search__input').click(function (e) {
            e.preventDefault();
            var el = $(this);
            el.parent().addClass('focus');
        });
        $(document).click(function (e) {
            var el = $('.form-control');
            if (!el.is(e.target) && el.parent().has(e.target).length === 0) {
                el.parent().removeClass('focus');
            }
        });
    }

    // -------------------------------------------------------------------------------------------------
    // SIDEBAR
    // -------------------------------------------------------------------------------------------------

    function sidebarNav() {
        $('.sidebar-nav__link').on('click', function (event) {
            var el = $(this);
            var navItem = el.closest('.sidebar-nav__item');
            var isActive = el.parent().hasClass('is-active');

            $('.sidebar-nav__item').removeClass('is-active');

            if (!isActive) {
                el.parent().addClass('is-active');
            }

            if ($('body').hasClass('sidebar-sm') || $('body').hasClass('sidebar-md')) {
                var offsetTop = 0;

                offsetTop = $('.sidebar').position().top + navItem.position().top + 65;
                $('.sidebar-subnav').not(el.next()).slideUp(0);
                el.next().slideToggle(0);
                el.next().css('top', offsetTop);
            } else {
                $('.sidebar-subnav').not(el.next()).slideUp(150);
                el.next().slideToggle(150);
            }

            //localStorage.setItem('navItem', elIndex);

            setTimeout(function () {
                $(document).trigger('recalculate-sidebar-scroll');
            }, 200);

            if (el.closest('.sidebar-nav__item').find('.sidebar-subnav').length) {
                return false;
            }
        });
    }

    $('.sidebar-section-nav__link').on('click', function (event) {
        var el = $(this);
        var navItem = el.closest('.sidebar-section-nav__item');
        // Begin created by habx@esvn.com.vn
        if (navItem === null || navItem === undefined || navItem.length === 0) {
            navItem = $(this).next('ul');
        }
        // End created by habx@esvn.com.vn
        var isActive = el.parent().hasClass('is-active');

        $('.sidebar-section-nav__item').removeClass('is-active');

        if (!isActive) {
            el.parent().addClass('is-active');
        }

        if ($('body').hasClass('sidebar-sm') || $('body').hasClass('sidebar-md')) {
            var offsetTop = 0;
            offsetTop = $('.sidebar-section').position().top + navItem.position().top;
            $('.sidebar-section-subnav').not(el.next()).slideUp(0);
            el.next().slideToggle(0);
            ////This old version
            //el.next().css('top', offsetTop);
            // Begin created by habx@esvn.com.vn
            el.next().css('top', this.getBoundingClientRect().top);
            // End created by habx@esvn.com.vn
        } else {
            $('.sidebar-section-subnav').not(el.next()).slideUp(150);
            el.next().slideToggle(150);
        }

        setTimeout(function () {
            $(document).trigger('recalculate-sidebar-scroll');
        }, 200);

        if (el.closest('.sidebar-section-nav__item').find('.sidebar-section-subnav').length) {
            return false;
        }
    });
    // Begin created by habx@esvn.com.vn
    $('.es_menu_lv2').on('mouseenter mouseleave', function (event) {
        //    display: inline-block;
        //left: 306px;
        //top: 100px;
        if ($('body').hasClass('sidebar-sm') || $('body').hasClass('sidebar-md')) {
            var _el = $((this.offsetParent).offsetParent);
            _el = $(_el).find('ul');
            for (var i = 0; i < _el.length; i++) {
                $(_el[i]).css('display', 'none');
                $(this).next().css('left', '');
                $(this).next().css('top', '');
            }
            if (($($(this).next()[0]).height() + this.getBoundingClientRect().top) > $('body').height()) {
                $(this).next().css('top', this.getBoundingClientRect().top - $($(this).next()[0]).height() + this.offsetHeight);
                $(this).next().css('display', 'inline-block');
                $(this).next().css('left', 306);
            } else {
                $(this).next().css('top', this.getBoundingClientRect().top);
                $(this).next().css('display', 'inline-block');
                $(this).next().css('left', 306);
            }
        }
    });
    $(document).ready(function () {
        try {
            var _el = $(document.getElementsByClassName('minize-menuleft')[0].offsetParent);
            var _elChild = _el.find('a');
            var _elActive = null;
            for (var i = 0; i < _elChild.length; i++) {
                try {
                    if (window.location.pathname.replace(/[^\w\s]/gi, '').toUpperCase() === _elChild[i].getAttribute('href').replace(/[^\w\s]/gi, '').toUpperCase()) {
                        _elActive = _elChild[i];
                        break;
                    }
                } catch (ex) {
                    console.log('Error : this line message.');
                }
            }
            _el = $(_elActive).parent()[0];
            $(_el).addClass('menuLeft-active');
            while (_el) {
                $(_el).css('display', '');
                _el = $(_el).parent()[0];
                if (_el !== null && $(_el).hasClass('menuLeft-SubnavChild')) {
                    $($(_el).prev()).addClass('menuLeft-active');
                }
            }
        } catch (ex) { console.log('Warming:', ex) }
    });
    $('.sidebar-section-nav__link-dropdown').on('click', function () {
        if ($(this).hasClass('es_menu_lv1')) {
            $(this).toggleClass('menuLeft-active');
        }
        //var _el = $(document.getElementsByClassName('minize-menuleft')[0].offsetParent);
        //var _elChild = _el.find('.menuLeft-active');
        //for (var i = 0; i < _elChild.length; i++) {
        //    $(_elChild[i]).removeClass('menuLeft-active');
        //}
    })
    // End created by habx@esvn.com.vn
    function select2() {
        $('select').not('.selectbox').each(function () {
            var options = {
                placeholder: function () {
                    var el = $(this);
                    el.data('placeholder');
                }
            };

            if (!$(this).is('[data-search-enable]')) {
                options['minimumResultsForSearch'] = Infinity;
            }
            if ($(this).select2 !== undefined && $(this).select2 !== null)
                $(this).select2(options);
        });

        function formatUsers(user) {
            if (!user.id) { return user.text; }
            var $user = $(
                '<span class="select-user__img"><img src="img/users/' + user.element.value.toLowerCase() + '.png" /></span><span class="select-user__name">' + user.text + '</span>'
            );
            return $user;
        }
        if ($(".select-user").select2 !== undefined && $(".select-user").select2 !== null)
            $(".select-user").select2({
                dropdownCssClass: "select-user-dropdown",
                templateSelection: formatUsers,
                templateResult: formatUsers,
                minimumResultsForSearch: Infinity
            });

        $('.select2').click(function (e) {
            var el = $(this);
            el.find('b').hide();
        });
    }

    function flatpickr() {
        if (jQuery.flatpickr) {
            $('.flatpickr').flatpickr({
                altInput: true
            });
        }
    }

    function tooltips() {
        //$('[data-toggle="tooltip"]').tooltip();
        tippy('[data-toggle="tooltip"]', {
            arrow: true
        });
    }

    function toggleSidebar() {
        $('body').toggleClass('sidebar-sm');

        if ($('body').hasClass('sidebar-sm')) {
            initSidebarScrollbar(false);
        } else {
            initSidebarScrollbar(true);
        }

        setTimeout(function () {
            $(document).trigger('recalculate-sidebar-scroll');
        }, 200);
    }

    function sidebarCollapse() {
        $('.sidebar__collapse').on('click', function () {
            toggleSidebar();
        });
    }

    $(document).on('collapse-sidebar', function () {
        toggleSidebar();
    });

    /**
     * Scroll for any element
     */
    $('.js-scrollable').each(function () {
        new SimpleBar(this);
    });

    function initSidebarScrollbar() {
        var autoHide = true;

        if ($('body').hasClass('sidebar-sm') || $('body').hasClass('sidebar-md')) {
            autoHide = false;
        }

        if ($('.sidebar').length) {
            if ($('.sidebar').hasClass('js-disable-scrollbar')) {
                return;
            }

            /**
             * Scroll for sidebar
             */
            if ($('.sidebar').css('position') === 'fixed') {
                var sidebarScroll = new SimpleBar($('.sidebar__scroll').get(0), {
                    autoHide: autoHide
                });

                sidebarScroll.getScrollElement().addEventListener('scroll', function () {
                    if ($('body').hasClass('sidebar-sm') || $('body').hasClass('sidebar-md')) {
                        $('.sidebar-subnav').hide();
                        $('.sidebar-nav__item').removeClass('is-active');
                    }
                });
            }
        }


        if ($('.sidebar-section').length) {
            /**
             * Scroll for section sidebar
             */
            if ($('.sidebar-section').css('position') === 'fixed') {
                var sidebarSectionScroll = new SimpleBar($('.sidebar-section__scroll').get(0), {
                    autoHide: autoHide
                });

                sidebarSectionScroll.getScrollElement().addEventListener('scroll', function () {
                    if ($('body').hasClass('sidebar-sm')) {
                        $('.sidebar-section-subnav').hide();
                        $('.sidebar-section-nav__item').removeClass('is-active');
                    }
                });
            }
        }
    }

    function sidebarActiveLink(sidebarName) {
        // Select current sidebar menu link
        var pathName = location.pathname.split('/');
        var url = pathName[pathName.length - 1];
        var currentSubNavLink = $('.' + sidebarName + '-subnav__link[href="' + url + '"]');
        var currentNavLink = $('.' + sidebarName + '-nav__link[href="' + url + '"]');

        if (currentSubNavLink.length) {
            currentSubNavLink.addClass('is-active');
            currentSubNavLink.closest('.' + sidebarName + '-nav__item').addClass('is-active');

            if (!$('body').hasClass('sidebar-sm') && !$('body').hasClass('sidebar-md')) {
                currentSubNavLink.closest('.' + sidebarName + '-subnav').show();
            }
        } else if (currentNavLink.length) {
            currentNavLink.closest('.' + sidebarName + '-nav__item').addClass('is-active');
        }
    }

    $('.textavatar').each(function () {
        $(this).textAvatar({
            width: $(this).data('width'),
            height: $(this).data('height')
        });
    });

    $('.sidebar-toggler').on('click', function () {
        $('body').toggleClass('sidebar-is-opened');
    });

    $(document).on('click', function (e) {
        if ($('body').hasClass('sidebar-is-opened') && !$(e.target).closest('.sidebar-toggler').length) {
            if (!$(e.target).closest('.sidebar').length) {
                $('body').removeClass('sidebar-is-opened');
            }
        }

        if ($('.navbar-collapse').hasClass('show') && !$(e.target).closest('.navbar-toggler').length) {
            if (!$(e.target).closest('.navbar-collapse').length) {
                $('.navbar-collapse').removeClass('show');
                $('body').removeClass('is-navbar-opened');
            }
        }

        // Hide sidebar submenu if sidebar collapsed
        if ($('body').hasClass('sidebar-sm') || $('body').hasClass('sidebar-md')) {
            if (!$(e.target).closest('.sidebar').length) {
                $('.sidebar-subnav').hide();
                $('.sidebar-nav__item').removeClass('is-active');
            }

            if (!$(e.target).closest('.sidebar-section').length) {
                $('.sidebar-section-subnav').hide();
                $('.sidebar-section-nav__item').removeClass('is-active');
            }
        }
    });

    $('.navbar-toggler').on('click', function () {
        $('body').toggleClass('is-navbar-opened');
    });

    if ($('.js-page-preloader').length) {
        setTimeout(function () {
            $('#page-loader-progress-bar').css('width', '20%');
        }, 200);

        setTimeout(function () {
            $('#page-loader-progress-bar').css('width', '40%');
        }, 400);

        setTimeout(function () {
            $('#page-loader-progress-bar').css('width', '60%');
        }, 600);

        setTimeout(function () {
            $('#page-loader-progress-bar').css('width', '100%');
        }, 800);

        setTimeout(function () {
            $('.js-page-preloader').fadeOut(0);
            $('body').removeClass('js-loading');
        }, 1000);
    }

    $('.color-checkbox :checkbox').on('change', function () {
        var parent = $(this).closest('.color-checkbox');

        if (this.checked) {
            parent.addClass('is-checked');
        } else {
            parent.removeClass('is-checked');
        }
    });

    $('.color-radio :radio').on('click', function () {
        var parent = $(this).closest('.color-radio');
        var name = $(this).attr('name');

        $('.color-radio input[name="' + name + '"]').each(function () {
            $(this).closest('.color-radio').removeClass('is-checked');
        });

        parent.addClass('is-checked');
    });

    $('.btn').on('mouseup', function () {
        var self = this;

        setTimeout(function () {
            self.blur();
        }, 500);
    });

    if (jQuery.flatpickr) {
        $('.js-datepicker').flatpickr();
    }

    $('[data-toggle="popover"]').popover();

    sidebarActiveLink('sidebar');
    sidebarActiveLink('sidebar-section');

    $('[data-js-height]').each(function () {
        $(this).height($(this).data('js-height'));
    });
})(jQuery);

//Begin create by habx@esvn.com.vn
var OnESConfirm = [];
var ESConfirmIcon = [
    {
        'class': 'esmailbox',
        'title': 'Thông báo',
        'description': '',
        'value': 'PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjwhLS0gR2VuZXJhdG9yOiBBZG9iZSBJbGx1c3RyYXRvciAyMS4xLjAsIFNWRyBFeHBvcnQgUGx1Zy1JbiAuIFNWRyBWZXJzaW9uOiA2LjAwIEJ1aWxkIDApICAtLT4NCjxzdmcgdmVyc2lvbj0iMS4xIiBpZD0iR3JvdXBfMV8xXyIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgeD0iMHB4IiB5PSIwcHgiDQoJIHZpZXdCb3g9IjAgMCA5NiA5NiIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgOTYgOTY7IiB4bWw6c3BhY2U9InByZXNlcnZlIj4NCjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+DQoJLnN0MHtmaWxsLXJ1bGU6ZXZlbm9kZDtjbGlwLXJ1bGU6ZXZlbm9kZDtmaWxsOiMyMTk2RjM7fQ0KCS5zdDF7ZmlsbC1ydWxlOmV2ZW5vZGQ7Y2xpcC1ydWxlOmV2ZW5vZGQ7ZmlsbDojRkZDQzgwO30NCgkuc3Qye2ZpbGwtcnVsZTpldmVub2RkO2NsaXAtcnVsZTpldmVub2RkO2ZpbGw6IzE5NzZEMjt9DQoJLnN0M3tmaWxsLXJ1bGU6ZXZlbm9kZDtjbGlwLXJ1bGU6ZXZlbm9kZDtmaWxsOiM2NEI1RjY7fQ0KCS5zdDR7ZmlsbC1ydWxlOmV2ZW5vZGQ7Y2xpcC1ydWxlOmV2ZW5vZGQ7ZmlsbDojMEQ0N0ExO30NCgkuc3Q1e2ZpbGwtcnVsZTpldmVub2RkO2NsaXAtcnVsZTpldmVub2RkO2ZpbGw6I0ZGM0QwMDt9DQoJLnN0NntmaWxsLXJ1bGU6ZXZlbm9kZDt' +
            'jbGlwLXJ1bGU6ZXZlbm9kZDtmaWxsOiNERjJDMDA7fQ0KCS5zdDd7ZmlsbC1ydWxlOmV2ZW5vZGQ7Y2xpcC1ydWxlOmV2ZW5vZGQ7ZmlsbDojRkZBNzI2O30NCjwvc3R5bGU+DQo8ZyBpZD0iU2hhcGVfMTcwX2NvcHkiPg0KCTxnPg0KCQk8cGF0aCBjbGFzcz0ic3QwIiBkPSJNNjksMTRIMjdjLTguOCwwLTE2LDcuMi0xNiwxNnYzMmg3NFYzMEM4NSwyMS4yLDc3LjgsMTQsNjksMTR6Ii8+DQoJPC9nPg0KPC9nPg0KPGcgaWQ9IlNoYXBlXzE3MF9jb3B5XzgiPg0KCTxnPg0KCQk8cmVjdCB4PSI0NSIgeT0iNjIiIGNsYXNzPSJzdDEiIHdpZHRoPSIxMCIgaGVpZ2h0PSIyMCIvPg0KCTwvZz4NCjwvZz4NCjxnIGlkPSJTaGFwZV8xNzBfY29weV8yIj4NCgk8Zz4NCgkJPHBhdGggY2xhc3M9InN0MiIgZD0iTTI3LDE0Yy04LjgsMC0xNiw3LjItMTYsMTZ2MzJoMzJWMzBDNDMsMjEuMiwzNS44LDE0LDI3LDE0eiIvPg0KCTwvZz4NCjwvZz4NCjxnIGlkPSJTaGFwZV8xNzBfY29weV8zIj4NCgk8Zz4NCgkJPHBhdGggY2xhc3M9InN0MyIgZD0iTTI3LDE4Yy02LjYsMC0xMiw1LjQtMTIsMTJ2MjhoMjRWMzBDMzksMjMuNCwzMy42LDE4LDI3LDE4eiIvPg0KCTwvZz4NCjwvZz4NCjxnIGlkPSJTaGFwZV8xNzBfY29weV80Ij4NCgk8Zz4NCgkJPHBhdGggY2xhc3M9InN0NCIgZD0iTTMzLDMySDIxYy0xLjEsMC0yLDAuOS0yLDJjMCwxLjEsMC45LDIsMiwyaDEyYzEuMSwwLDItMC45LDItMkMzNSwzMi45LDM0LjEsMzIsMzMsMzJ6Ii8+DQ' +
            'oJPC9nPg0KPC9nPg0KPGcgaWQ9IlNoYXBlXzE3MF9jb3B5XzUiPg0KCTxnPg0KCQk8cGF0aCBjbGFzcz0ic3Q1IiBkPSJNNTYuNCwyOGMtMC43LTEuMi0yLTItMy40LTJjLTIuMiwwLTQsMS44LTQsNGMwLDIuMiwxLjgsNCw0LDRjMS41LDAsMi44LTAuOCwzLjQtMkg3MXY4aDhWMjhINTYuNHoiLz4NCgk8L2c+DQo8L2c+DQo8ZyBpZD0iU2hhcGVfMTcwX2NvcHlfNiI+DQoJPGc+DQoJCTxjaXJjbGUgY2xhc3M9InN0NiIgY3g9IjUzIiBjeT0iMzAiIHI9IjIiLz4NCgk8L2c+DQo8L2c+DQo8ZyBpZD0iU2hhcGVfMTcwIj4NCgk8Zz4NCgkJPHJlY3QgeD0iNDUiIHk9IjYyIiBjbGFzcz0ic3Q3IiB3aWR0aD0iMTAiIGhlaWdodD0iNCIvPg0KCTwvZz4NCjwvZz4NCjwvc3ZnPg0K',
    },
    {
        'class': 'escheck',
        'title': 'Thông báo',
        'description': '',
        'value': 'PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjwhLS0gR2VuZXJhdG9yOiBBZG9iZSBJbGx1c3RyYXRvciAyMS4xLjAsIFNWRyBFeHBvcnQgUGx1Zy1JbiAuIFNWRyBWZXJzaW9uOiA2LjAwIEJ1aWxkIDApICAtLT4NCjxzdmcgdmVyc2lvbj0iMS4xIiBpZD0iR3JvdXBfMl8xXyIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgeD0iMHB4IiB5PSIwcHgiDQoJIHZpZXdCb3g9IjAgMCA5NiA5NiIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgOTYgOTY7IiB4bWw6c3BhY2U9InByZXNlcnZlIj4NCjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+DQoJLnN0MHtmaWxsLXJ1bGU6ZXZlbm9kZDtjbGlwLXJ1bGU6ZXZlbm9kZDtmaWxsOiM0RkJBNTM7fQ0KCS5zdDF7ZmlsbC1ydWxlOmV2ZW5vZGQ7Y2xpcC1ydWxlOmV2ZW5vZGQ7ZmlsbDojRkZGRkZGO30NCjwvc3R5bGU+DQo8ZyBpZD0iU2hhcGVfMTY5Ij4NCgk8Zz4NCgkJPGNpcmNsZSBjbGFzcz0ic3QwIiBjeD0iNDgiIGN5PSI0OCIgcj0iMzgiLz4NCgk8L2c+DQo8L2c+DQo8ZyBpZD0iU2hhcGVfMTY5X2NvcHkiPg0KCTxnPg0KCQk8cG9seWdvbiBjbGFzcz0ic3QxIiBwb2ludHM9IjY5LjIsMjkuMiA0Miw1Ni4zIDMwLjgsNDUuMiAyNS4yLDUwLjggNDIsNjcuNyA3NC44LDM0LjggCQkiLz4NCgk8L2c+DQo8L2c+DQo8L3N2Zz4NCg=='
    },
    {
        'class': 'esclose',
        'title': 'Thông báo',
        'description': '',
        'value': 'PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjwhLS0gR2VuZXJhdG9yOiBBZG9iZSBJbGx1c3RyYXRvciAyMS4xLjAsIFNWRyBFeHBvcnQgUGx1Zy1JbiAuIFNWRyBWZXJzaW9uOiA2LjAwIEJ1aWxkIDApICAtLT4NCjxzdmcgdmVyc2lvbj0iMS4xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCINCgkgdmlld0JveD0iMCAwIDk2IDk2IiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCA5NiA5NjsiIHhtbDpzcGFjZT0icHJlc2VydmUiPg0KPHN0eWxlIHR5cGU9InRleHQvY3NzIj4NCgkuc3Qwe2VuYWJsZS1iYWNrZ3JvdW5kOm5ldyAgICA7fQ0KCS5zdDF7ZmlsbC1ydWxlOmV2ZW5vZGQ7Y2xpcC1ydWxlOmV2ZW5vZGQ7ZmlsbDojRjM1ODYzO30NCgkuc3Qye2ZpbGwtcnVsZTpldmVub2RkO2NsaXAtcnVsZTpldmVub2RkO2ZpbGw6I0ZGRkZGRjt9DQo8L3N0eWxlPg0KPGcgaWQ9IlNoYXBlXzE3MV8xXyIgY2xhc3M9InN0MCI+DQoJPGcgaWQ9IlNoYXBlXzE3MSI+DQoJCTxnPg0KCQkJPHBhdGggY2xhc3M9InN0MSIgZD0iTTc0LDEySDIyYy01LjUsMC0xMCw0LjUtMTAsMTB2NTJjMCw1LjUsNC41LDEwLDEwLDEwaDUyYzUuNSwwLDEwLTQuNSwxMC0xMFYyMkM4NCwxNi41LDc5LjUsMTIsNzQsMTJ6Ii8+DQoJCTwvZz4NCgk8L2c+DQo8L2c+DQo8ZyBpZD0iU2hhcGVfMTc' +
            'xX2NvcHlfMV8iIGNsYXNzPSJzdDAiPg0KCTxnIGlkPSJTaGFwZV8xNzFfY29weSI+DQoJCTxnPg0KCQkJPHBvbHlnb24gY2xhc3M9InN0MiIgcG9pbnRzPSI2OCw2MS45IDUzLjksNDcuNyA2Ny43LDM0IDYyLDI4LjMgNDguMyw0Mi4xIDMzLjksMjcuNyAyOC4yLDMzLjQgNDIuNiw0Ny43IDI4LDYyLjMgMzMuNyw2OCANCgkJCQk0OC4zLDUzLjQgNjIuNCw2Ny41IAkJCSIvPg0KCQk8L2c+DQoJPC9nPg0KPC9nPg0KPC9zdmc+DQo=',
    },
    {
        'class': 'esmail',
        'value': 'PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjwhLS0gR2VuZXJhdG9yOiBBZG9iZSBJbGx1c3RyYXRvciAyMS4xLjAsIFNWRyBFeHBvcnQgUGx1Zy1JbiAuIFNWRyBWZXJzaW9uOiA2LjAwIEJ1aWxkIDApICAtLT4NCjxzdmcgdmVyc2lvbj0iMS4xIiBpZD0iR3JvdXBfMV8xXyIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgeD0iMHB4IiB5PSIwcHgiDQoJIHZpZXdCb3g9IjAgMCA5NiA5NiIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgOTYgOTY7IiB4bWw6c3BhY2U9InByZXNlcnZlIj4NCjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+DQoJLnN0MHtmaWxsLXJ1bGU6ZXZlbm9kZDtjbGlwLXJ1bGU6ZXZlbm9kZDtmaWxsOiMyMTk2RjM7fQ0KCS5zdDF7ZmlsbC1ydWxlOmV2ZW5vZGQ7Y2xpcC1ydWxlOmV2ZW5vZGQ7ZmlsbDojMEQ0N0ExO30NCgkuc3Qye2ZpbGwtcnVsZTpldmVub2RkO2NsaXAtcnVsZTpldmVub2RkO2ZpbGw6IzkwQ0FGOTt9DQo8L3N0eWxlPg0KPGcgaWQ9IlNoYXBlXzE3MiI+DQoJPGc+DQoJCTxwYXRoIGNsYXNzPSJzdDAiIGQ9Ik03MCwxMkgxNGMtNC40LDAtOCwzLjYtOCw4djM0YzAsNC40LDMuNiw4LDgsOGg1NmM0LjQsMCw4LTMuNiw4LThWMjBDNzgsMTUuNiw3NC40LDEyLDcwLDEyeiIvPg0KCTwvZz4NCjwvZz4NCjxnIGlkPSJTaGFwZV8xNzJfY29weV80Ij4NCgk8Zz4' +
            'NCgkJPHBhdGggY2xhc3M9InN0MSIgZD0iTTc1LjgsMTQuNUw0MiwzOS4xTDguMiwxNC41Yy0wLjksMS0xLjYsMi4yLTEuOSwzLjVMNDIsNDRsMzUuNy0yNS45Qzc3LjQsMTYuNyw3Ni43LDE1LjUsNzUuOCwxNC41eiIvPg0KCTwvZz4NCjwvZz4NCjxnIGlkPSJTaGFwZV8xNzJfY29weSI+DQoJPGc+DQoJCTxwYXRoIGNsYXNzPSJzdDIiIGQ9Ik04MiwzNEgyNmMtNC40LDAtOCwzLjYtOCw4djM0YzAsNC40LDMuNiw4LDgsOGg1NmM0LjQsMCw4LTMuNiw4LThWNDJDOTAsMzcuNiw4Ni40LDM0LDgyLDM0eiIvPg0KCTwvZz4NCjwvZz4NCjxnIGlkPSJTaGFwZV8xNzJfY29weV8yIj4NCgk8Zz4NCgkJPHBhdGggY2xhc3M9InN0MCIgZD0iTTg3LjgsMzYuNUw1NCw2MS4xTDIwLjIsMzYuNWMtMC45LDEtMS42LDIuMi0xLjksMy41TDU0LDY2bDM1LjctMjUuOUM4OS40LDM4LjcsODguNywzNy41LDg3LjgsMzYuNXoiLz4NCgk8L2c+DQo8L2c+DQo8L3N2Zz4NCg==',
    },
    {
        'class': 'eswarning',
        'title': 'Xác nhận thông tin',
        'description': 'Bạn có chắc chắn xóa ',
        'value': 'PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjwhLS0gR2VuZXJhdG9yOiBBZG9iZSBJbGx1c3RyYXRvciAyMS4xLjAsIFNWRyBFeHBvcnQgUGx1Zy1JbiAuIFNWRyBWZXJzaW9uOiA2LjAwIEJ1aWxkIDApICAtLT4NCjxzdmcgdmVyc2lvbj0iMS4xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCINCgkgdmlld0JveD0iMCAwIDk2IDk2IiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCA5NiA5NjsiIHhtbDpzcGFjZT0icHJlc2VydmUiPg0KPHN0eWxlIHR5cGU9InRleHQvY3NzIj4NCgkuc3Qwe2VuYWJsZS1iYWNrZ3JvdW5kOm5ldyAgICA7fQ0KCS5zdDF7ZmlsbC1ydWxlOmV2ZW5vZGQ7Y2xpcC1ydWxlOmV2ZW5vZGQ7ZmlsbDojRjM1ODYzO30NCgkuc3Qye2ZpbGwtcnVsZTpldmVub2RkO2NsaXAtcnVsZTpldmVub2RkO2ZpbGw6I0ZGRkZGRjt9DQo8L3N0eWxlPg0KPGcgaWQ9IlNoYXBlXzE3M18xXyIgY2xhc3M9InN0MCI+DQoJPGcgaWQ9IlNoYXBlXzE3MyI+DQoJCTxnPg0KCQkJPHBvbHlnb24gY2xhc3M9InN0MSIgcG9pbnRzPSI2OCwxNCAyNy45LDE0IDcuOSw0OCAyNy45LDgyIDY4LDgyIDg4LDQ4IAkJCSIvPg0KCQk8L2c+DQoJPC9nPg0KPC9nPg0KPGcgaWQ9IlNoYXBlXzE3M19jb3B5XzFfIiBjbGFzcz0ic3QwIj4NCgk8ZyBpZD0iU2hhcGVfMTczX2NvcHk' +
            'iPg0KCQk8Zz4NCgkJCTxwYXRoIGNsYXNzPSJzdDIiIGQ9Ik00My45LDI4djI2aDhWMjhINDMuOXogTTQzLjksNjhoOHYtOGgtOFY2OHoiLz4NCgkJPC9nPg0KCTwvZz4NCjwvZz4NCjwvc3ZnPg0K',
    },
    {
        'class': 'eswarning_v2',
        'title': 'Xác nhận thông tin',
        'description': '',
        'value': 'PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjwhLS0gR2VuZXJhdG9yOiBBZG9iZSBJbGx1c3RyYXRvciAyMS4xLjAsIFNWRyBFeHBvcnQgUGx1Zy1JbiAuIFNWRyBWZXJzaW9uOiA2LjAwIEJ1aWxkIDApICAtLT4NCjxzdmcgdmVyc2lvbj0iMS4xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCINCgkgdmlld0JveD0iMCAwIDk2IDk2IiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCA5NiA5NjsiIHhtbDpzcGFjZT0icHJlc2VydmUiPg0KPHN0eWxlIHR5cGU9InRleHQvY3NzIj4NCgkuc3Qwe2VuYWJsZS1iYWNrZ3JvdW5kOm5ldyAgICA7fQ0KCS5zdDF7ZmlsbC1ydWxlOmV2ZW5vZGQ7Y2xpcC1ydWxlOmV2ZW5vZGQ7ZmlsbDojRjM1ODYzO30NCgkuc3Qye2ZpbGwtcnVsZTpldmVub2RkO2NsaXAtcnVsZTpldmVub2RkO2ZpbGw6I0ZGRkZGRjt9DQo8L3N0eWxlPg0KPGcgaWQ9IlNoYXBlXzE3M18xXyIgY2xhc3M9InN0MCI+DQoJPGcgaWQ9IlNoYXBlXzE3MyI+DQoJCTxnPg0KCQkJPHBvbHlnb24gY2xhc3M9InN0MSIgcG9pbnRzPSI2OCwxNCAyNy45LDE0IDcuOSw0OCAyNy45LDgyIDY4LDgyIDg4LDQ4IAkJCSIvPg0KCQk8L2c+DQoJPC9nPg0KPC9nPg0KPGcgaWQ9IlNoYXBlXzE3M19jb3B5XzFfIiBjbGFzcz0ic3QwIj4NCgk8ZyBpZD0iU2hhcGVfMTczX2NvcHk' +
            'iPg0KCQk8Zz4NCgkJCTxwYXRoIGNsYXNzPSJzdDIiIGQ9Ik00My45LDI4djI2aDhWMjhINDMuOXogTTQzLjksNjhoOHYtOGgtOFY2OHoiLz4NCgkJPC9nPg0KCTwvZz4NCjwvZz4NCjwvc3ZnPg0K',
    },
    {
        'class': 'eswarning_v3',
        'title': 'Cảnh báo',
        'description': '',
        'value': 'PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjwhLS0gR2VuZXJhdG9yOiBBZG9iZSBJbGx1c3RyYXRvciAyMS4xLjAsIFNWRyBFeHBvcnQgUGx1Zy1JbiAuIFNWRyBWZXJzaW9uOiA2LjAwIEJ1aWxkIDApICAtLT4NCjxzdmcgdmVyc2lvbj0iMS4xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCINCgkgdmlld0JveD0iMCAwIDk2IDk2IiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCA5NiA5NjsiIHhtbDpzcGFjZT0icHJlc2VydmUiPg0KPHN0eWxlIHR5cGU9InRleHQvY3NzIj4NCgkuc3Qwe2VuYWJsZS1iYWNrZ3JvdW5kOm5ldyAgICA7fQ0KCS5zdDF7ZmlsbC1ydWxlOmV2ZW5vZGQ7Y2xpcC1ydWxlOmV2ZW5vZGQ7ZmlsbDojRjM1ODYzO30NCgkuc3Qye2ZpbGwtcnVsZTpldmVub2RkO2NsaXAtcnVsZTpldmVub2RkO2ZpbGw6I0ZGRkZGRjt9DQo8L3N0eWxlPg0KPGcgaWQ9IlNoYXBlXzE3M18xXyIgY2xhc3M9InN0MCI+DQoJPGcgaWQ9IlNoYXBlXzE3MyI+DQoJCTxnPg0KCQkJPHBvbHlnb24gY2xhc3M9InN0MSIgcG9pbnRzPSI2OCwxNCAyNy45LDE0IDcuOSw0OCAyNy45LDgyIDY4LDgyIDg4LDQ4IAkJCSIvPg0KCQk8L2c+DQoJPC9nPg0KPC9nPg0KPGcgaWQ9IlNoYXBlXzE3M19jb3B5XzFfIiBjbGFzcz0ic3QwIj4NCgk8ZyBpZD0iU2hhcGVfMTczX2NvcHk' +
            'iPg0KCQk8Zz4NCgkJCTxwYXRoIGNsYXNzPSJzdDIiIGQ9Ik00My45LDI4djI2aDhWMjhINDMuOXogTTQzLjksNjhoOHYtOGgtOFY2OHoiLz4NCgkJPC9nPg0KCTwvZz4NCjwvZz4NCjwvc3ZnPg0K',
    },
    {
        'class': 'esadduser',
        'title': 'Thông báo',
        'description': '',
        'value': 'PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjwhLS0gR2VuZXJhdG9yOiBBZG9iZSBJbGx1c3RyYXRvciAyMS4xLjAsIFNWRyBFeHBvcnQgUGx1Zy1JbiAuIFNWRyBWZXJzaW9uOiA2LjAwIEJ1aWxkIDApICAtLT4NCjxzdmcgdmVyc2lvbj0iMS4xIiBpZD0iR3JvdXBfMV8xXyIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgeD0iMHB4IiB5PSIwcHgiDQoJIHZpZXdCb3g9IjAgMCA5NiA5NiIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgOTYgOTY7IiB4bWw6c3BhY2U9InByZXNlcnZlIj4NCjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+DQoJLnN0MHtmaWxsLXJ1bGU6ZXZlbm9kZDtjbGlwLXJ1bGU6ZXZlbm9kZDtmaWxsOiM0RkMzRjc7fQ0KCS5zdDF7ZmlsbC1ydWxlOmV2ZW5vZGQ7Y2xpcC1ydWxlOmV2ZW5vZGQ7ZmlsbDojRkY5ODAwO30NCgkuc3Qye2ZpbGwtcnVsZTpldmVub2RkO2NsaXAtcnVsZTpldmVub2RkO2ZpbGw6IzAxNTc5Qjt9DQoJLnN0M3tmaWxsLXJ1bGU6ZXZlbm9kZDtjbGlwLXJ1bGU6ZXZlbm9kZDtmaWxsOiNGRkI3NEQ7fQ0KCS5zdDR7ZmlsbC1ydWxlOmV2ZW5vZGQ7Y2xpcC1ydWxlOmV2ZW5vZGQ7ZmlsbDojNDI0MjQyO30NCgkuc3Q1e2ZpbGwtcnVsZTpldmVub2RkO2NsaXAtcnVsZTpldmVub2RkO2ZpbGw6Izc4NDcxOTt9DQoJLnN0NntmaWxsLXJ1bGU6ZXZlbm9kZDt' +
            'jbGlwLXJ1bGU6ZXZlbm9kZDtmaWxsOiM0M0EwNDc7fQ0KCS5zdDd7ZmlsbC1ydWxlOmV2ZW5vZGQ7Y2xpcC1ydWxlOmV2ZW5vZGQ7ZmlsbDojRkZGRkZGO30NCjwvc3R5bGU+DQo8ZyBpZD0iU2hhcGVfMTc1Ij4NCgk8Zz4NCgkJPHBhdGggY2xhc3M9InN0MCIgZD0iTTUxLjksNThjMCwwLTIsOC0xMCw4Yy04LDAtMTAtOC0xMC04cy0yMiw0LTIyLDI2aDYzLjlDNzMuOSw2Miw1MS45LDU4LDUxLjksNTh6Ii8+DQoJPC9nPg0KPC9nPg0KPGcgaWQ9IlNoYXBlXzE3NV9jb3B5XzkiPg0KCTxnPg0KCQk8cGF0aCBjbGFzcz0ic3QxIiBkPSJNNTkuOSwzMHYtOGMwLTE1LjMtMzUuOS05LjktMzUuOSwwdjhjLTIuMiwwLTQsMS44LTQsNGMwLDIuMiwxLjgsNCw0LDRjMCwwLDAuMSwwLDAuMSwwYzEsOSw4LjYsMTYsMTcuOSwxNg0KCQkJczE2LjktNywxNy45LTE2YzAsMCwwLjEsMCwwLjEsMGMyLjIsMCw0LTEuOCw0LTRDNjMuOSwzMS44LDYyLjEsMzAsNTkuOSwzMHoiLz4NCgk8L2c+DQo8L2c+DQo8ZyBpZD0iU2hhcGVfMTc1X2NvcHlfNyI+DQoJPGc+DQoJCTxwYXRoIGNsYXNzPSJzdDEiIGQ9Ik01NS44LDU5LjFjLTIuMy0wLjgtMy45LTEuMS0zLjktMS4xVjQ2SDMydjEyYzAsMC0xLjYsMC4zLTMuOSwxLjFjMC40LDIuOSwzLjcsMTAuMywxMi44LDEwLjgNCgkJCWMwLjMsMCwwLjcsMC4xLDEuMSwwLjFjMC40LDAsMC43LDAsMS4xLTAuMUM1Mi4yLDY5LjQsNTUuNCw2Miw1NS44LDU5LjF6Ii8+DQoJPC9nPg0KPC9nPg0KPGcgaW' +
            'Q9IlNoYXBlXzE3NV9jb3B5XzYiPg0KCTxnPg0KCQk8cGF0aCBjbGFzcz0ic3QyIiBkPSJNNTEuOSw1OGMwLDAtMiw4LTEwLDhjLTgsMC0xMC04LTEwLThzLTEuNiwwLjMtMy45LDEuMUMyOC41LDYyLjEsMzIsNzAsNDIsNzBjMTAsMCwxMy41LTcuOSwxMy45LTEwLjkNCgkJCUM1My41LDU4LjMsNTEuOSw1OCw1MS45LDU4eiIvPg0KCTwvZz4NCjwvZz4NCjxnIGlkPSJTaGFwZV8xNzVfY29weV84Ij4NCgk8Zz4NCgkJPHBhdGggY2xhc3M9InN0MyIgZD0iTTI0LDIydjE0YzAsOS45LDgsMTgsMTgsMThjOS45LDAsMTgtOC4xLDE4LTE4VjIyQzU5LjksNi43LDI0LDEyLjEsMjQsMjJ6Ii8+DQoJPC9nPg0KPC9nPg0KPGcgaWQ9IlNoYXBlXzE3NV9jb3B5XzEwIj4NCgk8Zz4NCgkJPHBhdGggY2xhc3M9InN0NCIgZD0iTTQ5LjksOGwtMi00aC02Yy0xMi4xLDAtMjAsOS45LTIwLDIydjQuNmw0LDMuNFYyNGwyNC04bDgsOHYxMGw0LTMuNVYyNkM2MS45LDE4LDU5LjgsMTAsNDkuOSw4eiIvPg0KCTwvZz4NCjwvZz4NCjxnIGlkPSJTaGFwZV8xNzVfY29weV8xMSI+DQoJPGc+DQoJCTxwYXRoIGNsYXNzPSJzdDUiIGQ9Ik0zNCwzMmMtMS4xLDAtMiwwLjktMiwyYzAsMS4xLDAuOSwyLDIsMmMxLjEsMCwyLTAuOSwyLTJDMzYsMzIuOSwzNS4xLDMyLDM0LDMyeiBNNDkuOSwzMmMtMS4xLDAtMiwwLjktMiwyDQoJCQljMCwxLjEsMC45LDIsMiwyYzEuMSwwLDItMC45LDItMkM1MS45LDMyLjksNTEsMzIsNDkuOSwzMnoiLz4NCgk8L2c+D' +
            'Qo8L2c+DQo8ZyBpZD0iU2hhcGVfMTc1X2NvcHlfMTIiPg0KCTxnPg0KCQk8ZWxsaXBzZSBjbGFzcz0ic3Q2IiBjeD0iNjUuOSIgY3k9IjcyIiByeD0iMjAiIHJ5PSIyMCIvPg0KCTwvZz4NCjwvZz4NCjxnIGlkPSJTaGFwZV8xNzVfY29weV8xMyI+DQoJPGc+DQoJCTxwb2x5Z29uIGNsYXNzPSJzdDciIHBvaW50cz0iNzcuOSw2OCA2OS45LDY4IDY5LjksNjAgNjEuOSw2MCA2MS45LDY4IDUzLjksNjggNTMuOSw3NiA2MS45LDc2IDYxLjksODQgNjkuOSw4NCA2OS45LDc2IDc3LjksNzYgDQoJCQkJCSIvPg0KCTwvZz4NCjwvZz4NCjwvc3ZnPg0K',
    },
];
var ESConfirm = (function (d, t, callback, noAction) {
    try {
        var _innerText = '';
        var _liButton = [];
        OnESConfirm.push({
            'value': ('ES' + (new Date()).getTime()),
            'active': null,
        });
        _innerText = '<div class="modal fade custom-modal show" >' +
            '<div class="modal-dialog" role="document">' +
            '<button type="button" class="close custom-modal__close" data-dismiss="modal" aria-label="Close">' +
            '<span aria-hidden="true" class="ua-icon-modal-close"></span>' +
            '</button>' +
            '<div id="' +
            OnESConfirm[(OnESConfirm.length - 1)].value + '-content' + '" ' +
            'class="modal-content">' +
            '<div style="background: white;" class="modal-header custom-modal__image" modal-drag>';
        for (var i = 0; i < ESConfirmIcon.length; i++) {
            if (ESConfirmIcon[i].class.toUpperCase() === t.class.toUpperCase()) {
                if (t.classvalue === undefined) {
                    _innerText += '<img width="80" height="90" src="data:image/svg+xml;base64,' + ESConfirmIcon[i].value + '" class="' + ESConfirmIcon[i].class + '">' +
                        '</div>' +
                        '<div class="modal-body custom-modal__body">' +
                        '<h4 class="custom-modal__body-heading">' +
                        ESConfirmIcon[i].title +
                        '</h4>' +
                        '<div class="custom-modal__body-desc">' +
                        ESConfirmIcon[i].description + t.description +
                        '</div>' +
                        '</div>' +
                        '<div class="modal-footer">' +
                        '<div class="custom-modal__buttons">';
                } else {
                    _innerText += '<img class="' + t.class + '">' +
                        '</div>' +
                        '<div class="modal-body custom-modal__body">' +
                        '<h4 class="custom-modal__body-heading">' +
                        t.title +
                        '</h4>' +
                        '<div class="custom-modal__body-desc">' +
                        t.description +
                        '</div>' +
                        '</div>' +
                        '<div class="modal-footer">' +
                        '<div class="custom-modal__buttons">';
                }
                break;
            }
        }
        ;
        for (i = 0; i < d.length; i++) {
            _liButton.push(((new Date()).getTime() + i));
            _innerText += ('<button type="button" name="' + OnESConfirm[(OnESConfirm.length - 1)].value + '" class="btn btn-' + d[i].type + '" value="' + d[i].value + '" id="ESActionRS' + (_liButton[_liButton.length - 1]) + '">' + d[i].title + '</button>');
        }
        _innerText += ('</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>');
        var _el = document.createElement('div');
        _el.id = OnESConfirm[(OnESConfirm.length - 1)].value;
        _el.className = "es-tm-css";
        _el.innerHTML = _innerText;
        document.body.appendChild(_el);
        for (i = 0; i < _liButton.length; i++) {
            var __el = document.getElementById(('ESActionRS' + _liButton[i]));
            __el.addEventListener('click', function () {
                callback(this.value);
                for (i = OnESConfirm.length - 1; i >= 0; i--) {
                    if (OnESConfirm[i].value === this.name) {
                        OnESConfirm[i].active = true;
                        break;
                    }
                }
            });
        }
        _el = document.getElementById(OnESConfirm[(OnESConfirm.length - 1)].value);
        var _elChild = _el.firstChild;
        _elChild.addEventListener("click", function (e) {
            if (OnESConfirm.length > 0) {
                for (i = OnESConfirm.length - 1; i >= 0; i--) {
                    var _el = document.getElementById(OnESConfirm[i].value + '-content');
                    if (_el.contains(e.target) && OnESConfirm[i].active !== true) {
                        break;
                    } else {
                        document.getElementById(OnESConfirm[i].value).remove();
                        if (OnESConfirm[i].active === null) {
                            noAction();
                        }
                        OnESConfirm.splice(i, 1);
                        break;
                    }
                }
            }
        });
    } catch (ex) {
        console.log('Error comfirm : ' + ex);
    }
});
////Example using
//Thông báo xác nhận
////Example using
//Thông báo xác nhận
//ESConfirm([{ 'value': 1, 'title': 'Xác nhận', 'type': 'info' }, { 'value': 2, 'title': 'Đóng', 'type': 'danger' }], { 'description': 'dữ liệu xyz', 'class': 'eswarning' }, function (rs) {
//    if (rs === '1') {
//        console.log('Chấp nhận');
//    }
//    if (rs === '2') {
//        console.log('Không chấp nhận');
//    }
//}, function () {
//    //some error in this function
//    console.log('Không lựa chọn');
//    });
////Thông báo cảnh báo
//ESConfirm([{ 'value': 1, 'title': 'Xác nhận', 'type': 'info' }], { 'description': 'nội dung thông báo', 'class': 'escheck' }, function (rs) {
//    if (rs === '1') {
//        console.log('Chấp nhận');
//    }
//    if (rs === '2') {
//        console.log('Không chấp nhận');
//    }
//}, function () {
//    //some error in this function
//    console.log('Không lựa chọn');
//});
/* begin using fo minize menuleft*/
function toggleMenuminize() {
    $('body').toggleClass('sidebar-sm');
    var _el = $('.menuLeft-SubnavChild');
    for (var i = 0; i < _el.length; i++) {
        $(_el[i]).toggleClass('sidebar-section-subnav');
        $(_el[i]).toggleClass('sidebar-section-nav');
    }
    if ($('body').hasClass('sidebar-sm')) {
        localStorage.setItem('ESVN-MENU-LEFT', '1');
        document.getElementsByClassName("minize-menuleft")[0].setAttribute("style", "display:contents;margin: 10px;");
        document.getElementById("minize-menuleft").setAttribute("style", "margin: 14px;");
    } else {
        localStorage.setItem('ESVN-MENU-LEFT', '0');
        document.getElementsByClassName("minize-menuleft")[0].removeAttribute("style");
        document.getElementById("minize-menuleft").removeAttribute("style");

    }
    $('#minize-menuleft').toggleClass('mdi mdi-arrow-right-bold-circle-outline');
    $('#minize-menuleft').toggleClass('mdi mdi-arrow-left-bold-circle-outline');
}
function toggleMenuminize_init() {
    var _status_menu = localStorage.getItem('ESVN-MENU-LEFT');
    if (_status_menu === '1') {
        toggleMenuminize();
    }
}
toggleMenuminize_init();
/* end using fo minize menuleft*/

//End create by habx@esvn.com.vn