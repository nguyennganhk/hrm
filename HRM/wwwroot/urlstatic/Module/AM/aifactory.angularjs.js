﻿///*********************
/// Using in angularjs 
/// Have function:
/// + using jexcel with namespace ngJexcel
/// + using sumoselect with namespace ngSmselect
/// + using option date with namespace ngOptionDate
/// You can find any support 'ng-aimodu' when search keyword 'ng-aimodu' on google.com.vn
///*********************
//Begin thiết đặt loại tiền tệ quốc gia cho jexcel
var LANGUAGE_CONFIG = 'VN';
//End thiết đặt loại tiền tệ quốc gia cho jexcel
//Begin dùng cho menu chuột phải
var MENURIGHT_ACTIVE = false;
typeof (CONFIGSETTING_OBJECT) === 'undefined' ? CONFIGSETTING_OBJECT = {} : '';
//End dùng cho menu chuột phải
var aimodu = angular.module('ng-aimodu', []);
aimodu.directive("filesInput", function () {
    return {
        require: "ngModel",
        link: function postLink(scope, elem, attrs, ngModel) {
            elem.on("change", function (e) {
                var files = elem[0].files;
                ngModel.$setViewValue(files);
            })
        }
    }
})
aimodu.filter('reverse', function () {
    return function (items) {
        if (items !== undefined && items !== null) {
            return items.slice().reverse();
        } else {
            return items;
        }
    };
});
aimodu.directive('onEnter', function () {
    return {
        scope: { onEnter: '&' },
        link: function (scope, element) {
            element.bind("keydown", function (e) {
                if (e.which === 13 && !e.shiftKey) {
                    scope.onEnter();
                    scope.$apply();
                }
            });
        }
    }
});
aimodu.directive('file', function () {
    return {
        restrict: 'E',
        //template: '<input type="file" />',
        replace: true,
        //require: 'ngModel',
        link: function (scope, element, attr, ctrl) {
            var listener = function () {
                scope.$apply(function () {
                    attr.multiple ? ctrl.$setViewValue(element[0].files) : ctrl.$setViewValue(element[0].files[0]);
                });
            }
            element.bind('change', listener);
        }
    }
});
aimodu.directive('numOnly', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, modelCtrl) {

            modelCtrl.$parsers.push(function (inputValue) {
                var transformedInput = inputValue ? inputValue.replace(/[^\d.-]/g, '') : null;

                if (transformedInput != inputValue) {
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                }

                return transformedInput;
            });
        }
    };
});
aimodu.directive('ngOnlyNumber', ['$filter', function ($filter) {
    return {
        restrict: 'EAC',
        scope: {
            'ngData': '=ngData',
            'ngCall': '=ngCall',
            'ngInit': '=ngInit'
        },
        template: '',
        link: function (scope, elem, attrs, ctrl) {
            var _el = elem;
            var _pendding = false;
            var _first = true;
            var _oldVal = 0;
            var _newVal = 0;
            if (scope.ngData !== undefined && scope.ngData !== null) {
                $(_el[0]).val(scope.ngData);
            }
            scope.checkValue = function () {
                if (scope.ngInit !== undefined && scope.ngInit !== null) {
                    if (scope.ngInit.max !== undefined && scope.ngInit.max) {
                        if (_newVal > scope.ngInit.max) {
                            $(_el[0]).val(scope.ngInit.max);
                            return false;
                        }
                    }
                    if (scope.ngInit.min !== undefined && scope.ngInit.min) {
                        if (_newVal < scope.ngInit.min) {
                            $(_el[0]).val(scope.ngInit.min);
                            return false;
                        }
                    }
                }
                return true;
            };
            $(elem).keydown(function (d) {
                AllowOnlyNumbers(d, true, this);
                _oldVal = parseFloat((this.value.toString().replaceAll('[.]', '')).replaceAll('[,]', '.'));
            });
            $(elem).on('keyup', function (d) {
                AllowOnlyNumbers(d, true, this);
                if (this.value !== '' && this.value !== undefined && this.value.indexOf(',') !== -1 && this.value.split(',')[1] === '') {
                    return;
                }
                _newVal = parseFloat((this.value.toString().replaceAll('[.]', '')).replaceAll('[,]', '.'));
                if (!scope.checkValue()) {
                    return;
                }
                _pendding = true;
                changeInputMoney(d, this, true);
                if (this.value.split(',').length > 1) {
                    if (this.value.split(',')[1] === '') {
                        this.value = this.value + '0';
                    } else {
                        if (this.value.split(',')[0] === '') {
                            this.value = '0' + this.value;
                        }
                    }
                }
                scope.ngData = parseFloat((this.value.toString().replaceAll('[.]', '')).replaceAll('[,]', '.'));
                scope.ngOnlyNumberChangeModel();
                if (scope.ngCall !== undefined)
                    scope.ngCall();
                if (scope.$root.$$phase !== '$apply' && scope.$root.$$phase !== '$digest') {
                    scope.$apply();
                }
            });
            $(elem).on('keypress', function () {
                $(elem).removeAttr('dir');
                if (scope.ngCall !== undefined)
                    scope.ngCall('leave');
            });
            $(elem).on('focusout', function () {
                $(elem).attr('dir', "rtl");
                scope.ngData = parseFloat((this.value.toString().replaceAll('[.]', '')).replaceAll('[,]', '.'));
                scope.ngOnlyNumberChangeModel();
                if (scope.$root.$$phase !== '$apply' && scope.$root.$$phase !== '$digest') {
                    scope.$apply();
                }
            });
            $(elem).on('click', function () {
                $(elem).attr('dir', "rtl");
            });
            scope.ngOnlyNumberChangeModel = function () {
                if (scope.ngData === null || scope.ngData === undefined || (scope.ngData).toString().toUpperCase() === 'NaN'.toUpperCase()) {
                    scope.ngData = 0;
                    if (scope.ngCall !== undefined)
                        scope.ngCall();
                }
                if (typeof (scope.ngData) === 'string') {
                    scope.ngData = parseFloat((scope.ngData.replaceAll('[.]', '')));
                }
                _el[0].value = convertMoneyDecimal(((scope.ngData).toString().replaceAll('[.]', ',')));
                if (scope.$root.$$phase !== '$apply' && scope.$root.$$phase !== '$digest') {
                    scope.$apply();
                }
            };
            scope.$watch('ngData', function (n, ol) {
                if (_first) {
                    _first = undefined;
                } else {
                    if (scope.ngCall !== undefined)
                        scope.ngCall();
                    if (scope.$root.$$phase !== '$apply' && scope.$root.$$phase !== '$digest') {
                        scope.$apply();
                    }
                }
                if (_pendding) {
                    _pendding = false;
                    return '';
                }
                scope.ngOnlyNumberChangeModel();
            }, true);
        }
    };
}]);
aimodu.directive('ngNonUnicode', ['$filter', function ($filter) {
    return {
        restrict: 'EAC',
        scope: {
            'ngModel': '=',
        },
        template: '',
        link: function (scope, elem, attrs, ctrl) {
            var Latinise = {}; Latinise.latin_map = {
                "Á": "A", "Ă": "A", "Ắ": "A",
                "Ặ": "A", "Ằ": "A", "Ẳ": "A", "Ẵ": "A",
                "Ǎ": "A", "Â": "A", "Ấ": "A", "Ậ": "A",
                "Ầ": "A", "Ẩ": "A", "Ẫ": "A", "Ä": "A",
                "Ǟ": "A", "Ȧ": "A", "Ǡ": "A", "Ạ": "A",
                "Ȁ": "A", "À": "A", "Ả": "A", "Ȃ": "A",
                "Ā": "A", "Ą": "A", "Å": "A", "Ǻ": "A",
                "Ḁ": "A", "Ⱥ": "A", "Ã": "A", "Ꜳ": "AA", "Æ": "AE",
                "Ǽ": "AE", "Ǣ": "AE", "Ꜵ": "AO", "Ꜷ": "AU",
                "Ꜹ": "AV", "Ꜻ": "AV", "Ꜽ": "AY", "Ḃ": "B", "Ḅ": "B",
                "Ɓ": "B", "Ḇ": "B", "Ƀ": "B", "Ƃ": "B", "Ć": "C",
                "Č": "C", "Ç": "C", "Ḉ": "C", "Ĉ": "C", "Ċ": "C",
                "Ƈ": "C", "Ȼ": "C", "Ď": "D", "Ḑ": "D", "Ḓ": "D", "Ḋ": "D", "Ḍ": "D",
                "Ɗ": "D", "Ḏ": "D", "ǲ": "D", "ǅ": "D", "Đ": "D", "Ƌ": "D", "Ǳ": "DZ",
                "Ǆ": "DZ", "É": "E", "Ĕ": "E", "Ě": "E", "Ȩ": "E", "Ḝ": "E", "Ê": "E",
                "Ế": "E", "Ệ": "E", "Ề": "E", "Ể": "E", "Ễ": "E", "Ḙ": "E", "Ë": "E",
                "Ė": "E", "Ẹ": "E", "Ȅ": "E", "È": "E", "Ẻ": "E", "Ȇ": "E", "Ē": "E",
                "Ḗ": "E", "Ḕ": "E", "Ę": "E", "Ɇ": "E", "Ẽ": "E", "Ḛ": "E", "Ꝫ": "ET",
                "Ḟ": "F", "Ƒ": "F", "Ǵ": "G", "Ğ": "G", "Ǧ": "G", "Ģ": "G", "Ĝ": "G",
                "Ġ": "G", "Ɠ": "G", "Ḡ": "G", "Ǥ": "G", "Ḫ": "H", "Ȟ": "H", "Ḩ": "H",
                "Ĥ": "H", "Ⱨ": "H", "Ḧ": "H", "Ḣ": "H", "Ḥ": "H", "Ħ": "H", "Í": "I",
                "Ĭ": "I", "Ǐ": "I", "Î": "I", "Ï": "I", "Ḯ": "I", "İ": "I", "Ị": "I",
                "Ȉ": "I", "Ì": "I", "Ỉ": "I", "Ȋ": "I", "Ī": "I", "Į": "I", "Ɨ": "I",
                "Ĩ": "I", "Ḭ": "I", "Ꝺ": "D", "Ꝼ": "F", "Ᵹ": "G", "Ꞃ": "R", "Ꞅ": "S",
                "Ꞇ": "T", "Ꝭ": "IS", "Ĵ": "J", "Ɉ": "J", "Ḱ": "K", "Ǩ": "K", "Ķ": "K",
                "Ⱪ": "K", "Ꝃ": "K", "Ḳ": "K", "Ƙ": "K", "Ḵ": "K", "Ꝁ": "K", "Ꝅ": "K",
                "Ĺ": "L", "Ƚ": "L", "Ľ": "L", "Ļ": "L", "Ḽ": "L", "Ḷ": "L", "Ḹ": "L",
                "Ⱡ": "L", "Ꝉ": "L", "Ḻ": "L", "Ŀ": "L", "Ɫ": "L", "ǈ": "L", "Ł": "L",
                "Ǉ": "LJ", "Ḿ": "M", "Ṁ": "M", "Ṃ": "M", "Ɱ": "M", "Ń": "N", "Ň": "N",
                "Ņ": "N", "Ṋ": "N", "Ṅ": "N", "Ṇ": "N", "Ǹ": "N", "Ɲ": "N", "Ṉ": "N",
                "Ƞ": "N", "ǋ": "N", "Ñ": "N", "Ǌ": "NJ", "Ó": "O", "Ŏ": "O", "Ǒ": "O", "Ô": "O",
                "Ố": "O", "Ộ": "O", "Ồ": "O", "Ổ": "O", "Ỗ": "O", "Ö": "O", "Ȫ": "O", "Ȯ": "O", "Ȱ": "O",
                "Ọ": "O", "Ő": "O", "Ȍ": "O", "Ò": "O", "Ỏ": "O", "Ơ": "O", "Ớ": "O", "Ợ": "O", "Ờ": "O",
                "Ở": "O", "Ỡ": "O", "Ȏ": "O", "Ꝋ": "O", "Ꝍ": "O", "Ō": "O", "Ṓ": "O", "Ṑ": "O", "Ɵ": "O", "Ǫ": "O",
                "Ǭ": "O", "Ø": "O", "Ǿ": "O", "Õ": "O", "Ṍ": "O", "Ṏ": "O", "Ȭ": "O", "Ƣ": "OI", "Ꝏ": "OO", "Ɛ": "E",
                "Ɔ": "O", "Ȣ": "OU", "Ṕ": "P", "Ṗ": "P", "Ꝓ": "P", "Ƥ": "P", "Ꝕ": "P", "Ᵽ": "P", "Ꝑ": "P", "Ꝙ": "Q",
                "Ꝗ": "Q", "Ŕ": "R", "Ř": "R", "Ŗ": "R", "Ṙ": "R", "Ṛ": "R", "Ṝ": "R", "Ȑ": "R", "Ȓ": "R", "Ṟ": "R", "Ɍ": "R",
                "Ɽ": "R", "Ꜿ": "C", "Ǝ": "E", "Ś": "S", "Ṥ": "S", "Š": "S", "Ṧ": "S", "Ş": "S", "Ŝ": "S", "Ș": "S", "Ṡ": "S",
                "Ṣ": "S", "Ṩ": "S", "Ť": "T", "Ţ": "T", "Ṱ": "T", "Ț": "T", "Ⱦ": "T", "Ṫ": "T", "Ṭ": "T", "Ƭ": "T", "Ṯ": "T",
                "Ʈ": "T", "Ŧ": "T", "Ɐ": "A", "Ꞁ": "L", "Ɯ": "M", "Ʌ": "V", "Ꜩ": "TZ", "Ú": "U", "Ŭ": "U", "Ǔ": "U", "Û": "U", "Ṷ": "U",
                "Ü": "U", "Ǘ": "U", "Ǚ": "U", "Ǜ": "U", "Ǖ": "U", "Ṳ": "U", "Ụ": "U", "Ű": "U", "Ȕ": "U", "Ù": "U", "Ủ": "U", "Ư": "U",
                "Ứ": "U", "Ự": "U", "Ừ": "U", "Ử": "U", "Ữ": "U", "Ȗ": "U", "Ū": "U", "Ṻ": "U", "Ų": "U", "Ů": "U", "Ũ": "U", "Ṹ": "U",
                "Ṵ": "U", "Ꝟ": "V", "Ṿ": "V", "Ʋ": "V", "Ṽ": "V", "Ꝡ": "VY", "Ẃ": "W", "Ŵ": "W", "Ẅ": "W", "Ẇ": "W", "Ẉ": "W", "Ẁ": "W",
                "Ⱳ": "W", "Ẍ": "X", "Ẋ": "X", "Ý": "Y", "Ŷ": "Y", "Ÿ": "Y", "Ẏ": "Y", "Ỵ": "Y", "Ỳ": "Y", "Ƴ": "Y", "Ỷ": "Y", "Ỿ": "Y", "Ȳ": "Y",
                "Ɏ": "Y", "Ỹ": "Y", "Ź": "Z", "Ž": "Z", "Ẑ": "Z", "Ⱬ": "Z", "Ż": "Z", "Ẓ": "Z", "Ȥ": "Z", "Ẕ": "Z", "Ƶ": "Z", "Ĳ": "IJ", "Œ": "OE",
                "ᴀ": "A", "ᴁ": "AE", "ʙ": "B", "ᴃ": "B", "ᴄ": "C", "ᴅ": "D", "ᴇ": "E", "ꜰ": "F", "ɢ": "G", "ʛ": "G", "ʜ": "H", "ɪ": "I", "ʁ": "R",
                "ᴊ": "J", "ᴋ": "K", "ʟ": "L", "ᴌ": "L", "ᴍ": "M", "ɴ": "N", "ᴏ": "O", "ɶ": "OE", "ᴐ": "O", "ᴕ": "OU", "ᴘ": "P", "ʀ": "R", "ᴎ": "N",
                "ᴙ": "R", "ꜱ": "S", "ᴛ": "T", "ⱻ": "E", "ᴚ": "R", "ᴜ": "U", "ᴠ": "V", "ᴡ": "W", "ʏ": "Y", "ᴢ": "Z", "á": "a", "ă": "a", "ắ": "a",
                "ặ": "a", "ằ": "a", "ẳ": "a", "ẵ": "a", "ǎ": "a", "â": "a", "ấ": "a", "ậ": "a", "ầ": "a", "ẩ": "a", "ẫ": "a", "ä": "a",
                "ǟ": "a", "ȧ": "a", "ǡ": "a", "ạ": "a", "ȁ": "a", "à": "a", "ả": "a", "ȃ": "a", "ā": "a", "ą": "a", "ᶏ": "a",
                "ẚ": "a", "å": "a", "ǻ": "a", "ḁ": "a", "ⱥ": "a", "ã": "a", "ꜳ": "aa", "æ": "ae", "ǽ": "ae", "ǣ": "ae",
                "ꜵ": "ao", "ꜷ": "au", "ꜹ": "av", "ꜻ": "av", "ꜽ": "ay", "ḃ": "b", "ḅ": "b", "ɓ": "b", "ḇ": "b", "ᵬ": "b",
                "ᶀ": "b", "ƀ": "b", "ƃ": "b", "ɵ": "o", "ć": "c", "č": "c", "ç": "c", "ḉ": "c", "ĉ": "c", "ɕ": "c",
                "ċ": "c", "ƈ": "c", "ȼ": "c", "ď": "d", "ḑ": "d", "ḓ": "d", "ȡ": "d", "ḋ": "d", "ḍ": "d", "ɗ": "d",
                "ᶑ": "d", "ḏ": "d", "ᵭ": "d", "ᶁ": "d", "đ": "d", "ɖ": "d", "ƌ": "d", "ı": "i", "ȷ": "j", "ɟ": "j",
                "ʄ": "j", "ǳ": "dz", "ǆ": "dz", "é": "e", "ĕ": "e", "ě": "e", "ȩ": "e", "ḝ": "e", "ê": "e", "ế": "e",
                "ệ": "e", "ề": "e", "ể": "e", "ễ": "e", "ḙ": "e", "ë": "e", "ė": "e", "ẹ": "e", "ȅ": "e", "è": "e",
                "ẻ": "e", "ȇ": "e", "ē": "e", "ḗ": "e", "ḕ": "e", "ⱸ": "e", "ę": "e", "ᶒ": "e", "ɇ": "e", "ẽ": "e",
                "ḛ": "e", "ꝫ": "et", "ḟ": "f", "ƒ": "f", "ᵮ": "f", "ᶂ": "f", "ǵ": "g", "ğ": "g", "ǧ": "g",
                "ģ": "g", "ĝ": "g", "ġ": "g", "ɠ": "g", "ḡ": "g", "ᶃ": "g", "ǥ": "g", "ḫ": "h", "ȟ": "h",
                "ḩ": "h", "ĥ": "h", "ⱨ": "h", "ḧ": "h", "ḣ": "h", "ḥ": "h", "ɦ": "h", "ẖ": "h", "ħ": "h",
                "ƕ": "hv", "í": "i", "ĭ": "i", "ǐ": "i", "î": "i", "ï": "i", "ḯ": "i", "ị": "i", "ȉ": "i",
                "ì": "i", "ỉ": "i", "ȋ": "i", "ī": "i", "į": "i", "ᶖ": "i", "ɨ": "i", "ĩ": "i", "ḭ": "i",
                "ꝺ": "d", "ꝼ": "f", "ᵹ": "g", "ꞃ": "r", "ꞅ": "s", "ꞇ": "t", "ꝭ": "is", "ǰ": "j", "ĵ": "j",
                "ʝ": "j", "ɉ": "j", "ḱ": "k", "ǩ": "k", "ķ": "k", "ⱪ": "k", "ꝃ": "k", "ḳ": "k", "ƙ": "k",
                "ḵ": "k", "ᶄ": "k", "ꝁ": "k", "ꝅ": "k", "ĺ": "l", "ƚ": "l", "ɬ": "l", "ľ": "l", "ļ": "l",
                "ḽ": "l", "ȴ": "l", "ḷ": "l", "ḹ": "l", "ⱡ": "l", "ꝉ": "l", "ḻ": "l", "ŀ": "l", "ɫ": "l",
                "ᶅ": "l", "ɭ": "l", "ł": "l", "ǉ": "lj", "ſ": "s", "ẜ": "s", "ẛ": "s", "ẝ": "s", "ḿ": "m",
                "ṁ": "m", "ṃ": "m", "ɱ": "m", "ᵯ": "m", "ᶆ": "m", "ń": "n", "ň": "n", "ņ": "n", "ṋ": "n",
                "ȵ": "n", "ṅ": "n", "ṇ": "n", "ǹ": "n", "ɲ": "n", "ṉ": "n", "ƞ": "n", "ᵰ": "n",
                "ᶇ": "n", "ɳ": "n", "ñ": "n", "ǌ": "nj", "ó": "o", "ŏ": "o", "ǒ": "o",
                "ô": "o", "ố": "o", "ộ": "o", "ồ": "o", "ổ": "o", "ỗ": "o", "ö": "o",
                "ȫ": "o", "ȯ": "o", "ȱ": "o", "ọ": "o", "ő": "o", "ȍ": "o", "ò": "o",
                "ỏ": "o", "ơ": "o", "ớ": "o", "ợ": "o", "ờ": "o", "ở": "o", "ỡ": "o",
                "ȏ": "o", "ꝋ": "o", "ꝍ": "o", "ⱺ": "o", "ō": "o", "ṓ": "o", "ṑ": "o",
                "ǫ": "o", "ǭ": "o", "ø": "o", "ǿ": "o", "õ": "o", "ṍ": "o", "ṏ": "o",
                "ȭ": "o", "ƣ": "oi", "ꝏ": "oo", "ɛ": "e", "ᶓ": "e", "ɔ": "o",
                "ᶗ": "o", "ȣ": "ou", "ṕ": "p", "ṗ": "p", "ꝓ": "p", "ƥ": "p",
                "ᵱ": "p", "ᶈ": "p", "ꝕ": "p", "ᵽ": "p", "ꝑ": "p", "ꝙ": "q",
                "ʠ": "q", "ɋ": "q", "ꝗ": "q", "ŕ": "r", "ř": "r", "ŗ": "r",
                "ṙ": "r", "ṛ": "r", "ṝ": "r", "ȑ": "r", "ɾ": "r", "ᵳ": "r", "ȓ": "r", "ṟ": "r", "ɼ": "r", "ᵲ": "r", "ᶉ": "r", "ɍ": "r",
                "ɽ": "r", "ↄ": "c", "ꜿ": "c", "ɘ": "e", "ɿ": "r", "ś": "s", "ṥ": "s", "š": "s", "ṧ": "s", "ş": "s", "ŝ": "s", "ș": "s",
                "ṡ": "s", "ṣ": "s", "ṩ": "s", "ʂ": "s", "ᵴ": "s", "ᶊ": "s", "ȿ": "s", "ɡ": "g", "ᴑ": "o", "ᴓ": "o", "ᴝ": "u", "ť": "t",
                "ţ": "t", "ṱ": "t", "ț": "t", "ȶ": "t", "ẗ": "t", "ⱦ": "t", "ṫ": "t", "ṭ": "t", "ƭ": "t", "ṯ": "t", "ᵵ": "t", "ƫ": "t",
                "ʈ": "t", "ŧ": "t", "ᵺ": "th", "ɐ": "a", "ᴂ": "ae", "ǝ": "e", "ᵷ": "g", "ɥ": "h", "ʮ": "h", "ʯ": "h", "ᴉ": "i", "ʞ": "k",
                "ꞁ": "l", "ɯ": "m", "ɰ": "m", "ᴔ": "oe", "ɹ": "r", "ɻ": "r", "ɺ": "r", "ⱹ": "r", "ʇ": "t", "ʌ": "v", "ʍ": "w", "ʎ": "y",
                "ꜩ": "tz", "ú": "u", "ŭ": "u", "ǔ": "u", "û": "u", "ṷ": "u", "ü": "u", "ǘ": "u", "ǚ": "u", "ǜ": "u", "ǖ": "u", "ṳ": "u",
                "ụ": "u", "ű": "u", "ȕ": "u", "ù": "u", "ủ": "u", "ư": "u", "ứ": "u", "ự": "u", "ừ": "u", "ử": "u", "ữ": "u", "ȗ": "u",
                "ū": "u", "ṻ": "u", "ų": "u", "ᶙ": "u", "ů": "u", "ũ": "u", "ṹ": "u", "ṵ": "u", "ᵫ": "ue", "ꝸ": "um", "ⱴ": "v", "ꝟ": "v",
                "ṿ": "v", "ʋ": "v", "ᶌ": "v", "ⱱ": "v", "ṽ": "v", "ꝡ": "vy", "ẃ": "w", "ŵ": "w", "ẅ": "w", "ẇ": "w", "ẉ": "w", "ẁ": "w",
                "ⱳ": "w", "ẘ": "w", "ẍ": "x", "ẋ": "x", "ᶍ": "x", "ý": "y", "ŷ": "y", "ÿ": "y", "ẏ": "y", "ỵ": "y", "ỳ": "y", "ƴ": "y",
                "ỷ": "y", "ỿ": "y", "ȳ": "y", "ẙ": "y", "ɏ": "y", "ỹ": "y", "ź": "z", "ž": "z", "ẑ": "z", "ʑ": "z", "ⱬ": "z",
                "ż": "z", "ẓ": "z", "ȥ": "z", "ẕ": "z", "ᵶ": "z", "ᶎ": "z", "ʐ": "z", "ƶ": "z", "ɀ": "z", "ﬀ": "ff",
                "ﬃ": "ffi", "ﬄ": "ffl", "ﬁ": "fi", "ﬂ": "fl", "ĳ": "ij", "œ": "oe", "ﬆ": "st", "ₐ": "a", "ₑ": "e",
                "ᵢ": "i", "ⱼ": "j", "ₒ": "o", "ᵣ": "r", "ᵤ": "u", "ᵥ": "v", "ₓ": "x"
            };
            String.prototype.latinise = function () { return this.replace(/[^A-Za-z0-9\[\] ]/g, function (a) { return Latinise.latin_map[a] || a }) };
            String.prototype.latinize = String.prototype.latinise;
            String.prototype.isLatin = function () { return this === this.latinise() }
            var _el = elem;
            $(elem).on('keyup', function (d) {
                if (!attrs.ngModel && scope.ngModel) { return; }
                var _rs = scope.ngModel.latinize();
                $(_el).val(_rs);
            });
            $(elem).on('keypress', function () {
                if (!attrs.ngModel && scope.ngModel) { return; }
                var _rs = scope.ngModel.latinize();
                $(_el).val(_rs);
            });
            $(elem).on('focusout', function () {
                if (!attrs.ngModel && scope.ngModel) { return; }
                var _rs = scope.ngModel.latinize();
                $(_el).val(_rs);
            });
            $(elem).on('click', function () {
                if (!attrs.ngModel && scope.ngModel) { return; }
                var _rs = scope.ngModel.latinize();
                $(_el).val(_rs);
            });
        }
    };
}]);
aimodu.factory('keyhandle', function () {
    var _e = [];
    var _c = [];
    var _d = [];
    var _s = [];
    document.onkeydown = function (e) {
        e = e || window.event;
        var _keyCode = e.keyCode || e.which;
        if (_s[_keyCode] === 1) {
            switch (_keyCode) {
                case _c[_keyCode]:
                    typeof _e[_keyCode] === 'function' ? _e[_keyCode](_d[_keyCode]) : '';
                    break;
            }
        }
        if (_s[_keyCode] === 2) {
            if (e.ctrlKey) {
                switch (_keyCode) {
                    case _c[_keyCode]:
                        typeof _e[_keyCode] === 'function' ? _e[_keyCode](_d[_keyCode]) : '';
                        break;
                }
            }
        }
    };
    return {
        event: function (k, e, d) {
            _c[k] = k;
            _e[k] = e;
            _d[k] = d;
            _s[k] = 1;
        },
        eventCtrl: function (k, e, d) {
            _c[k] = k;
            _e[k] = e;
            _d[k] = d;
            _s[k] = 2;
        }
    }
});
//Begin menuRight
//ctrl.initMenu = [{
//    Title: 'Tải lên',
//    Icon: 'mdi mdi-cloud-upload',
//    Click: ctrl.openFormUpload,
//}, {
//    Title: 'Tạo thư mục',
//    Icon: 'mdi mdi-folder-plus',
//    Click: ctrl.createFolder,
//},];
//End menuRight
aimodu.directive('menuRight', function () {
    return {
        restrict: 'EAC',
        scope: {
            'ngInit': '=',
            'ngData': '=',
            'ngLeft': '=',
            'ngHover': '=',
        },
        link: function (scope, elem, attrs) {
            function _eventTem(event,c) {
                event.preventDefault();
                $('body').off('click');
                removeEle();
                var _el = document.createElement('menu');
                _el.style.display = "block";

                if ($('body').height() < (event.pageY + event.data.Object.length * 34)) {
                    _el.style.top = (event.pageY - event.data.Object.length * 34) + "px";
                } else {
                    _el.style.top = (event.pageY) + "px";
                }
                $(_el).addClass('es-cursor');
                var _elU = document.createElement('menu');
                $(_elU).attr('class', 'dropdown-menu');
                $(_elU).attr('role', 'menu');
                $(_elU).attr('style', 'display: block; position: absolute;z-index: 10000;');
                for (var i = 0; i < event.data.Object.length; i++) {
                    var _elLi = document.createElement('li');
                    if (event.data.Object[i].line) {
                        $(_elLi).attr('style', 'border-bottom: 1px solid #e0e5e8;');
                    }
                    $(_elLi).on("click", {
                        Data: event.data.Data,
                        Item: event.data.Object[i],
                    }, function (event) {
                        event.data.Item.Click(event.data.Data, event.data.Item);
                    });
                    var _elA = document.createElement('a');
                    $(_elA).attr('class', 'dropdown-item');
                    var _elI = document.createElement('span');
                    $(_elI).attr('class', event.data.Object[i].Icon);
                    $(_elI).text(event.data.Object[i].Title);
                    _elA.appendChild(_elI);
                    _elLi.appendChild(_elA);
                    _elU.appendChild(_elLi);
                }
                _el.appendChild(_elU);
                $('body').append(_el);
                setTimeout(function () {
                    if ($('body').width() < (event.pageX - 40 + $(_elU).width())) {
                        _el.style.left = (event.pageX - 40 - $(_elU).width()) + "px";
                    } else {
                        _el.style.left = (event.pageX - 40) + "px";
                    }
                });
                //Chưa tối ưu, còn phải click 2 lần
                $('body').on('click', {c:c }, function (e) {
                    if (e.data.c === 1) {
                        $('body').off('click');
                        $('body').on('click', function (e) {
                            removeEle();
                            $('body').off('click');
                        })
                        return;
                    }
                    removeEle();
                    $('body').off('click');
                })
                function removeEle() {
                    var _el = document.getElementsByTagName('menu');
                    $(_el).remove();
                }
            }
            var _ngInit = scope.ngInit;
            var _ngData = scope.ngData;
            $(elem).on("contextmenu", {
                Object: _ngInit,
                Data: _ngData
            }, function (event) {
                _eventTem(event,2);
            });
            if (scope.ngLeft !== undefined && scope.ngLeft !== null) {
                $(elem).on("click", {
                    Object: _ngInit,
                    Data: _ngData
                }, function (event) {
                    _eventTem(event,1);
                });
            }
            if (scope.ngHover !== undefined && scope.ngHover !== null) {
                $(elem).on("mouseover", {
                    Object: _ngInit,
                    Data: _ngData
                }, function (event) {
                    _eventTem(event,2);
                });
            }
        }
    };
})
aimodu.directive('ngJexcel', ['$filter', function ($filter) {
    return {
        restrict: 'EAC',
        scope: {
            'jxinit': '=jxInit',
            'ngModel': '=',
        },
        link: function (scope, elem, attrs) {
            $(elem).addClass('spreadsheet__table table es-jexcel');
            scope.ngModel = elem;
            var first = false;
            scope.$watch('jxinit.data', function (rs) {
                if (first) {
                    $(scope.ngModel).jexcel('setData', rs, false);
                    //scope.ngModel = $(_elem);
                } else {
                    first = !first;
                };
                return true;
            });
            var _i = {};
            if (scope.jxinit !== undefined) {
                for (var key in scope.jxinit) {
                    if (key === 'reloadData') continue;
                    _i[key] = scope.jxinit[key];
                }
            }
            $(elem).jexcel(_i);
            $(elem).jexcel('updateSettings', {
                table: function (instance, cell, col, row, val, id, h, c) {
                    // Number formating
                    if (h[col] === 'numeric') {
                        // using in next time variable 'c': c.formatNumber==='VN-MONEY'
                        $(cell).html(convertMoneyDecimal($(cell).text()));
                    }
                }
            });
            scope.ngModel !== undefined ? scope.ngModel = $(elem) : '';
            if (scope.$root.$$phase !== '$apply' && scope.$root.$$phase !== '$digest') {
                scope.$apply();
            }
            if (scope.jxinit.reloadData !== undefined) {
                angular.extend(scope.jxinit.reloadData, {
                    reloadJexcel: function () {
                        $(scope.ngModel).jexcel('setData', scope.jxinit.data, false);
                    }
                });
            }
            scope.$on('$viewContentLoaded', function () {
                console.log(" ===> Called on View Load ");
            });
        }
    };
}]);
aimodu.directive('ngSmselect', ['$filter', function ($filter) {
    return {
        restrict: 'EAC',
        scope: {
            'data': '=seInit',
            'seChange': '&',
            'ngModel': '=',
            'seData': '=',
            'placeholder': '@',
            'title': '@',
        },
        require: 'ngModel',
        template: '',
        link: function (scope, elem, attrs, ctrl) {
            scope._el = elem;
            scope.$watch('seData', function (n, ol) {
                scope._el[0].sumo.reloadData(n);
                if (scope.$root.$$phase !== '$apply' && scope.$root.$$phase !== '$digest') {
                    scope.$apply();
                }
            }, true)
            scope.$watch('ngModel', function (rs) {
                if (rs !== undefined && rs !== null && rs !== '') {
                    var _i = Array.isArray(rs) ? rs : [rs];
                    scope._el[0].sumo.reloadData(scope.seData, _i);
                    if (scope.$root.$$phase !== '$apply' && scope.$root.$$phase !== '$digest') {
                        scope.$apply();
                    }
                } else {
                    var _i = Array.isArray(rs) ? null : [];
                    scope._el[0].sumo.unSelectItem();
                    scope._el[0].sumo.reloadData(scope.seData, _i);
                    if (scope.$root.$$phase !== '$apply' && scope.$root.$$phase !== '$digest') {
                        scope.$apply();
                    }
                }
            }, true)
            scope.is_multi = scope._el.attr("multiple");
            scope.changedata = function (d1) {
                scope.ngModel !== undefined ? ctrl.$setViewValue(d1) : '';
                if (scope.$root.$$phase !== '$apply' && scope.$root.$$phase !== '$digest') {
                    scope.$apply();
                }
                if (scope.data !== undefined && scope.data.change !== undefined) {
                    scope.data.change(d1);
                } else {
                    scope.seChange({ data: d1 });
                }
            }
            var _i = {};
            scope.placeholder !== undefined ? _i.placeholder = scope.placeholder : '';
            scope.title !== undefined ? _i.title = scope.title : '';
            scope.seData === undefined ? _i.data = [] : _i.data = scope.seData;
            if (scope.data !== undefined) {
                for (var key in scope.data) {
                    if (key === 'reload') {
                        continue;
                    }
                    _i[key] = scope.data[key];
                }
            }
            if (scope.data !== undefined && scope.data.reload !== undefined) {
                angular.extend(scope.data.reload, {
                    reloadDefault: function (d) {
                        scope._el[0].sumo.reloadData(scope.seData, d);
                    }
                });
            }
            initSmallSelect(elem[0], _i, scope.changedata);
        }
    };
}]);
aimodu.directive('ngOptionDate', [function ($filter) {
    return {
        restrict: 'EAC',
        scope: {
            'optionChange': '&',
            'placeholder': '@',
            'sPoint': '@',
            'ngPoint': '=ngPoint',
            'ngInit': '=ngInit',
        },
        template: '',
        link: function (scope, elem, attrs, ctrl) {
            Date.prototype.getWeekNumber = function () {
                var d = new Date(Date.UTC(this.getFullYear(), this.getMonth(), this.getDate()));
                var dayNum = d.getUTCDay() || 7;
                d.setUTCDate(d.getUTCDate() + 4 - dayNum);
                var yearStart = new Date(Date.UTC(d.getUTCFullYear(), 0, 1));
                return Math.ceil((((d - yearStart) / 86400000) + 1) / 7)
            };
            Date.prototype.getWeek = function () {
                var onejan = new Date(this.getFullYear(), 0, 1);
                return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
            }
            Date.prototype.addDays = function (days) {
                var date = new Date(this.valueOf());
                date.setDate(date.getDate() + days);
                return date;
            }
            function GetWeekObject(d) {
                d = new Date(d);
                var day = d.getDay(),
                    diff = d.getDate() - day + (day == 0 ? -6 : 1); // adjust when day is sunday
                var _data = {};
                _data.StartDate = new Date(d.setDate(diff));
                _data.EndDate = _data.StartDate.addDays(6);
                return _data;
            }
            function getWeekNumber(d) {
                d = new Date(+d);
                d.setHours(0, 0, 0);
                d.setDate(d.getDate() + 4 - (d.getDay() || 7));
                var yearStart = new Date(d.getFullYear(), 0, 1);
                var weekNo = Math.ceil((((d - yearStart) / 86400000) + 1) / 7)
                return [d.getFullYear(), weekNo];
            }
            function weeksInYear(year) {
                var month = 11, day = 31, week;
                do {
                    d = new Date(year, month, day--);
                    week = getWeekNumber(d)[1];
                } while (week === 1);
                return week;
            }
            function daysInMonth(year, month) {
                return new Date(year, month, 0).getDate();
            }
            function GetMonthObject(d) {
                var _data = {};
                _data.StartDate = new Date(d.getFullYear(), d.getMonth(), 1);   //new DateTime(year, month, 1);
                _data.EndDate = new Date(d.getFullYear(), d.getMonth(), daysInMonth(d.getFullYear(), d.getMonth() + 1)); //new DateTime(year, month, DateTime.DaysInMonth(year, month));
                return _data;
            }
            scope.LoadWeekForMonth = function (data) {
                var date1 = new Date(scope.model.Year, data - 1, 1);
                var date2 = new Date(scope.model.Year, data, 0);
                var Start = date1.getWeekNumber();
                var End = date2.getWeekNumber();
                scope.liWeek = [];
                if (End < Start) {
                    for (var i = 0; i < scope.liWeekTemp.length; i++) {
                        if (scope.liWeekTemp[i].Value >= Start) {
                            scope.liWeek.push(scope.liWeekTemp[i]);
                        }
                    }
                }
                else {
                    for (var i = 0; i < scope.liWeekTemp.length; i++) {
                        if (scope.liWeekTemp[i].Value >= Start && scope.liWeekTemp[i].Value <= End) {
                            scope.liWeek.push(scope.liWeekTemp[i]);
                        }
                    }
                }
            }
            scope.LoadMonthForQuarter = function (data) {
                if (data === undefined || data === null) {
                    scope.liMonth = [
                        {
                            'Title': 1,
                            'Value': 1
                        },
                        {
                            'Title': 2,
                            'Value': 2
                        },
                        {
                            'Title': 3,
                            'Value': 3
                        },
                        {
                            'Title': 4,
                            'Value': 4
                        },
                        {
                            'Title': 5,
                            'Value': 5
                        },
                        {
                            'Title': 6,
                            'Value': 6
                        },
                        {
                            'Title': 7,
                            'Value': 7
                        },
                        {
                            'Title': 8,
                            'Value': 8
                        },
                        {
                            'Title': 9,
                            'Value': 9
                        },
                        {
                            'Title': 10,
                            'Value': 10
                        },
                        {
                            'Title': 11,
                            'Value': 11
                        },
                        {
                            'Title': 12,
                            'Value': 12
                        },];
                    scope.model.Week = undefined;
                    return;
                }
                else
                    if (data === 1) {
                        scope.liMonth = [
                            {
                                'Title': 1,
                                'Value': 1
                            },
                            {
                                'Title': 2,
                                'Value': 2
                            },
                            {
                                'Title': 3,
                                'Value': 3
                            },
                        ]
                    } else
                        if (data === 2) {
                            scope.liMonth = [
                                {
                                    'Title': 4,
                                    'Value': 4
                                },
                                {
                                    'Title': 5,
                                    'Value': 5
                                },
                                {
                                    'Title': 6,
                                    'Value': 6
                                },
                            ]
                        } else
                            if (data === 3) {
                                scope.liMonth = [
                                    {
                                        'Title': 7,
                                        'Value': 7
                                    },
                                    {
                                        'Title': 8,
                                        'Value': 8
                                    },
                                    {
                                        'Title': 9,
                                        'Value': 9
                                    },
                                ]
                            } else {
                                if (data === 4) {
                                    scope.liMonth = [
                                        {
                                            'Title': 10,
                                            'Value': 10
                                        },
                                        {
                                            'Title': 11,
                                            'Value': 11
                                        },
                                        {
                                            'Title': 12,
                                            'Value': 12
                                        },
                                    ]
                                }
                            }
                scope.model.month = undefined;
                scope.model.Week = undefined;
            }
            scope.initData = function () {
                scope.liWeek = [];
                scope.liWeekTemp = [];
                scope.CheckAll = true;
                scope.liMonth = [
                    {
                        'Title': 1,
                        'Value': 1
                    },
                    {
                        'Title': 2,
                        'Value': 2
                    },
                    {
                        'Title': 3,
                        'Value': 3
                    },
                    {
                        'Title': 4,
                        'Value': 4
                    },
                    {
                        'Title': 5,
                        'Value': 5
                    },
                    {
                        'Title': 6,
                        'Value': 6
                    },
                    {
                        'Title': 7,
                        'Value': 7
                    },
                    {
                        'Title': 8,
                        'Value': 8
                    },
                    {
                        'Title': 9,
                        'Value': 9
                    },
                    {
                        'Title': 10,
                        'Value': 10
                    },
                    {
                        'Title': 11,
                        'Value': 11
                    },
                    {
                        'Title': 12,
                        'Value': 12
                    },];
                scope.liQuarter = [
                    {
                        'Title': '1',
                        'Value': 1,
                    },
                    {
                        'Title': '2',
                        'Value': 2,
                    },
                    {
                        'Title': '3',
                        'Value': 3,
                    },
                    {
                        'Title': '4',
                        'Value': 4,
                    },
                ];
                scope.liYear = [];
                var _Date = new Date();
                var dayNames = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
                var count = weeksInYear(_Date.getFullYear());
                var _date = new Date(_Date.getFullYear(), 0, 1);
                if (_date.getWeekNumber() != 1) {
                    var _Date2 = new Date(_date.getFullYear(), 0, 1);
                    _Date2.setDate(_Date2.getDate() + 7);
                    var startDate = new Date(_Date2.getFullYear(), 0, 1);
                    var endDate = new Date(_Date2.getFullYear(), 0, 1);
                    if (_date.getDay() === 0) {
                        endDate.setDate(endDate.getDay() + 1);
                        startDate.setDate(startDate.getDate() + 1);
                    }
                    if (_date.getDay() === 1) {
                        endDate.setDate(endDate.getDay() + 7);
                        startDate.setDate(startDate.getDate() + 7);
                    }
                    if (_date.getDay() === 2) {
                        endDate.setDate(endDate.getDay() + 6);
                        startDate.setDate(startDate.getDate() + 6);
                    }
                    if (_date.getDay() === 3) {
                        endDate.setDate(endDate.getDay() + 5);
                        startDate.setDate(startDate.getDate() + 5);
                    }
                    if (_date.getDay() === 4) {
                        endDate.setDate(endDate.getDay() + 4);
                        startDate.setDate(startDate.getDate() + 4);
                    }
                    if (_date.getDay() === 5) {
                        endDate.setDate(endDate.getDay() + 3);
                        startDate.setDate(startDate.getDate() + 3);
                    }
                    if (_date.getDay() === 6) {
                        endDate.setDate(endDate.getDay() + 2);
                        startDate.setDate(startDate.getDate() + 2);
                    }
                    for (var i = 1; i <= count; i++) {
                        var item = {};
                        if (i === 1) {
                            item = {
                                'Title': i,
                                'Value': i,
                                'StartDate': ConDate(startDate, 3),
                                'EndDate': ConDate(endDate, 3),
                            };
                        } else {
                            var _xDate = new Date(startDate);
                            startDate.setDate(startDate.getDate() + 7);
                            endDate = new Date(_xDate).setDate(_xDate.getDate() + 7 + 6);
                            item = {
                                'Title': i,
                                'Value': i,
                                'StartDate': ConDate(startDate, 3),
                                'EndDate': ConDate(endDate, 3),
                            };
                        }
                        scope.liWeek.push(item);
                        scope.liWeekTemp.push(item);
                    }
                } else {
                    var startDate = new Date(_date.getFullYear(), 0, 1);
                    var endDate = new Date(_date.getFullYear(), 0, 1);
                    if (_date.getDay() === 0) {
                        endDate.setDate(endDate.getDay());
                    }
                    if (_date.getDay() === 1) {
                        endDate.setDate(endDate.getDay() + 6);
                    }
                    if (_date.getDay() === 2) {
                        endDate.setDate(endDate.getDay() + 5);
                    }
                    if (_date.getDay() === 3) {
                        endDate.setDate(endDate.getDay() + 4);
                    }
                    if (_date.getDay() === 4) {
                        endDate.setDate(endDate.getDay() + 3);
                    }
                    if (_date.getDay() === 5) {
                        endDate.setDate(endDate.getDay() + 2);
                    }
                    if (_date.getDay() === 6) {
                        endDate.setDate(endDate.getDay() + 1);
                    }
                    for (var i = 1; i <= count; i++) {
                        var item = {};
                        if (i === 1) {
                            item = {
                                'Title': i,
                                'Value': i,
                                'StartDate': ConDate(startDate, 3),
                                'EndDate': ConDate(endDate, 3),
                            };
                        } else
                            if (i === 2) {
                                if (_date.getDay() === 0) {
                                    startDate.setDate(startDate.getDate() + 1);
                                }
                                if (_date.getDay() === 1) {
                                    startDate.setDate(startDate.getDate() + 7);
                                }
                                if (_date.getDay() === 2) {
                                    startDate.setDate(startDate.getDate() + 6);
                                }
                                if (_date.getDay() === 3) {
                                    startDate.setDate(startDate.getDate() + 5);
                                }
                                if (_date.getDay() === 4) {
                                    startDate.setDate(startDate.getDate() + 4);
                                }
                                if (_date.getDay() === 5) {
                                    startDate.setDate(startDate.getDate() + 3);
                                }
                                if (_date.getDay() === 6) {
                                    startDate.setDate(startDate.getDate() + 2);
                                }
                                endDate.setDate(startDate.getDate() + 6);
                                item = {

                                    'Title': i,
                                    'Value': i,
                                    'StartDate': ConDate(startDate, 3),
                                    'EndDate': ConDate(endDate, 3),
                                };
                            }
                            else {
                                var _xDate = new Date(startDate);
                                startDate.setDate(startDate.getDate() + 7);
                                endDate = new Date(_xDate).setDate(_xDate.getDate() + 7 + 6);
                                item = {
                                    'Title': i,
                                    'Value': i,
                                    'StartDate': ConDate(startDate, 3),
                                    'EndDate': ConDate(endDate, 3),
                                };
                            }
                        scope.liWeek.push(item);
                        scope.liWeekTemp.push(item);
                    }
                }
                for (var i = 1; i <= 30; i++) {
                    var item = {
                        'Title': 2012 + i,
                        'Value': 2012 + i,
                    };
                    scope.liYear.push(item);
                }
            }
            scope.initData();
            scope._el = elem;
            scope.is_multi = scope._el.attr("multiple");
            function getThisQuarter(_tem) {
                var _i = (new Date()).getMonth() + 1;
                var _d = [];
                if (1 <= _i && _i <= 3) {
                    if (_tem === 1) {
                        _d[0] = new Date((new Date()).getFullYear() - 1, 9, 1);
                        _d[1] = new Date((new Date()).getFullYear() - 1, 12, 0);
                    } else {
                        _d[0] = new Date((new Date()).getFullYear(), 0, 1);
                        _d[1] = new Date((new Date()).getFullYear(), 3, 0);
                    }
                    return _d;
                }
                if (4 <= _i && _i <= 6) {
                    if (_tem === 1) {
                        _d[0] = new Date((new Date()).getFullYear(), 0, 1);
                        _d[1] = new Date((new Date()).getFullYear(), 3, 0);
                    } else {
                        _d[0] = new Date((new Date()).getFullYear(), 3, 1);
                        _d[1] = new Date((new Date()).getFullYear(), 6, 0);
                    }
                    return _d;
                }
                if (7 <= _i && _i <= 9) {
                    if (_tem === 1) {
                        _d[0] = new Date((new Date()).getFullYear(), 3, 1);
                        _d[1] = new Date((new Date()).getFullYear(), 6, 0);
                    } else {
                        _d[0] = new Date((new Date()).getFullYear(), 6, 1);
                        _d[1] = new Date((new Date()).getFullYear(), 9, 0);
                    }
                    return _d;
                }
                if (10 <= _i && _i <= 12) {
                    if (_tem === 1) {
                        _d[0] = new Date((new Date()).getFullYear(), 6, 1);
                        _d[1] = new Date((new Date()).getFullYear(), 9, 0);
                    } else {
                        _d[0] = new Date((new Date()).getFullYear(), 9, 1);
                        _d[1] = new Date((new Date()).getFullYear(), 12, 0);
                    }
                    return _d;
                }
            }
            scope.changedata = function (d1) {
                var _d = [];
                var _start = null;
                var _end = null;
                switch (d1) {
                    case '0':
                        break;
                    case '1':
                        _start = new Date((new Date()).getFullYear(), 0, 1);
                        _end = new Date((new Date()).getFullYear(), 12, 0);
                        break;
                    case '2':
                        var _x = getThisQuarter();
                        _start = _x[0];
                        _end = _x[1];
                        break;
                    case '3':
                        _start = new Date((new Date()).getFullYear(), (new Date()).getMonth(), 1);
                        _end = new Date((new Date()).getFullYear(), (new Date()).getMonth() + 1, 0);
                        break;
                    case '4':
                        _start = GetWeekObject(new Date()).StartDate;
                        _end = GetWeekObject(new Date()).EndDate;
                        break;
                    case '5':
                        _start = new Date((new Date()).getFullYear(), (new Date()).getMonth(), (new Date()).getDate());
                        _end = new Date((new Date()).getFullYear(), (new Date()).getMonth(), (new Date()).getDate(), 23, 59, 59);
                        break;
                    case '6':
                        _start = new Date((new Date()).getFullYear() - 1, 0, 1);
                        _end = new Date((new Date()).getFullYear(), 0, 0);
                        break;
                    case '7':
                        var _x = getThisQuarter(1);
                        _start = _x[0];
                        _end = _x[1];
                        break;
                    case '8': //tháng trước
                        _start = new Date((new Date()).getFullYear(), (new Date()).getMonth() - 1, 1);
                        _end = new Date((new Date()).getFullYear(), (new Date()).getMonth(), 0);
                        break;
                    case '12': //tháng sau
                        var _x = new Date((new Date()).getFullYear(), (new Date()).getMonth() + 1);
                        _start = GetMonthObject(_x).StartDate;
                        _end = GetMonthObject(_x).EndDate;
                        break;
                    case '9':
                        //have error when first week in year
                        var _x = (new Date()).getWeek() - 1;
                        for (var i = 0; i < scope.liWeek.length; i++) {
                            if (scope.liWeek[i].Value === _x) {
                                _start = scope.liWeek[i].StartDate;
                                _end = scope.liWeek[i].EndDate;
                                break;
                            }
                        }
                        break;
                    case '11': // get next week 
                        //have error when first week in year
                        var _x = (new Date()).addDays(6);
                        _start = GetWeekObject(_x).StartDate;
                        _end = GetWeekObject(_x).EndDate;
                        break;
                    case '10':
                        _start = new Date((new Date()).getFullYear(), (new Date()).getMonth(), (new Date()).getDate() - 1);
                        _end = new Date((new Date()).getFullYear(), (new Date()).getMonth(), (new Date()).getDate() - 1, 23, 59, 59);
                        break;
                    case '30NT':
                        _start = new Date((new Date()).getFullYear(), (new Date()).getMonth() - 1, (new Date()).getDate());
                        _end = new Date((new Date()).getFullYear(), (new Date()).getMonth(), (new Date()).getDate(), 23, 59, 59);
                        break;
                    case '30NS':
                        _start = new Date((new Date()).getFullYear(), (new Date()).getMonth(), (new Date()).getDate());
                        _end = new Date((new Date()).getFullYear(), (new Date()).getMonth() + 1, (new Date()).getDate(), 23, 59, 59);
                        break;
                }
                (parseInt(d1) === 0) ? _d = [] : _d = [_start.getTime(), _end.getTime()];
                scope.optionChange({ data: _d });
                if (scope.$root.$$phase !== '$apply' && scope.$root.$$phase !== '$digest') {
                    scope.$apply();
                }
            }
            var _i = {};
            scope.placeholder !== undefined ? _i.placeholder = scope.placeholder : '';
            scope.seData === undefined ? _i.data = [] : _i.data = scope.seData;
            _i.selected = [scope.sPoint !== undefined ? scope.sPoint : (scope.ngPoint !== undefined ? scope.ngPoint.toString() : '3')];
            _i.data = [
                {
                    text: 'Tùy chọn',
                    value: '0'
                },
                {
                    text: 'Năm hiện tại',
                    value: '1'
                },
                {
                    text: 'Quý hiện tại',
                    value: '2'
                },
                {
                    text: 'Tháng hiện tại',
                    value: '3'
                },
                {
                    text: '30 ngày trước',
                    value: '30NT'
                },
                {
                    text: '30 ngày sau',
                    value: '30NS'
                },
                {
                    text: 'Tuần hiện tại',
                    value: '4'
                },
                {
                    text: 'Ngày hiện tại',
                    value: '5'
                },
                {
                    text: 'Năm trước',
                    value: '6'
                },
                {
                    text: 'Quý trước',
                    value: '7'
                },
                {
                    text: 'Tháng trước',
                    value: '8'
                },
                {
                    text: 'Tháng sau',
                    value: '12'
                },
                {
                    text: 'Tuần trước',
                    value: '9'
                },
                {
                    text: 'Tuần sau',
                    value: '11'
                },
                {
                    text: 'Ngày trước',
                    value: '10'
                },
            ];
            scope.seData === undefined ? scope.seData = _i.data : '';
            initSmallSelect(elem[0], _i, scope.changedata);
            scope.changedata(scope.sPoint !== undefined ? scope.sPoint : (scope.ngPoint !== undefined ? scope.ngPoint.toString() : '3'));
            if (scope.ngInit !== undefined && scope.ngInit.reloadData !== undefined) {
                angular.extend(scope.ngInit.reloadData, {
                    reloadDefault: function () {
                        scope._el[0].sumo.reloadData(scope.seData, [scope.sPoint !== undefined ? scope.sPoint : (scope.ngPoint.toString() !== undefined ? scope.ngPoint : '3')]);
                        scope.changedata(scope.sPoint !== undefined ? scope.sPoint : (scope.ngPoint !== undefined ? scope.ngPoint.toString() : '3'));
                    }
                });
            }
        }
    };
}]);
String.prototype.replaceAll = function (search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};
function ConDate(data, number) {
    try {
        if (data == null || data == "") {
            return '';
        }
        if (data !== null && data != "" && data != undefined) {
            try {
                if (data.indexOf("SA") != -1 || data.indexOf("CH") != -1) {
                    if (data.indexOf("SA") != -1) { }
                    if (data.indexOf("CH") != -1) { }
                }

                if (data.indexOf('Date') != -1) {
                    data = data.substring(data.indexOf("Date") + 5);
                    data = parseInt(data);
                }
            }
            catch (ex) { }
            var newdate = new Date(data);
            if (number == 3) {
                return newdate;
            }
            if (number == 2) {
                return "/Date(" + newdate.getTime() + ")/";
            }
            var month = newdate.getMonth() + 1;
            var day = newdate.getDate();
            var year = newdate.getFullYear();
            var hh = newdate.getHours();
            var mm = newdate.getMinutes();
            if (month < 10)
                month = "0" + month;
            if (day < 10)
                day = "0" + day;
            if (mm < 10)
                mm = "0" + mm;
            if (number == 0) {
                return todayDate = day + "/" + month + "/" + year;
            }
            if (number == 1) {
                return todayDate = day + "/" + month + "/" + year + " " + hh + ":" + mm;
            }
            if (number == 4) {
                return new Date(year, month - 1, day);
            }
        } else {
            return '';
        }
    } catch (ex) {
        return "";
    }
}
function AllowOnlyNumbers(e, s) {

    e = (e) ? e : window.event;
    var key = null;
    var charsKeys = [
        97, // a  Ctrl + a Select All
        65, // A Ctrl + A Select All
        99, // c Ctrl + c Copy
        67, // C Ctrl + C Copy
        118, // v Ctrl + v paste
        86, // V Ctrl + V paste
        115, // s Ctrl + s save
        83, // S Ctrl + S save
        112, // p Ctrl + p print
        80 // P Ctrl + P print
    ];

    var specialKeys = [
        8, // backspace
        9, // tab
        27, // escape
        13, // enter
        35, // Home & shiftKey +  #
        36, // End & shiftKey + $
        37, // left arrow &  shiftKey + %
        39, //right arrow & '
        46, // delete & .
        45, //Ins &  -
        96,
        97,
        98,
        99,
        100,
        101,
        102,
        103,
        104,
        105,
        110,
        231 //lỗi do các loại bàn phím không có phím bên
        //188 // . dicemal
    ];
    if (s === true)
        specialKeys.push(188);

    key = e.keyCode ? e.keyCode : e.which ? e.which : e.charCode;

    //console.log("e.charCode: " + e.charCode + ", " + "e.which: " + e.which + ", " + "e.keyCode: " + e.keyCode);
    //console.log(String.fromCharCode(key));

    // check if pressed key is not number 
    if (key && (key < 48 || key > 57)) {

        //Allow: Ctrl + char for action save, print, copy, ...etc
        if ((e.ctrlKey && charsKeys.indexOf(key) != -1) ||
            //Fix Issue: f1 : f12 Or Ctrl + f1 : f12, in Firefox browser
            (navigator.userAgent.indexOf("Firefox") != -1 && ((e.ctrlKey && e.keyCode && e.keyCode > 0 && key >= 112 && key <= 123) || (e.keyCode && e.keyCode > 0 && key && key >= 112 && key <= 123)))) {
            return true
        }
        // Allow: Special Keys
        else if (specialKeys.indexOf(key) != -1) {
            //Fix Issue: right arrow & Delete & ins in FireFox
            if ((key == 39 || key == 45 || key == 46)) {
                return (navigator.userAgent.indexOf("Firefox") != -1 && e.keyCode != undefined && e.keyCode > 0);
            }
            //DisAllow : "#" & "$" & "%"
            //add e.altKey to prevent AltGr chars
            else if ((e.shiftKey || e.altKey) && (key == 35 || key == 36 || key == 37)) {
                return false;
            }
            else {
                return true;
            }
        }
        else {
            return false;
        }
    }
    else {
        if (key)
            return true;
        else return false;
    }
}
function changeInputMoney(d1, t, d2) {
    if (!AllowOnlyNumbers(t, d2)) {
        t.value = t.value.replace(/[^\d.-?\,]/g, '');
    }
    if (d2) if (d1.keyCode === 188) return t.value;
    var _s = t.selectionStart;
    _s = t.value.length - _s;
    var x1 = t.value;
    if (x1 === undefined || x1 === null || x1 === '') {
        return 0;
    }
    x1 = x1.replaceAll('[.]', '');
    var _x2 = '';
    if (x1.split(',').length > 1) {
        _x2 = x1.split(',')[1];
        if (_x2.length > 2) {
            _x2 = _x2.substring(0, 2);
        }
        x1 = x1.split(',')[0];
    }
    x1 = parseInt(x1).toString();
    var i1 = String(x1);
    i1 = i1.split('').reverse().join('');
    var arr = [];
    while (i1.length > 0) {
        arr.push(i1.substr(0, 3));
        i1 = i1.slice(3, i1.length);
    }
    for (var i = 0; i < arr.length; i++) {
        if (i === (arr.length - 1))
            i1 += arr[i];
        else {
            i1 += arr[i] + ".";
        }
    }
    i1 = i1.split('').reverse().join('');
    if (_x2 !== undefined && _x2 !== null && t.value.split(',').length > 1) {
        t.value = i1 + ',' + _x2;
    } else {
        t.value = i1;
    }
    _s = t.value.length - _s;
    t.setSelectionRange(_s, _s);
}
function convertMoney(d) {
    var _isNumber = typeof (d) === 'number' ? true : false;
    var _d;
    if (d === undefined || d === null || d === '') {
        return 0;
    }
    var d = String(d);
    var x1 = d;
    if (x1 === undefined || x1 === null) {
        return 0;
    }
    if (_isNumber) {
        x1 = x1.replaceAll('[.]', ',');
    }
    else {
        x1 = x1.replaceAll('[.]', '');
    }
    _d = x1;
    var _x2 = '';
    if (x1.split(',').length > 1) {
        _x2 = x1.split(',')[1];
        if (_x2.length > 2) {
            _x2 = _x2.substring(0, 2);
        }
        x1 = x1.split(',')[0];
    }
    x1 = parseInt(x1).toString();
    var i1 = String(x1);
    i1 = i1.split('').reverse().join('');
    var arr = [];
    while (i1.length > 0) {
        arr.push(i1.substr(0, 3));
        i1 = i1.slice(3, i1.length);
    }
    for (var i = 0; i < arr.length; i++) {
        if (i === (arr.length - 1))
            i1 += arr[i];
        else {
            i1 += arr[i] + ".";
        }
    }
    i1 = i1.split('').reverse().join('');
    if (_x2 !== undefined && _x2 !== null && _d.split(',').length > 1) {
        x1 = i1 + ',' + _x2;
    } else {
        x1 = i1;
    }
    return x1;
}
function convertMoneyDecimal(d) {
    if (d === undefined || d === null || d === '') {
        return 0;
    }
    //Created by habx@esvn.com.vn  
    if (typeof (d) === 'number') {
        d = d.toString().replaceAll('[.]', ',');
    }
    //Created by habx@esvn.com.vn
    var d = String(d);
    var x1 = d;
    if (x1 === undefined || x1 === null) {
        return 0;
    }
    x1 = x1.replaceAll('[.]', '');
    var _x2 = '';
    if (x1.split(',').length > 1) {
        _x2 = x1.split(',')[1];
        if (_x2.length > 2) {
            _x2 = _x2.substring(0, 2);
        }
        x1 = x1.split(',')[0];
    }
    x1 = parseInt(x1).toString();
    var i1 = String(x1);
    i1 = i1.split('').reverse().join('');
    var arr = [];
    while (i1.length > 0) {
        arr.push(i1.substr(0, 3));
        i1 = i1.slice(3, i1.length);
    }
    for (var i = 0; i < arr.length; i++) {
        if (i === (arr.length - 1))
            i1 += arr[i];
        else {
            i1 += arr[i] + ".";
        }
    }
    i1 = i1.split('').reverse().join('');
    if (_x2 !== undefined && _x2 !== null && d.split(',').length > 1) {
        x1 = i1 + ',' + _x2;
    } else {
        x1 = i1;
    }
    return x1;
}
function RenderGuid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

aimodu.factory('utility', function () {
    return {
        SUPPORT_IMG: [
            { key: 'doc', type: 'file' },
            { key: 'doc-icon', type: 'file' },
            { key: 'docx', type: 'file' },
            { key: 'docx-icon', type: 'file' },
            { key: 'excel', type: 'file' },
            { key: 'excel-icon', type: 'file' },
            { key: 'excel-icon-1', type: 'file' },
            { key: 'folder', type: 'img' },
            { key: 'folder-icon', type: 'img' },
            { key: 'image', type: 'img' },
            { key: 'image-icon', type: 'img' },
            { key: 'img', type: 'img' },
            { key: 'img-icon', type: 'img' },
            { key: 'jpeg', type: 'img' },
            { key: 'jpg', type: 'img' },
            { key: 'jpg-icon', type: 'img' },
            { key: 'mp4', type: 'file' },
            { key: 'mp4-icon', type: 'file' },
            { key: 'pdf', type: 'file' },
            { key: 'pdf-icon', type: 'file' },
            { key: 'pdf-icon-1', type: 'file' },
            { key: 'png', type: 'img' },
            { key: 'png-icon', type: 'img' },
            { key: 'ppt', type: 'file' },
            { key: 'ppt-icon', type: 'file' },
            { key: 'ppt-icon-1', type: 'file' },
            { key: 'rar', type: 'file' },
            { key: 'rar-icon', type: 'file' },
            { key: 'txt', type: 'file' },
            { key: 'txt-file', type: 'file' },
            { key: 'txt-icon', type: 'img' },
            { key: 'word', type: 'file' },
            { key: 'word-icon', type: 'file' },
            { key: 'word-icon-1', type: 'file' },
            { key: 'xls', type: 'file' },
            { key: 'xls-icon', type: 'file' },
            { key: 'xlxs', type: 'file' },
            { key: 'xlsx-icon', type: 'file' },
            { key: 'zip', type: 'file' },
            { key: 'zip-icon', type: 'file' },
        ],
        subTitle: function (data, num, last) {
            if (data === null || data === "") {
                return last;
            }
            if (data.length <= num) {
                return data;
            }
            var temp = data.substring(0, num - 1);
            return temp + last;
        },
        COMMON_LINK_API: typeof (CONFIGSETTING_OBJECT) !== 'undefined' ? {
            link_employee: CONFIGSETTING_OBJECT["APIHost".toUpperCase()] + (CONFIGSETTING_OBJECT["APIGetImage".toUpperCase()] ? CONFIGSETTING_OBJECT["APIGetImage".toUpperCase()] : '/api/Common/Employee/getimage/'),
            link_employee_image_email: CONFIGSETTING_OBJECT["APIHost".toUpperCase()] + CONFIGSETTING_OBJECT["APIGetImageByEmail".toUpperCase()],
            link_employee_loginName: CONFIGSETTING_OBJECT["APIHost".toUpperCase()] + '/api/Common/Employee/getImageLoginName/',
            link_type_icon: CONFIGSETTING_OBJECT["DataStatic".toUpperCase()] + '/img/fileicon/',
            link_document_icon: CONFIGSETTING_OBJECT["DataStatic".toUpperCase()] + '/img/file-manager/',
            link_document_user: CONFIGSETTING_OBJECT["DataStatic".toUpperCase()] + '/img/users/',
            link_common: CONFIGSETTING_OBJECT["DataStatic".toUpperCase()] + '/img/',
            link_user: CONFIGSETTING_OBJECT["DataStatic".toUpperCase()] + '/img/avatars/',
            link_domain: CONFIGSETTING_OBJECT["DataStatic".toUpperCase()] + '/',
            link_domain_apicm: CONFIGSETTING_OBJECT["APIHost".toUpperCase()],
        } : {},
        COMMON_ICON_PUBLIC: function (d) {
            if (d !== undefined && d !== null && d !== '' && d.trim() !== '' && typeof (d) === 'string') {
                var _d = d.toLowerCase();
                for (var i = 0; i < this.SUPPORT_IMG.length; i++) {
                    if (this.SUPPORT_IMG[i].key.toUpperCase() === d.toUpperCase()) {
                        return d + '.png';
                    }
                }
            }
            return 'default.png';
        },
        COMMON_ICON_ANIMATION: {
            init: ['QAK', 'mini'],
            QAK: {
                link: '/img/chatbox/icon_q/a',
                name: 'QAK',
                default: '/img/chatbox/icon_q/icon.png',
                type: '.png',
                data: 32
            },
            mini: {
                link: '/img/chatbox/icon_d/a',
                name: 'mini',
                default: '/img/chatbox/icon_d/icon.png',
                type: '.png',
                data: 40
            },
            renderCode: function (code, sort) {
                return code + '%' + sort;
            },
            renderImg: function (code) {
                var _a = code.split('%');
                return CONFIGSETTING_OBJECT['DATASTATIC'] + this[_a[0]].link + _a[1] + this[_a[0]].type;
            }
        }
        //var _liExtended = ['jpg', ''];
    }
});
// begin config link image for Employee
aimodu.directive('scrollToBottom', function ($timeout, $window) {
    return {
        scope: {
            scrollToBottom: "="
        },
        restrict: 'A',
        link: function (scope, element, attr) {
            scope.$watchCollection('scrollToBottom', function (newVal) {
                if (newVal) {
                    $timeout(function () {
                        element[0].scrollTop = element[0].scrollHeight;
                    }, 0);
                }

            });
        }
    };
});
// end config link image for Employee
aimodu.directive('customOnChange', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var onChangeHandler = scope.$eval(attrs.customOnChange);
            element.on('change', onChangeHandler);
            element.on('$destroy', function () {
                element.off();
            });

        }
    };
});
//Begin thiết đặt dung lượng upload tối đa (đơn vị MB)
var FILE_SIZE_UPLOAD = 50;
function ChangeUploadFile() {
    if ($(this).prop('files')[0] !== undefined) {
        var _file = $(this).prop('files')[0];
        if (_file.size > (1024 * 1024) * FILE_SIZE_UPLOAD) {
            $(this).val('');
            ESConfirm([{ 'value': 1, 'title': 'Đóng', 'type': 'info' }], { 'description': 'Chỉ được upload tối đa ' + FILE_SIZE_UPLOAD + 'MB.', 'class': 'eswarning_v3' }, function (rs) {
                if (rs === '1') {
                }
            }, function () {
                //some error in this function
            });
        }
    };
}
document.onkeydown = function (e) {
    var _el = $('input[type="file"]');
    for (var i = 0; i < _el.length; i++) {
        $(_el[i]).change(ChangeUploadFile);
    }
};
document.onmousedown = function (e) {
    var _el = $('input[type="file"]');
    for (var i = 0; i < _el.length; i++) {
        $(_el[i]).change(ChangeUploadFile);
    }
};
//End thiết đặt dung lượng upload tối đa (đơn vị MB)
//tính hiệu 2 ngày 
function SubtractDate(date1, date2) {
    function ConDate(data, number) {
        if (data == null || data == "") {
            return '';
        }
        if (data !== null && data != "" && data != undefined) {
            try {
                if (data.indexOf("SA") != -1 || data.indexOf("CH") != -1) {
                    if (data.indexOf("SA") != -1) { }
                    if (data.indexOf("CH") != -1) { }
                }

                if (data.indexOf('Date') != -1) {
                    data = data.substring(data.indexOf("Date") + 5);
                    data = parseInt(data);
                }
            }
            catch (ex) { }
            var newdate = new Date(data);
            if (number == 3) {
                return newdate;
            }
            if (number == 2) {
                return "/Date(" + newdate.getTime() + ")/";
            }
            var month = newdate.getMonth() + 1;
            var day = newdate.getDate();
            var year = newdate.getFullYear();
            var hh = newdate.getHours();
            var mm = newdate.getMinutes();
            if (month < 10)
                month = "0" + month;
            if (day < 10)
                day = "0" + day;
            if (mm < 10)
                mm = "0" + mm;
            if (number == 0) {
                return todayDate = day + "/" + month + "/" + year;
            }
            if (number == 1) {
                return todayDate = day + "/" + month + "/" + year + " " + hh + ":" + mm;
            }
            if (number == 4) {
                return new Date(year, month - 1, day);
            }
        } else {
            return '';
        }
    }
    var _date1 = ConDate(date1, 3);
    var _date2 = ConDate(date2, 3);
    //Return thời gian
    const diffTime = Math.abs(_date2.getTime() - _date1.getTime());
    //Return ngày
    return Math.ceil(diffTime / (1000 * 60 * 60 * 24));
}
//Cắt chuỗi rút gọn theo số lượng ký tự truyền vào
function _subTitle(data, num, last) {
    if (data === null || data === "") {
        return last;
    }
    if (data.length <= num) {
        return data;
    }
    var temp = data.substring(0, num - 1);
    return temp + last;
};
//begin habx@esvn.com.vn
var SATATE_NOTIFI = true;
function restartNotiAlert() {
    setTimeout(function () {
        SATATE_NOTIFI = true;
    }, 1000);
}
function notifyAlert(d1, link) {
    if (Notification.permission !== "granted")
        Notification.requestPermission();
    else {
        var _link = link;
        var notification = new Notification('Thông báo: Tin nhắn mới.', {
            icon: CONFIGSETTING_OBJECT["DataStatic".toUpperCase()] + '/img/logo-sm.png',
            body: d1,
        });
        notification.onclick = function () {
            window.open(_link);
        };
    }
}
function notifyChat(d1, d2) {
    if (SATATE_NOTIFI) {
        SATATE_NOTIFI = false;
        if (Notification.permission !== "granted")
            Notification.requestPermission();
        else {
            var notification = new Notification('CHATBOX: Bạn có tin mới', {
                icon: CONFIGSETTING_OBJECT["DataStatic".toUpperCase()] + '/img/logo-sm.png',
                body: d2 == '' ? '' : d1,
            });
            notification.onclick = function () {
                window.open('/');
            };
        }
        restartNotiAlert();
    }
}
function notifyChatGroup(d1) {
    if (SATATE_NOTIFI) {
        SATATE_NOTIFI = false;
        if (Notification.permission !== "granted")
            Notification.requestPermission();
        else {
            var notification = new Notification('CHATBOX: Bạn có tin mới từ nhóm', {
                icon: CONFIGSETTING_OBJECT["DataStatic".toUpperCase()] + '/img/logo-sm.png',
                body: d2 == '' ? '' : d1,
            });
            notification.onclick = function () {
                window.open('/');
            };
        }
        restartNotiAlert();
    }
}
function addItemToMenuAlert(data) {
    var _el = $('#esAlertItems');
    for (var i = 0; i < data.length; i++) {
        var _elDiv = document.createElement('div');
        $(_elDiv).addClass("navbar-dropdown-notification es-cursor");
        if (!data[i].IsRead) {
            $(_elDiv).addClass("is-new");
        }
        $(_elDiv).attr('title', "Bấm vào để xem chi tiết.");
        $(_elDiv).on('click', { url: data[i].Url }, function (rs) {
            if (rs.data.url) {
                window.open(rs.data.url);
            }
        })

        var _el2 = document.createElement('div');
        $(_el2).addClass("navbar-dropdown-notification__user");

        var _el2_1 = document.createElement('img');
        $(_el2_1).addClass('navbar-dropdown-notification__avatar rounded-circle');
        $(_el2_1).attr("src", CONFIGSETTING_OBJECT['APIHOST'.toUpperCase()] + CONFIGSETTING_OBJECT['APIGetImageLoginName'.toUpperCase()] + data[i].FromLoginName);
        $(_el2_1).attr('width', '40');
        $(_el2_1).attr('height', '40');
        _el2.appendChild(_el2_1);

        var _el2_2 = document.createElement('div');
        $(_el2_2).addClass(data[i].IconUrl + ' navbar-dropdown-notification__icon color-success');
        _el2.appendChild(_el2_2);

        var _el3 = document.createElement('div');
        $(_el3).addClass("navbar-dropdown-notification__content");

        var _el3_1 = document.createElement('a');
        $(_el3_1).addClass("navbar-dropdown-notification__action-name");
        _el3_1.innerText = data[i].Title;
        _el3.appendChild(_el3_1);

        var _el3_2 = document.createElement('div');
        $(_el3_2).addClass("m-invoices__activity-event-text-date");
        _el3_2.innerText = _subTitle(data[i].Description.replace(/(\r\n|\n|\r)/gm, ""), 30, '...');
        _el3.appendChild(_el3_2);

        var _el3_3 = document.createElement('div');
        $(_el3_3).addClass("m-invoices__activity-event-text-date");
        _el3_3.innerText = data[i].DepartmentName;
        _el3.appendChild(_el3_3);

        var _el4 = document.createElement('span');
        $(_el4).addClass("navbar-dropdown-notification__date");
        _el4.innerText = ConDate(data[i].CreatedDate, 1);

        _elDiv.appendChild(_el2);
        _elDiv.appendChild(_el3);
        _elDiv.appendChild(_el4);
        $(_elDiv).append(createMarkCheck(data[i].AlertGuid));
        _el.prepend(_elDiv);
    }
}
function createMarkCheck(g) {
    var _el = document.createElement('div');
    $(_el).addClass('navbar-dropdown-notifications__item-actions');
    var _span1 = document.createElement('span');
    $(_span1).attr('title', 'Đã biết');
    $(_span1).addClass('icon ua-icon-circle-check navbar-dropdown-notifications__item-mark-as-read');
    $(_span1).on('click', { data: g }, function (e) {
        _activeRead([e.data.data]);
        $($($(e.currentTarget).parent()).parent()).removeClass('is-new');
        e.stopPropagation();
    })
    $(_el).append(_span1);
    //var _span2 = document.createElement('span');
    //$(_span2).attr('title', 'Xóa');
    //$(_span2).addClass('icon ua-icon-circle-close navbar-dropdown-notifications__item-remove');
    //$(_span1).on('click', { g }, function (e) {
    //    _activeDelete([e.data]);
    //});
    //$(_el).append(_span2);

    return _el;
}
function createMenuAlert() {
    var _menuAlert = document.createElement('div');
    $(_menuAlert).addClass("navbar-dropdown-notifications__header");

    var _span = document.createElement('span');
    $(_span).text('Thông báo');

    var _a = document.createElement('a');
    $(_a).addClass('navbar-dropdown-notifications__mark-read');
    $(_a).text('Chọn tất cả đã đọc');
    $(_a).on('click', function (rs) {
        activeReadAll();
    })

    var _a_span = document.createElement('span');
    $(_a_span).addClass('navbar-dropdown-notifications__mark-read-icon ua-icon-circle-check');
    _a.appendChild(_a_span);

    //begin add list 
    var _div = document.createElement('div');
    $(_div).addClass("navbar-dropdown-notifications__body task-scroll-custom");
    $(_div).attr('id', 'esAlertItems');
    $(_div).css('height', '70vh');

    _menuAlert.appendChild(_span);
    _menuAlert.appendChild(_a);
    //end add list

    $('#esRootMenuAlert').append(_menuAlert);
    $('#esRootMenuAlert').append(_div);
}
function activeReadAll() {
    $('#esAlertItems .is-new').removeClass('is-new');
    _activeRead(GUID_ALERT);
    $('#esAlertItems').html('');
    GUID_ALERT = [];
}
function _activeRead(_g) {
    var _tem = {
        GuidAlert: _g,
        LoginName: CONFIGSETTING_OBJECT['LOGINNAME'],
        OrganizationGuid: CONFIGSETTING_OBJECT['ORGANIZATIONGUID'],
    };
    connectionAlert.server.readAlert(_tem);
}
function _activeDelete(_g) {
    var _tem = {
        GuidAlert: _g,
        LoginName: CONFIGSETTING_OBJECT['LOGINNAME'],
        OrganizationGuid: CONFIGSETTING_OBJECT['ORGANIZATIONGUID'],
    };
    connectionAlert.server.deleteAlert(_tem);
}
function openMenuAlert() {
    var _el = $('.navbar-notify--notifications');
    var _menuAlertActive = false;
    _el.on('click', function (rs) {
        _menuAlertActive = false;
        $('#esMenuAlert').toggleClass('show_Menu_Alert');
        _menuAlertActive = true;
    })
    $('body').on('click', function (rs) {
        if (_menuAlertActive) {
            $('#esMenuAlert').removeClass('show_Menu_Alert');
        }
    })
}
//StopService
//PingRead (username, list<AlertGuid>)
var RUNTIME_V1 = {
    SendTo: SendMessChat,
    ReceiveFrom: {},
    NotifyAlert: notifyAlert,
    PingOnline: function () { },
};
var connectionAlert;
var GUID_ALERT = [];
function loadSignalrJS() {
    var _newScript;
    _newScript = document.createElement('script');
    _newScript.type = 'text/javascript';
    //_newScript.src = CONFIGSETTING_OBJECT["DataStatic".toUpperCase()] + '/plugins/signalr/signalr.js';
    _newScript.src = CONFIGSETTING_OBJECT["DomainMessRuntime".toUpperCase()] + '/content/jquery.signalR-2.4.1.js';
    document.getElementsByTagName("head")[0].appendChild(_newScript);
}
function loadHub() {
    var _newScript;
    _newScript = document.createElement('script');
    _newScript.type = 'text/javascript';
    //_newScript.src = CONFIGSETTING_OBJECT["DataStatic".toUpperCase()] + '/plugins/signalr/signalr.js';
    _newScript.src = CONFIGSETTING_OBJECT["DomainMessRuntime".toUpperCase()] + '/ChatHub/hubs';
    document.getElementsByTagName("head")[0].appendChild(_newScript);
}
function loadOneSignal() {
    var _newScript;
    _newScript = document.createElement('script');
    _newScript.type = 'text/javascript';
    //_newScript.src = CONFIGSETTING_OBJECT["DataStatic".toUpperCase()] + '/plugins/signalr/signalr.js';
    _newScript.src = 'https://cdn.onesignal.com/sdks/OneSignalSDK.js';
    document.getElementsByTagName("head")[0].appendChild(_newScript);
}
function RuntimeAlert() {
    connectionAlert.server.ping();
}
function CallMes() {
    connectionAlert.server.sendMess(loginName);
}
function SendMessChat(obj) {
    connectionAlert.server.sendMessChat(obj);
}
function SendMessChat(obj) {
    obj.LoginName = CONFIGSETTING_OBJECT["LoginName".toUpperCase()];
    obj.OrganizationGuid = CONFIGSETTING_OBJECT["OrganizationGuid".toUpperCase()];
    connectionAlert.server.sendMessChat(obj);
}
function _alertRuntime() {
    var _obj = {
        LoginName: CONFIGSETTING_OBJECT["LoginName".toUpperCase()],
        OrganizationGuid: CONFIGSETTING_OBJECT["OrganizationGuid".toUpperCase()],
    };
    connectionAlert.server.alert(_obj);
}
function CheckHub_MVC5() {
    $.connection.hub.start().done(function () {
        var _obj = {
            LoginName: CONFIGSETTING_OBJECT["LoginName".toUpperCase()],
            OrganizationGuid: CONFIGSETTING_OBJECT["OrganizationGuid".toUpperCase()],
        };
        connectionAlert.server.ping(_obj);
        setTimeout(function () {
            setInterval(function () {
                _alertRuntime();
            }, 15000);
        }, 2000);
    })
        .fail(() => { console.log('connect signalr error') });
}
function CreateAlert(t, d) {
    for (var i = 0; i < d.length; i++) {
        for (var j = 0; j < GUID_ALERT.length; j++) {
            if (GUID_ALERT[j] === d[i].AlertGuid) {
                d.splice(i, 1);
                i--;
                break;
            }
        }
    }
    _countAlert = 0;
    for (var i = 0; i < d.length; i++) {
        if (!d[i].IsRead) {
            _countAlert++;
        }
        GUID_ALERT.push(d[i].AlertGuid);
    }
    if (_countAlert === 1) {
        notifyAlert(d[0].Title, d[0].Url);
    } else {
        if (_countAlert > 1) {
            notifyAlert('Bạn có ' + _countAlert + ' thông báo.');
        }
    }
    if (_countAlert === 0) {
        $('#esRootMenuAlertCount').hide();
    } else {
        $('#esRootMenuAlertCount').show();
    }
    $('#esRootMenuAlertCount').text((_countAlert !== 0) ? _countAlert : '');
    addItemToMenuAlert(d);
}
function initAlert() {
    if ($.connection.chatHub !== undefined && $.connection.chatHub !== null) {
        var _url = CONFIGSETTING_OBJECT["DomainMessRuntime".toUpperCase()];
        $.connection.hub.url = _url + '/ChatHub';
        $.connection.hub.logging = true;
        connectionAlert = $.connection.chatHub;
        connectionAlert.client.ReceiveMessChat = function (n, mO) {
            if (typeof (RUNTIME_V1.ReceiveFrom) === 'object') {
                var _m = JSON.parse(mO);
                if (_m.Code === 'CHAT') {
                    var _item = [{
                        url: 'https://tm.easternsun.vn/Chatbox#/',
                        IsRead: false,
                        IconUrl: 'ua-icon-letter-alt ',
                        Title: '[Tin nhắn] ' + _m.FullName,
                        Description: _m.Message,
                        DepartmentName: '',
                        CreatedDate: new Date(_m.Time),
                        AlertGuid: '',
                    }];
                    addItemToMenuAlert(_item);
                    notifyChat(_m.Message, 'https://tm.easternsun.vn/Chatbox#/');
                }
            } else {
                RUNTIME_V1.ReceiveFrom(n, mO);
            }
        };
        connectionAlert.client.PingOnline = function (s, obj) {
            RUNTIME_V1.PingOnline(s, obj);
        };
        connectionAlert.client.Alert = function (t, d) {
            CreateAlert(t, JSON.parse(d));
        };
        connectionAlert.client.SendAsync = function (t, d) {
            console.log(t);
            console.log(d);
        };
        CheckHub_MVC5();
    } else {
        setTimeout(initAlert, 200);
    }
    // Begin core 2 for signalr
    //if (typeof (signalR) === 'object' && signalR !== null && signalR !== undefined) {
    //    connectionAlert = new signalR.HubConnection(_url, {
    //        accessTokenFactory: () =>
    //            CONFIGSETTING_OBJECT['TOKEN']
    //    });
    //    var _countAlert = 0;
    //    connectionAlert.on('ReceivePing', function (data) {
    //        for (var i = 0; i < data.length; i++) {
    //            for (var j = 0; j < GUID_ALERT.length; j++) {
    //                if (GUID_ALERT[j] === data[i].AlertGuid) {
    //                    data.splice(i, 1);
    //                    i--;
    //                    break;
    //                }
    //            }
    //        }
    //        _countAlert = 0;
    //        for (var i = 0; i < data.length; i++) {
    //            if (!data[i].IsRead) {
    //                _countAlert++;
    //            }
    //            GUID_ALERT.push(data[i].AlertGuid);
    //        }
    //        if (_countAlert == 1) {
    //            notifyAlert(data[0].Title, data[0].Url);
    //        } else {
    //            if (_countAlert > 1) {
    //                notifyAlert('Bạn có ' + _countAlert + ' thông báo.');
    //            }
    //        }
    //        if (_countAlert === 0) {
    //            $('#esRootMenuAlertCount').hide();
    //        } else {
    //            $('#esRootMenuAlertCount').show();
    //        }
    //        $('#esRootMenuAlertCount').text((_countAlert !== 0) ? _countAlert : '');
    //        addItemToMenuAlert(data);
    //    });
    //    connectionAlert.on('ReceiveMessChat', function (d1, d2) {
    //        RUNTIME_V1.ReceiveFrom(d1,d2);
    //    });
    //    connectionAlert.on('ErrorPing', function (d1) { console.log(d1) });
    //    connectionAlert.on('ReceivePing', function (data) {
    //        for (var i = 0; i < data.length; i++) {
    //            for (var j = 0; j < GUID_ALERT.length; j++) {
    //                if (GUID_ALERT[j] === data[i].AlertGuid) {
    //                    data.splice(i, 1);
    //                    i--;
    //                    break;
    //                }
    //            }
    //        }
    //        _countAlert = 0;
    //        for (var i = 0; i < data.length; i++) {
    //            if (!data[i].IsRead) {
    //                _countAlert++;
    //            }
    //            GUID_ALERT.push(data[i].AlertGuid);
    //        }
    //        if (_countAlert == 1) {
    //            notifyAlert(data[0].Title, data[0].Url);
    //        } else {
    //            if (_countAlert > 1) {
    //                notifyAlert('Bạn có ' + _countAlert + ' thông báo.');
    //            }
    //        }
    //        if (_countAlert === 0) {
    //            $('#esRootMenuAlertCount').hide();
    //        } else {
    //            $('#esRootMenuAlertCount').show();
    //        }
    //        $('#esRootMenuAlertCount').text((_countAlert !== 0) ? _countAlert : '');
    //        addItemToMenuAlert(data);
    //    });
    //    connectionAlert.start()
    //        .then(function () {
    //            connectionAlert.send('Ping', loginName);
    //            setInterval(RuntimeAlert, 5000);
    //        })
    //        .catch(error => {
    //            console.log('SignalR log: ' + error.message);
    //        });
    //    connectionAlert.on("PingOffline", function (d1) {
    //        console.log(d1);
    //        connectionAlert.stop();
    //    })
    //} else {
    //    setTimeout(initAlert, 1000);
    //}
    // End core 2 for signalr
}
function loadSignalrHub() {
    if (typeof ($.signalR) !== "function") {
        setTimeout(loadSignalrHub, 200);
    } else {
        loadHub();
        createMenuAlert();
        initAlert();
    }
}
function loadOneSignalHub() {
    if (typeof ($.signalR) !== "function") {
        setTimeout(loadSignalrHub, 200);
    } else {
        loadHub();
        createMenuAlert();
        initAlert();
    }
}
if (CONFIGSETTING_OBJECT["SERVERRUNTIME".toUpperCase()] && CONFIGSETTING_OBJECT["SERVERRUNTIME".toUpperCase()].toUpperCase() === 'ON') {
    loadSignalrJS();
    loadSignalrHub();
}
//end habx@esvn.com.vn
// begin add hover image open new larg image
function viewImageDataTable() {
    var _liImgae = $('.es_viewImage').find('img');
    var _el = {};
    for (var i = 0; i < _liImgae.length; i++) {
        $(_liImgae[i]).unbind();
        $(_liImgae[i]).hover(function () {
            var _el = document.createElement('div');
            var _img = document.createElement('img');
            $(_img).attr("src", $(this).attr('src'));
            $(_img).css('max-width', '150px');
            $(_img).css('max-height', '150px');
            $(_el).append(_img);
            $(_el).attr('id', 'viewImageFromThum');
            $(_el).css('position', 'absolute');
            $(_el).css('max-width', '150px');
            $(_el).css('max-height', '150px');
            $(_el).css('z-index', '999999999999999');
            $(_el).css('top', $(this).offset().top + $(this).height());
            $(_el).css('left', $(this).offset().left + $(this).width());
            $('body').append(_el);
        }, function () {
            $('#viewImageFromThum').remove();
        })
    }
}
window.addEventListener('DOMContentLoaded', function () {
    setInterval(viewImageDataTable, 2000);
});
// end add hover image
// Begin habx@esvn.com.vn sort data type file 
//d : data input 
//t : list type will sort
//n : name fied will sort
function SortDataByType(d, t, n) {
    try {
        if (typeof (d) === 'object') {
            var _d = [];
            if (typeof (t) === 'object') {
                for (var i = 0; i < t.length; i++) {
                    for (var j = 0; j < d.length; j++) {
                        if (d[j][n].toUpperCase() === t[i].toUpperCase()) {
                            _d.push(d[j]);
                            d.splice(j, 1);
                            j--;
                        }
                    }
                }
                for (var i = 0; i < d.length; i++) {
                    _d.push(d[i]);
                }
                d = _d;
                return _d;
            }
        }
    }
    catch (ex) {
        console.log(ex);
    }
    return d;
}
// End habx@esvn.com.vn sort data type file


// Begin create by habx@esvn.com.vn 2020/07/17 save setting by user in this browser and async to server database
var UTILITI_SET_SAVE_CONFIG = {
    KEY: 'ESVN_' + CONFIGSETTING_OBJECT['LOGINNAME'] + '_' + location.pathname,
    SET_KEY_SETTING: function (k, v) {
        var _o = JSON.parse(localStorage.getItem(this.KEY) ? localStorage.getItem(this.KEY) : '{}');
        _o[k] = v;
        localStorage.setItem(this.KEY, JSON.stringify(_o));
    },
    GET_KEY_SETTING: function (k) {
        var _o = JSON.parse(localStorage.getItem(this.KEY) ? localStorage.getItem(this.KEY) : '{}');
        return _o[k] ? _o[k] : {};
    }
}
// End create by habx@esvn.com.vn 2020/07/17
// Begin createdby habx@esvn.com.vn
var PLUGIN_DATATABLE_TABLE_INIT = [];
var PLUGIN_DATATABLE_TABLE_SCROLL = {
    EVENT: [],
    EVENTROOT: [],
    __EVENT: function (e) {
        if (!e.data.on) e.preventDefault();
        if (this.scrollHeight <= (this.scrollTop + this.clientHeight + 10) && e.data.on && this.scrollTop > e.data.next) {
            e.data.on = false;
            $(this).scrollTop(this.scrollTop - 11);
            for (var i = 0; i < PLUGIN_DATATABLE_TABLE_SCROLL.EVENTROOT.length; i++) {
                if (PLUGIN_DATATABLE_TABLE_SCROLL.EVENTROOT[i].id === e.data.id) {
                    PLUGIN_DATATABLE_TABLE_SCROLL.EVENTROOT[i].scroll();
                    break;
                }
            }
        }
        e.data.next = this.scrollTop;
    },
    INIT_EVENT: function (id, f) {
        if (typeof (f) !== 'function') {
            console.log('You need init function scroll input')
            return;
        }
        var _item = {
            id: id,
            scroll: f,
        };
        var _check = true;
        for (var i = 0; i < this.EVENTROOT.length; i++) {
            if (this.EVENTROOT[i].id === _item.id) {
                this.EVENTROOT[i].scroll = f;
                _check = false;
                break;
            }
        }
        if (_check) this.EVENTROOT.push(_item);
    },
    UNLOCK_EVENT: function (id) {
        for (var i = 0; i < PLUGIN_DATATABLE_TABLE_SCROLL.EVENT.length; i++) {
            if (PLUGIN_DATATABLE_TABLE_SCROLL.EVENT[i].id === id) {
                PLUGIN_DATATABLE_TABLE_SCROLL.EVENT[i].on = true;
                break;
            }
        }
    },
    SET_EVENT: function (s) {
        var _item = {
            id: s.sTableId,
            on: true,
            next: 0,
            scroll: function () {
            },
        };
        var _check = true;
        for (var i = 0; i < this.EVENT.length; i++) {
            if (this.EVENT[i].id === _item.id) {
                _item = this.EVENT[i];
                _check = false;
                break;
            }
        }
        if (_check) this.EVENT.push(_item);
        var _el = $('#' + s.sTableId).parent();
        $(_el).on('scroll', _item, this.__EVENT)
    }
};
var PLUGIN_DATATABLE_TABLE_Instance = [];
// PLUGIN_DATATABLE_MENU_FILTER using for filter: para 1
// PLUGIN_DATATABLE_MENU_COLUMN using for show column: para 1
function __Datatable_GetAll_Table() {
    try {
        var _t = $('[ng-controller]');
        var _p = angular.element(_t).scope().$$nextSibling.$$childTail;
        if (!_p || _p === null) {
            setTimeout(__Datatable_GetAll_Table, 1000);
        }
        for (var _item in _p) {
            if (_item && _item.includes('dtInstance')) {
                PLUGIN_DATATABLE_TABLE_INIT.push({ id: _p[_item].id, o: _p[_item] });
            }
        }
        setTimeout(function () {
            __Datatable_AutoFill_ShowColumn();
        }, 2000);
    } catch (ex) { console.log(ex) }
}