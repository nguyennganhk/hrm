﻿(function (app) {
    app.controller('homecontroller', HomeController);
    HomeController.$inject = ['$scope', '$filter', '$window', '$timeout', '$rootScope'];

    function HomeController($scope) {
        $scope.staticParam = {};
        $scope.staticParam.totalItems = 0;
        $scope.staticParam.CurentPage1 = 1;
        $scope.staticParam.Length = 4;

        angular.element(document).ready(function () {
            $scope.search();
        });

        $scope.pageChanged = function (item) {
            $scope.staticParam.CurentPage1 = item;
            $scope.search();
        }

        $scope.search = function () {
            var fd = new FormData();
            fd.append('data', JSON.stringify($scope.staticParam));
            $.ajax({
                type: 'post',
                url: '/Home/GetWorkFlow',
                processData: false,
                contentType: false,
                data: fd,
                success: function (rss) {
                    var rs = JSON.parse(rss.data);
                    $scope.listData = rs;
                    $scope.staticParam.totalItems = rs.total;
                    $scope.$apply();
                }
            })
        }

        $scope.Update = function (item) {
            window.location.href = "/Home/WorkFlow?id=" + item;
        }

        $scope.CreateWorkFlow = function () {
            window.location.href = "/DesignWorkflows/Index";
        }

        $scope.Delete = function (temp) {
            $.ajax({
                type: 'post',
                url: '/Home/Delete',
                success: function (rs) {
                    if (rs.error) {
                        toastr["error"](rs.Title);
                    }
                    else {
                        toastr["success"](rs.Title);
                    }
                }
            })
        }
    } 
})(angular.module('e-app'));

