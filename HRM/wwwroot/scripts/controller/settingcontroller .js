﻿(function (app) {
    var ctxfolder = "/views/Settings";
    app.factory('dataservice', function ($http) {
        var headers = {
            "Content-Type": "application/json;odata=verbose",
            "Accept": "application/json;odata=verbose",
        }
        return {
            insert: function (data, callback) {
                $http.post('/Settings/insert', data).success(callback);
            },
            update: function (data, callback) {
                $http.post('/Settings/update', data).success(callback);
            },
            deleteItems: function (data, callback) {
                $http.post('/c/deleteItems', data).success(callback);
            },
            delete: function (data, callback) {
                $http.post('/Settings/delete/' + data).success(callback);
            },
            getItem: function (data, callback) {
                $http.post('/Settings/getitem/' + data).success(callback);
            },
            getAll: function (data, callback) {
                $http.post('/Settings/getAll/' + data).success(callback);
            },
            gettreedata: function (data, callback) {
                $http.post('/Settings/gettreedata/' + data).success(callback);
            },
            getbyparent: function (data, callback) {
                $http.post('/Settings/getbyparent/' + data).success(callback);
            },
            UpdateIsActive: function (data, callback) {
                $http.post('/Settings/UpdateIsActive', data).success(callback);
            },
            CheckStatus: function (data, callback) {
                $http.post('/Settings/CheckStatus/' + data).success(callback);
            },
            resort: function (data, callback) {
                $http.post('/Settings/resort', data).success(callback);
            },
        }
    });
    app.filter('fdate', [
        '$filter', function ($filter) {
            return function (input, f) {
                if (input && input.toString().indexOf('Date') > -1) {
                    return moment(input).format(f);
                } if (input && input.toString().indexOf('T') > -1) {
                    return moment(input).format(f);
                } else return input;
            };
        }
    ]);
    app.directive('format', ['$filter', function ($filter) {
        return {
            require: '?ngModel',
            link: function (scope, elem, attrs, ctrl) {
                if (!ctrl) return;


                ctrl.$formatters.unshift(function (a) {
                    return $filter(attrs.format)(ctrl.$modelValue)
                });


                ctrl.$parsers.unshift(function (viewValue) {
                    var plainNumber = viewValue.replace(/[^\d|\-+|\.+]/g, '');
                    elem.val($filter(attrs.format)(plainNumber));
                    return plainNumber;
                });
            }
        };
    }]);
    app.filter('fstatus', [
        '$filter', function ($filter) {
            return function (s) {
                if (s === false) return "Không sử dụng";
                if (s === true) return "Sử dụng";
                return "";
            };
        }
    ]);
    app.filter('ValueType', [
        '$filter', function ($filter) {
            return function (data) {
                if (data == 0) {
                    return 'Số nguyên';
                } else if (data == 1) {
                    return 'Số thực (Decimal)';
                } else if (data == 2) {
                    return 'Ngày tháng';
                } else if (data == 3) {
                    return 'Chuỗi';
                } else if (data == 4) {
                    return 'Giá trị Max - Min';
                } else if (data == 5) {
                    return 'Chọn nhiều (Checkbox)';
                } else if (data == 5) {
                    return 'Chọn một (Optional)';
                } else if (data == 7) {
                    return 'Ngày bắt đầu - Ngày Kết thúc';
                } else if (data == 8) {
                    return 'Danh mục';
                } else {
                    return '';
                }
            };
        }
    ]);

    app.controller('Ctrl_ESSALE_Settings', function ($scope, $rootScope, $compile, $uibModal, dataservice) {
        $rootScope.go = function (path) {
            $location.path(path); return false;
        };
        $rootScope.validationOptions = {
            rules: {
                Title: {
                    required: true,
                    maxlength: 255
                },
                Description: {
                    maxlength: 255
                },
                Alias: {
                    maxlength: 50
                },
                StringValue: {
                    maxlength: 255
                }
            },
            messages: {
                Title: {
                    required: "Yêu cầu nhập tên thiết đặt.",
                    maxlength: "Tên thiết đặt không được quá 255 ký tự."
                },
                Description: {
                    maxlength: "Mô tả không được quá 255 ký tự."
                },
                Alias: {
                    maxlength: "Bí danh không được quá 50 ký tự."
                },
                StringValue: {
                    maxlength: "Giá trị kiểu chuỗi không được quá 255 ký tự."
                }
            }
        }
        $rootScope.StatusData = [{
            Value: 1,
            Name: 'Sử dụng'
        }, {
            Value: 0,
            Name: 'Không sử dụng'
        }];
        $rootScope.ConvertStatus = function (data) {
            if (data != null && data != undefined) {
                if (data == true) {
                    return 'Sử dụng';
                } else if (data == false) {
                    return 'Không sử dụng';
                } else {
                    return "";
                }
            }
        }
        $rootScope.ValueTypeData = [{
            Value: 0,
            Name: 'Số nguyên'
        }, {
            Value: 1,
            Name: 'Số thực (Decimal)'
        }, {
            Value: 2,
            Name: 'Ngày tháng'
        }, {
            Value: 3,
            Name: 'Chuỗi'
        }, {
            Value: 4,
            Name: 'Giá trị Max- Min'
        }, {
            Value: 5,
            Name: 'Chọn nhiều (Checkbox)'
        }, {
            Value: 6,
            Name: 'Chọn một (Optional)'
        }, {
            Value: 7,
            Name: 'Ngày bắt đầu- Ngày Kết thúc'
            }, {
                Value: 8,
                Name: 'Danh mục'
            }

        ];
        $rootScope.ConvertValueType = function (data) {
            if (data != null && data != undefined) {
                if (data == 0) {
                    return 'Số nguyên';
                } else if (data == 1) {
                    return 'Số thực (Decimal)';
                } else if (data == 2) {
                    return 'Ngày tháng';
                } else if (data == 3) {
                    return 'Chuỗi';
                } else if (data == 4) {
                    return 'Giá trị Max- Min';
                } else if (data == 5) {
                    return 'Chọn nhiều (Checkbox)';
                } else if (data == 6) {
                    return 'Chọn một (Optional)';
                } else if (data == 7) {
                    return 'Ngày bắt đầu- Ngày Kết thúc';
                } else if (data == 8) {
                    return 'Danh mục';
                } else {
                    return '';
                }
            }
        }
        $rootScope.ConvertTodayDate = function (date) {
            if (date !== null && date != "") {
                var newdate = new Date(date);
                var month = newdate.getMonth() + 1;
                var day = newdate.getDate();
                var year = newdate.getFullYear();
                var hh = newdate.getHours();
                var mm = newdate.getMinutes();
                if (month < 10)
                    month = "0" + month;
                if (day < 10)
                    day = "0" + day;
                if (mm < 10)
                    mm = "0" + mm;
                return todayDate = day + "/" + month + "/" + year;
            } else {
                return null;
            }
        }
        $rootScope.ConvertDateView = function (date) {
            if (date !== null && date != "") {
                var newdate = new Date(date);
                var month = newdate.getMonth() + 1;
                var day = newdate.getDate();
                var year = newdate.getFullYear();
                var hh = newdate.getHours();
                var mm = newdate.getMinutes();
                if (month < 10)
                    month = "0" + month;
                if (day < 10)
                    day = "0" + day;
                if (mm < 10)
                    mm = "0" + mm;
                return todayDate = day + "/" + month + "/" + year + ' ' + hh + ':' + mm;
            } else {
                return null;
            }
        }
        $rootScope.convertDate = function (datetime) {
            if (datetime !== null && datetime !== undefined && datetime != "") {
                var value = datetime.split(' ');
                return value[0];
            } else {
                return "";
            }
        }
        $rootScope.convertToDate = function (datetime) {
            if (datetime !== null && datetime != "") {
                var Str = datetime.toString();
                if (Str.indexOf("/Date") >= 0) {

                    var newdate = new Date(parseInt(datetime.substr(6)));
                    var month = newdate.getMonth() + 1;
                    var day = newdate.getDate();
                    var year = newdate.getFullYear();
                    var hh = newdate.getHours();
                    var mm = newdate.getMinutes();
                    if (month < 10)
                        month = "0" + month;
                    if (day < 10)
                        day = "0" + day;
                    return year + "-" + month + "-" + day;
                } else {
                    var value = datetime.split('/');

                    return value[2] + '-' + value[1] + '-' + value[0];
                }
            }
            return null;
        };
        $rootScope.ConvertNumber = function (number) {
            if (number != null && number != "" && number != undefined) {
                var data = number.split('.0000');
                if (data != undefined) {
                    var f = data[0];
                    return f.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                } else {
                    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                }

            }
        }
        //các hàm chuyển đổi định dạng /Date(785548800000)/
        $rootScope.convertToJSONDate = function (strDate) {
            if (strDate !== null && strDate != "") {
                var Str = strDate.toString();
                if (Str.indexOf("/Date") >= 0) {

                    return Str;
                } else {
                    var newDate = new Date(strDate);
                    return '/Date(' + newDate.getTime() + ')/';
                }
            }

        }

    });
    app.config(function ($routeProvider, $validatorProvider) {
        $routeProvider
            .when('/', {
                templateUrl: ctxfolder + '/index.html',
                controller: 'index'
            })
            .when('/edit/:id', {
                templateUrl: ctxfolder + '/edit.html',
                controller: 'edit'
            })
            .when('/add/', {
                templateUrl: ctxfolder + '/add.html',
                controller: 'add'
            })
        $validatorProvider.setDefaults({
            errorElement: 'span',
            errorClass: 'help-block',
            highlight: function (element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
            }
        });
    });
    app.controller('index', function ($scope, $rootScope, $compile, $uibModal, DTOptionsBuilder, DTColumnBuilder, DTInstances, dataservice) {
        $scope.model = {};
        $rootScope.ID = 0;
        $rootScope.reload = function () {
            $rootScope.ID = 0;
            $scope.model.Id = undefined;
            $scope.loadData();
            $.ajax({
                type: 'post',
                url: '/Settings/gettreedata/',
                data: { id: 0 },
                success: function (result) {
                    $rootScope.treeData = result;
                    $scope.$apply();
                }
            })
        }
        $scope.loadData = function () {
            $.ajax({
                type: 'post',
                url: '/Settings/getAll/',
                data: { id: $rootScope.ID },
                success: function (result) {
                    $scope.tree_data = buildJtree(result);
                    $.ajax({
                        type: 'post',
                        url: '/Settings/gettreedata/',
                        data: { id: 0 },
                        success: function (rs) {
                            $rootScope.treeData = rs;
                            $scope.$apply();
                        }
                    })
                    $scope.$apply();
                }
            })
            $.ajax({
                type: 'post',
                url: '/Settings/gettreedata/',
                data: { id: 0 },
                success: function (result) {
                    $rootScope.treeData = result;
                    $scope.$apply();
                }
            })

        }
        $scope.loadData();

        $scope.expanding_property = {
            field: "title",
            width: "200px",
            displayName: "Tên thiết đặt",
            sortable: true,
            filterable: true,
        };
        $scope.tree_data = [];
        $scope.my_tree = tree = {};
        var data = [];
        data.push('<a  style="margin-left:5px!important" class="btn btn-icon-only btn-default btn-custom" title="Xem" ng-click="cellTemplateScope.open(row.branch)"><i class="fa fa-eye"></i ></a >');
        data.push('<a  style="margin-left:5px!important" class="btn btn-icon-only btn-default btn-custom" title="Sửa" ng-click="cellTemplateScope.edit(row.branch)"><i class="fa fa-edit"></i ></a >');
        data.push('<a  style="margin-left:5px!important" class="btn btn-icon-only btn-default btn-custom" title="Sắp xếp" ng-click="cellTemplateScope.sort(row.branch)"><i class="fa fa-list"></i ></a >');
        //data.push('<a  style="margin-left:5px!important" class="btn btn-icon-only btn-default btn-custom" title="Xóa" ng-click="cellTemplateScope.delete(row.branch)"><i class="fa fa-trash-o"></i ></a >');
        var contextMenu = [];
        contextMenu.push("<li ><a  ng-click='cellTemplateScope.open(row.branch)' title='Xem thiết đặt'><i class='fa fa-eye'></i> Xem thiết đặt</a></li>");
        contextMenu.push("<li ><a  ng-click='cellTemplateScope.add(row.branch)' title='Thêm thiết đặt'><i class='fa fa-plus'></i> Thêm thiết đặt</a></li>");
        contextMenu.push("<li ><a  ng-click='cellTemplateScope.edit(row.branch)' title='Sửa thiết đặt'><i class='fa fa-edit'></i> Sửa thiết đặt</a></li>");
        contextMenu.push("<li ><a  ng-click='cellTemplateScope.sort(row.branch)' title='Sắp xếp thiết đặt'><i class='fa fa-list'></i> Sắp xếp thiết đặt</a></li>");
        contextMenu.push("<li  ><a  ng-click='cellTemplateScope.delete(row.branch)' title='xóa thiết đặt'><i class='fa fa-remove'></i> Xóa thiết đặt</a></li>");
        $scope.col_defs = [
            {
                field: "",
                sortable: false, width: '45px',
                displayName: "",
                colclass: "action",
                cellTemplate: "<div class='btn-group  '>"
                    + "<a   class='btn btn-icon-xs btn-icon-only   ' data-toggle='dropdown'  ><i class='fa   fa-list-ul'></i></a>"
                    + "<ul class='dropdown-menu  pull-right' >"
                    + contextMenu.join("")
                    + "</ul>"
                    + "</div>",
                cellTemplateScope: {
                    delete: function (data) {
                        $scope.delete(data);
                    },
                    edit: function (data) {
                        $scope.edit(data);
                    },
                    add: function (data) {
                        $scope.add(data.Id);
                    },
                    open: function (data) {
                        $scope.open(data);
                    },
                    sort: function (data) {
                        $scope.sort(data.Id);
                    },
                }
            },
            {
                field: "valueType", displayName: "Kiểu giá trị", width: "170px",
                sortable: true,
                cellTemplate: "<span> {{row.branch[col.field] | ValueType}} </span>"
            },
            {
                field: "description", displayName: "Mô tả", width: "220px",
                sortable: true,
                cellTemplate: "<span>{{row.branch['Description']}}</span>"
            }, {
                field: "modifiedDate", displayName: "Ngày cập nhật", width: "140px",
                sortable: true,
                cellTemplate: "<span> {{row.branch[col.field] | fdate:'DD/MM/YYYY HH:mm'}} </span>",
            },
            {
                field: "isActive", displayName: "Trạng thái", width: "120px",
                sortable: true,
                cellTemplate: "<span> {{row.branch[col.field] | fstatus}} </span>",
            },
            {
                field: "", displayName: "Điều khiển", width: "130px",
                sortable: true,
                cellTemplate: data.join(""),
                cellTemplateScope: {
                    delete: function (data) {
                        $scope.delete(data);
                    },
                    edit: function (data) {
                        $scope.edit(data);
                    },
                    add: function (data) {
                        $scope.add(data.id);
                    },
                    sort: function (data) {
                        $scope.sort(data.id);
                    },
                    open: function (data) {
                        $scope.open(data);
                    }
                }
            }
        ];

        $scope.my_tree_handler = function (branch) {
            $rootScope.selectedItem = branch;
        }

        function buildJtree(data) {
            var tree = [];
            if (!data || data.length === 0) return [];
            $.each(data, function (index, item) {
                if (!item.parentId) {
                    var treeObjs = item;
                    var sub = subJtree(data, item.id);
                    if (sub.length > 0) {
                        treeObjs.children = sub;
                    }
                    treeObjs.expanded = false;
                    tree.push(treeObjs);
                }
            });
            return tree;
        }
        function subJtree(data, parentVal) {
            var subTree = [];
            $.each(data, function (index, item) {
                if (item.parentId && item.parentId === parentVal) {
                    var treeObjs = item;
                    var sub = subJtree(data, item.id);
                    if (sub.length > 0) {
                        treeObjs.children = sub;
                    }
                    treeObjs.expanded = false;
                    subTree.push(treeObjs);
                }
            });
            return subTree;
        }


        $scope.add = function (p) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: ctxfolder + '/add.html',
                controller: 'add',
                backdrop: true,
                size: '80',
                resolve: {
                    para: function () {
                        return p;
                    }
                }
            });
            modalInstance.result.then(function (d) {
                if ($rootScope.ID != 0 && $rootScope.ID != undefined) {
                    $rootScope.ParentID();
                } else {
                    $rootScope.reload()
                }
            }, function () {
            });
        }
        $scope.open = function (p) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: ctxfolder + '/open.html',
                controller: 'open',
                backdrop: true,
                size: '80',
                resolve: {
                    para: function () {
                        return p.id;
                    }
                }
            });
            modalInstance.result.then(function (d) {
                if ($rootScope.ID != 0 && $rootScope.ID != undefined) {
                    $rootScope.ParentID();
                } else {
                    $rootScope.reload()
                }
            }, function () {
            });
        }
        //#endregion
        $scope.delete = function (temp) {
            $confirm({ text: 'Bạn có chắc chắn muốn xóa [' + temp.title + ']?', title: 'Xác nhận', ok: 'Đồng ý', cancel: ' Hủy ' })
                .then(function () {
                    dataservice.delete(temp.id, function (rs) {
                        if (rs.Error) {
                            App.notifyDanger(rs.Title);
                        } else {
                            App.notifyInfo(rs.Title);
                            temp.IsDeleted = true;
                            dataservice.UpdateIsActive(temp, function (rs) {
                                if (rs.Error) {
                                    App.notifyDanger(rs.Title);
                                    return;
                                } else {
                                    if ($rootScope.ID != 0 && $rootScope.ID != undefined) {
                                        $rootScope.ParentID();
                                    } else {
                                        $rootScope.reload()
                                    }
                                    App.unblockUI("#contentMain");
                                }
                            })

                        }
                        App.unblockUI("#contentMain");
                    });
                });
        }


        $scope.edit = function (temp) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: ctxfolder + '/edit.html',
                controller: 'edit',
                backdrop: true,
                size: '80',
                resolve: {
                    para: function () {
                        return temp.id;
                    }
                }
            });
            modalInstance.result.then(function (d) {
                if ($rootScope.ID != 0 && $rootScope.ID != undefined) {
                    $rootScope.ParentID();
                } else {
                    $rootScope.reload()
                }
            }, function () {
            });
        }
        $scope.sort = function (p) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: ctxfolder + '/sort.html',
                controller: 'sort',
                backdrop: true,
                size: '60',
                resolve: {
                    para: function () {
                        return p;
                    }
                }
            });
            modalInstance.result.then(function (d) {
                if ($rootScope.ID != 0 && $rootScope.ID != undefined) {
                    $rootScope.ParentID();
                } else {
                    $rootScope.reload()
                }
            }, function () {
            });
        }
        $scope.model = {};
        $rootScope.ParentID = function () {
            $rootScope.ID = $scope.model.Id;
            dataservice.getAll($scope.model.Id, function (rs) {
                if (rs.Error) {
                    App.notifyDanger(rs.Title);
                } else {
                    $scope.tree_data = buildJtree(rs);
                }
            });
            $scope.model.Id = undefined;
            dataservice.gettreedata(0, function (result) {
                $rootScope.treeData = result;
                $scope.model.Id = $rootScope.ID;
            });
        }
    });
    app.controller('open', function ($scope, $http, $rootScope, $compile, $uibModal, $uibModalInstance, dataservice, para) {
        $scope.Title = "Xem thông tin thiết đặt"
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        }
        $scope.initData = function () {
            $.ajax({
                type: 'post',
                url: '/Settings/getitem/',
                data: { id: para },
                success: function (rss) {
                    var rs = JSON.parse(rss);
                    if (rs.Error) {
                        toastr["error"](rs.Title);
                    } else {
                        $scope.model = rs;
                        $.ajax({
                            type: 'post',
                            url: '/Settings/gettreedata/',
                            data: { id: rs.Id },
                            success: function (result) {
                                $rootScope.treeData = result;
                                $scope.$apply();
                            }
                        })
                        if (rs.IsCategory == false) {
                            $scope.model.IsCategoryFalse = true;
                        } else {
                            $scope.model.IsCategoryTrue = true;
                        }
                        if (rs.StartDate != null) {
                            $scope.model.StartDate = $rootScope.convertToJSONDate(rs.StartDate);
                        }
                        if (rs.EndDate != null) {
                            $scope.model.EndDate = $rootScope.convertToJSONDate(rs.EndDate);
                        }
                        if (rs.DateValue != null) {
                            $scope.model.DateValue = $rootScope.convertToJSONDate(rs.DateValue);
                        }
                        if (rs.IsActive)
                            $scope.model.IsActive = 1;
                        else
                            $scope.model.IsActive = 0;
                        if (rs.ParentId != null) {
                            $.ajax({
                                type: 'post',
                                url: '/Settings/getitem/',
                                data: { id: rs.ParentId },
                                success: function (rs) {
                                    if (rs.Error) {
                                        App.notifyDanger(rs.Title);
                                    } else {
                                        $scope.model.ParentIdName = rs.Title;
                                    }
                                }
                            })
                        }
                    }
                }
            })
        }
        $scope.initData();

    });
    app.controller('add', function ($scope, $http, $rootScope, $compile, $uibModal, $uibModalInstance, dataservice, para) {
        $scope.Title = "Thêm thiết đặt";
        $scope.model = {};
        $scope.showalias = false;

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        }
        $scope.model.ParentId = para;
        if ($rootScope.ID != 0 && para == null) {
            $scope.model.ParentId = $rootScope.ID;
        }
        $scope.loadData = function () {
            $.ajax({
                type: 'post',
                url: '/Settings/gettreedata/',
                data: { id: 0 },
                success: function (result) {
                    $rootScope.treeData = result;
                    $scope.$apply();
                }
            })
        }
        $scope.loadData();
        $scope.CheckAlias = function () {
            if ($scope.model.ParentId != null && $scope.model.ParentId != undefined && $scope.model.ParentId != '') {
                if ($scope.model.ValueType != 8) {
                    $.ajax({
                        type: 'post',
                        url: '/Settings/GetAlias',
                        data: { id: $scope.model.ParentId },
                        success: function (rss) {
                            var rs = JSON.parse(rss);
                            $scope.model.Alias = rs.Alias;
                            $scope.showalias = true;
                            $scope.$apply();
                        }
                    })
                }
                else {
                    $scope.model.Alias = "";
                    $scope.showalias = true;
                }
            }
        }
        $scope.CheckAlias1 = function () {
            $.ajax({
                type: 'post',
                url: '/Settings/GetAlias',
                data: { id: $scope.model.ParentId },
                success: function (rss) {
                    var rs = JSON.parse(rss);
                    if ($scope.model.ParentId != null && $scope.model.ParentId != undefined && $scope.model.ParentId != '') {
                        if ($scope.model.ValueType != 8 && rs.ValueType == 8) {
                            $.ajax({
                                type: 'post',
                                url: '/Settings/GetAlias',
                                data: { id: $scope.model.ParentId },
                                success: function (rss) {
                                    var rs = JSON.parse(rss);
                                    $scope.model.Alias = rs.Alias;
                                    $scope.showalias = true;
                                    $scope.$apply();
                                }
                            })
                        }
                        else {
                            $scope.model.Alias = "";
                            $scope.showalias = false;
                        }
                    }
                    $scope.$apply()
                }
            })
           
        }
        $scope.Click = function () {
            if ($scope.model.IsCategoryFalse == true) {
                $scope.model.IsCategoryTrue = false
            }
        }

        $scope.Clicks = function () {
            if ($scope.model.IsCategoryTrue == true) {
                $scope.model.IsCategoryFalse = false;
            }
        }
        $scope.submit = function () {
            if ($scope.addform.validate()) {
                $scope.model.IsDeleted = false;
                if ($scope.model.IsCategoryFalse == true) {
                    $scope.model.IsCategory = false;
                } else {
                    $scope.model.IsCategory = true;
                }
                if ($scope.model.StartDate != null) {
                    $scope.model.StartDate = moment($scope.model.StartDate).format("YYYY-MM-DD");
                }
                if ($scope.model.EndDate != null) {
                    $scope.model.EndDate = moment($scope.model.EndDate).format("YYYY-MM-DD");
                }
                if ($scope.model.DateValue != null) {
                    $scope.model.DateValue = moment($scope.model.DateValue).format("YYYY-MM-DD");
                }
                $http.post('/Settings/insert', $scope.model)
                    .then(
                        function (rs) {
                            if (rs.Error) {
                                toastr["error"](rs.Title);
                            } else {
                                toastr["success"](rs.Title);
                                $uibModalInstance.close();
                            }
                        },
                        function (rs) {
                            toastr["error"]("Có lỗi xảy ra!");
                        }
                    );
            }
        }
    });
    app.controller('edit', function ($scope, $rootScope, $http, $compile, $uibModal, $uibModalInstance, dataservice, para) {
        $scope.Title = "Cập nhật thiết đặt";
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        }
        $scope.Status = false;
        $scope.IsActive = function () {
            $scope.Status = false;
            if ($scope.model.IsActive == 1) {
                dataservice.CheckStatus($scope.model.ParentId, function (rs) {
                    if (rs.length > 0) {
                        $scope.Status = true;
                        App.notifyDanger("Bạn không thể cập nhật trạng thái khi cấp trên không còn sử dụng");
                    }
                })
            }

        }
        $scope.Click = function () {
            if ($scope.model.IsCategoryFalse == true) {
                $scope.model.IsCategoryTrue = false
            }
        }
        $scope.Clicks = function () {
            if ($scope.model.IsCategoryTrue == true) {
                $scope.model.IsCategoryFalse = false;
            }
        }
        $scope.initData = function () {
            $.ajax({
                type: 'post',
                url: '/Settings/getitem/',
                data: { id: para },
                success: function (rss) {
                    var rs = JSON.parse(rss);
                    if (rs.Error) {
                        toastr["error"](rs.Title);
                    } else {
                        $scope.model = rs;
                        $.ajax({
                            type: 'post',
                            url: '/Settings/gettreedata/',
                            data: { id: rs.Id },
                            success: function (result) {
                                $rootScope.treeData = result;
                                $scope.$apply();
                            }
                        })
                        if (rs.IsCategory == false) {
                            $scope.model.IsCategoryFalse = true;
                        } else {
                            $scope.model.IsCategoryTrue = true;
                        }
                        if (rs.StartDate != null) {
                            $scope.model.StartDate = $rootScope.convertToJSONDate(rs.StartDate);
                        }
                        if (rs.EndDate != null) {
                            $scope.model.EndDate = $rootScope.convertToJSONDate(rs.EndDate);
                        }
                        if (rs.DateValue != null) {
                            $scope.model.DateValue = $rootScope.convertToJSONDate(rs.DateValue);
                        }
                        if (rs.IsActive) 
                            $scope.model.IsActive = 1;
                        else
                            $scope.model.IsActive = 0;
                        if (rs.ParentId != null) {
                            $.ajax({
                                type: 'post',
                                url: '/Settings/getitem/',
                                data: { id: rs.ParentId },
                                success: function (rs) {
                                    if (rs.Error) {
                                        App.notifyDanger(rs.Title);
                                    } else {
                                        $scope.model.ParentIdName = rs.Title;
                                    }
                                }
                            })
                        }
                    }
                }
            })
        }
        $scope.initData();
        $scope.submit = function () {
            if ($scope.editform.validate()) {
                if ($scope.model.IsCategoryFalse == true) {
                    $scope.model.IsCategory = false;
                } else {
                    $scope.model.IsCategory = true;
                }
                if ($scope.model.StartDate != null) {
                    $scope.model.StartDate = moment($scope.model.StartDate).format("YYYY-MM-DD");
                }
                if ($scope.model.EndDate != null) {
                    $scope.model.EndDate = moment($scope.model.EndDate).format("YYYY-MM-DD");
                }
                if ($scope.model.DateValue != null) {
                    $scope.model.DateValue = moment($scope.model.DateValue).format("YYYY-MM-DD");
                }
                $http.post('/Settings/update', $scope.model)
                    .then(
                        function (rs) {
                            if (rs.error) {
                                toastr["error"](rs.title);
                            } else {
                                $http.post('/Settings/UpdateIsActive', $scope.model)
                                    .then(
                                        function (rs) {
                                            if (rs.Error) {
                                                toastr["error"](rs.title);
                                            } else {
                                                toastr["success"](rs.title);
                                                $uibModalInstance.close();
                                            }
                                        },
                                        function (rs) {
                                            toastr["error"]("Có lỗi xảy ra!");
                                        }
                                    )
                                        
                            }
                        },
                        function (rs) {
                            toastr["error"]("Có lỗi xảy ra!");
                        }
                    );
            }
        }

    });
    app.controller('sort', function ($scope, $http, $rootScope, $compile, $uibModal, $uibModalInstance, dataservice, para) {

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        }

        $scope.initData = function () {
            $.ajax({
                type: 'post',
                url: '/Settings/getbyparent/',
                data: { id: para},
                success: function (rss) {
                    var rs = JSON.parse(rss);
                    $scope.model = rs;
                    $scope.$apply();
                }
            })
        }
        $scope.initData();
        $scope.resort = function (item, index) {
            $scope.model.splice(index, 1);
            $scope.model.splice(item.OrderId - 1, 0, item);
            $.each($scope.model, function (index, item) {
                item.OrderId = index + 1;
            });
        }
        $scope.submit = function () {
            $http.post('/Settings/resort', $scope.model)
                .then(
                    function (rs) {
                        if (rs.error) {
                            toastr["error"](rs.title);
                        } else {
                            $http.post('/Settings/UpdateIsActive', $scope.model)
                                .then(
                                    function (rs) {
                                        if (rs.Error) {
                                            toastr["error"](rs.title);
                                        } else {
                                            toastr["success"](rs.title);
                                            $uibModalInstance.close();
                                        }
                                    },
                                    function (rs) {
                                        toastr["error"]("Có lỗi xảy ra!");
                                    }
                                )

                        }
                    },
                    function (rs) {
                        toastr["error"]("Có lỗi xảy ra!");
                    }
                );
        }
    });
})(angular.module('e-app'));

