﻿///*********************
/// Using in angularjs 
/// Have function:
/// + using jexcel with namespace ngJexcel
/// + using sumoselect with namespace ngSmselect
/// + using option date with namespace ngOptionDate
/// You can find any support 'ng-aimodu' when search keyword 'ng-aimodu' on google.com.vn
///*********************
var aimodu = angular.module('ng-aimodu', []);
aimodu.directive('ngOnlyNumber', ['$filter', function ($filter) {
    return {
        restrict: 'EAC',
        scope: {
            'ngData': '=ngData',
            'ngModel': '=ngModel',
            'ngCall': '=ngCall',
        },
        template: '',
        link: function (scope, elem, attrs, ctrl) {
            var _el = elem;
            var _pendding = false;
            var _first = true;
            $(elem).keydown(function (d) {
                AllowOnlyNumbers(d, false, this);
            });
            $(elem).on('keyup', function (d) {
                _pendding = true;
                changeInputMoney(d, this, true);
                scope.ngData = (this.value.replaceAll('[.]', '')).replaceAll('[,]', '.');
                if (scope.ngCall !== undefined)
                    scope.ngCall();
                if (scope.$root.$$phase !== '$apply' && scope.$root.$$phase !== '$digest') {
                    scope.$apply();
                }
            });
            $(elem).on('keypress', function () {
                $(elem).removeAttr('dir');
                if (scope.ngCall !== undefined)
                    scope.ngCall('leave');
            });
            $(elem).on('focusout', function () {
                $(elem).attr('dir', "rtl");
                scope.ngData = (this.value.replaceAll('[.]', '')).replaceAll('[,]', '.');
                if (scope.$root.$$phase !== '$apply' && scope.$root.$$phase !== '$digest') {
                    scope.$apply();
                }
            });
            $(elem).on('click', function () {
                $(elem).attr('dir', "rtl");
            });
            scope.$watch('ngData', function (n, ol) {
                if (_first) {
                    _first = undefined;
                } else {
                    if (scope.ngCall !== undefined)
                        scope.ngCall();
                    if (scope.$root.$$phase !== '$apply' && scope.$root.$$phase !== '$digest') {
                        scope.$apply();
                    }
                }
                if (_pendding) {
                    _pendding = false;
                    return '';
                }
                _el[0].value = convertMoney(scope.ngData);
                if (scope.$root.$$phase !== '$apply' && scope.$root.$$phase !== '$digest') {
                    scope.$apply();
                }
            }, true)
        }
    };
}]);
aimodu.factory('keyhandle', function () {
    var _e = [];
    var _c = [];
    var _d = [];
    var _s = [];
    document.onkeydown = function (e) {
        e = e || window.event;
        var _keyCode = e.keyCode || e.which;
        if (_s[_keyCode] === 1) {
            switch (_keyCode) {
                case _c[_keyCode]:
                    typeof _e[_keyCode] === 'function' ? _e[_keyCode](_d[_keyCode]) : '';
                    break;
            }
        }
        if (_s[_keyCode] === 2) {
            if (e.ctrlKey) {
                switch (_keyCode) {
                    case _c[_keyCode]:
                        typeof _e[_keyCode] === 'function' ? _e[_keyCode](_d[_keyCode]) : '';
                        break;
                }
            }
        }
    };
    return {
        event: function (k, e, d) {
            _c[k] = k;
            _e[k] = e;
            _d[k] = d;
            _s[k] = 1;
        },
        eventCtrl: function (k, e, d) {
            _c[k] = k;
            _e[k] = e;
            _d[k] = d;
            _s[k] = 2;
        }
    }
});
aimodu.directive('ngJexcel', ['$filter', function ($filter) {
    return {
        restrict: 'EAC',
        scope: {
            'jxinit': '=jxInit',
            'jexcelChange': '&jexcelChange',
            'rowchange': '&rowchange',
            'ngModel': '=',
            'deleterowhotkey': '&deleterowhotkey',
            'deletees': '&deletees',
        },
        link: function (scope, elem, attrs) {
            $(elem).addClass('spreadsheet__table table ai-jexcel');
            scope.ngModel = elem;
            var first = false;
            scope.$watch('jxinit.data', function (rs) {
                if (first) {
                    $(scope.ngModel).jexcel('setData', rs, false);
                    //scope.ngModel = $(_elem);
                } else {
                    first = !first;
                };
                return true;
            });
            var _i = {};
            if (scope.jxinit !== undefined) {
                for (var key in scope.jxinit) {
                    if (key === 'reloadData') continue;
                    _i[key] = scope.jxinit[key];
                }
            }
            scope.rowchangeRoot = function (row, status) {
                scope.rowchange({ data: { row, status } });
                if (scope.$root.$$phase !== '$apply' && scope.$root.$$phase !== '$digest') {
                    scope.$apply();
                }
            }
            scope.jexcelChange !== undefined ? _i.onchange = scope.rowchangeRoot : '';
            scope.jexcelChange !== undefined ? _i.deleterowconfirm = scope.deletees() : '';
            scope.jexcelChange !== undefined ? _i.deleterowhotkey = scope.deleterowhotkey() : '';
            _i.rowchange = scope.rowchangeRoot;

            $(elem).jexcel(_i);
            $(elem).jexcel('updateSettings', {
                table: function (instance, cell, col, row, val, id, h, c) {
                    // Number formating
                    if (h[col] === 'numeric') {
                        // using in next time variable 'c': c.formatNumber==='VN-MONEY'
                        $(cell).html(convertMoney($(cell).text()));
                    }
                }
            });
            scope.ngModel !== undefined ? scope.ngModel = $(elem) : '';
            if (scope.$root.$$phase !== '$apply' && scope.$root.$$phase !== '$digest') {
                scope.$apply();
            }
            if (scope.jxinit.reloadData !== undefined) {
                angular.extend(scope.jxinit.reloadData, {
                    reloadJexcel: function () {
                        $(scope.ngModel).jexcel('setData', scope.jxinit.data, false);
                    }
                });
            }
        }
    };
}]);
aimodu.directive('ngSmselect', ['$filter', function ($filter) {
    return {
        restrict: 'EAC',
        scope: {
            'data': '=seInit',
            'seChange': '&',
            'ngModel': '=',
            'seData': '=',
            'placeholder': '@',
        },
        require: 'ngModel',
        template: '',
        link: function (scope, elem, attrs, ctrl) {
            scope._el = elem;
            scope.$watch('seData', function (n, ol) {
                scope._el[0].sumo.reloadData(n);
                if (scope.$root.$$phase !== '$apply' && scope.$root.$$phase !== '$digest') {
                    scope.$apply();
                }
            }, true)
            scope.$watch('ngModel', function (rs) {
                if (rs !== undefined && rs !== null && rs !== '') {
                    var _i = Array.isArray(rs) ? rs : [rs];
                    scope._el[0].sumo.reloadData(scope.seData, _i);
                    if (scope.$root.$$phase !== '$apply' && scope.$root.$$phase !== '$digest') {
                        scope.$apply();
                    }
                }
            }, true)
            scope.is_multi = scope._el.attr("multiple");
            scope.changedata = function (d1) {
                scope.ngModel !== undefined ? ctrl.$setViewValue(d1.length > 0 ? d1 : scope.ngModel) : '';
                if (scope.$root.$$phase !== '$apply' && scope.$root.$$phase !== '$digest') {
                    scope.$apply();
                }
                if (scope.data !== undefined && scope.data.change !== undefined) {
                    scope.data.change(d1);
                } else {
                    scope.seChange({ data: d1 });
                }
                if (scope.$root.$$phase !== '$apply' && scope.$root.$$phase !== '$digest') {
                    scope.$apply();
                }
            }
            var _i = {};
            scope.placeholder !== undefined ? _i.placeholder = scope.placeholder : '';
            scope.seData === undefined ? _i.data = [] : _i.data = scope.seData;
            if (scope.data !== undefined) {
                for (var key in scope.data) {
                    if (key === 'reload') {
                        continue;
                    }
                    _i[key] = scope.data[key];
                }
            }
            if (scope.data !== undefined && scope.data.reload !== undefined) {
                angular.extend(scope.data.reload, {
                    reloadDefault: function () {
                        scope._el[0].sumo.reloadData(scope.seData, []);
                    }
                });
            }
            initSmallSelect(elem[0], _i, scope.changedata);
        }
    };
}]);
aimodu.directive('ngOptionDate', [function ($filter) {
    return {
        restrict: 'EAC',
        scope: {
            'optionChange': '&',
            'placeholder': '@',
            'sPoint': '@',
        },
        template: '',
        link: function (scope, elem, attrs, ctrl) {
            Date.prototype.getWeekNumber = function () {
                var d = new Date(Date.UTC(this.getFullYear(), this.getMonth(), this.getDate()));
                var dayNum = d.getUTCDay() || 7;
                d.setUTCDate(d.getUTCDate() + 4 - dayNum);
                var yearStart = new Date(Date.UTC(d.getUTCFullYear(), 0, 1));
                return Math.ceil((((d - yearStart) / 86400000) + 1) / 7)
            };
            Date.prototype.getWeek = function () {
                var onejan = new Date(this.getFullYear(), 0, 1);
                return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
            }
            function getWeekNumber(d) {
                d = new Date(+d);
                d.setHours(0, 0, 0);
                d.setDate(d.getDate() + 4 - (d.getDay() || 7));
                var yearStart = new Date(d.getFullYear(), 0, 1);
                var weekNo = Math.ceil((((d - yearStart) / 86400000) + 1) / 7)
                return [d.getFullYear(), weekNo];
            }
            function weeksInYear(year) {
                var month = 11, day = 31, week;
                do {
                    d = new Date(year, month, day--);
                    week = getWeekNumber(d)[1];
                } while (week === 1);
                return week;
            }
            scope.LoadWeekForMonth = function (data) {
                var date1 = new Date(scope.model.Year, data - 1, 1);
                var date2 = new Date(scope.model.Year, data, 0);
                var Start = date1.getWeekNumber();
                var End = date2.getWeekNumber();
                scope.liWeek = [];
                if (End < Start) {
                    for (var i = 0; i < scope.liWeekTemp.length; i++) {
                        if (scope.liWeekTemp[i].Value >= Start) {
                            scope.liWeek.push(scope.liWeekTemp[i]);
                        }
                    }
                }
                else {
                    for (var i = 0; i < scope.liWeekTemp.length; i++) {
                        if (scope.liWeekTemp[i].Value >= Start && scope.liWeekTemp[i].Value <= End) {
                            scope.liWeek.push(scope.liWeekTemp[i]);
                        }
                    }
                }
            }
            scope.LoadMonthForQuarter = function (data) {
                if (data === undefined || data === null) {
                    scope.liMonth = [
                        {
                            'Title': 1,
                            'Value': 1
                        },
                        {
                            'Title': 2,
                            'Value': 2
                        },
                        {
                            'Title': 3,
                            'Value': 3
                        },
                        {
                            'Title': 4,
                            'Value': 4
                        },
                        {
                            'Title': 5,
                            'Value': 5
                        },
                        {
                            'Title': 6,
                            'Value': 6
                        },
                        {
                            'Title': 7,
                            'Value': 7
                        },
                        {
                            'Title': 8,
                            'Value': 8
                        },
                        {
                            'Title': 9,
                            'Value': 9
                        },
                        {
                            'Title': 10,
                            'Value': 10
                        },
                        {
                            'Title': 11,
                            'Value': 11
                        },
                        {
                            'Title': 12,
                            'Value': 12
                        },];
                    scope.model.Week = undefined;
                    return;
                }
                else
                    if (data === 1) {
                        scope.liMonth = [
                            {
                                'Title': 1,
                                'Value': 1
                            },
                            {
                                'Title': 2,
                                'Value': 2
                            },
                            {
                                'Title': 3,
                                'Value': 3
                            },
                        ]
                    } else
                        if (data === 2) {
                            scope.liMonth = [
                                {
                                    'Title': 4,
                                    'Value': 4
                                },
                                {
                                    'Title': 5,
                                    'Value': 5
                                },
                                {
                                    'Title': 6,
                                    'Value': 6
                                },
                            ]
                        } else
                            if (data === 3) {
                                scope.liMonth = [
                                    {
                                        'Title': 7,
                                        'Value': 7
                                    },
                                    {
                                        'Title': 8,
                                        'Value': 8
                                    },
                                    {
                                        'Title': 9,
                                        'Value': 9
                                    },
                                ]
                            } else {
                                if (data === 4) {
                                    scope.liMonth = [
                                        {
                                            'Title': 10,
                                            'Value': 10
                                        },
                                        {
                                            'Title': 11,
                                            'Value': 11
                                        },
                                        {
                                            'Title': 12,
                                            'Value': 12
                                        },
                                    ]
                                }
                            }
                scope.model.month = undefined;
                scope.model.Week = undefined;
            }
            scope.initData = function () {
                scope.liWeek = [];
                scope.liWeekTemp = [];
                scope.CheckAll = true;
                scope.liMonth = [
                    {
                        'Title': 1,
                        'Value': 1
                    },
                    {
                        'Title': 2,
                        'Value': 2
                    },
                    {
                        'Title': 3,
                        'Value': 3
                    },
                    {
                        'Title': 4,
                        'Value': 4
                    },
                    {
                        'Title': 5,
                        'Value': 5
                    },
                    {
                        'Title': 6,
                        'Value': 6
                    },
                    {
                        'Title': 7,
                        'Value': 7
                    },
                    {
                        'Title': 8,
                        'Value': 8
                    },
                    {
                        'Title': 9,
                        'Value': 9
                    },
                    {
                        'Title': 10,
                        'Value': 10
                    },
                    {
                        'Title': 11,
                        'Value': 11
                    },
                    {
                        'Title': 12,
                        'Value': 12
                    },];
                scope.liQuarter = [
                    {
                        'Title': '1',
                        'Value': 1,
                    },
                    {
                        'Title': '2',
                        'Value': 2,
                    },
                    {
                        'Title': '3',
                        'Value': 3,
                    },
                    {
                        'Title': '4',
                        'Value': 4,
                    },
                ];
                scope.liYear = [];
                var _Date = new Date();
                var dayNames = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
                var count = weeksInYear(_Date.getFullYear());
                var _date = new Date(_Date.getFullYear(), 0, 1);
                if (_date.getWeekNumber() != 1) {
                    var _Date2 = new Date(_date.getFullYear(), 0, 1);
                    _Date2.setDate(_Date2.getDate() + 7);
                    var startDate = new Date(_Date2.getFullYear(), 0, 1);
                    var endDate = new Date(_Date2.getFullYear(), 0, 1);
                    if (_date.getDay() === 0) {
                        endDate.setDate(endDate.getDay() + 1);
                        startDate.setDate(startDate.getDate() + 1);
                    }
                    if (_date.getDay() === 1) {
                        endDate.setDate(endDate.getDay() + 7);
                        startDate.setDate(startDate.getDate() + 7);
                    }
                    if (_date.getDay() === 2) {
                        endDate.setDate(endDate.getDay() + 6);
                        startDate.setDate(startDate.getDate() + 6);
                    }
                    if (_date.getDay() === 3) {
                        endDate.setDate(endDate.getDay() + 5);
                        startDate.setDate(startDate.getDate() + 5);
                    }
                    if (_date.getDay() === 4) {
                        endDate.setDate(endDate.getDay() + 4);
                        startDate.setDate(startDate.getDate() + 4);
                    }
                    if (_date.getDay() === 5) {
                        endDate.setDate(endDate.getDay() + 3);
                        startDate.setDate(startDate.getDate() + 3);
                    }
                    if (_date.getDay() === 6) {
                        endDate.setDate(endDate.getDay() + 2);
                        startDate.setDate(startDate.getDate() + 2);
                    }
                    for (var i = 1; i <= count; i++) {
                        var item = {};
                        if (i === 1) {
                            item = {
                                'Title': i,
                                'Value': i,
                                'StartDate': ConDate(startDate, 3),
                                'EndDate': ConDate(endDate, 3),
                            };
                        } else {
                            var _xDate = new Date(startDate);
                            startDate.setDate(startDate.getDate() + 7);
                            endDate = new Date(_xDate).setDate(_xDate.getDate() + 7 + 6);
                            item = {
                                'Title': i,
                                'Value': i,
                                'StartDate': ConDate(startDate, 3),
                                'EndDate': ConDate(endDate, 3),
                            };
                        }
                        scope.liWeek.push(item);
                        scope.liWeekTemp.push(item);
                    }
                } else {
                    var startDate = new Date(_date.getFullYear(), 0, 1);
                    var endDate = new Date(_date.getFullYear(), 0, 1);
                    if (_date.getDay() === 0) {
                        endDate.setDate(endDate.getDay());
                    }
                    if (_date.getDay() === 1) {
                        endDate.setDate(endDate.getDay() + 6);
                    }
                    if (_date.getDay() === 2) {
                        endDate.setDate(endDate.getDay() + 5);
                    }
                    if (_date.getDay() === 3) {
                        endDate.setDate(endDate.getDay() + 4);
                    }
                    if (_date.getDay() === 4) {
                        endDate.setDate(endDate.getDay() + 3);
                    }
                    if (_date.getDay() === 5) {
                        endDate.setDate(endDate.getDay() + 2);
                    }
                    if (_date.getDay() === 6) {
                        endDate.setDate(endDate.getDay() + 1);
                    }
                    for (var i = 1; i <= count; i++) {
                        var item = {};
                        if (i === 1) {
                            item = {
                                'Title': i,
                                'Value': i,
                                'StartDate': ConDate(startDate, 3),
                                'EndDate': ConDate(endDate, 3),
                            };
                        } else
                            if (i === 2) {
                                if (_date.getDay() === 0) {
                                    startDate.setDate(startDate.getDate() + 1);
                                }
                                if (_date.getDay() === 1) {
                                    startDate.setDate(startDate.getDate() + 7);
                                }
                                if (_date.getDay() === 2) {
                                    startDate.setDate(startDate.getDate() + 6);
                                }
                                if (_date.getDay() === 3) {
                                    startDate.setDate(startDate.getDate() + 5);
                                }
                                if (_date.getDay() === 4) {
                                    startDate.setDate(startDate.getDate() + 4);
                                }
                                if (_date.getDay() === 5) {
                                    startDate.setDate(startDate.getDate() + 3);
                                }
                                if (_date.getDay() === 6) {
                                    startDate.setDate(startDate.getDate() + 2);
                                }
                                endDate.setDate(startDate.getDate() + 6);
                                item = {

                                    'Title': i,
                                    'Value': i,
                                    'StartDate': ConDate(startDate, 3),
                                    'EndDate': ConDate(endDate, 3),
                                };
                            }
                            else {
                                var _xDate = new Date(startDate);
                                startDate.setDate(startDate.getDate() + 7);
                                endDate = new Date(_xDate).setDate(_xDate.getDate() + 7 + 6);
                                item = {
                                    'Title': i,
                                    'Value': i,
                                    'StartDate': ConDate(startDate, 3),
                                    'EndDate': ConDate(endDate, 3),
                                };
                            }
                        scope.liWeek.push(item);
                        scope.liWeekTemp.push(item);
                    }
                }
                for (var i = 1; i <= 30; i++) {
                    var item = {
                        'Title': 2012 + i,
                        'Value': 2012 + i,
                    };
                    scope.liYear.push(item);
                }
            }
            scope.initData();
            scope._el = elem;
            scope.is_multi = scope._el.attr("multiple");
            function getThisQuarter(_tem) {
                var _i = (new Date()).getMonth() + 1;
                var _d = [];
                if (1 <= _i <= 3) {
                    if (_tem === 1) {
                        _d[0] = new Date((new Date()).getFullYear() - 1, 9, 1);
                        _d[1] = new Date((new Date()).getFullYear() - 1, 12, 0);
                    } else {
                        _d[0] = new Date((new Date()).getFullYear(), 0, 1);
                        _d[1] = new Date((new Date()).getFullYear(), 3, 0);
                    }
                    return _d;
                }
                if (4 <= _i <= 6) {
                    if (_tem === 1) {
                        _d[0] = new Date((new Date()).getFullYear(), 0, 1);
                        _d[1] = new Date((new Date()).getFullYear(), 3, 0);
                    } else {
                        _d[0] = new Date((new Date()).getFullYear(), 3, 1);
                        _d[1] = new Date((new Date()).getFullYear(), 7, 0);
                    }
                    return _d;
                }
                if (7 <= _i <= 9) {
                    if (_tem === 1) {
                        _d[0] = new Date((new Date()).getFullYear(), 3, 1);
                        _d[1] = new Date((new Date()).getFullYear(), 7, 0);
                    } else {
                        _d[0] = new Date((new Date()).getFullYear(), 6, 1);
                        _d[1] = new Date((new Date()).getFullYear(), 10, 0);
                    }
                    return _d;
                }
                if (10 <= _i <= 12) {
                    if (_tem === 1) {
                        _d[0] = new Date((new Date()).getFullYear(), 6, 1);
                        _d[1] = new Date((new Date()).getFullYear(), 10, 0);
                    } else {
                        _d[0] = new Date((new Date()).getFullYear(), 9, 1);
                        _d[1] = new Date((new Date()).getFullYear(), 12, 0);
                    }
                    return _d;
                }
            }
            scope.changedata = function (d1) {
                var _d = [];
                var _start = null;
                var _end = null;
                switch (parseInt(d1)) {
                    case 0:
                        break;
                    case 1:
                        _start = new Date((new Date()).getFullYear(), 0, 1);
                        _end = new Date((new Date()).getFullYear(), 12, 0);
                        break;
                    case 2:
                        var _x = getThisQuarter();
                        _start = _x[0];
                        _end = _x[1];
                        break;
                    case 3:
                        _start = new Date((new Date()).getFullYear(), (new Date()).getMonth(), 1);
                        _end = new Date((new Date()).getFullYear(), (new Date()).getMonth() + 1, 0);
                        break;
                    case 4:
                        var _x = (new Date()).getWeek();
                        for (var i = 0; i < scope.liWeek.length; i++) {
                            if (scope.liWeek[i].Value === _x) {
                                _start = scope.liWeek[i].StartDate;
                                _end = scope.liWeek[i].EndDate;
                                break;
                            }
                        }
                        break;
                    case 5:
                        _start = new Date((new Date()).getFullYear(), (new Date()).getMonth(), (new Date()).getDate());
                        _end = new Date((new Date()).getFullYear(), (new Date()).getMonth(), (new Date()).getDate(), 23, 59, 59);
                        break;
                    case 6:
                        _start = new Date((new Date()).getFullYear() - 1, 0, 1);
                        _end = new Date((new Date()).getFullYear(), 0, 0);
                        break;
                    case 7:
                        var _x = getThisQuarter(1);
                        _start = _x[0];
                        _end = _x[1];
                        break;
                    case 8:
                        _start = new Date((new Date()).getFullYear(), (new Date()).getMonth() - 1, 1);
                        _end = new Date((new Date()).getFullYear(), (new Date()).getMonth(), 0);
                        break;
                    case 9:
                        //have error when first week in year
                        var _x = (new Date()).getWeek() - 1;
                        for (var i = 0; i < scope.liWeek.length; i++) {
                            if (scope.liWeek[i].Value === _x) {
                                _start = scope.liWeek[i].StartDate;
                                _end = scope.liWeek[i].EndDate;
                                break;
                            }
                        }
                        break;
                    case 10:
                        _start = new Date((new Date()).getFullYear(), (new Date()).getMonth(), (new Date()).getDate() - 1);
                        _end = new Date((new Date()).getFullYear(), (new Date()).getMonth(), (new Date()).getDate() - 1, 23, 59, 59);
                        break;
                }
                (parseInt(d1) === 0) ? _d = [] : _d = [_start.getTime(), _end.getTime()];
                scope.optionChange({ data: _d });
                if (scope.$root.$$phase !== '$apply' && scope.$root.$$phase !== '$digest') {
                    scope.$apply();
                }
            }
            var _i = {};
            scope.placeholder !== undefined ? _i.placeholder = scope.placeholder : '';
            scope.seData === undefined ? _i.data = [] : _i.data = scope.seData;
            _i.selected = [scope.sPoint !== undefined ? scope.sPoint : 3];
            _i.data = [
                {
                    text: 'Tùy chọn',
                    value: '0'
                },
                {
                    text: 'Năm hiện tại',
                    value: '1'
                },
                {
                    text: 'Quý hiện tại',
                    value: '2'
                },
                {
                    text: 'Tháng hiện tại',
                    value: '3'
                },
                {
                    text: 'Tuần hiện tại',
                    value: '4'
                },
                {
                    text: 'Ngày hiện tại',
                    value: '5'
                },
                {
                    text: 'Năm trước',
                    value: '6'
                },
                {
                    text: 'Quý trước',
                    value: '7'
                },
                {
                    text: 'Tháng trước',
                    value: '8'
                },
                {
                    text: 'Tuần trước',
                    value: '9'
                },
                {
                    text: 'Ngày trước',
                    value: '10'
                },];
            initSmallSelect(elem[0], _i, scope.changedata);
            scope.changedata(scope.sPoint !== undefined ? scope.sPoint : 3);
        }
    };
}]);
String.prototype.replaceAll = function (search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};
function ConDate(data, number) {
    try {
        if (data == null || data == "") {
            return '';
        }
        if (data !== null && data != "" && data != undefined) {
            try {
                if (data.indexOf("SA") != -1 || data.indexOf("CH") != -1) {
                    if (data.indexOf("SA") != -1) { }
                    if (data.indexOf("CH") != -1) { }
                }

                if (data.indexOf('Date') != -1) {
                    data = data.substring(data.indexOf("Date") + 5);
                    data = parseInt(data);
                }
            }
            catch (ex) { }
            var newdate = new Date(data);
            if (number == 3) {
                return newdate;
            }
            if (number == 2) {
                return "/Date(" + newdate.getTime() + ")/";
            }
            var month = newdate.getMonth() + 1;
            var day = newdate.getDate();
            var year = newdate.getFullYear();
            var hh = newdate.getHours();
            var mm = newdate.getMinutes();
            if (month < 10)
                month = "0" + month;
            if (day < 10)
                day = "0" + day;
            if (mm < 10)
                mm = "0" + mm;
            if (number == 0) {
                return todayDate = day + "/" + month + "/" + year;
            }
            if (number == 1) {
                return todayDate = day + "/" + month + "/" + year + " " + hh + ":" + mm;
            }
            if (number == 4) {
                return new Date(year, month - 1, day);
            }
        } else {
            return '';
        }
    } catch (ex) {
        return "";
    }
}
function AllowOnlyNumbers(e, s) {

    e = (e) ? e : window.event;
    var key = null;
    var charsKeys = [
        97, // a  Ctrl + a Select All
        65, // A Ctrl + A Select All
        99, // c Ctrl + c Copy
        67, // C Ctrl + C Copy
        118, // v Ctrl + v paste
        86, // V Ctrl + V paste
        115, // s Ctrl + s save
        83, // S Ctrl + S save
        112, // p Ctrl + p print
        80 // P Ctrl + P print
    ];

    var specialKeys = [
        8, // backspace
        9, // tab
        27, // escape
        13, // enter
        35, // Home & shiftKey +  #
        36, // End & shiftKey + $
        37, // left arrow &  shiftKey + %
        39, //right arrow & '
        46, // delete & .
        45, //Ins &  -
        96,
        97,
        98,
        99,
        100,
        101,
        102,
        103,
        104,
        105,
        110
        //188 // . dicemal
    ];
    if (s === true)
        specialKeys.push(188);

    key = e.keyCode ? e.keyCode : e.which ? e.which : e.charCode;

    //console.log("e.charCode: " + e.charCode + ", " + "e.which: " + e.which + ", " + "e.keyCode: " + e.keyCode);
    //console.log(String.fromCharCode(key));

    // check if pressed key is not number 
    if (key && key < 48 || key > 57) {

        //Allow: Ctrl + char for action save, print, copy, ...etc
        if ((e.ctrlKey && charsKeys.indexOf(key) != -1) ||
            //Fix Issue: f1 : f12 Or Ctrl + f1 : f12, in Firefox browser
            (navigator.userAgent.indexOf("Firefox") != -1 && ((e.ctrlKey && e.keyCode && e.keyCode > 0 && key >= 112 && key <= 123) || (e.keyCode && e.keyCode > 0 && key && key >= 112 && key <= 123)))) {
            return true
        }
        // Allow: Special Keys
        else if (specialKeys.indexOf(key) != -1) {
            //Fix Issue: right arrow & Delete & ins in FireFox
            if ((key == 39 || key == 45 || key == 46)) {
                return (navigator.userAgent.indexOf("Firefox") != -1 && e.keyCode != undefined && e.keyCode > 0);
            }
            //DisAllow : "#" & "$" & "%"
            //add e.altKey to prevent AltGr chars
            else if ((e.shiftKey || e.altKey) && (key == 35 || key == 36 || key == 37)) {
                return false;
            }
            else {
                return true;
            }
        }
        else {
            return false;
        }
    }
    else {
        return true;
    }
}
function changeInputMoney(d1, t, d2) {
    if (!AllowOnlyNumbers(t, d2)) return;
    if (d2) if (d1.keyCode === 188) return t.value;
    var _s = t.selectionStart;
    _s = t.value.length - _s;
    var x1 = t.value;
    if (x1 === undefined || x1 === null || x1 === '') {
        return 0;
    }
    x1 = x1.replaceAll('[.]', '');
    var _x2 = '';
    if (x1.split(',').length > 1) {
        _x2 = x1.split(',')[1];
        if (_x2.length > 2) {
            _x2 = _x2.substring(0, 2);
        }
        x1 = x1.split(',')[0];
    }
    x1 = parseInt(x1).toString();
    var i1 = String(x1);
    i1 = i1.split('').reverse().join('');
    var arr = [];
    while (i1.length > 0) {
        arr.push(i1.substr(0, 3));
        i1 = i1.slice(3, i1.length);
    }
    for (var i = 0; i < arr.length; i++) {
        if (i === (arr.length - 1))
            i1 += arr[i];
        else {
            i1 += arr[i] + ".";
        }
    }
    i1 = i1.split('').reverse().join('');
    if (_x2 !== undefined && _x2 !== null && t.value.split(',').length > 1) {
        t.value = i1 + ',' + _x2;
    } else {
        t.value = i1;
    }
    _s = t.value.length - _s;
    t.setSelectionRange(_s, _s);
}
function convertMoney(d) {
    if (d === undefined || d === null || d === '') {
        return 0;
    }
    var d = String(d);
    var x1 = d;
    if (x1 === undefined || x1 === null) {
        return 0;
    }
    x1 = x1.replaceAll('[.]', '');
    var _x2 = '';
    if (x1.split(',').length > 1) {
        _x2 = x1.split(',')[1];
        if (_x2.length > 2) {
            _x2 = _x2.substring(0, 2);
        }
        x1 = x1.split(',')[0];
    }
    x1 = parseInt(x1).toString();
    var i1 = String(x1);
    i1 = i1.split('').reverse().join('');
    var arr = [];
    while (i1.length > 0) {
        arr.push(i1.substr(0, 3));
        i1 = i1.slice(3, i1.length);
    }
    for (var i = 0; i < arr.length; i++) {
        if (i === (arr.length - 1))
            i1 += arr[i];
        else {
            i1 += arr[i] + ".";
        }
    }
    i1 = i1.split('').reverse().join('');
    if (_x2 !== undefined && _x2 !== null && d.split(',').length > 1) {
        //x1 = i1 + ',' + _x2;
        x1 = i1;
    } else {
        x1 = i1;
    }
    return x1;
}