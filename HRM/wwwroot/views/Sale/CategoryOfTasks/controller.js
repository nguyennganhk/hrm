﻿var ctxfolder = "/views/Sale/CategoryOfTasks";
app.factory('dataservice', function ($http) {
    var headers = {
        "Content-Type": "application/json;odata=verbose",
        "Accept": "application/json;odata=verbose",
    }
    return {
        insert: function (data, callback) {
            $http.post('/CategoryOfTasks/insert', data).success(callback);
        },
        update: function (data, callback) {
            $http.post('/CategoryOfTasks/update', data).success(callback);
        },
        delete: function (data, callback) {
            $http.post('/CategoryOfTasks/delete', data).success(callback);
        },
        gitem: function (data, callback) {
            $http.post('/CategoryOfTasks/getitem', data).success(callback);
        },
        getall: function (data, callback) {
            $http.post('/CategoryOfTasks/getall/', data).success(callback);
        },
        resort: function (data, callback) {
            $http.post('/CategoryOfTasks/resort', data).success(callback);
        },
        gettreedata: function (data, callback) {
            $http.post('/CategoryOfTasks/gettreedata/' + data).success(callback);
        },
        getbyparent: function (data, callback) {
            $http.post('/CategoryOfTasks/getbyparent/' + data).success(callback);
        },
        CheckStatus: function (data, callback) {
            $http.post('/CategoryOfTasks/CheckStatus/' + data).success(callback);
        },
        UpdateIsActive: function (data, callback) {
            $http.post('/CategoryOfTasks/UpdateIsActive', data).success(callback);
        },
        DeleteIsActive: function (data, callback) {
            $http.post('/CategoryOfTasks/DeleteIsActive', data).success(callback);
        },

        GetDepart: function (data, callback) {
            $http.post('/CategoryOfTasks/GetDepart', data).success(callback);
        },
        getDepartItem: function (data, callback) {
            $http.post('/CategoryOfTasks/getDepartItem', data).success(callback);
        },
        getEmployee: function (data, callback) {
            $http.post('/CategoryOfTasks/getEmployee', data).success(callback);
        },
        getEmployeeItem: function (data, callback) {
            $http.post('/CategoryOfTasks/getEmployeeItem', data).success(callback);
        },
        gettreedata: function (data, callback) {
            $http.post('/CategoryOfTasks/gettreedata/' + data).success(callback);
        },

        getDepart: function (data, callback) {
            $http.post('/Cooperation/Tasks/getDepartCOT/', data).success(callback);
        },
        getEmployee: function (data, callback) {
            $http.post('/Cooperation/Tasks/getEmployeeCOT', data).success(callback);
        },
        getEmployeeLogin: function (data, callback) {
            $http.post('/Cooperation/Tasks/GetEmployeeLogin', data).success(callback);
        },
        insertCategoryOfTask: function (data, callback) {
            $http.post('/CategoryOfTasks/Insert', data).success(callback);
        },
    }
});
app.filter('fdate', [
    '$filter', function ($filter) {
        return function (input, f) {
            if (input && input.toString().indexOf('Date') > -1) {
                return moment(input).format(f);
            } if (input && input.toString().indexOf('T') > -1) {
                return moment(input).format(f);
            } else return input;
        };
    }
]);
app.filter('fstatus', [
    '$filter', function ($filter) {
        return function (s) {
            if (s === false) return "Không sử dụng";
            if (s === true) return "Sử dụng";
            return "";
        };
    }
]);
app.controller('Ctrl_CategoryOfTasks', function ($scope, $rootScope, $compile, $uibModal, DTOptionsBuilder, $confirm, DTColumnBuilder, DTInstances, dataservice) {
    $rootScope.go = function (path) {
        $location.path(path); return false;
    };
    $rootScope.validationOptions = {
        rules: {
            Title: {
                required: true,
                maxlength: 255
            }
        },
        messages: {
            Title: {
                required: "Yêu cầu nhập tên nhóm công việc.",
                maxlength: "nhóm công việc không vượt quá 255 ký tự."
            }
        }
    }
    $rootScope.StatusData = [{
        Value: true,
        Name: 'Sử dụng'
    }, {
        Value: false,
        Name: 'Không sử dụng'
    }];
    $rootScope.ConvertStatus = function (data) {
        if (data !== null && data !== undefined) {
            if (data === true) {
                return 'Sử dụng';
            } else if (data === false) {
                return 'Không sử dụng';
            } else {
                return "";
            }
        }
    }
    dataservice.gettreedata(0, function (result) {
        $rootScope.treeData = result;
    });
    dataservice.GetDepart('', function (rs) {
        if (rs.Error) {
            App.notifyDanger(rs.Title);
        } else {
            for (var i = 0; i < rs.Object.length; i++) {
                rs.Object[i].Title = rs.Object[i].DepartmentName;
                rs.Object[i].Id = rs.Object[i].DepartmentGuid;
            }
            $rootScope.dDepartments = Recu([], rs.Object, null, '', '--- ', '');
            for (i = 0; i < $rootScope.dDepartments.length; i++) {
                $rootScope.dDepartments[i].text = $rootScope.dDepartments[i].DepartmentName;
                $rootScope.dDepartments[i].value = $rootScope.dDepartments[i].DepartmentGuid;
                $rootScope.dDepartments[i].DepartmentName = $rootScope.dDepartments[i].Title;
            }
        }
    });
    function Recu(rs, d, p, t, b, f) {
        var _d = [];
        if (p === null || p === 0) {
            for (var i = 0; i < d.length; i++) { if (d[i].ParentId === null) { _d.push(d[i]) } }
        }
        else {
            for (var i = 0; i < d.length; i++) { if (d[i].ParentId === p) { _d.push(d[i]) } }
        }
        for (var i = 0; i < _d.length; i++) {
            _d[i].Title = t + _d[i].Title;
            rs.push(_d[i]);
            var c = 0;
            for (var _i = 0; _i < d.length; _i++) {
                if (d[_i].ParentId === _d[i].Id) {
                    c++;
                }
            }
            if (c > 0) { Recu(rs, d, _d[i].Id, t + b, b) }
        }
        if (f !== undefined) {
            return rs
        }
    }
    $rootScope.GetSubTreeData = function (data, parentid, lstCategories, tab) {
        tab += 1;
        var contents = parentid == 0
            ? data.filter(x => x.MaCha == null)
            : data.filter(x => x.MaCha == parentid)
        angular.forEach(contents, function (value, key) {
            value.CapBac = tab;
            lstCategories.push(value);
            if (value.Ma != null) {
                var a = data.filter(x => x.MaCha == value.Ma)
                if (a.length > 0) $rootScope.GetSubTreeData(data, value.Ma, lstCategories, tab);
            }

        })
        return lstCategories;
    }
    $rootScope.getEmployee = function (d) {
        $rootScope.liEmployee = [];
        dataservice.getEmployee({ IdS: [d] }, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {
                $rootScope.liEmployee = rs.Object;
            }
        })
    }
    //0 11/11/2017
    //1 11/11/2017 22:22
    //2 123123123
    //3 type date
    //4 type data year month day
    $rootScope.ConDate = function (data, number) {
        if (data == null || data == "") {
            return '';
        }
        if (data !== null && data != "" && data != undefined) {
            try {
                if (data.indexOf("SA") != -1 || data.indexOf("CH") != -1) {
                    if (data.indexOf("SA") != -1) { }
                    if (data.indexOf("CH") != -1) { }
                }

                if (data.indexOf('Date') != -1) {
                    data = data.substring(data.indexOf("Date") + 5);
                    data = parseInt(data);
                }
            }
            catch (ex) { }
            var newdate = new Date(data);
            if (number == 3) {
                return newdate;
            }
            if (number == 2) {
                return "/Date(" + newdate.getTime() + ")/";
            }
            var month = newdate.getMonth() + 1;
            var day = newdate.getDate();
            var year = newdate.getFullYear();
            var hh = newdate.getHours();
            var mm = newdate.getMinutes();
            if (month < 10)
                month = "0" + month;
            if (day < 10)
                day = "0" + day;
            if (mm < 10)
                mm = "0" + mm;
            if (number == 0) {
                return todayDate = day + "/" + month + "/" + year;
            }
            if (number == 1) {
                return todayDate = day + "/" + month + "/" + year + " " + hh + ":" + mm;
            }
            if (number == 4) {
                return new Date(year, month - 1, day);
            }
        } else {
            return '';
        }
    }
});
app.config(function ($routeProvider, $validatorProvider) {
    $routeProvider
        .when('/', {
            templateUrl: ctxfolder + '/index.html',
            controller: 'index'
        })
        .when('/edit/:id', {
            templateUrl: ctxfolder + '/edit.html',
            controller: 'edit'
        })
        .when('/add/', {
            templateUrl: ctxfolder + '/add.html',
            controller: 'add'
        })
    $validatorProvider.setDefaults({
        errorElement: 'span',
        errorClass: 'help-block',
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        success: function (label) {
            label.closest('.form-group').removeClass('has-error');
        }
    });

});
app.controller('index', function ($scope, $rootScope, $confirm, $uibModal, dataservice, $ngConfirm) {
    $scope.model = {
        Keyword: '',
    };
    $rootScope.ID = 0;
    $rootScope.reload = function () {
        $rootScope.ID = 0;
        $scope.model.Id = undefined;
        $scope.loadData();
    }
    $scope.loadData = function () {
        App.blockUI({
            target: "#contentMain",
            boxed: true,
            message: 'Đang tải...'
        });
        dataservice.getall({ Id: 0, Keyword: $scope.model.Keyword }, function (result) {
            $scope.tree_data = buildJtree(result);
            App.unblockUI("#contentMain");
        });
    }
    $scope.loadData();

    $scope.expanding_property = {
        field: "Title",
        width: "200px",
        displayName: "Tên nhóm công việc",
        sortable: true,
        filterable: true
    };
    $scope.tree_data = [];
    $scope.my_tree = tree = {};
    var data = [];
    data.push('<a  style="margin-left:5px!important" class="table__cell-actions-item table__cell-actions-icon" title="Xem" ng-click="cellTemplateScope.open(row.branch)"><i class="mdi mdi-eye"></i ></a >');
    data.push('<a  style="margin-left:5px!important" class="table__cell-actions-item table__cell-actions-icon" title="Sửa" ng-click="cellTemplateScope.edit(row.branch)"><i class="mdi mdi-table-edit"></i ></a >');
    data.push('<a  style="margin-left:5px!important" class="table__cell-actions-item table__cell-actions-icon" title="Xóa" ng-click="cellTemplateScope.delete(row.branch)"><i class="mdi mdi-delete""></i ></a >');
    var contextMenu = [];
    contextMenu.push("<li ><a class='dropdown-item' ng-click='cellTemplateScope.open(row.branch)' title='Xem nhóm công việc'><i class='mdi mdi-eye'></i> Xem nhóm công việc</a></li>");
    contextMenu.push("<li ><a class='dropdown-item' ng-click='cellTemplateScope.add(row.branch)' title='Thêm nhóm công việc'><i class='btn-icon mdi mdi-plus-box'></i> Thêm nhóm công việc</a></li>");
    contextMenu.push("<li ><a class='dropdown-item' ng-click='cellTemplateScope.edit(row.branch)' title='Sửa nhóm công việc'><i class='mdi mdi-table-edit'></i> Sửa nhóm công việc</a></li>");
    contextMenu.push("<li  ><a class='dropdown-item' ng-click='cellTemplateScope.delete(row.branch)' title='xóa nhóm công việc'><i class='mdi mdi-delete'></i> Xóa nhóm công việc</a></li>");
    $scope.col_defs = [
        {
            field: "",
            sortable: false, width: '45px',
            displayName: "",
            colclass: "action",
            cellTemplate: "<div class='btn-group  '>"
                + "<a   class='btn btn-icon-xs btn-icon-only   ' data-toggle='dropdown'  ><i class='mdi mdi-view-list subnav__heading-icon'></i></a>"
                + "<ul class='dropdown-menu  pull-right NONE-transform' >"
                + contextMenu.join("")
                + "</ul>"
                + "</div>",
            cellTemplateScope: {
                delete: function (data) {
                    $scope.delete(data);
                },
                edit: function (data) {
                    $scope.edit(data);
                },
                add: function (data) {
                    $scope.add(data.CategoryOfTaskGuid);
                },
                sort: function (data) {
                    $scope.sort(data.CategoryOfTaskGuid);
                },
                open: function (data) {
                    $scope.open(data);
                }
            }
        }, {
            field: "ModifiedDate", displayName: "Ngày cập nhật", width: "140px",
            sortable: true,
            cellTemplate: "<span> {{row.branch[col.field] | fdate:'DD/MM/YYYY HH:mm'}} </span>",
        },
        {
            field: "IsActive", displayName: "Trạng thái", width: "120px",
            sortable: true,
            cellTemplate: "<span> {{row.branch[col.field] | fstatus}} </span>",
        },
        {
            field: "", displayName: "Điều khiển", width: "90px",
            sortable: true,
            cellTemplate: data.join(""),
            cellTemplateScope: {
                delete: function (data) {
                    $scope.delete(data);
                },
                edit: function (data) {
                    $scope.edit(data);
                },
                sort: function (data) {
                    $scope.sort(data.CategoryOfTaskGuid);
                },
                open: function (data) {
                    $scope.open(data);
                }
            }
        }

    ];

    $scope.my_tree_handler = function (branch) {
        $rootScope.selectedItem = branch;
    }

    function buildJtree(data) {
        var tree = [];
        if (!data || data.length === 0) return [];
        $.each(data, function (index, item) {
            if (!item.ParentId) {
                var treeObjs = item;
                var sub = subJtree(data, item.CategoryOfTaskGuid);
                if (sub.length > 0) {
                    treeObjs.children = sub;
                }
                treeObjs.expanded = true;
                tree.push(treeObjs);
            }
        });
        return tree;
    }
    function subJtree(data, parentVal) {
        var subTree = [];
        $.each(data, function (index, item) {
            if (item.ParentId && item.ParentId === parentVal) {
                var treeObjs = item;
                var sub = subJtree(data, item.CategoryOfTaskGuid);
                if (sub.length > 0) {
                    treeObjs.children = sub;
                }
                treeObjs.expanded = true;
                subTree.push(treeObjs);
            }
        });
        return subTree;
    }
    $scope.open = function (p) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/open.html',
            controller: 'open',
            backdrop: 'static',
            size: '60',
            resolve: {
                para: function () {
                    return p.CategoryOfTaskGuid;
                }
            }
        });
        modalInstance.result.then(function (d) {
            if ($rootScope.ID != 0 && $rootScope.ID != undefined) {
                $rootScope.ParentID();
            } else {
                $rootScope.reload()
            }
        }, function () {
        });
    }

    $scope.add = function (p) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/add.html',
            controller: 'Task_addCategoryOfTask',
            backdrop: 'static',
            size: 70,
            resolve: {
                para: function () {
                    return p;
                }
            }
        });
        modalInstance.result.then(function (d) {
            if ($rootScope.ID != 0 && $rootScope.ID != undefined) {
                $rootScope.ParentID();
            } else {
                $rootScope.reload()
            }
        }, function () {
        });
    }

    //#endregion
    $scope.delete = function (temp) {
        ESConfirm([{ 'value': 1, 'title': 'Xác nhận', 'type': 'info' }, { 'value': 2, 'title': 'Đóng', 'type': 'danger' }], { 'description': 'Bạn có chắc chắn muốn xóa [' + temp.Title + '] ?', 'class': 'eswarning_v2' }, function (rs) {
            if (rs === '1') {
                App.blockUI({
                    target: "#contentMain",
                    boxed: true,
                    message: 'Đang tải...'
                });
                dataservice.delete({ IdS: [temp.CategoryOfTaskGuid] }, function (rs) {
                    if (rs.Error) {
                        App.notifyDanger(rs.Title);
                    } else {
                        App.notifyInfo(rs.Title);
                        dataservice.DeleteIsActive(temp, function (rs) {
                            if (rs.Error) {
                                App.notifyDanger(rs.Title);
                                return;
                            } else {
                                if ($rootScope.ID != 0 && $rootScope.ID != undefined) {
                                    $rootScope.ParentID();
                                } else {
                                    $rootScope.reload()
                                }
                            }
                        })

                    }
                    App.unblockUI("#contentMain");
                });
            }
        }, function () {
        });
    };



    $scope.edit = function (temp) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/add.html',
            controller: 'CategoryOfTask_edit',
            backdrop: 'static',
            size: 70,
            resolve: {
                para: function () {
                    return temp.CategoryOfTaskGuid;
                }
            }
        });
        modalInstance.result.then(function (d) {
            if ($rootScope.ID != 0 && $rootScope.ID != undefined) {
                $rootScope.ParentID();
            } else {
                $rootScope.reload()
            }
        }, function () {
        });
    }

    $rootScope.ParentID = function () {
        $rootScope.ID = $scope.model.Id;
        dataservice.getall({ Id: 0, Keyword: $scope.model.Keyword }, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {
                $scope.tree_data = buildJtree(rs);
            }
        });
    }
});
app.controller('Task_addCategoryOfTask', function ($scope, $rootScope, $uibModal, $uibModalInstance, dataservice, para) {
    var ctrl = $scope;
    ctrl.Title = "Thêm mới nhóm công việc";

    //ctrl.dDepartments = [];
    ctrl.liTaskProcess = [{ OrderId: 1 }];
    ctrl.model = {
        DepartmentGuid: null,
        DepartmentGuids: [],
        TaskStepBySteps: [],
    };
    ctrl.model.ParentId = para === undefined ? null : para;
    ctrl.liEmployee = [];
    ctrl.liTaskStepBySteps = [];
    ctrl.getEmployee = function (d) {
        ctrl.liEmployee = [];
        dataservice.getEmployee({ IdS: [d] }, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {
                for (var i = 0; i < rs.Object.length; i++) {
                    rs.Object[i].value = rs.Object[i].EmployeeGuid;
                    rs.Object[i].text = rs.Object[i].FullName;
                }
                ctrl.liEmployee = rs.Object;
            }
        })
    }
    ctrl.changeDepartment = function () {
        if (ctrl.model.DepartmentGuid !== null && ctrl.model.DepartmentGuid != undefined) {
            ctrl.getEmployee(ctrl.model.DepartmentGuid);
        }
    }
    ctrl.StatusData = [{
        Value: true,
        Name: 'Sử dụng'
    }, {
        Value: false,
        Name: 'Không sử dụng'
    }];
    //ctrl.loadDepartment = function () {
    //    dataservice.getDepart('', function (rs) {
    //        if (rs.Error) {
    //            App.notifyDanger(rs.Title);
    //        } else {
    //            for (var i = 0; i < rs.Object.length; i++) {
    //                rs.Object[i].value = rs.Object[i].DepartmentGuid;
    //                rs.Object[i].text = rs.Object[i].DepartmentName;
    //            }
    //            ctrl.dDepartments = rs.Object;
    //        }
    //    });
    //}
    ctrl.getEmployeeLogin = function () {
        dataservice.getEmployeeLogin('', function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {
                ctrl.model.DepartmentGuid = rs.Object.DepartmentGuid;
                ctrl.model.DepartmentName = rs.Object.DepartmentName;
                ctrl.model.EmployeeGuid = rs.Object.EmployeeGuid;
                ctrl.changeDepartment();
            }
        })
    }
    ctrl.loadData = function () {
        dataservice.gettreedata(0, function (result) {
            ctrl.treeData = result;
        });
        //ctrl.loadDepartment();
        ctrl.getEmployeeLogin();
    }
    ctrl.loadData();
    ctrl.changeDepartment = function () {
        if (ctrl.model.DepartmentGuid !== null && ctrl.model.DepartmentGuid != undefined) {
            ctrl.getEmployee(ctrl.model.DepartmentGuid);
        }
    }
    ctrl.initDepartments = {
        change: ctrl.changeDepartment,
        search: true,
    };
    ctrl.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    }
    ctrl.ok = function () {
        $uibModalInstance.close();
    }
    ctrl.submit = function () {
        if (ctrl.addform.validate()) {
            if (ctrl.model.DepartmentGuid == null || ctrl.model.DepartmentGuid == undefined || ctrl.model.DepartmentGuid.trim() == '') {
                App.notifyDanger('Yêu cầu chọn phòng ban');
                return;
            }
            if (ctrl.model.EmployeeGuid == null || ctrl.model.EmployeeGuid == undefined || ctrl.model.EmployeeGuid.trim() == '') {
                App.notifyDanger('Yêu cầu chọn người quản lý');
                return;
            }
            for (var i = 0; i < ctrl.dDepartments.length; i++) {
                if (ctrl.model.DepartmentGuid === ctrl.dDepartments[i].DepartmentGuid) {
                    ctrl.model.DepartmentId = ctrl.dDepartments[i].DepartmentId;
                };
            }
            //for (var i = 0; i < ctrl.liEmployee.length; i++) {
            //    if (ctrl.model.EmployeeGuid === ctrl.liEmployee[i].EmployeeGuid) {
            //        ctrl.model.EmployeeId = ctrl.liEmployee[i].EmployeeId;
            //        ctrl.model.EmployeeName = ctrl.liEmployee[i].FullName;
            //    };
            //}
            ctrl.model.CategoryOfProcess = [];
            //for (var i = 0; i < ctrl._liTaskProcessFixLock.length; i++) {
            //    var _item = {
            //        OrderId: ctrl._liTaskProcessFixLock[i].OrderId,
            //        TaskProcessGu: {
            //            Title: ctrl._liTaskProcessFixLock[i].Title,
            //            Class: ctrl._liTaskProcessFixLock[i].Class,
            //            IsActive: true,
            //        }
            //    };
            //    ctrl.model.CategoryOfProcess.push(_item);
            //}
            if (ctrl.liTaskProcess.length > 0) {
                for (var i = 0; i < ctrl.liTaskProcess.length; i++) {
                    if (ctrl.liTaskProcess[i].Title !== undefined && ctrl.liTaskProcess[i].Title !== null) {
                        var _item = {
                            OrderId: ctrl.liTaskProcess[i].OrderId,
                            TaskProcessGu: {
                                Title: ctrl.liTaskProcess[i].Title,
                                Class: ctrl.liTaskProcess[i].Class,
                                IsActive: true,
                            }
                        };
                        ctrl.model.CategoryOfProcess.push(_item);
                    }
                }
            }
            if (ctrl.model.DepartmentGuids && ctrl.model.DepartmentGuids.length > 0) {
                ctrl.model.CategoryShare = [];
                for (var i = 0; i < ctrl.model.DepartmentGuids.length; i++) {
                    var _item = {
                        DepartmentGuid: ctrl.model.DepartmentGuids[i].DepartmentGuid,
                        DepartmentId: ctrl.model.DepartmentGuids[i].DepartmentId,
                        DepartmentName: ctrl.model.DepartmentGuids[i].DepartmentName,
                    };
                    ctrl.model.CategoryShare.push(_item);
                }
            }
            if (ctrl.model.IsActiveStep && ctrl.model.TaskStepBySteps.length > 0) {
                for (var i = 0; i < ctrl.model.TaskStepBySteps.length; i++) {
                    for (var j = 0; j < ctrl.liTaskStepBySteps.length; j++) {
                        if (ctrl.model.TaskStepBySteps[i].Guid === ctrl.liTaskStepBySteps[j].value) {
                            ctrl.model.TaskStepBySteps[i].Name = ctrl.liTaskStepBySteps[j].text;
                            switch (ctrl.model.TypeStep) {
                                case 'DEP':
                                    ctrl.model.TaskStepBySteps[i].Code = ctrl.liTaskStepBySteps[j].DepartmentId;
                                    break;
                                case 'EMP':
                                    ctrl.model.TaskStepBySteps[i].Code = ctrl.liTaskStepBySteps[j].EmployeeId;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        }
                    }
                    for (var j = 0; j < ctrl.liEmployee.length; j++) {
                        if (ctrl.model.TaskStepBySteps[i].PermissionByGuid === ctrl.liEmployee[j].value) {
                            ctrl.model.TaskStepBySteps[i].PermissionByLogin = ctrl.liEmployee[j].LoginName;
                            ctrl.model.TaskStepBySteps[i].PermissionByName = ctrl.liEmployee[j].PermissionByName;
                            break;
                        }
                    }
                }
            }
            else {
                ctrl.model.TaskStepBySteps = [];
            }
            dataservice.insertCategoryOfTask(ctrl.model, function (rs) {
                if (rs.Error) {
                    App.notifyDanger(rs.Title);
                } else {
                    App.notifyInfo(rs.Title);
                    $uibModalInstance.close();
                }
            });
        }
    }
    ctrl.addTaskProcess = function () {
        ctrl.liTaskProcess.push({
            OrderId: ctrl.liTaskProcess.length + 1,
            Class: '#b09611',
        })
    }
    ctrl.delTaskProcess = function (i) {
        ctrl.liTaskProcess.splice(i, 1);
    }
    // Begin 2020/10/16
    ctrl.liDepartments = [];
    ctrl.loadCategoryShare = function () {
        dataservice.getDepart('', function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {
                if (rs.Object !== null) {
                    rs.Object = RecuDepartment([], rs.Object, null, '');
                    ctrl.liDepartments = rs.Object;
                }
            }
        })
    }
    /*   ctrl.loadCategoryShare();*/
    // End 2020/10/16
    // Begin 2021/1/18
    ctrl.liTypeStep = [{
        value: 'DEP',
        text: 'Phòng ban',
    }, {
        value: 'EMP',
        text: 'Nhân viên',
    },];
    ctrl.loadComboStep = function () {
        switch (ctrl.model.TypeStep) {
            case 'DEP':
                ctrl.liTaskStepBySteps = ctrl.dDepartments;
                break;
            case 'EMP':
                ctrl.liTaskStepBySteps = ctrl.liEmployee;
                break;
            default:
                break;
        }
        if (ctrl.$root.$$phase !== '$apply' && ctrl.$root.$$phase !== '$digest') {
            ctrl.$apply();
        }
    }
    ctrl.onChangeStep = function () {
        ctrl.model.TypeStep = 'DEP';
        if (ctrl.model.IsActiveStep) {
            ctrl.addTaskStepBySteps();
        } else {
            ctrl.model.TaskStepBySteps = [];
        }
        ctrl.loadComboStep();
    }
    ctrl.initTypeStep = {
        change: ctrl.loadComboStep,
    };
    ctrl.delTaskStepBySteps = function (index) {
        ctrl.model.TaskStepBySteps.splice(index, 1);
        for (var i = 0; i < ctrl.model.TaskStepBySteps.length; i++) {
            ctrl.model.TaskStepBySteps[i].Sort = i + 1;
        }
    }
    ctrl.addTaskStepBySteps = function () {
        ctrl.model.TaskStepBySteps.push({
            Sort: ctrl.model.TaskStepBySteps.length + 1,
            Guid: null,
        });
    }
    // End 2021/1/18
});
app.controller('CategoryOfTask_edit', function ($scope, $rootScope, $compile, $uibModal, $confirm, $uibModalInstance, dataservice, para) {
    var ctrl = $scope;
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    }
    $scope.Title = 'Sửa nhóm công việc';
    $scope.Status = false;
    ctrl.liEmployee = [];
    ctrl.liTaskStepBySteps = [];
    ctrl.dDepartments = [];
    ctrl.loadDepartment = function () {
        dataservice.getDepart('', function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {
                for (var i = 0; i < rs.Object.length; i++) {
                    rs.Object[i].value = rs.Object[i].DepartmentGuid;
                    rs.Object[i].text = rs.Object[i].DepartmentName;
                }
                ctrl.dDepartments = rs.Object;
            }
        });
    }
    ctrl.getEmployee = function (d) {
        ctrl.liEmployee = [];
        dataservice.getEmployee({ IdS: [d] }, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {
                for (var i = 0; i < rs.Object.length; i++) {
                    rs.Object[i].value = rs.Object[i].EmployeeGuid;
                    rs.Object[i].text = rs.Object[i].FullName;
                }
                ctrl.liEmployee = rs.Object;
            }
        })
    }
    $scope.IsActive = function () {
        $scope.Status = false;
        if ($scope.model.IsActive === true) {
            dataservice.CheckStatus($scope.model.ParentId, function (rs) {
                if (rs.length > 0) {
                    $scope.Status = true;
                    App.notifyDanger("Bạn không thể cập nhật trạng thái khi cấp trên không còn sử dụng");
                }
            })
        }
    }
    $scope.changeDepartment = function (d) {
        $scope.model.DepartmentGuid = d;
        if ($scope.model.DepartmentGuid !== null && $scope.model.DepartmentGuid !== undefined) {
            $scope.getEmployee($scope.model.DepartmentGuid);
        }
    }
    // Begin 2020/10/16
    ctrl.liDepartments = [];
    ctrl.loadCategoryShare = function () {
        dataservice.getDepart('', function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {
                if (rs.Object !== null) {
                    rs.Object = RecuDepartment([], rs.Object, null, '');
                    ctrl.liDepartments = rs.Object;
                    if (ctrl.model.DataCategoryShares && ctrl.model.DataCategoryShares.length > 0) {
                        if (ctrl.liDepartments && ctrl.liDepartments.length > 0) {
                            ctrl.model.DepartmentGuids = [];
                            for (var i = 0; i < ctrl.liDepartments.length; i++) {
                                for (var j = 0; j < ctrl.model.DataCategoryShares.length; j++) {
                                    if (ctrl.model.DataCategoryShares[j].DepartmentId.toUpperCase() === ctrl.liDepartments[i].DepartmentId.toUpperCase()) {
                                        ctrl.model.DepartmentGuids.push(ctrl.liDepartments[i]);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        })
    }
    // End 2020/10/16
    $scope.init = function () {
        ctrl.loadDepartment();
        dataservice.gitem({ IdS: [para] }, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {
                dataservice.gettreedata(rs.CategoryOfTaskGuid, function (result) {
                    $scope.treeData = result;
                });
                $scope.model = rs;
                $scope.liTaskProcess = rs.CategoryOfProcess;
                ctrl.loadCategoryShare();
                if (ctrl.model.TypeStep !== null && ctrl.model.TypeStep !== "") {
                    ctrl.model.IsActiveStep = true;
                }
                $scope.changeDepartment($scope.model.DepartmentGuid);
                ctrl.loadComboStep();
            }
        });
    }
    $scope.init();
    $scope.submit = function () {
        if ($scope.addform.validate()) {
            if ($scope.model.DepartmentGuid == null || $scope.model.DepartmentGuid == undefined || $scope.model.DepartmentGuid.trim() == '') {
                App.notifyDanger('Yêu cầu chọn phòng ban');
                return;
            }
            if ($scope.model.EmployeeGuid == null || $scope.model.EmployeeGuid == undefined || $scope.model.EmployeeGuid.trim() == '') {
                App.notifyDanger('Yêu cầu chọn người quản lý');
                return;
            }
            for (var i = 0; i < $rootScope.dDepartments.length; i++) {
                if ($scope.model.DepartmentGuid === $rootScope.dDepartments[i].DepartmentGuid) {
                    $scope.model.DepartmentId = $rootScope.dDepartments[i].DepartmentId;
                };
            }
            for (var i = 0; i < $scope.liEmployee.length; i++) {
                if ($scope.model.EmployeeGuid === $scope.liEmployee[i].EmployeeGuid) {
                    $scope.model.EmployeeId = $scope.liEmployee[i].EmployeeId;
                    $scope.model.EmployeeName = $scope.liEmployee[i].FullName;
                };
            }
            ctrl.model.CategoryOfProcess = [];
            if (ctrl.liTaskProcess.length > 0) {
                for (var i = 0; i < ctrl.liTaskProcess.length; i++) {
                    if (ctrl.liTaskProcess[i].Title !== undefined && ctrl.liTaskProcess[i].Title !== null) {
                        var _item = {
                            OrderId: ctrl.liTaskProcess[i].OrderId,
                            TaskProcessGu: {
                                Title: ctrl.liTaskProcess[i].Title,
                                Class: ctrl.liTaskProcess[i].Class,
                                IsActive: true,
                            }
                        };
                        ctrl.model.CategoryOfProcess.push(_item);
                    }
                }
            }
            if (ctrl.model.DepartmentGuids && ctrl.model.DepartmentGuids.length > 0) {
                ctrl.model.CategoryShare = [];
                for (var i = 0; i < ctrl.model.DepartmentGuids.length; i++) {
                    var _item = {
                        DepartmentGuid: ctrl.model.DepartmentGuids[i].DepartmentGuid,
                        DepartmentId: ctrl.model.DepartmentGuids[i].DepartmentId,
                        DepartmentName: ctrl.model.DepartmentGuids[i].DepartmentName,
                    };
                    ctrl.model.CategoryShare.push(_item);
                }
            }
            if (ctrl.model.TaskStepBySteps.length > 0) {
                for (var i = 0; i < ctrl.model.TaskStepBySteps.length; i++) {
                    for (var j = 0; j < ctrl.liTaskStepBySteps.length; j++) {
                        if (ctrl.model.TaskStepBySteps[i].Guid === ctrl.liTaskStepBySteps[j].value) {
                            ctrl.model.TaskStepBySteps[i].Name = ctrl.liTaskStepBySteps[j].text;
                            switch (ctrl.model.TypeStep) {
                                case 'DEP':
                                    ctrl.model.TaskStepBySteps[i].Code = ctrl.liTaskStepBySteps[j].DepartmentId;
                                    break;
                                case 'EMP':
                                    ctrl.model.TaskStepBySteps[i].Code = ctrl.liTaskStepBySteps[j].EmployeeId;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        }
                    }
                    for (var j = 0; j < ctrl.liEmployee.length; j++) {
                        if (ctrl.model.TaskStepBySteps[i].PermissionByGuid === ctrl.liEmployee[j].value) {
                            ctrl.model.TaskStepBySteps[i].PermissionByLogin = ctrl.liEmployee[j].LoginName;
                            ctrl.model.TaskStepBySteps[i].PermissionByName = ctrl.liEmployee[j].PermissionByName;
                            break;
                        }
                    }
                }
            }
            dataservice.update($scope.model, function (rs) {
                if (rs.Error) {
                    App.notifyDanger(rs.Title);
                } else {
                    App.notifyInfo(rs.Title);
                    $uibModalInstance.close();
                    dataservice.UpdateIsActive($scope.model, function (rs) {
                        if (rs.Error) {
                            App.notifyDanger(rs.Title);
                            return;
                        } else {
                            $uibModalInstance.close();
                        }
                    })
                }
            });
        }
    }
    ctrl.addTaskProcess = function () {
        ctrl.liTaskProcess.push({
            OrderId: ctrl.liTaskProcess.length + 1,
        })
    }
    ctrl.delTaskProcess = function (i) {
        ctrl.liTaskProcess.splice(i, 1);
    }
    // Begin 2021/1/18
    ctrl.liTypeStep = [{
        value: 'DEP',
        text: 'Phòng ban',
    }, {
        value: 'EMP',
        text: 'Nhân viên',
    },];
    ctrl.loadComboStep = function () {
        switch (ctrl.model.TypeStep) {
            case 'DEP':
                ctrl.liTaskStepBySteps = ctrl.dDepartments;
                break;
            case 'EMP':
                ctrl.liTaskStepBySteps = ctrl.liEmployee;
                break;
            default:
                break;
        }
        if (ctrl.$root.$$phase !== '$apply' && ctrl.$root.$$phase !== '$digest') {
            ctrl.$apply();
        }
    }
    ctrl.onChangeStep = function () {
        ctrl.model.TypeStep = 'DEP';
        if (ctrl.model.IsActiveStep) {
            ctrl.addTaskStepBySteps();
        } else {
            ctrl.model.TaskStepBySteps = [];
        }
        ctrl.loadComboStep();
    }
    ctrl.initTypeStep = {
        change: ctrl.loadComboStep,
    };
    ctrl.delTaskStepBySteps = function (index) {
        ctrl.model.TaskStepBySteps.splice(index, 1);
        for (var i = 0; i < ctrl.model.TaskStepBySteps.length; i++) {
            ctrl.model.TaskStepBySteps[i].Sort = i + 1;
        }
    }
    ctrl.addTaskStepBySteps = function () {
        ctrl.model.TaskStepBySteps.push({
            Sort: ctrl.model.TaskStepBySteps.length + 1,
            Guid: null,
        });
    }
    // End 2021/1/18
});

app.controller('open', function ($scope, $rootScope, $compile, $uibModal, $confirm, $uibModalInstance, dataservice, para) {
    var ctrl = $scope;
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    }
    $scope.Title = 'Xem nhóm công việc';
    $scope.Status = false;
    ctrl.liEmployee = [];
    ctrl.liTaskStepBySteps = [];
    ctrl.dDepartments = [];
    ctrl.loadDepartment = function () {
        dataservice.getDepart('', function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {
                for (var i = 0; i < rs.Object.length; i++) {
                    rs.Object[i].value = rs.Object[i].DepartmentGuid;
                    rs.Object[i].text = rs.Object[i].DepartmentName;
                }
                ctrl.dDepartments = rs.Object;
            }
        });
    }
    ctrl.getEmployee = function (d) {
        ctrl.liEmployee = [];
        dataservice.getEmployee({ IdS: [d] }, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {
                for (var i = 0; i < rs.Object.length; i++) {
                    rs.Object[i].value = rs.Object[i].EmployeeGuid;
                    rs.Object[i].text = rs.Object[i].FullName;
                }
                ctrl.liEmployee = rs.Object;
            }
        })
    }
    $scope.IsActive = function () {
        $scope.Status = false;
        if ($scope.model.IsActive === true) {
            dataservice.CheckStatus($scope.model.ParentId, function (rs) {
                if (rs.length > 0) {
                    $scope.Status = true;
                    App.notifyDanger("Bạn không thể cập nhật trạng thái khi cấp trên không còn sử dụng");
                }
            })
        }
    }
    $scope.changeDepartment = function (d) {
        $scope.model.DepartmentGuid = d;
        if ($scope.model.DepartmentGuid !== null && $scope.model.DepartmentGuid !== undefined) {
            $scope.getEmployee($scope.model.DepartmentGuid);
        }
    }
    // Begin 2020/10/16
    ctrl.liDepartments = [];
    ctrl.loadCategoryShare = function () {
        dataservice.getDepart('', function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {
                if (rs.Object !== null) {
                    rs.Object = RecuDepartment([], rs.Object, null, '');
                    ctrl.liDepartments = rs.Object;
                    if (ctrl.model.DataCategoryShares && ctrl.model.DataCategoryShares.length > 0) {
                        if (ctrl.liDepartments && ctrl.liDepartments.length > 0) {
                            ctrl.model.DepartmentGuids = [];
                            for (var i = 0; i < ctrl.liDepartments.length; i++) {
                                for (var j = 0; j < ctrl.model.DataCategoryShares.length; j++) {
                                    if (ctrl.model.DataCategoryShares[j].DepartmentId.toUpperCase() === ctrl.liDepartments[i].DepartmentId.toUpperCase()) {
                                        ctrl.model.DepartmentGuids.push(ctrl.liDepartments[i]);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        })
    }
    // End 2020/10/16
    $scope.init = function () {
        ctrl.loadDepartment();
        dataservice.gitem({ IdS: [para] }, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {
                dataservice.gettreedata(rs.CategoryOfTaskGuid, function (result) {
                    $scope.treeData = result;
                });
                $scope.model = rs;
                ctrl.model.DepartmentGuidName = rs.DepartmentName;
                ctrl.model.EmployeeGuidName = rs.EmployeeName;
                $scope.liTaskProcess = rs.CategoryOfProcess;
                ctrl.loadCategoryShare();
                if (ctrl.model.TypeStep !== null && ctrl.model.TypeStep !== "") {
                    ctrl.model.IsActiveStep = true;
                }
                $scope.changeDepartment($scope.model.DepartmentGuid);
                ctrl.loadComboStep();
            }
        });
    }
    $scope.init();
    ctrl.addTaskProcess = function () {
        ctrl.liTaskProcess.push({
            OrderId: ctrl.liTaskProcess.length + 1,
        })
    }
    ctrl.delTaskProcess = function (i) {
        ctrl.liTaskProcess.splice(i, 1);
    }
    // Begin 2021/1/18
    ctrl.liTypeStep = [{
        value: 'DEP',
        text: 'Phòng ban',
    }, {
        value: 'EMP',
        text: 'Nhân viên',
    },];
    ctrl.loadComboStep = function () {
        switch (ctrl.model.TypeStep) {
            case 'DEP':
                ctrl.liTaskStepBySteps = ctrl.dDepartments;
                break;
            case 'EMP':
                ctrl.liTaskStepBySteps = ctrl.liEmployee;
                break;
            default:
                break;
        }
        if (ctrl.$root.$$phase !== '$apply' && ctrl.$root.$$phase !== '$digest') {
            ctrl.$apply();
        }
    }
    ctrl.onChangeStep = function () {
        ctrl.model.TypeStep = 'DEP';
        if (ctrl.model.IsActiveStep) {
            ctrl.addTaskStepBySteps();
        } else {
            ctrl.model.TaskStepBySteps = [];
        }
        ctrl.loadComboStep();
    }
    ctrl.initTypeStep = {
        change: ctrl.loadComboStep,
    };
    ctrl.delTaskStepBySteps = function (index) {
        ctrl.model.TaskStepBySteps.splice(index, 1);
        for (var i = 0; i < ctrl.model.TaskStepBySteps.length; i++) {
            ctrl.model.TaskStepBySteps[i].Sort = i + 1;
        }
    }
    ctrl.addTaskStepBySteps = function () {
        ctrl.model.TaskStepBySteps.push({
            Sort: ctrl.model.TaskStepBySteps.length + 1,
            Guid: null,
        });
    }
    // End 2021/1/18
});