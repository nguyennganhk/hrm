﻿
var ctxfolder = "/views/Sale/Announcements";
app.factory('dataservice', function ($http) {
    var headers = {
        "Content-Type": "application/json;odata=verbose",
        "Accept": "application/json;odata=verbose"
    };
    return {
        //------------------------*Store*-------------------------
        getById: function (data, callback) {
            $http.post('/Announcements/GetById/' + data).success(callback);
        },
        delete: function (data, callback) {
            $http.post('/Announcements/Remove?id=' + data).success(callback);
        },
        deleteItems: function (data, callback) {
            $http.post('/Announcements/RemoveItems/', data).success(callback);
        },
        getDepartment: function (callback) {
            $http.post('/Announcements/GetDepartment/').success(callback);
        },
        getLogin: function (callback) {
            $http.post('/Announcements/GetLogin').success(callback);
        },
        GetAttachments: function (data, callback) {
            $http.post('/Announcements/GetAttachments/', data).success(callback);
        },
        GetEmpAnnoucement: function (callback) {
            $http.get('/Announcements/GetEmpAnnoucement').success(callback);
        },
        GetBylistEmp: function (data, callback) {
            $http.post('/Announcements/GetBylistEmp/' + data).success(callback);
        },
        getDepartmentAll: function (callback) {
            $http.get('/Announcements/GetDepartmentAll').success(callback);
        },
        GetAllCommentPage: function (data, callback) {
            $http.post('/Announcements/GetAllCommentPage/', data).success(callback);
        },
        ListEmpAnnouncement_Update: function (data, callback) {
            $http.post('/Announcements/ListEmpAnnouncement_Update/', data).success(callback);
        },

    };

});
app.filter('html', ['$sce', function ($sce) {
    return function (text) {
        return $sce.trustAsHtml(text);
    };
}]);

app.directive('format', ['$filter', function ($filter) {
    return {
        require: '?ngModel',
        link: function (scope, elem, attrs, ctrl) {
            if (!ctrl) return;
            ctrl.$formatters.unshift(function (a) {
                return $filter(attrs.format)(ctrl.$modelValue);
            });
            ctrl.$parsers.unshift(function (viewValue) {
                var plainNumber = viewValue.replace(/[^\d|\-+|\.+]/g, '');
                elem.val($filter(attrs.format)(plainNumber));
                return plainNumber;
            });
        }
    };
}]);
//định dạng ngày
app.filter('fdate', [
    '$filter', function ($filter) {
        return function (input, f) {
            if (input && input.toString().indexOf('Date') > -1) {
                return moment(input).format(f);
            } if (input && input.toString().indexOf('T') > -1) {
                return moment(input).format(f);
            } else return input;
        };
    }
]);
function onlickFileComment(d) {
    var __el = document.getElementById('file-comment-id');
    __el.setAttribute('accept', '.rar, .zip');
    d === 1 ? __el.setAttribute('accept', 'video/mp4,video/x-m4v,video/*') : '';
    d === 2 ? __el.setAttribute('accept', '.jpg, .png, .jpeg, .gif, .bmp, .tif, .tiff|images/*') : '';
    __el.click();
}
app.controller('Ctrl_ES_VCM_Announcements', function ($scope, $rootScope, $sce, $compile, $uibModal, dataservice) {
    $rootScope.Title = "";
    $rootScope.go = function (path) {
        $location.path(path); return false;
    };

    $rootScope.validationOptions = {
        rules: {
            AnnouncementGuid: {
                required: true
            },
            Title: {
                required: true,
                maxlength: 255
            },
            Description: {
                maxlength: 255
            },
            StartDate: {
                required: true
            },
            EndDate: {
                required: true
            },
            Contents: {
                required: true
            },
            ExternalUrl: {
                maxlength: 255
            }
        },
        messages: {
            AnnouncementGuid: {
                required: "Yêu cầu nhập mã thông báo"
            },
            Title: {
                required: "Yêu cầu nhập tên thông báo.",
                maxlength: "Tên thông báo không vượt quá 255 ký tự"
            },
            Description: {
                maxlength: "Mô tả không vượt quá 255 ký tự."
            },
            StartDate: {
                required: "Yêu cầu nhập ngày bắt đầu"
            },
            EndDate: {
                required: "Yêu cầu nhập ngày kết thúc"
            },
            Contents: {
                required: "Yêu cầu nhập nội dung thông báo"
            },
            ExternalUrl: {
                maxlength: "Liên kết không vượt quá 255 ký tự."
            }
        }
    };
    $rootScope.ListDepartments = []; $rootScope.ListDepartmentsIndex = [];
    $rootScope.GetDepartment = function () {
        dataservice.getDepartment(function (result) {
            $rootScope.ListDepartments = result;
            $rootScope.ListDepartmentsIndex = result;
            $scope.ListDepartmentsIndex.push({
                'value': null,
                'text': 'Tất cả'
            });
            $scope.ListDepartmentsIndex.reverse();

        });
    };
    $rootScope.GetDepartment();
    dataservice.getLogin(function (result) {
        $rootScope.ListLogin = result;
        if (result !== null) {
            $rootScope.userLogin = result.EmployeeId;
            $rootScope.loginName = result.LoginName;
        } else {
            $rootScope.userLogin = "";
        }

    });

    dataservice.GetEmpAnnoucement(function (rs) {
        $rootScope.ListEmpAnnoucement = [];
        $rootScope.ListEmpAnnoucement.push({ value: null, text: "---Mời bạn chọn---" })
        for (var i = 0; i < rs.length; i++) {
            rs[i].value = rs[i].EmployeeGuid;
            rs[i].text = "[" + rs[i].EmployeeId + "] " + rs[i].FullName;
        }
        $rootScope.ListEmpAnnoucement = rs;
    });
    $rootScope.ListStatus = [{
        value: null,
        text: 'Chọn tất cả'
    }, {
        value: 'Y',
        text: 'Phát hành'
    }, {
        value: 'N',
        text: 'Chưa phát hành'
    }, {
        value: 'E',
        text: 'Hết hạn'
    }];
    $rootScope.StatusCombo = [{
        value: 'Y',
        text: 'Phát hành'
    }, {
        value: 'N',
        text: 'Chưa phát hành'
    }, {
        value: 'E',
        text: 'Hết hạn'
    }];
    $rootScope.ListIspriority = [{
        value: null,
        text: 'Chọn tất cả'
    }, {
        value: 'Q',
        text: 'Quan trọng'
    }, {
        value: 'B',
        text: 'Bình thường'
    }];
    $rootScope.StatusConfig = {
        placeholder: 'Trạng thái',
        search: true
    };
    $rootScope.LiIspriority = [{
        value: 'Q',
        text: 'Quan trọng'
    }, {
        value: 'B',
        text: 'Bình thường'
    }];
    $rootScope.StatusConfig = {
        placeholder: 'Trạng thái',
        search: true
    };
    $rootScope.IsPublicCombo = [{
        value: "1",
        text: 'Công khai'
    }, {
        value: "2",
        text: 'Nhân viên'
    },
    {
        value: "3",
        text: 'Phòng ban'
    }

    ];
    $rootScope.ListIsPublic = [{
        value: '',
        text: 'Chọn tất cả'
    }, {
        value: "1",
        text: 'Công khai'
    }, {
        value: "2",
        text: 'Nhân viên'
    }
      , {
        value: "3",
        text: 'Phòng ban'
    }

    ];
   
    $rootScope.IsActive = [{
        value: "N",
        text: 'Bản nháp'
    }, {
        value: "Y",
        text: 'Phát hành'
    }];
    $rootScope.ColumnMultiConfig = {
        selectAll: true,
        search: true,
        searchText: 'Tìm kiếm...',
        showTitle: 'true',
        csvSepChar: ',',
        captionFormat: '{0} lựa chọn',
        captionFormatAllSelected: '{0} lựa chọn',
        noMatch: 'Không có dữ liệu "{0}"',
        okCancelInMulti: true,
        locale: ['Đồng ý', 'Hủy', 'Chọn tất cả']
    };
    $rootScope.ColumnConfig = {
        search: true
    };
    $rootScope.CreateDate = function (date) {
        if (date !== null && date !== undefined) {
            var newdate = new Date(date);
            var month = newdate.getMonth() + 1;
            var day = newdate.getDate();
            var year = newdate.getFullYear();
            var hh = newdate.getHours();
            var mm = newdate.getMinutes();
            if (month < 10)
                month = "0" + month;
            if (day < 10)
                day = "0" + day;
            if (mm < 10)
                mm = "0" + mm;
            return todayDate = day + "/" + month + "/" + year;
        } else {
            return null;
        }
    };
    $rootScope.EditDate = function (datetime) {
        if (datetime !== null && datetime !== undefined) {
            var Str = datetime.toString();
            if (Str.indexOf("/Date") >= 0) {

                var newdate = new Date(parseInt(datetime.substr(6)));
                var month = newdate.getMonth() + 1;
                var day = newdate.getDate();
                var year = newdate.getFullYear();
                var hh = newdate.getHours();
                var mm = newdate.getMinutes();
                if (month < 10)
                    month = "0" + month;
                if (day < 10)
                    day = "0" + day;
                return year + "/" + month + "/" + day;
            } else {
                var value = datetime.split('/');

                return value[2] + '/' + value[1] + '/' + value[0];
            }
        }
        return null;
    };
    $rootScope.convertToJSONDate = function (datetime) {
        if (datetime !== null && datetime !== "") {
            var Str = datetime.toString();
            if (Str.indexOf("/Date") >= 0) {
                return Str;
            } else {
                var date = new Date(datetime);
                var month1 = date.getMonth() + 1;
                var day1 = date.getDate();
                var year1 = date.getFullYear();
                if (month1 < 10)
                    month1 = "0" + month1;
                if (day1 < 10)
                    day1 = "0" + day1;
                var newDate = new Date(year1 + '-' + month1 + '-' + day1);
                return '/Date(' + newDate.getTime() + ')/';
            }
        }
    };
    //Thêm phần tinymceOptions
    $rootScope.tinymceOptions = {
        selector: 'textarea',
        plugins: [
            'advlist autolink lists link charmap print preview hr anchor pagebreak image',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
        ],
        skin: 'lightgray', language: 'vi_VN',
        theme: 'modern', height: 70,
        toolbar1: 'codesample | bold italic sizeselect fontselect fontsizeselect | hr alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | insertfile undo redo | emoticons | code',
        fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
        toolbar2: 'print preview insert link image media | forecolor backcolor emoticons | codesample',
        image_advtab: true,

        file_browser_callback: function (field_name, url, type, win) {
            var connector = "/FileManager/Folder/File";
            var enableAutoTypeSelection = true;
            if (enableAutoTypeSelection && type) {
                connector += "&Type=" + type;
            }
            connector += "&Field=" + field_name;
            App.openPage(connector, "File Manager", 1150, 600);
        },
        relative_urls: false,
        remove_script_host: true,
        convert_urls: true
    };
    $rootScope.CalenderReturnDate = function (item) {
        //xác định tuần hiện tại
        var d = new Date();
        if (item === '0') {
            var day = d.getDay(),
                fday = d.getDate() - day,
                lday = fday + 6;
            var data = {
                sdate: moment(new Date(d.getFullYear(), d.getMonth(), fday)).format(),
                edate: moment(new Date(d.getFullYear(), d.getMonth(), lday)).format()
            };
            return data;
        }
        //Tháng Hiện tại
        if (item === '1') {
            var data1 = {
                sdate: moment(new Date(d.getFullYear(), d.getMonth(), 1)).format(),
                edate: moment(new Date(d.getFullYear(), d.getMonth() + 1, 0)).format()
            };
            return data1;
        }
        //Quý Hiện tại
        if (item === '2') {
            var motnth = d.getMonth();
            var year = d.getFullYear();
            if (d.getMonth() === 0 || d.getMonth() === 1 || d.getMonth() === 2) {
                motnth = 1;
                year = year;
            }
            else if (d.getMonth() === 3 || d.getMonth() === 4 || d.getMonth() === 5)
                motnth = 4;
            else if (d.getMonth() === 6 || d.getMonth() === 7 || d.getMonth() === 8)
                motnth = 7;
            else
                motnth = 10;
            var data2 = {
                sdate: moment(new Date(year, motnth - 1, 1)).format(),
                edate: moment(new Date(year, motnth + 2, 0)).format()
            };
            return data2;
        }
        //Năm Hiện tại
        if (item === '3') {
            var data3 = {
                sdate: moment(new Date(d.getFullYear(), 0, 1)).format(),
                edate: moment(new Date(d.getFullYear(), 12, 0)).format()
            };
            return data3;
        }
        //xác định tuần trước
        var d4 = new Date();
        if (item === '4') {
            var day4 = d4.getDay(),
                fday4 = d4.getDate() - day4 + (day4 === 0 ? -6 : 1) - 7,
                lday4 = d4.getDate() - day4;
            var data4 = {
                sdate: moment(new Date(d4.getFullYear(), d4.getMonth(), fday4)).format(),
                edate: moment(new Date(d4.getFullYear(), d4.getMonth(), lday4)).format()
            };
            return data4;
        }
        //Tháng trước
        if (item === '5') {
            var data5 = {
                sdate: moment(new Date(d.getFullYear(), d.getMonth() - 1, 1)).format(),
                edate: moment(new Date(d.getFullYear(), d.getMonth(), 0)).format()
            };
            return data5;
        }
        //Quý trước
        if (item === '6') {
            var motnth6 = d.getMonth();
            var year6 = d.getFullYear();
            if (d.getMonth() === 0 || d.getMonth() === 1 || d.getMonth() === 2) {
                motnth6 = 10;
                year6 = year6 - 1;
            }
            else if (d.getMonth() === 3 || d.getMonth() === 4 || d.getMonth() === 5)
                motnth6 = 1;
            else if (d.getMonth() === 6 || d.getMonth() === 7 || d.getMonth() === 8)
                motnth6 = 4;
            else
                motnth6 = 7;
            var data6 = {
                sdate: moment(new Date(year6, motnth6 - 1, 1)).format(),
                edate: moment(new Date(year6, motnth6 + 2, 0)).format()
            };
            return data6;
        }
        //Năm trước
        if (item === '7') {
            var data7 = {
                sdate: moment(new Date(d.getFullYear() - 1, 0, 1)).format(),
                edate: moment(new Date(d.getFullYear() - 1, 12, 0)).format()
            };
            return data7;
        }
    };
    $rootScope.ConDate = function (data, number) {
        try {
            if (data == null || data == "") {
                return '';
            }
            if (data !== null && data != "" && data != undefined) {
                try {
                    if (data.indexOf("SA") != -1 || data.indexOf("CH") != -1) {
                        if (data.indexOf("SA") != -1) { }
                        if (data.indexOf("CH") != -1) { }
                    }

                    if (data.indexOf('Date') != -1) {
                        data = data.substring(data.indexOf("Date") + 5);
                        data = parseInt(data);
                    }
                }
                catch (ex) { }
                var newdate = new Date(data);
                if (number == 3) {
                    return newdate;
                }
                if (number == 2) {
                    return "/Date(" + newdate.getTime() + ")/";
                }
                var month = newdate.getMonth() + 1;
                var day = newdate.getDate();
                var year = newdate.getFullYear();
                var hh = newdate.getHours();
                var mm = newdate.getMinutes();
                if (month < 10)
                    month = "0" + month;
                if (day < 10)
                    day = "0" + day;
                if (mm < 10)
                    mm = "0" + mm;
                if (number == 0) {
                    return todayDate = day + "/" + month + "/" + year;
                }
                if (number == 1) {
                    return todayDate = day + "/" + month + "/" + year + " " + hh + ":" + mm;
                }
                if (number == 4) {
                    return new Date(year, month - 1, day);
                }
            } else {
                return '';
            }
        } catch (ex) {
            return "";
        }
    }
});
app.config(function ($routeProvider, $validatorProvider) {
    $routeProvider
        .when('/', {
            templateUrl: ctxfolder + '/index.html',
            controller: 'index'
        })
        .when('/edit/:id', {
            templateUrl: ctxfolder + '/edit.html',
            controller: 'edit'
        })
        .when('/add/', {
            templateUrl: ctxfolder + '/add.html',
            controller: 'add'
        });
    $validatorProvider.setDefaults({
        errorElement: 'span',
        errorClass: 'help-block',
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        success: function (label) {
            label.closest('.form-group').removeClass('has-error');
        }
    });
});
//controller hiển thị, tìm kiếm
app.controller('index', function ($scope, $rootScope, $compile, $http, $confirm, $ngConfirm, $uibModal, DTOptionsBuilder, DTColumnBuilder, DTInstances, dataservice, utility) {
    $scope.utility = utility;
    $rootScope.TableName = "/Announcements";
    $scope.model = {};
    var vm = $scope;
    var ctrl = $scope;
    $scope.staticParam = {
        totalItems: 0,
        currentPage: 1,
        pageSize: 25,
        maxSize: 5,
        Search: "",
        IsPublic: null,
        Status: null,
        StartDate: $rootScope.convertToJSONDate($rootScope.CalenderReturnDate("1").sdate),
        EndDate: $rootScope.convertToJSONDate($rootScope.CalenderReturnDate("1").edate),
        Ispriority: null,
        DepartmentGuid: "",
        RowGuid: ""
    };
    ctrl.changeOptionDate = function (d) {
        if (d.length === 0) {
            ctrl.staticParam.StartDate = null;
            ctrl.staticParam.EndDate = null;
        } else {
            ctrl.staticParam.StartDate = $rootScope.ConDate(d[0], 2);
            ctrl.staticParam.EndDate = $rootScope.ConDate(d[1], 2);
        }
    }
    $scope.$watch('staticParam.StarDate', function (newvalue, oldvalue) {
        if (newvalue !== undefined && newvalue !== oldvalue) {
            reloadData(true);
        }
    });
    $scope.$watch('staticParam.EndDate', function (newvalue, oldvalue) {

        if (newvalue !== undefined && newvalue !== oldvalue) {
            reloadData(true);
        }
    });
    $rootScope.EmpConfig = {
        placeholder: 'Chọn phòng ban',
        search: true,
        change: function () {
            $scope.reload();
        }
    };
    dataservice.getDepartmentAll(function (result) {
        $rootScope.ListDepartmentGuid = result;
    });
    $rootScope.ListDepartmentGuid = [];
    $scope.ListDepartment = [];
   
    $scope.StatusChange = function (data) {
        $scope.staticParam.Status = data !== 'null' ? data : "";
        $scope.selectAll = false;
        $scope.Search();
    };
    $scope.IsPublicChange = function (data) {
        $scope.staticParam.IsPublic = data !== 'null' ? data : "";
        $scope.selectAll = false;
        $scope.Search();
    };
    $scope.IspriorityChange = function (data) {
        $scope.staticParam.Ispriority = data !== 'null' ? data : "";
        $scope.selectAll = false;
        $scope.Search();
    };
    
    $scope.selected = [];
    $scope.selectAll = false;
    $scope.toggleOne = toggleOne;
    $scope.toggleAll = toggleAll;
    $scope.$watch('staticParam.StartDate', function (newvalue, oldvalue) {
        if (newvalue !== oldvalue) {
            $scope.staticParam.StartDate = newvalue;
            if (parseInt(moment(newvalue).format("YYYYMMDD")) > parseInt(moment($scope.staticParam.EndDate).format("YYYYMMDD"))) {
                App.notifyDanger('Yêu cầu ngày bắt đầu nhỏ hơn ngày kết thúc');
                $scope.staticParam.StartDate = oldvalue;
                return;
            } else {
                $scope.reload(true);
            }
        }
    });
    $scope.$watch('staticParam.EndDate', function (newvalue, oldvalue) {
        if (newvalue !== oldvalue) {
            $scope.staticParam.EndDate = newvalue;
            if (parseInt(moment(newvalue).format("YYYYMMDD")) < parseInt(moment($scope.staticParam.StartDate).format("YYYYMMDD"))) {
                App.notifyDanger('Yêu cầu ngày bắt đầu nhỏ hơn ngày kết thúc');
                $scope.staticParam.EndDate = oldvalue;
                return;
            } else {
                $scope.reload(true);

            }
        }
    });
    var titleHtml = '<label style="text-align: center;" class="mt-checkbox" ><input type="checkbox" ng-model="selectAll" ng-click="toggleAll(selectAll, selected)"/><span></span></label>';
    //if (App.Permissions.LISTVIEW)
    vm.dtOptions = DTOptionsBuilder.newOptions()
        .withPaginationType('full_numbers')
        .withDataProp('data').withDisplayLength(15)
        .withOption('order', [6, 'desc'])
        .withOption('serverSide', true)
        .withOption('pageLength', 25)
        .withOption('info', false)
        .withOption('stateLoadParams', function (settings, data) {
            data.search.search = '';
        })
        .withOption('headerCallback', function (header) {
            if (!$scope.headerCompiled) {
                $scope.headerCompiled = true;
                $compile(angular.element(header).contents())($scope);
            }
        })
        .withOption('initComplete', function (settings, json) {
            //thêm sự kiện scroll phân trang
            //  $('#tblData').parent().attr("onscroll", "angular.element(this).scope().LoadScroll(this)");
            PLUGIN_DATATABLE_TABLE_SCROLL.INIT_EVENT('tblData', $scope.LoadScroll);

        })
        .withOption('createdRow', function (row, data, dataIndex) {
            const contextScope = $scope.$new(true);
            contextScope.data = data;
            $scope.data = data;
            $compile(row)($scope);
            contextScope.contextMenu = $scope.contextMenu;
            $compile(angular.element(row).find('input'))($scope);
            $compile(angular.element(row).attr('context-menu', 'contextMenu'))(contextScope);
        });
    // if (App.Permissions.LISTVIEW)
    vm.dtOptions.withOption('ajax', function (data, callback, settings) {
        data.search.value = $scope.staticParam.Search;
        data.StartDate = $scope.staticParam.StartDate;
        data.EndDate = $scope.staticParam.EndDate;
        data.DepartmentGuid = $scope.staticParam.DepartmentGuid != "" && $scope.staticParam.DepartmentGuid != "null" ? $scope.staticParam.DepartmentGuid : null;
        data.Ispriority = $scope.staticParam.Ispriority;
        data.Status = $scope.staticParam.Status != "" && $scope.staticParam.Status != "null" ? $scope.staticParam.Status : null;       
        data.IsPublic = $scope.staticParam.IsPublic != "" && $scope.staticParam.IsPublic != "null" ? $scope.staticParam.IsPublic : null  ;
        
        App.blockUI({
            target: "#tblData",
            boxed: true,
            message: 'Đang tải...'
        });
        $http.post('/Announcements/JTable', data)
            .success(function (res) {
                $scope.selected = [];
                if (data.start === 0) {
                    $scope.dataload = [];
                    angular.forEach(res.data, function (val, key) {
                        $scope.dataload.push(val);
                    });
                    callback({
                        data: $scope.dataload,
                        recordsTotal: res.recordsTotal,
                        recordsFiltered: res.recordsFiltered
                    });
                } else {
                    if (res.recordsTotal > $scope.dataload.length) {
                        angular.forEach(res.data, function (val, key) {
                            $scope.dataload.push(val);
                        });
                        callback({
                            data: $scope.dataload,
                            recordsTotal: res.recordsTotal,
                            recordsFiltered: res.recordsFiltered
                        });
                    }
                }
                PLUGIN_DATATABLE_TABLE_SCROLL.UNLOCK_EVENT('tblData');
                App.unblockUI("#tblData");
            });
    });
    //scroll theo tỷ lệ màn hình
    vm.dtOptions.withOption('scrollY', '70vh')
        .withOption('scrollX', '100%') //mặc định 100%
        .withOption('scrollCollapse', true) //xóa khoảng trắng khi dữ liệu trong bảng ít
        .withOption('rowCallback', rowCallback) //sự kiên click vào 1 dòng
        .withOption('scroller', {
            loadingIndicator: true
        });
    //.withFixedColumns({
    //    leftColumns: 0 //fix bên trái 2 cột
    //    //rightColumns: 1 //fix bên phải 1 cột
    //}
    //);
    //table
    vm.dtColumns = [];
    vm.dtColumns.push(DTColumnBuilder.newColumn("AnnouncementGuid").withTitle(titleHtml).notSortable().renderWith(function (data, type, full, meta) {
        $scope.selected[full.AnnouncementGuid] = false;
        return '<label class="mt-checkbox"><input type="checkbox" ng-model="selected[\'' + full.AnnouncementGuid + '\']" ng-click="toggleOne(selected)"/><span></span></label>';
    }).withOption('sWidth', '5px').withOption('sClass', 'tcenter tcenter-header'));
    vm.dtColumns.push(DTColumnBuilder.newColumn('AnnouncementGuid').withTitle('STT').notSortable().withOption('sWidth', '5px').renderWith(function (data, type, full, meta) {
        return meta.row + 1;
    }).withOption('sClass', 'tcenter tcenter-header'));
    vm.dtColumns.push(DTColumnBuilder.newColumn('Title').withTitle('Tên thông báo').withOption('sWidth', '350px').withOption('sClass', 'tcenter-header').renderWith(function (data, type, full, meta) {
        return '<a  style="color: #4285f4;" href="" ng-click="open(\'' + full.AnnouncementGuid + '\')" >' + data + '</a>';
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('DepartmentName').withTitle('Phòng ban').withOption('sWidth', '150px').withOption('sClass', 'tcenter-header').renderWith(function (data, type, full, meta) {
        return data;
    }));
    //vm.dtColumns.push(DTColumnBuilder.newColumn('StartDateString').withTitle('Ngày bắt đầu').withOption('sClass', 'mw250').withOption('sClass', 'es-tright tcenter-header').renderWith(function (data, type, full, meta) {
    //    return data;
    //}));
    //vm.dtColumns.push(DTColumnBuilder.newColumn('EndDateString').withTitle('Ngày kết thúc').withOption('sClass', 'mw250').withOption('sClass', 'es-tright tcenter-header').renderWith(function (data, type, full, meta) {
    //    return data;
    //}));
    vm.dtColumns.push(DTColumnBuilder.newColumn('IsPublic').notSortable().withTitleDescription('Loại thông báo').withTitle('Loại thông báo').withOption('sClass', 'tcenter-header').withOption('sWidth', '60px').renderWith(function (data, type, full) {
        if (data === '1') {
            return "Công khai";
        }
        else if (data === '2') {
            return "Nhân viên";
        }
        else if (data === '3') {
            return "Phòng ban";
        }
        else {
            return "";
        }
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('Status').notSortable().withTitleDescription('Trạng thái').withTitle('Trạng thái').withOption('sClass', 'tcenter-header').withOption('sWidth', '80px').renderWith(function (data, type, full) {
        if (data === "Y") {
            return "Phát hành";
        }
        if (data === "N") {
            return "Chưa phát hành";
        }
        if (data === "E") {
            return "Hết hạn";
        }
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('StartDate').withTitle('Ngày bắt đầu').withOption('sClass', 'mw130').withOption('sWidth', '60px').withOption('sClass', 'tcenter').renderWith(function (data, type, full, meta) {
        return full.StartDateString;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('FullName').withTitle('Người tạo').withOption('sWidth', '80px').withOption('sClass', 'tcenter-header').renderWith(function (data, type, full, meta) {
        return data;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('Status').withTitle('Mức độ ưu tiên').notSortable().withOption('sWidth', '60px').withOption('sClass', 'tcenter').renderWith(function (data, type, full, meta) {
        if (full.Ispriority === 'Q') {
            return '<span class="badge badge-sm badge-danger badge-rounded">Quan trọng</span >';
        } if (full.Ispriority === "B") {
            return "Bình thường";
        }
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('AnnouncementGuid').notSortable().withTitle('Điều khiển').notSortable().withOption('sWidth', '80px').withOption('sClass', 'tcenter').renderWith(function (data, type, full, meta) {
        var tb = ""; var tb1 = "";
        if (full.IsRead == "False") {
            tb = "Chưa";
             tb1 = '<a ng-click="open(\'' + full.AnnouncementGuid + '\')" class="table__cell-actions-item table__cell-actions-icon"><span class="navbar-notify es-cursor" ><span><span class="navbar-notify__icon mdi mdi-eye"></span></span><span class="navbar-notify__amount">' + tb + '</span ></span ></a > ';
        } else {
             tb1 = '<a ng-click="open(\'' + full.AnnouncementGuid + '\')" class="table__cell-actions-item table__cell-actions-icon"><span class="navbar-notify es-cursor" ><span><span class="navbar-notify__icon mdi mdi-eye"></span></span></a > ';
        }        
        return '<div class="table__cell-actions-wrap">' +               
            tb1
            +
            '<a href="javascript:;" ng-click="edit(\'' + full.AnnouncementGuid + '\',\'' + full.PermissEdit + '\')" class="table__cell-actions-item table__cell-actions-icon es-cursor"> <span class="mdi mdi-table-edit" title="Sửa"></span> </a>' +
            '<a ng-click="delete (\'' + full.AnnouncementGuid + '\',\'' + full.PermissEdit + '\')" class="table__cell-actions-item table__cell-actions-icon"><span class="mdi mdi-delete" title="Xóa"></span> </a>'
            +
            '<a ng-click="comment(\'' + full.AnnouncementGuid + '\')" class="table__cell-actions-item table__cell-actions-icon"><span class="navbar-notify es-cursor" ><span><span class="navbar-notify__icon mdi mdi-comment-multiple-outline"></span></span><span class="navbar-notify__amount">' + full.CountComment + '</span></span ></a > ';
       
    }));
    vm.reloadData = reloadData;
    vm.dtInstance = {};
    function reloadData(resetPaging) {
        $scope.staticParam.currentPage = resetPaging;
        vm.dtInstance.reloadData(callback, resetPaging);
    }
    function callback(json) {

    }
    function toggleOne(selectedItems) {
        for (var id in selectedItems) {
            if (selectedItems.hasOwnProperty(id)) {
                if (!selectedItems[id]) {
                    vm.selectAll = false;
                    return;
                }
            }
        }
        vm.selectAll = true;
    }
    function toggleAll(selectAll, selectedItems) {
        for (var id in selectedItems) {
            if (selectedItems.hasOwnProperty(id)) {
                selectedItems[id] = selectAll;
            }
        }
    }
    function rowCallback(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        $('td', nRow).unbind('dblclick');
        $('td', nRow).bind('dblclick', function () {
            $scope.$apply(function () {
                $scope.open(aData.AnnouncementGuid);
            });
        });
        return nRow;

    }
    //Load dữ liệu cuộn
    $scope.total = 0;
    $scope.check = false;
    $scope.LoadScroll = function (obj) {
        vm.dtInstance.DataTable.page('next').draw('page');
    };
    //tìm kiếm
    $scope.reload = function () {
        $rootScope.IsPublic = $scope.staticParam.IsActive;
        $rootScope.Status = $scope.staticParam.Status;
        $scope.dtInstance.DataTable.search($scope.staticParam.DepartmentGuid);
        $scope.dtInstance.DataTable.search($scope.staticParam.Search);
        $scope.dtInstance.DataTable.search($scope.staticParam.Search).draw();
    };
    $scope.reloadAll = function () {
        $scope.staticParam.Search = "";
        $scope.selectAll = false;
        $scope.staticParam.IsPublic = null;
        $scope.staticParam.Status = null;
        $scope.staticParam.Ispriority = null;
        $scope.staticParam.DepartmentGuid = "";
        reloadData(true);
        $scope.dtInstance.DataTable.search($scope.staticParam.Search);
        $scope.dtInstance.DataTable.search($scope.staticParam.Search).draw();
        $scope.staticParam.StartDate = $rootScope.convertToJSONDate($rootScope.CalenderReturnDate("1").sdate);
        $scope.staticParam.EndDate = $rootScope.convertToJSONDate($rootScope.CalenderReturnDate("1").edate);
    };
    $rootScope.Search = function () {
        reloadData(true);
    };
    // thêm mới
    $scope.add = function () {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/add.html',
            controller: 'add',
            backdrop: 'static',
            keyboard: false,
            size: '80'
        });
        modalInstance.result.then(function (d) {
        }, function () {
        });
    };
    // chi tiết
    $scope.open = function (temp) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/open.html',
            controller: 'open',
            backdrop: 'static',
            size: '80',
            resolve: {
                para: function () {
                    return temp;
                }
            }
        });
        modalInstance.result.then(function (d) {
        }, function () {
        });

    };
    // sửa 
    $scope.edit = function (temp, PermissEdit) {
        if (PermissEdit === "1") {
        } else {
            App.notifyDanger("Bạn không có quyền sửa thông báo này");
            return;
        }
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/edit.html',
            controller: 'edit',
            backdrop: 'static',
            size: '80',
            resolve: {
                para: function () {
                    return temp;
                }
            }
        });
        modalInstance.result.then(function (d) {
            $rootScope.Search();
        }, function () {

        });

    };
    //Xóa 1 bản ghi
    $scope.delete = function (temp, PermissEdit) {
        if (PermissEdit === "1") {
        } else {
            App.notifyDanger("Bạn không có quyền xóa thông báo này");
            return;
        }
        dataservice.getById(temp, function (rs) {
            ESConfirm([{ 'value': 1, 'title': 'Xác nhận', 'type': 'info' }, { 'value': 2, 'title': 'Đóng', 'type': 'danger' }], { 'description': 'Bạn có muốn xóa ' + rs.Title, 'class': 'eswarning_v2' }, function (rs) {
                if (rs === '1') {
                    App.blockUI({
                        target: "#contentMain",
                        boxed: true,
                        message: 'Đang tải...'
                    });
                    dataservice.delete(temp, function (result) {
                        if (result.Error) {
                            App.notifyDanger(result.Title);
                        } else {
                            App.notifyInfo(result.Title);
                            $rootScope.Search();
                        }
                        App.unblockUI("#contentMain");
                    });
                }
            }, function () {
            });
        });
    };
    // xóa nhiều bản ghi
    $scope.deleteChecked = function () {

        var deleteItems = [];
        for (var id in $scope.selected) {
            if ($scope.selected.hasOwnProperty(id)) {
                if ($scope.selected[id]) {
                    deleteItems.push(id);
                }
            }
        }
        if (deleteItems.length > 0) {
            ESConfirm([{ 'value': 1, 'title': 'Xác nhận', 'type': 'info' }, { 'value': 2, 'title': 'Đóng', 'type': 'danger' }], { 'description': 'Bạn có muốn xóa những thông báo này?', 'class': 'eswarning_v2' }, function (rs) {
                if (rs === '1') {
                    App.blockUI({
                        target: "#contentMain",
                        boxed: true,
                        message: 'Đang tải...'
                    });
                    dataservice.deleteItems(deleteItems, function (result) {
                        if (result.Error) {
                            App.notifyDanger(result.Title);
                        } else {
                            App.notifyInfo(result.Title);
                            $scope.selectAll = false;
                            $rootScope.Search();
                        }
                        App.unblockUI("#contentMain");
                    });
                }
            }, function () {
            });
        } else {
            App.notifyDanger("Không có bản ghi nào được chọn");
        }
    };
    //thay đổi trạng thái
    var da = false;
    $scope.checkbox = function (Id, IsActive, Name) {
        var _item = [];

        if (IsActive === "True") {
            _item.push({ Id: Id, IsActive: false });
        } else if (IsActive === "False") {
            _item.push({ Id: Id, IsActive: true });
        }
        else {
            _item.push({ Id: Id, IsActive: false });
        }
        if (da === false) {
            da = true;
            dataservice.update(_item, function (result) {
                if (result.Error) {
                    App.notifyDanger(result.Title);
                } else {
                    da = false;
                    App.notifyInfo(result.Title);
                    $rootScope.Search();
                }
                App.unblockUI("#contentMain");
            });
        }

    };
    //thêm sưa xóa menu chuột phải
    $scope.contextMenu = [
        //xem chi tiết trong menu
        [function ($itemScope) {
            return '<i class="mdi mdi-eye"></i> Xem thông báo';
        }, function ($itemScope, $event, model) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: ctxfolder + '/open.html',
                controller: 'open',
                backdrop: true,
                size: '80',
                resolve: {
                    para: function () {
                        return $itemScope.data.AnnouncementGuid;
                    }
                }
            });
            modalInstance.result.then(function (d) {
                $rootScope.Search();
            }, function () {
            });
        }],
        //sửa phiếu
        [function ($itemScope) {
            return '<i class="mdi mdi-table-edit"></i> Sửa thông báo';
        }, function ($itemScope, $event, model) {
            if ($itemScope.data.PermissEdit === "1") {
            } else {
                App.notifyDanger("Bạn không có quyền sửa thông báo này");
                return;
            }
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: ctxfolder + '/edit.html',
                controller: 'edit',
                backdrop: 'static',
                keyboard: false,
                size: '80',
                resolve: {
                    para: function () {
                        return $itemScope.data.AnnouncementGuid;
                    }
                }
            });
            modalInstance.result.then(function (d) {
                $rootScope.Search();
            }, function () {

            });
        }, function ($itemScope, $event, model) {

            return true;

        }],
        //xóa trong menu
        [function ($itemScope) {
            return '<i class="mdi mdi-delete"></i> Xóa thông báo';
        }, function ($itemScope, $event, model) {
            if ($itemScope.data.PermissEdit === "1") {
            } else {
                App.notifyDanger("Bạn không có quyền xóa thông báo này");
                return;
            }
            ESConfirm([{ 'value': 1, 'title': 'Xác nhận', 'type': 'info' }, { 'value': 2, 'title': 'Đóng', 'type': 'danger' }], { 'description': 'Bạn có muốn xóa ' + $itemScope.data.Title + ' không?', 'class': 'eswarning_v2' }, function (rs) {
                if (rs === '1') {
                    App.blockUI({
                        target: "#contentMain",
                        boxed: true,
                        message: 'Đang tải...'
                    });
                    dataservice.delete($itemScope.data.AnnouncementGuid, function (result) {
                        if (result.Error) {
                            App.notifyDanger(result.Title);
                        } else {
                            App.notifyInfo(result.Title);
                            $rootScope.Search();
                        }
                        App.unblockUI("#contentMain");
                    });
                }
            }, function () {
                //some error in this function
                console.log('Không lựa chọn');
            });
        }, function ($itemScope, $event, model) {

            return true;

        }],
        [function ($itemScope) {
            return '<i class="mdi mdi-comment-multiple-outline"></i> Ý Kiến';
        }, function ($itemScope, $event, model) {
            loadTMStart();
            vm.staticParam.RowGuid = $itemScope.data.AnnouncementGuid;
            $scope.txtComment = $itemScope.data.Title;
            $scope.liComment = [];
            $scope.initPara = { Page: 1, ItemPage: 15, RecordGuid: vm.staticParam.RowGuid };
            $scope.loadDataComment();
        }],
    ];

    //Comment
    $scope.comment = function (temp) {
        dataservice.getById(temp, function (rs) {
            loadTMStart();
            vm.staticParam.RowGuid = temp;
            $scope.txtComment = rs.Title;
            $scope.liComment = [];
            $scope.initPara = { Page: 1, ItemPage: 15, RecordGuid: temp };
            $scope.loadDataComment();
        });

    };

    function loadTMStart() {
        setTimeout(_loadTMStart, 100);
    }
    function _loadTMStart() {
        var _el = document.getElementsByClassName('settings-panel')[0];
        _el.classList.add('is-opened');
    }
    $scope.close = function () {
        var _el = document.getElementsByClassName('settings-panel')[0];
        _el.classList.remove('is-opened');
    }
    $scope.model = {};
    $scope.initPara = { Page: 1, ItemPage: 15, RecordGuid: null };
    $scope.txtComment = "";
    $scope.liComment = [];
    $scope.check_commnent = false;
    $scope.total_comment = 0;
    $scope.LoadScroll_commnet = function (obj) {

        var total = obj.offsetHeight + obj.scrollTop;
        if (obj.offsetHeight + obj.scrollTop >= obj.scrollHeight && $scope.check_commnent && $scope.total_comment < total) {
            $scope.check_commnent = false;
            try {
                $scope.IconLoad = true;
                $scope.initPara.Page = $scope.initPara.Page + 1;
                $scope.loadDataComment();
            }
            catch (ex) {
            }
        }
        if (obj.offsetHeight + obj.scrollTop >= obj.scrollHeight) {
            $scope.total_comment = obj.offsetHeight + obj.scrollTop;
        }
        else {
            $scope.check_commnent = true;
        }
    }
    $scope.liComment = [];
    $scope.loadDataComment = function () {
        if ($scope.initPara.RecordGuid !== "" && $scope.initPara.RecordGuid !== null && $scope.initPara.RecordGuid !== undefined) {
            dataservice.GetAllCommentPage($scope.initPara, function (rs) {
                if (rs.Error) {
                    App.notifyDanger(rs.Title);
                } else {
                    $scope.IconLoad = false;
                    for (var i = 0; i < rs.Object.length; i++) {
                        $scope.liComment.unshift(rs.Object[i]);
                    }
                    $scope.total_comment = $scope.liComment.length;

                    angular.forEach($scope.liComment, function (value, key) {
                        if (value.CreatedBy.split('#')[0] === $rootScope.loginName) {
                            value.Class = "is-interlocutor";
                        } else {
                            value.Class = "is-self";
                        }

                        if (value.CommentAttachments !== null) {
                            var listAttachments = [];
                            if (value.CommentAttachments.includes('|')) {
                                var data = value.CommentAttachments.split('|');

                                var obj = {
                                    AttachmentGuid: data[0],
                                    FileName: data[1],
                                    checksrc: false
                                };
                                if (data[1].includes('.')) {
                                    obj.FileExtension = data[1].split('.')[1];
                                } else {
                                    obj.FileExtension = data[1];
                                }
                                if (!obj.FileExtension.includes('xlsx') && !obj.FileExtension.includes('xls') && !obj.FileExtension.includes('png') && !obj.FileExtension.includes('docx') && !obj.FileExtension.includes('doc') && !obj.FileExtension.includes('pdf') && !obj.FileExtension.includes('txt') && !obj.FileExtension.includes('png') && !obj.FileExtension.includes('jpg')) {
                                    obj.checksrc = false;
                                } else {
                                    obj.checksrc = true;
                                }
                                listAttachments.push(obj);
                            } else {
                                var data1 = value.CommentAttachments;
                                angular.forEach(data1, function (val, keys) {
                                    var obj = {
                                        AttachmentGuid: val.AttachmentGuid,
                                        FileName: val.FileName
                                    }
                                    if (val.FileExtension.includes('.')) {
                                        obj.FileExtension = val.FileExtension.split('.')[1];
                                    } else {
                                        obj.FileExtension = val.FileExtension;
                                    }
                                    if (!obj.FileExtension.includes('xlsx') && !obj.FileExtension.includes('xls') && !obj.FileExtension.includes('png') && !obj.FileExtension.includes('docx') && !obj.FileExtension.includes('doc') && !obj.FileExtension.includes('pdf') && !obj.FileExtension.includes('txt') && !obj.FileExtension.includes('png') && !obj.FileExtension.includes('jpg')) {
                                        obj.checksrc = false;
                                    } else {
                                        obj.checksrc = true;
                                    }
                                    listAttachments.push(obj);
                                });
                            }
                            value.CommentAttachments = listAttachments;

                        }
                    });
                    gotoBottom('setting-content-scroll');
                }
            })
        }

    }
    $scope.loadDataComment();

    $scope.insertcomment = function () {
        if ($scope.model.Comment === undefined || $scope.model.Comment === "") {
            App.notifyDanger("Bạn phải nhập ý kiến");
            return true;
        }
        var fd = new FormData();
        $scope.model.RecordGuid = $scope.initPara.RecordGuid;
        var file = $('#file-comment-id').prop('files')[0];
        var message = $('#message').val().replace(/(\n)+/g, '<br />');
        $scope.model.Comment = message;
        fd.append('insert', JSON.stringify($scope.model));
        fd.append('file', file);
        $.ajax({
            type: "POST",
            url: "/Announcements/InsertComments",
            contentType: false,
            processData: false,
            data: fd,
            success: function (rs) {
                if (rs.Error) {
                    App.notifyDanger(rs.Title);
                }
                else {
                    $('#message').val('');
                    $scope.model.Comment = "";
                    $scope.NameFile = undefined;
                    $("#file-comment-id").val('');
                    $scope.liComment = [];
                    $scope.initPara = { Page: 1, ItemPage: 15, RecordGuid: $scope.initPara.RecordGuid };
                    $scope.loadDataComment();
                }
            }
        });
    }
    $scope.FileNameChnage = '';
    $scope.addNameFile = function () {
        if ($('#file-comment-id').prop('files')[0] !== undefined)
            if ($scope.model !== undefined && $scope.model.Comment !== '') {
                $scope.model.Comment += " " + $('#file-comment-id').prop('files')[0].name;
            } else {
                $scope.model.Comment = " " + $('#file-comment-id').prop('files')[0].name;
            }
        if (!$scope.$$phase) {
            $scope.$apply();
        }
    }

    //End comment



});
function gotoBottom(id) {
    var element = document.getElementById(id);
    element.scrollTop = 10;/* element.scrollHeight - element.clientHeight;*/
}
// controller Add
app.controller('add', function ($scope, $rootScope, $uibModalInstance, FileUploader) {
    $scope.Title = "Thêm mới thông báo";
    $scope.model = { DepartmentGuid: $rootScope.ListLogin.DepartmentGuid, IsPublic: '1', Status: "Y", IsActive: "Y", Ispriority: "B" };
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
    $scope.ListReplate = {
        EmployeeGuid: [],
        EmployeeName: null,
    }

    $scope.changeEmpAnnoucement = function (data) {
        $scope.ListReplate.EmployeeGuid = data;
        var _data = '';
        for (var i = 0; i < data.length; i++) {
            _data += data[i] + ';';
        }
    }
    $scope.ChangeIsPublic = function (data) {
        $scope.model.IsPublic = parseInt(data);
        if ($scope.model.IsPublic === 2) {
            $scope.isShow = true;
        } else {
            $scope.isShow = false;
        }
        if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
            $scope.$apply();
        }
    };
   
    $scope.ChangeIsActive = function (data) {
        $scope.model.IsActive = data;
    };
    $scope.ChangeStatus = function (data) {
        $scope.model.Status = data;
    };
    $scope.ChangeIspriority = function (data) {
        $scope.model.Ispriority = data;
    };
    //Cấu hình file đính kèm
    $scope.model.FileAttachments = [];
    var uploader = $scope.uploader = new FileUploader({
        // url: App.baseService() + '/upload'
    });
    uploader.filters.push({
        name: 'customFilter',
        fn: function (item /*{File|FileLikeObject}*/, options) {
            return this.queue.length < 10;
        }
    });
    uploader.onSuccessItem = function (fileItem, response, status, headers) {
        $scope.model.FileAttachments.push(response);
    };
    uploader.onCompleteAll = function () {
        $scope.commit();
    };
    //Store
    $scope.insert = function () {
        if ($scope.addform.validate()) {
            if ($scope.model.Title.trim() === "") {
                App.notifyDanger("Yêu cầu nhập tên thông báo");
                return;
            }
            if ($scope.model.StartDate > $scope.model.EndDate) {
                App.notifyDanger(" Ngày kết thúc phải lớn hơn hoặc bằng ngày bắt đầu");
                return;
            }
            if ($scope.model.IsPublic == 2) {
                if ($scope.ListReplate.EmployeeGuid.length == 0) {
                    App.notifyDanger("Yêu cầu chọn nhân viên");
                    return;
                }
            }
            var fd = new FormData();
            $scope.lstTitlefile = [];
            if (uploader.queue.length > 0) {
                for (i = 0; i < uploader.queue.length; i++) {
                    var files = uploader.queue[i]._file;
                    if (files.size > 30720000) {
                        App.notifyDanger("Yêu cầu file đính kèm không vượt quá 3MB");
                        return;
                    }
                    var titleName = "";
                    if (uploader.queue[i].file.FileName === "" || uploader.queue[i].file.FileName === null || uploader.queue[i].file.FileName === undefined) {
                        titleName = uploader.queue[i].file.name.split('.')[0];
                    } else {
                        titleName = uploader.queue[i].file.FileName;
                    }
                    $scope.lstTitlefile.push({ STT: i, Title: titleName });
                    fd.append("file", files);
                }
            }
            var list = [];
            for (var i = 0; i < $scope.ListReplate.EmployeeGuid.length; i++) {
                for (var j = 0; j < $rootScope.ListEmpAnnoucement.length; j++) {
                    if ($rootScope.ListEmpAnnoucement[j].EmployeeGuid == $scope.ListReplate.EmployeeGuid[i]) {
                        list.push({
                            EmployeeGuid: $rootScope.ListEmpAnnoucement[j].EmployeeGuid,
                            EmployeeName: $rootScope.ListEmpAnnoucement[j].FullName,
                        });
                        break;
                    }
                }
            }
            fd.append('submit', JSON.stringify($scope.model));
            fd.append('lstTitlefile', JSON.stringify($scope.lstTitlefile));
            fd.append('submitListEmp', JSON.stringify(list));
            $.ajax({
                type: "POST",
                url: "/Announcements/Submit",
                contentType: false,
                processData: false,
                data: fd,
                success: function (rs) {
                    if (rs.Error) {
                        App.notifyDanger(rs.Title);
                    } else {
                        App.notifyInfo(rs.Title);
                        $uibModalInstance.close();
                        $rootScope.Search();
                    }
                }
            });
        }
    };
});
// controller edit
app.controller('edit', function ($scope, $rootScope, $uibModalInstance, dataservice, para, FileUploader) {
    $scope.Title = "Sửa thông báo";
    $scope.model = {};
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
    $scope.ListReplate = {
        EmployeeGuid: [],
        EmployeeName: null,
    }
    $scope.ChangeIsPublic = function (data) {
        $scope.model.IsPublic = parseInt(data);
        if ($scope.model.IsPublic === 2) {
            $scope.isShow = true;
        } else {
            $scope.isShow = false;
        }
        if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
            $scope.$apply();
        }
    };
    $scope.ChangeIsActive = function (data) {
        $scope.model.IsActive = data;
    };
    $scope.ChangeStatus = function (data) {
        $scope.model.Status = data;
    };
    $scope.ChangeIspriority = function (data) {
        $scope.model.Ispriority = data;
    };

    $scope.changeEmpAnnoucement = function (data) {
        $scope.ListReplate.EmployeeGuid = data;
        var _data = '';
        for (var i = 0; i < data.length; i++) {
            _data += data[i] + ';';
        }
    }
    $scope.initData = function () {
        dataservice.getById(para, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {
                $scope.model = rs;
                if (rs.IsPublic === false) {
                    $scope.isShow = true;
                } else {
                    $scope.isShow = false;
                }
                if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                    $scope.$apply();
                }
                //Lấy file đính kèm
                var obj = {
                    RecordGuid: $scope.model.AnnouncementGuid
                }
                dataservice.GetAttachments(JSON.stringify(obj), function (result) {
                    $scope.jdataattach = result;
                });
                $scope.ListReplate.EmployeeGuid = [];
                dataservice.GetBylistEmp($scope.model.AnnouncementGuid, function (rs) {
                    angular.forEach(rs, function (value, key) {
                        $scope.ListReplate.EmployeeGuid.push(value.EmployeeGuid);
                    })
                });

                dataservice.GetEmpAnnoucement(function (rs) {
                    $rootScope.ListEmpAnnoucement = [];
                    $rootScope.ListEmpAnnoucement.push({ value: null, text: "---Mời bạn chọn---" })
                    for (var i = 0; i < rs.length; i++) {
                        rs[i].value = rs[i].EmployeeGuid;
                        rs[i].text = "[" + rs[i].EmployeeId + "] " + rs[i].FullName;
                    }
                    $rootScope.ListEmpAnnoucement = rs;
                });
                if (rs.StartDate !== null) {
                    $scope.model.StartDate = $rootScope.convertToJSONDate(rs.StartDate);
                }
                if (rs.EndDate !== null) {
                    $scope.model.EndDate = $rootScope.convertToJSONDate(rs.EndDate);
                }
                $scope.model.DueDate !== null ? $scope.model.DueDate = "/Date(" + new Date($scope.model.DueDate).getTime() + ")/" : undefined;
            }
        });
    };
    $scope.initData();
    //Cấu hình file đính kèm
    var uploader = $scope.uploader = new FileUploader({
        // url: App.baseService() + '/upload'
    });
    uploader.filters.push({
        name: "customFilter",
        fn: function (item /*{File|FileLikeObject}*/, options) {
            return this.queue.length < 10;
        }
    });
    $scope.lstDeleteFile = [];

    //Xóa file đính kèm
    $scope.remoteAttach = function (item, number) {
        ESConfirm([{ 'value': 1, 'title': 'Xác nhận', 'type': 'info' }, { 'value': 2, 'title': 'Đóng', 'type': 'danger' }], { 'description': 'file này không?', 'class': 'eswarning' }, function (rs) {
            if (rs === '1') {
                $scope.lstDeleteFile.push(item.AttachmentGuid);
                $scope.jdataattach.splice(number, 1);
                App.notifyInfo("Xóa thành công");
                if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                    $scope.$apply();
                }
            }
            if (rs === '2') {
                console.log('Không chấp nhận');
            }
        }, function () {
            console.log('Không lựa chọn');
        });

    };
 

    //store
    $scope.edit = function () {
        if ($scope.editform.validate()) {
            if ($scope.model.Title.trim() === "") {
                App.notifyDanger("Yêu cầu nhập tên thông báo");
                return;
            }
            if ($scope.model.StartDate > $scope.model.EndDate) {
                App.notifyDanger(" Ngày kết thúc phải lớn hơn hoặc bằng ngày bắt đầu");
                return;
            }

            if ($scope.model.IsPublic == 2) {
                if ($scope.ListReplate.EmployeeGuid.length ==0) {
                    App.notifyDanger("Yêu cầu chọn nhân viên");
                    return;
                }
            }

            var fd = new FormData();
            $scope.lstTitlefile = [];
            if (uploader.queue.length > 0) {
                for (i = 0; i < uploader.queue.length; i++) {
                    var files = uploader.queue[i]._file;
                    var titleName = "";
                    if (uploader.queue[i].file.FileName === "" || uploader.queue[i].file.FileName === null || uploader.queue[i].file.FileName === undefined) {
                        titleName = uploader.queue[i].file.name.split('.')[0];
                    } else {
                        titleName = uploader.queue[i].file.FileName;
                    }
                    $scope.lstTitlefile.push({ STT: i, Title: titleName });
                    fd.append("file", files);
                }
            }

            var list = [];
            for (var i = 0; i < $scope.ListReplate.EmployeeGuid.length; i++) {
                for (var j = 0; j < $rootScope.ListEmpAnnoucement.length; j++) {
                    if ($rootScope.ListEmpAnnoucement[j].EmployeeGuid == $scope.ListReplate.EmployeeGuid[i]) {
                        list.push({
                            EmployeeGuid: $rootScope.ListEmpAnnoucement[j].EmployeeGuid,
                            EmployeeName: $rootScope.ListEmpAnnoucement[j].FullName,
                        });
                        break;
                    }
                }
            }
            fd.append('edit', JSON.stringify($scope.model));
            fd.append('lstTitlefile', JSON.stringify($scope.lstTitlefile));
            fd.append('submitListEmp', JSON.stringify(list));
            fd.append('DeleteAttach', JSON.stringify($scope.lstDeleteFile));

            $.ajax({
                type: "POST",
                url: "/Announcements/Edit",
                contentType: false,
                processData: false,
                data: fd,
                success: function (rs) {
                    if (rs.Error) {
                        App.notifyDanger(rs.Title);
                    } else {
                        App.notifyInfo(rs.Title);
                        $uibModalInstance.close();
                        $rootScope.Search();
                    }
                }
            });
        }
    };
});
// controller open
app.controller('open', function ($scope, $rootScope, $uibModalInstance, dataservice, para, FileUploader) {
    $scope.Title = "Xem thông báo";
    $scope.model = {};
    $scope.ListReplate = {};
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
    var uploader = $scope.uploader = new FileUploader({
        // url: App.baseService() + '/upload'
    });
    uploader.filters.push({
        name: 'customFilter',
        fn: function (item /*{File|FileLikeObject}*/, options) {
            return this.queue.length < 10;
        }
    });
    $scope.loadData = function () {

        dataservice.getById(para, function (rs) {
            $scope.model = rs;          
            $scope.model.DueDate !== null ? $scope.model.DueDate = "/Date(" + new Date($scope.model.DueDate).getTime() + ")/" : undefined;
            if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                $scope.$apply();
            }
            var listGuid = [];
            listGuid.push($scope.model.AnnouncementGuid);
            dataservice.ListEmpAnnouncement_Update(listGuid, function (rs) {
                if (rs.Error) {
                } else {
                    $rootScope.Search();
                }
            })
            //Lấy file đính kèm
            var obj = {
                RecordGuid: $scope.model.AnnouncementGuid
            }
            dataservice.GetAttachments(JSON.stringify(obj), function (result) {
                $scope.jdataattach = result;
            });

            dataservice.GetBylistEmp(rs.AnnouncementGuid, function (rs) {
                $scope.ListReplate.EmployeeGuid = '';
                angular.forEach(rs, function (value, key) {
                    if ($scope.ListReplate.EmployeeGuid === '') {
                        $scope.ListReplate.EmployeeGuid = value.EmployeeName;
                    } else {
                        $scope.ListReplate.EmployeeGuid += ',' + (value.EmployeeName);
                    }

                })
            });
            $scope.lstTitlefile = [];
            if (uploader.queue.length > 0) {
                for (i = 0; i < uploader.queue.length; i++) {
                    var files = uploader.queue[i]._file;
                    var titleName = "";
                    if (uploader.queue[i].file.FileName === "" || uploader.queue[i].file.FileName === null || uploader.queue[i].file.FileName === undefined) {
                        titleName = uploader.queue[i].file.name.split('.')[0];
                    } else {
                        titleName = uploader.queue[i].file.FileName;
                    }
                    $scope.lstTitlefile.push({ STT: i, Title: titleName });
                    fd.append("file", files);
                }
            }
            if ($scope.model.IsActive === "Y") {
                $scope.model.IsActive = "Phát hành";
            }
            if ($scope.model.IsActive === "N") {
                $scope.model.IsActive = "Nháp";
            }
            if ($scope.model.Status === "Y") {
                $scope.model.Status = "Phát hành";
            }
            if ($scope.model.Status === "N") {
                $scope.model.Status = "Chưa phát hành";
            }
            if ($scope.model.Status === "E") {
                $scope.model.Status = "Hết hạn";
            }
            if ($scope.model.Ispriority === "Q") {
                $scope.model.Ispriority = "Quan trọng";
            }
            if ($scope.model.Ispriority === "B") {
                $scope.model.Ispriority = "Bình thường";
            }
            if ($scope.model.IsPublic === "1") {
                $scope.model.IsPublicText = "Công khai";
            } else if ($scope.model.IsPublic === "2") {
                $scope.model.IsPublicText = "Nhân viên";
            } else if ($scope.model.IsPublic === "3") {
                $scope.model.IsPublicText = "Phòng ban";
            }
            else {
                $scope.model.IsPublicText = "";
            }
            if (rs.CreatedBy !== null) {
                $scope.model.CreatedBy = $scope.model.CreatedBy.split('#')[1];
            }
            if (rs.ModifiedBy !== null) {
                $scope.model.ModifiedBy = $scope.model.ModifiedBy.split('#')[1];
            }
        });
    };
    $scope.loadData();
});
