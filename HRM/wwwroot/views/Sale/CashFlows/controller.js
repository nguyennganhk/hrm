﻿var ctxfolder = "/views/Sale/CashFlows";
app.factory('dataservice', function ($http) {
    var headers = {
        "Content-Type": "application/json;odata=verbose",
        "Accept": "application/json;odata=verbose"
    };
    return {
        //------------------------*Store*-------------------------
        getById: function (data, callback) {
            $http.post('/CashFlows/GetItem?Id=' + data).success(callback);
        },
        delete: function (data, callback) {
            $http.post('/CashFlows/Delete?Id=' + data).success(callback);
        },
        GetUnit: function (callback) {
            $http.post('/CashFlows/GetUnit/').success(callback);
        },
        getTotalDT: function (data, callback) {
            $http.post('/CashFlows/getTotalDT/', data).success(callback);
        },

    };

});
app.filter('html', ['$sce', function ($sce) {
    return function (text) {
        return $sce.trustAsHtml(text);
    };
}]);
app.filter('fdate', [
    '$filter', function ($filter) {
        return function (input, f) {
            if (input && input.toString().indexOf('Date') > -1) {
                return moment(input).format(f);
            } if (input && input.toString().indexOf('T') > -1) {
                return moment(input).format(f);
            } else return input;
        };
    }
]);
app.directive('format', ['$filter', function ($filter) {
    return {
        require: '?ngModel',
        link: function (scope, elem, attrs, ctrl) {
            if (!ctrl) return;
            ctrl.$formatters.unshift(function (a) {
                return $filter(attrs.format)(ctrl.$modelValue);
            });
            ctrl.$parsers.unshift(function (viewValue) {
                var plainNumber = viewValue.replace(/[^\d|\-+|\.+]/g, '');
                elem.val($filter(attrs.format)(plainNumber));
                return plainNumber;
            });
        }
    };
}]);
var spassAresSlug = function (number, isview) {
    try {
        // khai bao bien su dung       
        var numbers = number; var length = numbers.length; if (isview) {
            // khi dang la hien thi        
            if (numbers.indexOf(".") != -1) {
                length = numbers.indexOf(".");
                numbers = number.replace(".", ",");
            }
        } else {
            // khi dang la input textbox           
            if (numbers.indexOf(",") != -1) length = numbers.indexOf(",");
        }
        // loai bo nhung ky tu khong can thiet       
        // check so 0         
        var isTrue = true; while (isTrue) {
            if (numbers[0] == "0") numbers = numbers.substr(1, numbers.length);
            else isTrue = false;
        };
        // check dau phay      
        numbers = numbers.split(",");
        var string = "";
        for (var i = 0; i < numbers.length; i++) {
            if (i == 0) string += numbers[i];
            else if (i == 1) string += "," + numbers[i];
            else string += numbers[i];
        } numbers = string;
        // dung chung cho 2 truong hop       
        var no = 3, index = length - no;
        while (index > 0) { numbers = numbers.substr(0, index) + '.' + numbers.substr(index); index -= no; };
        return numbers == "" ? 0 : numbers;
    } catch (ex) { }
    return numbers == "" ? 0 : numbers;
}

app.directive('formatsub', ['$filter', function ($filter) {
    return {
        require: '?ngModel', link: function (scope, elem, attrs, ctrl) {
            if (!ctrl) return; ctrl.$formatters.unshift(function (viewValue) {
                try {
                    plainNumber = viewValue.replace('.', ','); elem.val(spassAresSlug(plainNumber));
                    return spassAresSlug(plainNumber);
                } catch (ex) { return undefined; }
            }); ctrl.$parsers.unshift(function (viewValue) {
                plainNumber = viewValue.replace(/[^-0-9,]/g, '');
                elem.val(spassAresSlug(plainNumber)); return plainNumber.replace(/[,]/g, '.');
            });
        }
    };
}]);
app.controller('Ctrl_ES_VCM_CashFlows', function ($scope, $rootScope, $sce, $compile, $uibModal, dataservice) {
    $rootScope.Title = "";
    $rootScope.go = function (path) {
        $location.path(path); return false;
    };

    $rootScope.validationOptions = {
        rules: {
            DateHD: {
                required: true
            },
            ItemName: {
                required: true,
                maxlength: 255
            }
        },
        messages: {
            DateHD: {
                required: "Yêu cầu nhập ngày."
            },
            ItemName: {
                required: "Yêu cầu nhập tên sản phẩm.",
                maxlength: "Tên sản phẩm không vượt quá 255 ký tự"
            },

        }
    };
    $rootScope.ListUnit = [];
    $rootScope.GetUnit = function () {
        dataservice.GetUnit(function (rs) {
            $rootScope.ListUnit = rs;
        });
    };
    $rootScope.GetUnit();

    $rootScope.dOrderStatus = [
        {
            value: "0",
            text: 'Dòng tiền vào'
        }, {
            value: "1",
            text: 'Dòng tiền ra'
        }];
    $rootScope.dTypesVao = [{
        value: "",
        text: 'Bỏ chọn'
    },
    {
        value: "0",
        text: 'Doanh thu bán hàng'
    }, {
        value: "1",
        text: 'Huy động vốn'
    }
        , {
        value: "2",
        text: 'Khoản thu khác'
    }
    ];
    $rootScope.dTypesRa = [{
        value: "",
        text: 'Bỏ chọn'
    }, {
        value: "4",
        text: 'Chi phí sx đơn hàng'
    }, {
        value: "5",
        text: 'Chi phí quản lý'
    }, {
        value: "6",
        text: 'Mua TSCĐ'
    }, {
        value: "7",
        text: 'Khoản chi khác'
    }
    ];
    $rootScope.ColumnMultiConfig = {
        selectAll: true,
        search: true,
        searchText: 'Tìm kiếm...',
        showTitle: 'true',
        csvSepChar: ',',
        captionFormat: '{0} lựa chọn',
        captionFormatAllSelected: '{0} lựa chọn',
        noMatch: 'Không có dữ liệu "{0}"',
        okCancelInMulti: true,
        locale: ['Đồng ý', 'Hủy', 'Chọn tất cả']
    };
    $rootScope.ColumnConfig = {
        search: true
    };
    $rootScope.CreateDate = function (date) {
        if (date !== null && date !== undefined) {
            var newdate = new Date(date);
            var month = newdate.getMonth() + 1;
            var day = newdate.getDate();
            var year = newdate.getFullYear();
            var hh = newdate.getHours();
            var mm = newdate.getMinutes();
            if (month < 10)
                month = "0" + month;
            if (day < 10)
                day = "0" + day;
            if (mm < 10)
                mm = "0" + mm;
            return todayDate = day + "/" + month + "/" + year;
        } else {
            return null;
        }
    };
    //các hàm chuyển đổi định dạng /Date(785548800000)/
    $rootScope.convertToJSONDate = function (strDate) {
        if (strDate !== null && strDate !== "") {
            var Str = strDate.toString();
            if (Str.indexOf("/Date") >= 0) {

                return Str;
            } else {
                var newDate = new Date(strDate);
                return '/Date(' + newDate.getTime() + ')/';
            }
        }

    }
    //định dạng số xóa dấu chấm phẩy
    $rootScope.addPeriod = function (nStr) {
        if (nStr !== null && nStr !== "" && nStr !== undefined) {
            nStr = Math.round(parseFloat(nStr) * 100) / 100;

            nStr += "";
            if (nStr.indexOf(",") >= 0) {
                var x = nStr.split(",");
            } else {
                var x = nStr.split(".");
            }
            var x1 = x[0];
            var x2 = x.length > 1 && parseInt(x[1]) > 0 ? "," + x[1] : "";
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, "$1" + "." + "$2");
            }
            var Total = x1 + x2;
            return Total;
        }
        else {
            return 0;
        }
    };


    //Thêm phần tinymceOptions
    $rootScope.tinymceOptions = {
        selector: 'textarea',
        plugins: [
            'advlist autolink lists link charmap print preview hr anchor pagebreak image',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
        ],
        skin: 'lightgray', language: 'vi_VN',
        theme: 'modern', height: 70,
        toolbar1: 'codesample | bold italic sizeselect fontselect fontsizeselect | hr alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | insertfile undo redo | emoticons | code',
        fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
        toolbar2: 'print preview insert link image media | forecolor backcolor emoticons | codesample',
        image_advtab: true,

        file_browser_callback: function (field_name, url, type, win) {
            var connector = "/FileManager/Folder/File";
            var enableAutoTypeSelection = true;
            if (enableAutoTypeSelection && type) {
                connector += "&Type=" + type;
            }
            connector += "&Field=" + field_name;
            App.openPage(connector, "File Manager", 1150, 600);
        },
        relative_urls: false,
        remove_script_host: true,
        convert_urls: true
    };

    $rootScope.ConDate = function (data, number) {
        try {
            if (data == null || data == "") {
                return '';
            }
            if (data !== null && data != "" && data != undefined) {
                try {
                    if (data.indexOf("SA") != -1 || data.indexOf("CH") != -1) {
                        if (data.indexOf("SA") != -1) { }
                        if (data.indexOf("CH") != -1) { }
                    }

                    if (data.indexOf('Date') != -1) {
                        data = data.substring(data.indexOf("Date") + 5);
                        data = parseInt(data);
                    }
                }
                catch (ex) { }
                var newdate = new Date(data);
                if (number == 3) {
                    return newdate;
                }
                if (number == 2) {
                    return "/Date(" + newdate.getTime() + ")/";
                }
                var month = newdate.getMonth() + 1;
                var day = newdate.getDate();
                var year = newdate.getFullYear();
                var hh = newdate.getHours();
                var mm = newdate.getMinutes();
                if (month < 10)
                    month = "0" + month;
                if (day < 10)
                    day = "0" + day;
                if (mm < 10)
                    mm = "0" + mm;
                if (number == 0) {
                    return todayDate = day + "/" + month + "/" + year;
                }
                if (number == 1) {
                    return todayDate = day + "/" + month + "/" + year + " " + hh + ":" + mm;
                }
                if (number == 4) {
                    return new Date(year, month - 1, day);
                }
            } else {
                return '';
            }
        } catch (ex) {
            return "";
        }
    }
});
app.config(function ($routeProvider, $validatorProvider) {
    $routeProvider
        .when('/', {
            templateUrl: ctxfolder + '/index.html',
            controller: 'index'
        })
        .when('/edit/:id', {
            templateUrl: ctxfolder + '/edit.html',
            controller: 'edit'
        })
        .when('/add/', {
            templateUrl: ctxfolder + '/add.html',
            controller: 'add'
        });
    $validatorProvider.setDefaults({
        errorElement: 'span',
        errorClass: 'help-block',
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        success: function (label) {
            label.closest('.form-group').removeClass('has-error');
        }
    });
});
//controller hiển thị, tìm kiếm
app.controller('index', function ($scope, $rootScope, $compile, $http, $confirm, $ngConfirm, $uibModal, DTOptionsBuilder, DTColumnBuilder, DTInstances, dataservice, utility) {
    $scope.utility = utility;
    $scope.model = {};
    var vm = $scope;
    var ctrl = $scope;
    $scope.staticParam = {
        totalItems: 0,
        currentPage: 1,
        pageSize: 25,
        maxSize: 5,
        Search: "",
        Type: "0",
        StartDate: null,
        EndDate: null
    };
    ctrl.changeOptionDate = function (d) {
        if (d.length === 0) {
            ctrl.staticParam.StartDate = null;
            ctrl.staticParam.EndDate = null;
        } else {
            ctrl.staticParam.StartDate = $rootScope.ConDate(d[0], 2);
            ctrl.staticParam.EndDate = $rootScope.ConDate(d[1], 2);
        }
    }


    $scope.selected = [];
    $scope.selectAll = false;
    $scope.toggleOne = toggleOne;
    $scope.toggleAll = toggleAll;

    var titleHtml = '<label style="text-align: center;" class="mt-checkbox" ><input type="checkbox" ng-model="selectAll" ng-click="toggleAll(selectAll, selected)"/><span></span></label>';
    //if (App.Permissions.LISTVIEW)
    vm.dtOptions = DTOptionsBuilder.newOptions()
        .withPaginationType('full_numbers')
        .withDataProp('data').withDisplayLength(15)
        .withOption('order', [1, 'desc'])
        .withOption('serverSide', true)
        .withOption('pageLength', 25)
        .withOption('info', false)
        .withOption('stateLoadParams', function (settings, data) {
            data.search.search = '';
        })
        .withOption('headerCallback', function (header) {
            if (!$scope.headerCompiled) {
                $scope.headerCompiled = true;
                $compile(angular.element(header).contents())($scope);
            }
        })
        .withOption('initComplete', function (settings, json) {
            //thêm sự kiện scroll phân trang
            //  $('#tblData').parent().attr("onscroll", "angular.element(this).scope().LoadScroll(this)");
            PLUGIN_DATATABLE_TABLE_SCROLL.INIT_EVENT('tblData', $scope.LoadScroll);

        })
        .withOption('createdRow', function (row, data, dataIndex) {
            const contextScope = $scope.$new(true);
            contextScope.data = data;
            $scope.data = data;
            $compile(row)($scope);
            contextScope.contextMenu = $scope.contextMenu;
            $compile(angular.element(row).find('input'))($scope);
            $compile(angular.element(row).attr('context-menu', 'contextMenu'))(contextScope);
        });
    // if (App.Permissions.LISTVIEW)
    vm.dtOptions.withOption('ajax', function (data, callback, settings) {
        data.Keyword = $scope.staticParam.Search;
        data.StartDate = $scope.staticParam.StartDate;
        data.EndDate = $scope.staticParam.EndDate;
        data.Type = $scope.staticParam.Type;
        App.blockUI({
            target: "#tblData",
            boxed: true,
            message: 'Đang tải...'
        });
        $http.post('/CashFlows/JTable', data)
            .success(function (res) {
                $scope.selected = [];
                if (data.start === 0) {
                    $scope.dataload = [];
                    angular.forEach(res.data, function (val, key) {
                        $scope.dataload.push(val);
                    });
                    callback({
                        data: $scope.dataload,
                        recordsTotal: res.recordsTotal,
                        recordsFiltered: res.recordsFiltered
                    });
                } else {
                    if (res.recordsTotal > $scope.dataload.length) {
                        angular.forEach(res.data, function (val, key) {
                            $scope.dataload.push(val);
                        });
                        callback({
                            data: $scope.dataload,
                            recordsTotal: res.recordsTotal,
                            recordsFiltered: res.recordsFiltered
                        });
                    }
                }
                $scope.getTotalDT();
                PLUGIN_DATATABLE_TABLE_SCROLL.UNLOCK_EVENT('tblData');
                App.unblockUI("#tblData");
            });
    });
    //scroll theo tỷ lệ màn hình
    vm.dtOptions.withOption('scrollY', '70vh')
        .withOption('scrollX', '100%') //mặc định 100%
        .withOption('scrollCollapse', true) //xóa khoảng trắng khi dữ liệu trong bảng ít
        .withOption('rowCallback', rowCallback) //sự kiên click vào 1 dòng
        .withOption('scroller', {
            loadingIndicator: true
        });
    //.withFixedColumns({
    //    leftColumns: 0 //fix bên trái 2 cột
    //    //rightColumns: 1 //fix bên phải 1 cột
    //}
    //);
    //table
    vm.dtColumns = [];

    vm.dtColumns.push(DTColumnBuilder.newColumn('RowGuid').withTitle('STT').notSortable().withOption('sWidth', '5px').renderWith(function (data, type, full, meta) {
        return meta.row + 1;
    }).withOption('sClass', 'tcenter tcenter-header'));
    vm.dtColumns.push(DTColumnBuilder.newColumn('DateHD').withTitle('Ngày').withOption('sWidth', '60px').withOption('sClass', 'tcenter-header').renderWith(function (data, type, full, meta) {
        return '<a  style="color: #4285f4;" href="" ng-click="open(\'' + full.RowGuid + '\')" >' + full.DateHDString + '</a>';
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('Types').withTitle('Loại huy động').withOption('sWidth', '60px').withOption('sClass', 'tcenter-header').renderWith(function (data, type, full, meta) {
        if (data == "0") {
            return "Doanh thu bán hàng";
        } else if (data == "1") {
            return "Huy động vốn";
        } else if (data == "2") {
            return "Khoản thu khác";
        } else if (data == "4") {
            return "Chi phí sx đơn hàng";
        } else if (data == "5") {
            return "Chi phí quản lý";
        } else if (data == "6") {
            return "Mua TSCĐ";
        } else if (data == "7") {
            return "Khoản chi khác";
        } else {
            return "";
        }
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('Description').withTitle('Diễn giải').withOption('sWidth', '150px').withOption('sClass', 'tcenter-header').renderWith(function (data, type, full, meta) {
        return data;
    }));

    vm.dtColumns.push(DTColumnBuilder.newColumn('AmountOc').withTitle('Số tiền').withOption('sClass', 'tright').withOption('sWidth', '80px').renderWith(function (data, type, full) {
        return $rootScope.addPeriod(data);
    }));

    vm.dtColumns.push(DTColumnBuilder.newColumn('RowGuid').notSortable().withTitle('Điều khiển').notSortable().withOption('sWidth', '40px').withOption('sClass', 'tcenter').renderWith(function (data, type, full, meta) {
        return '<div class="table__cell-actions-wrap">' +
            '<a ng-click="open(\'' + full.RowGuid + '\')" class="table__cell-actions-item table__cell-actions-icon"><span class="mdi mdi-eye" ></span></a > ' +
            '<a href="javascript:;" ng-click="edit(\'' + full.RowGuid + '\')" class="table__cell-actions-item table__cell-actions-icon es-cursor"> <span class="mdi mdi-table-edit" title="Sửa"></span> </a>' +
            '<a ng-click="delete (\'' + full.RowGuid + '\')" class="table__cell-actions-item table__cell-actions-icon"><span class="mdi mdi-delete" title="Xóa"></span> </a>';

    }));
    vm.reloadData = reloadData;
    vm.dtInstance = {};
    function reloadData(resetPaging) {
        $scope.staticParam.currentPage = resetPaging;
        vm.dtInstance.reloadData(callback, resetPaging);
    }
    function callback(json) {

    }
    function toggleOne(selectedItems) {
        for (var id in selectedItems) {
            if (selectedItems.hasOwnProperty(id)) {
                if (!selectedItems[id]) {
                    vm.selectAll = false;
                    return;
                }
            }
        }
        vm.selectAll = true;
    }
    function toggleAll(selectAll, selectedItems) {
        for (var id in selectedItems) {
            if (selectedItems.hasOwnProperty(id)) {
                selectedItems[id] = selectAll;
            }
        }
    }
    function rowCallback(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        $('td', nRow).unbind('dblclick');
        $('td', nRow).bind('dblclick', function () {
            $scope.$apply(function () {
                $scope.open(aData.RowGuid);
            });
        });
        return nRow;

    }
    //Load dữ liệu cuộn
    $scope.total = 0;
    $scope.check = false;
    $scope.LoadScroll = function (obj) {
        vm.dtInstance.DataTable.page('next').draw('page');
    };
    ctrl.changeOptionDate = function (d) {
        if (d.length === 0) {
            ctrl.staticParam.StartDate = null;
            ctrl.staticParam.EndDate = null;
        } else {
            ctrl.staticParam.StartDate = $rootScope.ConDate(d[0], 2);
            ctrl.staticParam.EndDate = $rootScope.ConDate(d[1], 2);
        }
    }
    $scope.$watch('staticParam.StartDate', function (newvalue, oldvalue) {
        if (newvalue !== undefined && newvalue !== oldvalue) {
            $rootScope.reload();
        }
    });
    $scope.$watch('staticParam.EndDate', function (newvalue, oldvalue) {
        if (newvalue !== undefined && newvalue !== oldvalue) {
            $rootScope.reload();
        }
    });
    $scope.reloadAll = function () {
        $scope.staticParam.Search = "";
        $scope.selectAll = false;
        reloadData(true);
    };
    $rootScope.reload = function () {      
        $scope.dataload = [];
        reloadData(true);
    };

    $scope.jxcelAddDetailInit = {
        dataObject: true,
        reloadData: {},
        data: [],
        colHeaders: ['', 'Dòng tiền Vào DK/TT', 'Dòng tiền Ra DK/TT', "Dòng tiền thuần DK/TT", 'Số dư tiền CK DK/TT', 'Số tiền thừa/thiếu DK/TT'
        ],
        colTitles: ['', 'Dòng tiền vào dự kiến/thực tế', 'Dòng tiền ra dự kiến/thực tế', "Dòng tiền thuần dự kiến/thực tế", 'Số dư tiền cuối kỳ dự kiến/thực tế', 'Số tiền thừa/thiếu dự kiến/thực tế'
        ],
        colFooters: ['', '', '', '', '', ''
        ],
        columns: [
            {
                name: 'RowGuid',
                type: 'text',
                readOnly: true,
            },
            {
                name: 'TotalDTVaoDK',//Xếp hạng 15
                type: 'text', readOnly: true

            },
            {
                name: 'TotalDTRaDK',//Xếp hạng 15
                type: 'text',  readOnly: true

            },
            {
                name: 'TotalDTDK',//Xếp hạng 3
                type: 'text', readOnly: true
            },
            {
                name: 'TotalSDTCDK',//Xếp hạng 15
                type: 'text', readOnly: true
            },
            {
                name: 'TotalTTTDK',//Xếp hạng 15
                type: 'text', readOnly: true
            }
            
        ],

        columnShow: [false, true, true, true, true, true],
        initDataRows: ['', '', '', '', '', '', '', '', '', '', ''],
        colWidths: [50, 200, 200, 200, 200, 200],
        allrowevent: true,
        tableOverflow: true,
        csvHeaders: true,
        tableHeight: '200px',
        changeCell: $scope.rowchange,
        oninsertrow: $scope.oninsertrow1,
        onload: $scope.onload1,
        allowInsertRow: false,
        contextMenu: function () { return ""; }
    };
    $scope.getTotalDT = function () {
        var obj = {
            Keyword: $scope.staticParam.Search,
            StartDate: $scope.staticParam.StartDate,
            EndDate: $scope.staticParam.EndDate,
            Type: $scope.staticParam.Type
        }
        dataservice.getTotalDT(obj, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {
                var ApplyOutsideDetails = [];
                var TotalDTVaoDK = $rootScope.addPeriod(rs.TotalDTVaoDK.toString()) + " / " + $rootScope.addPeriod( rs.TotalDTVaoTT.toString());
                var TotalDTRaDK = $rootScope.addPeriod(rs.TotalDTRaDK.toString()) + " / " + $rootScope.addPeriod(rs.TotalDTRaTT.toString());
                var TotalDTDK = $rootScope.addPeriod(rs.TotalDTDK.toString()) + " / " + $rootScope.addPeriod(rs.TotalDTTT.toString());
                var TotalSDTCDK = $rootScope.addPeriod(rs.TotalSDTCDK.toString()) + " / " + $rootScope.addPeriod(rs.TotalSDTCTT.toString());
                var TotalTTTDK = $rootScope.addPeriod(rs.TotalTTTDK.toString()) + " / " + $rootScope.addPeriod(rs.TotalTTTTT.toString());
                ApplyOutsideDetails.push([
                    rs.Id,
                    TotalDTVaoDK,
                    TotalDTRaDK,
                    TotalDTDK,
                    TotalSDTCDK,
                    TotalTTTDK                 
                ]);
                $scope.jxcelAddDetailInit.data = ApplyOutsideDetails;

            }
        });
    }


    $scope.ChangeStatusCompleted = function (data) {
        $scope.staticParam.Type = data;
        $rootScope.reload();
    }
    // thêm mới
    $scope.add = function () {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/add.html',
            controller: 'add',
            backdrop: 'static',
            keyboard: false,
            size: '100',
            resolve: {
                para: function () {
                    return "";
                }
            }
        });
        modalInstance.result.then(function (d) {
        }, function () {
        });
    };
    $scope.open = function (temp) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/open.html',
            controller: 'open',
            backdrop: 'static',
            size: '100',
            resolve: {
                para: function () {
                    return temp;
                }
            }
        });
        modalInstance.result.then(function (d) {
        }, function () {
        });

    };
    // sửa 
    $scope.edit = function (temp) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/add.html',
            controller: 'add',
            backdrop: 'static',
            size: '100',
            resolve: {
                para: function () {
                    return temp;
                }
            }
        });
    };
    //Xóa 1 bản ghi
    $scope.delete = function (temp) {
        dataservice.getById(temp, function (rs) {
            $scope.Description = JSON.parse(rs.Table1)[0].Description;
            ESConfirm([{ 'value': 1, 'title': 'Xác nhận', 'type': 'info' }, { 'value': 2, 'title': 'Đóng', 'type': 'danger' }], { 'description': 'Bạn có muốn xóa ' + $scope.Description, 'class': 'eswarning_v2' }, function (rs) {
                if (rs === '1') {
                    App.blockUI({
                        target: "#contentMain",
                        boxed: true,
                        message: 'Đang tải...'
                    });
                    dataservice.delete(temp, function (result) {
                        if (result.Error) {
                            App.notifyDanger(result.Title);
                        } else {
                            App.notifyInfo(result.Title);
                            $rootScope.reload();
                        }
                        App.unblockUI("#contentMain");
                    });
                }
            }, function () {
            });
        });
    };

    $scope.contextMenu = [
        [function ($itemScope) {
            return '<i class="mdi mdi-eye"></i> Xem';
        }, function ($itemScope, $event, model) {
            $scope.open($itemScope.data.RowGuid);
        }],
        [function ($itemScope) {
            return '<i class="mdi mdi-table-edit"></i> Sửa';
        }, function ($itemScope, $event, model) {
            $scope.edit($itemScope.data.RowGuid);
        }, function ($itemScope, $event, model) {
            return true;
        }],
        [function ($itemScope) {
            return '<i class="mdi mdi-delete"></i> Xóa';
        }, function ($itemScope, $event, model) {
            $scope.delete($itemScope.data.RowGuid);
        }, function ($itemScope, $event, model) {
            return true;
        }]
    ];

});
app.controller('add', function ($scope, $rootScope, $uibModalInstance, FileUploader, dataservice, para) {
    $scope.model = { Temps: "", TypesDT: "0", DateHD: $rootScope.convertToJSONDate(new Date()) };
    $scope.disColumn = false;
    if (para != "") {
        $scope.Title = "Sửa";
        $scope.model.Temps = "2";
        $scope.disColumn = true;
        $scope.initData = function () {
            dataservice.getById(para, function (rs) {
                if (rs.Error) {
                    App.notifyDanger(rs.Title);
                } else {
                    $scope.model = JSON.parse(rs.Table1)[0];
                    $scope.model.DateHD !== null ? $scope.model.DateHD = "/Date(" + new Date($scope.model.DateHD).getTime() + ")/" : undefined;
                }
            })
        }
        $scope.initData();
    } else {
        $scope.Title = "Thêm mới";
        $scope.model.Temps = "1";
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    //Cấu hình file đính kèm
    $scope.model.FileAttachments = [];
    var uploader = $scope.uploader = new FileUploader({
        // url: App.baseService() + '/upload'
    });
    uploader.filters.push({
        name: 'customFilter',
        fn: function (item /*{File|FileLikeObject}*/, options) {
            return this.queue.length < 10;
        }
    });
    uploader.onSuccessItem = function (fileItem, response, status, headers) {
        $scope.model.FileAttachments.push(response);
    };
    uploader.onCompleteAll = function () {
        $scope.commit();
    };
    $scope.ChangeUnitID = function (data) {
        $scope.model.UnitID = data;
    }
    $scope.ChangeTypesDT = function (data) {
        $scope.model.TypesDT = data;
    }
    $scope.ChangeTypes = function (data) {
        $scope.model.Types = data;
    }

    $scope.insert = function () {
        if ($scope.addform.validate()) {
            var fd = new FormData();
            $scope.lstTitlefile = [];
            if (uploader.queue.length > 0) {
                for (i = 0; i < uploader.queue.length; i++) {
                    var files = uploader.queue[i]._file;
                    if (files.size > 30720000) {
                        App.notifyDanger("Yêu cầu file đính kèm không vượt quá 3MB");
                        return;
                    }
                    fd.append("file", files);
                }
            }
            fd.append('submit', JSON.stringify($scope.model));
            $.ajax({
                type: "POST",
                url: "/CashFlows/Submit",
                contentType: false,
                processData: false,
                data: fd,
                success: function (rs) {
                    if (rs.Error) {
                        App.notifyDanger(rs.Title);
                    } else {
                        if ($scope.model.Temps == "1") {
                            App.notifyInfo("Thêm mới thành công");
                        } else {
                            App.notifyInfo("Cập nhật thành công");
                        }                       
                        $uibModalInstance.close();
                        $rootScope.reload();
                    }
                }
            });
        }
    };
});
// controller open
app.controller('open', function ($scope, $rootScope, $uibModalInstance, dataservice, para, FileUploader) {
    $scope.Title = "Xem ";
    $scope.model = {};
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
    var uploader = $scope.uploader = new FileUploader({
        // url: App.baseService() + '/upload'
    });
    uploader.filters.push({
        name: 'customFilter',
        fn: function (item /*{File|FileLikeObject}*/, options) {
            return this.queue.length < 10;
        }
    });
    $scope.loadData = function () {
        dataservice.getById(para, function (rs) {
            $scope.model = JSON.parse(rs.Table1)[0];
            $scope.model.DateHD !== null ? $scope.model.DateHD = "/Date(" + new Date($scope.model.DateHD).getTime() + ")/" : undefined;

            if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                $scope.$apply();
            }
        });
    };
    $scope.loadData();
});
