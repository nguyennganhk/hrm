﻿var ctxfolder = "/views/Sale/ProductionManager";
app.factory('dataservice', function ($http) {
    var headers = {
        "Content-Type": "application/json;odata=verbose",
        "Accept": "application/json;odata=verbose",
    }
    return {
        IsDelete: function (data, callback) {
            $http.post('/ProductionManager/Delete', data).success(callback);
        },
        getItem: function (data, callback) {
            $http.post('/ProductionManager/GetItem', data).success(callback);
        },
        getUnitJexcel: function (data, callback) {
            $http.post('/ProductDeparments/getUnitJexcel/', data).success(callback);
        },
        getUnit_by_ItemId: function (data, callback) {
            $http.post('/ProductDeparments/getUnit_by_ItemId/', data).success(callback);
        },
        checkLogin: function (data, callback) {
            $http.post('/ProductionManager/checkLogin', data).success(callback);
        },
        GetProcessWF: function (data, callback) {
            $http.post('/ProductionManager/GetProcessWF?Id=' + data).success(callback);
        },
        GetAllCommentPage: function (data, callback) {
            $http.post('/ProductionManager/GetAllCommentPage', data).success(callback);
        },
        getCustomer: function (callback) {
            $http.post('/ProductionManager/getCustomer_GetAll').success(callback);
        },
        getattachment: function (data, callback) {
            $http.post('/ProductionManager/GetItemAttachment/', data).success(callback);
        },
        getById_Customers: function (data, callback) {
            $http.post('/Customers/GetItem/' + data).success(callback);
        },
        delete_Customers: function (data, callback) {
            $http.post('/Customers/Delete?Id=' + data).success(callback);
        },
    }
});
app.filter('fdate', [
    '$filter', function ($filter) {
        return function (input, f) {
            if (input && input.toString().indexOf('Date') > -1) {
                return moment(input).format(f);
            } if (input && input.toString().indexOf('T') > -1) {
                return moment(input).format(f);
            } else return input;
        };
    }
]);
app.directive('format', ['$filter', function ($filter) {
    return {
        require: '?ngModel',
        link: function (scope, elem, attrs, ctrl) {
            if (!ctrl) return;
            ctrl.$formatters.unshift(function (a) {
                return $filter(attrs.format)(ctrl.$modelValue)
            });

            ctrl.$parsers.unshift(function (viewValue) {
                var plainNumber = viewValue.replace(/[^\d|\-+|\.+]/g, '');
                elem.val($filter(attrs.format)(plainNumber));
                return plainNumber;
            });
        }
    };
}]);
app.filter('html', ['$sce', function ($sce) {
    return function (text) {
        return $sce.trustAsHtml(text);
    };
}])
var spassAresSlug = function (number, isview) {
    try {
        // khai bao bien su dung       
        var numbers = number; var length = numbers.length; if (isview) {
            // khi dang la hien thi        
            if (numbers.indexOf(".") != -1) {
                length = numbers.indexOf(".");
                numbers = number.replace(".", ",");
            }
        } else {
            // khi dang la input textbox           
            if (numbers.indexOf(",") != -1) length = numbers.indexOf(",");
        }
        // loai bo nhung ky tu khong can thiet       
        // check so 0         
        var isTrue = true; while (isTrue) {
            if (numbers[0] == "0") numbers = numbers.substr(1, numbers.length);
            else isTrue = false;
        };
        // check dau phay      
        numbers = numbers.split(",");
        var string = "";
        for (var i = 0; i < numbers.length; i++) {
            if (i == 0) string += numbers[i];
            else if (i == 1) string += "," + numbers[i];
            else string += numbers[i];
        } numbers = string;
        // dung chung cho 2 truong hop       
        var no = 3, index = length - no;
        while (index > 0) { numbers = numbers.substr(0, index) + '.' + numbers.substr(index); index -= no; };
        return numbers == "" ? 0 : numbers;
    } catch (ex) { }
    return numbers == "" ? 0 : numbers;
}

app.directive('formatsub', ['$filter', function ($filter) {
    return {
        require: '?ngModel', link: function (scope, elem, attrs, ctrl) {
            if (!ctrl) return; ctrl.$formatters.unshift(function (viewValue) {
                try {
                    plainNumber = viewValue.replace('.', ','); elem.val(spassAresSlug(plainNumber));
                    return spassAresSlug(plainNumber);
                } catch (ex) { return undefined; }
            }); ctrl.$parsers.unshift(function (viewValue) {
                plainNumber = viewValue.replace(/[^-0-9,]/g, '');
                elem.val(spassAresSlug(plainNumber)); return plainNumber.replace(/[,]/g, '.');
            });
        }
    };
}]);
function onlickFileComment(d) {
    var __el = document.getElementById('file-comment-id');
    __el.setAttribute('accept', '.rar, .zip');
    d === 1 ? __el.setAttribute('accept', 'video/mp4,video/x-m4v,video/*') : '';
    d === 2 ? __el.setAttribute('accept', '.jpg, .png, .jpeg, .gif, .bmp, .tif, .tiff|images/*') : '';
    __el.click();
}
app.controller('Ctrl_Sale_ProductionManager', function ($scope, $rootScope, $compile, dataservice) {
    $rootScope.go = function (path) {
        $location.path(path); return false;
    };
    $rootScope.ListIsActive = [
        {
            value: null,
            text: 'Bỏ chọn'
        },
        {
            value: '1',
            text: 'Sử dụng'
        }, {
            value: '0',
            text: 'Không sử dụng'
        }
    ];
    $rootScope.ListBusinessTypeName = [
        {
            value: 'KHTT',
            text: 'Khách hàng trung thành'
        }, {
            value: 'KHM',
            text: 'Khách hàng mới'
        }, {
            value: 'KHTN',
            text: 'Khách hàng tiềm năng'
        }, {
            value: 'KHKTN',
            text: 'Khách hàng không tiềm năng'
        }
    ];


    $rootScope.validationOptions = {
        rules: {
            CodeID: {
                required: true,
                maxlength: 20
            }
        },
        messages: {
            CodeID: {
                required: "Yêu cầu nhập mã báo giá",
                maxlength: "Mã báo giá không vượt quá 20 ký tự."
            }
        }
    }
    $rootScope.TypeData = [{
        value: 1,
        text: 'Khả thi'
    }, {
        value: 0,
        text: 'Phấn đấu'
    }];
    $rootScope.StatusData = [{
        value: 1,
        text: 'Dự án mới'
    }, {
        value: 2,
        text: 'Đang triển khai'
    }
        , {
        value: 3,
        text: 'Hoàn thành'
    }
    ];


    $rootScope.CreateDate = function (date) {
        if (date !== null && date !== undefined) {
            var newdate = new Date(date);
            var month = newdate.getMonth() + 1;
            var day = newdate.getDate();
            var year = newdate.getFullYear();
            var hh = newdate.getHours();
            var mm = newdate.getMinutes();
            if (month < 10)
                month = "0" + month;
            if (day < 10)
                day = "0" + day;
            if (mm < 10)
                mm = "0" + mm;
            return todayDate = day + "/" + month + "/" + year;
        } else {
            return null;
        }
    }

    //0 11/11/2017
    //1 11/11/2017 22:22
    //2 123123123
    //3 type date
    //4 type data year month day
    $rootScope.ConDate = function (data, number) {
        try {
            if (data == null || data == "") {
                return '';
            }
            if (data !== null && data != "" && data != undefined) {
                try {
                    if (data.indexOf("SA") != -1 || data.indexOf("CH") != -1) {
                        if (data.indexOf("SA") != -1) { }
                        if (data.indexOf("CH") != -1) { }
                    }

                    if (data.indexOf('Date') != -1) {
                        data = data.substring(data.indexOf("Date") + 5);
                        data = parseInt(data);
                    }
                }
                catch (ex) { }
                var newdate = new Date(data);
                if (number == 3) {
                    return newdate;
                }
                if (number == 2) {
                    return "/Date(" + newdate.getTime() + ")/";
                }
                var month = newdate.getMonth() + 1;
                var day = newdate.getDate();
                var year = newdate.getFullYear();
                var hh = newdate.getHours();
                var mm = newdate.getMinutes();
                if (month < 10)
                    month = "0" + month;
                if (day < 10)
                    day = "0" + day;
                if (mm < 10)
                    mm = "0" + mm;
                if (number == 0) {
                    return todayDate = day + "/" + month + "/" + year;
                }
                if (number == 1) {
                    return todayDate = day + "/" + month + "/" + year + " " + hh + ":" + mm;
                }
                if (number == 4) {
                    return new Date(year, month - 1, day);
                }
            } else {
                return '';
            }
        } catch (ex) {
            return "";
        }
    }
    //định dạng số xóa dấu chấm phẩy
    $rootScope.addPeriod = function (nStr) {
        if (nStr !== null && nStr !== "" && nStr !== undefined) {
            nStr = Math.round(parseFloat(nStr) * 100) / 100;

            nStr += "";
            if (nStr.indexOf(",") >= 0) {
                var x = nStr.split(",");
            } else {
                var x = nStr.split(".");
            }
            var x1 = x[0];
            var x2 = x.length > 1 && parseInt(x[1]) > 0 ? "," + x[1] : "";
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, "$1" + "." + "$2");
            }
            var Total = x1 + x2;
            return Total;
        }
        else {
            return 0;
        }
    };

    $rootScope.YearData = []; $rootScope.MonthData = [];
    //$rootScope.MonthData.push({ value: null, text: "Bỏ chọn" });
    for (var i = 2018; i < 2050; i++) {
        var ob = { value: i, text: 'Năm ' + i.toString() };
        $rootScope.YearData.push(ob);
    }
    for (var i = 1; i <= 12; i++) {
        var ob = { value: i, text: 'Tháng ' + i.toString() };
        $rootScope.MonthData.push(ob);
    }
    $rootScope.ColumnMultiConfig = {
        selectAll: true,
        search: true,
        searchText: 'Tìm kiếm...',
        showTitle: 'true',
        csvSepChar: ',',
        captionFormat: '{0} lựa chọn',
        captionFormatAllSelected: '{0} lựa chọn',
        noMatch: 'Không có dữ liệu "{0}"',
        okCancelInMulti: true,
        locale: ['Đồng ý', 'Hủy', 'Chọn tất cả']
    };
    $rootScope.ColumnConfig = {
        search: true
    };

    $rootScope.ListCustomer = [];
    $rootScope.GetCustomer = function () {
        dataservice.getCustomer(function (rs) {
            $rootScope.ListCustomer = rs;
        });
    };
    $rootScope.GetCustomer();



});

app.config(function ($routeProvider, $validatorProvider) {
    $routeProvider
        .when('/', {
            templateUrl: ctxfolder + '/index.html',
            controller: 'index'
        })
        .when('/edit/:id', {
            templateUrl: ctxfolder + '/edit.html',
            controller: 'edit'
        }).when('/open/:id', {
            templateUrl: ctxfolder + '/open.html',
            controller: 'open'
        })
        .when('/add/', {
            templateUrl: ctxfolder + '/add.html',
            controller: 'add'
        })
    $validatorProvider.setDefaults({
        errorElement: 'span',
        errorClass: 'help-block',
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        success: function (label) {
            label.closest('.form-group').removeClass('has-error');
        }
    });
});
app.controller('index', function ($http, $scope, $timeout, $uibModal, $rootScope, $compile, $confirm, $ngConfirm, $uibModal, DTOptionsBuilder, DTColumnBuilder, DTInstances, dataservice, keyhandle, utility) {
    $scope.utility = utility;
    $rootScope.TableName = "/ProductionManager";

    $scope.model = {};
    var vm = $scope;
    var ctrl = $scope;
    ctrl.nextRow = function (d) {
        if (ctrl.ElementRowCheck === -1) {
            if (ctrl.allRow.length > 1) {
                ctrl.ElementRowCheck = ctrl.allRow[0].row;
                $('td', ctrl.ElementRowCheck).addClass("es-iSeleted");
                ctrl.iSelected = ctrl.allRow[0].data;
                ctrl.initData(ctrl.iSelected.RowGuid);

            }
        } else
            for (var i = 0; i < ctrl.allRow.length; i++) {
                if (ctrl.allRow[i].row === ctrl.ElementRowCheck) {
                    if (i === ctrl.allRow.length - 1) break;
                    $('td', ctrl.ElementRowCheck).removeClass('es-iSeleted');
                    ctrl.ElementRowCheck = ctrl.allRow[i + 1].row;
                    $('td', ctrl.ElementRowCheck).addClass("es-iSeleted");
                    ctrl.iSelected = ctrl.allRow[i + 1].data;
                    ctrl.initData(ctrl.iSelected.RowGuid);
                    break;
                }
            }
    }
    ctrl.upRow = function (d) {
        if (ctrl.ElementRowCheck === -1) {
            if (ctrl.allRow.length > 1) {
                ctrl.ElementRowCheck = ctrl.allRow[ctrl.allRow.length - 1].row;
                $('td', ctrl.ElementRowCheck).addClass("es-iSeleted");
                ctrl.iSelected = ctrl.allRow[ctrl.allRow.length - 1].row;
                ctrl.initData(ctrl.iSelected.RowGuid);
            }
        } else
            for (var i = 0; i < ctrl.allRow.length; i++) {
                if (ctrl.allRow[i].row === ctrl.ElementRowCheck) {
                    if (i === 0) break;
                    $('td', ctrl.ElementRowCheck).removeClass('es-iSeleted');
                    ctrl.ElementRowCheck = ctrl.allRow[i - 1].row;
                    $('td', ctrl.ElementRowCheck).addClass("es-iSeleted");
                    ctrl.iSelected = ctrl.allRow[i - 1].data;
                    ctrl.initData(ctrl.iSelected.RowGuid);
                    break;
                }
            }
    }
    keyhandle.eventCtrl(40, ctrl.nextRow);
    keyhandle.eventCtrl(38, ctrl.upRow);
    ctrl.ElementRowCheck = -1;
    ctrl.allRow = [];
    ctrl.iSelected = {};
    ctrl.dataScrollPage = [];
    $scope.staticParam = {
        totalItems: 0,
        currentPage: 1,
        pageSize: 25,
        maxSize: 5,
        Search: "",
        Status: "0",
        RowGuid: "",
        OrderID: ""
    };

    $scope.selected = [];
    $scope.selectAll = false;
    $scope.toggleOne = toggleOne;
    $scope.toggleAll = toggleAll;

    var titleHtml = '<label style="text-align: center;" class="mt-checkbox" ><input type="checkbox" ng-model="selectAll" ng-click="toggleAll(selectAll, selected)"/><span></span></label>';
    //if (App.Permissions.LISTVIEW)
    vm.dtOptions = DTOptionsBuilder.newOptions()
        .withPaginationType('full_numbers')
        .withDataProp('data').withDisplayLength(15)
        .withOption('order', [2, 'desc'])
        .withOption('serverSide', true)
        .withOption('pageLength', 25)
        .withOption('info', false)
        .withOption('stateLoadParams', function (settings, data) {
            data.search.search = '';
        })
        .withOption('headerCallback', function (header) {
            if (!$scope.headerCompiled) {
                $scope.headerCompiled = true;
                $compile(angular.element(header).contents())($scope);
            }
        })
        .withOption('initComplete', function (settings, json) {
            //thêm sự kiện scroll phân trang
            //  $('#tblData').parent().attr("onscroll", "angular.element(this).scope().LoadScroll(this)");
            PLUGIN_DATATABLE_TABLE_SCROLL.INIT_EVENT('tblData', $scope.LoadScroll);

        })
        .withOption('createdRow', function (row, data, dataIndex) {
            const contextScope = $scope.$new(true);
            contextScope.data = data;
            $scope.data = data;
            if (data._STT.toString() === "1") {
                if (ctrl.allRow.length === 0) {
                    $('td', row).addClass("es-iSeleted");
                    vm.ElementRowCheck = row;
                    ctrl.ElementRowCheck = row;
                    ctrl.iSelected = data;
                } else {
                    if (ctrl.allRow[0] !== null) {
                        $('td', ctrl.allRow[0].row).addClass("es-iSeleted");
                        ctrl.ElementRowCheck = ctrl.allRow[0].row;
                        ctrl.iSelected = ctrl.allRow[0].data;
                        vm.staticParam.RowGuid = ctrl.allRow[0].data.RowGuid;
                    }
                }

            }
            $compile(row)($scope);
            contextScope.contextMenu = $scope.contextMenu;
            $compile(angular.element(row).find('input'))($scope);
            $compile(angular.element(row).attr('context-menu', 'contextMenu'))(contextScope);
        });
    // if (App.Permissions.LISTVIEW)
    vm.dtOptions.withOption('ajax', function (data, callback, settings) {
        data.Keyword = $scope.staticParam.Search;
        data.StartDate = $scope.staticParam.StartDate;
        data.EndDate = $scope.staticParam.EndDate;
        data.lstString = [];
        App.blockUI({
            target: "#tblData",
            boxed: true,
            message: 'Đang tải...'
        });
        $http.post('/ProductionManager/JTable', data)
            .success(function (res) {
                $scope.selected = [];
                if (data.start === 0) {
                    $scope.dataload = [];
                    angular.forEach(res.data, function (val, key) {
                        $scope.dataload.push(val);
                    });
                    callback({
                        data: $scope.dataload,
                        recordsTotal: res.recordsTotal,
                        recordsFiltered: res.recordsFiltered
                    });
                } else {
                    if (res.recordsTotal > $scope.dataload.length) {
                        angular.forEach(res.data, function (val, key) {
                            $scope.dataload.push(val);
                        });
                        callback({
                            data: $scope.dataload,
                            recordsTotal: res.recordsTotal,
                            recordsFiltered: res.recordsFiltered
                        });
                    }
                }
                for (var i = 0; i < ctrl.allRow.length; i++) {
                    if (ctrl.allRow[i].data.RowGuid === ctrl.iSelected.RowGuid) {
                        $('td', ctrl.allRow[i].row).addClass("es-iSeleted");
                        ctrl.ElementRowCheck = ctrl.allRow[i].row;
                        ctrl.initData(ctrl.iSelected.RowGuid);
                        break;
                    }
                }
                if ($scope.dataload.length > 0) {
                    ctrl.initData($scope.dataload[0].RowGuid);
                } else {
                    ctrl.initData(null);
                }
                PLUGIN_DATATABLE_TABLE_SCROLL.UNLOCK_EVENT('tblData');
                App.unblockUI("#tblData");
            });
    });
    //scroll theo tỷ lệ màn hình
    vm.dtOptions.withOption('scrollY', '70vh')
        .withOption('scrollX', '100%') //mặc định 100%
        .withOption('scrollCollapse', true) //xóa khoảng trắng khi dữ liệu trong bảng ít
        .withOption('rowCallback', rowCallback) //sự kiên click vào 1 dòng
        .withOption('scroller', {
            loadingIndicator: true
        });
    //.withFixedColumns({
    //    leftColumns: 0 //fix bên trái 2 cột
    //    //rightColumns: 1 //fix bên phải 1 cột
    //}
    //);
    //table
    vm.dtColumns = [];

    vm.dtColumns.push(DTColumnBuilder.newColumn('RowGuid').withTitle('STT').notSortable().withOption('sWidth', '5px').renderWith(function (data, type, full, meta) {
        return meta.row + 1;
    }).withOption('sClass', 'tcenter tcenter-header'));
    vm.dtColumns.push(DTColumnBuilder.newColumn('CodeID').withTitle('Mã báo giá').withOption('sWidth', '60px').withOption('sClass', 'tcenter-header').renderWith(function (data, type, full, meta) {
        return '<a  style="color: #4285f4;" href="" ng-click="open(\'' + full.RowGuid + '\')" >' + data + '</a>';
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('CustomerID').withTitle('Mã khách hàng').withOption('sWidth', '80px').withOption('sClass', 'tcenter-header').renderWith(function (data, type, full, meta) {
        return data;
    }));

    vm.dtColumns.push(DTColumnBuilder.newColumn('CustomerName').withTitle('Tên khách hàng').withOption('sWidth', '150px').withOption('sClass', 'tcenter-header').renderWith(function (data, type, full, meta) {
        return data;
    }));

    vm.dtColumns.push(DTColumnBuilder.newColumn('EffectiveDate').withTitle('Ngày hiệu lực').withOption('sWidth', '60px').withOption('sClass', 'tcenter').renderWith(function (data, type, full, meta) {
        return full.EffectiveDatetring;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('DateSX').withTitle('Ngày báo giá').withOption('sWidth', '60px').withOption('sClass', 'tcenter').renderWith(function (data, type, full, meta) {
        return full.DateSXString;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('EmployeeID').withTitle('Người thực hiện').withOption('sClass', 'tleft').withOption('sWidth', '100px').renderWith(function (data, type, full) {
        return data;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('EmployeeName').withTitle('Người theo dõi').withOption('sClass', 'tleft').withOption('sWidth', '100px').renderWith(function (data, type, full) {
        return data;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('RowGuid').notSortable().withTitle('Điều khiển').notSortable().withOption('sWidth', '40px').withOption('sClass', 'tcenter').renderWith(function (data, type, full, meta) {
        return '<div class="table__cell-actions-wrap">' +
            '<a ng-click="open(\'' + full.RowGuid + '\')" class="table__cell-actions-item table__cell-actions-icon"><span class="mdi mdi-eye" ></span></a > ' +
            '<a href="javascript:;" ng-click="edit(\'' + full.RowGuid + '\')" class="table__cell-actions-item table__cell-actions-icon es-cursor"> <span class="mdi mdi-table-edit" title="Sửa"></span> </a>' +
            '<a ng-click="delete (\'' + full.RowGuid + '\')" class="table__cell-actions-item table__cell-actions-icon"><span class="mdi mdi-delete" title="Xóa"></span> </a>';

    }));
   
    vm.reloadData = reloadData;
    vm.dtInstance = {};
    function reloadData(resetPaging) {
        $scope.staticParam.currentPage = resetPaging;
        vm.dtInstance.reloadData(callback, resetPaging);
    }
    function callback(json) {

    }
    function toggleOne(selectedItems) {
        for (var id in selectedItems) {
            if (selectedItems.hasOwnProperty(id)) {
                if (!selectedItems[id]) {
                    vm.selectAll = false;
                    return;
                }
            }
        }
        vm.selectAll = true;
    }
    function toggleAll(selectAll, selectedItems) {
        for (var id in selectedItems) {
            if (selectedItems.hasOwnProperty(id)) {
                selectedItems[id] = selectAll;
            }
        }
    }
    $('#minize-menuleft').click(function () {
        vm.dtInstance.DataTable.draw();
    });
    function rowCallback(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        if (iDisplayIndexFull === 0) ctrl.allRow = [];
        $('td', nRow).unbind('click');
        $('td', nRow).bind('click', function () {
            if (vm.ElementRowCheck === nRow) return;
            $('td', nRow).addClass("es-iSeleted");
            $('td', vm.ElementRowCheck).removeClass('es-iSeleted');
            vm.ElementRowCheck = nRow;
            ctrl.ElementRowCheck = nRow;
            ctrl.iSelected = aData;
            vm.staticParam.RowGuid = aData.RowGuid;
            vm.staticParam.OrderID = aData.OrderID;
            $scope.initData(vm.staticParam.RowGuid);
        });
        $('td', nRow).unbind('dblclick');
        $('td', nRow).bind('dblclick', function () {
            $scope.$apply(function () {
                $scope.open(aData.RowGuid);
            });
        });
        ctrl.allRow.push({ row: nRow, data: aData });
        return nRow;
    }
    //Load dữ liệu cuộn
    $scope.total = 0;
    $scope.check = false;
    $scope.LoadScroll = function (obj) {
        vm.dtInstance.DataTable.page('next').draw('page');
    };

    $scope.reloadAll = function () {
        $scope.staticParam.Search = "";
        $scope.selectAll = false;
        reloadData(true);
    };
    $rootScope.reload = function () {
        $scope.dataload = [];
        reloadData(true);
    };
    ctrl.changeOptionDate = function (d) {
        if (d.length === 0) {
            ctrl.staticParam.StartDate = null;
            ctrl.staticParam.EndDate = null;
        } else {
            ctrl.staticParam.StartDate = $rootScope.ConDate(d[0], 2);
            ctrl.staticParam.EndDate = $rootScope.ConDate(d[1], 2);
        }
    }
    $scope.$watch('staticParam.StartDate', function (newvalue, oldvalue) {
        if (newvalue !== undefined && newvalue !== oldvalue) {
            $scope.reload();
        }
    });
    $scope.$watch('staticParam.EndDate', function (newvalue, oldvalue) {
        if (newvalue !== undefined && newvalue !== oldvalue) {
            $scope.reload();
        }
    });
    $scope.add = function () {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/add.html',
            controller: 'add',
            backdrop: 'static',
            keyboard: false,
            size: '100',
            resolve: {
                para: function () {
                    return "";
                }
            }
        });
        modalInstance.result.then(function (d) {
            $scope.reload();
        }, function () {
        });
    }
    $scope.edit = function (Id) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/add.html',
            controller: 'add',
            backdrop: 'static',
            keyboard: false,
            size: '100',
            resolve: {
                para: function () {
                    return Id;
                }
            }
        });
        modalInstance.result.then(function (d) {
            $scope.reload();
        }, function () {
        });


    }
    $scope.open = function (Id) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/open.html',
            controller: 'open',
            backdrop: 'static',
            keyboard: false,
            size: '100',
            resolve: {
                para: function () {
                    return Id;
                }            
            }
        });
        modalInstance.result.then(function (d) {
            $scope.reload();
        }, function () {
        });
    }
    $scope.delete = function (Id) {
        dataservice.getItem({ IdS: [Id] }, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {
                ESConfirm([{ 'value': 1, 'title': 'Xác nhận', 'type': 'info' }, { 'value': 2, 'title': 'Đóng', 'type': 'danger' }], { 'description': 'Bạn có muốn xóa ' + JSON.parse(rs.Table1)[0].CodeID, 'class': 'eswarning_v2' }, function (rs) {
                    if (rs === '1') {
                        App.blockUI({
                            target: "#contentMain",
                            boxed: true,
                            message: 'Đang tải...'
                        });
                        dataservice.IsDelete({ IdS: [Id] }, function (result) {
                            if (result.Error) {
                                App.notifyDanger(result.Title);
                            } else {
                                App.notifyInfo(result.Title);
                                $rootScope.reload();
                            }
                            App.unblockUI("#contentMain");
                        });
                    }
                });

            }
        })
    }
    $scope.deleteChecked = function () {
        var deleteItems = [];
        for (var id in $scope.selected) {
            if ($scope.selected.hasOwnProperty(id)) {
                if ($scope.selected[id]) {
                    deleteItems.push(id);
                }
            }
        }
        if (deleteItems.length > 0) {
            $confirm({ text: 'Bạn có chắc chắn muốn xóa các khoản mục đã chọn?', title: 'Xác nhận', ok: 'Đồng ý', cancel: ' Đóng' })
                .then(function () {
                    App.blockUI({
                        target: "#contentMain",
                        boxed: true,
                        message: 'Đang tải...'
                    });
                    dataservice.deleteItems(deleteItems, function (result) {
                        if (result.Error) {
                            App.notifyDanger(result.Title);
                        } else {
                            App.notifyInfo(result.Title);
                            $scope.reload();
                        }
                        App.unblockUI("#contentMain");
                    });

                });
        } else {
            App.notifyDanger("Không có khoản mục nào được chọn");
        }
    }
    //comment
    $scope.comment = function (Id) {
        dataservice.getItem({ IdS: [Id] }, function (rs) {
            loadTMStart();
            vm.staticParam.RowGuid = Id;
            $scope.txtComment = JSON.parse(rs.Table1)[0].CodeID;
            $scope.liComment = [];
            $scope.initPara = { Page: 1, ItemPage: 15, RecordGuid: vm.staticParam.RowGuid };
            $scope.loadDataComment();
        });

    };

    //xử lý phiếu
    $scope.approve = function (temp, permisstion) {
        if (permisstion === "1") {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: ctxfolder + '/approve.html',
                controller: 'approve',
                backdrop: true,
                size: 'md',
                resolve: {
                    para: function () {
                        return temp;
                    }
                }
            });
            modalInstance.result.then(function (d) {
                $scope.reload();
            }, function () {
            });
        } else {
            App.notifyDanger("Bạn không có quyền xử lý");
            return;
        }
    };
    $scope.contextMenu = [
        [function ($itemScope) {
            return '<i class="mdi mdi-eye"></i> Xem';
        }, function ($itemScope, $event, model) {
            $scope.open($itemScope.data.RowGuid);
        }, function ($itemScope, $event, model) {
            return true;
        }],
        [function ($itemScope) {
            return '<i class="mdi mdi-table-edit"></i> Sửa';
        }, function ($itemScope, $event, model) {
            $scope.edit($itemScope.data.RowGuid);
        }, function ($itemScope, $event, model) {
            return true;
        }],
        [function ($itemScope) {
            return '<i class="mdi mdi-delete"></i> Xóa';
        }, function ($itemScope, $event, model) {
            $scope.delete($itemScope.data.RowGuid);
        }, function ($itemScope, $event, model) {
            return true;
            }],
        [function ($itemScope) {
            return '<i class="mdi mdi-attachment"></i> Đính kèm';
        }, function ($itemScope, $event, model) {
            $scope.AttchmentOrder($itemScope.data.RowGuid);
        }, function ($itemScope, $event, model) {
            return true;
        }],
        //[function ($itemScope) {
        //    return '<i class="mdi mdi-comment-outline"></i> Ý Kiến';
        //}, function ($itemScope, $event, model) {
        //    loadTMStart();
        //    vm.staticParam.RowGuid = $itemScope.data.RowGuid;
        //    $scope.txtComment = $itemScope.data.CodeID;
        //    $scope.liComment = [];
        //    $scope.initPara = { Page: 1, ItemPage: 15, RecordGuid: vm.staticParam.RowGuid };
        //    $scope.loadDataComment();
        //}],
    ];
    $scope.AttchmentOrder = function (data) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/addAttchment.html',
            controller: 'addattchment',
            backdrop: 'static',
            size: '80',
            resolve: {
                para: function () {
                    return data;
                },
                data: function () {
                    return "";
                },
                para1: function () {
                    return $scope.openAttach;
                }
            }
        });
    }
    $scope.addCustomers = function () {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/Customers_index.html',
            controller: 'Customers_index',
            backdrop: 'static',
            keyboard: false,
            size: '100',
            resolve: {
                para: function () {
                    return "";
                }
            }
        });
        modalInstance.result.then(function (d) {
        }, function () {
        });
    };

    //Comment
    $scope.model = {};
    $scope.initPara = { Page: 1, ItemPage: 15, RecordGuid: null };
    $scope.txtComment = "";
    $scope.check_commnent = false;
    $scope.total_comment = 0;
    $scope.LoadScroll_commnet = function (obj) {

        var total = obj.offsetHeight + obj.scrollTop;
        if (obj.offsetHeight + obj.scrollTop >= obj.scrollHeight && $scope.check_commnent && $scope.total_comment < total) {
            $scope.check_commnent = false;
            try {
                $scope.IconLoad = true;
                $scope.initPara.Page = $scope.initPara.Page + 1;
                $scope.loadDataComment();
            }
            catch (ex) {
            }
        }
        if (obj.offsetHeight + obj.scrollTop >= obj.scrollHeight) {
            $scope.total_comment = obj.offsetHeight + obj.scrollTop;
        }
        else {
            $scope.check_commnent = true;
        }
    }
    $scope.liComment = [];
    $scope.loadDataComment = function () {
        if ($scope.initPara.RecordGuid !== null && $scope.initPara.RecordGuid !== "" && $scope.initPara.RecordGuid !== undefined) {
            dataservice.GetProcessWF($scope.initPara.RecordGuid, function (rs) {
                if (rs.Error) {
                    App.notifyDanger(rs.Title);
                } else {
                    $scope.lstProcess = rs.item;
                }
            })
        }
        if ($scope.initPara.RecordGuid !== "" && $scope.initPara.RecordGuid !== null && $scope.initPara.RecordGuid !== undefined) {
            dataservice.GetAllCommentPage($scope.initPara, function (rs) {
                if (rs.Error) {
                    App.notifyDanger(rs.Title);
                } else {
                    $scope.IconLoad = false;
                    for (var i = 0; i < rs.Object.length; i++) {
                        $scope.liComment.unshift(rs.Object[i]);
                    }
                    $scope.total_comment = $scope.liComment.length;

                    angular.forEach($scope.liComment, function (value, key) {
                        if (value.CreatedBy.split('#')[0] === $rootScope.loginName) {
                            value.Class = "is-interlocutor";
                        } else {
                            value.Class = "is-self";
                        }

                        if (value.CommentAttachments !== null) {
                            var listAttachments = [];
                            if (value.CommentAttachments.includes('|')) {
                                var data = value.CommentAttachments.split('|');

                                var obj = {
                                    AttachmentGuid: data[0],
                                    FileName: data[1],
                                    checksrc: false
                                };
                                if (data[1].includes('.')) {
                                    obj.FileExtension = data[1].split('.')[1];
                                } else {
                                    obj.FileExtension = data[1];
                                }
                                if (!obj.FileExtension.includes('xlsx') && !obj.FileExtension.includes('xls') && !obj.FileExtension.includes('png') && !obj.FileExtension.includes('docx') && !obj.FileExtension.includes('doc') && !obj.FileExtension.includes('pdf') && !obj.FileExtension.includes('txt') && !obj.FileExtension.includes('png') && !obj.FileExtension.includes('jpg')) {
                                    obj.checksrc = false;
                                } else {
                                    obj.checksrc = true;
                                }
                                listAttachments.push(obj);
                            } else {
                                var data1 = value.CommentAttachments;
                                angular.forEach(data1, function (val, keys) {
                                    var obj = {
                                        AttachmentGuid: val.AttachmentGuid,
                                        FileName: val.FileName
                                    }
                                    if (val.FileExtension.includes('.')) {
                                        obj.FileExtension = val.FileExtension.split('.')[1];
                                    } else {
                                        obj.FileExtension = val.FileExtension;
                                    }
                                    if (!obj.FileExtension.includes('xlsx') && !obj.FileExtension.includes('xls') && !obj.FileExtension.includes('png') && !obj.FileExtension.includes('docx') && !obj.FileExtension.includes('doc') && !obj.FileExtension.includes('pdf') && !obj.FileExtension.includes('txt') && !obj.FileExtension.includes('png') && !obj.FileExtension.includes('jpg')) {
                                        obj.checksrc = false;
                                    } else {
                                        obj.checksrc = true;
                                    }
                                    listAttachments.push(obj);
                                });
                            }
                            value.CommentAttachments = listAttachments;

                        }
                    });
                    gotoBottom('setting-content-scroll');
                }
            })
        }

    }
    //  $scope.loadDataComment();
    $scope.insertcomment = function () {
        if ($scope.model.Comment === undefined || $scope.model.Comment === "") {
            App.notifyDanger("Bạn phải nhập ý kiến");
            return true;
        }
        var fd = new FormData();
        $scope.model.RecordGuid = vm.staticParam.RowGuid;
        var file = $('#file-comment-id').prop('files')[0];
        var message = $('#message').val().replace(/(\n)+/g, '<br />');
        $scope.model.Comment = message;
        fd.append('insert', JSON.stringify($scope.model));
        fd.append('file', file);
        $.ajax({
            type: "POST",
            url: "/ProductionManager/InsertComments",
            contentType: false,
            processData: false,
            data: fd,
            success: function (rs) {
                if (rs.Error) {
                    App.notifyDanger(rs.Title);
                }
                else {
                    $('#message').val('');
                    $scope.model.Comment = "";
                    $scope.NameFile = undefined;
                    $("#file-comment-id").val('');
                    $scope.liComment = [];
                    $scope.initPara = { Page: 1, ItemPage: 15, RecordGuid: vm.staticParam.RowGuid };
                    $scope.loadDataComment();
                }
            }
        });
    }
    $scope.FileNameChnage = '';
    $scope.addNameFile = function () {
        if ($('#file-comment-id').prop('files')[0] !== undefined)
            if ($scope.model !== undefined && $scope.model.Comment !== '') {
                $scope.model.Comment += " " + $('#file-comment-id').prop('files')[0].name;
            } else {
                $scope.model.Comment = " " + $('#file-comment-id').prop('files')[0].name;
            }
        if (!$scope.$$phase) {
            $scope.$apply();
        }
    }
    function loadTMStart() {
        setTimeout(_loadTMStart, 100);
    }
    function _loadTMStart() {
        var _el = document.getElementsByClassName('settings-panel')[0];
        _el.classList.add('is-opened');
    }
    $scope.deleteItem = function (element, row) {
        $scope.ListData = $scope.jxcelAddDetail.jexcel('getData');
        if ($scope.ListData.length > 1) {
            ESConfirm([{ 'value': 1, 'title': 'Xác nhận', 'type': 'info' }, { 'value': 2, 'title': 'Đóng', 'type': 'danger' }], { 'description': 'dữ liệu này không ?', 'class': 'eswarning' }, function (rs) {
                if (rs === '1') {
                    var cell = 'A' + (parseInt(row) + 1);
                    var id = $scope.jxcelAddDetail.jexcel('getValue', cell);
                    if (id !== "" && id !== null) {
                        var sta = false;
                        for (var i = 0; i < $scope.ListDetailDelete.length; i++) {
                            if ($scope.ListDetailDelete[i] === id) {
                                sta = true;
                            }
                        }
                        if (!sta) {
                            $scope.ListDetailDelete.push(id);
                        }
                    }
                    $scope.jxcelAddDetail.jexcel('deleteRow', row);
                    $scope.onload1();

                    App.notifyInfo('Xóa thành công');
                }
                if (rs === '2') {
                    console.log('Không chấp nhận');
                }
            }, function () {
                //some error in this function
                console.log('Không lựa chọn');
            });
        }
        else {
            App.notifyDanger('Không xóa được dòng cuối cùng này');
            return;
        }
    };


    $scope.rowchange = function (obj, row, cell, columnName) {

        $scope.onload1();
    };
    $scope.roundNumber = function (number) {
        if (number > 0) {
            return parseFloat(number);
        } else return 0;

    }
    $scope.onload1 = function () {
        if ($('#myhtindex').jexcel('getData') !== undefined) {
            $rootScope.Quantity = 0;
            $rootScope.AmountOc = 0;
            $rootScope.UnitPrice = 0;
            $rootScope.VATAmountOC = 0;
            $scope.ListData = $('#myhtindex').jexcel('getData');
            angular.forEach($scope.ListData, function (val, key) {
                if (val.Quantity !== null && val.Quantity !== "" && val.Quantity !== undefined) {
                    $rootScope.Quantity += $scope.roundNumber(parseFloat(val.Quantity));
                }
            });
            $('#myhtindex #colfoot-9').html($rootScope.addPeriod($scope.Quantity));
            if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                $scope.$apply();
            }
        }
    }

    $scope.dropdownItemId = function (d1, d2, d3) {
        var _tem1 = d1.ItemID;
        if (_tem1 === "" || _tem1 === null) {
            _tem1 = "";
        }
        var obj = {
            Keyword: _tem1
        }
        dataservice.getItemALL(obj, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {

                d3.source = rs;
                d2(rs, "");
            }
        })
    }
    $scope.dropdownUnitID = function (d1, d2, d3) {
        var _tem1 = d1.UnitID;
        if (_tem1 === "" || _tem1 === null) {
            _tem1 = "";
        }
        var obj = {
            Keyword: _tem1
        }
        dataservice.getUnitJexcel(obj, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {

                d3.source = rs;
                d2(rs, "");
            }
        })
    }

    $scope.jxcelAddDetailInit = {
        dataObject: true,
        reloadData: {},
        data: [],
        colHeaders: ['', 'Mã sản phẩm', 'Tên sản phẩm', "Quy cách sản phẩm", "Quy cách đóng gói", 'Tại Xưởng', 'Mã đơn vị tính', 'Tên đơn vị tính', 'Loại ngoại tệ', 'Số lượng', 'Ghi chú'
        ],
        colTitles: ['', 'Mã sản phẩm', 'Tên sản phẩm', "Quy cách sản phẩm", "Quy cách đóng gói", 'Tại Xưởng', 'Mã đơn vị tính', 'Tên đơn vị tính', 'Loại ngoại tệ', 'Số lượng', 'Ghi chú'
        ],
        colFooters: ['', '', '', '', '', '', '', '', '', '', ''
        ],
        columns: [
            {
                name: 'RowGuid',
                type: 'text',
                readOnly: true,
            },
            {
                name: 'ItemID',
                type: 'text',
                readOnly: true,
            },
            {
                name: 'ItemName',
                type: 'text',
                readOnly: true,
            },
            {
                name: 'ProductSpecifications',
                type: 'text', readOnly: true,
            },
            {
                name: 'Packing',
                type: 'text', readOnly: true,
            },
            {
                name: 'FactoryType',
                type: 'autocomplete',
                url: '/Sale/ProductionManager/FactoryType/',
                readOnly: false,
                updateReadOnly: true, readOnly: true,
            },
            {
                name: 'UnitID',
                type: 'autocomplete',
                source: $rootScope.litsUnitID,
                sourceReload: $scope.dropdownUnitID,
                searchReload: true,
                onlyCode: true,
                isFixColumn: true, readOnly: true,
            },
            {
                name: 'UnitName',
                type: 'text'
                , readOnly: true,
            },
            {
                name: 'Title',
                type: 'autocomplete',
                url: '/Sale/ProductDeparments/MoneyType/',
                readOnly: false,
                updateReadOnly: true, readOnly: true,
            },
            {
                name: 'Quantity',//Xếp hạng 3
                type: 'numeric', formatNumber: 'VN-NUMERIC', readOnly: true,
            },

            {
                name: 'Note',
                type: 'text', readOnly: true,
            }
        ],

        columnShow: [false, true, true, true, true, true, true, true, true, true, true],
        initDataRows: ['', '', '', '', '', '', '', '', '', '', ''],
        colWidths: [50, 150, 200, 200, 150, 150, 150, 150, 100, 150, 300],
        allrowevent: true,
        deleterowconfirm: $scope.deleteItem,
        tableOverflow: true,
        csvHeaders: true,
        changeCell: $scope.rowchange,
        tableHeight: '200px',
        onpaste: $scope.onpaste1,
        onload: $scope.onload1,
        allowInsertRow: false,
        contextMenu: function () { return ""; },
    };
    $scope.initData = function (para) {
        if (para == null) return;
        dataservice.getItem({ IdS: [para] }, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {
                $scope.model = JSON.parse(rs.Table1)[0];
                $scope.model.DateSX !== null ? $scope.model.DateSX = "/Date(" + new Date($scope.model.DateSX).getTime() + ")/" : undefined;
                $scope.model.EffectiveDate !== null ? $scope.model.EffectiveDate = "/Date(" + new Date($scope.model.EffectiveDate).getTime() + ")/" : undefined;
                $scope.lstDetail = JSON.parse(rs.Table2);
                var ApplyOutsideDetails = [];
                angular.forEach($scope.lstDetail, function (value, key) {
                    ApplyOutsideDetails.push([
                        value.RowGuid,
                        value.ItemID,
                        value.ItemName,
                        value.ProductSpecifications,
                        value.Packing,
                        value.FactoryType,
                        value.UnitID,
                        value.UnitName,
                        value.Title,
                        value.Quantity,
                        value.Note
                    ]);
                });
                $scope.jxcelAddDetailInit.data = ApplyOutsideDetails;
                $scope.onload1();
                if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                    $scope.$apply();
                }
            }
        })
    }
});
function gotoBottom(id) {
    var element = document.getElementById(id);
    element.scrollTop = 10;/* element.scrollHeight - element.clientHeight;*/
}
app.controller('add', function ($scope, $rootScope, $compile, $uibModal, $confirm, $uibModalInstance, FileUploader, dataservice, para) {
    $scope.model = { Temps: "" };
    $scope.disColumn = false;
    $scope.check = false;
    $scope.fullscreen = function (Check) {
        if (Check == false) {
            $scope.check = true;
        } else {
            $scope.check = false;
        }
    }
    $scope.ListDetailDelete = [];
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    }

    $scope.model.FileAttachments = [];
    var uploader = $scope.uploader = new FileUploader({
        // url: App.baseService() + '/upload'
    });
    uploader.filters.push({
        name: "customFilter",
        fn: function (item /*{File|FileLikeObject}*/, options) {
            return this.queue.length < 10;
        }
    });
    uploader.onSuccessItem = function (fileItem, response, status, headers) {
        $scope.jmodel.FileAttachments.push(response);
    };
    $scope.jmodelPer = {};
    uploader.onCompleteAll = function () {
        $scope.commit();
    };

    $scope.ChangeCustomerID = function (data) {
        $scope.model.CustomerID = data == "null" ? "" : data;
    };

    $scope.deleteItem = function (element, row) {
        $scope.ListData = $scope.jxcelAddDetail.jexcel('getData');
        if ($scope.ListData.length > 1) {
            ESConfirm([{ 'value': 1, 'title': 'Xác nhận', 'type': 'info' }, { 'value': 2, 'title': 'Đóng', 'type': 'danger' }], { 'description': 'dữ liệu này không ?', 'class': 'eswarning' }, function (rs) {
                if (rs === '1') {
                    var cell = 'A' + (parseInt(row) + 1);
                    var id = $scope.jxcelAddDetail.jexcel('getValue', cell);
                    if (id !== "" && id !== null) {
                        var sta = false;
                        for (var i = 0; i < $scope.ListDetailDelete.length; i++) {
                            if ($scope.ListDetailDelete[i] === id) {
                                sta = true;
                            }
                        }
                        if (!sta) {
                            $scope.ListDetailDelete.push(id);
                        }
                    }
                    $scope.jxcelAddDetail.jexcel('deleteRow', row);
                    $scope.onload1();

                    App.notifyInfo('Xóa thành công');
                }
                if (rs === '2') {
                    console.log('Không chấp nhận');
                }
            }, function () {
                //some error in this function
                console.log('Không lựa chọn');
            });
        }
        else {
            App.notifyDanger('Không xóa được dòng cuối cùng này');
            return;
        }
    };


    $scope.rowchange = function (obj, row, cell, columnName) {
        var rowindex = parseInt(row + 1); // dòng muốn thay đổi      
        $scope.ListData = $scope.jxcelAddDetail.jexcel('getData');
        //if (columnName === "ItemID") {
        //    dataservice.getItem_by_ItemId({ IdS: [$scope.ListData[row].ItemID] }, function (rs) {
        //        if (rs.Error) {
        //            App.notifyDanger(rs.Title);
        //        } else {
        //            $scope.jxcelAddDetail.jexcel("setValueByName", "ItemName", row, rs.ItemName);
        //            $scope.jxcelAddDetail.jexcel("setValueByName", "UnitID", row, rs.UnitID);
        //            dataservice.getUnit_by_ItemId({ IdS: [rs.UnitID] }, function (rs) {
        //                if (rs.Error) {
        //                    App.notifyDanger(rs.Title);
        //                } else {

        //                    $scope.jxcelAddDetail.jexcel("setValueByName", "UnitName", row, rs.UnitName);
        //                    if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
        //                        $scope.$apply();
        //                    }
        //                }
        //            });
        //            if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
        //                $scope.$apply();
        //            }
        //        }
        //    })
        //}
        if (columnName === "UnitID") {
            if ($scope.ListData[row].UnitID != "") {
                dataservice.getUnit_by_ItemId({ IdS: [$scope.ListData[row].UnitID] }, function (rs) {
                    if (rs.Error) {
                        App.notifyDanger(rs.Title);
                    } else {
                        $scope.jxcelAddDetail.jexcel("setValueByName", "UnitName", row, rs.UnitName);
                        if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                            $scope.$apply();
                        }
                    }
                });
            } else {
                $scope.jxcelAddDetail.jexcel("setValueByName", "UnitName", row, "");
                if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                    $scope.$apply();
                }
            }
        }
        //if (columnName === "Quantity" || columnName === "UnitPrice") {
        //    var Quantity = parseFloat($scope.ListData[row].Quantity);
        //    var UnitPrice = parseFloat($scope.ListData[row].UnitPrice);
        //    var ExchangeRate = parseFloat($scope.ListData[row].ExchangeRate);
        //    var AmountOc = Quantity * UnitPrice * ExchangeRate;
        //    $scope.jxcelAddDetail.jexcel("setValueByName", "AmountOc", row, AmountOc);
        //}
        $scope.onload1();
    };
    $scope.roundNumber = function (number) {
        if (number > 0) {
            return parseFloat(number);
        } else return 0;

    }
    $scope.onload1 = function () {
        if ($('#myht').jexcel('getData') !== undefined) {
            $rootScope.Quantity = 0;
            $rootScope.AmountOc = 0;
            $rootScope.UnitPrice = 0;
            $rootScope.VATAmountOC = 0;
            $scope.ListData = $('#myht').jexcel('getData');
            angular.forEach($scope.ListData, function (val, key) {
                if (val.Quantity !== null && val.Quantity !== "" && val.Quantity !== undefined) {
                    $rootScope.Quantity += $scope.roundNumber(parseFloat(val.Quantity));
                }           
            });
            $('#myht #colfoot-9').html($rootScope.addPeriod($scope.Quantity));            
            if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                $scope.$apply();
            }
        }
    }

    $scope.dropdownItemId = function (d1, d2, d3) {
        var _tem1 = d1.ItemID;
        if (_tem1 === "" || _tem1 === null) {
            _tem1 = "";
        }
        var obj = {
            Keyword: _tem1
        }
        dataservice.getItemALL(obj, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {

                d3.source = rs;
                d2(rs, "");
            }
        })
    }
    $scope.dropdownUnitID = function (d1, d2, d3) {
        var _tem1 = d1.UnitID;
        if (_tem1 === "" || _tem1 === null) {
            _tem1 = "";
        }
        var obj = {
            Keyword: _tem1
        }
        dataservice.getUnitJexcel(obj, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {

                d3.source = rs;
                d2(rs, "");
            }
        })
    }

    $scope.jxcelAddDetailInit = {
        dataObject: true,
        reloadData: {},
        data: [],
        colHeaders: ['', 'Mã sản phẩm', 'Tên sản phẩm', "Quy cách sản phẩm", "Quy cách đóng gói", 'Tại Xưởng', 'Mã đơn vị tính', 'Tên đơn vị tính', 'Loại ngoại tệ', 'Số lượng', 'Ghi chú'
        ],
        colTitles: ['', 'Mã sản phẩm', 'Tên sản phẩm', "Quy cách sản phẩm", "Quy cách đóng gói", 'Tại Xưởng', 'Mã đơn vị tính', 'Tên đơn vị tính', 'Loại ngoại tệ', 'Số lượng', 'Ghi chú'
        ],
        colFooters: ['', '', '', '', '', '', '', '', '', '', ''
        ],
        columns: [
            {
                name: 'RowGuid',
                type: 'text'
            },
            {
                name: 'ItemID',
                type: 'text'
            },
            {
                name: 'ItemName',
                type: 'text'
            },
            {
                name: 'ProductSpecifications',
                type: 'text'
            },
            {
                name: 'Packing',
                type: 'text'
            },
            {
                name: 'FactoryType',
                type: 'autocomplete',
                url: '/Sale/ProductionManager/FactoryType/',
                readOnly: false,
                updateReadOnly: true
            },
            {
                name: 'UnitID',
                type: 'autocomplete',
                source: $rootScope.litsUnitID,
                sourceReload: $scope.dropdownUnitID,
                searchReload: true,
                onlyCode: true
            },
            {
                name: 'UnitName',
                type: 'text'
            },
            {
                name: 'Title',
                type: 'autocomplete',
                url: '/Sale/ProductDeparments/MoneyType/',
                readOnly: false,
                updateReadOnly: true
            },
            {
                name: 'Quantity',//Xếp hạng 3
                type: 'numeric', formatNumber: 'VN-NUMERIC'
            },
            {
                name: 'Note',
                type: 'text'
            }
        ],
        columnShow: [false, true, true, true, true, true, true, true, true, true, true],
        initDataRows: ['', '', '', '', '', '', '', '', '', '', ''],
        colWidths: [50, 150, 200, 200, 150, 150, 150, 150, 100, 150, 300],
        allrowevent: true,
        deleterowconfirm: $scope.deleteItem,
        tableOverflow: true,
        csvHeaders: true,
        changeCell: $scope.rowchange,
        tableHeight: '200px',
        onpaste: $scope.onpaste1,
        onload: $scope.onload1
    };


    if (para != "") {
        $scope.Title = "Sửa";
        $scope.model.Temps = "2";
        $scope.disColumn = true;
        $scope.initData = function () {
            dataservice.getItem({ IdS: [para] }, function (rs) {
                if (rs.Error) {
                    App.notifyDanger(rs.Title);
                } else {
                    $scope.model = JSON.parse(rs.Table1)[0];
                    $scope.model.DateSX !== null ? $scope.model.DateSX = "/Date(" + new Date($scope.model.DateSX).getTime() + ")/" : undefined;
                    $scope.model.EffectiveDate !== null ? $scope.model.EffectiveDate = "/Date(" + new Date($scope.model.EffectiveDate).getTime() + ")/" : undefined;
                    $scope.lstDetail = JSON.parse(rs.Table2);
                    var ApplyOutsideDetails = [];
                    angular.forEach($scope.lstDetail, function (value, key) {
                        ApplyOutsideDetails.push([
                            value.RowGuid,
                            value.ItemID,
                            value.ItemName,
                            value.ProductSpecifications,
                            value.Packing,
                            value.FactoryType,
                            value.UnitID,
                            value.UnitName,
                            value.Title,
                            value.Quantity,
                            value.Note
                        ]);
                    });
                    $scope.jxcelAddDetailInit.data = ApplyOutsideDetails;
                    $scope.onload1();
                    if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                        $scope.$apply();
                    }
                }
            })
        }
        $scope.initData();
    } else {
        $scope.Title = "Thêm mới";
        $scope.model.Temps = "1";
        $scope.model.IsApprove = "0";
    }

    $scope.submit = function () {

        if ($scope.addform.validate()) {
            var fd = new FormData();
            if (uploader.queue.length > 0) {
                for (i = 0; i < uploader.queue.length; i++) {
                    var files = uploader.queue[i]._file;
                    var titleName = "";
                    if (uploader.queue[i].file.FileName === "" || uploader.queue[i].file.FileName === null || uploader.queue[i].file.FileName === undefined) {
                        titleName = uploader.queue[i].file.name.split('.')[0];
                    } else {
                        titleName = uploader.queue[i].file.FileName;
                    }
                    $scope.lstTitlefile.push({ STT: i, Title: titleName });
                    fd.append("file", files);
                }
            }
            var datatab1 = $scope.jxcelAddDetail.jexcel('getData');
            var ApplyOutsideDetails = [];
            for (var i = 0; i < datatab1.length; i++) {
                var value = datatab1[i];
                if (value.RowGuid != null && value.RowGuid != undefined && value.RowGuid != "") {
                    if (value.ItemID != null && value.ItemID != undefined && value.ItemID != "") {
                        ApplyOutsideDetails.push({
                            RowGuid: value.RowGuid,
                            ItemID: value.ItemID,
                            ItemName: value.ItemName,
                            ProductSpecifications: value.ProductSpecifications,
                            Packing: value.Packing,
                            FactoryType: value.FactoryType,
                            UnitID: value.UnitID,
                            UnitName: value.UnitName,
                            Quantity: value.Quantity,
                            UnitPrice: value.UnitPrice,
                            Title: value.Title,
                            ExchangeRate: value.ExchangeRate,
                            AmountOc: value.AmountOc,
                            Note: value.Note,
                            SortOrder: i
                        });
                    }
                }
                else {
                    if (value.ItemID != null && value.ItemID != undefined && value.ItemID != "") {
                        ApplyOutsideDetails.push({
                            ItemID: value.ItemID,
                            ItemName: value.ItemName,
                            ProductSpecifications: value.ProductSpecifications,
                            Packing: value.Packing,
                            FactoryType: value.FactoryType,
                            Packing: value.Packing,
                            UnitID: value.UnitID,
                            UnitName: value.UnitName,
                            Quantity: value.Quantity,
                            UnitPrice: value.UnitPrice,
                            Title: value.Title,
                            ExchangeRate: value.ExchangeRate,
                            AmountOc: value.AmountOc,
                            Note: value.Note,
                            SortOrder: i
                        });
                    }
                }
            };
            if (ApplyOutsideDetails.length == 0) {
                App.notifyDanger("Yêu cầu nhập chi tiết");
                return;
            }
            App.blockUI({
                target: "#table_load",
                boxed: true,
                message: 'Đang tải...'
            });
            fd.append('submit', JSON.stringify($scope.model));
            fd.append('detail', JSON.stringify(ApplyOutsideDetails));
            fd.append('detailDelete', JSON.stringify($scope.ListDetailDelete));
            $.ajax({
                url: '/ProductionManager/Submit',
                data: fd,
                processData: false,
                contentType: false,
                type: 'POST',
                success: function (rs) {
                    if (rs.Error) {
                        App.unblockUI("#table_load");
                        App.notifyDanger(rs.Title);
                    } else {
                        if ($scope.model.Temps == "1") {
                            App.notifyInfo("Thêm mới thành công");
                        } else {
                            App.notifyInfo("Cập nhật thành công");
                        }
                        $rootScope.reload();
                        App.unblockUI("#table_load");
                        $uibModalInstance.close();
                    }
                },
                error: function (rs) {
                    App.notifyInfo(rs.Title);
                }
            });
        }
    }

});

app.controller('open', function ($scope, $rootScope, $compile, $uibModal, $confirm, $uibModalInstance, dataservice, para) {
    $scope.Title = "Xem ";
    $scope.model = {};
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    }
    $scope.rowchange = function (obj, row, cell, columnName) {

        $scope.onload1();
    };
    $scope.roundNumber = function (number) {
        if (number > 0) {
            return parseFloat(number);
        } else return 0;

    }
    $scope.onload1 = function () {
        if ($('#myhtindex').jexcel('getData') !== undefined) {
            $rootScope.Quantity = 0;
            $rootScope.AmountOc = 0;
            $rootScope.UnitPrice = 0;
            $rootScope.VATAmountOC = 0;
            $scope.ListData = $('#myhtindex').jexcel('getData');
            angular.forEach($scope.ListData, function (val, key) {
                if (val.Quantity !== null && val.Quantity !== "" && val.Quantity !== undefined) {
                    $rootScope.Quantity += $scope.roundNumber(parseFloat(val.Quantity));
                }
            });
            $('#myhtindex #colfoot-9').html($rootScope.addPeriod($scope.Quantity));
            if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                $scope.$apply();
            }
        }
    }

    $scope.dropdownItemId = function (d1, d2, d3) {
        var _tem1 = d1.ItemID;
        if (_tem1 === "" || _tem1 === null) {
            _tem1 = "";
        }
        var obj = {
            Keyword: _tem1
        }
        dataservice.getItemALL(obj, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {

                d3.source = rs;
                d2(rs, "");
            }
        })
    }
    $scope.dropdownUnitID = function (d1, d2, d3) {
        var _tem1 = d1.UnitID;
        if (_tem1 === "" || _tem1 === null) {
            _tem1 = "";
        }
        var obj = {
            Keyword: _tem1
        }
        dataservice.getUnitJexcel(obj, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {

                d3.source = rs;
                d2(rs, "");
            }
        })
    }

    $scope.jxcelAddDetailInit = {
        dataObject: true,
        reloadData: {},
        data: [],
        colHeaders: ['', 'Mã sản phẩm', 'Tên sản phẩm', "Quy cách sản phẩm", "Quy cách đóng gói", 'Tại Xưởng', 'Mã đơn vị tính', 'Tên đơn vị tính', 'Loại ngoại tệ', 'Số lượng', 'Ghi chú'
        ],
        colTitles: ['', 'Mã sản phẩm', 'Tên sản phẩm', "Quy cách sản phẩm", "Quy cách đóng gói", 'Tại Xưởng', 'Mã đơn vị tính', 'Tên đơn vị tính', 'Loại ngoại tệ', 'Số lượng', 'Ghi chú'
        ],
        colFooters: ['', '', '', '', '', '', '', '', '', '', ''
        ],
        columns: [
            {
                name: 'RowGuid',
                type: 'text',
                readOnly: true,
            },
            {
                name: 'ItemID',
                type: 'text',
                readOnly: true,
            },
            {
                name: 'ItemName',
                type: 'text',
                readOnly: true,
            },
            {
                name: 'ProductSpecifications',
                type: 'text', readOnly: true,
            },
            {
                name: 'Packing',
                type: 'text', readOnly: true,
            },
            {
                name: 'FactoryType',
                type: 'autocomplete',
                url: '/Sale/ProductionManager/FactoryType/',
                readOnly: false,
                updateReadOnly: true, readOnly: true,
            },
            {
                name: 'UnitID',
                type: 'autocomplete',
                source: $rootScope.litsUnitID,
                sourceReload: $scope.dropdownUnitID,
                searchReload: true,
                onlyCode: true,
                isFixColumn: true, readOnly: true,
            },
            {
                name: 'UnitName',
                type: 'text'
                , readOnly: true,
            },
            {
                name: 'Title',
                type: 'autocomplete',
                url: '/Sale/ProductDeparments/MoneyType/',
                readOnly: false,
                updateReadOnly: true, readOnly: true,
            },
            {
                name: 'Quantity',//Xếp hạng 3
                type: 'numeric', formatNumber: 'VN-NUMERIC', readOnly: true,
            },

            {
                name: 'Note',
                type: 'text', readOnly: true,
            }
        ],

        columnShow: [false, true, true, true, true, true, true, true, true, true, true],
        initDataRows: ['', '', '', '', '', '', '', '', '', '', ''],
        colWidths: [50, 150, 200, 200, 150, 150, 150, 150, 100, 150, 300],
        allrowevent: true,
        deleterowconfirm: $scope.deleteItem,
        tableOverflow: true,
        csvHeaders: true,
        changeCell: $scope.rowchange,
        tableHeight: '200px',
        onpaste: $scope.onpaste1,
        onload: $scope.onload1,
        allowInsertRow: false,
        contextMenu: function () { return ""; },
    };
    $scope.initData = function (para) {
        if (para == null) return;
        dataservice.getItem({ IdS: [para] }, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {
                $scope.model = JSON.parse(rs.Table1)[0];
                $scope.model.DateSX !== null ? $scope.model.DateSX = "/Date(" + new Date($scope.model.DateSX).getTime() + ")/" : undefined;
                $scope.model.EffectiveDate !== null ? $scope.model.EffectiveDate = "/Date(" + new Date($scope.model.EffectiveDate).getTime() + ")/" : undefined;
                $scope.lstDetail = JSON.parse(rs.Table2);
                var ApplyOutsideDetails = [];
                angular.forEach($scope.lstDetail, function (value, key) {
                    ApplyOutsideDetails.push([
                        value.RowGuid,
                        value.ItemID,
                        value.ItemName,
                        value.ProductSpecifications,
                        value.Packing,
                        value.FactoryType,
                        value.UnitID,
                        value.UnitName,
                        value.Title,
                        value.Quantity,
                        value.Note
                    ]);
                });
                $scope.jxcelAddDetailInit.data = ApplyOutsideDetails;
                $scope.onload1();
                if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                    $scope.$apply();
                }
            }
        })
    }
    $scope.initData(para);
});
// controller approve
app.controller('approve', function ($scope, $rootScope, $compile, $uibModal, $confirm, $uibModalInstance, dataservice, para) {
    $scope.model = {};
    $scope.txtTitle = "Xử lý phiếu";
    $scope.model1 = {};
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
    $scope.Init = function () {
        $.ajax({
            type: 'post',
            url: '/ProductionManager/GetItems',
            data: { Id: para },
            success: function (rs) {
                if (rs.Error) {
                    App.notifyDanger(rs.Error);
                } else {
                    $scope.model = rs.item;
                    $scope.model.Comment = "Trình duyệt";
                    var ob = {
                        WorkFlowGuid: $scope.model.WorkFlowGuid,
                        RowGuid: $scope.model.RowGuid,
                    }
                    dataservice.checkLogin(ob, function (result) {
                        if (result.Error) {
                            App.notifyDanger(result.Title);
                        } else {
                            $scope.checkLogin = result.Object;
                        }
                    });
                }
            }
        });
    };
    $scope.Init();
    $scope.submitapprove = function () {
        if ($scope.addform.validate()) {
            if ($scope.model.Comment === null || $scope.model.Comment === undefined || $scope.model.Comment === "null") {
                App.notifyDanger("Mời bạn nhập ý kiến");
                return;
            }
            App.blockUI({
                target: "#contentMain",
                boxed: true,
                message: 'Đang tải...'
            });
            $uibModalInstance.close();
            $.ajax({
                type: 'post',
                url: '/ProductionManager/Approve',
                data: $scope.model,
                success: function (rs) {
                    if (rs.Error) {
                        App.notifyDanger(rs.Title);
                        App.unblockUI("#contentMain");
                    } else {
                        $uibModalInstance.close();
                        $rootScope.reload();
                        App.notifyInfo(rs.Title);
                    }
                    App.unblockUI("#contentMain");
                }
            });
        };
    };
    $scope.submitnotapprove = function () {
        if ($scope.addform.validate()) {
            if ($scope.model.Comment === null || $scope.model.Comment === undefined || $scope.model.Comment === "null") {
                App.notifyDanger("Yêu cầu nhập ý kiến");
                return;
            }
            App.blockUI({
                target: "#contentMain",
                boxed: true,
                message: 'Đang tải...'
            });
            $uibModalInstance.close();
            $.ajax({
                type: 'post',
                url: '/ProductionManager/NotApprove',
                data: $scope.model,
                success: function (rs) {
                    if (rs.Error) {
                        App.notifyDanger(rs.Title);
                        App.unblockUI("#contentMain");
                    } else {
                        $uibModalInstance.close();
                        $rootScope.reload();
                        App.notifyInfo(rs.Title);
                    }
                    App.unblockUI("#contentMain");
                }
            });
        };
    };
});
app.controller('addattchment', function ($scope, $rootScope, $compile, $uibModal, $confirm, $uibModalInstance, dataservice, para, para1, FileUploader) {
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
    $scope.model = { RecordGuid: para, STT: para1 };
    $scope.nameController = "";

    //Cấu hình file đính kèm
    var uploader = $scope.uploader = new FileUploader({
        // url: App.baseService() + '/upload'
    });
    uploader.filters.push({
        name: "customFilter",
        fn: function (item /*{File|FileLikeObject}*/, options) {
            return this.queue.length < 10;
        }
    });
    uploader.onCompleteAll = function () {
    };

    // Bảo hiểm
    $scope.getattachment = function () {
        var obj = {
            RecordGuid: $scope.model.RecordGuid
        }
        dataservice.getattachment(JSON.stringify(obj), function (result) {
            $scope.jdataattach = result;
            angular.forEach($scope.jdataattach, function (value, key) {
                if (value.CreatedBy !== null && value.CreatedBy !== undefined) {
                    if (value.CreatedBy.includes('#')) {
                        value.CreatedBy = value.CreatedBy.split('#')[1];
                    }
                }
            })
        });
    }
    $scope.getattachment(); // danh sách attach bảo hiểm

    $scope.lstDeleteFile = [];
    $scope.remoteAttach = function (item, number) {
        $scope.lstDeleteFile.push(item.AttachmentGuid);
        $scope.jdataattach.splice(number, 1);
        App.notifyInfo("Xóa thành công");
        if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
            $scope.$apply();
        }
    }
    $scope.submit = function () {
        var fd = new FormData();
        $scope.lstTitlefile = [];
        if (uploader.queue.length > 0) {
            for (i = 0; i < uploader.queue.length; i++) {
                var files = uploader.queue[i]._file;
                var titleName = "";
                if (uploader.queue[i].file.FileName === "" || uploader.queue[i].file.FileName === null || uploader.queue[i].file.FileName === undefined) {
                    titleName = uploader.queue[i].file.name.split('.')[0];
                } else {
                    titleName = uploader.queue[i].file.FileName;
                }
                $scope.lstTitlefile.push({ STT: i, Title: titleName });
                fd.append("file", files);
            }
        }
        App.blockUI({
            target: ".modal-content",
            boxed: true,
            message: 'Đang tải...'
        });
        fd.append('model', JSON.stringify($scope.model));
        fd.append('lstTitlefile', JSON.stringify($scope.lstTitlefile));
        fd.append('DeleteAttach', JSON.stringify($scope.lstDeleteFile));
        $.ajax({
            type: "POST",
            url: "/ProductionManager/UpdateFile",
            contentType: false,
            processData: false,
            data: fd,
            success: function (rs) {
                if (rs.Error === true) {
                    App.notifyDanger(rs.Title);
                    App.unblockUI(".modal-content");
                } else {
                    $scope.lstDeleteFile = [];
                    uploader.queue = [];
                    App.notifyInfo(rs.Title);
                    $scope.getattachment();
                    App.unblockUI(".modal-content");
                }
            },
            error: function (rs) {
                App.notifyDanger(rs.Title);
            }
        })
    }
});
//Thông tin hợp đồng
app.controller('Customers_index', function ($http, $scope, $rootScope, $compile, $confirm, $uibModal, $uibModalInstance, DTOptionsBuilder, DTColumnBuilder, DTInstances, keyhandle, FileUploader, dataservice, para) {
    var vm = $scope;
    $scope.TitleIndex = "Danh sách khách hàng";
    var ctrl = $scope;
    $scope.statusDisable = false;
    $scope.changeEdit = function (data) {
        if (data === 0) {
            $scope.statusDisable = false;
            $("div.SumoSelect").removeClass("SumoSelect disabled").addClass("SumoSelect");
        } else {
            $scope.statusDisable = true;
        }
    }
    ctrl.nextRow = function (d) {
        if (ctrl.ElementRowCheck === -1) {
            if (ctrl.allRow.length > 1) {
                ctrl.ElementRowCheck = ctrl.allRow[0].row;
                $('td', ctrl.ElementRowCheck).addClass("es-iSeleted");
                ctrl.iSelected = ctrl.allRow[0].data;
                $scope.getById_Customers(ctrl.iSelected.CustomerGuid);

            }
        } else
            for (var i = 0; i < ctrl.allRow.length; i++) {
                if (ctrl.allRow[i].row === ctrl.ElementRowCheck) {
                    if (i === ctrl.allRow.length - 1) break;
                    $('td', ctrl.ElementRowCheck).removeClass('es-iSeleted');
                    ctrl.ElementRowCheck = ctrl.allRow[i + 1].row;
                    $('td', ctrl.ElementRowCheck).addClass("es-iSeleted");
                    ctrl.iSelected = ctrl.allRow[i + 1].data;
                    $scope.getById_Customers(ctrl.iSelected.CustomerGuid);
                    break;
                }
            }
    }
    ctrl.upRow = function (d) {
        if (ctrl.ElementRowCheck === -1) {
            if (ctrl.allRow.length > 1) {
                ctrl.ElementRowCheck = ctrl.allRow[ctrl.allRow.length - 1].row;
                $('td', ctrl.ElementRowCheck).addClass("es-iSeleted");
                ctrl.iSelected = ctrl.allRow[ctrl.allRow.length - 1].row;
                $scope.getById_Customers(ctrl.iSelected.CustomerGuid);
            }
        } else
            for (var i = 0; i < ctrl.allRow.length; i++) {
                if (ctrl.allRow[i].row === ctrl.ElementRowCheck) {
                    if (i === 0) break;
                    $('td', ctrl.ElementRowCheck).removeClass('es-iSeleted');
                    ctrl.ElementRowCheck = ctrl.allRow[i - 1].row;
                    $('td', ctrl.ElementRowCheck).addClass("es-iSeleted");
                    ctrl.iSelected = ctrl.allRow[i - 1].data;
                    $scope.getById_Customers(ctrl.iSelected.CustomerGuid);

                    break;
                }
            }
    }
    keyhandle.eventCtrl(40, ctrl.nextRow);
    keyhandle.eventCtrl(38, ctrl.upRow);
    ctrl.ElementRowCheck = -1;
    ctrl.allRow = [];
    ctrl.iSelected = {};
    ctrl.dataScrollPage = [];
    $scope.selected = [];
    $scope.selectAll = false;
    $scope.toggleAll = toggleAll;
    $scope.toggleOne = toggleOne;
    $scope.dataload = [];
    $scope.CheckReload = false;
    $scope.staticParam = {
        totalItems: 0,
        CurrentPage: 1,
        pageSize: 25,
        maxSize: 5,
        Keyword: "",
        EmployeeId: null,
        CustomerGuid: null
    };
    $scope.txtComment = "";

    $scope.lstInt = [];
    $rootScope.TongGT = 0;
    $scope.selected = [];
    $scope.selectAll = false;
    $scope.toggleAll = toggleAll;
    $scope.toggleOne = toggleOne;
    var titleHtml = '<label class="mt-checkbox"><input type="checkbox" ng-model="selectAll" ng-click="toggleAll(selectAll, selected)"/><span></span></label>';

    vm.dtOptions = DTOptionsBuilder.newOptions()
        .withOption('serverSide', true)
        .withOption('order', [1, 'desc'])
        .withDataProp('data')
        .withOption('pageLength', 25)
        .withOption('initComplete', function (settings, json) {
            $('.dataTables_scrollBody').css({
                'overflow-y': 'scroll',
                'overflow-x': 'hidden',
                'border': 'none'
            });
            //hiền scroll footer
            $('#tblData_wrapper .dataTables_scrollFoot').css('overflow', 'auto');
            //cuộn toàn bảng theo footer
            $('#tblData_wrapper .dataTables_scrollFoot').on('scroll', function () {
                $('#tblData_wrapper .dataTables_scrollBody').scrollLeft($(this).scrollLeft());
            });

            //thêm sự kiện scroll phân trang
            $('#tblData').parent().attr("onscroll", "angular.element(this).scope().LoadScroll(this)");
        })
        .withOption('searching', false)
        .withOption('createdRow', function (row, data, dataIndex) {
            const contextScope = $scope.$new(true);
            contextScope.data = data;
            $scope.data = data;
            var obj = {
                STT: data._STT.toString(),
                Id: data.CustomerGuid
            }
            $scope.lstInt.push(obj);
            $compile(row)($scope);
            if (data._STT.toString() === "1") {
                $('td', row).addClass("es-iSeleted");
                vm.ElementRowCheck = row;
                ctrl.ElementRowCheck = row;
                ctrl.iSelected = data;
            }
            contextScope.contextMenu = $scope.contextMenu;
            $compile(angular.element(row).find('input'))($scope);
            $compile(angular.element(row).attr('context-menu', 'contextMenu'))(contextScope);
        });
    vm.dtOptions.withOption('ajax', function (data, callback, settings) {
        $scope.staticParam.CurrentPage = $scope.dtInstance.DataTable.page() + 1;
        if ($scope.CheckReload) {
            data.Start = 0;
        }
        App.blockUI({
            target: "#table_load",
            boxed: true,
            message: 'Đang tải...'
        });
        $http.post('/ProductionManager/JTable_Customers', data)
            .success(function (res) {

                if (res.recordsTotal > $scope.dataload.length) {
                    angular.forEach(res.data, function (val, key) {
                        $scope.dataload.push(val);
                    });
                    $rootScope.TongGT = 0;
                    angular.forEach($scope.dataload, function (val, key) {
                        $rootScope.TongGT += parseFloat(val.TotalOfMoney);

                    });
                    callback({
                        data: $scope.dataload,
                        recordsTotal: res.recordsTotal,
                        recordsFiltered: res.recordsFiltered
                    });
                } else {
                    if (data.start === 0) {
                        $scope.dataload = [];
                        angular.forEach(res.data, function (val, key) {
                            $scope.dataload.push(val);
                        });
                        $rootScope.TongGT = 0;
                        angular.forEach($scope.dataload, function (val, key) {
                            $rootScope.TongGT += parseFloat(val.TotalOfMoney);

                        });
                        callback({
                            data: $scope.dataload,
                            recordsTotal: res.recordsTotal,
                            recordsFiltered: res.recordsFiltered
                        });
                    }
                }
                if ($scope.dataload.length === 0) {
                    $scope.clear_Contracts();
                }
                App.unblockUI("#table_load");

            });
    });
    //scroll theo tỷ lệ màn hình
    vm.dtOptions.withOption('scrollY', '40vh')
        .withOption('scrollX', '100%') //mặc định 100%
        .withOption('scrollCollapse', true) //xóa khoảng trắng khi dữ liệu trong bảng ít
        .withOption('rowCallback', rowCallback) //sự kiên click vào 1 dòng
        .withOption('scroller', {
            loadingIndicator: true
        }).withFixedColumns({
            leftColumns: 0 //fix bên trái 2 cột
            //rightColumns: 1 //fix bên phải 1 cột
        });

    vm.dtColumns = [];

    vm.dtColumns.push(DTColumnBuilder.newColumn('_STT').withTitle('STT').notSortable().withOption('sClass', 'tcenter').withOption('sWidth', '5px').renderWith(function (data, type, full, meta) {
        return meta.row + 1;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('CustomerId').withTitle('Mã khách hàng').withOption('sWidth', '60px').renderWith(function (data, type, full, meta) {
        return data;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('CustomerName').withTitle('Tên khách hàng').withOption('sWidth', '150px').renderWith(function (data, type, full, meta) {
        return data;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('Address').withTitle('Địa chỉ').withOption('sWidth', '150px').renderWith(function (data, type, full, meta) {
        return data;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('WorkPlace').withTitle('Liên hệ').withOption('sWidth', '120px').renderWith(function (data, type, full, meta) {
        return data;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('ActivityFieldName').withTitle('Sản phẩm').withOption('sWidth', '120px').renderWith(function (data, type, full, meta) {
        return data;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('BusinessTypeName').withTitle('Nhóm khách hàng').withOption('sWidth', '80px').renderWith(function (data, type, full, meta) {
        if (data == "KHTT") {
            return "Khách hàng trung thành";
        } else if (data == "KHTN") {
            return "Khách hàng tiềm năng";
        } else if (data == "KHM") {
            return "Khách hàng mới";
        } else if (data == "KHKTN") {
            return "Khách hàng không tiềm năng";
        } else {
            return "";
        }
       
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('LeadName').withTitle('Người thực hiện').withOption('sWidth', '100px').renderWith(function (data, type, full, meta) {
        return data;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('Method').withTitle('Hình thức TT').withOption('sWidth', '60px').renderWith(function (data, type, full, meta) {
        if (data == "C") {
            return "Chuyển khoản";
        } else if (data == "T") {
            return "Tiền mặt";
        } else {
            return "";
        }
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('Note').withTitle('Lưu ý khác').withOption('sWidth', '120px').renderWith(function (data, type, full, meta) {
        return data;
    }));

    vm.reloadData = reloadData;
    vm.dtInstance = {};
    function reloadData(resetPaging) {
        ctrl.ElementRowCheck = -1;
        $scope.staticParam.currentPage = resetPaging;
        vm.dtInstance.reloadData(callback, resetPaging);
        vm.dtInstance.DataTable.draw();
    }
    function callback(json) {

    }
    function toggleAll(selectAll, selectedItems) {
        for (var id in selectedItems) {
            if (selectedItems.hasOwnProperty(id)) {
                selectedItems[id] = selectAll;
            }
        }
    }
    function toggleOne(selectedItems) {
        for (var id in selectedItems) {
            if (selectedItems.hasOwnProperty(id)) {
                if (!selectedItems[id]) {
                    vm.selectAll = false;
                    return;
                }
            }
        }
        vm.selectAll = true;
    }
    $scope.total = 0;
    $scope.check = false;
    $scope.LoadScroll = function (obj) {
        var total = obj.offsetHeight + obj.scrollTop;
        if (obj.offsetHeight + obj.scrollTop >= obj.scrollHeight && $scope.check && $scope.total <= total) {
            $scope.check = false;
            try {
                $scope.CheckReload = false;
                vm.dtInstance.DataTable.page('next').draw('page');
            }
            catch (ex) {
            }
        }
        if (obj.offsetHeight + obj.scrollTop >= obj.scrollHeight) {
            $scope.total = obj.offsetHeight + obj.scrollTop;
        }
        else {
            $scope.check = true;
        }

    };
    function rowCallback(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        if (iDisplayIndexFull === 0) ctrl.allRow = [];
        $('td', nRow).unbind('click');
        $('td', nRow).bind('click', function () {
            if (vm.ElementRowCheck === nRow) return;
            $('td', nRow).addClass("es-iSeleted");
            $('td', vm.ElementRowCheck).removeClass('es-iSeleted');
            vm.ElementRowCheck = nRow;
            ctrl.ElementRowCheck = nRow;
            ctrl.iSelected = aData;
            vm.staticParam.CustomerGuid = aData.CustomerGuid;
            $scope.getById_Customers(vm.staticParam.CustomerGuid);
            $scope.changeEdit(1);
        });
        $('td', nRow).unbind('dblclick');
        $('td', nRow).bind('dblclick', function () {
            $scope.$apply(function () {
                $scope.getById_Customers(aData.CustomerGuid);
            });
        });
        ctrl.allRow.push({ row: nRow, data: aData });
        return nRow;
    }
    $rootScope.reload = function () {
        $scope.lstInt = [];
        $scope.dataload = [];
        reloadData(true);
    };
    $scope.addPeriod = function (nStr1) {
        if (nStr1 !== null && nStr1 !== undefined && nStr1 !== "") {
            var nStr = nStr1.split('.');
            nStr += '';
            x = nStr.split(',');
            x1 = x[0];
            x2 = x.length > 1 ? ',' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
            }
            return x1;
        } else {
            return "";
        }
    }
    $scope.contextMenu = [
        [function ($itemScope) {
            return '<i class="fa fa-remove"></i> Xóa';
        }, function ($itemScope, $event, model) {
            ESConfirm([{ 'value': 1, 'title': 'Xác nhận', 'type': 'info' }, { 'value': 2, 'title': 'Đóng', 'type': 'danger' }], { 'description': 'nhà cung cấp này không?', 'class': 'eswarning' }, function (rs) {
                if (rs === '1') {
                    App.blockUI({
                        target: "#contentMain",
                        boxed: true,
                        message: 'Đang tải...'
                    });
                    dataservice.delete_Customers($itemScope.data.CustomerGuid, function (result) {
                        if (result.Error) {
                            App.notifyDanger(result.Title);
                        } else {
                            App.notifyInfo(result.Title);
                            $scope.clear_Contracts();
                            $scope.reload();
                        }
                        App.unblockUI("#contentMain");
                    });
                }
                if (rs === '2') {
                    console.log('Không chấp nhận');
                }
            }, function () {
                //some error in this function
                console.log('Không lựa chọn');
            });

        }, function ($itemScope, $event, model) {
            return true;
        }]
    ];

    $scope.ConvertNumber = function (number) {
        if (number != null && number != "") {
            return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
        return null;
    }

    $rootScope.validationOptions = {
        rules: {
            CustomerId: {
                required: true,
                maxlength: 20
            }
        },
        messages: {
            CustomerId: {
                required: "Yêu cầu nhập mã khách hàng.",
                maxlength: "Mã khách hàng không vượt quá 20 ký tự."
            }
        }
    }
    $rootScope.StatusData = [{
        value: 1,
        text: 'Sử dụng'
    }, {
        value: 0,
        text: 'Không sử dụng'
    }];
    $rootScope.ListMethod = [{
        value: "C",
        text: 'Chuyển khoản'
    }, {
        value: "T",
        text: 'Tiền mặt'
    }

    ];
    $('#tblData').parent().removeAttr("onscroll");
    $scope.cancel = function () {
        $('#tblData').parent().removeAttr("onscroll");
        $('#tblData_Salaries').parent().removeAttr("onscroll");
        $('#tblData_Insurances').parent().removeAttr("onscroll");
        $('#tblData_Contracts').parent().removeAttr("onscroll");
        $('#tblData_Histories').parent().removeAttr("onscroll");
        $('#tblData_Certificates').parent().removeAttr("onscroll");
        $('#tblData_Reward').parent().removeAttr("onscroll");
        $('#tblData_TrainingRequirement').parent().removeAttr("onscroll");
        $uibModalInstance.dismiss('cancel');
    }

    //================Thêm hợp đồng===========================
    $scope.model = {};
    $scope.ChangeBusinessTypeName = function (data) {
        $scope.model.BusinessTypeName = data;
    }
    $scope.ChangeMethod = function (data) {
        $scope.model.Method = data;
    }
    $scope.model.FileAttachments = [];
    var uploader = $scope.uploader = new FileUploader({
        // url: App.baseService() + '/upload'
    });
    uploader.filters.push({
        name: 'customFilter',
        fn: function (item /*{File|FileLikeObject}*/, options) {
            return this.queue.length < 10;
        }
    });
    uploader.onSuccessItem = function (fileItem, response, status, headers) {
        $scope.model.FileAttachments.push(response);
    };

    uploader.onCompleteAll = function () {
        $scope.commit();
    };

    $scope.getById_Customers = function (data) {
        dataservice.getById_Customers(data, function (rs) {
            if (rs !== null) {
                $scope.model = rs;
                $scope.statusDisable = false;

            } else {
                $scope.clear_Contracts();
            }
        });
    }
    $scope.clear_Contracts = function () {
        $scope.model = {};
        $scope.model.CustomerGuid = undefined;
        $scope.model.CustomerId = "";
        $scope.model.CustomerName = "";
        $scope.model.Address = "";
        $scope.model.WorkPlace = "";
        $scope.model.ActivityFieldName = "";
        $scope.model.BusinessTypeName = "";
        $scope.model.LeadName = "";
        $scope.model.Method = "T";
        $scope.model.Note = "";
        $scope.lstDeleteFile = [];
        uploader.queue = [];
        $scope.jdataattach = [];
        $scope.statusDisable = false;
        $("div.SumoSelect").removeClass("SumoSelect disabled").addClass("SumoSelect");
    }
    $scope.lstDeleteFile = [];
    $scope.submit_Contracts = function () {
        if ($scope.addform.validate()) {
            var fd = new FormData();
            $scope.model.IsCustomer = true;
            if ($scope.model.CustomerGuid != undefined && $scope.model.CustomerGuid != null) {
                fd.append('submit', JSON.stringify($scope.model));
                fd.append('DeleteAttach', JSON.stringify($scope.lstDeleteFile));
                $.ajax({
                    type: "POST",
                    url: "/Customers/Update",
                    contentType: false,
                    processData: false,
                    data: fd,
                    success: function (rs) {
                        if (rs.Error) {
                            App.notifyDanger(rs.Title);
                        } else {
                            App.notifyInfo(rs.Title);
                            $scope.reload();
                        }
                    }
                });
            }
            else {
                fd.append('submit', JSON.stringify($scope.model));
                $.ajax({
                    type: "POST",
                    url: "/Customers/Insert",
                    contentType: false,
                    processData: false,
                    data: fd,
                    success: function (rs) {
                        if (rs.Error) {
                            App.notifyDanger(rs.Title);
                        } else {
                            App.notifyInfo(rs.Title);
                            $scope.reload();
                        }
                    }
                });
            }
        }
    };


});


