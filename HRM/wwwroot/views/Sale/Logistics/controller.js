﻿var ctxfolder = "/views/Sale/Logistics";
app.factory('dataservice', function ($http) {
    var headers = {
        "Content-Type": "application/json;odata=verbose",
        "Accept": "application/json;odata=verbose"
    };
    return {
        //------------------------*Store*-------------------------
        getById: function (data, callback) {
            $http.post('/Logistics/GetItem?Id=' + data).success(callback);
        },
        delete: function (data, callback) {
            $http.post('/Logistics/Delete?Id=' + data).success(callback);
        },
        getItemALL: function (data, callback) {
            $http.post('/Logistics/getItemjexcel/', data).success(callback);
        },
        getItem_by_ItemId: function (data, callback) {
            $http.post('/Logistics/getItem_by_ItemId/', data).success(callback);
        },
        getUnitJexcel: function (data, callback) {
            $http.post('/Logistics/getUnitJexcel/', data).success(callback);
        },
        getUnit_by_ItemId: function (data, callback) {
            $http.post('/Logistics/getUnit_by_ItemId/', data).success(callback);
        },
        GetProductionManager: function (callback) {
            $http.post('/Logistics/GetProductionManager/').success(callback);
        },
        GetItem_ExportDocuments: function (data, callback) {
            $http.post('/Logistics/GetItem_ExportDocuments?Id=' + data).success(callback);
        },
        Delete_ExportDocuments: function (data, callback) {
            $http.post('/Logistics/Delete_ExportDocuments?Id=' + data).success(callback);
        },
        getattachment: function (data, callback) {
            $http.post('/ProductionManager/GetItemAttachment/', data).success(callback);
        },
        delete_Customers: function (data, callback) {
            $http.post('/Customers/Delete?Id=' + data).success(callback);
        },
        getById_Customers: function (data, callback) {
            $http.post('/Customers/GetItem/' + data).success(callback);
        },
        delete_Customers: function (data, callback) {
            $http.post('/Customers/Delete?Id=' + data).success(callback);
        },
    };

});
app.filter('html', ['$sce', function ($sce) {
    return function (text) {
        return $sce.trustAsHtml(text);
    };
}]);
app.filter('fdate', [
    '$filter', function ($filter) {
        return function (input, f) {
            if (input && input.toString().indexOf('Date') > -1) {
                return moment(input).format(f);
            } if (input && input.toString().indexOf('T') > -1) {
                return moment(input).format(f);
            } else return input;
        };
    }
]);
var spassAresSlug = function (number, isview) {
    try {
        // khai bao bien su dung       
        var numbers = number; var length = numbers.length; if (isview) {
            // khi dang la hien thi        
            if (numbers.indexOf(".") != -1) {
                length = numbers.indexOf(".");
                numbers = number.replace(".", ",");
            }
        } else {
            // khi dang la input textbox           
            if (numbers.indexOf(",") != -1) length = numbers.indexOf(",");
        }
        // loai bo nhung ky tu khong can thiet       
        // check so 0         
        var isTrue = true; while (isTrue) {
            if (numbers[0] == "0") numbers = numbers.substr(1, numbers.length);
            else isTrue = false;
        };
        // check dau phay      
        numbers = numbers.split(",");
        var string = "";
        for (var i = 0; i < numbers.length; i++) {
            if (i == 0) string += numbers[i];
            else if (i == 1) string += "," + numbers[i];
            else string += numbers[i];
        } numbers = string;
        // dung chung cho 2 truong hop       
        var no = 3, index = length - no;
        while (index > 0) { numbers = numbers.substr(0, index) + '.' + numbers.substr(index); index -= no; };
        return numbers == "" ? 0 : numbers;
    } catch (ex) { }
    return numbers == "" ? 0 : numbers;
}

app.directive('formatsub', ['$filter', function ($filter) {
    return {
        require: '?ngModel', link: function (scope, elem, attrs, ctrl) {
            if (!ctrl) return; ctrl.$formatters.unshift(function (viewValue) {
                try {
                    plainNumber = viewValue.replace('.', ','); elem.val(spassAresSlug(plainNumber));
                    return spassAresSlug(plainNumber);
                } catch (ex) { return undefined; }
            }); ctrl.$parsers.unshift(function (viewValue) {
                plainNumber = viewValue.replace(/[^-0-9,]/g, '');
                elem.val(spassAresSlug(plainNumber)); return plainNumber.replace(/[,]/g, '.');
            });
        }
    };
}]);

app.controller('Ctrl_ES_VCM_Logistics', function ($scope, $rootScope, $sce, $compile, $uibModal, dataservice) {
    $rootScope.Title = "";
    $rootScope.go = function (path) {
        $location.path(path); return false;
    };
    $rootScope.ListProductionManager = [];
    $rootScope.GetProductionManager = function () {
        dataservice.GetProductionManager(function (rs) {
            $rootScope.ListProductionManager = rs;
        });
    };
    $rootScope.GetProductionManager();
    var obj = {
        Keyword: ""
    }
    dataservice.getItemALL(obj, function (rs) {
        if (rs.Error) {
            App.notifyDanger(rs.Title);
        } else {
            $rootScope.listItemId = rs;
        }
    })

    dataservice.getUnitJexcel(obj, function (rs) {
        if (rs.Error) {
            App.notifyDanger(rs.Title);
        } else {
            $rootScope.litsUnitID = rs;
        }
    })

    $rootScope.validationOptions = {
        rules: {
            CodeID: {
                required: true,
                maxlength: 255
            }
        },
        messages: {
            CodeID: {
                required: "Yêu cầu nhập mã phiếu.",
                maxlength: "Mã phiếu không vượt quá 255 ký tự"
            }


        }
    };


    $rootScope.IsActive = [{
        value: "N",
        text: 'Bản nháp'
    }, {
        value: "Y",
        text: 'Phát hành'
    }];
    $rootScope.ColumnMultiConfig = {
        selectAll: true,
        search: true,
        searchText: 'Tìm kiếm...',
        showTitle: 'true',
        csvSepChar: ',',
        captionFormat: '{0} lựa chọn',
        captionFormatAllSelected: '{0} lựa chọn',
        noMatch: 'Không có dữ liệu "{0}"',
        okCancelInMulti: true,
        locale: ['Đồng ý', 'Hủy', 'Chọn tất cả']
    };
    $rootScope.ColumnConfig = {
        search: true
    };

    //định dạng số xóa dấu chấm phẩy
    $rootScope.addPeriod = function (nStr) {
        if (nStr !== null && nStr !== "" && nStr !== undefined) {
            nStr = Math.round(parseFloat(nStr) * 100) / 100;

            nStr += "";
            if (nStr.indexOf(",") >= 0) {
                var x = nStr.split(",");
            } else {
                var x = nStr.split(".");
            }
            var x1 = x[0];
            var x2 = x.length > 1 && parseInt(x[1]) > 0 ? "," + x[1] : "";
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, "$1" + "." + "$2");
            }
            var Total = x1 + x2;
            return Total;
        }
        else {
            return 0;
        }
    };


    //Thêm phần tinymceOptions
    $rootScope.tinymceOptions = {
        selector: 'textarea',
        plugins: [
            'advlist autolink lists link charmap print preview hr anchor pagebreak image',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
        ],
        skin: 'lightgray', language: 'vi_VN',
        theme: 'modern', height: 70,
        toolbar1: 'codesample | bold italic sizeselect fontselect fontsizeselect | hr alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | insertfile undo redo | emoticons | code',
        fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
        toolbar2: 'print preview insert link image media | forecolor backcolor emoticons | codesample',
        image_advtab: true,

        file_browser_callback: function (field_name, url, type, win) {
            var connector = "/FileManager/Folder/File";
            var enableAutoTypeSelection = true;
            if (enableAutoTypeSelection && type) {
                connector += "&Type=" + type;
            }
            connector += "&Field=" + field_name;
            App.openPage(connector, "File Manager", 1150, 600);
        },
        relative_urls: false,
        remove_script_host: true,
        convert_urls: true
    };

    $rootScope.ConDate = function (data, number) {
        try {
            if (data == null || data == "") {
                return '';
            }
            if (data !== null && data != "" && data != undefined) {
                try {
                    if (data.indexOf("SA") != -1 || data.indexOf("CH") != -1) {
                        if (data.indexOf("SA") != -1) { }
                        if (data.indexOf("CH") != -1) { }
                    }

                    if (data.indexOf('Date') != -1) {
                        data = data.substring(data.indexOf("Date") + 5);
                        data = parseInt(data);
                    }
                }
                catch (ex) { }
                var newdate = new Date(data);
                if (number == 3) {
                    return newdate;
                }
                if (number == 2) {
                    return "/Date(" + newdate.getTime() + ")/";
                }
                var month = newdate.getMonth() + 1;
                var day = newdate.getDate();
                var year = newdate.getFullYear();
                var hh = newdate.getHours();
                var mm = newdate.getMinutes();
                if (month < 10)
                    month = "0" + month;
                if (day < 10)
                    day = "0" + day;
                if (mm < 10)
                    mm = "0" + mm;
                if (number == 0) {
                    return todayDate = day + "/" + month + "/" + year;
                }
                if (number == 1) {
                    return todayDate = day + "/" + month + "/" + year + " " + hh + ":" + mm;
                }
                if (number == 4) {
                    return new Date(year, month - 1, day);
                }
            } else {
                return '';
            }
        } catch (ex) {
            return "";
        }
    }
});
app.config(function ($routeProvider, $validatorProvider) {
    $routeProvider
        .when('/', {
            templateUrl: ctxfolder + '/index.html',
            controller: 'index'
        })
        .when('/edit/:id', {
            templateUrl: ctxfolder + '/edit.html',
            controller: 'edit'
        })
        .when('/add/', {
            templateUrl: ctxfolder + '/add.html',
            controller: 'add'
        });
    $validatorProvider.setDefaults({
        errorElement: 'span',
        errorClass: 'help-block',
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        success: function (label) {
            label.closest('.form-group').removeClass('has-error');
        }
    });
});
//controller hiển thị, tìm kiếm
app.controller('index', function ($http, $scope, $timeout, $uibModal, $rootScope, $compile, $confirm, $ngConfirm, $uibModal, DTOptionsBuilder, DTColumnBuilder, DTInstances, dataservice, keyhandle, utility) {
    $scope.utility = utility;
    $rootScope.TableName = "/ProductionManager";

    $scope.model = {};
    var vm = $scope;
    var ctrl = $scope;
    ctrl.nextRow = function (d) {
        if (ctrl.ElementRowCheck === -1) {
            if (ctrl.allRow.length > 1) {
                ctrl.ElementRowCheck = ctrl.allRow[0].row;
                $('td', ctrl.ElementRowCheck).addClass("es-iSeleted");
                ctrl.iSelected = ctrl.allRow[0].data;
                ctrl.initData(ctrl.iSelected.RowGuid);

            }
        } else
            for (var i = 0; i < ctrl.allRow.length; i++) {
                if (ctrl.allRow[i].row === ctrl.ElementRowCheck) {
                    if (i === ctrl.allRow.length - 1) break;
                    $('td', ctrl.ElementRowCheck).removeClass('es-iSeleted');
                    ctrl.ElementRowCheck = ctrl.allRow[i + 1].row;
                    $('td', ctrl.ElementRowCheck).addClass("es-iSeleted");
                    ctrl.iSelected = ctrl.allRow[i + 1].data;
                    ctrl.initData(ctrl.iSelected.RowGuid);
                    break;
                }
            }
    }
    ctrl.upRow = function (d) {
        if (ctrl.ElementRowCheck === -1) {
            if (ctrl.allRow.length > 1) {
                ctrl.ElementRowCheck = ctrl.allRow[ctrl.allRow.length - 1].row;
                $('td', ctrl.ElementRowCheck).addClass("es-iSeleted");
                ctrl.iSelected = ctrl.allRow[ctrl.allRow.length - 1].row;
                ctrl.initData(ctrl.iSelected.RowGuid);
            }
        } else
            for (var i = 0; i < ctrl.allRow.length; i++) {
                if (ctrl.allRow[i].row === ctrl.ElementRowCheck) {
                    if (i === 0) break;
                    $('td', ctrl.ElementRowCheck).removeClass('es-iSeleted');
                    ctrl.ElementRowCheck = ctrl.allRow[i - 1].row;
                    $('td', ctrl.ElementRowCheck).addClass("es-iSeleted");
                    ctrl.iSelected = ctrl.allRow[i - 1].data;
                    ctrl.initData(ctrl.iSelected.RowGuid);
                    break;
                }
            }
    }
    keyhandle.eventCtrl(40, ctrl.nextRow);
    keyhandle.eventCtrl(38, ctrl.upRow);
    ctrl.ElementRowCheck = -1;
    ctrl.allRow = [];
    ctrl.iSelected = {};
    ctrl.dataScrollPage = [];
    $scope.staticParam = {
        totalItems: 0,
        currentPage: 1,
        pageSize: 25,
        maxSize: 5,
        Search: ""
    };
    ctrl.changeOptionDate = function (d) {
        if (d.length === 0) {
            ctrl.staticParam.StartDate = null;
            ctrl.staticParam.EndDate = null;
        } else {
            ctrl.staticParam.StartDate = $rootScope.ConDate(d[0], 2);
            ctrl.staticParam.EndDate = $rootScope.ConDate(d[1], 2);
        }
    }


    $scope.selected = [];
    $scope.selectAll = false;
    $scope.toggleOne = toggleOne;
    $scope.toggleAll = toggleAll;

    var titleHtml = '<label style="text-align: center;" class="mt-checkbox" ><input type="checkbox" ng-model="selectAll" ng-click="toggleAll(selectAll, selected)"/><span></span></label>';
    //if (App.Permissions.LISTVIEW)
    vm.dtOptions = DTOptionsBuilder.newOptions()
        .withPaginationType('full_numbers')
        .withDataProp('data').withDisplayLength(15)
        .withOption('order', [4, 'desc'])
        .withOption('serverSide', true)
        .withOption('pageLength', 25)
        .withOption('info', false)
        .withOption('stateLoadParams', function (settings, data) {
            data.search.search = '';
        })
        .withOption('headerCallback', function (header) {
            if (!$scope.headerCompiled) {
                $scope.headerCompiled = true;
                $compile(angular.element(header).contents())($scope);
            }
        })
        .withOption('initComplete', function (settings, json) {
            //thêm sự kiện scroll phân trang
            //  $('#tblData').parent().attr("onscroll", "angular.element(this).scope().LoadScroll(this)");
            PLUGIN_DATATABLE_TABLE_SCROLL.INIT_EVENT('tblData', $scope.LoadScroll);

        })
        .withOption('createdRow', function (row, data, dataIndex) {
            const contextScope = $scope.$new(true);
            contextScope.data = data;
            $scope.data = data;
            if (data._STT.toString() === "1") {
                if (ctrl.allRow.length === 0) {
                    $('td', row).addClass("es-iSeleted");
                    vm.ElementRowCheck = row;
                    ctrl.ElementRowCheck = row;
                    ctrl.iSelected = data;
                } else {
                    if (ctrl.allRow[0] !== null) {
                        $('td', ctrl.allRow[0].row).addClass("es-iSeleted");
                        ctrl.ElementRowCheck = ctrl.allRow[0].row;
                        ctrl.iSelected = ctrl.allRow[0].data;
                        vm.staticParam.RowGuid = ctrl.allRow[0].data.RowGuid;
                    }
                }

            }
            $compile(row)($scope);
            contextScope.contextMenu = $scope.contextMenu;
            $compile(angular.element(row).find('input'))($scope);
            $compile(angular.element(row).attr('context-menu', 'contextMenu'))(contextScope);
        });
    // if (App.Permissions.LISTVIEW)
    vm.dtOptions.withOption('ajax', function (data, callback, settings) {
        data.Keyword = $scope.staticParam.Search;

        App.blockUI({
            target: "#tblData",
            boxed: true,
            message: 'Đang tải...'
        });
        $http.post('/Logistics/JTable', data)
            .success(function (res) {
                $scope.selected = [];
                if (data.start === 0) {
                    $scope.dataload = [];
                    angular.forEach(res.data, function (val, key) {
                        $scope.dataload.push(val);
                    });
                    callback({
                        data: $scope.dataload,
                        recordsTotal: res.recordsTotal,
                        recordsFiltered: res.recordsFiltered
                    });
                } else {
                    if (res.recordsTotal > $scope.dataload.length) {
                        angular.forEach(res.data, function (val, key) {
                            $scope.dataload.push(val);
                        });
                        callback({
                            data: $scope.dataload,
                            recordsTotal: res.recordsTotal,
                            recordsFiltered: res.recordsFiltered
                        });
                    }
                }
                for (var i = 0; i < ctrl.allRow.length; i++) {
                    if (ctrl.allRow[i].data.RowGuid === ctrl.iSelected.RowGuid) {
                        $('td', ctrl.allRow[i].row).addClass("es-iSeleted");
                        ctrl.ElementRowCheck = ctrl.allRow[i].row;
                        ctrl.initData(ctrl.iSelected.RowGuid);
                        break;
                    }
                }
                if ($scope.dataload.length > 0) {
                    ctrl.initData($scope.dataload[0].RowGuid);
                } else {
                    ctrl.initData(null);
                }
                PLUGIN_DATATABLE_TABLE_SCROLL.UNLOCK_EVENT('tblData');
                App.unblockUI("#tblData");
            });
    });
    //scroll theo tỷ lệ màn hình
    vm.dtOptions.withOption('scrollY', '70vh')
        .withOption('scrollX', '100%') //mặc định 100%
        .withOption('scrollCollapse', true) //xóa khoảng trắng khi dữ liệu trong bảng ít
        .withOption('rowCallback', rowCallback) //sự kiên click vào 1 dòng
        .withOption('scroller', {
            loadingIndicator: true
        });
    //.withFixedColumns({
    //    leftColumns: 0 //fix bên trái 2 cột
    //    //rightColumns: 1 //fix bên phải 1 cột
    //}
    //);
    //table
    vm.dtColumns = [];

    vm.dtColumns.push(DTColumnBuilder.newColumn('RowGuid').withTitle('STT').notSortable().withOption('sWidth', '5px').renderWith(function (data, type, full, meta) {
        return meta.row + 1;
    }).withOption('sClass', 'tcenter tcenter-header'));
    vm.dtColumns.push(DTColumnBuilder.newColumn('CodeID').withTitle('Mã Y/c logistic').withOption('sWidth', '60px').withOption('sClass', 'tcenter-header').renderWith(function (data, type, full, meta) {
        return '<a  style="color: #4285f4;" href="" ng-click="open(\'' + full.RowGuid + '\')" >' + data + '</a>';
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('EmployeeID').withTitle('Người thực hiện').withOption('sWidth', '120px').withOption('sClass', 'tcenter-header').renderWith(function (data, type, full, meta) {
        return data;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('EmployeeName').withTitle('Người theo dõi').withOption('sWidth', '120px').withOption('sClass', 'tcenter-header').renderWith(function (data, type, full, meta) {
        return data;
    }));

    vm.dtColumns.push(DTColumnBuilder.newColumn('Title').withTitle('Lưu ý khác').withOption('sWidth', '150px').withOption('sClass', 'tleft').renderWith(function (data, type, full, meta) {
        return data;
    }));

    vm.dtColumns.push(DTColumnBuilder.newColumn('RowGuid').notSortable().withTitle('Điều khiển').notSortable().withOption('sWidth', '40px').withOption('sClass', 'tcenter').renderWith(function (data, type, full, meta) {
        return '<div class="table__cell-actions-wrap">' +
            '<a ng-click="open(\'' + full.RowGuid + '\')" class="table__cell-actions-item table__cell-actions-icon"><span class="mdi mdi-eye" ></span></a > ' +
            '<a href="javascript:;" ng-click="edit(\'' + full.RowGuid + '\')" class="table__cell-actions-item table__cell-actions-icon es-cursor"> <span class="mdi mdi-table-edit" title="Sửa"></span> </a>' +
            '<a ng-click="delete (\'' + full.RowGuid + '\')" class="table__cell-actions-item table__cell-actions-icon"><span class="mdi mdi-delete" title="Xóa"></span> </a>';

    }));
    vm.reloadData = reloadData;
    vm.dtInstance = {};
    function reloadData(resetPaging) {
        $scope.staticParam.currentPage = resetPaging;
        vm.dtInstance.reloadData(callback, resetPaging);
    }
    function callback(json) {

    }
    function toggleOne(selectedItems) {
        for (var id in selectedItems) {
            if (selectedItems.hasOwnProperty(id)) {
                if (!selectedItems[id]) {
                    vm.selectAll = false;
                    return;
                }
            }
        }
        vm.selectAll = true;
    }
    function toggleAll(selectAll, selectedItems) {
        for (var id in selectedItems) {
            if (selectedItems.hasOwnProperty(id)) {
                selectedItems[id] = selectAll;
            }
        }
    }
    function rowCallback(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        if (iDisplayIndexFull === 0) ctrl.allRow = [];
        $('td', nRow).unbind('click');
        $('td', nRow).bind('click', function () {
            if (vm.ElementRowCheck === nRow) return;
            $('td', nRow).addClass("es-iSeleted");
            $('td', vm.ElementRowCheck).removeClass('es-iSeleted');
            vm.ElementRowCheck = nRow;
            ctrl.ElementRowCheck = nRow;
            ctrl.iSelected = aData;
            vm.staticParam.RowGuid = aData.RowGuid;
            vm.staticParam.CodeID = aData.CodeID;
            $scope.initData(vm.staticParam.RowGuid);
        });
        $('td', nRow).unbind('dblclick');
        $('td', nRow).bind('dblclick', function () {
            $scope.$apply(function () {
                $scope.open(aData.RowGuid);
            });
        });
        ctrl.allRow.push({ row: nRow, data: aData });
        return nRow;
    }
    //Load dữ liệu cuộn
    $scope.total = 0;
    $scope.check = false;
    $scope.LoadScroll = function (obj) {
        vm.dtInstance.DataTable.page('next').draw('page');
    };

    $scope.reloadAll = function () {
        $scope.staticParam.Search = "";
        $scope.selectAll = false;
        reloadData(true);
    };
    $rootScope.reload = function () {
        $scope.dataload = [];
        reloadData(true);
    };
    // thêm mới
    $scope.add = function () {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/add.html',
            controller: 'add',
            backdrop: 'static',
            keyboard: false,
            size: '90',
            resolve: {
                para: function () {
                    return "";
                }
            }
        });
        modalInstance.result.then(function (d) {
        }, function () {
        });
    };
    $scope.open = function (temp) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/open.html',
            controller: 'open',
            backdrop: 'static',
            size: '90',
            resolve: {
                para: function () {
                    return temp;
                }
            }
        });
        modalInstance.result.then(function (d) {
        }, function () {
        });

    };
    // sửa 
    $scope.edit = function (temp) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/add.html',
            controller: 'add',
            backdrop: 'static',
            size: '90',
            resolve: {
                para: function () {
                    return temp;
                }
            }
        });
    };
    //Xóa 1 bản ghi
    $scope.delete = function (temp) {
        dataservice.getById(temp, function (rs) {
            $scope.model = JSON.parse(rs.Table1)[0];
            ESConfirm([{ 'value': 1, 'title': 'Xác nhận', 'type': 'info' }, { 'value': 2, 'title': 'Đóng', 'type': 'danger' }], { 'description': 'Bạn có muốn xóa ' + $scope.model.CodeID, 'class': 'eswarning_v2' }, function (rs) {
                if (rs === '1') {
                    App.blockUI({
                        target: "#contentMain",
                        boxed: true,
                        message: 'Đang tải...'
                    });
                    dataservice.delete(temp, function (result) {
                        if (result.Error) {
                            App.notifyDanger(result.Title);
                        } else {
                            App.notifyInfo(result.Title);
                            $rootScope.reload();
                        }
                        App.unblockUI("#contentMain");
                    });
                }
            }, function () {
            });
        });
    };
    $scope.AttchmentOrder = function (data) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/addAttchment.html',
            controller: 'addattchment',
            backdrop: 'static',
            size: '80',
            resolve: {
                para: function () {
                    return data;
                },
                data: function () {
                    return "";
                },
                para1: function () {
                    return $scope.openAttach;
                }
            }
        });
    }
    $scope.addCustomers = function () {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/Customers_index.html',
            controller: 'Customers_index',
            backdrop: 'static',
            keyboard: false,
            size: '100',
            resolve: {
                para: function () {
                    return "";
                }
            }
        });
        modalInstance.result.then(function (d) {
        }, function () {
        });
    };
    $scope.addExportDocuments = function () {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/ExportDocuments_index.html',
            controller: 'ExportDocuments_index',
            backdrop: 'static',
            keyboard: false,
            size: '100',
            resolve: {
                para: function () {
                    return "";
                }
            }
        });
        modalInstance.result.then(function (d) {
        }, function () {
        });
    };
    $scope.contextMenu = [
        [function ($itemScope) {
            return '<i class="mdi mdi-eye"></i> Xem';
        }, function ($itemScope, $event, model) {
            $scope.open($itemScope.data.RowGuid);
        }],
        [function ($itemScope) {
            return '<i class="mdi mdi-table-edit"></i> Sửa';
        }, function ($itemScope, $event, model) {
            $scope.edit($itemScope.data.RowGuid);
        }, function ($itemScope, $event, model) {
            return true;
        }],
        [function ($itemScope) {
            return '<i class="mdi mdi-delete"></i> Xóa';
        }, function ($itemScope, $event, model) {
            $scope.delete($itemScope.data.RowGuid);
        }, function ($itemScope, $event, model) {
            return true;
        }],
        [function ($itemScope) {
            return '<i class="mdi mdi-attachment"></i> Đính kèm';
        }, function ($itemScope, $event, model) {
            $scope.AttchmentOrder($itemScope.data.RowGuid);
        }, function ($itemScope, $event, model) {
            return true;
        }],
    ];

    $scope.roundNumber = function (number) {
        if (number > 0) {
            return parseFloat(number);
        } else return 0;

    }
    $scope.onload1 = function () {
        if ($('#myhtindex').jexcel('getData') !== undefined) {
            $rootScope.Quantity = 0;
            $rootScope.AmountOc = 0;
            $rootScope.UnitPrice = 0;
            $rootScope.VATAmountOC = 0;
            $scope.ListData = $('#myhtindex').jexcel('getData');
            angular.forEach($scope.ListData, function (val, key) {
                if (val.Quantity !== null && val.Quantity !== "" && val.Quantity !== undefined) {
                    $rootScope.Quantity += $scope.roundNumber(parseFloat(val.Quantity));
                }
                if (val.UnitPrice !== null && val.UnitPrice !== "" && val.UnitPrice !== undefined) {
                    $rootScope.UnitPrice += $scope.roundNumber(parseFloat(val.UnitPrice));
                }
                if (val.AmountOc !== null && val.AmountOc !== "" && val.AmountOc !== undefined) {
                    $rootScope.AmountOc += $scope.roundNumber(parseFloat(val.AmountOc));
                }

            });
            $('#myhtindex #colfoot-6').html($rootScope.addPeriod($scope.Quantity));
            $('#myhtindex #colfoot-7').html($rootScope.addPeriod($scope.UnitPrice));
            $('#myhtindex #colfoot-8').html($rootScope.addPeriod($scope.AmountOc));
            if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                $scope.$apply();
            }
        }
    }

    $scope.dropdownUnitID = function (d1, d2, d3) {
        var _tem1 = d1.UnitID;
        if (_tem1 === "" || _tem1 === null) {
            _tem1 = "";
        }
        var obj = {
            Keyword: _tem1
        }
        dataservice.getUnitJexcel(obj, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {

                d3.source = rs;
                d2(rs, "");
            }
        })
    }
    $scope.jxcelAddDetailInit = {
        dataObject: true,
        reloadData: {},
        data: [],
        colHeaders: ['', 'Mã dịch vụ', 'Tên dịch vụ', 'Mô tả chi tiết', 'Mã đơn vị tính', 'Tên đơn vị tính', 'Số lượng', 'Đơn giá', 'Thành tiền', 'ETD/ETA', 'Hình thức thanh toán', 'Ghi chú'
        ],
        colTitles: ['', 'Mã dịch vụ', 'Tên dịch vụ', 'Mô tả chi tiết', 'Mã đơn vị tính', 'Tên đơn vị tính', 'Số lượng', 'Đơn giá', 'Thành tiền', 'ETD/ETA', 'Hình thức thanh toán', 'Ghi chú'
        ],
        colFooters: ['', '', '', '', '', '', '', '', '', '', '', ''
        ],
        columns: [
            {
                name: 'RowGuid',
                type: 'text',
                isFixColumn: true, readOnly: true
            },
            {
                name: 'ItemID',
                type: 'text', readOnly: true
            },
            {
                name: 'ItemName',
                type: 'text', readOnly: true
            },
            {
                name: 'Title',
                type: 'text', readOnly: true
            },
            {
                name: 'UnitID',
                type: 'autocomplete',
                source: $rootScope.litsUnitID,
                sourceReload: $scope.dropdownUnitID,
                searchReload: true,
                onlyCode: true, readOnly: true
            },
            {
                name: 'UnitName',
                type: 'text', readOnly: true
            },
            {
                name: 'Quantity',//Xếp hạng 3
                type: 'numeric', formatNumber: 'VN-NUMERIC', readOnly: true
            },
            {
                name: 'UnitPrice',//Xếp hạng 15
                type: 'numeric', formatNumber: 'VN-NUMERIC', readOnly: true
            },
            {
                name: 'AmountOc',//Xếp hạng 15
                type: 'numeric', formatNumber: 'VN-NUMERIC', readOnly: true
            },
            {
                name: 'ETDETA',//Xếp hạng 15
                type: 'text', readOnly: true
            },
            {
                name: 'BookingNumber',
                type: 'autocomplete',
                url: '/Sale/Logistics/BookingNumber/',
                readOnly: true,
                updateReadOnly: true
            },
            {
                name: 'Note',
                type: 'text', readOnly: true,
            }
        ],
        columnShow: [false, true, true, true, true, true, true, true, true, true, true, true],
        initDataRows: ['', '', '', '', '', '', '', '', '', '', '', ''],
        colWidths: [50, 150, 200, 200, 120, 120, 150, 150, 150, 200, 150, 250],
        allrowevent: true,
        deleterowconfirm: $scope.deleteItem,
        tableOverflow: true,
        csvHeaders: true,
        changeCell: $scope.rowchange,
        tableHeight: '200px',
        onpaste: $scope.onpaste1,
        onload: $scope.onload1,
        allowInsertRow: false,
        contextMenu: function () { return ""; },
    };
    $scope.initData = function (para) {
        if (para == null) return;
        dataservice.getById(para, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {
                $scope.model = JSON.parse(rs.Table1)[0];
                $scope.lstDetail = JSON.parse(rs.Table2);
                var ApplyOutsideDetails = [];
                angular.forEach($scope.lstDetail, function (value, key) {
                    ApplyOutsideDetails.push([
                        value.RowGuid,
                        value.ItemID,
                        value.ItemName,
                        value.Title,
                        value.UnitID,
                        value.UnitName,
                        value.Quantity,
                        value.UnitPrice,
                        value.AmountOc,
                        value.ETDETA,
                        value.BookingNumber,
                        value.Note
                    ]);
                });
                $scope.jxcelAddDetailInit.data = ApplyOutsideDetails;
                $scope.onload1();
                if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                    $scope.$apply();
                }
            }
        })
    }
});
app.controller('add', function ($scope, $rootScope, $uibModalInstance, FileUploader, dataservice, para) {
    $scope.model = { Temps: "" };
    $scope.disColumn = false;
    $scope.check = false;
    $scope.fullscreen = function (Check) {
        if (Check == false) {
            $scope.check = true;
        } else {
            $scope.check = false;
        }
    }

    $scope.deleteItem = function (element, row) {
        if (row === 0) {
            $scope.jxcelAddDetail.jexcel("setValueByName", "Id", row, "");
            $scope.jxcelAddDetail.jexcel("setValueByName", "ItemID", row, "");
            $scope.jxcelAddDetail.jexcel("setValueByName", "ItemName", row, "");
            $scope.jxcelAddDetail.jexcel("setValueByName", "UnitID", row, "");
            $scope.jxcelAddDetail.jexcel("setValueByName", "UnitName", row, "");
            $scope.jxcelAddDetail.jexcel("setValueByName", "Quantity", row, "");
            $scope.jxcelAddDetail.jexcel("setValueByName", "UnitPrice", row, "");
            $scope.jxcelAddDetail.jexcel("setValueByName", "AmountOc", row, "");
            $scope.jxcelAddDetail.jexcel("setValueByName", "VATPercent", row, "");
            $scope.jxcelAddDetail.jexcel("setValueByName", "VATAmountOC", row, "");
            $scope.jxcelAddDetail.jexcel("setValueByName", "Note", row, "");
        }
        $scope.jxcelAddDetail.jexcel('deleteRow', row);
    };
    $scope.rowchange = function (obj, row, cell, columnName) {
        var rowindex = parseInt(row + 1); // dòng muốn thay đổi      
        $scope.ListData = $scope.jxcelAddDetail.jexcel('getData');
        //if (columnName === "ItemID") {
        //    dataservice.getItem_by_ItemId({ IdS: [$scope.ListData[row].ItemID] }, function (rs) {
        //        if (rs.Error) {
        //            App.notifyDanger(rs.Title);
        //        } else {
        //            $scope.jxcelAddDetail.jexcel("setValueByName", "ItemName", row, rs.ItemName);
        //            $scope.jxcelAddDetail.jexcel("setValueByName", "UnitID", row, rs.UnitID);
        //            dataservice.getUnit_by_ItemId({ IdS: [rs.UnitID] }, function (rs) {
        //                if (rs.Error) {
        //                    App.notifyDanger(rs.Title);
        //                } else {

        //                    $scope.jxcelAddDetail.jexcel("setValueByName", "UnitName", row, rs.UnitName);
        //                    if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
        //                        $scope.$apply();
        //                    }
        //                }
        //            });
        //            if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
        //                $scope.$apply();
        //            }
        //        }
        //    })
        //}
        if (columnName === "UnitID") {
            if ($scope.ListData[row].UnitID != "") {
                dataservice.getUnit_by_ItemId({ IdS: [$scope.ListData[row].UnitID] }, function (rs) {
                    if (rs.Error) {
                        App.notifyDanger(rs.Title);
                    } else {
                        $scope.jxcelAddDetail.jexcel("setValueByName", "UnitName", row, rs.UnitName);
                        if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                            $scope.$apply();
                        }
                    }
                });
            } else {
                $scope.jxcelAddDetail.jexcel("setValueByName", "UnitName", row, "");
                if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                    $scope.$apply();
                }
            }
        }
        if (columnName === "Quantity" || columnName === "UnitPrice") {
            var Quantity = parseFloat($scope.ListData[row].Quantity);
            var UnitPrice = parseFloat($scope.ListData[row].UnitPrice);
            var AmountOc = Quantity * UnitPrice;
            $scope.jxcelAddDetail.jexcel("setValueByName", "AmountOc", row, AmountOc);
        }

        $scope.onload1();
    };
    $scope.onpaste1 = function (obj, row) {

        $scope.ListData = $('#myht').jexcel('getData');
        $scope.ListData = $scope.ListData.filter(a => a.ItemID !== '');
        var list = $scope.ListData.map(a => a.ItemID);

        var fd = new FormData();
        fd.append('Insert', JSON.stringify($scope.model));
        fd.append('Detail', JSON.stringify(list));

        $.ajax({
            url: '/Logistics/GetItemsByItemlistId',
            data: fd,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (rs) {
                if (rs.Error) {
                    App.notifyDanger(rs.Title);
                }
                else {
                    $scope.ListData1 = rs;
                    $scope.lstdata = [];
                    for (var j = 0; j < $scope.ListData.length; j++) {
                        let val1 = $scope.ListData1.find(o => o.ItemID.toUpperCase() === $scope.ListData[j].ItemID.toUpperCase());
                        var value = $scope.ListData[j];
                        value.ItemName = val1.ItemName !== null ? val1.ItemName : "";
                        value.UnitID = val1.UnitID !== null ? val1.UnitID : 0;
                        value.UnitName = val1.UnitName !== null ? val1.UnitName : 0;
                        var obj = [
                            value.RowGuid,
                            value.ItemID,
                            value.ItemName,
                            value.UnitID,
                            value.UnitName,
                            value.Quantity,
                            value.UnitPrice,
                            value.AmountOc,
                            value.VATPercent,
                            value.VATAmountOC,
                            value.Note];
                        $scope.lstdata.push(obj);
                    }
                    $scope.jxcelAddDetailInit.data = $scope.lstdata;
                    $scope.onload1();
                }
            },
            error: function (rs) {
                App.notifyInfo(rs.Title);
            }
        });
    }
    $scope.roundNumber = function (number) {
        if (number > 0) {
            return parseFloat(number);
        } else return 0;

    }
    $scope.onload1 = function () {
        if ($('#myht').jexcel('getData') !== undefined) {
            $rootScope.Quantity = 0;
            $rootScope.AmountOc = 0;
            $rootScope.UnitPrice = 0;
            $rootScope.VATAmountOC = 0;
            $scope.ListData = $('#myht').jexcel('getData');
            angular.forEach($scope.ListData, function (val, key) {
                if (val.Quantity !== null && val.Quantity !== "" && val.Quantity !== undefined) {
                    $rootScope.Quantity += $scope.roundNumber(parseFloat(val.Quantity));
                }
                if (val.UnitPrice !== null && val.UnitPrice !== "" && val.UnitPrice !== undefined) {
                    $rootScope.UnitPrice += $scope.roundNumber(parseFloat(val.UnitPrice));
                }
                if (val.AmountOc !== null && val.AmountOc !== "" && val.AmountOc !== undefined) {
                    $rootScope.AmountOc += $scope.roundNumber(parseFloat(val.AmountOc));
                }

            });
            $('#myht #colfoot-6').html($rootScope.addPeriod($scope.Quantity));
            $('#myht #colfoot-7').html($rootScope.addPeriod($scope.UnitPrice));
            $('#myht #colfoot-8').html($rootScope.addPeriod($scope.AmountOc));
            if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                $scope.$apply();
            }
        }
    }

    $scope.dropdownItemId = function (d1, d2, d3) {
        var _tem1 = d1.ItemID;
        if (_tem1 === "" || _tem1 === null) {
            _tem1 = "";
        }
        var obj = {
            Keyword: _tem1
        }
        dataservice.getItemALL(obj, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {

                d3.source = rs;
                d2(rs, "");
            }
        })
    }
    $scope.dropdownUnitID = function (d1, d2, d3) {
        var _tem1 = d1.UnitID;
        if (_tem1 === "" || _tem1 === null) {
            _tem1 = "";
        }
        var obj = {
            Keyword: _tem1
        }
        dataservice.getUnitJexcel(obj, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {

                d3.source = rs;
                d2(rs, "");
            }
        })
    }
    $scope.jxcelAddDetailInit = {
        dataObject: true,
        reloadData: {},
        data: [],
        colHeaders: ['', 'Mã dịch vụ', 'Tên dịch vụ', 'Mô tả chi tiết', 'Mã đơn vị tính', 'Tên đơn vị tính', 'Số lượng', 'Đơn giá', 'Thành tiền', 'ETD/ETA', 'Hình thức thanh toán', 'Ghi chú'
        ],
        colTitles: ['', 'Mã dịch vụ', 'Tên dịch vụ', 'Mô tả chi tiết', 'Mã đơn vị tính', 'Tên đơn vị tính', 'Số lượng', 'Đơn giá', 'Thành tiền', 'ETD/ETA', 'Hình thức thanh toán', 'Ghi chú'
        ],
        colFooters: ['', '', '', '', '', '', '', '', '', '', '', ''
        ],
        columns: [
            {
                name: 'RowGuid',
                type: 'text',
                isFixColumn: true,
            },
            {
                name: 'ItemID',
                type: 'text'
            },
            {
                name: 'ItemName',
                type: 'text'
            },
            {
                name: 'Title',
                type: 'text'
            },
            {
                name: 'UnitID',
                type: 'autocomplete',
                source: $rootScope.litsUnitID,
                sourceReload: $scope.dropdownUnitID,
                searchReload: true,
                onlyCode: true
            },
            {
                name: 'UnitName',
                type: 'text'
            },
            {
                name: 'Quantity',//Xếp hạng 3
                type: 'numeric', formatNumber: 'VN-NUMERIC'

            },
            {
                name: 'UnitPrice',//Xếp hạng 15
                type: 'numeric', formatNumber: 'VN-NUMERIC'
            },
            {
                name: 'AmountOc',//Xếp hạng 15
                type: 'numeric', formatNumber: 'VN-NUMERIC'
            },
            {
                name: 'ETDETA',//Xếp hạng 15
                type: 'text',
            }
            ,
            {
                name: 'BookingNumber',
                type: 'autocomplete',
                url: '/Sale/Logistics/BookingNumber/',
                readOnly: false,
                updateReadOnly: true
            },
            {
                name: 'Note',
                type: 'text'
            }
        ],

        columnShow: [false, true, true, true, true, true, true, true, true, true, true, true],
        initDataRows: ['', '', '', '', '', '', '', '', '', '', '', ''],
        colWidths: [50, 150, 200, 200, 120, 120, 150, 150, 150, 200, 150, 250],
        allrowevent: true,
        deleterowconfirm: $scope.deleteItem,
        tableOverflow: true,
        csvHeaders: true,
        changeCell: $scope.rowchange,
        tableHeight: '200px',
        onpaste: $scope.onpaste1,
        onload: $scope.onload1
    };

    if (para != "") {
        $scope.Title = "Sửa vận chuyển";
        $scope.model.Temps = "2";
        $scope.disColumn = true;
        $scope.initData = function () {
            dataservice.getById(para, function (rs) {
                if (rs.Error) {
                    App.notifyDanger(rs.Title);
                } else {
                    $scope.model = JSON.parse(rs.Table1)[0];
                    $scope.lstDetail = JSON.parse(rs.Table2);
                    var ApplyOutsideDetails = [];
                    angular.forEach($scope.lstDetail, function (value, key) {
                        ApplyOutsideDetails.push([
                            value.RowGuid,
                            value.ItemID,
                            value.ItemName,
                            value.Title,
                            value.UnitID,
                            value.UnitName,
                            value.Quantity,
                            value.UnitPrice,
                            value.AmountOc,
                            value.ETDETA,
                            value.BookingNumber,
                            value.Note
                        ]);
                    });
                    $scope.jxcelAddDetailInit.data = ApplyOutsideDetails;
                    $scope.onload1();
                    if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                        $scope.$apply();
                    }
                }
            })
        }
        $scope.initData();
    } else {
        $scope.Title = "Thêm mới vận chuyển";
        $scope.model.Temps = "1";
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    //Cấu hình file đính kèm
    $scope.model.FileAttachments = [];
    var uploader = $scope.uploader = new FileUploader({
        // url: App.baseService() + '/upload'
    });
    uploader.filters.push({
        name: 'customFilter',
        fn: function (item /*{File|FileLikeObject}*/, options) {
            return this.queue.length < 10;
        }
    });
    uploader.onSuccessItem = function (fileItem, response, status, headers) {
        $scope.model.FileAttachments.push(response);
    };
    uploader.onCompleteAll = function () {
        $scope.commit();
    };
    $scope.ChangeProductionManagerGuid = function (data) {
        $scope.model.ProductionManagerGuid = data;
    }
    $scope.insert = function () {

        if ($scope.addform.validate()) {
            var fd = new FormData();
            $scope.lstTitlefile = [];
            if (uploader.queue.length > 0) {
                for (i = 0; i < uploader.queue.length; i++) {
                    var files = uploader.queue[i]._file;
                    if (files.size > 30720000) {
                        App.notifyDanger("Yêu cầu file đính kèm không vượt quá 3MB");
                        return;
                    }
                    fd.append("file", files);
                }
            }
            var datatab1 = $scope.jxcelAddDetail.jexcel('getData');
            var ApplyOutsideDetails = [];
            for (var i = 0; i < datatab1.length; i++) {
                var value = datatab1[i];
                if (value.RowGuid != null && value.RowGuid != undefined && value.RowGuid != "") {
                    ApplyOutsideDetails.push({
                        RowGuid: value.RowGuid,
                        ItemID: value.ItemID,
                        ItemName: value.ItemName,
                        Title: value.Title,
                        UnitID: value.UnitID,
                        UnitName: value.UnitName,
                        Quantity: value.Quantity,
                        UnitPrice: value.UnitPrice,
                        AmountOc: value.AmountOc,
                        ETDETA: value.ETDETA,
                        BookingNumber: value.BookingNumber,
                        Note: value.Note,
                        SortOrder: i
                    });
                }
                else {
                    ApplyOutsideDetails.push({
                        ItemID: value.ItemID,
                        ItemName: value.ItemName,
                        Title: value.Title,
                        UnitID: value.UnitID,
                        UnitName: value.UnitName,
                        Quantity: value.Quantity,
                        UnitPrice: value.UnitPrice,
                        AmountOc: value.AmountOc,
                        ETDETA: value.ETDETA,
                        BookingNumber: value.BookingNumber,
                        Note: value.Note,
                        SortOrder: i
                    });
                }
            };
            if (ApplyOutsideDetails.length == 0) {
                App.notifyDanger("Yêu cầu nhập chi tiết");
                return;
            }

            fd.append('submit', JSON.stringify($scope.model));
            fd.append('detail', JSON.stringify(ApplyOutsideDetails));
            $.ajax({
                type: "POST",
                url: "/Logistics/Submit",
                contentType: false,
                processData: false,
                data: fd,
                success: function (rs) {
                    if (rs.Error) {
                        App.notifyDanger(rs.Title);
                    } else {
                        App.notifyInfo(rs.Title);
                        $uibModalInstance.close();
                        $rootScope.reload();
                    }
                }
            });
        }
    };
});
// controller open
app.controller('open', function ($scope, $rootScope, $uibModalInstance, dataservice, para, FileUploader) {
    $scope.Title = "Xem vận chuyển";
    $scope.model = { Temps: "" };
    $scope.disColumn = false;
    $scope.check = false;
    $scope.fullscreen = function (Check) {
        if (Check == false) {
            $scope.check = true;
        } else {
            $scope.check = false;
        }
    }

    $scope.roundNumber = function (number) {
        if (number > 0) {
            return parseFloat(number);
        } else return 0;

    }
    $scope.onload1 = function () {
        if ($('#myht').jexcel('getData') !== undefined) {
            $rootScope.Quantity = 0;
            $rootScope.AmountOc = 0;
            $rootScope.UnitPrice = 0;
            $rootScope.VATAmountOC = 0;
            $scope.ListData = $('#myht').jexcel('getData');
            angular.forEach($scope.ListData, function (val, key) {
                if (val.Quantity !== null && val.Quantity !== "" && val.Quantity !== undefined) {
                    $rootScope.Quantity += $scope.roundNumber(parseFloat(val.Quantity));
                }
                if (val.UnitPrice !== null && val.UnitPrice !== "" && val.UnitPrice !== undefined) {
                    $rootScope.UnitPrice += $scope.roundNumber(parseFloat(val.UnitPrice));
                }
                if (val.AmountOc !== null && val.AmountOc !== "" && val.AmountOc !== undefined) {
                    $rootScope.AmountOc += $scope.roundNumber(parseFloat(val.AmountOc));
                }

            });
            $('#myht #colfoot-6').html($rootScope.addPeriod($scope.Quantity));
            $('#myht #colfoot-7').html($rootScope.addPeriod($scope.UnitPrice));
            $('#myht #colfoot-8').html($rootScope.addPeriod($scope.AmountOc));
            if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                $scope.$apply();
            }
        }
    }

    $scope.dropdownItemId = function (d1, d2, d3) {
        var _tem1 = d1.ItemID;
        if (_tem1 === "" || _tem1 === null) {
            _tem1 = "";
        }
        var obj = {
            Keyword: _tem1
        }
        dataservice.getItemALL(obj, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {

                d3.source = rs;
                d2(rs, "");
            }
        })
    }
    $scope.dropdownUnitID = function (d1, d2, d3) {
        var _tem1 = d1.UnitID;
        if (_tem1 === "" || _tem1 === null) {
            _tem1 = "";
        }
        var obj = {
            Keyword: _tem1
        }
        dataservice.getUnitJexcel(obj, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {

                d3.source = rs;
                d2(rs, "");
            }
        })
    }
    $scope.jxcelAddDetailInit = {
        dataObject: true,
        reloadData: {},
        data: [],
        colHeaders: ['', 'Mã dịch vụ', 'Tên dịch vụ', 'Mô tả chi tiết', 'Mã đơn vị tính', 'Tên đơn vị tính', 'Số lượng', 'Đơn giá', 'Thành tiền', 'ETD/ETA', 'Hình thức thanh toán', 'Ghi chú'
        ],
        colTitles: ['', 'Mã dịch vụ', 'Tên dịch vụ', 'Mô tả chi tiết', 'Mã đơn vị tính', 'Tên đơn vị tính', 'Số lượng', 'Đơn giá', 'Thành tiền', 'ETD/ETA', 'Hình thức thanh toán', 'Ghi chú'
        ],
        colFooters: ['', '', '', '', '', '', '', '', '', '', '', ''
        ],
        columns: [
            {
                name: 'RowGuid',
                type: 'text',
                isFixColumn: true, readOnly: true
            },
            {
                name: 'ItemID',
                type: 'text', readOnly: true
            },
            {
                name: 'ItemName',
                type: 'text', readOnly: true
            },
            {
                name: 'Title',
                type: 'text', readOnly: true
            },
            {
                name: 'UnitID',
                type: 'autocomplete',
                source: $rootScope.litsUnitID,
                sourceReload: $scope.dropdownUnitID,
                searchReload: true,
                onlyCode: true, readOnly: true
            },
            {
                name: 'UnitName',
                type: 'text', readOnly: true
            },
            {
                name: 'Quantity',//Xếp hạng 3
                type: 'numeric', formatNumber: 'VN-NUMERIC', readOnly: true
            },
            {
                name: 'UnitPrice',//Xếp hạng 15
                type: 'numeric', formatNumber: 'VN-NUMERIC', readOnly: true
            },
            {
                name: 'AmountOc',//Xếp hạng 15
                type: 'numeric', formatNumber: 'VN-NUMERIC', readOnly: true
            },
            {
                name: 'ETDETA',//Xếp hạng 15
                type: 'text', readOnly: true
            },
            {
                name: 'BookingNumber',
                type: 'autocomplete',
                url: '/Sale/Logistics/BookingNumber/',
                readOnly: true,
                updateReadOnly: true
            },
            {
                name: 'Note',
                type: 'text', readOnly: true,
            }
        ],
        columnShow: [false, true, true, true, true, true, true, true, true, true, true, true],
        initDataRows: ['', '', '', '', '', '', '', '', '', '', '', ''],
        colWidths: [50, 150, 200, 200, 120, 120, 150, 150, 150, 200, 150, 250],
        allrowevent: true,
        deleterowconfirm: $scope.deleteItem,
        tableOverflow: true,
        csvHeaders: true,
        changeCell: $scope.rowchange,
        tableHeight: '200px',
        onpaste: $scope.onpaste1,
        onload: $scope.onload1,
        allowInsertRow: false,
        contextMenu: function () { return ""; },
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
    var uploader = $scope.uploader = new FileUploader({
        // url: App.baseService() + '/upload'
    });
    uploader.filters.push({
        name: 'customFilter',
        fn: function (item /*{File|FileLikeObject}*/, options) {
            return this.queue.length < 10;
        }
    });
    $scope.initData = function () {
        dataservice.getById(para, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {
                $scope.model = JSON.parse(rs.Table1)[0];
                $scope.lstDetail = JSON.parse(rs.Table2);
                var ApplyOutsideDetails = [];
                angular.forEach($scope.lstDetail, function (value, key) {
                    ApplyOutsideDetails.push([
                        value.RowGuid,
                        value.ItemID,
                        value.ItemName,
                        value.Title,
                        value.UnitID,
                        value.UnitName,
                        value.Quantity,
                        value.UnitPrice,
                        value.AmountOc,
                        value.ETDETA,
                        value.BookingNumber,
                        value.Note
                    ]);
                });
                $scope.jxcelAddDetailInit.data = ApplyOutsideDetails;
                $scope.onload1();
                if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                    $scope.$apply();
                }
            }
        })
    }
    $scope.initData();
});
app.controller('addattchment', function ($scope, $rootScope, $compile, $uibModal, $confirm, $uibModalInstance, dataservice, para, para1, FileUploader) {
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
    $scope.model = { RecordGuid: para, STT: para1 };
    $scope.nameController = "";

    //Cấu hình file đính kèm
    var uploader = $scope.uploader = new FileUploader({
        // url: App.baseService() + '/upload'
    });
    uploader.filters.push({
        name: "customFilter",
        fn: function (item /*{File|FileLikeObject}*/, options) {
            return this.queue.length < 10;
        }
    });
    uploader.onCompleteAll = function () {
    };

    // Bảo hiểm
    $scope.getattachment = function () {
        var obj = {
            RecordGuid: $scope.model.RecordGuid
        }
        dataservice.getattachment(JSON.stringify(obj), function (result) {
            $scope.jdataattach = result;
            angular.forEach($scope.jdataattach, function (value, key) {
                if (value.CreatedBy !== null && value.CreatedBy !== undefined) {
                    if (value.CreatedBy.includes('#')) {
                        value.CreatedBy = value.CreatedBy.split('#')[1];
                    }
                }
            })
        });
    }
    $scope.getattachment(); // danh sách attach bảo hiểm

    $scope.lstDeleteFile = [];
    $scope.remoteAttach = function (item, number) {
        $scope.lstDeleteFile.push(item.AttachmentGuid);
        $scope.jdataattach.splice(number, 1);
        App.notifyInfo("Xóa thành công");
        if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
            $scope.$apply();
        }
    }
    $scope.submit = function () {
        var fd = new FormData();
        $scope.lstTitlefile = [];
        if (uploader.queue.length > 0) {
            for (i = 0; i < uploader.queue.length; i++) {
                var files = uploader.queue[i]._file;
                var titleName = "";
                if (uploader.queue[i].file.FileName === "" || uploader.queue[i].file.FileName === null || uploader.queue[i].file.FileName === undefined) {
                    titleName = uploader.queue[i].file.name.split('.')[0];
                } else {
                    titleName = uploader.queue[i].file.FileName;
                }
                $scope.lstTitlefile.push({ STT: i, Title: titleName });
                fd.append("file", files);
            }
        }
        App.blockUI({
            target: ".modal-content",
            boxed: true,
            message: 'Đang tải...'
        });
        fd.append('model', JSON.stringify($scope.model));
        fd.append('lstTitlefile', JSON.stringify($scope.lstTitlefile));
        fd.append('DeleteAttach', JSON.stringify($scope.lstDeleteFile));
        $.ajax({
            type: "POST",
            url: "/ProductionManager/UpdateFile",
            contentType: false,
            processData: false,
            data: fd,
            success: function (rs) {
                if (rs.Error === true) {
                    App.notifyDanger(rs.Title);
                    App.unblockUI(".modal-content");
                } else {
                    $scope.lstDeleteFile = [];
                    uploader.queue = [];
                    App.notifyInfo(rs.Title);
                    $scope.getattachment();
                    App.unblockUI(".modal-content");
                }
            },
            error: function (rs) {
                App.notifyDanger(rs.Title);
            }
        })
    }
});
//Thông tin nhà cung cấp
app.controller('Customers_index', function ($http, $scope, $rootScope, $compile, $confirm, $uibModal, $uibModalInstance, DTOptionsBuilder, DTColumnBuilder, DTInstances, keyhandle, FileUploader, dataservice, para) {
    var vm = $scope;
    $scope.TitleIndex = "Danh sách nhà cung cấp dịch vụ Logistic";
    var ctrl = $scope;
    $scope.statusDisable = false;
    $scope.changeEdit = function (data) {
        if (data === 0) {
            $scope.statusDisable = false;
            $("div.SumoSelect").removeClass("SumoSelect disabled").addClass("SumoSelect");
        } else {
            $scope.statusDisable = true;
        }
    }
    ctrl.nextRow = function (d) {
        if (ctrl.ElementRowCheck === -1) {
            if (ctrl.allRow.length > 1) {
                ctrl.ElementRowCheck = ctrl.allRow[0].row;
                $('td', ctrl.ElementRowCheck).addClass("es-iSeleted");
                ctrl.iSelected = ctrl.allRow[0].data;
                $scope.getById_Customers(ctrl.iSelected.CustomerGuid);

            }
        } else
            for (var i = 0; i < ctrl.allRow.length; i++) {
                if (ctrl.allRow[i].row === ctrl.ElementRowCheck) {
                    if (i === ctrl.allRow.length - 1) break;
                    $('td', ctrl.ElementRowCheck).removeClass('es-iSeleted');
                    ctrl.ElementRowCheck = ctrl.allRow[i + 1].row;
                    $('td', ctrl.ElementRowCheck).addClass("es-iSeleted");
                    ctrl.iSelected = ctrl.allRow[i + 1].data;
                    $scope.getById_Customers(ctrl.iSelected.CustomerGuid);
                    break;
                }
            }
    }
    ctrl.upRow = function (d) {
        if (ctrl.ElementRowCheck === -1) {
            if (ctrl.allRow.length > 1) {
                ctrl.ElementRowCheck = ctrl.allRow[ctrl.allRow.length - 1].row;
                $('td', ctrl.ElementRowCheck).addClass("es-iSeleted");
                ctrl.iSelected = ctrl.allRow[ctrl.allRow.length - 1].row;
                $scope.getById_Customers(ctrl.iSelected.CustomerGuid);
            }
        } else
            for (var i = 0; i < ctrl.allRow.length; i++) {
                if (ctrl.allRow[i].row === ctrl.ElementRowCheck) {
                    if (i === 0) break;
                    $('td', ctrl.ElementRowCheck).removeClass('es-iSeleted');
                    ctrl.ElementRowCheck = ctrl.allRow[i - 1].row;
                    $('td', ctrl.ElementRowCheck).addClass("es-iSeleted");
                    ctrl.iSelected = ctrl.allRow[i - 1].data;
                    $scope.getById_Customers(ctrl.iSelected.CustomerGuid);

                    break;
                }
            }
    }
    keyhandle.eventCtrl(40, ctrl.nextRow);
    keyhandle.eventCtrl(38, ctrl.upRow);
    ctrl.ElementRowCheck = -1;
    ctrl.allRow = [];
    ctrl.iSelected = {};
    ctrl.dataScrollPage = [];
    $scope.selected = [];
    $scope.selectAll = false;
    $scope.toggleAll = toggleAll;
    $scope.toggleOne = toggleOne;
    $scope.dataload = [];
    $scope.CheckReload = false;
    $scope.staticParam = {
        totalItems: 0,
        CurrentPage: 1,
        pageSize: 25,
        maxSize: 5,
        Keyword: "",
        EmployeeId: null,
        CustomerGuid: null
    };
    $scope.txtComment = "";

    $scope.lstInt = [];
    $rootScope.TongGT = 0;
    $scope.selected = [];
    $scope.selectAll = false;
    $scope.toggleAll = toggleAll;
    $scope.toggleOne = toggleOne;
    var titleHtml = '<label class="mt-checkbox"><input type="checkbox" ng-model="selectAll" ng-click="toggleAll(selectAll, selected)"/><span></span></label>';

    vm.dtOptions = DTOptionsBuilder.newOptions()
        .withOption('serverSide', true)
        .withOption('order', [1, 'desc'])
        .withDataProp('data')
        .withOption('pageLength', 25)
        .withOption('initComplete', function (settings, json) {
            $('.dataTables_scrollBody').css({
                'overflow-y': 'scroll',
                'overflow-x': 'hidden',
                'border': 'none'
            });
            //hiền scroll footer
            $('#tblData_wrapper .dataTables_scrollFoot').css('overflow', 'auto');
            //cuộn toàn bảng theo footer
            $('#tblData_wrapper .dataTables_scrollFoot').on('scroll', function () {
                $('#tblData_wrapper .dataTables_scrollBody').scrollLeft($(this).scrollLeft());
            });

            //thêm sự kiện scroll phân trang
            $('#tblData').parent().attr("onscroll", "angular.element(this).scope().LoadScroll(this)");
        })
        .withOption('searching', false)
        .withOption('createdRow', function (row, data, dataIndex) {
            const contextScope = $scope.$new(true);
            contextScope.data = data;
            $scope.data = data;
            var obj = {
                STT: data._STT.toString(),
                Id: data.CustomerGuid
            }
            $scope.lstInt.push(obj);
            $compile(row)($scope);          
            contextScope.contextMenu = $scope.contextMenu;
            $compile(angular.element(row).find('input'))($scope);
            $compile(angular.element(row).attr('context-menu', 'contextMenu'))(contextScope);
        });
    vm.dtOptions.withOption('ajax', function (data, callback, settings) {
        $scope.staticParam.CurrentPage = $scope.dtInstance.DataTable.page() + 1;
        if ($scope.CheckReload) {
            data.Start = 0;
        }
        App.blockUI({
            target: "#table_load",
            boxed: true,
            message: 'Đang tải...'
        });
        $http.post('/Logistics/JTable_Customers', data)
            .success(function (res) {

                $scope.selected = [];
                if (data.start === 0) {
                    $scope.dataload = [];
                    angular.forEach(res.data, function (val, key) {
                        $scope.dataload.push(val);
                    });
                    callback({
                        data: $scope.dataload,
                        recordsTotal: res.recordsTotal,
                        recordsFiltered: res.recordsFiltered
                    });
                } else {
                    if (res.recordsTotal > $scope.dataload.length) {
                        angular.forEach(res.data, function (val, key) {
                            $scope.dataload.push(val);
                        });
                        callback({
                            data: $scope.dataload,
                            recordsTotal: res.recordsTotal,
                            recordsFiltered: res.recordsFiltered
                        });
                    }
                }
                if ($scope.dataload.length === 0) {
                    $scope.clear_Contracts();
                }
                App.unblockUI("#table_load");

            });
    });
    //scroll theo tỷ lệ màn hình
    vm.dtOptions.withOption('scrollY', '40vh')
        .withOption('scrollX', '100%') //mặc định 100%
        .withOption('scrollCollapse', true) //xóa khoảng trắng khi dữ liệu trong bảng ít
        .withOption('rowCallback', rowCallback) //sự kiên click vào 1 dòng
        .withOption('scroller', {
            loadingIndicator: true
        }).withFixedColumns({
            leftColumns: 0 //fix bên trái 2 cột
            //rightColumns: 1 //fix bên phải 1 cột
        });

    vm.dtColumns = [];

    vm.dtColumns.push(DTColumnBuilder.newColumn('_STT').withTitle('STT').notSortable().withOption('sClass', 'tcenter').withOption('sWidth', '5px').renderWith(function (data, type, full, meta) {
        return meta.row + 1;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('CustomerId').withTitle('Mã nhà cung cấp').withOption('sWidth', '60px').renderWith(function (data, type, full, meta) {
        return data;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('CustomerName').withTitle('Tên nhà cung cấp').withOption('sWidth', '150px').renderWith(function (data, type, full, meta) {
        return data;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('Address').withTitle('Địa chỉ').withOption('sWidth', '150px').renderWith(function (data, type, full, meta) {
        return data;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('WorkPlace').withTitle('Liên hệ').withOption('sWidth', '120px').renderWith(function (data, type, full, meta) {
        return data;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('ActivityFieldName').withTitle('Sản phẩm').withOption('sWidth', '120px').renderWith(function (data, type, full, meta) {
        return data;
    }));

    vm.dtColumns.push(DTColumnBuilder.newColumn('LeadName').withTitle('Ký tên').withOption('sWidth', '100px').renderWith(function (data, type, full, meta) {
        return data;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('Method').withTitle('Hình thức TT').withOption('sWidth', '60px').renderWith(function (data, type, full, meta) {
        if (data == "C") {
            return "Chuyển khoản";
        } else if (data == "T") {
            return "Tiền mặt";
        } else {
            return "";
        }
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('Note').withTitle('Lưu ý khác').withOption('sWidth', '120px').renderWith(function (data, type, full, meta) {
        return data;
    }));

    vm.reloadData = reloadData;
    vm.dtInstance = {};
    function reloadData(resetPaging) {
        ctrl.ElementRowCheck = -1;
        $scope.staticParam.currentPage = resetPaging;
        vm.dtInstance.reloadData(callback, resetPaging);
        vm.dtInstance.DataTable.draw();
    }
    function callback(json) {

    }
    function toggleAll(selectAll, selectedItems) {
        for (var id in selectedItems) {
            if (selectedItems.hasOwnProperty(id)) {
                selectedItems[id] = selectAll;
            }
        }
    }
    function toggleOne(selectedItems) {
        for (var id in selectedItems) {
            if (selectedItems.hasOwnProperty(id)) {
                if (!selectedItems[id]) {
                    vm.selectAll = false;
                    return;
                }
            }
        }
        vm.selectAll = true;
    }
    $scope.total = 0;
    $scope.check = false;
    $scope.LoadScroll = function (obj) {
        var total = obj.offsetHeight + obj.scrollTop;
        if (obj.offsetHeight + obj.scrollTop >= obj.scrollHeight && $scope.check && $scope.total <= total) {
            $scope.check = false;
            try {
                $scope.CheckReload = false;
                vm.dtInstance.DataTable.page('next').draw('page');
            }
            catch (ex) {
            }
        }
        if (obj.offsetHeight + obj.scrollTop >= obj.scrollHeight) {
            $scope.total = obj.offsetHeight + obj.scrollTop;
        }
        else {
            $scope.check = true;
        }

    };
    function rowCallback(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        if (iDisplayIndexFull === 0) ctrl.allRow = [];
        $('td', nRow).unbind('click');
        $('td', nRow).bind('click', function () {
            if (vm.ElementRowCheck === nRow) return;
            $('td', nRow).addClass("es-iSeleted");
            $('td', vm.ElementRowCheck).removeClass('es-iSeleted');
            vm.ElementRowCheck = nRow;
            ctrl.ElementRowCheck = nRow;
            ctrl.iSelected = aData;
            vm.staticParam.CustomerGuid = aData.CustomerGuid;
            $scope.getById_Customers(vm.staticParam.CustomerGuid);
            $scope.changeEdit(1);
        });
        $('td', nRow).unbind('dblclick');
        $('td', nRow).bind('dblclick', function () {
            $scope.$apply(function () {
                $scope.getById_Customers(aData.CustomerGuid);
            });
        });
        ctrl.allRow.push({ row: nRow, data: aData });
        return nRow;
    }
    $scope.reload = function () {
        $scope.lstInt = [];
        $scope.dataload = [];
        reloadData(true);
    };
    $scope.addPeriod = function (nStr1) {
        if (nStr1 !== null && nStr1 !== undefined && nStr1 !== "") {
            var nStr = nStr1.split('.');
            nStr += '';
            x = nStr.split(',');
            x1 = x[0];
            x2 = x.length > 1 ? ',' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
            }
            return x1;
        } else {
            return "";
        }
    }
    $scope.contextMenu = [
        [function ($itemScope) {
            return '<i class="fa fa-remove"></i> Xóa';
        }, function ($itemScope, $event, model) {
            ESConfirm([{ 'value': 1, 'title': 'Xác nhận', 'type': 'info' }, { 'value': 2, 'title': 'Đóng', 'type': 'danger' }], { 'description': 'nhà cung cấp này không?', 'class': 'eswarning' }, function (rs) {
                if (rs === '1') {
                    App.blockUI({
                        target: "#contentMain",
                        boxed: true,
                        message: 'Đang tải...'
                    });
                    dataservice.delete_Customers($itemScope.data.CustomerGuid, function (result) {
                        if (result.Error) {
                            App.notifyDanger(result.Title);
                        } else {
                            App.notifyInfo(result.Title);
                            $scope.clear_Contracts();
                            $scope.reload();
                        }
                        App.unblockUI("#contentMain");
                    });
                }
                if (rs === '2') {
                    console.log('Không chấp nhận');
                }
            }, function () {
                //some error in this function
                console.log('Không lựa chọn');
            });

        }, function ($itemScope, $event, model) {
            return true;
        }]
    ];

    $scope.ConvertNumber = function (number) {
        if (number != null && number != "") {
            return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
        return null;
    }

    $rootScope.validationOptions = {
        rules: {
            CustomerId: {
                required: true,
                maxlength: 20
            }
        },
        messages: {
            CustomerId: {
                required: "Yêu cầu nhập mã nhà cung cấp.",
                maxlength: "Mã nhà cung cấp không vượt quá 20 ký tự."
            }
        }
    }
    $rootScope.StatusData = [{
        value: 1,
        text: 'Sử dụng'
    }, {
        value: 0,
        text: 'Không sử dụng'
    }];
    $rootScope.ListMethod = [{
        value: "C",
        text: 'Chuyển khoản'
    }, {
        value: "T",
        text: 'Tiền mặt'
    }

    ];
    $('#tblData').parent().removeAttr("onscroll");
    $scope.cancel = function () {
        $('#tblData').parent().removeAttr("onscroll");
        $('#tblData_Salaries').parent().removeAttr("onscroll");
        $('#tblData_Insurances').parent().removeAttr("onscroll");
        $('#tblData_Contracts').parent().removeAttr("onscroll");
        $('#tblData_Histories').parent().removeAttr("onscroll");
        $('#tblData_Certificates').parent().removeAttr("onscroll");
        $('#tblData_Reward').parent().removeAttr("onscroll");
        $('#tblData_TrainingRequirement').parent().removeAttr("onscroll");
        $uibModalInstance.dismiss('cancel');
    }

    //================Thêm hợp đồng===========================
    $scope.model = {};
    $scope.ChangeBusinessTypeName = function (data) {
        $scope.model.BusinessTypeName = data;
    }
    $scope.ChangeMethod = function (data) {
        $scope.model.Method = data;
    }
    $scope.model.FileAttachments = [];
    var uploader = $scope.uploader = new FileUploader({
        // url: App.baseService() + '/upload'
    });
    uploader.filters.push({
        name: 'customFilter',
        fn: function (item /*{File|FileLikeObject}*/, options) {
            return this.queue.length < 10;
        }
    });
    uploader.onSuccessItem = function (fileItem, response, status, headers) {
        $scope.model.FileAttachments.push(response);
    };

    uploader.onCompleteAll = function () {
        $scope.commit();
    };

    $scope.getById_Customers = function (data) {
        dataservice.getById_Customers(data, function (rs) {
            if (rs !== null) {
                $scope.model = rs;
                $scope.statusDisable = false;

            } else {
                $scope.clear_Contracts();
            }
        });
    }
    $scope.clear_Contracts = function () {
        $scope.model = {};
        $scope.model.CustomerGuid = undefined;
        $scope.model.CustomerId = "";
        $scope.model.CustomerName = "";
        $scope.model.Address = "";
        $scope.model.WorkPlace = "";
        $scope.model.ActivityFieldName = "";
        $scope.model.BusinessTypeName = "";
        $scope.model.LeadName = "";
        $scope.model.Method = "T";
        $scope.model.Note = "";
        $scope.lstDeleteFile = [];
        uploader.queue = [];
        $scope.jdataattach = [];
        $scope.statusDisable = false;
        $("div.SumoSelect").removeClass("SumoSelect disabled").addClass("SumoSelect");
    }
    $scope.lstDeleteFile = [];
    $scope.submit_Contracts = function () {
        if ($scope.addform.validate()) {
            var fd = new FormData();
            $scope.model.IsLeads = true;
            if ($scope.model.CustomerGuid != undefined && $scope.model.CustomerGuid != null) {
                fd.append('submit', JSON.stringify($scope.model));
                fd.append('DeleteAttach', JSON.stringify($scope.lstDeleteFile));
                $.ajax({
                    type: "POST",
                    url: "/Customers/Update",
                    contentType: false,
                    processData: false,
                    data: fd,
                    success: function (rs) {
                        if (rs.Error) {
                            App.notifyDanger(rs.Title);
                        } else {
                            App.notifyInfo("Cập nhật thành công");
                            $scope.reload();
                        }
                    }
                });
            }
            else {
                fd.append('submit', JSON.stringify($scope.model));
                $.ajax({
                    type: "POST",
                    url: "/Customers/Insert",
                    contentType: false,
                    processData: false,
                    data: fd,
                    success: function (rs) {
                        if (rs.Error) {
                            App.notifyDanger(rs.Title);
                        } else {
                            App.notifyInfo(rs.Title);
                            $scope.reload();
                        }
                    }
                });
            }
        }
    };


});
//Thông tin chứng từ xuất khẩu
app.controller('ExportDocuments_index', function ($http, $scope, $rootScope, $compile, $confirm, $uibModal, $uibModalInstance, DTOptionsBuilder, DTColumnBuilder, DTInstances, keyhandle, FileUploader, dataservice, para) {
    var vm = $scope;
    $scope.TitleIndex = "Danh sách chứng từ xuất khẩu";
    var ctrl = $scope;
    $scope.statusDisable = false;
    $scope.changeEdit = function (data) {
        if (data === 0) {
            $scope.statusDisable = false;
            $("div.SumoSelect").removeClass("SumoSelect disabled").addClass("SumoSelect");
        } else {
            $scope.statusDisable = true;
        }
    }
    ctrl.nextRow = function (d) {
        if (ctrl.ElementRowCheck === -1) {
            if (ctrl.allRow.length > 1) {
                ctrl.ElementRowCheck = ctrl.allRow[0].row;
                $('td', ctrl.ElementRowCheck).addClass("es-iSeleted");
                ctrl.iSelected = ctrl.allRow[0].data;
                $scope.getById_Customers(ctrl.iSelected.CustomerGuid);

            }
        } else
            for (var i = 0; i < ctrl.allRow.length; i++) {
                if (ctrl.allRow[i].row === ctrl.ElementRowCheck) {
                    if (i === ctrl.allRow.length - 1) break;
                    $('td', ctrl.ElementRowCheck).removeClass('es-iSeleted');
                    ctrl.ElementRowCheck = ctrl.allRow[i + 1].row;
                    $('td', ctrl.ElementRowCheck).addClass("es-iSeleted");
                    ctrl.iSelected = ctrl.allRow[i + 1].data;
                    $scope.getById_Customers(ctrl.iSelected.CustomerGuid);
                    break;
                }
            }
    }
    ctrl.upRow = function (d) {
        if (ctrl.ElementRowCheck === -1) {
            if (ctrl.allRow.length > 1) {
                ctrl.ElementRowCheck = ctrl.allRow[ctrl.allRow.length - 1].row;
                $('td', ctrl.ElementRowCheck).addClass("es-iSeleted");
                ctrl.iSelected = ctrl.allRow[ctrl.allRow.length - 1].row;
                $scope.getById_Customers(ctrl.iSelected.CustomerGuid);
            }
        } else
            for (var i = 0; i < ctrl.allRow.length; i++) {
                if (ctrl.allRow[i].row === ctrl.ElementRowCheck) {
                    if (i === 0) break;
                    $('td', ctrl.ElementRowCheck).removeClass('es-iSeleted');
                    ctrl.ElementRowCheck = ctrl.allRow[i - 1].row;
                    $('td', ctrl.ElementRowCheck).addClass("es-iSeleted");
                    ctrl.iSelected = ctrl.allRow[i - 1].data;
                    $scope.getById_Customers(ctrl.iSelected.CustomerGuid);

                    break;
                }
            }
    }
    keyhandle.eventCtrl(40, ctrl.nextRow);
    keyhandle.eventCtrl(38, ctrl.upRow);
    ctrl.ElementRowCheck = -1;
    ctrl.allRow = [];
    ctrl.iSelected = {};
    ctrl.dataScrollPage = [];
    $scope.selected = [];
    $scope.selectAll = false;
    $scope.toggleAll = toggleAll;
    $scope.toggleOne = toggleOne;
    $scope.dataload = [];
    $scope.CheckReload = false;
    $scope.staticParam = {
        totalItems: 0,
        CurrentPage: 1,
        pageSize: 25,
        maxSize: 5,
        Keyword: "",
        EmployeeId: null,
        CustomerGuid: null
    };
    $scope.txtComment = "";

    $scope.lstInt = [];
    $rootScope.TongGT = 0;
    $scope.selected = [];
    $scope.selectAll = false;
    $scope.toggleAll = toggleAll;
    $scope.toggleOne = toggleOne;
    var titleHtml = '<label class="mt-checkbox"><input type="checkbox" ng-model="selectAll" ng-click="toggleAll(selectAll, selected)"/><span></span></label>';

    vm.dtOptions = DTOptionsBuilder.newOptions()
        .withOption('serverSide', true)
        .withOption('order', [1, 'desc'])
        .withDataProp('data')
        .withOption('pageLength', 25)
        .withOption('initComplete', function (settings, json) {
            $('.dataTables_scrollBody').css({
                'overflow-y': 'scroll',
                'overflow-x': 'hidden',
                'border': 'none'
            });
            //hiền scroll footer
            $('#tblData_wrapper .dataTables_scrollFoot').css('overflow', 'auto');
            //cuộn toàn bảng theo footer
            $('#tblData_wrapper .dataTables_scrollFoot').on('scroll', function () {
                $('#tblData_wrapper .dataTables_scrollBody').scrollLeft($(this).scrollLeft());
            });

            //thêm sự kiện scroll phân trang
            $('#tblData').parent().attr("onscroll", "angular.element(this).scope().LoadScroll(this)");
        })
        .withOption('searching', false)
        .withOption('createdRow', function (row, data, dataIndex) {
            const contextScope = $scope.$new(true);
            contextScope.data = data;
            $scope.data = data;
            var obj = {
                STT: data._STT.toString(),
                Id: data.CustomerGuid
            }
            $scope.lstInt.push(obj);
            $compile(row)($scope);
           
            contextScope.contextMenu = $scope.contextMenu;
            $compile(angular.element(row).find('input'))($scope);
            $compile(angular.element(row).attr('context-menu', 'contextMenu'))(contextScope);
        });
    vm.dtOptions.withOption('ajax', function (data, callback, settings) {
        $scope.staticParam.CurrentPage = $scope.dtInstance.DataTable.page() + 1;
        if ($scope.CheckReload) {
            data.Start = 0;
        }
        App.blockUI({
            target: "#table_load",
            boxed: true,
            message: 'Đang tải...'
        });
        $http.post('/Logistics/JTable_ExportDocuments', data)
            .success(function (res) {

                $scope.selected = [];
                if (data.start === 0) {
                    $scope.dataload = [];
                    angular.forEach(res.data, function (val, key) {
                        $scope.dataload.push(val);
                    });
                    callback({
                        data: $scope.dataload,
                        recordsTotal: res.recordsTotal,
                        recordsFiltered: res.recordsFiltered
                    });
                } else {
                    if (res.recordsTotal > $scope.dataload.length) {
                        angular.forEach(res.data, function (val, key) {
                            $scope.dataload.push(val);
                        });
                        callback({
                            data: $scope.dataload,
                            recordsTotal: res.recordsTotal,
                            recordsFiltered: res.recordsFiltered
                        });
                    }
                }
                if ($scope.dataload.length === 0) {
                    $scope.clear_Contracts();
                }
                App.unblockUI("#table_load");

            });
    });
    //scroll theo tỷ lệ màn hình
    vm.dtOptions.withOption('scrollY', '40vh')
        .withOption('scrollX', '100%') //mặc định 100%
        .withOption('scrollCollapse', true) //xóa khoảng trắng khi dữ liệu trong bảng ít
        .withOption('rowCallback', rowCallback) //sự kiên click vào 1 dòng
        .withOption('scroller', {
            loadingIndicator: true
        }).withFixedColumns({
            leftColumns: 0 //fix bên trái 2 cột
            //rightColumns: 1 //fix bên phải 1 cột
        });

    vm.dtColumns = [];

    vm.dtColumns.push(DTColumnBuilder.newColumn('_STT').withTitle('STT').notSortable().withOption('sClass', 'tcenter').withOption('sWidth', '5px').renderWith(function (data, type, full, meta) {
        return meta.row + 1;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('CodeID').withTitle('Mã chứng từ').withOption('sWidth', '60px').renderWith(function (data, type, full, meta) {
        return data;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('Title').withTitle('Tên chứng từ').withOption('sWidth', '150px').renderWith(function (data, type, full, meta) {
        return data;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('ExportMarket').withTitle('Thị trường xuất khẩu').withOption('sWidth', '150px').renderWith(function (data, type, full, meta) {
        return data;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('ExportForm').withTitle('Mẫu/Form chứng từ').withOption('sWidth', '120px').renderWith(function (data, type, full, meta) {
        return data;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('ExportFile').withTitle('Bộ hồ sơ').withOption('sWidth', '120px').renderWith(function (data, type, full, meta) {
        return data;
    }));

    vm.dtColumns.push(DTColumnBuilder.newColumn('Agencies').withTitle('Cơ quan cấp').withOption('sWidth', '100px').renderWith(function (data, type, full, meta) {
        return data;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('EmployeeName').withTitle('Ký tên').withOption('sWidth', '60px').renderWith(function (data, type, full, meta) {
        return data;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('Note').withTitle('Lưu ý khác').withOption('sWidth', '120px').renderWith(function (data, type, full, meta) {
        return data;
    }));

    vm.reloadData = reloadData;
    vm.dtInstance = {};
    function reloadData(resetPaging) {
        ctrl.ElementRowCheck = -1;
        $scope.staticParam.currentPage = resetPaging;
        vm.dtInstance.reloadData(callback, resetPaging);
        vm.dtInstance.DataTable.draw();
    }
    function callback(json) {

    }
    function toggleAll(selectAll, selectedItems) {
        for (var id in selectedItems) {
            if (selectedItems.hasOwnProperty(id)) {
                selectedItems[id] = selectAll;
            }
        }
    }
    function toggleOne(selectedItems) {
        for (var id in selectedItems) {
            if (selectedItems.hasOwnProperty(id)) {
                if (!selectedItems[id]) {
                    vm.selectAll = false;
                    return;
                }
            }
        }
        vm.selectAll = true;
    }
    $scope.total = 0;
    $scope.check = false;
    $scope.LoadScroll = function (obj) {
        var total = obj.offsetHeight + obj.scrollTop;
        if (obj.offsetHeight + obj.scrollTop >= obj.scrollHeight && $scope.check && $scope.total <= total) {
            $scope.check = false;
            try {
                $scope.CheckReload = false;
                vm.dtInstance.DataTable.page('next').draw('page');
            }
            catch (ex) {
            }
        }
        if (obj.offsetHeight + obj.scrollTop >= obj.scrollHeight) {
            $scope.total = obj.offsetHeight + obj.scrollTop;
        }
        else {
            $scope.check = true;
        }

    };
    function rowCallback(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        if (iDisplayIndexFull === 0) ctrl.allRow = [];
        $('td', nRow).unbind('click');
        $('td', nRow).bind('click', function () {
            if (vm.ElementRowCheck === nRow) return;
            $('td', nRow).addClass("es-iSeleted");
            $('td', vm.ElementRowCheck).removeClass('es-iSeleted');
            vm.ElementRowCheck = nRow;
            ctrl.ElementRowCheck = nRow;
            ctrl.iSelected = aData;
            vm.staticParam.CustomerGuid = aData.RowGuid;
            $scope.getById_Customers(vm.staticParam.CustomerGuid);
            $scope.changeEdit(1);
        });
        $('td', nRow).unbind('dblclick');
        $('td', nRow).bind('dblclick', function () {
            $scope.$apply(function () {
               // $scope.getById_Customers(aData.CustomerGuid);
            });
        });
        ctrl.allRow.push({ row: nRow, data: aData });
        return nRow;
    }
    $scope.reload = function () {
        ctrl.iSelected = [];
        $scope.lstInt = [];
        $scope.dataload = [];
        reloadData(true);
    };
    $scope.addPeriod = function (nStr1) {
        if (nStr1 !== null && nStr1 !== undefined && nStr1 !== "") {
            var nStr = nStr1.split('.');
            nStr += '';
            x = nStr.split(',');
            x1 = x[0];
            x2 = x.length > 1 ? ',' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
            }
            return x1;
        } else {
            return "";
        }
    }
    $scope.contextMenu = [
        [function ($itemScope) {
            return '<i class="fa fa-remove"></i> Xóa';
        }, function ($itemScope, $event, model) {
            ESConfirm([{ 'value': 1, 'title': 'Xác nhận', 'type': 'info' }, { 'value': 2, 'title': 'Đóng', 'type': 'danger' }], { 'description': 'bản ghi này không?', 'class': 'eswarning' }, function (rs) {
                if (rs === '1') {
                    App.blockUI({
                        target: "#contentMain",
                        boxed: true,
                        message: 'Đang tải...'
                    });
                    dataservice.Delete_ExportDocuments($itemScope.data.RowGuid, function (result) {
                        if (result.Error) {
                            App.notifyDanger(result.Title);
                        } else {
                            App.notifyInfo(result.Title);
                            $scope.clear_Contracts();
                            $scope.reload();
                        }
                        App.unblockUI("#contentMain");
                    });
                }
                if (rs === '2') {
                    console.log('Không chấp nhận');
                }
            }, function () {
                //some error in this function
                console.log('Không lựa chọn');
            });

        }, function ($itemScope, $event, model) {
            return true;
        }]
    ];

    $scope.ConvertNumber = function (number) {
        if (number != null && number != "") {
            return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
        return null;
    }

    $rootScope.validationOptions = {
        rules: {
            CodeID: {
                required: true,
                maxlength: 20
            }
        },
        messages: {
            CodeID: {
                required: "Yêu cầu nhập mã chứng từ.",
                maxlength: "Mã chứng từ không vượt quá 20 ký tự."
            }
        }
    }
    $rootScope.StatusData = [{
        value: 1,
        text: 'Sử dụng'
    }, {
        value: 0,
        text: 'Không sử dụng'
    }];
    $rootScope.ListMethod = [{
        value: "C",
        text: 'Chuyển khoản'
    }, {
        value: "T",
        text: 'Tiền mặt'
    }

    ];
    $('#tblData').parent().removeAttr("onscroll");
    $scope.cancel = function () {
        $('#tblData').parent().removeAttr("onscroll");
        $('#tblData_Salaries').parent().removeAttr("onscroll");
        $('#tblData_Insurances').parent().removeAttr("onscroll");
        $('#tblData_Contracts').parent().removeAttr("onscroll");
        $('#tblData_Histories').parent().removeAttr("onscroll");
        $('#tblData_Certificates').parent().removeAttr("onscroll");
        $('#tblData_Reward').parent().removeAttr("onscroll");
        $('#tblData_TrainingRequirement').parent().removeAttr("onscroll");
        $uibModalInstance.dismiss('cancel');
    }

    //================Thêm hợp đồng===========================
    $scope.model = {};
    $scope.ChangeBusinessTypeName = function (data) {
        $scope.model.BusinessTypeName = data;
    }
    $scope.ChangeMethod = function (data) {
        $scope.model.Method = data;
    }
    $scope.model.FileAttachments = [];
    var uploader = $scope.uploader = new FileUploader({
        // url: App.baseService() + '/upload'
    });
    uploader.filters.push({
        name: 'customFilter',
        fn: function (item /*{File|FileLikeObject}*/, options) {
            return this.queue.length < 10;
        }
    });
    uploader.onSuccessItem = function (fileItem, response, status, headers) {
        $scope.model.FileAttachments.push(response);
    };

    uploader.onCompleteAll = function () {
        $scope.commit();
    };

    $scope.getById_Customers = function (data) {
        dataservice.GetItem_ExportDocuments(data, function (rs) {
            if (rs !== null) {
                $scope.model = rs;
                $scope.statusDisable = false;

            } else {
                $scope.clear_Contracts();
            }
        });
    }
    $scope.clear_Contracts = function () {
        $scope.model = {};
        $scope.model.RowGuid = undefined;
        $scope.model.Title = "";
        $scope.model.CustomerName = "";
        $scope.model.Address = "";
        $scope.model.WorkPlace = "";
        $scope.model.ActivityFieldName = "";
        $scope.model.BusinessTypeName = "";
        $scope.model.LeadName = "";
        $scope.model.Method = "T";
        $scope.model.Note = "";
        $scope.lstDeleteFile = [];
        uploader.queue = [];
        $scope.jdataattach = [];
        $scope.statusDisable = false;
        $("div.SumoSelect").removeClass("SumoSelect disabled").addClass("SumoSelect");
    }
    $scope.lstDeleteFile = [];
    $scope.submit_Contracts = function () {
        if ($scope.addform.validate()) {
            var fd = new FormData();
            $scope.model.IsLeads = true;
            fd.append('submit', JSON.stringify($scope.model));
            $.ajax({
                type: "POST",
                url: "/Logistics/Submit_ExportDocuments",
                contentType: false,
                processData: false,
                data: fd,
                success: function (rs) {
                    if (rs.Error) {
                        App.notifyDanger(rs.Title);
                    } else {
                        if ($scope.model.RowGuid != undefined && $scope.model.RowGuid != null) {
                            App.notifyInfo("Cập nhật thành công");
                        } else {
                            App.notifyInfo("Thêm mới thành công");
                        }                      
                        $scope.reload();
                    }
                }
            });
        }
    };


});
