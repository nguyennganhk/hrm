﻿var ctxfolder = "/views/Sale/RatingTasks";
app.factory('dataservice', function ($http) {
    var headers = {
        "Content-Type": "application/json;odata=verbose",
        "Accept": "application/json;odata=verbose"
    };
    return {
        insert: function (data, callback) {
            $http.post('/RatingTasks/Insert', data).success(callback);
        },
        getItem: function (data, callback) {
            $http.post('/RatingTasks/GetById?Id=' + data).success(callback);
        },
        GetBieuDo: function (data, callback) {
             $http.post('/RatingTasks/GetBieuDo', data).success(callback);
        },
        
    };
});
app.filter('html', ['$sce', function ($sce) {
    return function (text) {
        return $sce.trustAsHtml(text);
    };
}]);

app.directive('format', ['$filter', function ($filter) {
    return {
        require: '?ngModel',
        link: function (scope, elem, attrs, ctrl) {
            if (!ctrl) return;
            ctrl.$formatters.unshift(function (a) {
                return $filter(attrs.format)(ctrl.$modelValue);
            });
            ctrl.$parsers.unshift(function (viewValue) {
                var plainNumber = viewValue.replace(/[^\d|\-+|\.+]/g, '');
                elem.val($filter(attrs.format)(plainNumber));
                return plainNumber;
            });
        }
    };
}]);
//định dạng ngày
app.filter('fdate', [
    '$filter', function ($filter) {
        return function (input, f) {
            if (input && input.toString().indexOf('StartDate') > -1) {
                return moment(input).format(f);
            } if (input && input.toString().indexOf('T') > -1) {
                return moment(input).format(f);
            } else return input;
        };
    }
]);

app.controller('Ctrl_RatingTasks', function ($scope, $rootScope, $sce, $compile, $uibModal, dataservice) {
    $rootScope.Title = "";
    $rootScope.go = function (path) {
        $location.path(path); return false;
    };

    $rootScope.validationOptions = {
        rules: {
            LableName: {
                required: true,
                maxlength: 30
            },
            ProjectTypeName: {
                required: true,
                maxlength: 50
            }
        },
        messages: {
            LableName: {
                required: "Yêu cầu nhập tên nhãn.",
                maxlength: "Tên nhãn không vượt quá 30 ký tự."
            },
            ProjectTypeName: {
                required: "Yêu cầu nhập tên loại dự án.",
                maxlength: "Tên loại dự án không vượt quá 50 ký tự."
            }
        }
    };

    $rootScope.listApproval = [{
        value: "0",
        text: 'Chưa đánh giá'
    }, {
        value: "1",
        text: 'Tốt hơn yêu cầu'
    }, {
        value: "2",
        text: 'Đúng yêu cầu'
    }
        , {
        value: "3",
        text: 'Không đúng yêu cầu'
    }
    ];
    $rootScope.dStatusProcess = [
        {
            value: 'T',
            text: 'Tất cả',
        },
        {
            value: 'X',
            text: 'Chờ giao',
        },
        {
            value: 'N',
            text: 'Chờ xử lý',
        },
        {
            value: 'P',
            text: 'Đang thực hiện',
        },
        {
            value: 'C',
            text: 'Hoàn thành',
        },
        {
            value: 'W',
            text: 'Chờ người khác',
        },
        {
            value: 'D',
            text: 'Hoãn lại',
        },
    ];
    //định dạng số xóa dấu chấm phẩy
    $rootScope.addPeriod = function (nStr) {
        if (nStr !== null && nStr !== "" && nStr !== undefined) {
            nStr = Math.round(parseFloat(nStr) * 100) / 100;

            nStr += "";
            if (nStr.indexOf(",") >= 0) {
                var x = nStr.split(",");
            } else {
                var x = nStr.split(".");
            }
            var x1 = x[0];
            var x2 = x.length > 1 && parseInt(x[1]) > 0 ? "," + x[1] : "";
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, "$1" + "." + "$2");
            }
            var Total = x1 + x2;
            return Total;

        }
        else {
            return 0;
        }
    };
    $rootScope.ConDate = function (data, number) {
        try {
            if (data == null || data == "") {
                return '';
            }
            if (data !== null && data != "" && data != undefined) {
                try {
                    if (data.indexOf("SA") != -1 || data.indexOf("CH") != -1) {
                        if (data.indexOf("SA") != -1) { }
                        if (data.indexOf("CH") != -1) { }
                    }

                    if (data.indexOf('Date') != -1) {
                        data = data.substring(data.indexOf("Date") + 5);
                        data = parseInt(data);
                    }
                }
                catch (ex) { }
                var newdate = new Date(data);
                if (number == 3) {
                    return newdate;
                }
                if (number == 2) {
                    return "/Date(" + newdate.getTime() + ")/";
                }
                var month = newdate.getMonth() + 1;
                var day = newdate.getDate();
                var year = newdate.getFullYear();
                var hh = newdate.getHours();
                var mm = newdate.getMinutes();
                if (month < 10)
                    month = "0" + month;
                if (day < 10)
                    day = "0" + day;
                if (mm < 10)
                    mm = "0" + mm;
                if (number == 0) {
                    return todayDate = day + "/" + month + "/" + year;
                }
                if (number == 1) {
                    return todayDate = day + "/" + month + "/" + year + " " + hh + ":" + mm;
                }
                if (number == 4) {
                    return new Date(year, month - 1, day);
                }
            } else {
                return '';
            }
        } catch (ex) {
            return "";
        }
    }

    $rootScope.ColumnMultiConfig = {
        selectAll: true,
        search: true,
        searchText: 'Tìm kiếm...',
        showTitle: 'true',
        csvSepChar: ',',
        captionFormat: '{0} lựa chọn',
        captionFormatAllSelected: '{0} lựa chọn',
        noMatch: 'Không có dữ liệu "{0}"',
        okCancelInMulti: true,
        locale: ['Đồng ý', 'Hủy', 'Chọn tất cả']
    };
    $rootScope.ColumnConfig = {
        search: true
    };
});
app.config(function ($routeProvider, $validatorProvider) {
    $routeProvider
        .when('/', {
            templateUrl: ctxfolder + '/index.html',
            controller: 'index'
        })
        .when('/edit/:id', {
            templateUrl: ctxfolder + '/edit.html',
            controller: 'edit'
        })
        .when('/add/', {
            templateUrl: ctxfolder + '/add.html',
            controller: 'add'
        });
    $validatorProvider.setDefaults({
        errorElement: 'span',
        errorClass: 'help-block',
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        success: function (label) {
            label.closest('.form-group').removeClass('has-error');
        }
    });
});
app.controller('index', function ($http, $scope, $timeout, $rootScope, $compile, $confirm, $ngConfirm, $uibModal, DTOptionsBuilder, DTColumnBuilder, DTInstances, dataservice, keyhandle) {
    var vm = $scope;
    var ctrl = $scope;
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    }

    ctrl.nextRow = function (d) {
        if (ctrl.ElementRowCheck === -1) {
            if (ctrl.allRow.length > 1) {
                ctrl.ElementRowCheck = ctrl.allRow[0].row;
                $('td', ctrl.ElementRowCheck).addClass("es-iSeleted");
                ctrl.iSelected = ctrl.allRow[0].data;
              //  ctrl.open_Detail(ctrl.iSelected.QuotationNo, ctrl.iSelected.CurrencyRate);

            }
        } else
            for (var i = 0; i < ctrl.allRow.length; i++) {
                if (ctrl.allRow[i].row === ctrl.ElementRowCheck) {
                    if (i === ctrl.allRow.length - 1) break;
                    $('td', ctrl.ElementRowCheck).removeClass('es-iSeleted');
                    ctrl.ElementRowCheck = ctrl.allRow[i + 1].row;
                    $('td', ctrl.ElementRowCheck).addClass("es-iSeleted");
                    ctrl.iSelected = ctrl.allRow[i + 1].data;
                  //  ctrl.open_Detail(ctrl.iSelected.QuotationNo, ctrl.iSelected.CurrencyRate);
                    break;
                }
            }
    }
    ctrl.upRow = function (d) {
        if (ctrl.ElementRowCheck === -1) {
            if (ctrl.allRow.length > 1) {
                ctrl.ElementRowCheck = ctrl.allRow[ctrl.allRow.length - 1].row;
                $('td', ctrl.ElementRowCheck).addClass("es-iSeleted");
                ctrl.iSelected = ctrl.allRow[ctrl.allRow.length - 1].row;
                ctrl.open_Detail(ctrl.iSelected.QuotationNo, ctrl.iSelected.CurrencyRate);
            }
        } else
            for (var i = 0; i < ctrl.allRow.length; i++) {
                if (ctrl.allRow[i].row === ctrl.ElementRowCheck) {
                    if (i === 0) break;
                    $('td', ctrl.ElementRowCheck).removeClass('es-iSeleted');
                    ctrl.ElementRowCheck = ctrl.allRow[i - 1].row;
                    $('td', ctrl.ElementRowCheck).addClass("es-iSeleted");
                    ctrl.iSelected = ctrl.allRow[i - 1].data;
                  //  ctrl.open_Detail(ctrl.iSelected.QuotationNo, ctrl.iSelected.CurrencyRate);
                    break;
                }
            }
    }
    keyhandle.eventCtrl(40, ctrl.nextRow);
    keyhandle.eventCtrl(38, ctrl.upRow);
    ctrl.ElementRowCheck = -1;
    ctrl.allRow = [];
    ctrl.iSelected = {};
    ctrl.dataScrollPage = [];
    var today = new Date();
    var day = today.getDate();
    var month = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    $scope.staticParam = {
        totalItems: 0,
        currentPage: 1,
        pageSize: 15,
        maxSize: 5,
        Search: "",
        StartDate: null,
        EndDate: null,
        StatusCompleted: "T",
        isBieudo: "0"
    };


    $scope.txtComment = "";
    $scope.dataload = [];
    $scope.CheckReload = false;
    $scope.lstInt = [];
    $rootScope.TongGT = 0;
    $scope.selected = [];
    $scope.selectAll = false;
    $scope.toggleAll = toggleAll;
    $scope.toggleOne = toggleOne;
    var titleHtml = '<label class="mt-checkbox"><input type="checkbox" ng-model="selectAll" ng-click="toggleAll(selectAll, selected)"/><span></span></label>';

    vm.dtOptions = DTOptionsBuilder.newOptions()
        .withOption('serverSide', true)
        .withOption('order', [3, 'desc'])
        .withDataProp('data')
        .withOption('pageLength', 25)
        .withOption('initComplete', function (settings, json) {
            //chỉ dùng khi có footer
            //ẩn scroll-x body
            $('.dataTables_scrollBody').css({
                'overflow-y': 'scroll',
                'overflow-x': 'hidden',
                'border': 'none'
            });
            //hiền scroll footer
            $('#tblData_wrapper .dataTables_scrollFoot').css('overflow', 'auto');
            //cuộn toàn bảng theo footer
            $('#tblData_wrapper .dataTables_scrollFoot').on('scroll', function () {
                $('#tblData_wrapper .dataTables_scrollBody').scrollLeft($(this).scrollLeft());
            });

            //thêm sự kiện scroll phân trang
            $('#tblData').parent().attr("onscroll", "angular.element(this).scope().LoadScroll(this)");
        })
        .withOption('searching', false)
        .withOption('createdRow', function (row, data, dataIndex) {
            const contextScope = $scope.$new(true);
            contextScope.data = data;
            $scope.data = data;
            var obj = {
                STT: data._STT.toString(),
                Id: data.TaskGuid
            }
            $scope.lstInt.push(obj);
            $compile(row)($scope);
            contextScope.contextMenu = $scope.contextMenu;
            $compile(angular.element(row).find('input'))($scope);
            $compile(angular.element(row).attr('context-menu', 'contextMenu'))(contextScope);
        });
    vm.dtOptions.withOption('ajax', function (data, callback, settings) {
        data.Keyword = $scope.staticParam.Search;
        data.StartDate = $scope.staticParam.StartDate;
        data.EndDate = $scope.staticParam.EndDate;
        data.StatusCompleted = $scope.staticParam.StatusCompleted;
        $scope.staticParam.currentPage = $scope.dtInstance.DataTable.page() + 1;
        if ($scope.CheckReload) {
            data.Start = 0;
        }
        App.blockUI({
            target: "#table_load",
            boxed: true,
            message: 'Đang tải...'
        });
        $http.post('/RatingTasks/JTable', data)
            .success(function (res) {
                if (data.start === 0) {
                    $scope.dataload = [];
                    angular.forEach(res.data, function (val, key) {
                        $scope.dataload.push(val);
                    });
                    callback({
                        data: $scope.dataload,
                        recordsTotal: res.recordsTotal,
                        recordsFiltered: res.recordsFiltered
                    });
                }
                else if (res.recordsTotal > $scope.dataload.length) {
                    angular.forEach(res.data, function (val, key) {
                        $scope.dataload.push(val);
                    });

                    callback({
                        data: $scope.dataload,
                        recordsTotal: res.recordsTotal,
                        recordsFiltered: res.recordsFiltered
                    });
                }
                App.unblockUI("#table_load");

            });
    });
    //scroll theo tỷ lệ màn hình
    vm.dtOptions.withOption('scrollY', '40vh')
        .withOption('scrollX', '100%') //mặc định 100%
        .withOption('scrollCollapse', true) //xóa khoảng trắng khi dữ liệu trong bảng ít
        .withOption('rowCallback', rowCallback) //sự kiên click vào 1 dòng
        .withOption('scroller', {
            loadingIndicator: true
        }).withFixedColumns({
            leftColumns: 0 //fix bên trái 2 cột
            //rightColumns: 1 //fix bên phải 1 cột
        });

    vm.dtColumns = [];

    vm.dtColumns.push(DTColumnBuilder.newColumn('_STT').withTitle('STT').notSortable().withOption('sClass', 'tcenter').withOption('sWidth', '5px').renderWith(function (data, type, full, meta) {
        return meta.row + 1;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('TaskNumber').withTitle('Mã công việc').withOption('sWidth', '60px').renderWith(function (data, type, full, meta) {
        return data;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('Subject').withOption('sWidth', '250px').withTitle('Tên công việc').renderWith(function (data, type, full, meta) {
        return data;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('StartDate').withOption('sWidth', '40px').withTitle('Ngày bắt đầu').withOption('sClass', 'tcenter').renderWith(function (data, type, full, meta) {
        return data;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('EndDate').withOption('sWidth', '40px').withTitle('Ngày đến hạn').withOption('sClass', 'tcenter').renderWith(function (data, type, full, meta) {
        return data;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('StatusCompleted').withOption('sWidth', '60px').withTitle('Trạng thái').withOption('sClass', 'tleft').renderWith(function (data, type, full, meta) {
        return data;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('Approval').withOption('sWidth', '60px').withTitle('Đánh giá KQ').withOption('sClass', 'tleft').renderWith(function (data, type, full, meta) {
        return data;
    }));

    vm.dtColumns.push(DTColumnBuilder.newColumn('ApprovalStar').withOption('sWidth', '40px').withTitle('Đánh giá').withOption('sClass', 'tcenter').renderWith(function (data, type, full, meta) {
        return data;
    }));
    vm.reloadData = reloadData;
    vm.dtInstance = {};
    function reloadData(resetPaging) {
        $scope.staticParam.currentPage = resetPaging;
        vm.dtInstance.reloadData(callback, resetPaging);
    }
    function callback(json) {

    }
    function toggleAll(selectAll, selectedItems) {
        for (var id in selectedItems) {
            if (selectedItems.hasOwnProperty(id)) {
                selectedItems[id] = selectAll;
            }
        }
    }
    function toggleOne(selectedItems) {
        for (var id in selectedItems) {
            if (selectedItems.hasOwnProperty(id)) {
                if (!selectedItems[id]) {
                    vm.selectAll = false;
                    return;
                }
            }
        }
        vm.selectAll = true;
    }
    $scope.total = 0;
    $scope.check = false;
    $scope.LoadScroll = function (obj) {
        var total = obj.offsetHeight + obj.scrollTop;
        if (obj.offsetHeight + obj.scrollTop >= obj.scrollHeight && $scope.check && $scope.total <= total) {
            $scope.check = false;
            try {
                $scope.CheckReload = false;
                vm.dtInstance.DataTable.page('next').draw('page');
            }
            catch (ex) {
            }
        }
        if (obj.offsetHeight + obj.scrollTop >= obj.scrollHeight) {
            $scope.total = obj.offsetHeight + obj.scrollTop;
        }
        else {
            $scope.check = true;
        }

    };

    $('#minize-menuleft').click(function () {
        vm.dtInstance.DataTable.draw();
    });
    function rowCallback(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        if (iDisplayIndexFull === 0) ctrl.allRow = [];
        $('td', nRow).unbind('click');
        $('td', nRow).bind('click', function () {
            if (vm.ElementRowCheck === nRow) return;
            $('td', nRow).addClass("es-iSeleted");
            $('td', vm.ElementRowCheck).removeClass('es-iSeleted');
            vm.ElementRowCheck = nRow;
            ctrl.ElementRowCheck = nRow;
            ctrl.iSelected = aData;
            vm.staticParam.TaskGuid = aData.TaskGuid;

        });
        $('td', nRow).unbind('dblclick');
        $('td', nRow).bind('dblclick', function () {
            $scope.$apply(function () {
                $scope.edit(aData.TaskGuid);
            });
        });
        ctrl.allRow.push({ row: nRow, data: aData });
        return nRow;
    }

    $rootScope.reloadData = function () {
        $scope.dataload = [];
        reloadData(true);
        $scope.reloadbySLDH();
    }
    $scope.changeOptionDate = function (d) {
        if (d.length === 0) {
            ctrl.staticParam.StartDate = null;
            ctrl.staticParam.EndDate = null;
        } else {
            ctrl.staticParam.StartDate = $rootScope.ConDate(d[0], 2);
            ctrl.staticParam.EndDate = $rootScope.ConDate(d[1], 2);
        }
        $scope.reloadbySLDH();
    }
    $scope.$watch('staticParam.StarDate', function (newvalue, oldvalue) {
        if (newvalue !== undefined && newvalue !== oldvalue) {
            $rootScope.reloadData();
        }
    });
    $scope.$watch('staticParam.EndDate', function (newvalue, oldvalue) {
        if (newvalue !== undefined && newvalue !== oldvalue) {
            $rootScope.reloadData();
        }
    });

    $scope.ChangeStatusCompleted = function (data) {
        $scope.staticParam.StatusCompleted = data;
        $rootScope.reloadData();
    }
    $scope.edit = function (data) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/edit.html',
            controller: 'edit',
            backdrop: 'static',
            size: '40',
            resolve: {
                para: function () {
                    return data;
                }
            }
        });

    }

    $scope.deleteChecked = function () {
        var deleteItems = [];
        for (var id in $scope.selected) {
            if ($scope.selected.hasOwnProperty(id)) {
                if ($scope.selected[id]) {
                    deleteItems.push(id);
                }
            }
        }
        if (deleteItems.length > 0) {
            ESConfirm([{ 'value': 1, 'title': 'Xác nhận', 'type': 'info' }, { 'value': 2, 'title': 'Đóng', 'type': 'danger' }], { 'description': 'Bạn có chắc chắn muốn xóa không?', 'class': 'eswarning_v2' }, function (rs) {
                if (rs === '1') {
                    App.blockUI({
                        target: "#contentMain",
                        boxed: true,
                        message: 'Đang tải...'
                    });
                    dataservice.deleteItems(deleteItems, function (result) {
                        if (result.Error) {
                            App.notifyDanger(result.Title);
                        } else {
                            App.notifyInfo(result.Title);
                            $rootScope.reloadData();
                        }
                        App.unblockUI("#contentMain");
                    });
                }
            });
        } else {
            App.notifyDanger("Không có khoản mục nào được chọn");
        }
    }

    $scope.contextMenu = [
        [function ($itemScope) {
            return '<i class="mdi mdi-table-edit"></i> Đánh giá kết quả';
        }, function ($itemScope, $event, model) {
            $scope.edit($itemScope.data.TaskGuid);
        }, function ($itemScope, $event, model) {
            return true;
        }]
    ];

    // Biểu đồ ------------------------------------------------------------------
    //Thống kê Đánh giá
    $scope.reloadbySLDH = function () {
          
        var obj = {
           Keyword : $scope.staticParam.Search,
            StartDate: $scope.staticParam.StartDate,
            EndDate: $scope.staticParam.EndDate,
           StatusCompleted : $scope.staticParam.StatusCompleted
        }
        dataservice.GetBieuDo(obj, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {
                $scope.dataProvider1 = [];     
                angular.forEach(rs, function (value, key) {                   
                    var ob = {
                        "year1": value.EmployeeName,
                        "year": value.EmployeeName,
                        "income": $rootScope.addPeriod(value.SaoDGRS),
                        "expenses": $rootScope.addPeriod(value.SaoDGRS)
                    };
                    $scope.dataProvider1.push(ob);
                    ob = {};
                });
                var chart = AmCharts.makeChart("chartdiv2", {
                    "type": "serial",
                    "addClassNames": true,
                    "theme": "none",
                    "autoMargins": false,
                    "marginLeft": 30,
                    "marginRight": 8,
                    "marginTop": 10,
                    "marginBottom": 26,
                    "balloon": {
                        "adjustBorderColor": false,
                        "horizontalPadding": 10,
                        "verticalPadding": 8,
                        "color": "#ffffff",
                        "font-size": 10
                    },
                    "dataProvider": $scope.dataProvider1,
                    "valueAxes": [{
                        "axisAlpha": 0,
                        "position": "left"
                    }],
                    "startDuration": 1,
                    "graphs": [{
                        "alphaField": "alpha",
                        "balloonText": "<span style='font-size:12px;'>[[expenses]]</span>",
                        "fillAlphas": 1,
                        "title": "Income",
                        "type": "column",
                        "valueField": "income",
                        "dashLengthField": "dashLengthColumn",
                        "labelText": "[[value]]",
                    }],
                    "categoryField": "year",
                    "categoryAxis": {
                        //"labelRotation": 90,
                        "fontSize": 10
                    },
                    "numberFormatter": {
                        "precision": -1,
                        "decimalSeparator": ",",
                        "thousandsSeparator": "."
                    },
                    "export": {
                        "enabled": true
                    }
                });
                if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                    $scope.$apply();
                }
            }
        });
       
    };
    $scope.ClickTab = function () {
        if ($scope.staticParam.isBieudo == "0") {
            $("#chartdiv2").addClass("Hienthi");
            $scope.staticParam.isBieudo = "1";
        } else {
            $("#chartdiv2").removeClass("Hienthi");
            $scope.staticParam.isBieudo = "0";
            $scope.reloadbySLDH();
        }
    }
    $scope.ClickTab();
});
app.controller('Bieudo', function ($scope, $rootScope, $compile, $uibModal, $confirm, $uibModalInstance, dataservice, para) {
    $scope.Title = "Biểu đồ bảng xếp hạng";
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };


});
// controller edit
app.controller('edit', function ($scope, $rootScope, $compile, $uibModal, $confirm, $uibModalInstance, dataservice, para) {
    $scope.Title = "Đánh giá kết quả công việc";
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
    $scope.ChangeApproval = function (data) {
        $scope.model.Approval = data;
    };
    $scope.initData = function () {
        dataservice.getItem(para, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {
                $scope.model = rs;
            }
        });
    };
    $scope.initData();
    $scope.submit = function () {
        if ($scope.editform.validate()) {
            var fd = new FormData();
            fd.append('edit', JSON.stringify($scope.model));
            $.ajax({
                type: "POST",
                url: "/RatingTasks/Update",
                contentType: false,
                processData: false,
                data: fd,
                success: function (rs) {
                    if (rs.Error) {
                        App.notifyDanger(rs.Title);
                    } else {
                        App.notifyInfo(rs.Title);
                        $uibModalInstance.close();
                        $rootScope.reloadData();
                    }
                }
            });
        }
    };
});


