﻿var ctxfolder = "/views/Sale/Tasks";
app.factory('Task_dataservice', function ($http) {
    var headers = {
        "Content-Type": "application/json;odata=verbose",
        "Accept": "application/json;odata=verbose",
    }
    return {
        insert: function (data, callback) {
            $http.post('/Cooperation/Tasks/insert', data).success(callback);
        },
        del: function (data, callback) {
            $http.post('/Cooperation/Tasks/delete', data).success(callback);
        },
        getAll: function (data, callback) {
            $http.post('/Cooperation/Tasks/GetAll', data).success(callback);
        },
        getItem: function (data, callback) {
            $http.post('/Cooperation/Tasks/GetItem', data).success(callback);
        },
        empList: function (callback) {
            $http.post('/Cooperation/Tasks/GetEmployee').success(callback);
        },
        comInsert: function (data, callback) {
            $http.post('/Cooperation/Tasks/ComInsert', data).success(callback);
        },
        comList: function (data, callback) {
            $http.post('/Cooperation/Tasks/ComList', data).success(callback);
        },
        deleteAttachComment: function (data, callback) {
            $http.post('/Cooperation/Tasks/DeleteAttachComment', data).success(callback);
        },
        deleteAttach: function (data, callback) {
            $http.post('/Cooperation/Tasks/DeleteAttach', data).success(callback);
        },
        updateProcess: function (data, callback) {
            $http.post('/Cooperation/Tasks/UpdateProcess', data).success(callback);
        },
        cateOfTaskList: function (callback) {
            $http.get('/Cooperation/Tasks/CateOfTaskList').success(callback);
        },
        getTastOfUsers: function (data, callback) {
            $http.post('/Cooperation/Tasks/GetTastOfUsers', data).success(callback);
        },
        checkPermission: function (data, callback) {
            $http.post('/Cooperation/Tasks/CheckPermission', data).success(callback);
        },
        insertToCalenderOfUser: function (data, callback) {
            $http.post('/Cooperation/Tasks/InsertToCalenderOfUser', data).success(callback);
        },
        getChildTask: function (data, callback) {
            $http.post('/Cooperation/Tasks/GetChildTask', data).success(callback);
        },
        getAllChildTask: function (data, callback) {
            $http.post('/Cooperation/Tasks/GetAllChildTask', data).success(callback);
        },
        updateProcessSuccess: function (data, callback) {
            $http.post('/Cooperation/Tasks/updateProcessSuccess', data).success(callback);
        },
        loadCheckList: function (data, callback) {
            $http.post('/Cooperation/Tasks/LoadCheckList', data).success(callback);
        },
        updateCheckList: function (data, callback) {
            $http.post('/Cooperation/Tasks/UpdateCheckList', data).success(callback);
        },
        insertCheckList: function (data, callback) {
            $http.post('/Cooperation/Tasks/insertCheckList', data).success(callback);
        },
        deleteCheckList: function (data, callback) {
            $http.post('/Cooperation/Tasks/deleteCheckList', data).success(callback);
        },
        updateFollow: function (data, callback) {
            $http.post('/Cooperation/Tasks/UpdateFollow', data).success(callback);
        },
        //updateViewTask: function (data, callback) {
        //    $http.post('/Cooperation/Tasks/UpdateViewTask', data).success(callback);
        //},
        updateViewed: function (data, callback) {
            $http.post('/Cooperation/Tasks/UpdateViewed', data).success(callback);
        },
        getAddressBook: function (data, callback) {
            $http.post('/Cooperation/Tasks/getAddressBook', data).success(callback);
        },
        getLiEmployee: function (data, callback) {
            $http.post('/Cooperation/Tasks/getLiEmployee', data).success(callback);
        },
        checkIsDisable: function (data, callback) {
            $http.post('/Cooperation/Tasks/CheckIsDisable', data).success(callback);
        },
        getEmployeeLogin: function (data, callback) {
            $http.post('/Cooperation/Tasks/GetEmployeeLogin', data).success(callback);
        },
        getActivitiesByTask: function (data, callback) {
            $http.post('/Cooperation/Tasks/getActivitiesByTask', data).success(callback);
        },
        getItemTaskOfUser: function (data, callback) {
            $http.post('/Cooperation/Tasks/getItemTaskOfUser', data).success(callback);
        },
        updateAccept: function (data, callback) {
            $http.post('/Cooperation/Tasks/updateAccept', data).success(callback);
        },
        recallTask: function (data, callback) {
            $http.post('/Cooperation/Tasks/recallTask', data).success(callback);
        },
        updateRecall: function (data, callback) {
            $http.post('/Cooperation/Tasks/UpdateRecall', data).success(callback);
        },
        //module calender
        getAllCalender: function (data, callback) {
            //$http.post('/Calendars/getAll', data).success(callback);
        },
        //versionUpdateRecall
        getListVer: function (data, callback) {
            $http.post('/Cooperation/Tasks/GetListVer', data).success(callback);
        },
        getViewVer: function (data, callback) {
            $http.post('/Cooperation/Tasks/GetViewVer', data).success(callback);
        },
        //Category Of Task
        gettreedata: function (data, callback) {
            $http.post('/Cooperation/Tasks/gettreedataCOT/' + data).success(callback);
        },
        insertCategoryOfTask: function (data, callback) {
            $http.post('/CategoryOfTasks/Insert', data).success(callback);
        },
        getDepart: function (data, callback) {
            $http.post('/Cooperation/Tasks/getDepartCOT/', data).success(callback);
        },
        getEmployee: function (data, callback) {
            $http.post('/Cooperation/Tasks/getEmployeeCOT', data).success(callback);
        },
        //Using for Profile
        insertProfile: function (data, callback) {
            $http.post('/Cooperation/Tasks/insertProfile', data).success(callback);
        },
        updateProfile: function (data, callback) {
            $http.post('/Cooperation/Tasks/updateProfile', data).success(callback);
        },
        deleteProfile: function (data, callback) {
            $http.post('/Cooperation/Tasks/deleteProfile', data).success(callback);
        },
        getItemProfile: function (data, callback) {
            $http.post('/Cooperation/Tasks/getItemProfile', data).success(callback);
        },
        loadProfile: function (data, callback) {
            //  $http.post('/Cooperation/Tasks/loadProfile', data).success(callback);
        },
        insertTaskOfProfile: function (data, callback) {
            $http.post('/Cooperation/Tasks/InsertTaskOfProfile', data).success(callback);
        },
        sendEmail: function (data, callback) {
            $http.post('/Cooperation/Tasks/SendEmail', data).success(callback);
        },
        getLabels: function (data, callback) {
            $http.post('/Cooperation/Tasks/GetLabels', data).success(callback);
        },
        insertLabelToTask: function (data, callback) {
            $http.post('/Cooperation/Tasks/InsertLabelToTask', data).success(callback);
        },
        getLabelForTasks: function (data, callback) {
            $http.post('/Cooperation/Tasks/GetLabelForTasks', data).success(callback);
        },
        updateEndDate: function (data, callback) {
            $http.post('/Cooperation/Tasks/updateEndDate', data).success(callback);
        },
        updateArchive: function (data, callback) {
            $http.post('/Cooperation/Tasks/updateArchive', data).success(callback);
        },
        saveTemplate: function (data, callback) {
            $http.post('/Cooperation/Tasks/saveTemplate', data).success(callback);
        },
        getTemplate: function (data, callback) {
            $http.post('/Cooperation/Tasks/GetTemplate', data).success(callback);
        },
        updateTaskCategory: function (data, callback) {
            $http.post('/Cooperation/Tasks/UpdateTaskCategory', data).success(callback);
        },
        getAttachments: function (data, callback) {
            $http.post('/Cooperation/Tasks/GetAttachments/', data).success(callback);
        },
        updateTaskProcess: function (data, callback) {
            $http.post('/Cooperation/Tasks/updateTaskProcess/', data).success(callback);
        },
        getEmployeeByDepartments: function (data, callback) {
            $http.post('/Cooperation/Tasks/getEmployeeByDepartments/', data).success(callback);
        },
        getAddress: function (data, callback) {
            $http.post('/Cooperation/Tasks/GetAddress/', data).success(callback);
        },
        getAddressEmployee: function (data, callback) {
            $http.post('/Cooperation/Tasks/getAddressEmployee/', data).success(callback);
        },
        updateTaskOfUser: function (data, callback) {
            $http.post('/Cooperation/Tasks/UpdateTaskOfUser/', data).success(callback);
        },
        getCategoryHollidays: function (data, callback) {
            $http.post('/Cooperation/Tasks/GetCategoryHollidays/', data).success(callback);
        },
        getAttachByType: function (data, callback) {
            $http.post('/Cooperation/Tasks/GetAttachByType/', data).success(callback);
        },
        updateStatusCompleted: function (data, callback) {
            $http.post('/Cooperation/Tasks/updateStatusCompleted/', data).success(callback);
        },
        attList: function (data, callback) {
            $http.post('/Cooperation/Tasks/attList/', data).success(callback);
        },
        checkHasMailConfig: function (data, callback) {
            $http.post('/Cooperation/Tasks/CheckHasMailConfig/', data).success(callback);
        },
        getTemplateData: function (data, callback) {
            $http.post('/Cooperation/Tasks/GetTemplateData/', data).success(callback);
        },
        //Task Contain
        getCategoryByProject: function (data, callback) {
            $http.post('/Cooperation/Tasks/GetCategoryByProject/', data).success(callback);
        },
        getTaskProcess: function (data, callback) {
            $http.post('/Cooperation/Tasks/GetTaskProcess/', data).success(callback);
        },
        getDashboardProject: function (data, callback) {
            $http.post('/Cooperation/Tasks/GetDashboardProject/', data).success(callback);
        },
        updateCategoryOfTask: function (data, callback) {
            $http.post('/Cooperation/Tasks/UpdateCategoryOfTask/', data).success(callback);
        },
        updateTaskDate: function (data, callback) {
            $http.post('/Cooperation/Tasks/UpdateTaskDate/', data).success(callback);
        },
        updateMoveTaskToCategory: function (data, callback) {
            $http.post('/Cooperation/Tasks/updateMoveTaskToCategory/', data).success(callback);
        },
        getNameProcess: function (data, callback) {
            $http.post('/Cooperation/Tasks/getNameProcess/', data).success(callback);
        },
        getRecurrence: function (data, callback) {
            $http.post('/Cooperation/Tasks/GetRecurrence/', data).success(callback);
        },
        getCalendarByDate: function (data, callback) {
            $http.post('/Cooperation/Tasks/GetCalendarByDate/', data).success(callback);
        },
        getCategoryStepByStep: function (data, callback) {
            $http.post('/Cooperation/Tasks/GetCategoryStepByStep/', data).success(callback);
        },
        //Begin 2021/03/04 check accept success task
        Get_ItemTaskOfUser: function (data, callback) {
            $http.post('/Cooperation/Tasks/Get_ItemTaskOfUser/', data).success(callback);
        },
        Update_ProcessAccept: function (data, callback) {
            $http.post('/Cooperation/Tasks/Update_ProcessAccept/', data).success(callback);
        },
        //End 2021/03/04
        updateUnArchive: function (data, callback) {
            $http.post('/Cooperation/Tasks/updateUnArchive/', data).success(callback);
        },
        Active_ProcessDelete: function (data, callback) {
            $http.post('/Cooperation/Tasks/Active_ProcessDelete/', data).success(callback);
        },
        GetEmployeeByCategoryOfTask: function (data, callback) {
            $http.post('/Cooperation/Tasks/GetEmployeeByCategoryOfTask/', data).success(callback);
        },
        GetDeprartmentByCategoryOfTask: function (data, callback) {
            $http.post('/Cooperation/Tasks/GetDeprartmentByCategoryOfTask/', data).success(callback);
        },
        GetStepByStepByCategory: function (data, callback) {
            $http.post('/Cooperation/Tasks/GetStepByStepByCategory/', data).success(callback);
        },
        MoveStepByStep: function (data, callback) {
            $http.post('/Cooperation/Tasks/MoveStepByStep/', data).success(callback);
        },
        UpdateStatusCompleted_TaskProcess: function (data, callback) {
            $http.post('/Cooperation/Tasks/UpdateStatusCompleted_TaskProcess/', data).success(callback);
        },
    }
});
app.directive('ngRightClick', function ($parse) {
    return function (scope, element, attrs) {
        var fn = $parse(attrs.ngRightClick);
        element.bind('contextmenu', function (event) {
            scope.$apply(function () {
                event.preventDefault();
                fn(scope, { $event: event });
            });
        });
    };
});
app.controller('Ctrl_Task', function ($scope, $rootScope, $compile, $uibModal, Task_dataservice) {
    $rootScope.Title = "";
    //var userLogin = 'ngannd'
    var userLogin =  document.getElementById('loginName').value;
    $rootScope.loginName = userLogin;

    $rootScope.LevelData = [
        {
            Value: 'A',
            Name: 'A',
        },
        {
            Value: 'B',
            Name: 'B',
        },
        {
            Value: 'C',
            Name: 'C',
        },
        {
            Value: 'D',
            Name: 'D',
        },
    ];



    $rootScope.initadd = function () {
        Task_dataservice.empList(function (rs) {
            if (rs.Error) {

            } else {
                $rootScope.liEmployee = [];
                $rootScope.liEmployee1 = [];
                $rootScope.liEmployee2 = [];
                $rootScope.liEmployee3 = [];
                angular.forEach(rs, function (value, key) {
                    value.FullName = value.text;
                    value.EmployeeGuid = value.value;
                    $rootScope.liEmployee.push(value);
                    $rootScope.liEmployee1.push(value);
                    $rootScope.liEmployee2.push(value);
                    $rootScope.liEmployee3.push(value);
                });

            }
        });

        Task_dataservice.getLabels({ IdS: [""] }, function (rs) {
            if (rs.Error) {

            } else {
                $rootScope.liLabels = [];
                $rootScope.liLabels = rs.Object;
            }
        });
        Task_dataservice.gettreedata(null, function (rs) {
            if (rs.Error) {
            } else {
                $rootScope.liCateOfTaskList = [];
                $rootScope.liCateOfTaskList.push({ value: null, text: 'Tất cả' });
                angular.forEach(rs, function (value, key) {
                    value.value = value.Id;
                    value.text = value.Title;
                    $rootScope.liCateOfTaskList.push(value);
                });
            }
        })
    }
    $rootScope.initadd();

    $rootScope.dStatusProcess = [
        {
            value: 'T',
            text: 'Tất cả',
        },
        {
            value: 'X',
            text: 'Chờ giao',
        },
        {
            value: 'N',
            text: 'Chờ xử lý',
        },
        {
            value: 'P',
            text: 'Đang thực hiện',
        },
        {
            value: 'C',
            text: 'Hoàn thành',
        },
        {
            value: 'W',
            text: 'Chờ người khác',
        },
        {
            value: 'D',
            text: 'Hoãn lại',
        },
    ];
    //0 11/11/2017
    //1 11/11/2017 22:22
    //2 123123123
    //3 type date
    //4 type data year month day
     //5 type int year month day
    $rootScope.ConDate = function (data, number) {
        if (data == null || data == "") {
            return '';
        }
        if (data !== null && data != "" && data != undefined) {
            try {
                if (data.indexOf("SA") != -1 || data.indexOf("CH") != -1) {
                    if (data.indexOf("SA") != -1) { }
                    if (data.indexOf("CH") != -1) { }
                }

                if (data.indexOf('Date') != -1) {
                    data = data.substring(data.indexOf("Date") + 5);
                    data = parseInt(data);
                }
            }
            catch (ex) { }
            var newdate = new Date(data);
            if (number == 3) {
                return newdate;
            }
            if (number == 2) {
                return "/Date(" + newdate.getTime() + ")/";
            }
            var month = newdate.getMonth() + 1;
            var day = newdate.getDate();
            var year = newdate.getFullYear();
            var hh = newdate.getHours();
            var mm = newdate.getMinutes();
            if (month < 10)
                month = "0" + month;
            if (day < 10)
                day = "0" + day;
            if (mm < 10)
                mm = "0" + mm;
            if (number == 0) {
                return todayDate = day + "/" + month + "/" + year;
            }
            else if (number == 1) {
                return todayDate = day + "/" + month + "/" + year + " " + hh + ":" + mm;
            }
            else if (number == 4) {
                return new Date(year, month - 1, day);
            }
            else if (number === 5) {
                return todayDate = year.toString() + month.toString() + day.toString();
            }
        } else {
            return '';
        }
    }
    $rootScope.Con = function (data) {
        if (data == null || data == "") {
            return '';
        }
        if (data == 'LOW') {
            return 'Thấp';
        }
        if (data == 'MEDIUM') {
            return 'Trung bình';
        }
        if (data == 'HIGH') {
            return 'Cao';
        }
    }
    $rootScope.Con1 = function (data) {
        if (data == null || data == "" || data == undefined) {
            return 'Chưa bắt đầu';
        }
        if (data == 'N') {
            return 'Chưa bắt đầu';
        }
        if (data == 'P') {
            return 'Đang xử lý';
        }
        if (data == 'D') {
            return 'Hoãn lại';
        }
        if (data == 'N') {
            return 'Chờ xử lý';
        }
        if (data == 'C') {
            return 'Hoàn thành';
        }
        return 'Chưa bắt đầu';
    }
    $rootScope.DataPriority = [
        {
            text: 'Tất cả',
            value: null,
        }, {
            text: 'Cao',
            value: 'H',
        },
        {
            text: 'Trung bình',
            value: 'N',
        },
        {
            text: 'Thấp',
            value: 'M',
        },
    ];
    $rootScope.filterNotAdded = function (item) {
        return item.show;
    }
    $rootScope.convertDatetime = function (datetime) {
        if (datetime != null && datetime != "" && datetime != undefined) {
            var newdate = new Date(datetime);
            var month = newdate.getMonth() + 1;
            var day = newdate.getDate();
            var year = newdate.getFullYear();
            var hh = newdate.getHours();
            var mm = newdate.getMinutes();
            if (month < 10)
                month = "0" + month;
            if (day < 10)
                day = "0" + day;
            return year + "-" + month + "-" + day;
        }
        return null;
    };
    $rootScope.SubTitle = function (data, num, last) {
        if (data == null || data == "") {
            return '';
        }
        if (data.length <= num) {
            return data;
        }
        var temp = data.substring(0, num - 1);
        return temp + last;
    }
    $rootScope.loadAssignedToNoCreated = function (d) {
        var _a = {};
        for (var i = 0; i < d.TaskOfUsers.length; i++) {
            switch (d.TaskOfUsers[i].IsOwner) {
                case 'A':
                    if (d.CreatedBy.split('#')[0].toUpperCase() === d.TaskOfUsers[i].LoginName.toUpperCase()) {
                        d.TaskOfUsers[i].IsOwnerName = 'Người tạo';
                    } else {
                        d.TaskOfUsers[i].IsOwnerName = 'Chủ trì';
                    }
                    break;
                case 'B':
                    d.TaskOfUsers[i].IsOwnerName = 'Phối hợp xử lý';
                    break;
                case 'C':
                    d.TaskOfUsers[i].IsOwnerName = 'Theo dõi';
                    break;
                default:
                    d.TaskOfUsers[i].IsOwnerName = '[Chưa có vị trí]';
                    break;
            }
        }
        for (var i = 0; i < d.TaskOfUsers.length; i++) {
            if (d.TaskOfUsers[i].IsOwner === 'A') {
                if (d.CreatedBy.split('#')[0].toUpperCase() !== d.TaskOfUsers[i].LoginName.toUpperCase()) {
                    _a = d.TaskOfUsers[i];
                } else {
                    if (d.CreatedBy.split('#')[0].toUpperCase() === d.TaskOfUsers[i].LoginName.toUpperCase() && !_a.LoginName) {
                        _a = d.TaskOfUsers[i];
                    }
                }
            }
        }
        return _a;
    }
    $rootScope.ComDateQuahan = function (d2) {
        if (d2 != null && d2 != undefined) {
            var ht = $rootScope.ConDate(new Date(), 5);
            var ngay = $rootScope.ConDate(d2, 5);
            if (parseInt(ht) - parseInt(ngay) > 0) {
                return "E";
            } else {
                return "";
            }    
        } else {
            return "";
        }   
           
    }
    $rootScope.ComDateProcess = function (d1, d2) {
        for (var i = 0; i < $rootScope.LoadDataProcess.length; i++) {
            if ($rootScope.LoadDataProcess[i].value === d1) {
                if (($rootScope.ConDate(new Date(), 3).getTime() - $rootScope.ConDate(d2, 3).getTime()) >= 0 && $rootScope.LoadDataProcess[i].value !== 'C') {
                    for (var j = 0; j < $rootScope.LoadDataProcess.length; j++) {
                        if ($rootScope.LoadDataProcess[j].value === 'E') {
                            return $rootScope.LoadDataProcess[j];
                        }
                    }
                }
                return $rootScope.LoadDataProcess[i];
            }
        }
        if (d1 === '0') {
            if (($rootScope.ConDate(new Date(), 3).getTime() - $rootScope.ConDate(d2, 3).getTime()) >= 0) {
                for (var j = 0; j < $rootScope.LoadDataProcess.length; j++) {
                    if ($rootScope.LoadDataProcess[j].value === 'E') {
                        return $rootScope.LoadDataProcess[j];
                    }
                }
            }
            for (var j = 0; j < $rootScope.LoadDataProcess.length; j++) {
                if ($rootScope.LoadDataProcess[j].value === 'N') {
                    return $rootScope.LoadDataProcess[j];
                }
            }
        }
        return "";
    }
    $rootScope.LoadDataProcess = RootLoadDataProcess;
    $rootScope.autoChangeStatusCompleted = function (d, m, r) {
        if (d == '' || d == 'N' || d == null || d == undefined) {
            m.PercentComplete = 0;
        }
        if (d == 'P') {
            m.PercentComplete = 50;
        }
        if (d == 'C') {
            m.PercentComplete = 100;
        }
        try {
            r.$apply();
        } catch (ex) { }
    }
    $rootScope.tinymceOptions = {
        selector: "textarea",
        plugins: [
            "advlist autolink lists link  charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern imagetools codesample toc"
        ],
        skin: "lightgray", language: "vi_VN",
        theme: "modern", height: 100,
        toolbar1: 'codesample | bold italic sizeselect fontselect fontsizeselect | hr alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | insertfile undo redo | emoticons | code',
        toolbar2: "print preview insert link | forecolor backcolor emoticons | codesample",
        image_advtab: true,
        fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
        file_browser_callback: function (field_name, url, type, win) {
            console.log(type + "|" + win);
            var connector = "/_FM/Index.aspx?f=L1B1Ymxpc2hpbmdJbWFnZXM=";
            var enableAutoTypeSelection = true;
            if (enableAutoTypeSelection && type) {
                connector += "&Type=" + type;
            }
            connector += "&Field=" + field_name;
            App.openPage(connector, "File Manager", 1150, 600);
        },
        relative_urls: false,
        remove_script_host: true,
        convert_urls: true
    };
    $rootScope.validationOptions = {
        rules: {
            Title: {
                required: true,
                maxlength: 500
            },
        },
        messages: {
            Title: {
                required: "Yêu cầu nhập tên công việc.",
                maxlength: "Tên công việc không dài quá 500 ký tự."
            },
        }
    }
    $rootScope.ColumnMultiConfig = {
        selectAll: true,
        search: true,
        searchText: 'Tìm kiếm...',
        showTitle: 'true',
        csvSepChar: ',',
        captionFormat: '{0} lựa chọn',
        captionFormatAllSelected: '{0} lựa chọn',
        noMatch: 'Không có dữ liệu "{0}"',
        okCancelInMulti: true,
        locale: ['Đồng ý', 'Hủy', 'Chọn tất cả']
    };
    $rootScope.ColumnConfig = {
        search: true
    };

    $rootScope.StatusProcess = function (d1) {
        for (var j = 0; j < RootLoadDataProcess.length; j++) {
            if (RootLoadDataProcess[j].value === d1) {
                return RootLoadDataProcess[j];
            }
        }
    }

});
var RootLoadDataProcess = [
    {
        value: 'X',
        name: 'Chờ giao',
        class: 'waiting',
        classbefore: 'before-notstart',
        classProcess: 'waiting',
        classDrop: 'iconfont-users dropdown-item__icon',
        classSearch: 'mdi mdi-account-outline',
        data: [],
        show: true,
        kanban: true,
    },
    {
        value: 'N',
        name: 'Chờ xử lý',
        class: 'notstart',
        classbefore: 'before-notstart',
        classProcess: 'notstart',
        classDrop: 'iconfont-users dropdown-item__icon',
        classSearch: 'mdi mdi-buffer',
        data: [],
        show: true,
        kanban: true,
    }, {
        value: 'P',
        name: 'Đang thực hiện',
        class: "process",
        classbefore: 'before-process',
        classProcess: 'process',
        classDrop: 'iconfont-user-circle dropdown-item__icon',
        classSearch: 'mdi mdi-check-all',
        data: [],
        show: true,
        kanban: true,
    }, {
        value: 'C',
        name: 'Hoàn thành',
        class: 'complete',
        classbefore: 'before-complete',
        classProcess: 'complete',
        classDrop: 'iconfont-building-b dropdown-item__icon',
        classSearch: 'mdi mdi-briefcase-check',
        data: [],
        show: true,
        kanban: true,
    },
    {
        value: 'S',
        name: 'Công việc lưu',
        class: 'saved',
        classbefore: 'before-complete',
        classProcess: 'complete',
        classDrop: 'iconfont-building-b dropdown-item__icon',
        classSearch: 'mdi mdi-content-save',
        data: [],
        show: false,
        kanban: false,
    }, {
        value: 'W',
        name: 'Chờ người khác',
        class: 'waitsomeone',
        classbefore: 'before-waitsomeone',
        classProcess: 'waitsomeone',
        classDrop: 'iconfont-building-b dropdown-item__icon',
        classSearch: 'mdi mdi-account-check',
        data: [],
        show: true,
        kanban: true,
    }, {
        value: 'D',
        name: 'Hoãn lại',
        class: 'deferred',
        classbefore: 'before-deferred',
        classProcess: 'deferred',
        classDrop: 'iconfont-deal dropdown-item__icon',
        classSearch: 'mdi mdi-flag',
        data: [],
        show: true,
        kanban: true,
    }, {
        value: 'E',
        name: 'Quá hạn',
        class: 'expiry',
        classbefore: 'before-expiry',
        classProcess: 'expiry',
        classDrop: 'iconfont-building-b dropdown-item__icon',
        classSearch: 'mdi mdi-bell-ring-outline',
        data: [],
        show: false,
        kanban: true,
    },
];
// End init status of task
function htmlToElement(html) {
    var template = document.createElement('template');
    html = html.trim(); // Never return a text node of whitespace as the result
    template.innerHTML = html;
    return template.content.firstChild;
}
app.config(function ($routeProvider, $validatorProvider) {
    $routeProvider
        .when('/', {
            templateUrl: ctxfolder + '/index.html',
            controller: 'index'
        })
        .when('/:id', {
            templateUrl: ctxfolder + '/index.html',
            controller: 'index'
        });
    $validatorProvider.setDefaults({
        errorElement: 'span',
        errorClass: 'help-block',
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        success: function (label) {
            label.closest('.form-group').removeClass('has-error');
        }
    });
});
app.controller('index', function ($scope, $timeout, $rootScope, $compile, $confirm, $uibModal, FileUploader, Task_dataservice, $routeParams, utility) {
    var ctrl = $scope;
    ctrl.utility = utility;
    ctrl.ActivitiesPage = 1000;
    ctrl.liAtt = [];
    ctrl.DataVersion = [];
    ctrl.filterUser = {
        Status1: true,
        Status2: true,
        Status3: true,
    };
    ctrl.initPara = {
        Page: 1,
        ItemPage: 15,
        Search: '',
        StartDate: null,
        EndDate: null,
        Priority: null,
        Process: null,
        ProfileGuid: null,
        CategoryOfTaskGuid: null,
        TaskGuid: $routeParams.RecordGuid,
        State: $routeParams.RecordGuid !== undefined ? 'SEARCH' : null,
    };
    var uploader = ctrl.uploader = new FileUploader({});
    uploader.filters.push({
        name: 'customFilter',
        fn: function (item /*{File|FileLikeObject}*/, options) {
            return this.queue.length < MAXFILELENGTH_UPLOAD;
        }
    });
    uploader.onSuccessItem = function (fileItem, response, status, headers) {
        ctrl.model.FileAttachments.push(response);
    };
    uploader.onCompleteAll = function () {
        ctrl.commit();
    };
    ctrl.pluginComment = {};
    ctrl.liComment = [];
    ctrl.iOpened = null;
    ctrl.StatusView = {
        IsComplate: true,
    };
    ctrl.marginLeft = 20;
    ctrl.isHasChildPadding = "task-child-padding";
    ctrl.iSelected = {};
    ctrl.isViewTask = 'itemisviewed';
    ctrl.isSelectedViewTask = 'itemactive';
    ctrl.isHasChildClose = "mdi mdi-chevron-double-down";
    ctrl.isHasChildOpen = "mdi mdi-chevron-double-right";
    ctrl.iSelected.liActivities = [];
    ctrl.activitiesPage = {};
    ctrl.dataAttbyType = {
        Files: [],
        Images: [],
        Links: [],
    };
    ctrl.showViewMoreListAtt = false;

    // Begin 2020/10/29
    $rootScope.initData = function (d1) {
        if (ctrl.initPara.Page === 1) {
            ctrl.liTasks = [];
        }
        App.blockUI({
            target: "#contentMain",
            boxed: true,
            message: 'Đang tải...'
        });
        try {
            Task_dataservice.getAll(ctrl.initPara, function (rs) {
                ctrl.check = true;
                App.unblockUI("#contentMain");
                if (rs.Error) {
                    App.notifyDanger(rs.Title);
                } else {
                    for (var i = 0; i < rs.Object.length; i++) {
                        rs.Object[i].Id = rs.Object[i].TaskGuid;
                        rs.Object[i].marginLeft = '';
                        rs.Object[i].iOpened = false;
                        rs.Object[i].isSelected = "";
                        rs.Object[i].isViewed = '';
                        if (rs.Object[i].IsRead) {
                            rs.Object[i].isViewed = ctrl.isViewTask;
                        }
                        if (ctrl.iSelected.TaskGuid === rs.Object[i].TaskGuid) {
                            rs.Object[i].isSelected = ctrl.isSelectedViewTask;
                        }
                    }
                    ctrl._processTasks(rs.Object);
                    ctrl.liTasks = ctrl.liTasks.concat(rs.Object);
                    if (ctrl.iSelected.TaskGuid !== undefined && ctrl.iSelected.TaskGuid !== null && ctrl.iSelected.TaskGuid.trim() !== '') {
                        for (var i = 0; i < ctrl.liTasks.length; i++) {
                            if (ctrl.liTasks[i].TaskGuid === ctrl.iSelected.TaskGuid) {
                                ctrl.getItem(ctrl.iSelected.TaskGuid);
                                break;
                            }
                        }
                    } else
                        if (ctrl.liTasks.length > 0) {
                            ctrl.getItem(ctrl.liTasks[0].TaskGuid);
                        }

                }
                if (ctrl.$root.$$phase !== '$apply' && ctrl.$root.$$phase !== '$digest') {
                    ctrl.$apply();
                }
            });
        }
        catch (ex) {
            App.unblockUI("#contentMain");
        }
    };
    $rootScope.initData(3);
    function treatAsUTC(date) {
        var result = new Date(date);
        result.setMinutes(result.getMinutes() - result.getTimezoneOffset());
        return result;
    }

    function daysBetween(str1, str2) {
        var start = moment(str1);
        var end = moment(str2);
        return end.diff(start, "days")
    }
    ctrl._processTasks = function (d) {
        for (var i = 0; i < d.length; i++) {
            if (d[i].CheckChild > 0) {
                d[i].ClassHasChild = ctrl.isHasChildOpen;
            } else {
                d[i].ClassHasChild = "";
            }
            d[i].NameUserFirst = "";
            if (d[i].EmpJoin.length > 0) {
                for (var ii = 0; ii < d[i].EmpJoin.length; ii++) {
                    if (d[i].EmpJoin[ii].IsOwner == "A" || d[i].EmpJoin[ii].IsOwner == "B") {
                        d[i].EmpJoin[ii].PercentCompleteText = d[i].EmpJoin[ii].PercentComplete > 0 ? " - " + d[i].EmpJoin[ii].PercentComplete.toString() + "%" : " - " + "0" + "%";
                    } else {
                        d[i].EmpJoin[ii].PercentCompleteText = "";
                    }
                    
                    if (d[i].EmpJoin[ii].IsOwner === 1) {
                        d[i].NameUserFirst = d[i].EmpJoin[ii].FullName;
                        break;
                    }
                }
            }
            var trangthai = $rootScope.ComDateQuahan(d[i].TimeReminderTaskOfUser);
            if (trangthai != "" && d[i].StatusCompletedTaskOfUser !="C") {
                d[i].StatusCompletedTaskOfUser = trangthai;
            }
            
            d[i].ActualdayWorking = daysBetween(d[i].StartDate, d[i].EndDate);
            d[i]._TaskContent = d[i].TaskContent;
            d[i].TaskContent = ctrl.SubTitle($(('<p>' + d[i].TaskContent + '</p>')).text(), 140, '...');
            d[i]._Subject = d[i].Subject;
            d[i].Subject = ctrl.SubTitle(d[i].Subject, 50, '...');
        }
    }




    // End 2020/10/29
    ctrl.LoginPermission = {
        IsRead: true,
        IsCheck: true,
        Permission: "FullControl",
        IsView: true,
        IsUpdate: true,
        IsDelete: true,
        IsComment: true,
        IsApproval: true,
        IsDisable: true,
    };
    ctrl.TaskOfUserId = null;
    ctrl.customFilter = function (data) {
        return data.IsOwner == 3 || data.IsOwner == 2;
    }
    ctrl.customFilter1 = function (data) {
        return data.IsOwner == 1;
    }
    Recu = function (data) {
        var datatemp = [];
        var datatempParent = [];
        for (var i = 0; i < data.length; i++) {
            if (data[i].ParentId == null || data[i].ParentId == '') {
                datatemp.push(data[i]);
                datatempParent.push(data[i]);
            }
        }

    }
    ctrl.liUserProcess = {
        li1: [],
        li2: [],
        li3: [],
    };
    ctrl.liAction = {
        comment: true,
    };
    ctrl.activeComent = function () {
        if (ctrl.liAction.comment) {
            ctrl.liAction.comment = false;
        } else {
            ctrl.liAction.comment = true;
        }
    }


    ctrl.dataSearch = {
        searchPriority: "",
        searchProcess: -1,
    };


    ctrl.changesearchProcess = function (data) {
        ctrl.initPara.Page = 1;
        $scope.initPara.Process = data !== "null" ? data : "";
        $rootScope.initData(2);
    }

    ctrl.changesearchPriority = function (data) {
        ctrl.initPara.Page = 1;
        $scope.initPara.Priority = data !== "null" ? data : "";
        $rootScope.initData(2);
    }
    ctrl.ComEndDate = function (d1) {
        if ($rootScope.ConDate(d1, 3) <= (new Date())) {
            return true;
        }
        else {
            return false;
        }
    }
    ctrl.CheckIconFa = function (d1, d2) {
        if (d1 === undefined || d1 === null) {
            return "";
        }
        if ($rootScope.ConDate(d1, 3) < (new Date())) {
            if (d2 === 1) {
                return 'fa-spin';
            } else {
                return '';
            }
        }
        if (d2 === 1) {
            if ($rootScope.ConDate(d1, 3) < (new Date())) {
                return '';
            } else {
                return 'fa-spin';
            }
        }
    }
    ctrl.ComRest = function (d1, d2) {
        if (d1 === undefined || d1 === null) {
            return "";
        }
        if ($rootScope.ConDate(d1, 3) < (new Date())) {
            if (d2 >= 100) {
                return 'Hoàn thành';
            } else {
                return '<span class="itemred">Quá hạn: <b>' + (Math.round(($rootScope.ConDate(d1, 4) - $rootScope.ConDate(new Date(), 4)) / 86400000)) + '</b> ngày </span>';
            }
        }
        if ($rootScope.ConDate(d1, 3) >= (new Date())) {
            if (d2 < 100 && d2 > 0) {
                return 'Còn lại: <b>' + (Math.round(($rootScope.ConDate(d1, 4) - $rootScope.ConDate(new Date(), 4)) / 86400000)) + '</b> ngày';
            } else {
                if (d2 === undefined || d2 === null || d2 === 0) {
                    return 'Chưa bắt đầu';
                } else {
                    return 'Hoàn thành';
                }
            }
        }
    }
    ctrl.ClearChild = function (data) {
        var temp = false;
        for (var i = 0; i < ctrl.liTasks.length; i++) {
            for (var j = 0; j < data.length; j++) {
                if (ctrl.liTasks[i].ParentId == data[j]) {
                    data.push(ctrl.liTasks[i].TaskGuid);
                    ctrl.liTasks.splice(i, 1); i--;
                    temp = true;
                }
            }
        }
        if (temp) {
            ctrl.ClearChild(data);
        } else {
            for (var i = 0; i < ctrl.liTasks.length; i++) {
                if (ctrl.liTasks[i].TaskGuid == data.TaskGuid) {
                    ctrl.liTasks.splice(i, 1);
                    i--;
                }
            }
        }
    }
    ctrl.loadChild = function (data) {
        if (data.iOpened) {
            var temp = [];
            for (var i = 0; i < ctrl.liTasks.length; i++) {
                if (ctrl.liTasks[i].ParentId === data.TaskGuid) {
                    temp.push(ctrl.liTasks[i].TaskGuid);
                    ctrl.liTasks.splice(i, 1);
                    i--;
                }
            }
            ctrl.ClearChild(temp);
            data.iOpened = false;
            data.ClassHasChild = ctrl.isHasChildOpen;
        } else {
            data.ClassHasChild = ctrl.isHasChildClose;
            ctrl.iOpened = data;
            Task_dataservice.getChildTask({ IdS: [data.TaskGuid] }, function (rs) {
                if (rs.Error) {
                    App.notifyDanger(rs.Title);
                } else {
                    for (var i = 0; i < rs.Object.length; i++) {
                        rs.Object[i].Id = rs.Object[i].TaskGuid;
                        rs.Object[i].TaskGuid = rs.Object[i].TaskGuid;
                        rs.Object[i].marginLeft = '';
                        rs.Object[i].iOpened = false;
                        rs.Object[i].isSelected = "";
                        rs.Object[i].isViewed = '';
                        if (rs.Object[i].IsRead) {
                            rs.Object[i].isViewed = ctrl.isViewTask;
                        }
                        //if (rs.Object[i].LiViewed !== null && rs.Object[i].LiViewed !== "") {
                        //    var _liViewed = rs.Object[i].LiViewed.split('#');
                        //    if (_liViewed.length > 0) {
                        //        for (var ii = 0; ii < _liViewed.length; ii++) {
                        //            if (_liViewed[ii].toUpperCase() === userLogin) {
                        //                rs.Object[i].isViewed = ctrl.isViewTask;
                        //                break;
                        //            }
                        //        }
                        //    }
                        //}
                        if (ctrl.iSelected.TaskGuid === rs.Object[i].TaskGuid) {
                            rs.Object[i].isSelected = ctrl.isSelectedViewTask;
                        }
                    }
                    for (var i = 0; i < rs.Object.length; i++) {
                        //rs.Object[i].marginLeft = ctrl.iOpened.marginLeft + ctrl.marginLeft;
                        rs.Object[i].marginLeft = (data.marginLeft !== '' ? (data.marginLeft + '-' + ctrl.isHasChildPadding) : ctrl.isHasChildPadding);
                    }
                    ctrl._processTasks(rs.Object);
                    var tempStart = [];
                    var tempEnd = [];
                    for (var i = 0; i < ctrl.liTasks.length; i++) {
                        if (ctrl.iOpened.TaskGuid == ctrl.liTasks[i].TaskGuid) {
                            ctrl.liTasks[i].iOpened = true;
                            tempStart = ctrl.liTasks.slice(0, i + 1);
                            tempEnd = ctrl.liTasks.slice(i + 1, ctrl.liTasks.length);
                            break;
                        }
                    }
                    for (var i = 0; i < rs.Object.length; i++) {
                        tempStart.push(rs.Object[i]);
                    }
                    ctrl.liTasks = [];
                    ctrl.liTasks = tempStart.concat(tempEnd);
                    ctrl.reloadAddLink();
                }
            })
        }
    }

    ctrl.insertCalendarFromList = function (d) {
        var _tempTaskGuid = d.TaskGuid;
        Task_dataservice.checkPermission({ IdS: [d.TaskGuid] }, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {
                if (rs.Object !== null) {
                    var TaskOfUserId = rs.Object.Id;
                    if (TaskOfUserId !== null) {
                        Task_dataservice.insertToCalenderOfUser({ IdI: [TaskOfUserId, _tempTaskGuid] }, function (rs) {
                            if (rs.Error) {
                                App.notifyDanger(rs.Title);
                            } else {
                                App.notifyInfo(rs.Title);
                            }
                        });
                    }
                }
            }
        });
    }
    ctrl.ConStatusCompleted = function (d) {
        if (d === 0 || d === null || d === undefined) {
            return "fa fa-play-circle-o ";
        }
        if (d === 1) {
            return " fa fa-spinner fa-spin";
        }
        if (d === 2) {
            return "fa fa-stop-circle-o ";
        }
        if (d === 3) {
            return "fa fa-pause-circle-o";
        }
        if (d === 4) {
            return "fa  fa-check-circle-o";
        }
    }
    ctrl.reloadGetItem = function () {
        ctrl.ClearAdd();
        ctrl.iSelected = {};
        ctrl.liAtt = [];
        ctrl.comment.liComment = [];
    }
    ctrl.getItem = function (data) {
        ctrl.reloadGetItem();
        App.blockUI({
            target: "#contentMain",
            boxed: true,
            message: 'Đang tải...'
        });
        Task_dataservice.getItem({ IdS: [data] }, function (rs) {
            App.unblockUI("#contentMain");
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {
                if (rs.Object === null) {
                    App.notifyDanger('Công việc không tồn tại.');
                    return;
                }
                if (rs.Object.StatusCompleted === null && rs.Object !== null) {
                    rs.Object.StatusCompleted = 'None';
                }
                if (rs.Object.PercentComplete === 0) {
                    rs.Object.PercentCompleteClass = 'label-danger';
                } else
                    if (rs.Object.PercentComplete === 100) {
                        rs.Object.PercentCompleteClass = 'label-success';
                    } else { rs.Object.PercentCompleteClass = 'label-warning'; }
                ctrl.iSelected = rs.Object;
                ctrl.iSelected.EmployeeAssignedTo = $rootScope.loadAssignedToNoCreated(ctrl.iSelected);
                ctrl.getAttachments();
                ctrl.checkRecall();
                if ((ctrl.iSelected.IsAcceptTask === '' || ctrl.iSelected.IsAcceptTask === 'W' || ctrl.iSelected.IsAcceptTask === 'D') && ctrl.iSelected.IsCreated !== true) {
                    ctrl.iSelected.ShowAcceptTask = true;
                }
                for (var i = 0; i < ctrl.liTasks.length; i++) {
                    ctrl.liTasks[i].isSelected = '';
                    if (ctrl.liTasks[i].TaskGuid === ctrl.iSelected.TaskGuid) {
                        ctrl.liTasks[i].isViewed = ctrl.isViewTask;
                        ctrl.liTasks[i].isSelected = ctrl.isSelectedViewTask;
                    }
                }
                for (var ii = 0; ii < ctrl.iSelected.TaskOfUsers.length; ii++) {
                    if (ctrl.iSelected.TaskOfUsers[ii].Owner === 1) {
                        ctrl.iSelected.NameUserFirst = ctrl.iSelected.TaskOfUsers[ii].FullName;
                        break;
                    }
                }
                ctrl.liUserProcess = {
                    li1: [],
                    li2: [],
                    li3: [],
                };
                for (var i = 0; i < rs.Object.TaskOfUsers.length; i++) {
                    if (rs.Object.TaskOfUsers[i].Owner == 1) {
                        ctrl.liUserProcess.li1.push(rs.Object.TaskOfUsers[i]);
                    } else
                        if (rs.Object.TaskOfUsers[i].Owner == 2) {
                            ctrl.liUserProcess.li2.push(rs.Object.TaskOfUsers[i]);
                        } else
                            if (rs.Object.TaskOfUsers[i].Owner == 3) {
                                ctrl.liUserProcess.li3.push(rs.Object.TaskOfUsers[i]);
                            }
                }
                //Task_dataservice.checkPermission({ IdS: [ctrl.iSelected.TaskGuid] }, function (rs) {
                //    if (rs.Error) {
                //        App.notifyDanger(rs.Title);
                //    } else {
                //        if (rs.Object !== null) {
                //            ctrl.TaskOfUserId = rs.Object.TaskGuid;
                //            ctrl.LoginPermission = rs.Object.Permission;
                //        } else {
                //            ctrl.LoginPermission = {
                //                IsRead: false,
                //                IsCheck: false,
                //                Permission: "",
                //                IsView: false,
                //                IsUpdate: false,
                //                IsDelete: false,
                //                IsComment: false,
                //                IsApproval: false,
                //                IsDisable: false,
                //                IsRecall: false,
                //            };
                //            ctrl.TaskOfUserId = null;
                //        }
                //        //if (ctrl.iSelected.EmployeeGuid === $rootScope.loginName.toUpperCase()) {
                //        //    ctrl.LoginPermission.CreatedThis = false;
                //        //} else {
                //        //    ctrl.LoginPermission.CreatedThis = false;
                //        //}
                //    }
                //});
                //for (var i = 0; i < ctrl.liTasks.length; i++) {
                //    ctrl.liTasks[i].isSelected = "";
                //    if (ctrl.liTasks[i].TaskGuid == ctrl.iSelected.TaskGuid) {
                //        ctrl.liTasks[i].isSelected = (ctrl.isSelectedViewTask);
                //    }
                //}
                ctrl.getActivitiesByTask(data, 0, ctrl.comment.itemInPage, []);
                if (ctrl.iSelected.StatusCompleted === 'C') {
                    ctrl.StatusView.IsComplate = true;
                } else {
                    ctrl.StatusView.IsComplate = false;
                }
                ctrl.comment.init();
                ctrl.loadAttbyType();
            }
        });
        
    }
    //Begin using for get attach by type 2020/07/21
    ctrl.loadAttbyType = function () {
        Task_dataservice.getAttachByType({ IdS: [ctrl.iSelected.TaskGuid] }, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {
                ctrl.dataAttbyType['Files'] = [];
                ctrl.dataAttbyType['Images'] = [];
                ctrl.dataAttbyType['Links'] = [];
                for (var i = 0; i < rs.Object.length; i++) {
                    if (rs.Object[i].Value === null) continue;
                    switch (rs.Object[i].TypeExtension) {
                        case 'F':
                            ctrl.dataAttbyType['Files'] = JSON.parse(rs.Object[i].Value);
                            for (var j = 0; j < ctrl.dataAttbyType.Files.length; j++) {
                                ctrl.dataAttbyType.Files[j].link = "/Cooperation/Tasks/ComDownload/" + ctrl.dataAttbyType.Files[j].AttachmentGuid;
                            }
                            break;
                        case 'I':
                            ctrl.dataAttbyType['Images'] = JSON.parse(rs.Object[i].Value);
                            for (var j = 0; j < ctrl.dataAttbyType.Images.length; j++) {
                                ctrl.dataAttbyType.Images[j].link = "/Cooperation/Tasks/ComDownload/" + ctrl.dataAttbyType.Images[j].AttachmentGuid + '/img';
                            }
                            break;
                        case 'L':
                            ctrl.dataAttbyType['Links'] = JSON.parse(rs.Object[i].Value);
                            break;
                        default:
                            break;
                    }
                }
            }
        })
    }
    //Begin using for get attach  2020/07/21
    ctrl.getAttachments = function () {
        Task_dataservice.getAttachments({ IdS: [ctrl.iSelected.TaskGuid] }, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {
                for (var i = 0; i < rs.Object.length; i++) {
                    rs.Object[i].link = "/Cooperation/Tasks/Download?id=" + rs.Object[i].AttachmentGuid;
                }
                $scope.liAtt = rs.Object;
            }

        });
    }
    ctrl.ComTimeReminder = function (d1, d2) {
        if (d2 < (new Date())) {
            return true;
        }
        if (d1 != null && d1 != undefined) {
            return false;
        }
        return true;
    }
    ctrl.DueDate = function (data) {
        if (data == null || data == "") {
            return '';
        }
        var temp1 = new Date(data);
        var temp2 = new Date();
        if (temp1 < temp2) {
            return 'label-danger';
        } else {
            return 'label-warning';
        }
    }
    ctrl.changeSearch = function (data) {
        ctrl.reloadGetItem();
        ctrl.initPara.Page = 1;
        $scope.initPara.CategoryOfTaskGuid = data !== "null" ? data : "";
        //ctrl.initPara.ProfileGuid = ctrl.treeInstance.jstree(true).get_selected().length > 0 ? ctrl.treeInstance.jstree(true).get_selected()[0] : null;
        $rootScope.initData(4);
    }
    ctrl.reloadSearch = function () {
        if (ctrl.initPara.Search === '') {
            ctrl.changeSearch();
        }
    }
    ctrl.ViewComment = function () {
        if (ctrl.iSelected == undefined || ctrl.iSelected.TaskGuid == null || ctrl.iSelected.TaskGuid == '') {
            App.notifyDanger("Chọn công việc trước khi xem nội dụng trao đổi.");
            return;
        }
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: '/Views/Base/Comment.html',
            controller: 'Task_Comment',
            backdrop: 'static',
            size: '350',
            resolve: {
                para: function () {
                    if (ctrl.iSelected != undefined) {
                        return ctrl.iSelected;
                    } else
                        return null;
                }
            }
        });
        modalInstance.result.then(function (d) {
            ctrl.changeSearch();
            $rootScope.initData(4);
        }, function () {

        });
    }
    ctrl.ViewCommentFromList = function (d) {
        if (d == undefined || d.TaskGuid == null || d.TaskGuid == '') {
            App.notifyDanger("Chọn công việc trước khi xem nội dụng trao đổi.");
            return;
        }
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: '/Views/Base/Comment.html',
            controller: 'Task_Comment',
            backdrop: 'static',
            size: '350',
            resolve: {
                para: function () {
                    if (d != undefined) {
                        return d;
                    } else
                        return null;
                }
            }
        });
        modalInstance.result.then(function (d) {
            ctrl.changeSearch();
            $rootScope.initData(4);
        }, function () {
            //$rootScope.initData();
        });
    }
    ctrl.edit = function () {
        if (ctrl.checkHasSelected()) return;
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/add.html',
            controller: 'Task_edit',
            backdrop: 'static',
            size: 80,
            resolve: {
                para: function () {
                    return ctrl.iSelected.TaskGuid;
                }
            }
        });
        modalInstance.result.then(function (d) {
            ctrl.getItem(ctrl.iSelected.TaskGuid);
            ctrl.changeSearch();
        }, function () {
        });
    }
    //Add new Task
    ctrl.addTask = function () {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/add.html',
            controller: 'Task_add',
            backdrop: 'static',
            size: 80,
            resolve: {
                para: function () {
                    return ctrl.initPara;
                }
            }
        });
        modalInstance.result.then(function (d) {
            ctrl.changeSearch();
        }, function () {
            ctrl.changeSearch();
        });
    }
    ctrl.del = function () {
        if (ctrl.iSelected === undefined || ctrl.iSelected.TaskGuid === null || ctrl.iSelected.TaskGuid == '') {
            App.notifyDanger("Chọn công việc trước khi xóa");
            return;
        }
        ESConfirm([{ 'value': 'Y', 'title': 'Xác nhận', 'type': 'info' },
        { 'value': 'N', 'title': 'Hủy', 'type': 'danger' }],
            { 'description': 'Bạn có chắc chắn xóa công việc [' + ctrl.iSelected.Subject + '] ?', 'class': 'eswarning_v2' },
            function (rs) {
                if (rs === 'Y') {
                    App.blockUI({
                        target: "#contentMain",
                        boxed: true,
                        message: 'Đang tải...'
                    });
                    Task_dataservice.del({ IdS: [ctrl.iSelected.TaskGuid] }, function (rs) {
                        App.unblockUI("#contentMain");
                        if (rs.Error) {
                            App.notifyDanger(rs.Title);
                        } else {
                            App.notifyInfo(rs.Title);
                            ctrl.changeSearch();
                        }
                    });
                }
            }, function () {

            });
    }
    ctrl.assign = function () {
        if (ctrl.iSelected == undefined || ctrl.iSelected.TaskGuid == null || ctrl.iSelected.TaskGuid == '') {
            App.notifyDanger("Chọn công việc trước khi giao việc");
            return;
        }
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/assign.html',
            controller: 'Task_assign',
            backdrop: 'static',
            size: '100',
            resolve: {
                para: function () {
                    if (ctrl.iSelected != undefined) {
                        return ctrl.iSelected;
                    } else
                        return null;
                }
            }
        });
        modalInstance.result.then(function (d) {
            ctrl.changeSearch();
            $rootScope.initData(4);
        }, function () {
        });
    }

    ctrl.LoadScroll = function (obj) {
        if (obj.offsetHeight + obj.scrollTop >= obj.scrollHeight && ctrl.check) {
            ctrl.check = false;
            try {
                ctrl.IconLoad = true;
                ctrl.initPara.Page = ctrl.initPara.Page + 1;
                $rootScope.initData();
            }
            catch (ex) {

            }
        }
    }
    ctrl.Recu = function (data) {
        var datatemp = [];
        var datatempParent = [];
        for (var i = data.length - 1; i >= 0; i--) {
            if (data[i].ParentId == null || data[i].ParentId == '') {
                datatemp.push(data[i]);
            }
        }
        for (var i = data.length - 1; i >= 0; i--) {
            if (data[i].ParentId != null && data[i].ParentId != '') {
                datatemp.push(data[i]);
            }
        }
        return datatemp;
    }
    ctrl.addUnNewCheck = 0;
    ctrl.checkIsDisable = function () {
        Task_dataservice.checkIsDisable({ IdI: [ctrl.iSelected.TaskGuid] }, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {
                App.notifyInfo("Hủy công việc thành công.");
                ctrl.ClearAdd();
                $rootScope.initData(5);
            }
        });
    }
    ctrl.ConColor = function (d1, d2) {
        if ($rootScope.ConDate(d1, 3) < (new Date())) {
            if (d2 >= 100) {
                return "";
            } else {
                return "itemred";
            }
        }
        if ($rootScope.ConDate(d1, 3) >= (new Date())) {
            if (d2 < 100 && d2 > 0) {
                return 'itemblue';
            } else {
                return '';
            }
        }
    }
    ctrl.updateProcessSuccess = function () {
        if (ctrl.iSelected == undefined || ctrl.iSelected.TaskGuid == null || ctrl.iSelected.TaskGuid == '') {
            App.notifyDanger("Bạn chưa chọn công việc để cập nhật hoàn hành");
            return;
        }
        ESConfirm([{ 'value': 'Y', 'title': 'Xác nhận', 'type': 'info' },
        { 'value': 'N', 'title': 'Đóng', 'type': 'danger' }],
            { 'description': 'Bạn có chắc chắn hoàn thành công việc này không ?', 'class': 'eswarning_v2' },
            function (rs) {
                if (rs === 'Y') {
                    Task_dataservice.updateProcessSuccess({ IdS: [ctrl.iSelected.TaskGuid] }, function (rs) {
                        if (rs.Error) {
                            App.notifyDanger(rs.Title);
                        } else {
                            App.notifySuccess(rs.Title);
                            ctrl.getItem(ctrl.iSelected.TaskGuid);
                            ctrl.changeSearch();
                        }
                    });
                }
            }, function () {

            });
       
    }
    ctrl.getActivitiesByTask = function (RecordGuid, Skip, take, User) {
        Task_dataservice.getActivitiesByTask({ RecordGuid: RecordGuid, Skip: Skip, Take: take, GroupUser: User }, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {
                ctrl.iSelected.liActivities = rs.Object;
            }
        })
    }
    ctrl.changeAllPageActivities = function () {
        if (ctrl.iSelected === undefined || ctrl.iSelected.TaskGuid === null || ctrl.iSelected.TaskGuid === '') {
            App.notifyDanger("Bạn chưa chọn công việc để cập nhật hoàn hành");
            return;
        }
        ctrl.getActivitiesByTask(ctrl.iSelected.TaskGuid, 0, ctrl.ActivitiesPage, []);
    }
    ctrl.filterUser = {
        Status1: false,
        Status2: false,
        Status3: false,
    }
    ctrl.onchangeFilterUser = function (d1) {
        if (ctrl.iSelected.TaskGuid !== undefined && ctrl.iSelected.TaskGuid !== null && ctrl.iSelected.TaskGuid.trim() !== '') {
            if (d1 === 'A') {
                ctrl.filterUser.Status1 = !ctrl.filterUser.Status1;
            }
            if (d1 === 'B') {
                ctrl.filterUser.Status2 = !ctrl.filterUser.Status2;
            }
            if (d1 === 'C') {
                ctrl.filterUser.Status3 = !ctrl.filterUser.Status3;
            }
            for (var i = 0; i < ctrl.iSelected.TaskOfUsers.length; i++) {
                if (d1 === 'S') {
                    ctrl.filterUser.Status1 = false;
                    ctrl.filterUser.Status2 = false;
                    ctrl.filterUser.Status3 = false;
                    ctrl.iSelected.TaskOfUsers[i].filterStatus = false;
                } else {
                    if (ctrl.iSelected.TaskOfUsers[i].IsOwner === 'A') {
                        ctrl.iSelected.TaskOfUsers[i].filterStatus = !ctrl.filterUser.Status1;
                    }
                    if (ctrl.iSelected.TaskOfUsers[i].IsOwner === 'B') {
                        ctrl.iSelected.TaskOfUsers[i].filterStatus = !ctrl.filterUser.Status2;
                    }
                    if (ctrl.iSelected.TaskOfUsers[i].IsOwner === 'C') {
                        ctrl.iSelected.TaskOfUsers[i].filterStatus = !ctrl.filterUser.Status3;
                    }
                }
            }
            var _item = [];
            if (ctrl.filterUser.Status1) {
                _item.push('A');
            }
            if (ctrl.filterUser.Status2) {
                _item.push('B');
            }
            if (ctrl.filterUser.Status3) {
                _item.push('C');
            }
            if (d1 === 'S') {
                _item = [];
            }
            ctrl.iSelectedComment.GroupUser = _item;
            ctrl.comment.loadComment(ctrl.iSelected.TaskGuid, 1);
            ctrl.getActivitiesByTask(ctrl.iSelected.TaskGuid, 0, ctrl.comment.itemInPage, _item);
        }
    }
    ctrl.recallTask = function () {
        if (ctrl.iSelected === undefined || ctrl.iSelected.TaskGuid === null || ctrl.iSelected.TaskGuid === '') {
            App.notifyDanger("Bạn chưa chọn công việc để thu hồi");
            return;
        }
        ESConfirm([{ 'value': 'Y', 'title': 'Bắt đầu thu hồi', 'type': 'info' },
        { 'value': 'N', 'title': 'Bỏ qua', 'type': 'danger' }],
            { 'description': 'Yêu cầu thu hồi công việc [' + ctrl.iSelected.Subject + ']', 'class': 'eswarning_v2' },
            function (rs) {
                if (rs === 'Y') {
                    Task_dataservice.recallTask({ IdS: [ctrl.iSelected.TaskGuid] }, function (rs) {
                        if (rs.Error) {
                            App.notifyDanger(rs.Title);
                        } else {
                            $rootScope.initData(4);
                            ESConfirm([{ 'value': 1, 'title': 'Xác nhận', 'type': 'danger' }],
                                { 'description': rs.Title, 'class': 'escheck' }, function (rs) {

                                });
                        }
                    })
                }
            });
    }
    ctrl.checkRecall = function () {
        if (ctrl.iSelected.IsRecall === true && ctrl.iSelected.IsRecallAccept !== 'N' && ctrl.iSelected.IsRecallAccept !== 'Y' && ctrl.iSelected.IsCreated !== true) {
            ESConfirm([{ 'value': 'Y', 'title': 'Chấp nhận thu hồi', 'type': 'info' },
            { 'value': 'N', 'title': 'Không chấp nhận', 'type': 'danger' }],
                { 'description': 'Yêu cầu thu hồi công việc [' + ctrl.iSelected.Subject + ']', 'class': 'eswarning_v2' },
                function (rs) {
                    App.blockUI({
                        target: "#contentMain",
                        boxed: true,
                        message: 'Đang tải...'
                    });
                    Task_dataservice.updateRecall({ IdS: [ctrl.iSelected.TaskGuid, rs] }, function (rs) {
                        App.unblockUI("#contentMain");
                        if (rs.Error) {
                            App.notifyDanger(rs.Title);
                        } else {
                            App.notifySuccess(rs.Title);
                            ctrl.reloadSearch();
                        }
                    })
                }, function () {

                });
        }
    }
    //button top
    ctrl.ViewTask = function () {
        ctrl.addUnNewCheck = 0;
    };
    ctrl.ViewCalender = function () {
        ctrl.addUnNewCheck = 2;
        setTimeout(myFunction, 3);
    }
    ctrl.AcceptTask = function () {
        if ((ctrl.iSelected.IsAcceptTask === '' || ctrl.iSelected.IsAcceptTask === 'W' || ctrl.iSelected.IsAcceptTask === 'D') && ctrl.iSelected.IsCreated !== true) {
            ESConfirm([{ 'value': 'Y', 'title': 'Nhận việc', 'type': 'info' }, { 'value': 'N', 'title': 'Không nhận', 'type': 'info' }, { 'value': 'D', 'title': 'Để sau', 'type': 'danger' }], { 'description': 'Xác nhận công việc được giao  [' + ctrl.iSelected.Subject + ']', 'class': 'escheck' }, function (rs) {
                Task_dataservice.updateAccept({ IdS: [ctrl.iSelected.TaskGuid, rs] }, function (rs) {
                    if (rs.Error) {
                        App.notifyDanger(rs.Title);
                    } else {
                        ctrl.changeSearch();
                        $scope.getItem(ctrl.iSelected.TaskGuid);
                        App.notifySuccess(rs.Title);
                    }
                })
            }, function (rs) {
                if (rs) {
                    Task_dataservice.updateAccept({ IdS: [ctrl.iSelected.TaskGuid, rs] }, function (rs) {
                        if (rs.Error) {
                            App.notifyDanger(rs.Title);
                        } else {
                            $scope.getItem(ctrl.iSelected.TaskGuid);
                            return;
                        }
                    })
                }
            });
        }
    }
    function myFunction() {
        var x = document.getElementsByClassName("fc-today-button fc-button fc-state-default fc-corner-left fc-corner-right");
        if (x != null) {
            x[0].click();
        }
    }
    //begin calender plugin 
    ctrl.ListCalendar = [];
    function renderCalendar() {
    }
    ctrl.add = function () {
        ctrl.iSelected = {};
        ctrl.addUnNewCheck = 1;
        ctrl.model = {};
        ctrl.model.AssignedTo = [];
        for (var i = 0; i < ctrl.liEmployee1.length; i++) {
            if (ctrl.liEmployee1[i].LoginName == $rootScope.loginName) {
                ctrl.model.AssignedTo.push(ctrl.liEmployee1[i]);
            }
        }
    }
    //begin add
    ctrl.ClearAdd = function () {
        uploader.queue = [];
        ctrl.model = {};
        ctrl.addUnNewCheck = 0;
    }
    ctrl.LastSelect = {
        li1: [],
        li2: [],
        li3: [],
    };
    ctrl.LastSelectTemp = {
        li1: [],
        li2: [],
        li3: [],
    };
    ctrl.model = {
        AssignedTo: [],
        Supporter: [],
        Headship: []
    };
    ctrl.SelectOption = -1;
    ctrl.AddDataEmloyee = function (data) {
        for (var i = 0; i < data.length; i++) {
            for (var ii = 0; ii < ctrl.liEmployee.length; ii++) {
                if (data[i] == ctrl.liEmployee[ii].EmployeeId) {
                    if (ctrl.SelectOption == 0) {
                        ctrl.model.AssignedTo.push(ctrl.liEmployee[ii]);
                        ctrl.changeEmp(1);
                        break;
                    }
                    if (ctrl.SelectOption == 1) {
                        ctrl.model.Supporter.push(ctrl.liEmployee[ii]);
                        ctrl.changeEmp(2);
                        break;
                    }
                    if (ctrl.SelectOption == 2) {
                        ctrl.model.Headship.push(ctrl.liEmployee[ii]);
                        ctrl.changeEmp(3);
                        break;
                    }
                }
            }
        }
        //if (!ctrl.$$phase) {
        //    ctrl.$apply();
        //}
    }
    ctrl.SelectAddress = function (data) {
        ctrl.SelectOption = data;
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/SelectAddress.html',
            controller: 'Task_SelectAddress',
            backdrop: 'static',
            size: '50',
            resolve: {
                para: function () {
                    return null;
                }
            }
        });
        modalInstance.result.then(function (d) {
            ctrl.AddDataEmloyee(d);
            $rootScope.initData(4);
        }, function () {
        });
    }

    ctrl.changeEmp = function (data) {
        if (data == 1) {
            var temp = [];
            temp = temp.concat(ctrl.model.Supporter)
            temp = temp.concat(ctrl.model.Headship);
            for (var i = 0; i < temp.length; i++) {
                for (var j = 0; j < ctrl.model.AssignedTo.length; j++) {
                    if (ctrl.model.AssignedTo[j] == temp[i]) {
                        App.notifyDanger("Người này đã được chọn vào vị trí khác");
                        ctrl.model.AssignedTo = [];
                        for (var i = 0; i < ctrl.LastSelect.li1.length; i++) {
                            ctrl.model.AssignedTo.push(ctrl.LastSelect.li1[i]);
                        }
                        return;
                    }
                }
            }
            ctrl.LastSelect.li1 = [];
            for (var i = 0; i < ctrl.model.AssignedTo.length; i++) {
                ctrl.LastSelect.li1.push(ctrl.model.AssignedTo[i]);
            }
            return;
        }
        if (data == 2) {
            var temp = [];
            temp = temp.concat(ctrl.model.AssignedTo)
            temp = temp.concat(ctrl.model.Headship);
            for (var i = 0; i < temp.length; i++) {
                for (var j = 0; j < ctrl.model.Supporter.length; j++) {
                    if (ctrl.model.Supporter[j] == temp[i]) {
                        App.notifyDanger("Người này đã được chọn vào vị trí khác");
                        ctrl.model.Supporter = [];
                        for (var i = 0; i < ctrl.LastSelect.li2.length; i++) {
                            ctrl.model.Supporter.push(ctrl.LastSelect.li2[i]);
                        }
                        return;
                    }
                }
            }
            ctrl.LastSelect.li2 = [];
            for (var i = 0; i < ctrl.model.Supporter.length; i++) {
                ctrl.LastSelect.li2.push(ctrl.model.Supporter[i]);
            }
            return;
        }
        if (data == 3) {
            var temp = [];
            temp = temp.concat(ctrl.model.AssignedTo)
            temp = temp.concat(ctrl.model.Supporter);
            for (var i = 0; i < temp.length; i++) {
                for (var j = 0; j < ctrl.model.Headship.length; j++) {
                    if (ctrl.model.Headship[j] == temp[i]) {
                        App.notifyDanger("Người này đã được chọn vào vị trí khác");
                        ctrl.model.Headship = [];
                        for (var i = 0; i < ctrl.LastSelect.li3.length; i++) {
                            ctrl.model.Headship.push(ctrl.LastSelect.li3[i]);
                        }
                        return;
                    }
                }
            }
            ctrl.LastSelect.li3 = [];
            for (var i = 0; i < ctrl.model.Headship.length; i++) {
                ctrl.LastSelect.li3.push(ctrl.model.Headship[i]);
            }
            return;
        }
    }
    ctrl.splitEmployeeId = function (data) {
        var temp = [];
        for (var i = 0; i < data.length; i++) {
            temp.push(data[i].EmployeeId);
        }
        return temp;
    }
    ctrl.addNewItem = function () {
        if (ctrl.model.Subject == undefined || ctrl.model.Subject == null) {
            App.notifyDanger('Yêu cầu nhập tên công việc.');
            return;
        }
        if (ctrl.model.GroupOfTaskGuid == undefined || ctrl.model.GroupOfTaskGuid == null) {
            App.notifyDanger('Yêu cầu chọn nhóm công việc.');
            return;
        }
        if (ctrl.model.StartDate == undefined || ctrl.model.StartDate == null) {
            App.notifyDanger('Yêu cầu nhập ngày bắt đầu.');
            return;
        }
        if (ctrl.model.StartDate == undefined || ctrl.model.EndDate == null) {
            App.notifyDanger('Yêu cầu nhập ngày kết thúc.');
            return;
        }
        if (ctrl.model.AssignedTo == undefined || ctrl.model.AssignedTo == null) {
            App.notifyDanger('Yêu cầu chọn người chủ trì.');
            return;
        }
        if (ctrl.model.Content == undefined || ctrl.model.Content == null) {
            App.notifyDanger('Yêu cầu nhập nội dung công việc');
            return;
        }
        var fd = new FormData();
        if (uploader.queue.length > 0) {
            for (var i = 0; i < uploader.queue.length; i++) {
                var files = uploader.queue[i]._file;
                fd.append('file', files);
            }
        } else {

        }
        var item = {
            TaskGuid1: ctrl.splitEmployeeId(ctrl.model.AssignedTo),
            TaskGuid2: ctrl.splitEmployeeId(ctrl.model.Supporter),
            TaskGuid3: ctrl.splitEmployeeId(ctrl.model.Headship)
        }
        App.blockUI({
            target: "#contentMain",
            boxed: true,
            message: 'Đang tải...'
        });
        fd.append('insert', JSON.stringify(ctrl.model));
        fd.append('insertTOU', JSON.stringify(item));
        $.ajax({
            type: "POST",
            url: "/Cooperation/Tasks/insert",
            contentType: false,
            processData: false,
            data: fd,
            success: function (rs) {
                if (rs.Error) {
                    App.notifyDanger(rs.Title);
                }
                else {
                    App.notifyInfo("Thêm công việc thành công!");
                    ctrl.ClearAdd();
                    $rootScope.initData(7);
                }
            },
            error: function (rs) {

            }
        });
    }
    ctrl.addChildMenu = function () {
        ctrl.addChild();
    }
    ctrl.addUserToTaskMenu = function () {
        ctrl.editUserToTask();
    }
    ctrl.addChild = function () {
        if (ctrl.iSelected == undefined || ctrl.iSelected.TaskGuid == null || ctrl.iSelected.TaskGuid == '') {
            App.notifyDanger("Bạn chưa chọn công việc thêm công việc con");
            return;
        }
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/add.html',
            controller: 'Task_add',
            backdrop: 'static',
            size: TASK_OPTION_FORM_ADD.WIDTH_OPEN,
            resolve: {
                para: function () {
                    return {
                        ParentId: ctrl.iSelected.TaskGuid,
                        CategoryOfTaskGuid: ctrl.iSelected.CategoryOfTaskGuid,
                    }
                }
            }
        });
        modalInstance.result.then(function (d) {
            ctrl.changeSearch();
            $rootScope.initData(4);
        }, function () {
        });
    }
    $(document).on('click', function () {
        $('#ctxMenu').css('display', 'none');
    })
    ctrl.closeTabComment = function () {
        $(".settings-panel").removeClass("is-opened");
    }
    //end add
    //begin version view and recover
    ctrl.LoadVersion = function (d) {
        //Task_dataservice.getListVer({ IdI: [d] }, function (rs) {
        //    if (rs.Error) {
        //    } else {
        //        ctrl.DataVersion = rs;
        //    }
        //})
    }
    ctrl.OpenView = function (d) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/openVersion.html',
            controller: 'Task_openVersion',
            backdrop: 'static',
            size: '60',
            resolve: {
                para: function () {
                    return [ctrl.iSelected.TaskGuid, d.VerNumber];
                }
            }
        });
        modalInstance.result.then(function (d) {
            //ctrl.getItem(ctrl.iSelected.TaskGuid);
            //ctrl.changeSearch();
            $rootScope.initData(4);
        }, function () {
        });
    }
    //end version
    //Begin check list
    ctrl.modelCheckList = {};
    ctrl.loadCheckList = function () {
        Task_dataservice.loadCheckList({ IdS: [ctrl.iSelected.TaskGuid] }, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {
                ctrl.iSelected.CheckList = [];
                ctrl.iSelected.CheckList = rs.Object;
            }
        })
    }
    ctrl.insertCheckList = function () {
        if (ctrl.modelCheckList.CheckListTitle == undefined || ctrl.modelCheckList.CheckListTitle === null || ctrl.modelCheckList.CheckListTitle === '' || ctrl.modelCheckList.CheckListTitle.trim() === '') {
            App.notifyDanger('Yêu cầu nhập nội dung cho checklist.');
            return;
        }
        var _item = {
            CheckListTitle: ctrl.modelCheckList.CheckListTitle,
            Status: ctrl.modelCheckList.Status,
            TaskGuid: ctrl.iSelected.TaskGuid,
        };
        Task_dataservice.insertCheckList(_item, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {
                App.notifySuccess(rs.Title);
                ctrl.modelCheckList = {};
                ctrl.loadCheckList();
                ctrl.getActivitiesByTask(ctrl.iSelected.TaskGuid, 0, ctrl.comment.itemInPage, []);
            }
        })
    }
    ctrl.updateCheckList = function (d1) {
        Task_dataservice.updateCheckList({ IdI: [((d1.Status == true) ? 1 : 0)], IdS: [d1.CheckListGuid] }, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {
                App.notifySuccess(rs.Title);
                ctrl.loadCheckList();
            }
        })
    }
    ctrl.deleteCheckList = function (d1) {
        Task_dataservice.deleteCheckList({ IdS: [d1.CheckListGuid] }, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {
                App.notifySuccess(rs.Title);
                ctrl.loadCheckList();
            }
        });
    }
    //End check list
    //Begin comment
    ctrl.iSelectedComment = { GroupUser: [] };
    ctrl.comment = {
        liComment: [],
    };
    ctrl.comment.model = {
        Comment: '',
    };
    ctrl.comment.changeFileComment = function () {
        //if ($('#file-comment-id').prop('files').length > 0) {
        //    document.getElementById('task-upfile-name').innerHTML = $('#file-comment-id').prop('files')[0].name;
        //}
    }
    ctrl.comment.insert = function () {
        if (ctrl.iSelected.TaskGuid == undefined || ctrl.iSelected.TaskGuid == null || ctrl.iSelected.TaskGuid == '') {
            return;
        }
        if (ctrl.comment.model.Comment === undefined || ctrl.comment.model.Comment.trim() === '') {
            App.notifyDanger("Yêu cầu nhập nội dung trao đổi!");
            return;
        }
        ctrl.comment.model.ParentGuid = null;
        var fd = new FormData();
        ctrl.comment.model.RecordGuid = ctrl.iSelected.TaskGuid;
        var file = $('#file-comment-id').prop('files')[0];
        fd.append('insert', JSON.stringify(ctrl.comment.model));
        fd.append('file', file);
        App.blockUI({
            target: ".blockcomment",
            boxed: true,
            message: 'Đang tải...'
        });
        ctrl.comment.model.Comment = '';
        ctrl.comment.NameFile = undefined;

        try {
            ctrl.$apply();
        } catch (ex) {
            console.log(ex)
        }
        $.ajax({
            type: "POST",
            url: "/Cooperation/Tasks/ComInsert",
            contentType: false,
            processData: false,
            data: fd,
            success: function (rs) {
                App.unblockUI(".blockcomment");
                if (rs.Error) {
                    App.notifyDanger(rs.Title);
                }
                else {
                    $("#file-comment-id").val('');
                    try {
                        $rootScope.ConCommentAtt(rs.Object);
                        ctrl.runtimeSend(rs.Object);
                    } catch (ex) {
                        console.log(ex)
                    }
                    try {
                        setTimeout(function () {
                            $('#commentInsert').val('');
                            $('#commentInsertForm').val('');
                        }, 100);
                    } catch (ex) {
                        console.log('error set height scroll');
                    }
                    ctrl.loadAttbyType();
                }
            }
        });
    }
    ctrl.comment.pagingcomment = 1;
    ctrl.comment.itemInPage = 10;
    ctrl.comment.CountComment = 0;
    //Lọc theo các trạng thái user từ các biến
    ctrl.comment.loadComment = function (RecordGuid, Skip) {
        if (Skip === 1 || Skip === 0) {
            ctrl.comment.pagingcomment = 1;
            ctrl.comment.liComment = [];
        }
        Task_dataservice.comList({ RecordGuid: RecordGuid, Skip: Skip, Take: ctrl.comment.itemInPage, GroupUser: ctrl.iSelectedComment.GroupUser }, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {
                for (var i = 0; i < rs.Object.Data.length; i++) {
                    $rootScope.ConCommentAtt(rs.Object.Data[i]);
                    if (rs.Object.Data[i].TaskCommentAttachments && rs.Object.Data[i].TaskCommentAttachments != null && rs.Object.Data[i].TaskCommentAttachments.length > 0) {
                        for (var j = 0; j < rs.Object.Data[i].TaskCommentAttachments.length; j++) {
                            rs.Object.Data[i].TaskCommentAttachments[j].link = "/Cooperation/Tasks/ComDownload/" + rs.Object.Data[i].TaskCommentAttachments[j].AttachmentGuid;
                        }
                    }
                    ctrl.comment.liComment.push(rs.Object.Data[i]);
                }
                ctrl.comment.CountComment = rs.Object.Count;
            }
        });
    }
    ctrl.comment.init = function (d1) {
        ctrl.comment.loadComment(ctrl.iSelected.TaskGuid, 1);
    }
    ctrl.comment.loadMoreCommenttTask = function () {
        ctrl.comment.pagingcomment += 1;
        ctrl.comment.loadComment(ctrl.iSelected.TaskGuid, ctrl.comment.pagingcomment);
    }
    ctrl.replyComment = function () {
        if (ctrl.iSelected.TaskGuid === undefined || ctrl.iSelected.TaskGuid === null || ctrl.iSelected.TaskGuid === '') {
            return;
        }
        if (ctrl.comment.model.Comment === undefined || ctrl.comment.model.Comment.trim() === '') {
            App.notifyDanger("Yêu cầu nhập nội dung trao đổi!");
            return;
        }
        if (ctrl.iSelectedComment.haveQuote) {
            var _quote = '<div class="m-social-profile__activity-item-blockquote">';
            _quote += ctrl.iSelectedComment.EmployeeName + ' : ';
            _quote += htmlToElement(ctrl.iSelectedComment.Comment).textContent;
            _quote += '</div>';
            ctrl.comment.model.Comment = _quote + ctrl.comment.model.Comment;
        }
        if (ctrl.iSelectedComment.haveQuote && ctrl.iSelectedComment.haveQuote === 'QUOTECHILD') {
            ctrl.comment.model.ParentGuid = ctrl.iSelectedComment.ParentGuid;
        } else {
            ctrl.comment.model.ParentGuid = ctrl.iSelectedComment.CommentGuid;
        }
        ctrl.comment.model.RecordGuid = ctrl.iSelected.TaskGuid;
        var fd = new FormData();
        var file = $('#file-comment-id').prop('files')[0];

        fd.append('insert', JSON.stringify(ctrl.comment.model));
        fd.append('file', file);
        $.ajax({
            type: "POST",
            url: "/Cooperation/Tasks/ComInsert",
            contentType: false,
            processData: false,
            data: fd,
            success: function (rs) {
                if (rs.Error) {
                    App.notifyDanger(rs.Title);
                }
                else {
                    App.notifyInfo(rs.Title);
                    ctrl.comment.model.Comment = '';
                    ctrl.comment.NameFile = undefined;
                    ctrl.comment.model.ParentGuid = null;
                    $("#filenamefile").val('');
                    ctrl.comment.loadComment(ctrl.iSelected.TaskGuid, ctrl.comment.pagingcomment);
                    var _el = document.getElementById('reply-comment');
                    _el.classList.remove('reply-comment-show');
                }
            }
        });
    }
    ctrl.replyCommentClick = function (d, item, status) {
        ctrl.iSelectedComment = item;
        $(_form).removeAttr('style');
        element_comment = d.target;
        var _postion = d.target.getBoundingClientRect();
        var _form = document.getElementById("reply-comment");
        _form.style.top = (_postion.top - 120) + "px";
        _form.style.left = (_postion.left - 300) + "px";
        $(_form).css('z-index', 99999);
        _form.classList.add('reply-comment-show');
        if (status === 'QUOTE' || status === 'QUOTECHILD') {
            ctrl.comment.model.quote = item.Comment;
            ctrl.iSelectedComment.haveQuote = status;
        } else {
            if (status === 'ACCEPT') {
                ctrl.comment.model.Comment = "Xác nhận nội dung này.";
                ctrl.replyComment();
            } else {
                ctrl.comment.model.quote = '';
            }
        }
    }
    ctrl.FileNameChnage = '';
    ctrl.addNameFile = function () {
        if ($('#file-comment-id').prop('files')[0] !== undefined)
            if (ctrl.comment.model !== undefined && ctrl.comment.model.Comment !== '') {
                ctrl.comment.model.Comment += ' ' + $('#file-comment-id').prop('files')[0].name;
            } else {
                ctrl.comment.model.Comment = $('#file-comment-id').prop('files')[0].name + ' ';
            }
        if (!ctrl.$$phase) {
            ctrl.$apply();
        }
    }
    ctrl.activeViewCommentImage = function (d) {
        ctrl.iSelected.Image = d;
        ctrl.showViewMoreListAtt = true;
        ctrl.liAttackImage = ctrl.dataAttbyType.Images;
    }
    ctrl.hideImage = function () {
        ctrl.showViewMoreListAtt = false;
    }
    ctrl.linkDownImage = '/Cooperation/Tasks/ComDownload/';
    ctrl.linkViewImage = '/Cooperation/Tasks/ComDownload/';
    ctrl.refreshImageList = function () {
        $('#img-view-slides').attr('style', '');
        ctrl.iSelected.FlipImageDeg = 0;
    }
    ctrl.swichImage = function (item) {
        ctrl.iSelected.Image = item;
        ctrl.refreshImageList();
    }
    ctrl.nextViewImage = function () {
        for (var i = 0; i < ctrl.liAttackImage.length; i++) {
            if (ctrl.liAttackImage[i].AttachmentGuid.toUpperCase() === ctrl.iSelected.Image.AttachmentGuid.toUpperCase()) {
                if (i === (ctrl.liAttackImage.length - 1)) {
                    ctrl.iSelected.Image = ctrl.liAttackImage[0];
                    break;
                }
                ctrl.iSelected.Image = ctrl.liAttackImage[i + 1];
                break;
            }
        }
    }
    ctrl.backViewImage = function () {
        for (var i = 0; i < ctrl.liAttackImage.length; i++) {
            if (ctrl.liAttackImage[i].AttachmentGuid.toUpperCase() === ctrl.iSelected.Image.AttachmentGuid.toUpperCase()) {
                if (i === 0) {
                    ctrl.iSelected.Image = ctrl.liAttackImage[ctrl.liAttackImage.length - 1];
                    break;
                }
                ctrl.iSelected.Image = ctrl.liAttackImage[i - 1];
                break;
            }
        }
    }
    ctrl.flipImage = function (d) {
        if (typeof (ctrl.iSelected.FlipImageDeg) === 'undefined') {
            ctrl.iSelected.FlipImageDeg = 90 * d;
        } else {
            ctrl.iSelected.FlipImageDeg += 90 * d;
        }

        var _t = ((ctrl.iSelected.FlipImageDeg / 90) % 2);
        switch (_t) {
            case 0:
            case -0:
                $('#img-view-slides').attr('style', 'transform:rotate(' + (ctrl.iSelected.FlipImageDeg) + 'deg);');
                break;
            case 1:
            case -1:
                $('#img-view-slides').attr('style', 'transform:rotate(' + (ctrl.iSelected.FlipImageDeg) + 'deg);width:' + $($('#img-view-slides').parent()).height() + 'px;');
                break;
            default:
                break;
        }
    }
    ctrl.downLoadFile = function (d1) {
        if (d1 !== undefined && d1 !== null && d1 !== '') {
            window.open(ctrl.linkDownImage + d1);
        }
    }
    //End comment
    //Begin open download link
    ctrl.dowloadLink = function (d1) {
        window.open(d1, '_blank');
    }
    //End open download link
    ctrl.checkHasSelected = function () {
        if (ctrl.iSelected === undefined || ctrl.iSelected.TaskGuid === null
            || ctrl.iSelected.TaskGuid === '') {
            App.notifyDanger("Bạn chưa chọn công việc chỉnh sửa");
            return true;
        }
        return false;
    }
    ctrl.addUserToTask = function () {
        if (ctrl.iSelected === undefined || ctrl.iSelected.TaskGuid === null || ctrl.iSelected.TaskGuid === '') {
            App.notifyDanger("Bạn chưa chọn công việc chỉnh sửa");
            return;
        }
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/addUserToTask.html',
            controller: 'Task_addUserToTask',
            backdrop: 'static',
            size: '40',
            resolve: {
                para: function () {
                    return ctrl.iSelected.TaskGuid;
                }
            }
        });
        modalInstance.result.then(function (d) {
            ctrl.getItem(ctrl.iSelected.TaskGuid);
            $rootScope.initData(4);
        }, function () {
        });
    }
    ctrl.editUserToTask = function () {
        if (ctrl.iSelected == undefined || ctrl.iSelected.TaskGuid == null || ctrl.iSelected.TaskGuid == '') {
            App.notifyDanger("Bạn chưa chọn công việc chỉnh sửa");
            return;
        }
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/editUserToTask.html',
            controller: 'Task_editUserToTask',
            backdrop: 'static',
            size: '90',
            resolve: {
                para: function () {
                    return ctrl.iSelected.TaskGuid;
                }
            }
        });
        modalInstance.result.then(function (d) {
            ctrl.getItem(ctrl.iSelected.TaskGuid);
            ctrl.changeSearch();
            $rootScope.initData(4);
        }, function () {
        });
    }
    ctrl.choiceSaveFolder = function () {
        if (ctrl.iSelected === undefined || ctrl.iSelected.TaskGuid === null || ctrl.iSelected.TaskGuid === '') {
            App.notifyDanger("Bạn chưa chọn công việc lưu thư mục");
            return;
        }
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/viewSaveFolder.html',
            controller: 'Task_viewSaveFolder',
            backdrop: 'static',
            size: '30',
            resolve: {
                para: function () {
                    return ctrl.iSelected.TaskGuid;
                }
            }
        });
        modalInstance.result.then(function (d) {
            ctrl.getItem(ctrl.iSelected.TaskGuid);
            $rootScope.initData(4);
            ctrl.loadProfile();
        }, function () {
        });
    }
    ctrl.addSaveFolder = function () {
        if (ctrl.iSelected === undefined || ctrl.iSelected.TaskGuid === null || ctrl.iSelected.TaskGuid === '') {
            App.notifyDanger("Bạn chưa chọn công việc lưu thư mục");
            return;
        }
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/viewSaveFolder.html',
            controller: 'Task_addSaveFolder',
            backdrop: 'static',
            size: '30',
            resolve: {
                para: function () {
                    return ctrl.iSelected.TaskGuid;
                }
            }
        });
        modalInstance.result.then(function (d) {
            ctrl.getItem(ctrl.iSelected.TaskGuid);
            $rootScope.initData(4);
        }, function () {
        });
    }
    //Update follow task
    ctrl.updateFollow = function () {
        Task_dataservice.updateFollow({ IdS: [ctrl.iSelected.TaskGuid] }, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {
                App.notifySuccess(rs.Title);
                ctrl.getItem(ctrl.iSelected.TaskGuid);
            }
        })
    }
    // begin using for jstree
    ctrl.dProfiles = [];
    ctrl.jstreeData = [];
    ctrl.data = [

    ];
    function subJtree(data, parentVal) {
        var subTree = [];
        $.each(data, function (index, item) {
            if (item.ParentId && item.ParentId === parentVal) {
                var treeObjs = item;
                var sub = subJtree(data, item.Id);
                if (sub.length > 0) {
                    treeObjs.children = sub;
                }
                treeObjs.expanded = false;
                subTree.push(treeObjs);
            }
        });
        return subTree;
    }
    function initJsTreeData(data) {
        if (!data) {
            return null;
        }
        var jsData = [];
        data.forEach(function (item, index) {
            if (item.ParentId === null) {
                var treeItem = $.extend({}, item);
                treeItem = $.extend({
                    id: item.Id,
                    parent: '#',
                    text: item.text,
                    css: '',
                    TaskCount: item.TaskCount,
                    state: {
                        opened: true
                    }
                }, treeItem);
                jsData.push(treeItem);
                subJsTreeData(jsData, data, item.Id, 15);
            }
        });
        return jsData;
    }
    function subJsTreeData(jsData, data, parentid, css) {
        data.forEach(function (item, index) {
            if (item.ParentId && item.ParentId === parentid) {
                var treeItem = $.extend({}, item);
                treeItem = $.extend({
                    id: item.Id,
                    parent: item.ParentId,
                    text: item.text,
                    css: 'hasChild' + css,
                    TaskCount: item.TaskCount,
                    state: {
                        "opened": true
                    }
                }, treeItem);
                jsData.push(treeItem);
                subJsTreeData(jsData, data, item.Id, css + 15);
            }
        });

        return jsData;
    }
    ctrl.jstreeData = initJsTreeData(ctrl.data);
    ctrl.ignoreChanges = false;
    ctrl.applyModelChanges = function () {
        return !ctrl.ignoreChanges;
    };
    function customMenu(node) {
        var items = {
            'item1': {
                'label': 'Thêm mới ',
                'action': function () { /* action */ }
            },
            'item2': {
                'label': 'Sửa tên',
                'action': function () { /* action */ }
            },
            'item3': {
                'label': 'Chuyển thư mục',
                'action': function () { /* action */ }
            },
            'item4': {
                'label': 'Xóa',
                'action': function () { /* action */ }
            },
        };
        if (node.type === 'level_1') {
            delete items.item2;
        } else if (node.type === 'level_2') {
            delete items.item1;
        }
        return items;
    }
    ctrl.treeConfig = {
        core: {
            error: function (error) {
                $log.error('treeCtrl: error from js tree - ' + angular.toJson(error));
            },
            check_callback: true,
            worker: true,
            multiple: false,
        },
        "checkbox": {
            "three_state": false,
            "whole_node": false,
            "keep_selected_style": true,
            "cascade": "undetermined",
        },
        contextmenu: {
            'items': customMenu
        },
        plugins: ["checkbox", 'types', "dnd", "changed"],
        version: 1,
        types: {
            valid_children: ["selected"],
            types: {
                "selected": {
                    "select_node": false
                }
            }
        }
    };
    ctrl.applyModelChanges = function () {
        return !ctrl.ignoreChanges;
    };
    ctrl.readyCB = function () {
        $timeout(function () {
            ctrl.ignoreChanges = false;
        });
    }
    ctrl.changeNode = function (event, data) {
        var items = ctrl.treeInstance.jstree(true).get_selected();
    };
    ctrl.loadProfile = function () {
        Task_dataservice.loadProfile({ IdS: [] }, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {
                if (rs.Object.length > 0) {
                    for (var i = 0; i < rs.Object.length; i++) {
                        rs.Object[i].Id = rs.Object[i].ProfileGuid;
                        rs.Object[i].ParentId = rs.Object[i].ParentId;
                        rs.Object[i].text = rs.Object[i].Title;
                    }
                    ctrl.dProfiles = rs.Object;
                    ctrl.jstreeData = initJsTreeData(ctrl.dProfiles);
                } else {
                    ctrl.jstreeData = [];
                }
                ctrl.treeConfig.version++;
            }
        })
    }
    ctrl.init = function () {
        ctrl.loadProfile();
    }
    //ctrl.init();
    // end using for jstree
    // Begin using for profile
    ctrl.clickProfile = function () {
        cancelCloseDropdown();
    }
    // End using for profile
    // begin add link 
    ctrl.reloadAddLink = function () {
        for (var i = 0; i < ctrl.liTasks.length; i++) {
            for (var j = 0; j < ctrl.liTasks[i].EmpJoin.length; j++) {
                ctrl.liTasks[i].EmpJoin[j].linkImage = utility.COMMON_LINK_API.link_employee + ctrl.liTasks[i].EmpJoin[j].EmployeeGuid;
            }
        }
    }
    // end add link
    // begin send email
    ctrl.sendEmail = function () {
        if (ctrl.checkHasChoise()) {
            App.blockUI({
                target: "#contentMain",
                boxed: true,
                message: 'Đang tải...'
            });
            Task_dataservice.sendEmail({ TaskGuid: ctrl.iSelected.TaskGuid }, function (rs) {
                App.unblockUI("#contentMain");
                if (rs.Error) {
                    App.notifyDanger(rs.Title);
                } else {
                    App.notifySuccess(rs.Title);
                }
            })
        }
    }
    ctrl.checkHasChoise = function () {
        try {
            if (ctrl.iSelected && ctrl.iSelected.TaskGuid !== undefined && ctrl.iSelected.TaskGuid !== null && ctrl.iSelected.TaskGuid !== '') {
                return true;
            }
            App.notifyDanger('Yêu cầu chọn bạn bản ghi trước khi gửi email.');
            return false;
        } catch (ex) {
            App.notifyDanger('Yêu cầu chọn bạn bản ghi trước khi gửi email.');
            return false;
        }
    }
    // end send email
    // Begin using for runtime
    ctrl.runtimeChat = function (d1, d2) {
        var _obj = JSON.parse(d2);
        if (_obj.CS === ctrl.iSelected.TaskGuid) {
            if (d1.toUpperCase() === loginName.toUpperCase()) {
                _obj.IsSelf = false;
            } else {
                _obj.IsSelf = true;
            }
            ctrl.comment.liComment.unshift(_obj);
            for (var i = 0; i < ctrl.liTasks.length; i++) {
                if (ctrl.liTasks[i].TaskGuid.toUpperCase() === _obj.CS.toUpperCase()) {
                    ctrl.liTasks[i].CountComment++;
                    break;
                }
            }
            try {
                setTimeout(function () {
                    var _o = $('.m-messenger__messages-body');
                    _o[0].scrollTop = _o[0].scrollHeight + 100;
                    _o = $('.m-mail__content-scroll');
                    _o[0].scrollTop = _o[0].scrollHeight + 100;
                    if (ctrl.$root.$$phase !== '$apply' && ctrl.$root.$$phase !== '$digest') {
                        ctrl.$apply();
                    }
                }, 500);
            } catch (ex) {
                console.log(ex)
            }
        }
    }
    RUNTIME_V1.ReceiveFrom = ctrl.runtimeChat;
    ctrl.runtimeSend = function (obj) {
        obj.CS = ctrl.iSelected.TaskGuid;
        obj.Code = 'TASK';
        var _obj = {
            ToUser: obj.UserOfTask,
            DataJson: JSON.stringify(obj),
        };
        RUNTIME_V1.SendTo(_obj);
    }
    // End using for runtime
    //Begin using for menu left Folder 
    ctrl.showAddFolder = false;
    ctrl.ProfileActive = {};
    ctrl.modelProfiles = {};
    ctrl.reloadmodelProfiles = function () {
        ctrl.modelProfiles = {};
    }
    function __openMenuFolder() {
        $('#email-menu-Folder').css('top', $('.navbar.navbar-light.navbar-expand-lg').height());
        $('#email-menu-Folder').css('left', $('.page-wrap .sidebar-section').width());
        $('#email-menu-Folder').css('height', $('#splitTab1').height() + 20);
        $('#email-menu-Folder').css('display', 'unset');
        ctrl.resizeMenuFolder();
    }
    ctrl.init_ClickHSCV = function () {
        if ($('[link-code="MoreTask"] .mdi.mdi-chevron-right').length > 0) {
            $('[link-code="MoreTask"] .mdi.mdi-chevron-right').on('click', function (rs) {
                rs.preventDefault();
                __openMenuFolder();
            })
        } else {
            setTimeout(ctrl.init_ClickHSCV, 300);
        }
    }
    ctrl.init_ClickHSCV();
    $(window).resize(function () {
        $('#email-menu-Folder').css('height', $('#splitTab1').height());
        ctrl.resizeMenuFolder();
    })
    ctrl.resizeMenuFolder = function () {
        if (!ctrl.showAddFolder) {
            $('#email-menu-Folder-items').css('height', $('#splitTab1').height() - $('.Folder-actions').height());
        } else {
            $('#email-menu-Folder-items').css('height', $('#splitTab1').height() - $('.Folder-actions').height() - $('.Folder-add-from').height());
        }
    }
    ctrl.viewAddForm = function () {
        ctrl.showAddFolder = !ctrl.showAddFolder;
        ctrl.resizeMenuFolder();
        if (ctrl.$root.$$phase !== '$apply' && ctrl.$root.$$phase !== '$digest') {
            ctrl.$apply();
        }

    }
    ctrl.closeEmailFolder = function () {
        $('#email-menu-Folder').css('display', 'none');
    }
    ctrl.loadProfileFolder = function () {
        Task_dataservice.loadProfile({ IdS: [] }, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {
                if (rs.Object.length > 0) {
                    for (var i = 0; i < rs.Object.length; i++) {
                        rs.Object[i].Id = rs.Object[i].ProfileGuid;
                        rs.Object[i].ParentId = rs.Object[i].ParentId;
                        rs.Object[i].text = rs.Object[i].Title;
                    }
                    ctrl.dProfiles = initJsTreeData(rs.Object);
                    //ctrl.$apply();
                } else {
                    ctrl.dProfiles = [];
                }
            }
        })
    }
    ctrl.loadProfileFolder();
    ctrl._loadEmailByFolder = function (item) {
        ctrl.ProfileActive = item;
        ctrl.reloadGetItem();
        ctrl.initPara.ProfileGuid = ctrl.ProfileActive.id;
        ctrl.initPara.Page = 1;
        $rootScope.initData(4);
    }
    ctrl.reloadEmailFolder = function () {
        ctrl.showAddFolder = false;
        ctrl.modelProfiles.Title = '';
        ctrl.loadProfileFolder();
    }
    ctrl.addChildProfile = function (d, i) {
        ctrl.viewAddForm();
        ctrl.modelProfiles.ParentId = d.ProfileGuid;
    }
    ctrl.saveProfiles = function () {
        App.blockUI({
            target: ".modal-content",
            boxed: true,
            message: 'Đang tải...'
        });
        Task_dataservice.insertProfile(ctrl.modelProfiles, function (rs) {
            App.unblockUI(".modal-content");
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {
                App.notifySuccess(rs.Title);
                ctrl.reloadmodelProfiles();
                ctrl.loadProfileFolder();
                ctrl.viewAddForm();
            }
        })
    }
    ctrl._editEmailFolder = function (item) {
        ctrl.ProfileActive.BeginEdit = !ctrl.ProfileActive.BeginEdit;
        ctrl.ProfileActive.TitleEdit = ctrl.ProfileActive.Title;
    }
    ctrl.editProfileFolder = function (item) {
        item.Edit = false;
        item.BeginEdit = false;
        var _item = {
            ProfileGuid: item.ProfileGuid,
            ParentId: item.ParentId,
            Title: item.TitleEdit,
        };
        Task_dataservice.updateProfile(_item, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {
                App.notifyInfo(rs.Title);
                item.Title = item.TitleEdit;
                if (ctrl.initPara.ProfileGuid === ctrl.ProfileActive.ProfileGuid) {
                    ctrl._loadEmailByFolder({});
                }
                ctrl.reloadEmailFolder({});
            }
        })
    }
    ctrl._deleteProfile = function (item) {
        ESConfirm([{ 'value': 'Y', 'title': 'Xác nhận', 'type': 'info' },
        { 'value': 'N', 'title': 'Đóng', 'type': 'danger' }],
            { 'description': 'Bạn có chắc chắn xóa hồ sơ [' + item.Title + '] ?', 'class': 'eswarning_v2' },
            function (rs) {
                if (rs === 'Y') {
                    App.blockUI({
                        target: "#contentMain",
                        boxed: true,
                        message: 'Đang tải...'
                    });
                    Task_dataservice.deleteProfile({ IdS: [item.ProfileGuid] }, function (rs) {
                        App.unblockUI("#contentMain");
                        if (rs.Error) {
                            App.notifyDanger(rs.Title);
                        } else {
                            App.notifyInfo(rs.Title);
                            ctrl.reloadmodelProfiles();
                            ctrl.loadProfileFolder();
                            if (ctrl.ProfileActive.ProfileGuid === item.ProfileGuid) {
                                ctrl.loadEmailByFolder({});
                            }
                        }
                    })
                }
            }, function () {

            });
    }
    ctrl.deleteProfile = function () {
        if (ctrl.ProfileActive === undefined || ctrl.ProfileActive === null
            || ctrl.ProfileActive.ProfileGuid === undefined) {
            return;
        }
        ctrl._deleteProfile(ctrl.ProfileActive);
    }
    ctrl.loadEmailByFolder = function (item) {
        ctrl._loadEmailByFolder(item);
        ctrl.ProfileActive.Edit = true;
    }
    ctrl.liMenuChildFolder = [
        {
            Icon: 'mdi mdi-pencil-box-outline',
            Title: 'Sửa thư mục',
            Click: function (d, i) {
                for (var j = 0; j < ctrl.dProfiles.length; j++) {
                    if (d.ProfileGuid === ctrl.dProfiles[j].ProfileGuid) {
                        ctrl.loadEmailByFolder(ctrl.dProfiles[j]);
                        ctrl._editEmailFolder();
                    }
                }
            },
        },
        {
            Icon: 'mdi mdi-delete',
            Title: 'Xóa thư mục',
            Click: function (d, i) {
                ctrl.loadEmailByFolder(d);
                ctrl.deleteProfile(d);
            },
        },
        {
            Icon: 'mdi mdi-plus',
            Title: 'Thêm thư mục con',
            Click: function (d, i) {
                ctrl.addChildProfile(d, i);
            }
        },
    ];
    ctrl.reloadProfileActive = function () {
        ctrl.ProfileActive = {};
    }
    //End using for menu left
    //Begin function using for menu right click
    ctrl.addAttachment = function (item) {
        if (ctrl.checkHasSelected()) return;
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/addAttachment.html',
            controller: 'Task_addAttachment',
            backdrop: 'static',
            size: '70',
            resolve: {
                para: function () {
                    return ctrl.iSelected.TaskGuid;
                }
            }
        });
        modalInstance.result.then(function (d) {
            ctrl.getItem(ctrl.iSelected.TaskGuid);
            $rootScope.initData(4);
        }, function () {
        });
    }
    ctrl.addLabel = function (item) {
        if (ctrl.checkHasSelected()) return;
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/addLabel.html',
            controller: 'Task_editLabel',
            backdrop: 'static',
            size: '50',
            resolve: {
                para: function () {
                    return ctrl.iSelected.TaskGuid;
                }
            }
        });
        modalInstance.result.then(function (d) {
            ctrl.getItem(ctrl.iSelected.TaskGuid);
            $rootScope.initData(4);
        }, function () {
        });
    }
    ctrl.addEndDate = function (item) {
        if (ctrl.checkHasSelected()) return;
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/addDueDate.html',
            controller: 'Task_addDueDate',
            backdrop: 'static',
            size: '50',
            resolve: {
                para: function () {
                    return ctrl.iSelected;
                }
            }
        });
        modalInstance.result.then(function (d) {
            ctrl.getItem(ctrl.iSelected.TaskGuid);
            $rootScope.initData(3);
        }, function () {
        });
    }
    ctrl.upProcess = function () {
        if (ctrl.checkHasSelected()) return;
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/updateprocess.html',
            controller: 'Task_updateprocess',
            backdrop: 'static',
            size: '40',
            resolve: {
                para: function () {
                    return {
                        TaskGuid: ctrl.iSelected.TaskGuid,
                    }
                }
            }
        });
        modalInstance.result.then(function (d) {
            $rootScope.initData(3);
        }, function () {
        });
    }
    ctrl.moveToArchive = function () {
        if (ctrl.checkHasSelected()) return;
        ESConfirm([{ 'value': 'Y', 'title': 'Đồng ý', 'type': 'info' },
        { 'value': 'N', 'title': 'Bỏ qua', 'type': 'danger' }],
            { 'description': 'Xác nhận việc chuyển lưu công việc (công việc sẽ ẩn khỏi danh sách cho đến khi được khôi phục nếu cần) [' + ctrl.iSelected.Subject + ']', 'class': 'eswarning_v2' },
            function (rs) {
                if (rs === 'Y') {
                    App.blockUI({
                        target: "#contentMain",
                        boxed: true,
                        message: 'Đang tải...'
                    });
                    Task_dataservice.updateArchive({ IdS: [ctrl.iSelected.TaskGuid] }, function (rs) {
                        App.unblockUI("#contentMain");
                        if (rs.Error) {
                            App.notifyDanger(rs.Title);
                        } else {
                            $rootScope.initData(3);
                        }
                    })
                }
            }, function () {

            });
    }
    ctrl.addTemplate = function () {
        if (ctrl.checkHasSelected()) return;
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/addTemplate.html',
            controller: 'Task_addTemplate',
            backdrop: 'static',
            size: '40',
            resolve: {
                para: function () {
                    return ctrl.iSelected;
                }
            }
        });
        modalInstance.result.then(function (d) {
            $rootScope.initData(3);
        }, function () {
        });
    }
    ctrl.updateTaskCategory = function () {
        if (ctrl.checkHasSelected()) return;
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/updateTaskCategory.html',
            controller: 'Task_updateTaskCategory',
            backdrop: 'static',
            size: '40',
            resolve: {
                para: function () {
                    return ctrl.iSelected;
                }
            }
        });
        modalInstance.result.then(function (d) {
            $rootScope.initData(3);
        }, function () {
        });
    }
    ctrl.copy = function () {
        if (ctrl.checkHasSelected()) return;
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/add.html',
            controller: 'Task_copy',
            backdrop: 'static',
            size: '40',
            resolve: {
                para: function () {
                    return ctrl.iSelected.TaskGuid;
                }
            }
        });
        modalInstance.result.then(function (d) {
            $rootScope.initData(3);
        }, function () {
        });
    }
    ctrl.SetDelayProcess = function () {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/updateprocess.html',
            controller: 'Task_updateprocess',
            backdrop: 'static',
            size: '40',
            resolve: {
                para: function () {
                    return {
                        TaskGuid: ctrl.iSelected.TaskGuid,
                        StatusCompleted: 'D'
                    }
                },
            }
        });
        modalInstance.result.then(function (d) {
            $rootScope.initData(3);
        }, function () {
        });
    }
    //End function using for menu right click
    // Begin Using only kaban 
    ctrl.liMenuChildKaban = [
        {
            Icon: 'mdi mdi-eye',
            Title: 'Xem chi tiết',
            Code: 'VIEW',
            Click: function (d, i) {
                loadTMStart();
                ctrl.iSelected = d;
                ctrl.getItem(d.TaskGuid);
            },
        },
        {
            Icon: 'mdi mdi-plus',
            Title: 'Thêm công việc con',
            Code: 'ADD',
            Click: function (d, i) {
                ctrl.iSelected = d;
                ctrl.addChild();
            },
        },
        {
            Icon: 'mdi mdi-pencil-box-outline',
            Title: 'Sửa',
            Code: 'EDIT',
            Click: function (d, i) {
                ctrl.iSelected = d;
                ctrl.edit();
            },
            IsShow: function (d) {
                return d.EmpPermissionCustom.IsUpdate;
            }
        },
        {
            Icon: 'mdi mdi-delete',
            Title: 'Xóa vĩnh viễn',
            Code: 'DELETE',
            Click: function (d, i) {
                ctrl.iSelected = d;
                ctrl.del();
            },
        },
    ];
    $scope.StatusSearch = false;
    $scope.changeStatusSearch = function () {
        if ($scope.StatusSearch == false) {
            $scope.StatusSearch = true;
        } else {
            $scope.StatusSearch = false;
        }
    }
    $scope.Themhangmuc = false;
    $scope.loadChecklistAddTask123 = function () {
        if ($scope.Themhangmuc == false) {
            $scope.Themhangmuc = true;
        } else {
            $scope.Themhangmuc = false;
        }
    }
    $scope.modelCheckList = {
        CheckListTitle: ""
    }

    ctrl.Active_Remove = function () {
        var _d = {
            TaskOfUserGuid: ctrl.iSelected.TaskOfUserGuid,
            ProcessDelete: 'Y',
        };
        Task_dataservice.Active_ProcessDelete(_d, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {
                App.notifyInfo(rs.Title);
                ctrl.changeSearch();
            }
        })
    }


});

app.controller('Task_add', function ($scope, $timeout, $rootScope, $compile, $confirm, $uibModal, $uibModalInstance, FileUploader, Task_dataservice) {
    $scope.Title = 'Thêm mới công việc';
    $scope.model = { checkboxList: [], Labels: [], AssignedTo: [], Supporter: [], Headship: [], Priority: 'N', StatusCompleted:'N' };
    $scope.AssignedTo = false;
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    }
    var uploader = $scope.uploader = new FileUploader({});
    uploader.filters.push({
        name: 'customFilter',
        fn: function (item /*{File|FileLikeObject}*/, options) {
            return this.queue.length < 5000;
        }
    });
    uploader.onSuccessItem = function (fileItem, response, status, headers) {
        $scope.model.FileAttachments.push(response);
    };
    uploader.onCompleteAll = function () {
        $scope.commit();
    };
    $scope.addCategoryOfTask = function () {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + "/addCategoryOfTask.html",
            controller: 'Task_addCategoryOfTask',
            backdrop: 'static',
            size: '70',
            resolve: {
                para: function () {
                    return '';
                }
            }
        });
        modalInstance.result.then(function (d) {
            $scope.loadCategoryOfTask();
        }, function () {
        });
    }
    //End Category Of Task
    $scope.addLabel = function () {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + "/addLabel.html",
            controller: 'Task_addLabel',
            backdrop: 'static',
            size: '50',
            resolve: {
                para: function () {
                    return '';
                }
            }
        });
        modalInstance.result.then(function (d) {
            $scope.loadLabel();
        }, function () {
        });
    }
    $scope.CategoryOfTaskChange = function (data) {
        $scope.model.CategoryOfTaskGuid = data;
    }

    $scope.objectTypeChange = function (data) {
        $scope.model.Priority = data;
    }
    $scope.StatusCompletedChange = function (data) {
        $scope.model.StatusCompleted = data;
    }

    $scope.lstDeleteFile = [];
    $scope.submit = function () {
        if ($scope.model.AssignedTo == null || $scope.model.AssignedTo.length == 0) {
            App.notifyDanger("Yêu cầu chọn người chủ trì");
            return;
        }

        var fd = new FormData();
        $scope.lstTitlefile = [];
        if (uploader.queue.length > 0) {
            for (i = 0; i < uploader.queue.length; i++) {
                var files = uploader.queue[i]._file;
                var titleName = "";
                if (uploader.queue[i].file.FileName === "" || uploader.queue[i].file.FileName === null || uploader.queue[i].file.FileName === undefined) {
                    titleName = uploader.queue[i].file.name.split('.')[0];
                } else {
                    titleName = uploader.queue[i].file.FileName;
                }
                $scope.lstTitlefile.push({ STT: i, Title: titleName });
                fd.append("file", files);
            }
        }

        var _itemCheckList = [];
        for (var i = 0; i < $scope.model.checkboxList.length; i++) {
            if ($scope.model.checkboxList[i].CheckListTitle != null && $scope.model.checkboxList[i].CheckListTitle != undefined && $scope.model.checkboxList[i].CheckListTitle.trim() != "") {
                var _item = {
                    CheckListTitle: $scope.model.checkboxList[i].CheckListTitle,
                    Status: $scope.model.checkboxList[i].Status,
                };
                _itemCheckList.push(_item);
            }
        }
        var _liLabel = [];
        if ($scope.model.Labels && $scope.model.Labels !== null && $scope.model.Labels.length > 0) {
            for (var i = 0; i < $scope.model.Labels.length; i++) {
                var _item = {
                    LabelId: $scope.model.Labels[i].LabelId
                }
                _liLabel.push(_item);
            }
        }
        var _TaskOfUsers = [];
        if ($scope.model.AssignedTo !== null && $scope.model.AssignedTo.length > 0) {
            for (var i = 0; i < $scope.model.AssignedTo.length; i++) {
                var _item = {
                    EmployeeGuid: $scope.model.AssignedTo[i].EmployeeGuid,
                    IsOwner: 'A'
                }
                _TaskOfUsers.push(_item);
            }
        }
        if ($scope.model.Supporter !== null && $scope.model.Supporter.length > 0) {
            for (var i = 0; i < $scope.model.Supporter.length; i++) {
                var _item = {
                    EmployeeGuid: $scope.model.Supporter[i].EmployeeGuid,
                    IsOwner: 'B'
                }
                _TaskOfUsers.push(_item);
            }
        }
        if ($scope.model.Headship !== null && $scope.model.Headship.length > 0) {
            for (var i = 0; i < $scope.model.Headship.length; i++) {
                var _item = {
                    EmployeeGuid: $scope.model.Headship[i].EmployeeGuid,
                    IsOwner: 'C'
                }
                _TaskOfUsers.push(_item);
            }
        }

        App.blockUI({
            target: ".modal-content",
            boxed: true,
            message: 'Đang tải...'
        });
        fd.append('Task', JSON.stringify($scope.model));
        fd.append('Lables', JSON.stringify(_liLabel));
        fd.append('TaskCheckLists', JSON.stringify(_itemCheckList));
        fd.append('TaskOfUsers', JSON.stringify(_TaskOfUsers));
        fd.append('Label', JSON.stringify(_liLabel));
        fd.append('lstTitlefile', JSON.stringify($scope.lstTitlefile));
        fd.append('ListDeletefile', JSON.stringify($scope.lstDeleteFile));
        $.ajax({
            type: "POST",
            url: "/Cooperation/Tasks/Insert",
            contentType: false,
            processData: false,
            data: fd,
            success: function (rs) {
                App.unblockUI(".modal-content");
                if (rs.Error) {
                    App.notifyDanger(rs.Title);
                }
                else {
                    App.notifyInfo(rs.Title);
                    $rootScope.initData(3);
                    $scope.cancel();
                }

            },
            error: function (rs) {
                App.unblockUI(".modal-content");
            }
        });
    }


});
app.controller('Task_edit', function ($scope, $timeout, $rootScope, $compile, $confirm, $uibModal, $uibModalInstance, FileUploader, Task_dataservice, para) {
    var ctrl = $scope;
    $scope.Title = 'Sửa công việc';
    $scope.AssignedTo = true;
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    }
    var uploader = ctrl.uploader = new FileUploader({});
    uploader.filters.push({
        name: 'customFilter',
        fn: function (item /*{File|FileLikeObject}*/, options) {
            return this.queue.length < 5000;
        }
    });
    uploader.onSuccessItem = function (fileItem, response, status, headers) {
        ctrl.model.FileAttachments.push(response);
    };
    uploader.onCompleteAll = function () {
        ctrl.commit();
    };
    $scope.CategoryOfTaskChange = function (data) {
        $scope.model.CategoryOfTaskGuid = data;
    }

    $scope.objectTypeChange = function (data) {
        $scope.model.Priority = data;
    }
    $scope.StatusCompletedChange = function (data) {
        $scope.model.StatusCompleted = data;
    }
    $scope.liAtt = [];
    $scope.getAttachments = function () {
        Task_dataservice.getAttachments({ IdS: [para] }, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {
                for (var i = 0; i < rs.Object.length; i++) {
                    rs.Object[i].link = "/Cooperation/Tasks/Download?id=" + rs.Object[i].AttachmentGuid;
                }
                $scope.liAtt = rs.Object;
            }

        });
    }
    Task_dataservice.getItem({ IdS: [para] }, function (rs) {
        App.unblockUI("#contentMain");
        if (rs.Error) {
            App.notifyDanger(rs.Title);
        } else {
            rs.Object.StartDate = $rootScope.ConDate(rs.Object.StartDate, 2);
            rs.Object.EndDate = $rootScope.ConDate(rs.Object.EndDate, 2);
            ctrl.model = rs.Object;
            ctrl.model.AssignedTo = [];
            ctrl.model.Supporter = [];
            ctrl.model.Headship = [];
            var _liDataTaskOfUsers = [];
            for (var i = 0; i < ctrl.model.TaskOfUsers.length; i++) {
                _liDataTaskOfUsers.push(ctrl.model.TaskOfUsers[i]);
            }
            for (var i = 0; i < _liDataTaskOfUsers.length; i++) {
                if (_liDataTaskOfUsers[i].IsOwner == 'A') {
                    ctrl.model.AssignedTo.push(_liDataTaskOfUsers[i]);

                }
                if (_liDataTaskOfUsers[i].IsOwner == 'B') {
                    ctrl.model.Supporter.push(_liDataTaskOfUsers[i]);

                }
                if (_liDataTaskOfUsers[i].IsOwner == 'C') {
                    ctrl.model.Headship.push(_liDataTaskOfUsers[i]);
                }
            }
            ctrl.model.checkboxList = [];
            for (var i = 0; i < rs.Object.CheckList.length; i++) {
                ctrl.model.checkboxList.push(rs.Object.CheckList[i]);
            }
            $scope.getAttachments();
        }
    });

    $scope.lstDeleteFile = [];
    $scope.remoteAttach = function (item, number) {
        $scope.lstDeleteFile.push(item.AttachmentGuid);
        $scope.liAtt.splice(number, 1);
        if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
            $scope.$apply();
        }
    };

    $scope.submit = function () {
        if ($scope.model.AssignedTo == null || $scope.model.AssignedTo.length == 0) {
            App.notifyDanger("Yêu cầu chọn người chủ trì");
            return;
        }
        var fd = new FormData();
        $scope.lstTitlefile = [];
        if (uploader.queue.length > 0) {
            for (i = 0; i < uploader.queue.length; i++) {
                var files = uploader.queue[i]._file;
                var titleName = "";
                if (uploader.queue[i].file.FileName === "" || uploader.queue[i].file.FileName === null || uploader.queue[i].file.FileName === undefined) {
                    titleName = uploader.queue[i].file.name.split('.')[0];
                } else {
                    titleName = uploader.queue[i].file.FileName;
                }
                $scope.lstTitlefile.push({ STT: i, Title: titleName });
                fd.append("file", files);
            }
        }

        var _itemCheckList = [];
        for (var i = 0; i < $scope.model.checkboxList.length; i++) {
            if ($scope.model.checkboxList[i].CheckListTitle != null && $scope.model.checkboxList[i].CheckListTitle != undefined && $scope.model.checkboxList[i].CheckListTitle.trim() != "") {
                var _item = {
                    CheckListTitle: $scope.model.checkboxList[i].CheckListTitle,
                    Status: $scope.model.checkboxList[i].Status,
                };
                _itemCheckList.push(_item);
            }
        }
        var _liLabel = [];
        if ($scope.model.Labels && $scope.model.Labels !== null && $scope.model.Labels.length > 0) {
            for (var i = 0; i < $scope.model.Labels.length; i++) {
                var _item = {
                    LabelId: $scope.model.Labels[i].LabelId
                }
                _liLabel.push(_item);
            }
        }
        var _TaskOfUsers = [];
        if ($scope.model.AssignedTo !== null && $scope.model.AssignedTo.length > 0) {
            for (var i = 0; i < $scope.model.AssignedTo.length; i++) {
                var _item = {
                    EmployeeGuid: $scope.model.AssignedTo[i].EmployeeGuid,
                    IsOwner: 'A'
                }
                _TaskOfUsers.push(_item);
            }
        }
        if ($scope.model.Supporter !== null && $scope.model.Supporter.length > 0) {
            for (var i = 0; i < $scope.model.Supporter.length; i++) {
                var _item = {
                    EmployeeGuid: $scope.model.Supporter[i].EmployeeGuid,
                    IsOwner: 'B'
                }
                _TaskOfUsers.push(_item);
            }
        }
        if ($scope.model.Headship !== null && $scope.model.Headship.length > 0) {
            for (var i = 0; i < $scope.model.Headship.length; i++) {
                var _item = {
                    EmployeeGuid: $scope.model.Headship[i].EmployeeGuid,
                    IsOwner: 'C'
                }
                _TaskOfUsers.push(_item);
            }
        }

        App.blockUI({
            target: ".modal-content",
            boxed: true,
            message: 'Đang tải...'
        });
        fd.append('Task', JSON.stringify($scope.model));
        fd.append('Lables', JSON.stringify(_liLabel));
        fd.append('TaskCheckLists', JSON.stringify(_itemCheckList));
        fd.append('TaskOfUsers', JSON.stringify(_TaskOfUsers));
        fd.append('Label', JSON.stringify(_liLabel));
        fd.append('lstTitlefile', JSON.stringify($scope.lstTitlefile));
        fd.append('ListDeletefile', JSON.stringify($scope.lstDeleteFile));
        $.ajax({
            type: "POST",
            url: "/Cooperation/Tasks/Update",
            contentType: false,
            processData: false,
            data: fd,
            success: function (rs) {
                App.unblockUI(".modal-content");
                if (rs.Error) {
                    App.notifyDanger(rs.Title);
                }
                else {
                    App.notifyInfo(rs.Title);
                    $rootScope.initData(3);
                    $scope.cancel();
                }

            },
            error: function (rs) {
                App.unblockUI(".modal-content");
            }
        });
    }



});
app.controller('Task_updateprocess', function ($scope, $timeout, $rootScope, $compile, $confirm, $uibModal, $uibModalInstance, FileUploader, Task_dataservice, para) {
    var ctrl = $scope;
    $scope.Title = 'Cập nhật tiến độ công việc';
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    }
    var uploader = ctrl.uploader = new FileUploader({});
    uploader.filters.push({
        name: 'customFilter',
        fn: function (item /*{File|FileLikeObject}*/, options) {
            return this.queue.length < 5000;
        }
    });
    uploader.onSuccessItem = function (fileItem, response, status, headers) {
        ctrl.model.FileAttachments.push(response);
    };
    uploader.onCompleteAll = function () {
        ctrl.commit();
    };

    Task_dataservice.getItem({ IdS: [para.TaskGuid] }, function (rs) {
        App.unblockUI("#contentMain");
        if (rs.Error) {
            App.notifyDanger(rs.Title);
        } else {
            rs.Object.StartDate = $rootScope.ConDate(rs.Object.StartDate, 2);
            rs.Object.EndDate = $rootScope.ConDate(rs.Object.EndDate, 2);
            ctrl.modelCha = rs.Object;
            for (var i = 0; i < rs.Object.TaskOfUsers.length; i++) {
                if (rs.Object.TaskOfUsers[i].LoginName == $rootScope.loginName) {
                    rs.Object.TaskOfUsers[i].TimeReminder = $rootScope.ConDate(rs.Object.TaskOfUsers[i].TimeReminder, 2);
                    ctrl.model = rs.Object.TaskOfUsers[i];
                }
            }
           
        }
    });
    $scope.onchangeComplete = function () {
        if ($scope.model.PercentComplete == 0 || $scope.model.PercentComplete == null || $scope.model.PercentComplete == undefined) {
            $scope.model.StatusCompleted = 'N';
        }
        if ($scope.model.PercentComplete > 0 && $scope.model.PercentComplete < 100) {
            $scope.model.StatusCompleted = 'P';
        }
        if ($scope.model.PercentComplete == 100) {
            $scope.model.StatusCompleted = 'C';
        }
        try {
            if (!$scope.$$phase) {
                $scope.$apply();
            }
        } catch (ex) { }
    }
    $scope.onchangeStatus = function () {
        $rootScope.autoChangeStatusCompleted($scope.model.StatusCompleted, $scope.model, $scope);
    }
    $scope.submit = function () {
        if ($scope.model.TimeReminder == undefined || $scope.model.TimeReminder == null) {
            App.notifyDanger("Yều cầu nhập nhắc xử lý.");
            return;
        }
        if ($scope.model.PercentComplete >= 100) {
            $scope.model.CompleteDate = new Date();
        }
        if ($scope.model.PercentComplete === undefined || $scope.model.PercentComplete === null || $scope.model.PercentComplete > 100 || $scope.model.PercentComplete < 0) {
            App.notifyDanger("Yêu cầu nhập % hoàn thành trong khoảng 0 -> 100 ");
            return;
        }
        var fd = new FormData();
        if (uploader.queue.length > 0) {
            for (var i = 0; i < uploader.queue.length; i++) {
                var files = uploader.queue[i]._file;
                fd.append('file', files);
            }
        }
        fd.append('update', JSON.stringify($scope.model));
        App.blockUI({
            target: ".modal-dialog",
            boxed: true,
            message: 'Đang tải...'
        });
        $.ajax({
            type: "POST",
            url: "/Cooperation/Tasks/UpdateProcess",
            contentType: false,
            processData: false,
            data: fd,
            success: function (rs) {
                App.unblockUI(".modal-dialog");
                if (rs.Error) {
                    App.notifyDanger(rs.Title);
                }
                else {
                    App.notifyInfo(rs.Title);
                    $scope.cancel();
                    $rootScope.initData(3);
                }
            },
            error: function (rs) {

            }
        });
    }
});

app.controller('Task_addCategoryOfTask', function ($scope, $timeout, $rootScope, $compile, $confirm, $uibModal, $uibModalInstance, FileUploader, Task_dataservice, para) {
    $scope.Title = 'Thêm mới nhóm công việc';
    $scope.model = {};
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    }
    var uploader = $scope.uploader = new FileUploader({});
    uploader.filters.push({
        name: 'customFilter',
        fn: function (item /*{File|FileLikeObject}*/, options) {
            return this.queue.length < 5000;
        }
    });
    uploader.onSuccessItem = function (fileItem, response, status, headers) {
        $scope.model.FileAttachments.push(response);
    };
    uploader.onCompleteAll = function () {
        $scope.commit();
    };

    $scope.submit = function () {
        var fd = new FormData();
        $scope.lstTitlefile = [];
        if (uploader.queue.length > 0) {
            for (i = 0; i < uploader.queue.length; i++) {
                var files = uploader.queue[i]._file;
                var titleName = "";
                if (uploader.queue[i].file.FileName === "" || uploader.queue[i].file.FileName === null || uploader.queue[i].file.FileName === undefined) {
                    titleName = uploader.queue[i].file.name.split('.')[0];
                } else {
                    titleName = uploader.queue[i].file.FileName;
                }
                $scope.lstTitlefile.push({ STT: i, Title: titleName });
                fd.append("file", files);
            }
        }

        App.blockUI({
            target: ".modal-content",
            boxed: true,
            message: 'Đang tải...'
        });
        fd.append('Task', JSON.stringify($scope.model));    
        $.ajax({
            type: "POST",
            url: "/Cooperation/Tasks/Insert_CategoryOfTask",
            contentType: false,
            processData: false,
            data: fd,
            success: function (rs) {
                App.unblockUI(".modal-content");
                if (rs.Error) {
                    App.notifyDanger(rs.Title);
                }
                else {
                    App.notifyInfo(rs.Title);
                    $scope.cancel();
                }

            },
            error: function (rs) {
                App.unblockUI(".modal-content");
            }
        });
    }

});

app.controller('Task_addLabel', function ($scope, $timeout, $rootScope, $compile, $confirm, $uibModal, $uibModalInstance, FileUploader, Task_dataservice, para) {
    $scope.Title = 'Thêm mới nhãn';
    $scope.model = {};
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    }
    var uploader = $scope.uploader = new FileUploader({});
    uploader.filters.push({
        name: 'customFilter',
        fn: function (item /*{File|FileLikeObject}*/, options) {
            return this.queue.length < 5000;
        }
    });
    uploader.onSuccessItem = function (fileItem, response, status, headers) {
        $scope.model.FileAttachments.push(response);
    };
    uploader.onCompleteAll = function () {
        $scope.commit();
    };

    $scope.submit = function () {
        var fd = new FormData();
        $scope.lstTitlefile = [];
        if (uploader.queue.length > 0) {
            for (i = 0; i < uploader.queue.length; i++) {
                var files = uploader.queue[i]._file;
                var titleName = "";
                if (uploader.queue[i].file.FileName === "" || uploader.queue[i].file.FileName === null || uploader.queue[i].file.FileName === undefined) {
                    titleName = uploader.queue[i].file.name.split('.')[0];
                } else {
                    titleName = uploader.queue[i].file.FileName;
                }
                $scope.lstTitlefile.push({ STT: i, Title: titleName });
                fd.append("file", files);
            }
        }

        App.blockUI({
            target: ".modal-content",
            boxed: true,
            message: 'Đang tải...'
        });
        fd.append('Task', JSON.stringify($scope.model));
        $.ajax({
            type: "POST",
            url: "/Cooperation/Tasks/Insert_Labels",
            contentType: false,
            processData: false,
            data: fd,
            success: function (rs) {
                App.unblockUI(".modal-content");
                if (rs.Error) {
                    App.notifyDanger(rs.Title);
                }
                else {
                    App.notifyInfo(rs.Title);
                    $scope.cancel();
                }

            },
            error: function (rs) {
                App.unblockUI(".modal-content");
            }
        });
    }
});


