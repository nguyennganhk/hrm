﻿var ctxfolder = "/views/Sale/Lables";
app.factory('dataservice', function ($http) {
    var headers = {
        "Content-Type": "application/json;odata=verbose",
        "Accept": "application/json;odata=verbose"
    };
    return {
        getDepartments: function (callback) {
            $http.get('/Lables/GetDepartments').success(callback);
        },
        insert: function (data, callback) {
            $http.post('/Lables/Insert', data).success(callback);
        },
        delete: function (data, callback) {
            $http.post('/Lables/Delete?id=' + data).success(callback);
        },
        deleteItems: function (data, callback) {
            $http.post('/Lables/IsDeletedItems/', data).success(callback);
        },
        getItem: function (data, callback) {
            $http.post('/Lables/GetById?Id=' + data).success(callback);
        },
        update: function (data, callback) {
            $http.post('/Lables/Update/', data).success(callback);
        }
    };
});
app.filter('html', ['$sce', function ($sce) {
    return function (text) {
        return $sce.trustAsHtml(text);
    };
}]);

app.directive('format', ['$filter', function ($filter) {
    return {
        require: '?ngModel',
        link: function (scope, elem, attrs, ctrl) {
            if (!ctrl) return;
            ctrl.$formatters.unshift(function (a) {
                return $filter(attrs.format)(ctrl.$modelValue);
            });
            ctrl.$parsers.unshift(function (viewValue) {
                var plainNumber = viewValue.replace(/[^\d|\-+|\.+]/g, '');
                elem.val($filter(attrs.format)(plainNumber));
                return plainNumber;
            });
        }
    };
}]);
//định dạng ngày
app.filter('fdate', [
    '$filter', function ($filter) {
        return function (input, f) {
            if (input && input.toString().indexOf('StartDate') > -1) {
                return moment(input).format(f);
            } if (input && input.toString().indexOf('T') > -1) {
                return moment(input).format(f);
            } else return input;
        };
    }
]);

app.controller('Ctrl_Lables', function ($scope, $rootScope, $sce, $compile, $uibModal, dataservice) {
    $rootScope.Title = "";
    $rootScope.go = function (path) {
        $location.path(path); return false;
    };
    $rootScope.OnlyABS = function (d) {
        if (d.OrderId < 0) {
            d.OrderId = 0;
        }
    }
    $rootScope.validationOptions = {
        rules: {
            LableName: {
                required: true,
                maxlength: 30
            },
            ProjectTypeName: {
                required: true,
                maxlength: 50
            }
        },
        messages: {
            LableName: {
                required: "Yêu cầu nhập tên nhãn.",
                maxlength: "Tên nhãn không vượt quá 30 ký tự."
            },
            ProjectTypeName: {
                required: "Yêu cầu nhập tên loại dự án.",
                maxlength: "Tên loại dự án không vượt quá 50 ký tự."
            }
        }
    };
    $rootScope.ListIsActive = [{
        value: 1,
        text: 'Sử dụng'
    }, {
        value: 0,
        text: 'Không sử dụng'
    }];
    $rootScope.IsActiveConfig = {
        placeholder: 'Chọn trạng thái',
        search: true
    };
    $rootScope.IsActive = [{
        value: 1,
        text: 'Sử dụng'
    }, {
        value: 0,
        text: 'Không sử dụng'
    }];
    $rootScope.IsActiveConfig = {
        placeholder: 'Chọn trạng thái',
        search: true
    };
    $rootScope.CreateDate = function (date) {
        if (date !== null && date !== undefined) {
            var newdate = new Date(date);
            var month = newdate.getMonth() + 1;
            var day = newdate.getDate();
            var year = newdate.getFullYear();
            var hh = newdate.getHours();
            var mm = newdate.getMinutes();
            if (month < 10)
                month = "0" + month;
            if (day < 10)
                day = "0" + day;
            if (mm < 10)
                mm = "0" + mm;
            return todayDate = day + "/" + month + "/" + year;
        } else {
            return null;
        }
    };
    $rootScope.EditDate = function (datetime) {
        if (datetime !== null && datetime !== undefined) {
            var Str = datetime.toString();
            if (Str.indexOf("/Date") >= 0) {

                var newdate = new Date(parseInt(datetime.substr(6)));
                var month = newdate.getMonth() + 1;
                var day = newdate.getDate();
                var year = newdate.getFullYear();
                var hh = newdate.getHours();
                var mm = newdate.getMinutes();
                if (month < 10)
                    month = "0" + month;
                if (day < 10)
                    day = "0" + day;
                return year + "/" + month + "/" + day;
            } else {
                var value = datetime.split('/');

                return value[2] + '/' + value[1] + '/' + value[0];
            }
        }
        return null;
    };
    //định dạng số xóa dấu chấm phẩy
    $rootScope.addPeriod = function (nStr) {
        if (nStr !== null && nStr !== "" && nStr !== undefined) {
            nStr = Math.round(parseFloat(nStr) * 100) / 100;

            nStr += "";
            if (nStr.indexOf(",") >= 0) {
                var x = nStr.split(",");
            } else {
                var x = nStr.split(".");
            }
            var x1 = x[0];
            var x2 = x.length > 1 && parseInt(x[1]) > 0 ? "," + x[1] : "";
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, "$1" + "." + "$2");
            }
            var Total = x1 + x2;
            return Total;

        }
        else {
            return 0;
        }
    };
});
app.config(function ($routeProvider, $validatorProvider) {
    $routeProvider
        .when('/', {
            templateUrl: ctxfolder + '/index.html',
            controller: 'index'
        })
        .when('/edit/:id', {
            templateUrl: ctxfolder + '/edit.html',
            controller: 'edit'
        })
        .when('/add/', {
            templateUrl: ctxfolder + '/add.html',
            controller: 'add'
        });
    $validatorProvider.setDefaults({
        errorElement: 'span',
        errorClass: 'help-block',
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        success: function (label) {
            label.closest('.form-group').removeClass('has-error');
        }
    });
});
//controller hiển thị, tìm kiếm
//app.controller('index', function ($scope, $rootScope, $compile, $http, $confirm, $ngConfirm, $uibModal, DTOptionsBuilder, DTColumnBuilder, DTInstances, dataservice) {
//    $scope.model = {};
//    var vm = $scope;
//    $scope.staticParam = {
//        totalItems: 0,
//        currentPage: 1,
//        pageSize: 25,
//        maxSize: 5,
//        Search: "",
//        IsActive: "1"
//    };
//    $scope.IsActiveChange = function () {
//        $scope.selectAll = false;
//        $scope.Search();
//    };
//    $scope.selected = [];
//    $scope.selectAll = false;
//    $scope.toggleOne = toggleOne;
//    $scope.toggleAll = toggleAll;
//    var titleHtml = '<label class="mt-checkbox"><input type="checkbox" ng-model="selectAll" ng-click="toggleAll(selectAll, selected)"/><span></span></label>';
//    //if (App.Permissions.LISTVIEW)
//    vm.dtOptions = DTOptionsBuilder.newOptions()
//        .withPaginationType('full_numbers')
//        .withDataProp('data')
//        .withOption('order', [4, 'asc'])
//        .withOption('serverSide', true)
//        .withOption('pageLength', 25)
//        .withOption('info', false)
//        .withOption('stateLoadParams', function (settings, data) {
//            data.search.search = '';
//        })
//        .withOption('headerCallback', function (header) {
//            if (!$scope.headerCompiled) {
//                $scope.headerCompiled = true;
//                $compile(angular.element(header).contents())($scope);
//            }
//        })
//        .withOption('initComplete', function (settings, json) {
//            //thêm sự kiện scroll phân trang
//            $('#tblData').parent().attr("onscroll", "angular.element(this).scope().LoadScroll(this)");
//        })
//        .withOption('createdRow', function (row, data, dataIndex) {
//            const contextScope = $scope.$new(true);
//            contextScope.data = data;
//            $scope.data = data;
//            $compile(row)($scope);
//            contextScope.contextMenu = $scope.contextMenu;
//            $compile(angular.element(row).find('input'))($scope);
//            $compile(angular.element(row).attr('context-menu', 'contextMenu'))(contextScope);
//        });
//    // if (App.Permissions.LISTVIEW)
//    vm.dtOptions.withOption('ajax', function (data, callback, settings) {
//        data.search.value = $scope.staticParam.Search;
//        data.IsActive = parseInt($scope.staticParam.IsActive);
//        App.blockUI({
//            target: "#tblData",
//            boxed: true,
//            message: 'Đang tải...'
//        });
//        $http.post('/Lables/JTableLables', data)
//            .success(function (res) {
//                if (data.start === 0) {
//                    $scope.dataload = [];
//                    angular.forEach(res.data, function (val, key) {
//                        val._STT = key;
//                        $scope.dataload.push(val);
//                    });
//                    callback({
//                        data: $scope.dataload,
//                        recordsTotal: res.recordsTotal,
//                        recordsFiltered: res.recordsFiltered
//                    });
//                } else {
//                    if (res.recordsTotal > $scope.dataload.length) {
//                        angular.forEach(res.data, function (val, key) {
//                            val._STT = $scope.dataload.length;
//                            $scope.dataload.push(val);
//                        });
//                        callback({
//                            data: $scope.dataload,
//                            recordsTotal: res.recordsTotal,
//                            recordsFiltered: res.recordsFiltered
//                        });
//                    }
//                }
//                App.unblockUI("#tblData");
//            });
//    });
//    //scroll theo tỷ lệ màn hình
//    vm.dtOptions.withOption('scrollY', '60vh')
//        .withOption('scrollX', '100%') //mặc định 100%
//        .withOption('scrollCollapse', true) //xóa khoảng trắng khi dữ liệu trong bảng ít
//        .withOption('rowCallback', rowCallback) //sự kiên click vào 1 dòng
//        .withOption('scroller', {
//            loadingIndicator: true
//        })
//    //table
//    vm.dtColumns = [];
//    vm.dtColumns.push(DTColumnBuilder.newColumn("LableId").withTitle(titleHtml).notSortable()
//        .renderWith(function (data, type, full, meta) {
//            $scope.selected[full.LableId] = false;
//            return '<label class="mt-checkbox"><input type="checkbox" ng-model="selected[\'' + full.LableId + '\']" ng-click="toggleOne(selected)"/><span></span></label>';
//        }).withOption('sWidth', '5px').withOption('sClass', 'tcenter tcenter-header'));
//    vm.dtColumns.push(DTColumnBuilder.newColumn('LableId').withTitle('STT').notSortable().withOption('sWidth', '50px').renderWith(function (data, type, full, meta) {
//        return meta.row + 1;
//    }).withOption('sClass', 'tcenter tcenter-header'));

//    vm.dtColumns.push(DTColumnBuilder.newColumn('LableName').withTitle('Tên nhãn').withOption('sWidth', '200px').withOption('sClass', 'tcenter-header').renderWith(function (data, type, full, meta) {
//        return '<a  style="color: #4285f4;" href="" ng-click="open(\'' + full.LableId + '\')" >' + data + '</a>';
//    }));
//    vm.dtColumns.push(DTColumnBuilder.newColumn('Class').withTitle('Màu nhãn').withOption('sWidth', '100px').withOption('sClass', 'tcenter-header').renderWith(function (data, type, full, meta) {
//        return data;
//    }));
//    vm.dtColumns.push(DTColumnBuilder.newColumn('OrderId').withTitle('Thứ tự').withOption('sWidth', '80px').withOption('sClass', 'tcenter-header').renderWith(function (data, type, full, meta) {
//        return data;
//    }));
//    vm.dtColumns.push(DTColumnBuilder.newColumn('Description').withTitle('Nội dung').withOption('sWidth', '250px').withOption('sClass', 'tcenter-header').renderWith(function (data, type, full, meta) {
//        return data;
//    }));
//    vm.dtColumns.push(DTColumnBuilder.newColumn('LableId').withTitle('Điều khiển').notSortable().withOption('sWidth', '100px').withOption('sClass', 'tcenter').renderWith(function (data, type, full, meta) {
//        return '<div class="table__cell-actions-wrap">' +
//            '<a ng-click="open(\'' + full.LableId + '\')" class="table__cell-actions-item table__cell-actions-icon"> <span class="mdi mdi-eye" title="Xem chi tiết"></span> </a>' +
//            '<a ng-click="edit(\'' + full.LableId + '\')" class="table__cell-actions-item table__cell-actions-icon"> <span class="mdi mdi-table-edit" title="Sửa"></span> </a>' +
//            '<a ng-click="delete(\'' + full.LableId + '\')" class="table__cell-actions-item table__cell-actions-icon"><span class="mdi mdi-delete" title="Xóa"></span> </a>';
//    }));
//    vm.reloadData = reloadData;
//    vm.dtInstance = {};
//    function reloadData(resetPaging) {
//        $scope.staticParam.currentPage = resetPaging;
//        vm.dtInstance.reloadData(callback, resetPaging);
//    }
//    function callback(json) {

//    }
//    function toggleOne(selectedItems) {
//        for (var id in selectedItems) {
//            if (selectedItems.hasOwnProperty(id)) {
//                if (!selectedItems[id]) {
//                    vm.selectAll = false;
//                    return;
//                }
//            }
//        }
//        vm.selectAll = true;
//    }
//    function toggleAll(selectAll, selectedItems) {
//        for (var id in selectedItems) {
//            if (selectedItems.hasOwnProperty(id)) {
//                selectedItems[id] = selectAll;
//            }
//        }
//    }
//    function rowCallback(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
//        $('td', nRow).unbind('dblclick');
//        $('td', nRow).bind('dblclick', function () {
//            $scope.$apply(function () {
//                $scope.open(aData.LableId);
//            });
//        });
//        return nRow;

//    }
//    //Load dữ liệu cuộn
//    $scope.total = 0;
//    $scope.check = false;
//    $scope.LoadScroll = function (obj) {
//        var total = obj.offsetHeight + obj.scrollTop;
//        if (obj.offsetHeight + obj.scrollTop >= obj.scrollHeight && $scope.check && $scope.total < (total + 10)) {
//            $scope.check = false;
//            vm.dtInstance.DataTable.page('next').draw('page');
//        }
//        if (obj.offsetHeight + obj.scrollTop >= obj.scrollHeight) {
//            $scope.total = obj.offsetHeight + obj.scrollTop;
//        }
//        else {
//            $scope.check = true;
//        }
//    };
//    //tìm kiếm
//    $scope.reload = function () {
//        $rootScope.IsActive = $scope.staticParam.IsActive;
//        $scope.dtInstance.DataTable.search($scope.staticParam.Search);
//        $scope.dtInstance.DataTable.search($scope.staticParam.Search).draw();
//    };
//    $scope.reloadAll = function () {
//        $scope.staticParam.Search = "";
//        $scope.selectAll = false;
//        $scope.staticParam.IsActive = '1';
//        reloadData(true);
//        $scope.dtInstance.DataTable.search($scope.staticParam.Search);
//        $scope.dtInstance.DataTable.search($scope.staticParam.Search).draw();

//    };
//    $rootScope.Search = function () {
//        reloadData(true);
//    };
//    // thêm mới
//    $scope.add = function () {
//        var modalInstance = $uibModal.open({
//            animation: true,
//            templateUrl: ctxfolder + '/add.html',
//            controller: 'add',
//            backdrop: 'static',
//            keyboard: false,
//            size: '80',
//        });
//        modalInstance.result.then(function (d) {
//        }, function () {
//        });
//    };
//    // chi tiết
//    $scope.open = function (temp) {
//        var modalInstance = $uibModal.open({
//            animation: true,
//            templateUrl: ctxfolder + '/open.html',
//            controller: 'open',
//            backdrop: 'static',
//            keyboard: false,
//            size: '80',
//            resolve: {
//                para: function () {
//                    return temp;
//                }
//            }
//        });
//        modalInstance.result.then(function (d) {
//        }, function () {
//        });

//    };
//    // sửa
//    $scope.edit = function (temp) {
//        var modalInstance = $uibModal.open({
//            animation: true,
//            templateUrl: ctxfolder + '/edit.html',
//            controller: 'edit',
//            backdrop: 'static',
//            keyboard: false,
//            size: '80',
//            resolve: {
//                para: function () {
//                    return temp;
//                }
//            }
//        });
//        modalInstance.result.then(function (d) {
//        }, function () {
//        });

//    };
//    //Xóa 1 bản ghi
//    $scope.delete = function (temp) {
//        dataservice.getItem(temp, function (rs) {
//            ESConfirm([{ 'value': 1, 'title': 'Xác nhận', 'type': 'info' }, { 'value': 2, 'title': 'Đóng', 'type': 'danger' }], { 'description': 'Bạn có muốn xóa :' + rs.LableName + ' không?', 'class': 'eswarning_v2' }, function (rs) {
//                if (rs === '1') {
//                    App.blockUI({
//                        target: "#contentMain",
//                        boxed: true,
//                        message: 'Đang tải...'
//                    });
//                    dataservice.delete(temp, function (result) {
//                        if (result.Error) {
//                            App.notifyDanger(result.Title);
//                        } else {
//                            App.notifyInfo(result.Title);
//                            $rootScope.Search();
//                        }
//                        App.unblockUI("#contentMain");
//                    });
//                }
//            }, function () {
//            });
//        });
//    };
//    // xóa nhiều bản ghi
//    $scope.deleteChecked = function () {
//        var deleteItems = [];
//        for (var id in $scope.selected) {
//            if ($scope.selected.hasOwnProperty(id)) {
//                if ($scope.selected[id]) {
//                    deleteItems.push(id);
//                }
//            }
//        }
//        if (deleteItems.length > 0) {
//            ESConfirm([{ 'value': 1, 'title': 'Xác nhận', 'type': 'info' }, { 'value': 2, 'title': 'Đóng', 'type': 'danger' }], { 'description': 'Bạn có muốn xóa những bản ghi đã chọn không?', 'class': 'eswarning_v2' }, function (rs) {
//                if (rs === '1') {
//                    App.blockUI({
//                        target: "#contentMain",
//                        boxed: true,
//                        message: 'Đang tải...'
//                    });
//                    dataservice.deleteItems(deleteItems, function (result) {
//                        if (result.Error) {
//                            App.notifyDanger(result.Title);
//                        } else {
//                            App.notifyInfo(result.Title);
//                            $rootScope.Search();
//                        }
//                        App.unblockUI("#contentMain");
//                    });
//                }
//            }, function () {
//            });
//        } else {
//            App.notifyDanger("Không có bản ghi nào được chọn");
//        }
//    };
//    //thêm sưa xóa menu chuột phải
//    $scope.contextMenu = [
//        //xem chi tiết trong menu
//        [function ($itemScope) {
//            return '<i class="mdi mdi-eye"></i> Xem chi tiết';
//        }, function ($itemScope, $event, model) {
//            var modalInstance = $uibModal.open({
//                animation: true,
//                templateUrl: ctxfolder + '/open.html',
//                controller: 'open',
//                backdrop: 'static',
//                keyboard: false,
//                size: '80',
//                resolve: {
//                    para: function () {
//                        return $itemScope.data.LableId;
//                    }
//                }
//            });
//            modalInstance.result.then(function (d) {
//                $rootScope.Search();
//            }, function () {
//            });
//        }],
//        //sửa trong menu
//        [function ($itemScope) {
//            return '<i class="mdi mdi-table-edit"></i> Sửa';
//        }, function ($itemScope, $event, model) {
//            var modalInstance = $uibModal.open({
//                animation: true,
//                templateUrl: ctxfolder + '/edit.html',
//                controller: 'edit',
//                backdrop: 'static',
//                keyboard: false,
//                size: '80',
//                resolve: {
//                    para: function () {
//                        return $itemScope.data.LableId;
//                    }
//                }
//            });
//            modalInstance.result.then(function (d) {
//                $rootScope.Search();
//            }, function () {
//            });
//        }],
//        //xóa trong menu
//        [function ($itemScope) {
//            return '<i class="mdi mdi-delete"></i> Xóa ';
//        }, function ($itemScope, $event, model) {
//                ESConfirm([{ 'value': 1, 'title': 'Xác nhận', 'type': 'info' }, { 'value': 2, 'title': 'Đóng', 'type': 'danger' }], { 'description': 'Bạn có muốn xóa :' + $itemScope.data.LableName + ' không?', 'class': 'eswarning_v2' }, function (rs) {
//                if (rs === '1') {
//                    App.blockUI({
//                        target: "#contentMain",
//                        boxed: true,
//                        message: 'Đang tải...'
//                    });
//                    dataservice.delete($itemScope.data.LableId, function (result) {
//                        if (result.Error) {
//                            App.notifyDanger(result.Title);
//                        } else {
//                            App.notifyInfo(result.Title);
//                            $rootScope.Search();
//                        }
//                        App.unblockUI("#contentMain");
//                    });
//                }
//            }, function () {
//            });
//        }]
//    ];
//});
// controller Add
app.controller('index', function ($http, $scope, $timeout, $rootScope, $compile, $confirm, $ngConfirm, $uibModal, DTOptionsBuilder, DTColumnBuilder, DTInstances, dataservice, keyhandle) {
    $scope.model = {};
    var vm = $scope;
   
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    }
    var ctrl = $scope;
    ctrl.nextRow = function (d) {
        if (ctrl.ElementRowCheck === -1) {
            if (ctrl.allRow.length > 1) {
                ctrl.ElementRowCheck = ctrl.allRow[0].row;
                $('td', ctrl.ElementRowCheck).addClass("es-iSeleted");
                ctrl.iSelected = ctrl.allRow[0].data;
                ctrl.open_Detail(ctrl.iSelected.QuotationNo, ctrl.iSelected.CurrencyRate);

            }
        } else
            for (var i = 0; i < ctrl.allRow.length; i++) {
                if (ctrl.allRow[i].row === ctrl.ElementRowCheck) {
                    if (i === ctrl.allRow.length - 1) break;
                    $('td', ctrl.ElementRowCheck).removeClass('es-iSeleted');
                    ctrl.ElementRowCheck = ctrl.allRow[i + 1].row;
                    $('td', ctrl.ElementRowCheck).addClass("es-iSeleted");
                    ctrl.iSelected = ctrl.allRow[i + 1].data;
                    ctrl.open_Detail(ctrl.iSelected.QuotationNo, ctrl.iSelected.CurrencyRate);
                    break;
                }
            }
    }
    ctrl.upRow = function (d) {
        if (ctrl.ElementRowCheck === -1) {
            if (ctrl.allRow.length > 1) {
                ctrl.ElementRowCheck = ctrl.allRow[ctrl.allRow.length - 1].row;
                $('td', ctrl.ElementRowCheck).addClass("es-iSeleted");
                ctrl.iSelected = ctrl.allRow[ctrl.allRow.length - 1].row;
                ctrl.open_Detail(ctrl.iSelected.QuotationNo, ctrl.iSelected.CurrencyRate);
            }
        } else
            for (var i = 0; i < ctrl.allRow.length; i++) {
                if (ctrl.allRow[i].row === ctrl.ElementRowCheck) {
                    if (i === 0) break;
                    $('td', ctrl.ElementRowCheck).removeClass('es-iSeleted');
                    ctrl.ElementRowCheck = ctrl.allRow[i - 1].row;
                    $('td', ctrl.ElementRowCheck).addClass("es-iSeleted");
                    ctrl.iSelected = ctrl.allRow[i - 1].data;
                    ctrl.open_Detail(ctrl.iSelected.QuotationNo, ctrl.iSelected.CurrencyRate);
                    break;
                }
            }
    }
    keyhandle.eventCtrl(40, ctrl.nextRow);
    keyhandle.eventCtrl(38, ctrl.upRow);
    ctrl.ElementRowCheck = -1;
    ctrl.allRow = [];
    ctrl.iSelected = {};
    ctrl.dataScrollPage = [];
    var today = new Date();
    var day = today.getDate();
    var month = today.getMonth() + 1;
    var yyyy = today.getFullYear();

    $scope.staticParam = {
        totalItems: 0,
        currentPage: 1,
        pageSize: 15,
        maxSize: 5,
        Title: "",
        lstDepartmentID: [],
        Day: "" + day,
        Month: "" + month,
        Years: "" + yyyy,
        DepartmentID: [],
        TimeKeepingGuid: null,
        EmployeeName: null,
        IPMachine: ""
    };
    $scope.txtComment = "";
    $scope.dataload = [];
    $scope.CheckReload = false;
    $scope.lstInt = [];
    $rootScope.TongGT = 0;
    $scope.selected = [];
    $scope.selectAll = false;
    $scope.toggleAll = toggleAll;
    $scope.toggleOne = toggleOne;
    var titleHtml = '<label class="mt-checkbox"><input type="checkbox" ng-model="selectAll" ng-click="toggleAll(selectAll, selected)"/><span></span></label>';

    vm.dtOptions = DTOptionsBuilder.newOptions()
        .withOption('serverSide', true)
        .withOption('order', [4, 'asc'])
        .withDataProp('data')
        .withOption('pageLength', 25)
        .withOption('initComplete', function (settings, json) {
            //chỉ dùng khi có footer
            //ẩn scroll-x body
            $('.dataTables_scrollBody').css({
                'overflow-y': 'scroll',
                'overflow-x': 'hidden',
                'border': 'none'
            });
            //hiền scroll footer
            $('#tblData_wrapper .dataTables_scrollFoot').css('overflow', 'auto');
            //cuộn toàn bảng theo footer
            $('#tblData_wrapper .dataTables_scrollFoot').on('scroll', function () {
                $('#tblData_wrapper .dataTables_scrollBody').scrollLeft($(this).scrollLeft());
            });

            //thêm sự kiện scroll phân trang
            $('#tblData').parent().attr("onscroll", "angular.element(this).scope().LoadScroll(this)");
        })
        .withOption('searching', false)
        .withOption('createdRow', function (row, data, dataIndex) {
            const contextScope = $scope.$new(true);
            contextScope.data = data;
            $scope.data = data;
            var obj = {
                STT: data._STT.toString(),
                Id: data.CandidateGuid
            }
            $scope.lstInt.push(obj);
            $compile(row)($scope);
            contextScope.contextMenu = $scope.contextMenu;
            $compile(angular.element(row).find('input'))($scope);
            $compile(angular.element(row).attr('context-menu', 'contextMenu'))(contextScope);
        });
    vm.dtOptions.withOption('ajax', function (data, callback, settings) {
        data.Title = $scope.staticParam.Title;
        data.Day = $scope.staticParam.Day !== "" ? parseInt($scope.staticParam.Day) : null;
        data.Month = $scope.staticParam.Month !== "" ? parseInt($scope.staticParam.Month) : null;
        data.Years = $scope.staticParam.Years !== "" ? parseInt($scope.staticParam.Years) : null;
        data.DepartmentID = $scope.staticParam.DepartmentID;
        data.Timekeeper = $scope.staticParam.Timekeeper !== "" && $scope.staticParam.Timekeeper !== "null" ? $scope.staticParam.Timekeeper : null;

        /* data.IPMachine = $scope.staticParam.IPMachine; */
        $scope.staticParam.currentPage = $scope.dtInstance.DataTable.page() + 1;
        if ($scope.CheckReload) {
            data.Start = 0;
        }
        App.blockUI({
            target: "#table_load",
            boxed: true,
            message: 'Đang tải...'
        });
        $http.post('/Lables/JTableLables', data)
            .success(function (res) {
                if (data.start === 0) {
                    $scope.dataload = [];
                    angular.forEach(res.data, function (val, key) {
                        $scope.dataload.push(val);
                    });
                    callback({
                        data: $scope.dataload,
                        recordsTotal: res.recordsTotal,
                        recordsFiltered: res.recordsFiltered
                    });
                }
                else if (res.recordsTotal > $scope.dataload.length) {
                    angular.forEach(res.data, function (val, key) {
                        $scope.dataload.push(val);
                    });

                    callback({
                        data: $scope.dataload,
                        recordsTotal: res.recordsTotal,
                        recordsFiltered: res.recordsFiltered
                    });
                }
                App.unblockUI("#table_load");

            });
    });
    //scroll theo tỷ lệ màn hình
    vm.dtOptions.withOption('scrollY', '40vh')
        .withOption('scrollX', '100%') //mặc định 100%
        .withOption('scrollCollapse', true) //xóa khoảng trắng khi dữ liệu trong bảng ít
        .withOption('rowCallback', rowCallback) //sự kiên click vào 1 dòng
        .withOption('scroller', {
            loadingIndicator: true
        }).withFixedColumns({
            leftColumns: 0 //fix bên trái 2 cột
            //rightColumns: 1 //fix bên phải 1 cột
        });

    vm.dtColumns = [];

    vm.dtColumns.push(DTColumnBuilder.newColumn('_STT').withTitle('STT').notSortable().withOption('sClass', 'tcenter').withOption('sWidth', '5px').renderWith(function (data, type, full, meta) {
        return meta.row + 1;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('EmployeeId').withTitle('Mã NV').withOption('sWidth', '90px').renderWith(function (data, type, full, meta) {
        return data;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('EmployeeName').withOption('sWidth', '100px').withTitle('Tên NV').renderWith(function (data, type, full, meta) {
        return data;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('DepartmentName').withTitle('Phòng ban').withOption('sWidth', '140px').renderWith(function (data, type, full, meta) {
        return data;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('DateCheck').withTitle('Ngày').withOption('sWidth', '60px').renderWith(function (data, type, full, meta) {
        if (moment(data).isValid()) {
            return moment(data).format('DD/MM/YYYY');
        } else { return ''; }

    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('OrderId').withTitle('Thứ').withOption('sWidth', '30px').renderWith(function (data, type, full, meta) {
        return data;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('CheckIn').withTitle('Giờ vào').withOption('sWidth', '60px').renderWith(function (data, type, full, meta) {
        return data;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('CheckOut').withTitle('Giờ ra').withOption('sWidth', '60px').renderWith(function (data, type, full, meta) {
        return data;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('CheckInCheckDC').withTitle('Giờ vào TC').withOption('sWidth', '60px').renderWith(function (data, type, full, meta) {
        return data;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('CheckOutCheckDC').withTitle('Giờ ra TC').withOption('sWidth', '60px').renderWith(function (data, type, full, meta) {
        return data;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('MinutesLate').withTitle('Trễ').withOption('sWidth', '60px').renderWith(function (data, type, full, meta) {
        return $rootScope.addPeriod(data);
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('MinutesEarly').withTitle('Sớm').withOption('sWidth', '60px').renderWith(function (data, type, full, meta) {
        return $rootScope.addPeriod(data);
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('WorkedDay').withTitle('Công').withOption('sWidth', '40px').renderWith(function (data, type, full, meta) {
        return $rootScope.addPeriod(data);
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('HoursWork').withTitle('T.giờ').withTitleDescription('Tổng giờ').withOption('sWidth', '40px').renderWith(function (data, type, full, meta) {
        return $rootScope.addPeriod(data);
    }));

    vm.dtColumns.push(DTColumnBuilder.newColumn('OvertimeDay').withTitle('Giờ LT').withTitleDescription('Giờ làm thêm').withOption('sWidth', '40px').renderWith(function (data, type, full, meta) {
        return $rootScope.addPeriod(data);
    }));

    vm.dtColumns.push(DTColumnBuilder.newColumn('ShiftId').withTitle('Ca').withOption('sWidth', '60px').renderWith(function (data, type, full, meta) {
        return full.ShiftName1;
    }));


    vm.reloadData = reloadData;
    vm.dtInstance = {};
    function reloadData(resetPaging) {
        $scope.staticParam.currentPage = resetPaging;
        vm.dtInstance.reloadData(callback, resetPaging);
    }
    function callback(json) {

    }
    function toggleAll(selectAll, selectedItems) {
        for (var id in selectedItems) {
            if (selectedItems.hasOwnProperty(id)) {
                selectedItems[id] = selectAll;
            }
        }
    }
    function toggleOne(selectedItems) {
        for (var id in selectedItems) {
            if (selectedItems.hasOwnProperty(id)) {
                if (!selectedItems[id]) {
                    vm.selectAll = false;
                    return;
                }
            }
        }
        vm.selectAll = true;
    }
    $scope.total = 0;
    $scope.check = false;
    $scope.LoadScroll = function (obj) {
        var total = obj.offsetHeight + obj.scrollTop;
        if (obj.offsetHeight + obj.scrollTop >= obj.scrollHeight && $scope.check && $scope.total <= total) {
            $scope.check = false;
            try {
                $scope.CheckReload = false;
                vm.dtInstance.DataTable.page('next').draw('page');
            }
            catch (ex) {
            }
        }
        if (obj.offsetHeight + obj.scrollTop >= obj.scrollHeight) {
            $scope.total = obj.offsetHeight + obj.scrollTop;
        }
        else {
            $scope.check = true;
        }

    };

    $('#minize-menuleft').click(function () {
        vm.dtInstance.DataTable.draw();
    });
    function rowCallback(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        if (iDisplayIndexFull === 0) ctrl.allRow = [];
        $('td', nRow).unbind('click');
        $('td', nRow).bind('click', function () {
            if (vm.ElementRowCheck === nRow) return;
            $('td', nRow).addClass("es-iSeleted");
            $('td', vm.ElementRowCheck).removeClass('es-iSeleted');
            vm.ElementRowCheck = nRow;
            ctrl.ElementRowCheck = nRow;
            ctrl.iSelected = aData;
            vm.staticParam.TimeKeepingGuid = aData.TimeKeepingGuid;
            vm.staticParam.EmployeeName = aData.EmployeeName;

        });
        $('td', nRow).unbind('dblclick');
        $('td', nRow).bind('dblclick', function () {
            $scope.$apply(function () {
                $scope.open(aData.CandidateGuid);
            });
        });
        ctrl.allRow.push({ row: nRow, data: aData });
        return nRow;
    }

    $rootScope.reloadData = function () {
        $scope.dataload = [];
        reloadData(true);
    }

    $scope.deleteAll = function () {
        ESConfirm([{ 'value': 1, 'title': 'Xác nhận', 'type': 'info' }, { 'value': 2, 'title': 'Đóng', 'type': 'danger' }], { 'description': 'Bạn có chắc chắn muốn xóa không?', 'class': 'eswarning_v2' }, function (rs) {
            if (rs === '1') {
                App.blockUI({
                    target: "#contentMain",
                    boxed: true,
                    message: 'Đang tải...'
                });
                var obj = {
                    Month: $scope.staticParam.Month,
                    Years: $scope.staticParam.Years,
                    IPMachine: $scope.staticParam.IPMachine
                }
                dataservice.deleteAll(obj, function (result) {
                    if (result.Error) {
                        App.notifyDanger(result.Title);
                        App.unblockUI("#contentMain");
                    } else {
                        App.notifyInfo(result.Title);
                        $rootScope.reloadData();
                        App.unblockUI("#contentMain");
                    }

                });
            }
        });
    }
    $scope.DepartmentIDConfig = {
        placeholder: 'Chọn phòng ban',
        selectAll: true,
        search: true,
        searchText: 'Tìm kiếm...',
        showTitle: 'true',
        csvSepChar: ',',
        captionFormat: '{0} lựa chọn',
        captionFormatAllSelected: '{0} lựa chọn',
        noMatch: 'Không có dữ liệu "{0}"',
        okCancelInMulti: true,
        locale: ['Đồng ý', 'Hủy', 'Chọn tất cả']
    };
    $scope.DepartmentIDChange = function (rs) {
        $scope.staticParam.lstDepartmentID = rs;
        $scope.dataload = [];
        reloadData(true);
        $scope.$apply();

    }
    $scope.listTimekeepChange = function (rs) {
        $scope.staticParam.Timekeeper = rs;
        $scope.dataload = [];
        reloadData(true);
    }
    $scope.add = function () {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/add.html',
            controller: 'add',
            backdrop: true,
            size: '80'
        });
        modalInstance.result.then(function (d) {
            $rootScope.reloadData();
        }, function () {
        });
    }
    $scope.deleteChecked = function () {
        var deleteItems = [];
        for (var id in $scope.selected) {
            if ($scope.selected.hasOwnProperty(id)) {
                if ($scope.selected[id]) {
                    deleteItems.push(id);
                }
            }
        }
        if (deleteItems.length > 0) {
            ESConfirm([{ 'value': 1, 'title': 'Xác nhận', 'type': 'info' }, { 'value': 2, 'title': 'Đóng', 'type': 'danger' }], { 'description': 'Bạn có chắc chắn muốn xóa không?', 'class': 'eswarning_v2' }, function (rs) {
                if (rs === '1') {
                    App.blockUI({
                        target: "#contentMain",
                        boxed: true,
                        message: 'Đang tải...'
                    });
                    dataservice.deleteItems(deleteItems, function (result) {
                        if (result.Error) {
                            App.notifyDanger(result.Title);
                        } else {
                            App.notifyInfo(result.Title);
                            $rootScope.reloadData();
                        }
                        App.unblockUI("#contentMain");
                    });
                }
            });
        } else {
            App.notifyDanger("Không có khoản mục nào được chọn");
        }
    }

    $scope.DayConfig = {
        placeholder: 'Chọn ngày',
        search: true
    };
    $scope.MonthConfig = {
        search: true
    };
    $scope.YearsConfig = {
        search: true
    };
    $scope.reloadALL = function () {
        $scope.staticParam.Title = "";
        $scope.staticParam.Day = "" + day;
        $scope.staticParam.Month = "" + month;
        $scope.staticParam.Years = "" + yyyy;
        $scope.staticParam.lstDepartmentID = [];
        $rootScope.reloadData();
    }
    $scope.DepartmentIDChange = function (data) {
        $scope.staticParam.DepartmentID = data !== 'null' ? data : "";
        reloadData(true);
    }
    $scope.DayChange = function (data) {
        $scope.staticParam.Day = data !== 'null' ? data : "";
        reloadData(true);
    }
    $scope.MonthChange = function (data) {
        $scope.staticParam.Month = data !== 'null' ? data : "";
        reloadData(true);
    }
    $scope.YearsChange = function (data) {
        $scope.staticParam.Years = data !== 'null' ? data : "";
        reloadData(true);
    }

    $scope.contextMenu = [
        [function ($itemScope) {
            return '<i class="mdi mdi-eye"></i> Xem thông tin';
        }, function ($itemScope, $event, model) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: ctxfolder + '/open.html',
                controller: 'open',
                backdrop: 'static',
                size: '100',
                resolve: {
                    para: function () {
                        return $itemScope.data.TimeKeepingGuid;
                    }
                }
            });
            modalInstance.result.then(function (d) {
                $rootScope.reloadData();
            }, function () {
            });
        }, function ($itemScope, $event, model) {
            return App.Permissions.OPEN;
        }],
        [function ($itemScope) {
            return '<i class="mdi mdi-table-edit"></i> Sửa thông tin';
        }, function ($itemScope, $event, model) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: ctxfolder + '/edit.html',
                controller: 'edit',
                backdrop: 'static',
                size: '100',
                resolve: {
                    para: function () {
                        return $itemScope.data.TimeKeepingGuid;
                    }

                }
            });
            modalInstance.result.then(function (d) {
                $rootScope.reloadData();
            }, function () {
            });
        }, function ($itemScope, $event, model) {
            return App.Permissions.EDIT;
        }],
        [function ($itemScope) {
            return '<i class="mdi mdi-delete"></i> Xóa thông tin';
        }, function ($itemScope, $event, model) {
            ESConfirm([{ 'value': 1, 'title': 'Xác nhận', 'type': 'info' }, { 'value': 2, 'title': 'Đóng', 'type': 'danger' }], { 'description': '[' + $itemScope.data.EmployeeName + '] ?', 'class': 'eswarning' }, function (rs) {
                if (rs === '1') {

                    App.blockUI({
                        target: "#contentMain",
                        boxed: true,
                        message: 'Đang tải...'
                    });
                    dataservice.delete($itemScope.data.TimeKeepingGuid, function (result) {
                        if (result.Error) {
                            App.notifyDanger(result.Title);
                        } else {
                            App.notifyInfo(result.Title);
                            $rootScope.reloadData();
                        }
                        App.unblockUI("#contentMain");
                    });
                }
            });
        }, function ($itemScope, $event, model) {
            return App.Permissions.DELETE;
        }]

    ];

    $scope.getTimeKeepings = function () {
        App.blockUI({
            target: "#contentMain",
            boxed: true,
            message: 'Đang tải...'
        });
        var obj = {
            "Ip": "113.181.39.102",
            "port": 4371
        }
        dataservice.getTimeKeepings(obj, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            }
            else {
                $scope.lstTimeKeepings = rs;
            }
            App.unblockUI("#contentMain");
        })
    }
    // export excel
    $scope.exportExcel = function () {
        App.blockUI({
            target: "#contentMain",
            boxed: true,
            message: 'Đang tải dữ liệu...'
        });

        var obj = {
            Title: $scope.staticParam.Title,
            lstDepartmentID: $scope.staticParam.lstDepartmentID,
            Month: $scope.staticParam.Month !== "" ? parseInt($scope.staticParam.Month) : null,
            Years: $scope.staticParam.Years !== "" ? parseInt($scope.staticParam.Years) : null
        }
        dataservice.exportExcel(obj, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
                App.unblockUI("#contentMain");
                return;
            }
            else {
                window.location.href = rs.Title;
                App.unblockUI("#contentMain");
            }
        });
    }
    // export excel
    $scope.exportTimeKeepings = function () {
        App.blockUI({
            target: "#contentMain",
            boxed: true,
            message: 'Đang tải...'
        });

        dataservice.printExcel($rootScope.staticParam, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            }
            else {
                window.location.href = rs.Title;
            }
            App.unblockUI("#contentMain");
        })
    }
    // exportExcel2
    $scope.exportExcel2 = function () {


        var departmentGuid = '';
        var ListDepartmentId = '';
        angular.forEach($rootScope.ListDepartment, function (val, key) {
            if (val.DepartmentId === $scope.staticParam.DepartmentID) {
                departmentGuid = val.DepartmentGuid;
            }
        });
        if ($scope.staticParam.DepartmentID !== null && $scope.staticParam.DepartmentID !== undefined && $scope.staticParam.DepartmentID !== '') {
            ListDepartmentId = $scope.staticParam.DepartmentID.join();
        }

        var obj = {
            KeyWords: $scope.staticParam.Title,
            DepartmentGuid: departmentGuid,
            ListDepartmentId: ListDepartmentId,
            Year: $scope.staticParam.Years !== "" ? parseInt($scope.staticParam.Years) : null,
            Month: $scope.staticParam.Month !== "" ? parseInt($scope.staticParam.Month) : null
        }
        App.blockUI({
            target: "#contentMain",
            boxed: true,
            message: 'Đang tải...'
        });
        dataservice.exportExcel2(obj, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
                App.unblockUI("#contentMain");
                return;
            }
            else {
                window.location.href = rs.Title;
                App.unblockUI("#contentMain");
            }
        });
    }
    $scope.xemDiemdanh = function () {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/indexEmployee.html',
            controller: 'checkEmployee',
            backdrop: 'static',
            size: '100',
            resolve: {
                para: function () {
                    return $scope.staticParam.Month !== "" ? parseInt($scope.staticParam.Month) : null;
                },
                para1: function () {
                    return $scope.staticParam.Years !== "" ? parseInt($scope.staticParam.Years) : null;
                },
                para2: function () {
                    return $scope.staticParam.Day !== "" ? parseInt($scope.staticParam.Day) : null;
                }
            }
        });
    }
    // export excel of day
    $scope.exportExcelOfDay = function () {
        App.blockUI({
            target: "#contentMain",
            boxed: true,
            message: 'Đang tải dữ liệu...'
        });

        var obj = {
            Title: $scope.staticParam.Title,
            lstDepartmentID: $scope.staticParam.lstDepartmentID,
            Month: $scope.staticParam.Month !== "" ? parseInt($scope.staticParam.Month) : null,
            Years: $scope.staticParam.Years !== "" ? parseInt($scope.staticParam.Years) : null
        }
        dataservice.exportExcelOfDay(obj, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
                App.unblockUI("#contentMain");
                return;
            }
            else {
                window.location.href = rs.Title;
                App.unblockUI("#contentMain");
            }
        });
    }
    $scope.listOrganizationChange = function (rs) {
        $scope.staticParam.IPMachine = rs != "null" && rs != "" ? rs : "";
        $scope.dataload = [];
        reloadData(true);
        $scope.$apply();
    }

    // danh sách chi tiết
    $scope.selected_hhdv = [];
    $scope.selectAll_hhdv = false;
    $scope.toggleAll_hhdv = toggleAll_hhdv;
    $scope.toggleOne_hhdv = toggleOne_hhdv;
    $scope.dataload_hhdv = [];
    $scope.staticParam_hhdv = {
        totalItems: 0,
        currentPage: 1,
        pageSize: 15,
        maxSize: 5,
        SearchWards: "",
        IsActive: 0,
        QuotationNo: "",
        Start: 0

    };

    $scope.CheckReload_hhdv = false;
    $rootScope.TongSL = 0; $rootScope.TongThanhtien = 0; $rootScope.TongThue = 0; $rootScope.TongChietkhau = 0; $rootScope.TongTienTT = 0;
    $scope.lstInt_hhdv = [];
    // var titleHtml = '<label class="mt-checkbox"><input type="checkbox" ng-model="selectAll_hhdv" ng-click="toggleAll_hhdv(selectAl_hhdvl, selected_hhdv)"/><span></span></label>';

    vm.dtOptions_hhdv = DTOptionsBuilder.newOptions()
        .withOption('serverSide', true)
        .withOption('order', [7, 'desc'])
        .withDataProp('data')
        .withOption('pageLength', 15)
        .withOption('initComplete', function (settings, json) {
            //chỉ dùng khi có footer
            //ẩn scroll-x body
            $('.dataTables_scrollBody').css({
                'overflow-y': 'scroll',
                'overflow-x': 'hidden',
                'border': 'none'
            });
            //hiền scroll footer
            $('#tblData_hhdv_wrapper .dataTables_scrollFoot').css('overflow', 'auto');
            //cuộn toàn bảng theo footer
            $('#tblData_hhdv_wrapper .dataTables_scrollFoot').on('scroll', function () {
                $('#tblData_hhdv_wrapper .dataTables_scrollBody').scrollLeft($(this).scrollLeft());
            });

            //thêm sự kiện scroll phân trang
            $('#tblData_hhdv').parent().attr("onscroll", "angular.element(this).scope().LoadScroll_hhdv(this)");
        })
        .withOption('searching', false)
        .withOption('createdRow', function (row, data, dataIndex) {
            const contextScope = $scope.$new(true);
            contextScope.data = data;
            $scope.data = data;
            $compile(row)($scope);
            contextScope.contextMenu = $scope.contextMenu;
            $compile(angular.element(row).find('input'))($scope);
            $compile(angular.element(row).attr('context-menu', 'contextMenu'))(contextScope);
        });
    if (App.Permissions.LISTVIEW)
        vm.dtOptions_hhdv.withOption('ajax', function (data, callback, settings) {
            //data.EmployeeGuid = vm.staticParam.EmployeeGuid;
            //data.FullName = vm.staticParam.FullName;
            if ($scope.CheckReload_hhdv) {
                data.start = 0;
            }

            $http.post('/Lables/JTableLables', data)
                .success(function (res) {
                    if (data.start === 0) {
                        $scope.dataload_hhdv = [];
                        angular.forEach(res.data, function (val, key) {
                            $scope.dataload_hhdv.push(val);
                        });
                        callback({
                            data: $scope.dataload_hhdv,
                            recordsTotal: res.recordsTotal,
                            recordsFiltered: res.recordsFiltered
                        });
                    }
                    else {
                        if (res.recordsTotal > $scope.dataload_hhdv.length) {
                            angular.forEach(res.data, function (val, key) {
                                $scope.dataload_hhdv.push(val);
                            });
                            callback({
                                data: $scope.dataload_hhdv,
                                recordsTotal: res.recordsTotal,
                                recordsFiltered: res.recordsFiltered
                            });
                        }
                    }


                });
        });
    //scroll theo tỷ lệ màn hình
    vm.dtOptions_hhdv.withOption('scrollY', '30vh')
        .withOption('scrollX', '100%') //mặc định 100%
        .withOption('scrollCollapse', true) //xóa khoảng trắng khi dữ liệu trong bảng ít
        .withOption('rowCallback', rowCallback2) //sự kiên click vào 1 dòng
        .withOption('scroller', {
            loadingIndicator: true
        }).withFixedColumns({
            leftColumns: 0 //fix bên trái 2 cột
            //rightColumns: 1 //fix bên phải 1 cột
        });

    vm.dtColumns_hhdv = [];

    vm.dtColumns_hhdv.push(DTColumnBuilder.newColumn('_STT').withTitle('STT').notSortable().withOption('sWidth', '4px').withOption('sClass', 'tcenter').renderWith(function (data, type, full, meta) {
        return meta.row + 1;
    }));
    vm.dtColumns_hhdv.push(DTColumnBuilder.newColumn('ContractNo').withTitle('Số hợp đồng').withOption('sClass', 'tcenter').withOption('sWidth', '150px').renderWith(function (data, type, full, meta) {
        return '<a  style="color: #4285f4;" href="" ng-click="open(\'' + full.ContractGuid + '\')" >' + data + '</a>';
    }));
    vm.dtColumns_hhdv.push(DTColumnBuilder.newColumn('Salary').withTitle('Mức lương').withTitleDescription('Mức lương').withOption('sClass', 'tcenter').withOption('sWidth', '150px').renderWith(function (data, type) {
        if (data !== null && data !== "") {
            return $rootScope.addPeriod(data);
        } else {
            return "";
        }
    }));
    vm.dtColumns_hhdv.push(DTColumnBuilder.newColumn('ContractType').withTitle('Loại HĐ').withTitleDescription('Loại hợp đồng').withOption('sWidth', '150px').renderWith(function (data, type) {
        if (data === "N") return "Không xác định thời hạn";
        if (data === "Y") return "Xác định thời hạn";
        if (data === "T") return "Hợp đồng thời vụ";
        if (data === "D") return "Hợp đồng đào tạo";
        if (data === "K") return "Khác";
        return "";

    }));
    vm.dtColumns_hhdv.push(DTColumnBuilder.newColumn('ContractDate').withTitle('Ngày ký HĐ').withTitleDescription('Ngày ký hợp đồng').withOption('sClass', 'tcenter').withOption('sWidth', '80px').renderWith(function (data, type) {
        if (data !== null && data !== "") {
            return data;
        } else {
            return "";
        }
    }));
    vm.dtColumns_hhdv.push(DTColumnBuilder.newColumn('ContractExpired').withTitle('Hạn hợp đồng').withOption('sClass', 'tcenter').withOption('sWidth', '80px').renderWith(function (data, type) {
        if (data !== null && data !== "") {
            return data;
        } else {
            return "";
        }
    }));
    vm.dtColumns_hhdv.push(DTColumnBuilder.newColumn('Description').withTitle('Mô tả').withOption('sClass', 'tleft').renderWith(function (data, type, full, meta) {
        return data;
    }));
    vm.dtColumns_hhdv.push(DTColumnBuilder.newColumn('IsActive').withTitle('Trạng thái').withOption('sClass', 'tcenter').withOption('sWidth', '110px').renderWith(function (data, type) {
        if (data === "True") {
            return "<label class='mt-checkbox'><input type='checkbox' readonly checked ng-disabled='true'/><span readonly></span></label>";
        } else if (data === "False") {
            return "<label class='mt-checkbox'><input type='checkbox' readonly ng-disabled='true'/><span readonly></span></label>";
        }
        else {
            return "";
        }
    }));
    vm.dtColumns_hhdv.push(DTColumnBuilder.newColumn('ContractGuid').withTitle('HĐ TV').withTitleDescription('In hợp đồng lao động thử việc').withOption('sWidth', '40px').withOption('sClass', 'tcenter').renderWith(function (data, type, full, meta) {
        return '<a href="" ng-click="printclick(\'' + full.ContractGuid + '\',1)" title="In Hợp đồng"><i class="btn-icon mdi mdi-printer"></i></a>';
    }));
    vm.dtColumns_hhdv.push(DTColumnBuilder.newColumn('ContractGuid').withTitle('HĐ CT').withTitleDescription('In hợp đồng lao động chính thức').withOption('sWidth', '40px').withOption('sClass', 'tcenter').renderWith(function (data, type, full, meta) {
        return '<a href="" ng-click="printclick(\'' + full.ContractGuid + '\',0)" title="In Hợp đồng"><i class="btn-icon mdi mdi-printer"></i></a>';

    }));

    vm.reloadData_hhdv = reloadData_hhdv;
    vm.dtInstance_hhdv = {};
    function reloadData_hhdv(resetPaging) {
        //$scope.staticParam_hhdv.currentPage = resetPaging;
        $scope.CheckReload_hhdv = true;
        $scope.dataload_hhdv = [];
        $scope.total_hhdv = 0;
        $scope.check_hhdv = false;
        vm.dtInstance_hhdv.reloadData(callback_hhdv, resetPaging);
        vm.dtInstance_hhdv.DataTable.draw();
    }
    function callback_hhdv(json) {

    }
    function toggleAll_hhdv(selectAll, selectedItems) {
        for (var id in selectedItems) {
            if (selectedItems.hasOwnProperty(id)) {
                selectedItems[id] = selectAll;
            }
        }
    }
    function toggleOne_hhdv(selectedItems) {
        for (var id in selectedItems) {
            if (selectedItems.hasOwnProperty(id)) {
                if (!selectedItems[id]) {
                    vm.selectAll_hhdv = false;
                    return;
                }
            }
        }
        vm.selectAll_hhdv = true;
    }
    $scope.total_hhdv = 0;
    $scope.check_hhdv = false;
    $scope.LoadScroll_hhdv = function (obj) {
        var total = obj.offsetHeight + obj.scrollTop;
        if (obj.offsetHeight + obj.scrollTop >= obj.scrollHeight && $scope.check_hhdv && $scope.total_hhdv < total) {
            $scope.check_hhdv = false;
            $scope.CheckReload_hhdv = false;
            try {
                vm.dtInstance_hhdv.DataTable.page('next').draw('page');
            }
            catch (ex) {
            }
        }
        if (obj.offsetHeight + obj.scrollTop >= obj.scrollHeight) {
            $scope.total_hhdv = obj.offsetHeight + obj.scrollTop;
        }
        else {
            $scope.check_hhdv = true;
        }
    };

    //Hàm trả về khi click vào 1 dòng
    function rowCallback2(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        $('td', nRow).unbind('dblclick');
        $('td', nRow).bind('dblclick', function () {
            $scope.$apply(function () {
                //$scope.open(aData.BankGuid);
            });
        });
        return nRow;
    }

    $scope.reload_hhdv = function () {
        $scope.dataload_hhdv = [];

        reloadData_hhdv(true);

    };
    $scope.reloadALL_hhdv = function () {
        $scope.staticParam_hhdv.Search = "";
        $scope.model_hhdv.IsActive = 1;
        $scope.dataload_hhdv = [];
        reloadData_hhdv(true);
    };

    $scope.contextMenu = [
        [function ($itemScope) {
            return '<i class="mdi mdi-eye"></i> Xem hợp đồng';
        }, function ($itemScope, $event, model) {
            $scope.open($itemScope.data.ContractGuid);
        }, function ($itemScope, $event, model) {
            return App.Permissions.OPEN;
        }],
        [function ($itemScope) {
            return '<i class="mdi mdi-table-edit"></i> Sửa hợp đồng';
        }, function ($itemScope, $event, model) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: ctxfolder + '/edit.html',
                controller: 'edit',
                backdrop: 'static',
                size: '100',
                resolve: {
                    para: function () {
                        return $itemScope.data.ContractGuid;
                    }, para1: function () {
                        return $itemScope.data.ContractGuid;
                    },
                    TitleIndex: function () {
                        return "Sửa hợp đồng nhân sự  : " + vm.staticParam.FullName;
                    }
                }
            });
            modalInstance.result.then(function (d) {
                $scope.reload_hhdv();
            }, function () {
            });
        }, function ($itemScope, $event, model) {
            return App.Permissions.EDIT;
        }],
        [function ($itemScope) {
            return '<i class="mdi mdi-delete"></i> Xóa hợp đồng';
        }, function ($itemScope, $event, model) {
            ESConfirm([{ 'value': 1, 'title': 'Xác nhận', 'type': 'info' }, { 'value': 2, 'title': 'Đóng', 'type': 'danger' }], { 'description': '[' + $itemScope.data.ContractNo + '] ?', 'class': 'eswarning' }, function (rs) {
                if (rs === '1') {

                    App.blockUI({
                        target: "#contentMain",
                        boxed: true,
                        message: 'Đang tải...'
                    });
                    dataservice.deleteContracts($itemScope.data.ContractGuid, function (result) {
                        if (result.Error) {
                            App.notifyDanger(result.Title);
                        } else {
                            App.notifyInfo(result.Title);
                            $scope.reload_hhdv();
                        }
                        App.unblockUI("#contentMain");
                    });
                }
            });
        }, function ($itemScope, $event, model) {
            return App.Permissions.DELETE;
        }],
        [function ($itemScope) {
            return '<i class="mdi mdi-attachment"></i> Đính kèm';
        }, function ($itemScope, $event, model) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: ctxfolder + '/addAttchment.html',
                controller: 'addattchment',
                backdrop: 'static',
                size: '70',
                resolve: {
                    para: function () {
                        return $itemScope.data.ContractGuid;
                    },
                    para1: function () {
                        return 1;
                    }
                }
            });
            modalInstance.result.then(function (d) {
            }, function () {
            });
        }, function ($itemScope, $event, model) {
            return true;
        }]
    ];








});

app.controller('add', function ($scope, $rootScope, $compile, $uibModal, $confirm, $uibModalInstance, FileUploader, dataservice) {
    $scope.Title = "Thêm mới";
    $scope.model = { IsActive: $rootScope.IsActive };
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
    $scope.ChangeIsActive = function (data) {
        $scope.model.IsActive = parseInt(data);
    };

    $scope.submit = function () {
        if ($scope.addform.validate()) {
            var fd = new FormData();
            fd.append('submit', JSON.stringify($scope.model));
            $.ajax({
                type: "POST",
                url: "/Lables/Submit",
                contentType: false,
                processData: false,
                data: fd,
                success: function (rs) {
                    if (rs.Error) {
                        App.notifyDanger(rs.Title);
                    } else {
                        App.notifyInfo(rs.Title);
                        $uibModalInstance.close();
                        $rootScope.Search();
                    }
                }
            });
        }
    };
});
// controller edit
app.controller('edit', function ($scope, $rootScope, $compile, $uibModal, $confirm, $uibModalInstance, dataservice, para) {
    $scope.Title = "Sửa";
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
    $scope.ChangeIsActive = function (data) {
        $scope.model.IsActive = parseInt(data);
    };
    $scope.initData = function () {
        dataservice.getItem(para, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {
                $scope.model = rs;
                $scope.model.Employees = null;
                if ($scope.model.IsActive === true) {
                    $scope.model.IsActive = 1;
                } else {
                    $scope.model.IsActive = 0;
                }
            }
        });
    };
    $scope.initData();
    $scope.submit = function () {
        if ($scope.editform.validate()) {
            var fd = new FormData();
            fd.append('edit', JSON.stringify($scope.model));
            $.ajax({
                type: "POST",
                url: "/Lables/Edit",
                contentType: false,
                processData: false,
                data: fd,
                success: function (rs) {
                    if (rs.Error) {
                        App.notifyDanger(rs.Title);
                    } else {
                        App.notifyInfo(rs.Title);
                        $uibModalInstance.close();
                        $rootScope.Search();
                    }
                }
            });
        }
    };
});
// controller open
app.controller('open', function ($scope, $rootScope, $compile, $uibModal, $confirm, $uibModalInstance, dataservice, para) {
    $scope.Title = "Xem chi tiết";
    $scope.model = {};
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
    $scope.loadData = function () {

        dataservice.getItem(para, function (rs) {
            $scope.model = rs;

            if ($scope.model.IsActive === true) {
                $scope.model.IsActive = "Sử dụng";
            } else { $scope.model.IsActive = "Không sử dụng"; }
            if (rs.CreatedBy !== null) {
                $scope.model.CreatedBy = $scope.model.CreatedBy.split('#')[1];
            }
            if (rs.ModifiedBy !== null) {
                $scope.model.ModifiedBy = $scope.model.ModifiedBy.split('#')[1];
            }
        });
    };
    $scope.loadData();
}); 
app.controller('addattchment', function ($scope, $rootScope, $compile, $uibModal, $confirm, $uibModalInstance, dataservice, para, para1, FileUploader) {
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
    $scope.model = { RecordGuid: para, STT: para1 };
    $scope.nameController = "";

    //Cấu hình file đính kèm
    var uploader = $scope.uploader = new FileUploader({
        // url: App.baseService() + '/upload'
    });
    uploader.filters.push({
        name: "customFilter",
        fn: function (item /*{File|FileLikeObject}*/, options) {
            return this.queue.length < 10;
        }
    });
    uploader.onCompleteAll = function () {
    };

    // Bảo hiểm
    $scope.getattachmentContracts = function () {
        var obj = {
            RecordGuid: $scope.model.RecordGuid
        }
        dataservice.getattachmentContracts(JSON.stringify(obj), function (result) {
            $scope.jdataattach = result;
            angular.forEach($scope.jdataattach, function (value, key) {
                if (value.CreatedBy !== null && value.CreatedBy !== undefined) {
                    if (value.CreatedBy.includes('#')) {
                        value.CreatedBy = value.CreatedBy.split('#')[1];
                    }
                }
            })
        });
    }
   // $scope.getattachmentContracts(); // danh sách attach bảo hiểm

    $scope.lstDeleteFile = [];
    $scope.remoteAttach = function (item, number) {
        $scope.lstDeleteFile.push(item.AttachmentGuid);
        $scope.jdataattach.splice(number, 1);
        App.notifyInfo("Xóa thành công");
        if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
            $scope.$apply();
        }
    }
    $scope.submit = function () {
        var fd = new FormData();
        $scope.lstTitlefile = [];
        if (uploader.queue.length > 0) {
            for (i = 0; i < uploader.queue.length; i++) {
                var files = uploader.queue[i]._file;
                var titleName = "";
                if (uploader.queue[i].file.FileName === "" || uploader.queue[i].file.FileName === null || uploader.queue[i].file.FileName === undefined) {
                    titleName = uploader.queue[i].file.name.split('.')[0];
                } else {
                    titleName = uploader.queue[i].file.FileName;
                }
                $scope.lstTitlefile.push({ STT: i, Title: titleName });
                fd.append("file", files);
            }
        }
        App.blockUI({
            target: ".modal-content",
            boxed: true,
            message: 'Đang tải...'
        });
        fd.append('model', JSON.stringify($scope.model));
        fd.append('lstTitlefile', JSON.stringify($scope.lstTitlefile));
        fd.append('DeleteAttach', JSON.stringify($scope.lstDeleteFile));
        $.ajax({
            type: "POST",
            url: "/Contracts/UpdateFile",
            contentType: false,
            processData: false,
            data: fd,
            success: function (rs) {
                if (rs.Error === true) {
                    App.notifyDanger(rs.Title);
                } else {
                    $scope.lstDeleteFile = [];
                    uploader.queue = [];
                    App.notifyInfo(rs.Title);
                    //$rootScope.open_Detail($scope.model.EmployeeId);
                    $scope.cancel();
                    if (para1 === 1) {
                        $scope.getattachmentContracts(); // danh sách attach bảo hiểm
                    } else if (para1 === 2) {
                        $scope.getattachmentContracts(); // danh sách attach hợp đồng
                    } else if (para1 === 3) {
                        $scope.getAttachmentRewardDisciplines();
                    } else if (para1 === 4) {
                        $scope.getAttachmentCCCN();
                    }
                    App.unblockUI(".modal-content");
                }
            },
            error: function (rs) {
                App.notifyDanger(rs.Title);
            }
        })
    }
});
