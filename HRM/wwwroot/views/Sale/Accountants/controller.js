﻿var ctxfolder = "/views/Sale/Accountants";
app.factory('dataservice', function ($http) {
    var headers = {
        "Content-Type": "application/json;odata=verbose",
        "Accept": "application/json;odata=verbose"
    };
    return {
        //------------------------*Store*-------------------------
        getById: function (data, callback) {
            $http.post('/Accountants/GetItem?Id=' + data).success(callback);
        },
        getById_Logistics: function (data, callback) {
            $http.post('/Logistics/GetItem?Id=' + data).success(callback);
        },
        delete: function (data, callback) {
            $http.post('/Accountants/Delete?Id=' + data).success(callback);
        },
        getItemALL: function (data, callback) {
            $http.post('/ProductionMaterials/getItemjexcel/', data).success(callback);
        },
        getItem_by_ItemId: function (data, callback) {
            $http.post('/ProductionMaterials/getItem_by_ItemId/', data).success(callback);
        },
        getUnitJexcel: function (data, callback) {
            $http.post('/ProductionMaterials/getUnitJexcel/', data).success(callback);
        },
        getUnit_by_ItemId: function (data, callback) {
            $http.post('/ProductionMaterials/getUnit_by_ItemId/', data).success(callback);
        },
        GetAllCommentPage: function (data, callback) {
            $http.post('/Accountants/GetAllCommentPage', data).success(callback);
        },
        getattachment: function (data, callback) {
            $http.post('/Accountants/GetItemAttachment/', data).success(callback);
        },
        GetItem_AccountantsDebt: function (data, callback) {
            $http.post('/Accountants/GetItem_AccountantsDebt?Id=' + data).success(callback);
        },
        Delete_AccountantsDebt: function (data, callback) {
            $http.post('/Accountants/Delete_AccountantsDebt?Id=' + data).success(callback);
        },
        GetItem_AccountantsManagement: function (data, callback) {
            $http.post('/Accountants/GetItem_AccountantsManagement?Id=' + data).success(callback);
        },
        Delete_AccountantsManagement: function (data, callback) {
            $http.post('/Accountants/Delete_AccountantsManagement?Id=' + data).success(callback);
        },
        GetItem_AccountantsOthers: function (data, callback) {
            $http.post('/Accountants/GetItem_AccountantsOthers?Id=' + data).success(callback);
        },
        Delete_AccountantsOthers: function (data, callback) {
            $http.post('/Accountants/Delete_AccountantsOthers?Id=' + data).success(callback);
        },
        getOrderAll: function (callback) {
            $http.post('/Accountants/getOrderAll').success(callback);
        },

    };

});
app.filter('html', ['$sce', function ($sce) {
    return function (text) {
        return $sce.trustAsHtml(text);
    };
}]);
app.filter('fdate', [
    '$filter', function ($filter) {
        return function (input, f) {
            if (input && input.toString().indexOf('Date') > -1) {
                return moment(input).format(f);
            } if (input && input.toString().indexOf('T') > -1) {
                return moment(input).format(f);
            } else return input;
        };
    }
]);
var spassAresSlug = function (number, isview) {
    try {
        // khai bao bien su dung       
        var numbers = number; var length = numbers.length; if (isview) {
            // khi dang la hien thi        
            if (numbers.indexOf(".") != -1) {
                length = numbers.indexOf(".");
                numbers = number.replace(".", ",");
            }
        } else {
            // khi dang la input textbox           
            if (numbers.indexOf(",") != -1) length = numbers.indexOf(",");
        }
        // loai bo nhung ky tu khong can thiet       
        // check so 0         
        var isTrue = true; while (isTrue) {
            if (numbers[0] == "0") numbers = numbers.substr(1, numbers.length);
            else isTrue = false;
        };
        // check dau phay      
        numbers = numbers.split(",");
        var string = "";
        for (var i = 0; i < numbers.length; i++) {
            if (i == 0) string += numbers[i];
            else if (i == 1) string += "," + numbers[i];
            else string += numbers[i];
        } numbers = string;
        // dung chung cho 2 truong hop       
        var no = 3, index = length - no;
        while (index > 0) { numbers = numbers.substr(0, index) + '.' + numbers.substr(index); index -= no; };
        return numbers == "" ? 0 : numbers;
    } catch (ex) { }
    return numbers == "" ? 0 : numbers;
}

app.directive('formatsub', ['$filter', function ($filter) {
    return {
        require: '?ngModel', link: function (scope, elem, attrs, ctrl) {
            if (!ctrl) return; ctrl.$formatters.unshift(function (viewValue) {
                try {
                    plainNumber = viewValue.replace('.', ','); elem.val(spassAresSlug(plainNumber));
                    return spassAresSlug(plainNumber);
                } catch (ex) { return undefined; }
            }); ctrl.$parsers.unshift(function (viewValue) {
                plainNumber = viewValue.replace(/[^-0-9,]/g, '');
                elem.val(spassAresSlug(plainNumber)); return plainNumber.replace(/[,]/g, '.');
            });
        }
    };
}]);
function onlickFileComment(d) {
    var __el = document.getElementById('file-comment-id');
    __el.setAttribute('accept', '.rar, .zip');
    d === 1 ? __el.setAttribute('accept', 'video/mp4,video/x-m4v,video/*') : '';
    d === 2 ? __el.setAttribute('accept', '.jpg, .png, .jpeg, .gif, .bmp, .tif, .tiff|images/*') : '';
    __el.click();
}
app.controller('Ctrl_ES_VCM_Accountants', function ($scope, $rootScope, $sce, $compile, $uibModal, dataservice) {
    $rootScope.Title = "";
    $rootScope.go = function (path) {
        $location.path(path); return false;
    };
    $rootScope.TableName = "/Accountants";
    var obj = {
        Keyword: ""
    }
    dataservice.getUnitJexcel(obj, function (rs) {
        if (rs.Error) {
            App.notifyDanger(rs.Title);
        } else {
            angular.forEach(rs, function (value, key) {
                value.value = value.id;
                value.text = value.name;
            });
            $rootScope.ListUnitID = rs;
        }
    })
    dataservice.getOrderAll(function (rs) {
        if (rs.Error) {
            App.notifyDanger(rs.Title);
        } else {
            $rootScope.ListOrderID = rs;
        }
    })



    $rootScope.validationOptions = {
        rules: {
            CodeID: {
                required: true,
                maxlength: 50
            }
        },
        messages: {
            CodeID: {
                required: "Yêu cầu nhập mã Y/C dự chi.",
                maxlength: "Mã Y/C dự chi không vượt quá 50 ký tự"
            }
        }
    };
    $rootScope.ListTypes = [{
        value: '1',
        text: 'Vật tư sản xuất'
    }, {
        value: '2',
        text: 'Logistics'
    }];
    $rootScope.ListIsAccountant = [{
        value: 1,
        text: 'Duyệt'
    }, {
        value: 0,
        text: 'Chưa duyệt'
    }];
    $rootScope.ListStatusOrder = [{
        value: 'D',
        text: 'Đã duyệt'
    }, {
        value: 'C',
        text: 'Chưa duyệt'
    }];
    $rootScope.ListMethod = [{
        value: 'C',
        text: 'Chuyển khoản'
    }, {
        value: 'T',
        text: 'Tiền mặt'
    }];
    $rootScope.listMonth = [];
    for (var i = 1; i <= 12; i++) {
        var ob = {
            value: i.toString(),
            text: i.toString()
        }
        $rootScope.listMonth.push(ob);
    }

    $rootScope.listYear = [];
    for (var i = 2022; i < 2080; i++) {
        var ob = {
            value: i.toString(),
            text: i.toString()
        }
        $rootScope.listYear.push(ob);
    }
    $rootScope.ColumnMultiConfig = {
        selectAll: true,
        search: true,
        searchText: 'Tìm kiếm...',
        showTitle: 'true',
        csvSepChar: ',',
        captionFormat: '{0} lựa chọn',
        captionFormatAllSelected: '{0} lựa chọn',
        noMatch: 'Không có dữ liệu "{0}"',
        okCancelInMulti: true,
        locale: ['Đồng ý', 'Hủy', 'Chọn tất cả']
    };
    $rootScope.ColumnConfig = {
        search: true
    };
    //các hàm chuyển đổi định dạng /Date(785548800000)/
    $rootScope.convertToJSONDate = function (strDate) {
        if (strDate !== null && strDate !== "") {
            var Str = strDate.toString();
            if (Str.indexOf("/Date") >= 0) {

                return Str;
            } else {
                var newDate = new Date(strDate);
                return '/Date(' + newDate.getTime() + ')/';
            }
        }

    }
    //định dạng số xóa dấu chấm phẩy
    $rootScope.addPeriod = function (nStr) {
        if (nStr !== null && nStr !== "" && nStr !== undefined) {
            nStr = Math.round(parseFloat(nStr) * 100) / 100;

            nStr += "";
            if (nStr.indexOf(",") >= 0) {
                var x = nStr.split(",");
            } else {
                var x = nStr.split(".");
            }
            var x1 = x[0];
            var x2 = x.length > 1 && parseInt(x[1]) > 0 ? "," + x[1] : "";
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, "$1" + "." + "$2");
            }
            var Total = x1 + x2;
            return Total;
        }
        else {
            return 0;
        }
    };


    //Thêm phần tinymceOptions
    $rootScope.tinymceOptions = {
        selector: 'textarea',
        plugins: [
            'advlist autolink lists link charmap print preview hr anchor pagebreak image',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
        ],
        skin: 'lightgray', language: 'vi_VN',
        theme: 'modern', height: 70,
        toolbar1: 'codesample | bold italic sizeselect fontselect fontsizeselect | hr alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | insertfile undo redo | emoticons | code',
        fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
        toolbar2: 'print preview insert link image media | forecolor backcolor emoticons | codesample',
        image_advtab: true,

        file_browser_callback: function (field_name, url, type, win) {
            var connector = "/FileManager/Folder/File";
            var enableAutoTypeSelection = true;
            if (enableAutoTypeSelection && type) {
                connector += "&Type=" + type;
            }
            connector += "&Field=" + field_name;
            App.openPage(connector, "File Manager", 1150, 600);
        },
        relative_urls: false,
        remove_script_host: true,
        convert_urls: true
    };

    $rootScope.ConDate = function (data, number) {
        try {
            if (data == null || data == "") {
                return '';
            }
            if (data !== null && data != "" && data != undefined) {
                try {
                    if (data.indexOf("SA") != -1 || data.indexOf("CH") != -1) {
                        if (data.indexOf("SA") != -1) { }
                        if (data.indexOf("CH") != -1) { }
                    }

                    if (data.indexOf('Date') != -1) {
                        data = data.substring(data.indexOf("Date") + 5);
                        data = parseInt(data);
                    }
                }
                catch (ex) { }
                var newdate = new Date(data);
                if (number == 3) {
                    return newdate;
                }
                if (number == 2) {
                    return "/Date(" + newdate.getTime() + ")/";
                }
                var month = newdate.getMonth() + 1;
                var day = newdate.getDate();
                var year = newdate.getFullYear();
                var hh = newdate.getHours();
                var mm = newdate.getMinutes();
                if (month < 10)
                    month = "0" + month;
                if (day < 10)
                    day = "0" + day;
                if (mm < 10)
                    mm = "0" + mm;
                if (number == 0) {
                    return todayDate = day + "/" + month + "/" + year;
                }
                if (number == 1) {
                    return todayDate = day + "/" + month + "/" + year + " " + hh + ":" + mm;
                }
                if (number == 4) {
                    return new Date(year, month - 1, day);
                }
            } else {
                return '';
            }
        } catch (ex) {
            return "";
        }
    }
});
app.config(function ($routeProvider, $validatorProvider) {
    $routeProvider
        .when('/', {
            templateUrl: ctxfolder + '/index.html',
            controller: 'index'
        })
        .when('/edit/:id', {
            templateUrl: ctxfolder + '/edit.html',
            controller: 'edit'
        })
        .when('/add/', {
            templateUrl: ctxfolder + '/add.html',
            controller: 'add'
        });
    $validatorProvider.setDefaults({
        errorElement: 'span',
        errorClass: 'help-block',
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        success: function (label) {
            label.closest('.form-group').removeClass('has-error');
        }
    });
});
//controller hiển thị, tìm kiếm
app.controller('index', function ($http, $scope, $timeout, $uibModal, $rootScope, $compile, $confirm, $ngConfirm, $uibModal, DTOptionsBuilder, DTColumnBuilder, DTInstances, dataservice, keyhandle, utility) {
    $scope.utility = utility;
    $scope.model = {};
    var vm = $scope;
    var ctrl = $scope;
    ctrl.nextRow = function (d) {
        if (ctrl.ElementRowCheck === -1) {
            if (ctrl.allRow.length > 1) {
                ctrl.ElementRowCheck = ctrl.allRow[0].row;
                $('td', ctrl.ElementRowCheck).addClass("es-iSeleted");
                ctrl.iSelected = ctrl.allRow[0].data;
                ctrl.initData(ctrl.iSelected.RowGuid);

            }
        } else
            for (var i = 0; i < ctrl.allRow.length; i++) {
                if (ctrl.allRow[i].row === ctrl.ElementRowCheck) {
                    if (i === ctrl.allRow.length - 1) break;
                    $('td', ctrl.ElementRowCheck).removeClass('es-iSeleted');
                    ctrl.ElementRowCheck = ctrl.allRow[i + 1].row;
                    $('td', ctrl.ElementRowCheck).addClass("es-iSeleted");
                    ctrl.iSelected = ctrl.allRow[i + 1].data;
                    ctrl.initData(ctrl.iSelected.RowGuid);
                    break;
                }
            }
    }
    ctrl.upRow = function (d) {
        if (ctrl.ElementRowCheck === -1) {
            if (ctrl.allRow.length > 1) {
                ctrl.ElementRowCheck = ctrl.allRow[ctrl.allRow.length - 1].row;
                $('td', ctrl.ElementRowCheck).addClass("es-iSeleted");
                ctrl.iSelected = ctrl.allRow[ctrl.allRow.length - 1].row;
                ctrl.initData(ctrl.iSelected.RowGuid);
            }
        } else
            for (var i = 0; i < ctrl.allRow.length; i++) {
                if (ctrl.allRow[i].row === ctrl.ElementRowCheck) {
                    if (i === 0) break;
                    $('td', ctrl.ElementRowCheck).removeClass('es-iSeleted');
                    ctrl.ElementRowCheck = ctrl.allRow[i - 1].row;
                    $('td', ctrl.ElementRowCheck).addClass("es-iSeleted");
                    ctrl.iSelected = ctrl.allRow[i - 1].data;
                    ctrl.initData(ctrl.iSelected.RowGuid);
                    break;
                }
            }
    }
    keyhandle.eventCtrl(40, ctrl.nextRow);
    keyhandle.eventCtrl(38, ctrl.upRow);
    ctrl.ElementRowCheck = -1;
    ctrl.allRow = [];
    ctrl.iSelected = {};
    ctrl.dataScrollPage = [];
    var date = new Date();

    $scope.staticParam = {
        totalItems: 0,
        currentPage: 1,
        pageSize: 25,
        maxSize: 5,
        Search: "",
        Types: "1",
        Month: [(date.getMonth() + 1).toString()],
        Year: date.getFullYear()
    };
    ctrl.changeOptionDate = function (d) {
        if (d.length === 0) {
            ctrl.staticParam.StartDate = null;
            ctrl.staticParam.EndDate = null;
        } else {
            ctrl.staticParam.StartDate = $rootScope.ConDate(d[0], 2);
            ctrl.staticParam.EndDate = $rootScope.ConDate(d[1], 2);
        }
    }


    $scope.selected = [];
    $scope.selectAll = false;
    $scope.toggleOne = toggleOne;
    $scope.toggleAll = toggleAll;

    var titleHtml = '<label style="text-align: center;" class="mt-checkbox" ><input type="checkbox" ng-model="selectAll" ng-click="toggleAll(selectAll, selected)"/><span></span></label>';
    //if (App.Permissions.LISTVIEW)
    vm.dtOptions = DTOptionsBuilder.newOptions()
        .withPaginationType('full_numbers')
        .withDataProp('data').withDisplayLength(15)
        .withOption('order', [1, 'desc'])
        .withOption('serverSide', true)
        .withOption('pageLength', 25)
        .withOption('info', false)
        .withOption('stateLoadParams', function (settings, data) {
            data.search.search = '';
        })
        .withOption('headerCallback', function (header) {
            if (!$scope.headerCompiled) {
                $scope.headerCompiled = true;
                $compile(angular.element(header).contents())($scope);
            }
        })
        .withOption('initComplete', function (settings, json) {
            //thêm sự kiện scroll phân trang
            //  $('#tblData').parent().attr("onscroll", "angular.element(this).scope().LoadScroll(this)");
            PLUGIN_DATATABLE_TABLE_SCROLL.INIT_EVENT('tblData', $scope.LoadScroll);

        })
        .withOption('createdRow', function (row, data, dataIndex) {
            const contextScope = $scope.$new(true);
            contextScope.data = data;
            $scope.data = data;
            $compile(row)($scope);
            contextScope.contextMenu = $scope.contextMenu;
            $compile(angular.element(row).find('input'))($scope);
            $compile(angular.element(row).attr('context-menu', 'contextMenu'))(contextScope);
        });
    // if (App.Permissions.LISTVIEW)
    vm.dtOptions.withOption('ajax', function (data, callback, settings) {
        data.Keyword = $scope.staticParam.Search;
        data.Types = $scope.staticParam.Types;
        data.lstString = $scope.staticParam.Month != "" ? $scope.staticParam.Month : [];
        data.Years = $scope.staticParam.Year;
        App.blockUI({
            target: "#tblData",
            boxed: true,
            message: 'Đang tải...'
        });
        $http.post('/Accountants/JTable', data)
            .success(function (res) {
                $scope.selected = [];
                if (data.start === 0) {
                    $scope.dataload = [];
                    angular.forEach(res.data, function (val, key) {
                        $scope.dataload.push(val);
                    });
                    callback({
                        data: $scope.dataload,
                        recordsTotal: res.recordsTotal,
                        recordsFiltered: res.recordsFiltered
                    });
                } else {
                    if (res.recordsTotal > $scope.dataload.length) {
                        angular.forEach(res.data, function (val, key) {
                            $scope.dataload.push(val);
                        });
                        callback({
                            data: $scope.dataload,
                            recordsTotal: res.recordsTotal,
                            recordsFiltered: res.recordsFiltered
                        });
                    }
                }
                for (var i = 0; i < ctrl.allRow.length; i++) {
                    if (ctrl.allRow[i].data.RowGuid === ctrl.iSelected.RowGuid) {
                        $('td', ctrl.allRow[i].row).addClass("es-iSeleted");
                        ctrl.ElementRowCheck = ctrl.allRow[i].row;
                        ctrl.initData(ctrl.iSelected.RowGuid);
                        break;
                    }
                }
                if ($scope.dataload.length > 0) {
                    ctrl.initData($scope.dataload[0].RowGuid);
                } else {
                    ctrl.initData(null);
                }
                PLUGIN_DATATABLE_TABLE_SCROLL.UNLOCK_EVENT('tblData');
                App.unblockUI("#tblData");
            });
    });
    //scroll theo tỷ lệ màn hình
    vm.dtOptions.withOption('scrollY', '70vh')
        .withOption('scrollX', '100%') //mặc định 100%
        .withOption('scrollCollapse', true) //xóa khoảng trắng khi dữ liệu trong bảng ít
        .withOption('rowCallback', rowCallback) //sự kiên click vào 1 dòng
        .withOption('scroller', {
            loadingIndicator: true
        });
    //.withFixedColumns({
    //    leftColumns: 0 //fix bên trái 2 cột
    //    //rightColumns: 1 //fix bên phải 1 cột
    //}
    //);
    //table
    vm.dtColumns = [];

    vm.dtColumns.push(DTColumnBuilder.newColumn('RowGuid').withTitle('STT').notSortable().withOption('sWidth', '5px').renderWith(function (data, type, full, meta) {
        return meta.row + 1;
    }).withOption('sClass', 'tcenter tcenter-header'));
    vm.dtColumns.push(DTColumnBuilder.newColumn('CodeID').withTitle('Mã Y/C duyệt chi').withOption('sWidth', '60px').withOption('sClass', 'tcenter-header').renderWith(function (data, type, full, meta) {
        return '<a  style="color: #4285f4;" href="" ng-click="open(\'' + full.RowGuid + '\')" >' + data + '</a>';
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('DateSXString').withTitle('Ngày yêu cầu').withOption('sWidth', '40px').withOption('sClass', 'tcenter').renderWith(function (data, type, full, meta) {
        return data;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('EmployeeRequire').withTitle('Người yêu cầu').withOption('sWidth', '120px').withOption('sClass', 'tcenter-header').renderWith(function (data, type, full, meta) {
        return data;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('EmployeeID').withTitle('Người duyệt chi').withOption('sWidth', '120px').withOption('sClass', 'tcenter-header').renderWith(function (data, type, full, meta) {
        return data;
    }));

    vm.dtColumns.push(DTColumnBuilder.newColumn('EmployeeName').withTitle('Người thực hiện').withOption('sWidth', '120px').withOption('sClass', 'tcenter-header').renderWith(function (data, type, full, meta) {
        return data;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('Note').withTitle('Lưu ý khác').withOption('sWidth', '120px').withOption('sClass', 'tcenter-header').renderWith(function (data, type, full, meta) {
        return data;
    }));

    vm.dtColumns.push(DTColumnBuilder.newColumn('RowGuid').notSortable().withTitle('Điều khiển').notSortable().withOption('sWidth', '40px').withOption('sClass', 'tcenter').renderWith(function (data, type, full, meta) {
        return '<div class="table__cell-actions-wrap">' +
            '<a href="javascript:;" ng-click="edit(\'' + full.RowGuid + '\')" class="table__cell-actions-item table__cell-actions-icon es-cursor"> <span class="mdi mdi-table-edit" title="Sửa"></span> </a>' +
            '<a ng-click="open(\'' + full.RowGuid + '\')" class="table__cell-actions-item table__cell-actions-icon"><span class="mdi mdi-eye" ></span></a > ' +
            '<a href="javascript:;" ng-click="comment(\'' + full.RowGuid + '\')" class="table__cell-actions-item table__cell-actions-icon"><span class="navbar-notify es-cursor" ><span><span class="navbar-notify__icon mdi mdi-comment-multiple-outline"></span></span><span class="navbar-notify__amount">' + full.CountComment + '</span></span ></a > ';
        ;

    }));
    vm.reloadData = reloadData;
    vm.dtInstance = {};
    function reloadData(resetPaging) {
        $scope.staticParam.currentPage = resetPaging;
        vm.dtInstance.reloadData(callback, resetPaging);
    }
    function callback(json) {

    }
    function toggleOne(selectedItems) {
        for (var id in selectedItems) {
            if (selectedItems.hasOwnProperty(id)) {
                if (!selectedItems[id]) {
                    vm.selectAll = false;
                    return;
                }
            }
        }
        vm.selectAll = true;
    }
    function toggleAll(selectAll, selectedItems) {
        for (var id in selectedItems) {
            if (selectedItems.hasOwnProperty(id)) {
                selectedItems[id] = selectAll;
            }
        }
    }
    function rowCallback(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        if (iDisplayIndexFull === 0) ctrl.allRow = [];
        $('td', nRow).unbind('click');
        $('td', nRow).bind('click', function () {
            if (vm.ElementRowCheck === nRow) return;
            $('td', nRow).addClass("es-iSeleted");
            $('td', vm.ElementRowCheck).removeClass('es-iSeleted');
            vm.ElementRowCheck = nRow;
            ctrl.ElementRowCheck = nRow;
            ctrl.iSelected = aData;
            vm.staticParam.RowGuid = aData.RowGuid;
            vm.staticParam.CodeID = aData.CodeID;
            $scope.initData(vm.staticParam.RowGuid);
        });
        $('td', nRow).unbind('dblclick');
        $('td', nRow).bind('dblclick', function () {
            $scope.$apply(function () {
                $scope.open(aData.RowGuid);
            });
        });
        ctrl.allRow.push({ row: nRow, data: aData });
        return nRow;
    }
    //Load dữ liệu cuộn
    $scope.total = 0;
    $scope.check = false;
    $scope.LoadScroll = function (obj) {
        vm.dtInstance.DataTable.page('next').draw('page');
    };

    $scope.reloadAll = function () {
        $scope.staticParam.Search = "";
        $scope.staticParam.Month = [(date.getMonth() + 1)];
        $scope.staticParam.Year = date.getFullYear();
        $scope.selectAll = false;
        reloadData(true);
    };
    $rootScope.reload = function () {
        $scope.dataload = [];
        reloadData(true);
    };


    // thêm mới
    $scope.add = function () {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/add.html',
            controller: 'add',
            backdrop: 'static',
            keyboard: false,
            size: '100',
            resolve: {
                para: function () {
                    return "";
                }
            }
        });
        modalInstance.result.then(function (d) {
        }, function () {
        });
    };
    // sửa 
    $scope.edit = function (temp) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/add.html',
            controller: 'add',
            backdrop: 'static',
            size: '100',
            resolve: {
                para: function () {
                    return temp;
                }
            }
        });
    };
    $scope.open = function (temp) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/open.html',
            controller: 'open',
            backdrop: 'static',
            size: '100',
            resolve: {
                para: function () {
                    return temp;
                }
            }
        });
        modalInstance.result.then(function (d) {
        }, function () {
        });
    };
    // sửa 
    $scope.approve = function (temp) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/approve.html',
            controller: 'approve',
            backdrop: 'static',
            size: '40',
            resolve: {
                para: function () {
                    return temp;
                },
                Types: function () {
                    return $scope.staticParam.Types;
                },

            }
        });
    };
    //Xóa 1 bản ghi
    $scope.delete = function (temp) {
        dataservice.getById(temp, function (rs) {
            $scope.model = JSON.parse(rs.Table1)[0];

            ESConfirm([{ 'value': 1, 'title': 'Xác nhận', 'type': 'info' }, { 'value': 2, 'title': 'Đóng', 'type': 'danger' }], { 'description': 'Bạn có muốn xóa ' + $scope.model.CodeID, 'class': 'eswarning_v2' }, function (rs) {
                if (rs === '1') {
                    App.blockUI({
                        target: "#contentMain",
                        boxed: true,
                        message: 'Đang tải...'
                    });
                    dataservice.delete(temp, function (result) {
                        if (result.Error) {
                            App.notifyDanger(result.Title);
                        } else {
                            App.notifyInfo(result.Title);
                            $rootScope.reload();
                        }
                        App.unblockUI("#contentMain");
                    });
                }
            }, function () {
            });
        });
    };

    $scope.contextMenu = [
        [function ($itemScope) {
            return '<i class="mdi mdi-eye"></i> Xem';
        }, function ($itemScope, $event, model) {
            $scope.open($itemScope.data.RowGuid);
        }],
        [function ($itemScope) {
            return '<i class="mdi mdi-table-edit"></i> Sửa';
        }, function ($itemScope, $event, model) {
            $scope.edit($itemScope.data.RowGuid);
        }, function ($itemScope, $event, model) {
            return true;
        }],
        [function ($itemScope) {
            return '<i class="mdi mdi-delete"></i> Xóa';
        }, function ($itemScope, $event, model) {
            $scope.delete($itemScope.data.RowGuid);
        }, function ($itemScope, $event, model) {
            return true;
        }],
        [function ($itemScope) {
            return '<i class="mdi mdi-comment-outline"></i> Ý Kiến';
        }, function ($itemScope, $event, model) {
            loadTMStart();
            vm.staticParam.RowGuid = $itemScope.data.RowGuid;
            $scope.txtComment = $itemScope.data.CodeID;
            $scope.liComment = [];
            $scope.initPara = { Page: 1, ItemPage: 15, RecordGuid: vm.staticParam.RowGuid };
            $scope.loadDataComment();
        }],
        [function ($itemScope) {
            return '<i class="mdi mdi-attachment"></i> Đính kèm';
        }, function ($itemScope, $event, model) {
            $scope.AttchmentOrder($itemScope.data.RowGuid);
        }, function ($itemScope, $event, model) {
            return true;
        }],
    ];
    $scope.TypesChange = function (data) {
        $scope.staticParam.Types = data !== 'null' ? data : "";
        $scope.selectAll = false;
        $rootScope.reload();
    };
    $scope.ChangeMonth = function (data) {
        var filtered = data.filter(function (el) {
            return el != "null";
        });
        $scope.staticParam.Month =filtered;
        if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
            $scope.$apply();
        }
        $rootScope.reload();
    };
    $scope.ChangeYear = function (data) {
        $scope.staticParam.Year = data;
        if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
            $scope.$apply();
        }
        $rootScope.reload();
    };
    $scope.AttchmentOrder = function (data) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/addAttchment.html',
            controller: 'addattchment',
            backdrop: 'static',
            size: '80',
            resolve: {
                para: function () {
                    return data;
                },
                data: function () {
                    return "";
                },
                para1: function () {
                    return $scope.openAttach;
                }
            }
        });
    }
    $scope.addAccountantsOthers = function () {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/AccountantsOthers_index.html',
            controller: 'AccountantsOthers_index',
            backdrop: 'static',
            keyboard: false,
            size: '100',
            resolve: {
                para: function () {
                    return "";
                }
            }
        });
        modalInstance.result.then(function (d) {
        }, function () {
        });
    };
    $scope.addAccountantsManagement = function () {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/AccountantsManagement_index.html',
            controller: 'AccountantsManagement_index',
            backdrop: 'static',
            keyboard: false,
            size: '100',
            resolve: {
                para: function () {
                    return "";
                }
            }
        });
        modalInstance.result.then(function (d) {
        }, function () {
        });
    };
    $scope.addAccountantsDebt = function () {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/AccountantsDebt_index.html',
            controller: 'AccountantsDebt_index',
            backdrop: 'static',
            keyboard: false,
            size: '100',
            resolve: {
                para: function () {
                    return "";
                }
            }
        });
        modalInstance.result.then(function (d) {
        }, function () {
        });
    };
    //Comment
    $scope.model = {};
    $scope.initPara = { Page: 1, ItemPage: 15, RecordGuid: null };
    $scope.txtComment = "";
    $scope.check_commnent = false;
    $scope.total_comment = 0;
    $scope.LoadScroll_commnet = function (obj) {

        var total = obj.offsetHeight + obj.scrollTop;
        if (obj.offsetHeight + obj.scrollTop >= obj.scrollHeight && $scope.check_commnent && $scope.total_comment < total) {
            $scope.check_commnent = false;
            try {
                $scope.IconLoad = true;
                $scope.initPara.Page = $scope.initPara.Page + 1;
                $scope.loadDataComment();
            }
            catch (ex) {
            }
        }
        if (obj.offsetHeight + obj.scrollTop >= obj.scrollHeight) {
            $scope.total_comment = obj.offsetHeight + obj.scrollTop;
        }
        else {
            $scope.check_commnent = true;
        }
    }
    //comment
    $scope.comment = function (Id) {
        dataservice.getById(Id, function (rs) {
            loadTMStart();
            vm.staticParam.RowGuid = Id;
            var model = JSON.parse(rs.Table1)[0];
            $scope.txtComment = model.CodeID;
            $scope.liComment = [];
            $scope.initPara = { Page: 1, ItemPage: 15, RecordGuid: vm.staticParam.RowGuid };
            $scope.loadDataComment();
        });

    };
    $scope.liComment = [];
    $scope.loadDataComment = function () {
        if ($scope.initPara.RecordGuid !== "" && $scope.initPara.RecordGuid !== null && $scope.initPara.RecordGuid !== undefined) {
            dataservice.GetAllCommentPage($scope.initPara, function (rs) {
                if (rs.Error) {
                    App.notifyDanger(rs.Title);
                } else {
                    $scope.IconLoad = false;
                    for (var i = 0; i < rs.Object.length; i++) {
                        $scope.liComment.unshift(rs.Object[i]);
                    }
                    $scope.total_comment = $scope.liComment.length;

                    angular.forEach($scope.liComment, function (value, key) {
                        if (value.CreatedBy.split('#')[0] === $rootScope.loginName) {
                            value.Class = "is-interlocutor";
                        } else {
                            value.Class = "is-self";
                        }

                        if (value.CommentAttachments !== null) {
                            var listAttachments = [];
                            if (value.CommentAttachments.includes('|')) {
                                var data = value.CommentAttachments.split('|');

                                var obj = {
                                    AttachmentGuid: data[0],
                                    FileName: data[1],
                                    checksrc: false
                                };
                                if (data[1].includes('.')) {
                                    obj.FileExtension = data[1].split('.')[1];
                                } else {
                                    obj.FileExtension = data[1];
                                }
                                if (!obj.FileExtension.includes('xlsx') && !obj.FileExtension.includes('xls') && !obj.FileExtension.includes('png') && !obj.FileExtension.includes('docx') && !obj.FileExtension.includes('doc') && !obj.FileExtension.includes('pdf') && !obj.FileExtension.includes('txt') && !obj.FileExtension.includes('png') && !obj.FileExtension.includes('jpg')) {
                                    obj.checksrc = false;
                                } else {
                                    obj.checksrc = true;
                                }
                                listAttachments.push(obj);
                            } else {
                                var data1 = value.CommentAttachments;
                                angular.forEach(data1, function (val, keys) {
                                    var obj = {
                                        AttachmentGuid: val.AttachmentGuid,
                                        FileName: val.FileName
                                    }
                                    if (val.FileExtension.includes('.')) {
                                        obj.FileExtension = val.FileExtension.split('.')[1];
                                    } else {
                                        obj.FileExtension = val.FileExtension;
                                    }
                                    if (!obj.FileExtension.includes('xlsx') && !obj.FileExtension.includes('xls') && !obj.FileExtension.includes('png') && !obj.FileExtension.includes('docx') && !obj.FileExtension.includes('doc') && !obj.FileExtension.includes('pdf') && !obj.FileExtension.includes('txt') && !obj.FileExtension.includes('png') && !obj.FileExtension.includes('jpg')) {
                                        obj.checksrc = false;
                                    } else {
                                        obj.checksrc = true;
                                    }
                                    listAttachments.push(obj);
                                });
                            }
                            value.CommentAttachments = listAttachments;

                        }
                    });
                    gotoBottom('setting-content-scroll');
                }
            })
        }

    }
    //  $scope.loadDataComment();
    $scope.insertcomment = function () {
        if ($scope.model.Comment === undefined || $scope.model.Comment === "") {
            App.notifyDanger("Bạn phải nhập ý kiến");
            return true;
        }
        var fd = new FormData();
        $scope.model.RecordGuid = vm.staticParam.RowGuid;
        var file = $('#file-comment-id').prop('files')[0];
        var message = $('#message').val().replace(/(\n)+/g, '<br />');
        $scope.model.Comment = message;
        fd.append('insert', JSON.stringify($scope.model));
        fd.append('file', file);
        $.ajax({
            type: "POST",
            url: "/Accountants/InsertComments",
            contentType: false,
            processData: false,
            data: fd,
            success: function (rs) {
                if (rs.Error) {
                    App.notifyDanger(rs.Title);
                }
                else {
                    $('#message').val('');
                    $scope.model.Comment = "";
                    $scope.NameFile = undefined;
                    $("#file-comment-id").val('');
                    $scope.liComment = [];
                    $scope.initPara = { Page: 1, ItemPage: 15, RecordGuid: vm.staticParam.RowGuid };
                    $scope.loadDataComment();
                }
            }
        });
    }
    $scope.FileNameChnage = '';
    $scope.addNameFile = function () {
        if ($('#file-comment-id').prop('files')[0] !== undefined)
            if ($scope.model !== undefined && $scope.model.Comment !== '') {
                $scope.model.Comment += " " + $('#file-comment-id').prop('files')[0].name;
            } else {
                $scope.model.Comment = " " + $('#file-comment-id').prop('files')[0].name;
            }
        if (!$scope.$$phase) {
            $scope.$apply();
        }
    }
    function loadTMStart() {
        setTimeout(_loadTMStart, 100);
    }
    function _loadTMStart() {
        var _el = document.getElementsByClassName('settings-panel')[0];
        _el.classList.add('is-opened');
    }
    $scope.deleteItem = function (element, row) {
        $scope.ListData = $scope.jxcelAddDetail.jexcel('getData');
        if ($scope.ListData.length > 1) {
            ESConfirm([{ 'value': 1, 'title': 'Xác nhận', 'type': 'info' }, { 'value': 2, 'title': 'Đóng', 'type': 'danger' }], { 'description': 'dữ liệu này không ?', 'class': 'eswarning' }, function (rs) {
                if (rs === '1') {
                    var cell = 'A' + (parseInt(row) + 1);
                    var id = $scope.jxcelAddDetail.jexcel('getValue', cell);
                    if (id !== "" && id !== null) {
                        var sta = false;
                        for (var i = 0; i < $scope.ListDetailDelete.length; i++) {
                            if ($scope.ListDetailDelete[i] === id) {
                                sta = true;
                            }
                        }
                        if (!sta) {
                            $scope.ListDetailDelete.push(id);
                        }
                    }
                    $scope.jxcelAddDetail.jexcel('deleteRow', row);
                    $scope.onload1();

                    App.notifyInfo('Xóa thành công');
                }
                if (rs === '2') {
                    console.log('Không chấp nhận');
                }
            }, function () {
                //some error in this function
                console.log('Không lựa chọn');
            });
        }
        else {
            App.notifyDanger('Không xóa được dòng cuối cùng này');
            return;
        }
    };


    $scope.rowchange = function (obj, row, cell, columnName) {

        $scope.onload1();
    };
    $scope.roundNumber = function (number) {
        if (number > 0) {
            return parseFloat(number);
        } else return 0;

    }
    $scope.onload1 = function () {
        if ($('#myhtindex').jexcel('getData') !== undefined) {
            $rootScope.Quantity = 0;
            $rootScope.AmountOc = 0;
            $rootScope.UnitPrice = 0;
            $rootScope.VATAmountOC = 0;
            $scope.ListData = $('#myhtindex').jexcel('getData');
            angular.forEach($scope.ListData, function (val, key) {
                if (val.Quantity !== null && val.Quantity !== "" && val.Quantity !== undefined) {
                    $rootScope.Quantity += $scope.roundNumber(parseFloat(val.Quantity));
                }
                if (val.UnitPrice !== null && val.UnitPrice !== "" && val.UnitPrice !== undefined) {
                    $rootScope.UnitPrice += $scope.roundNumber(parseFloat(val.UnitPrice));
                }
                if (val.AmountOc !== null && val.AmountOc !== "" && val.AmountOc !== undefined) {
                    $rootScope.AmountOc += $scope.roundNumber(parseFloat(val.AmountOc));
                }
            });
            $('#myhtindex #colfoot-5').html($rootScope.addPeriod($scope.Quantity));
            $('#myhtindex #colfoot-6').html($rootScope.addPeriod($scope.UnitPrice));
            $('#myhtindex #colfoot-7').html($rootScope.addPeriod($scope.AmountOc));

            if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                $scope.$apply();
            }
        }
    }

    $scope.dropdownItemId = function (d1, d2, d3) {
        var _tem1 = d1.ItemID;
        if (_tem1 === "" || _tem1 === null) {
            _tem1 = "";
        }
        var obj = {
            Keyword: _tem1
        }
        dataservice.getItemALL(obj, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {

                d3.source = rs;
                d2(rs, "");
            }
        })
    }
    $scope.dropdownUnitID = function (d1, d2, d3) {
        var _tem1 = d1.UnitID;
        if (_tem1 === "" || _tem1 === null) {
            _tem1 = "";
        }
        var obj = {
            Keyword: _tem1
        }
        dataservice.getUnitJexcel(obj, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {

                d3.source = rs;
                d2(rs, "");
            }
        })
    }

    $scope.jxcelAddDetailInit = {
        dataObject: true,
        reloadData: {},
        data: [],
        colHeaders: ['', 'Tên và mô tả chi tiết', 'Số hoá đơn/hợp đồng', 'Mã đơn vị tính', 'Tên đơn vị tính', 'Số lượng', 'Đơn giá', 'Thành tiền', 'Tình trạng', 'Cách thức', 'Ghi chú'
        ],
        colTitles: ['', 'Tên và mô tả chi tiết', 'Số hoá đơn/hợp đồng', 'Mã đơn vị tính', 'Tên đơn vị tính', 'Số lượng', 'Đơn giá', 'Thành tiền', 'Tình trạng', 'Cách thức', 'Ghi chú'
        ],
        colFooters: ['', '', '', '', '', '', '', '', '', ''
        ],
        columns: [
            {
                name: 'RowGuid',
                type: 'text', ReadOnly: true

            },
            {
                name: 'ItemName',
                type: 'text'
                , ReadOnly: true
            },
            {
                name: 'NumberOrder',
                type: 'text'
                , ReadOnly: true
            },
            {
                name: 'UnitID',
                type: 'autocomplete',
                source: $rootScope.litsUnitID,
                sourceReload: $scope.dropdownUnitID,
                searchReload: true,
                onlyCode: true,
                ReadOnly: true, ReadOnly: true
            },
            {
                name: 'UnitName',
                type: 'text',
                ReadOnly: true
            },
            {
                name: 'Quantity',//Xếp hạng 3
                type: 'numeric', formatNumber: 'VN-NUMERIC', ReadOnly: true

            },
            {
                name: 'UnitPrice',//Xếp hạng 15
                type: 'numeric', formatNumber: 'VN-NUMERIC', ReadOnly: true

            },
            {
                name: 'AmountOc',//Xếp hạng 15
                type: 'numeric', formatNumber: 'VN-NUMERIC', ReadOnly: true

            },
            {
                name: 'StatusOrder',//Xếp hạng 15
                type: 'autocomplete',
                url: '/Sale/Accountants/StatusOrder/',
                updateReadOnly: true, ReadOnly: true
            }
            ,
            {
                name: 'Method',
                type: 'autocomplete',
                url: '/Sale/Logistics/BookingNumber/',
                updateReadOnly: true, ReadOnly: true
            },
            {
                name: 'Note',
                type: 'text', ReadOnly: true
            }
        ],

        columnShow: [false, true, true, true, true, true, true, true, true, true, true],
        initDataRows: ['', '', '', '', '', '', '', '', '', '', ''],
        colWidths: [50, 300, 200, 100, 150, 150, 150, 150, 100, 150, 300],
        allrowevent: true,
        deleterowconfirm: $scope.deleteItem,
        tableOverflow: true,
        csvHeaders: true,
        changeCell: $scope.rowchange,
        tableHeight: '200px',
        onpaste: $scope.onpaste1,
        onload: $scope.onload1,
        allowInsertRow: false,
        contextMenu: function () { return ""; },
    };
    $scope.initData = function (data) {
        dataservice.getById(data, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {
                $scope.lstDetail = JSON.parse(rs.Table2);
                var ApplyOutsideDetails = [];
                angular.forEach($scope.lstDetail, function (value, key) {
                    ApplyOutsideDetails.push([
                        value.RowGuid,
                        value.ItemName,
                        value.NumberOrder,
                        value.UnitID,
                        value.UnitName,
                        value.Quantity,
                        value.UnitPrice,
                        value.AmountOc,
                        value.StatusOrder,
                        value.Method,
                        value.Note
                    ]);
                });
                $scope.jxcelAddDetailInit.data = ApplyOutsideDetails;
                $scope.onload1();

                if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                    $scope.$apply();
                }
            }
        })
    }
});
function gotoBottom(id) {
    var element = document.getElementById(id);
    element.scrollTop = 10;/* element.scrollHeight - element.clientHeight;*/
}
app.controller('add', function ($scope, $rootScope, $uibModalInstance, FileUploader, dataservice, para) {
    $scope.model = { Temps: "" };
    $scope.disColumn = false;
    $scope.check = false;
    $scope.fullscreen = function (Check) {
        if (Check == false) {
            $scope.check = true;
        } else {
            $scope.check = false;
        }
    }

    $scope.ListDetailDelete = [];
    $scope.deleteItem = function (element, row) {
        $scope.ListData = $scope.jxcelAddDetail.jexcel('getData');
        if ($scope.ListData.length > 1) {
            ESConfirm([{ 'value': 1, 'title': 'Xác nhận', 'type': 'info' }, { 'value': 2, 'title': 'Đóng', 'type': 'danger' }], { 'description': 'dữ liệu này không ?', 'class': 'eswarning' }, function (rs) {
                if (rs === '1') {
                    var cell = 'A' + (parseInt(row) + 1);
                    var id = $scope.jxcelAddDetail.jexcel('getValue', cell);
                    if (id !== "" && id !== null) {
                        var sta = false;
                        for (var i = 0; i < $scope.ListDetailDelete.length; i++) {
                            if ($scope.ListDetailDelete[i] === id) {
                                sta = true;
                            }
                        }
                        if (!sta) {
                            $scope.ListDetailDelete.push(id);
                        }
                    }
                    $scope.jxcelAddDetail.jexcel('deleteRow', row);
                    $scope.onload1();

                    App.notifyInfo('Xóa thành công');
                }
                if (rs === '2') {
                    console.log('Không chấp nhận');
                }
            }, function () {
                //some error in this function
                console.log('Không lựa chọn');
            });
        }
        else {
            App.notifyDanger('Không xóa được dòng cuối cùng này');
            return;
        }
    };
    $scope.rowchange = function (obj, row, cell, columnName) {
        var rowindex = parseInt(row + 1); // dòng muốn thay đổi      
        $scope.ListData = $scope.jxcelAddDetail.jexcel('getData');
        if (columnName === "ItemID") {
            dataservice.getItem_by_ItemId({ IdS: [$scope.ListData[row].ItemID] }, function (rs) {
                if (rs.Error) {
                    App.notifyDanger(rs.Title);
                } else {
                    $scope.jxcelAddDetail.jexcel("setValueByName", "ItemName", row, rs.ItemName);
                    $scope.jxcelAddDetail.jexcel("setValueByName", "UnitID", row, rs.UnitID);
                    dataservice.getUnit_by_ItemId({ IdS: [rs.UnitID] }, function (rs) {
                        if (rs.Error) {
                            App.notifyDanger(rs.Title);
                        } else {

                            $scope.jxcelAddDetail.jexcel("setValueByName", "UnitName", row, rs.UnitName);
                            if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                                $scope.$apply();
                            }
                        }
                    });
                    if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                        $scope.$apply();
                    }
                }
            })
        }
        if (columnName === "UnitID") {
            if ($scope.ListData[row].UnitID != "") {
                dataservice.getUnit_by_ItemId({ IdS: [$scope.ListData[row].UnitID] }, function (rs) {
                    if (rs.Error) {
                        App.notifyDanger(rs.Title);
                    } else {
                        $scope.jxcelAddDetail.jexcel("setValueByName", "UnitName", row, rs.UnitName);
                        if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                            $scope.$apply();
                        }
                    }
                });
            } else {
                $scope.jxcelAddDetail.jexcel("setValueByName", "UnitName", row, "");
                if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                    $scope.$apply();
                }
            }


        }
        if (columnName === "Quantity" || columnName === "UnitPrice") {
            var Quantity = parseFloat($scope.ListData[row].Quantity);
            var UnitPrice = parseFloat($scope.ListData[row].UnitPrice);
            var AmountOc = Quantity * UnitPrice;
            $scope.jxcelAddDetail.jexcel("setValueByName", "AmountOc", row, AmountOc);
        }

        $scope.onload1();
    };
    $scope.onpaste1 = function (obj, row) {

        $scope.ListData = $('#myht').jexcel('getData');
        $scope.ListData = $scope.ListData.filter(a => a.ItemID !== '');
        var list = $scope.ListData.map(a => a.ItemID);

        var fd = new FormData();
        fd.append('Insert', JSON.stringify($scope.model));
        fd.append('Detail', JSON.stringify(list));

        $.ajax({
            url: '/ProductionMaterials/GetItemsByItemlistId',
            data: fd,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (rs) {
                if (rs.Error) {
                    App.notifyDanger(rs.Title);
                }
                else {
                    $scope.ListData1 = rs;
                    $scope.lstdata = [];
                    for (var j = 0; j < $scope.ListData.length; j++) {
                        let val1 = $scope.ListData1.find(o => o.ItemID.toUpperCase() === $scope.ListData[j].ItemID.toUpperCase());
                        var value = $scope.ListData[j];
                        value.ItemName = val1.ItemName !== null ? val1.ItemName : "";
                        value.UnitID = val1.UnitID !== null ? val1.UnitID : 0;
                        value.UnitName = val1.UnitName !== null ? val1.UnitName : 0;
                        var obj = [
                            value.RowGuid,
                            value.ItemID,
                            value.ItemName,
                            value.UnitID,
                            value.UnitName,
                            value.Quantity,
                            value.UnitPrice,
                            value.AmountOc,
                            value.VATPercent,
                            value.VATAmountOC,
                            value.Note];
                        $scope.lstdata.push(obj);
                    }
                    $scope.jxcelAddDetailInit.data = $scope.lstdata;
                    $scope.onload1();
                }
            },
            error: function (rs) {
                App.notifyInfo(rs.Title);
            }
        });
    }
    $scope.roundNumber = function (number) {
        if (number > 0) {
            return parseFloat(number);
        } else return 0;

    }
    $scope.onload1 = function () {
        if ($('#myht').jexcel('getData') !== undefined) {
            $rootScope.Quantity = 0;
            $rootScope.AmountOc = 0;
            $rootScope.UnitPrice = 0;
            $rootScope.VATAmountOC = 0;
            $scope.ListData = $('#myht').jexcel('getData');
            angular.forEach($scope.ListData, function (val, key) {
                if (val.Quantity !== null && val.Quantity !== "" && val.Quantity !== undefined) {
                    $rootScope.Quantity += $scope.roundNumber(parseFloat(val.Quantity));
                }
                if (val.UnitPrice !== null && val.UnitPrice !== "" && val.UnitPrice !== undefined) {
                    $rootScope.UnitPrice += $scope.roundNumber(parseFloat(val.UnitPrice));
                }
                if (val.AmountOc !== null && val.AmountOc !== "" && val.AmountOc !== undefined) {
                    $rootScope.AmountOc += $scope.roundNumber(parseFloat(val.AmountOc));
                }

            });
            $('#myht #colfoot-5').html($rootScope.addPeriod($scope.Quantity));
            $('#myht #colfoot-6').html($rootScope.addPeriod($scope.UnitPrice));
            $('#myht #colfoot-7').html($rootScope.addPeriod($scope.AmountOc));
            if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                $scope.$apply();
            }
        }
    }

    $scope.dropdownItemId = function (d1, d2, d3) {
        var _tem1 = d1.ItemID;
        if (_tem1 === "" || _tem1 === null) {
            _tem1 = "";
        }
        var obj = {
            Keyword: _tem1
        }
        dataservice.getItemALL(obj, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {

                d3.source = rs;
                d2(rs, "");
            }
        })
    }
    $scope.dropdownUnitID = function (d1, d2, d3) {
        var _tem1 = d1.UnitID;
        if (_tem1 === "" || _tem1 === null) {
            _tem1 = "";
        }
        var obj = {
            Keyword: _tem1
        }
        dataservice.getUnitJexcel(obj, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {

                d3.source = rs;
                d2(rs, "");
            }
        })
    }
    $scope.jxcelAddDetailInit = {
        dataObject: true,
        reloadData: {},
        data: [],
        colHeaders: ['', 'Tên và mô tả chi tiết', 'Số hoá đơn/hợp đồng', 'Mã đơn vị tính', 'Tên đơn vị tính', 'Số lượng', 'Đơn giá', 'Thành tiền', 'Tình trạng', 'Cách thức', 'Ghi chú'
        ],
        colTitles: ['', 'Tên và mô tả chi tiết', 'Số hoá đơn/hợp đồng', 'Mã đơn vị tính', 'Tên đơn vị tính', 'Số lượng', 'Đơn giá', 'Thành tiền', 'Tình trạng', 'Cách thức', 'Ghi chú'
        ],
        colFooters: ['', '', '', '', '', '', '', '', '', ''
        ],
        columns: [
            {
                name: 'RowGuid',
                type: 'text',
                isFixColumn: true,
            },
            {
                name: 'ItemName',
                type: 'text',
                isFixColumn: true,
            },
            {
                name: 'NumberOrder',
                type: 'text'
            },
            {
                name: 'UnitID',
                type: 'autocomplete',
                source: $rootScope.litsUnitID,
                sourceReload: $scope.dropdownUnitID,
                searchReload: true,
                onlyCode: true,
                isFixColumn: true,
            },
            {
                name: 'UnitName',
                type: 'text',
                isFixColumn: true,
            },
            {
                name: 'Quantity',//Xếp hạng 3
                type: 'numeric', formatNumber: 'VN-NUMERIC'

            },
            {
                name: 'UnitPrice',//Xếp hạng 15
                type: 'numeric', formatNumber: 'VN-NUMERIC'

            },
            {
                name: 'AmountOc',//Xếp hạng 15
                type: 'numeric', formatNumber: 'VN-NUMERIC'

            },
            {
                name: 'StatusOrder',//Xếp hạng 15
                type: 'autocomplete',
                url: '/Sale/Accountants/StatusOrder/',
                updateReadOnly: true
            }
            ,
            {
                name: 'Method',
                type: 'autocomplete',
                url: '/Sale/Logistics/BookingNumber/',
                updateReadOnly: true
            },
            {
                name: 'Note',
                type: 'text'
            }
        ],

        columnShow: [false, true, true, true, true, true, true, true, true, true, true],
        initDataRows: ['', '', '', '', '', '', '', '', '', '', ''],
        colWidths: [50, 300, 200, 100, 150, 150, 150, 150, 100, 150, 300],
        allrowevent: true,
        deleterowconfirm: $scope.deleteItem,
        tableOverflow: true,
        csvHeaders: true,
        changeCell: $scope.rowchange,
        tableHeight: '200px',
        onpaste: $scope.onpaste1,
        onload: $scope.onload1
    };

    if (para != "") {
        $scope.Title = "Sửa";
        $scope.model.Temps = "2";
        $scope.disColumn = true;
        $scope.initData = function () {
            dataservice.getById(para, function (rs) {
                if (rs.Error) {
                    App.notifyDanger(rs.Title);
                } else {
                    $scope.model = JSON.parse(rs.Table1)[0];
                    $scope.model.DateSX = $scope.model.DateSX !== null ? "/Date(" + new Date($scope.model.DateSX).getTime() + ")/" : null;
                    $scope.lstDetail = JSON.parse(rs.Table2);
                    var ApplyOutsideDetails = [];
                    angular.forEach($scope.lstDetail, function (value, key) {
                        ApplyOutsideDetails.push([
                            value.RowGuid,
                            value.ItemName,
                            value.NumberOrder,
                            value.UnitID,
                            value.UnitName,
                            value.Quantity,
                            value.UnitPrice,
                            value.AmountOc,
                            value.StatusOrder,
                            value.Method,
                            value.Note
                        ]);
                    });
                    $scope.jxcelAddDetailInit.data = ApplyOutsideDetails;
                    $scope.onload1();
                    if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                        $scope.$apply();
                    }
                }
            })
        }
        $scope.initData();
    } else {
        $scope.Title = "Thêm mới";
        $scope.model.Temps = "1";
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    //Cấu hình file đính kèm
    $scope.model.FileAttachments = [];
    var uploader = $scope.uploader = new FileUploader({
        // url: App.baseService() + '/upload'
    });
    uploader.filters.push({
        name: 'customFilter',
        fn: function (item /*{File|FileLikeObject}*/, options) {
            return this.queue.length < 10;
        }
    });
    uploader.onSuccessItem = function (fileItem, response, status, headers) {
        $scope.model.FileAttachments.push(response);
    };
    uploader.onCompleteAll = function () {
        $scope.commit();
    };
    $scope.ChangeProductionManagerGuid = function (data) {
        $scope.model.ProductionManagerGuid = data;
    }
    $scope.insert = function () {

        if ($scope.addform.validate()) {
            var fd = new FormData();
            $scope.lstTitlefile = [];
            if (uploader.queue.length > 0) {
                for (i = 0; i < uploader.queue.length; i++) {
                    var files = uploader.queue[i]._file;
                    if (files.size > 30720000) {
                        App.notifyDanger("Yêu cầu file đính kèm không vượt quá 3MB");
                        return;
                    }
                    fd.append("file", files);
                }
            }
            var datatab1 = $scope.jxcelAddDetail.jexcel('getData');
            var ApplyOutsideDetails = [];
            for (var i = 0; i < datatab1.length; i++) {
                var value = datatab1[i];
                if (value.RowGuid != null && value.RowGuid != undefined && value.RowGuid != "") {
                    ApplyOutsideDetails.push({
                        RowGuid: value.RowGuid,
                        NumberOrder: value.NumberOrder,
                        ItemName: value.ItemName,
                        UnitID: value.UnitID,
                        UnitName: value.UnitName,
                        Quantity: value.Quantity,
                        UnitPrice: value.UnitPrice,
                        AmountOc: value.AmountOc,
                        StatusOrder: value.StatusOrder,
                        Method: value.Method,
                        Note: value.Note,
                        SortOrder: i
                    });

                }
                else {
                    ApplyOutsideDetails.push({
                        NumberOrder: value.NumberOrder,
                        ItemName: value.ItemName,
                        UnitID: value.UnitID,
                        UnitName: value.UnitName,
                        Quantity: value.Quantity,
                        UnitPrice: value.UnitPrice,
                        AmountOc: value.AmountOc,
                        StatusOrder: value.StatusOrder,
                        Method: value.Method,
                        Note: value.Note,
                        SortOrder: i
                    });
                }

            };
            if (ApplyOutsideDetails.length == 0) {
                App.notifyDanger("Yêu cầu nhập chi tiết");
                return;
            }

            fd.append('submit', JSON.stringify($scope.model));
            fd.append('detail', JSON.stringify(ApplyOutsideDetails));
            fd.append('detailDelete', JSON.stringify($scope.ListDetailDelete));
            $.ajax({
                type: "POST",
                url: "/Accountants/Submit",
                contentType: false,
                processData: false,
                data: fd,
                success: function (rs) {
                    if (rs.Error) {
                        App.notifyDanger(rs.Title);
                    } else {
                        App.notifyInfo(rs.Title);
                        $uibModalInstance.close();
                        $rootScope.reload();
                    }
                }
            });
        }
    };
});
// controller open
app.controller('open', function ($scope, $rootScope, $uibModalInstance, dataservice, para, FileUploader) {
    $scope.Title = "Xem";
    $scope.model = { Temps: "" };
    $scope.disColumn = false;
    $scope.check = false;
    $scope.fullscreen = function (Check) {
        if (Check == false) {
            $scope.check = true;
        } else {
            $scope.check = false;
        }
    }

    $scope.deleteItem = function (element, row) {
        if (row === 0) {
            $scope.jxcelAddDetail.jexcel("setValueByName", "Id", row, "");
            $scope.jxcelAddDetail.jexcel("setValueByName", "ItemID", row, "");
            $scope.jxcelAddDetail.jexcel("setValueByName", "ItemName", row, "");
            $scope.jxcelAddDetail.jexcel("setValueByName", "UnitID", row, "");
            $scope.jxcelAddDetail.jexcel("setValueByName", "UnitName", row, "");
            $scope.jxcelAddDetail.jexcel("setValueByName", "Quantity", row, "");
            $scope.jxcelAddDetail.jexcel("setValueByName", "UnitPrice", row, "");
            $scope.jxcelAddDetail.jexcel("setValueByName", "AmountOc", row, "");
            $scope.jxcelAddDetail.jexcel("setValueByName", "VATPercent", row, "");
            $scope.jxcelAddDetail.jexcel("setValueByName", "VATAmountOC", row, "");
            $scope.jxcelAddDetail.jexcel("setValueByName", "Note", row, "");
        }
        $scope.jxcelAddDetail.jexcel('deleteRow', row);
    };
    $scope.rowchange = function (obj, row, cell, columnName) {
        var rowindex = parseInt(row + 1); // dòng muốn thay đổi      
        $scope.ListData = $scope.jxcelAddDetail.jexcel('getData');
        if (columnName === "ItemID") {
            dataservice.getItem_by_ItemId({ IdS: [$scope.ListData[row].ItemID] }, function (rs) {
                if (rs.Error) {
                    App.notifyDanger(rs.Title);
                } else {
                    $scope.jxcelAddDetail.jexcel("setValueByName", "ItemName", row, rs.ItemName);
                    $scope.jxcelAddDetail.jexcel("setValueByName", "UnitID", row, rs.UnitID);
                    dataservice.getUnit_by_ItemId({ IdS: [rs.UnitID] }, function (rs) {
                        if (rs.Error) {
                            App.notifyDanger(rs.Title);
                        } else {

                            $scope.jxcelAddDetail.jexcel("setValueByName", "UnitName", row, rs.UnitName);
                            if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                                $scope.$apply();
                            }
                        }
                    });
                    if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                        $scope.$apply();
                    }
                }
            })
        }
        if (columnName === "UnitID") {
            if ($scope.ListData[row].UnitID != "") {
                dataservice.getUnit_by_ItemId({ IdS: [$scope.ListData[row].UnitID] }, function (rs) {
                    if (rs.Error) {
                        App.notifyDanger(rs.Title);
                    } else {
                        $scope.jxcelAddDetail.jexcel("setValueByName", "UnitName", row, rs.UnitName);
                        if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                            $scope.$apply();
                        }
                    }
                });
            } else {
                $scope.jxcelAddDetail.jexcel("setValueByName", "UnitName", row, "");
                if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                    $scope.$apply();
                }
            }


        }
        if (columnName === "Quantity" || columnName === "UnitPrice") {
            var Quantity = parseFloat($scope.ListData[row].Quantity);
            var UnitPrice = parseFloat($scope.ListData[row].UnitPrice);
            var AmountOc = Quantity * UnitPrice;
            $scope.jxcelAddDetail.jexcel("setValueByName", "AmountOc", row, AmountOc);
            var VATPercent = parseFloat($scope.ListData[row].VATPercent);
            var VATAmountOC = AmountOc - parseFloat(AmountOc) * VATPercent / 100;
            $scope.jxcelAddDetail.jexcel("setValueByName", "VATAmountOC", row, VATAmountOC);
        }
        if (columnName === "VATPercent") {
            var AmountOc = parseFloat($scope.ListData[row].AmountOc);
            var VATPercent = parseFloat($scope.ListData[row].VATPercent);
            var VATAmountOC = AmountOc - parseFloat(AmountOc) * VATPercent / 100;
            $scope.jxcelAddDetail.jexcel("setValueByName", "VATAmountOC", row, VATAmountOC);
        }
        $scope.onload1();
    };
    $scope.onpaste1 = function (obj, row) {

        $scope.ListData = $('#myht').jexcel('getData');
        $scope.ListData = $scope.ListData.filter(a => a.ItemID !== '');
        var list = $scope.ListData.map(a => a.ItemID);

        var fd = new FormData();
        fd.append('Insert', JSON.stringify($scope.model));
        fd.append('Detail', JSON.stringify(list));

        $.ajax({
            url: '/ProductionMaterials/GetItemsByItemlistId',
            data: fd,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (rs) {
                if (rs.Error) {
                    App.notifyDanger(rs.Title);
                }
                else {
                    $scope.ListData1 = rs;
                    $scope.lstdata = [];
                    for (var j = 0; j < $scope.ListData.length; j++) {
                        let val1 = $scope.ListData1.find(o => o.ItemID.toUpperCase() === $scope.ListData[j].ItemID.toUpperCase());
                        var value = $scope.ListData[j];
                        value.ItemName = val1.ItemName !== null ? val1.ItemName : "";
                        value.UnitID = val1.UnitID !== null ? val1.UnitID : 0;
                        value.UnitName = val1.UnitName !== null ? val1.UnitName : 0;
                        var obj = [
                            value.RowGuid,
                            value.ItemID,
                            value.ItemName,
                            value.UnitID,
                            value.UnitName,
                            value.Quantity,
                            value.UnitPrice,
                            value.AmountOc,
                            value.VATPercent,
                            value.VATAmountOC,
                            value.Note];
                        $scope.lstdata.push(obj);
                    }
                    $scope.jxcelAddDetailInit.data = $scope.lstdata;
                    $scope.onload1();
                }
            },
            error: function (rs) {
                App.notifyInfo(rs.Title);
            }
        });
    }
    $scope.roundNumber = function (number) {
        if (number > 0) {
            return parseFloat(number);
        } else return 0;

    }
    $scope.onload1 = function () {
        if ($('#myht').jexcel('getData') !== undefined) {
            $rootScope.Quantity = 0;
            $rootScope.AmountOc = 0;
            $rootScope.UnitPrice = 0;
            $scope.ListData = $('#myht').jexcel('getData');
            angular.forEach($scope.ListData, function (val, key) {
                if (val.Quantity !== null && val.Quantity !== "" && val.Quantity !== undefined) {
                    $rootScope.Quantity += $scope.roundNumber(parseFloat(val.Quantity));
                }
                if (val.UnitPrice !== null && val.UnitPrice !== "" && val.UnitPrice !== undefined) {
                    $rootScope.UnitPrice += $scope.roundNumber(parseFloat(val.UnitPrice));
                }
                if (val.AmountOc !== null && val.AmountOc !== "" && val.AmountOc !== undefined) {
                    $rootScope.AmountOc += $scope.roundNumber(parseFloat(val.AmountOc));
                }

            });
            $('#myht #colfoot-5').html($rootScope.addPeriod($scope.Quantity));
            $('#myht #colfoot-6').html($rootScope.addPeriod($scope.UnitPrice));
            $('#myht #colfoot-7').html($rootScope.addPeriod($scope.AmountOc));
            if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                $scope.$apply();
            }
        }
    }

    $scope.dropdownItemId = function (d1, d2, d3) {
        var _tem1 = d1.ItemID;
        if (_tem1 === "" || _tem1 === null) {
            _tem1 = "";
        }
        var obj = {
            Keyword: _tem1
        }
        dataservice.getItemALL(obj, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {

                d3.source = rs;
                d2(rs, "");
            }
        })
    }
    $scope.dropdownUnitID = function (d1, d2, d3) {
        var _tem1 = d1.UnitID;
        if (_tem1 === "" || _tem1 === null) {
            _tem1 = "";
        }
        var obj = {
            Keyword: _tem1
        }
        dataservice.getUnitJexcel(obj, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {

                d3.source = rs;
                d2(rs, "");
            }
        })
    }
    $scope.jxcelAddDetailInit = {
        dataObject: true,
        reloadData: {},
        data: [],
        colHeaders: ['', 'Tên và mô tả chi tiết', 'Số hoá đơn/hợp đồng', 'Mã đơn vị tính', 'Tên đơn vị tính', 'Số lượng', 'Đơn giá', 'Thành tiền', 'Tình trạng', 'Cách thức', 'Ghi chú'
        ],
        colTitles: ['', 'Tên và mô tả chi tiết', 'Số hoá đơn/hợp đồng', 'Mã đơn vị tính', 'Tên đơn vị tính', 'Số lượng', 'Đơn giá', 'Thành tiền', 'Tình trạng', 'Cách thức', 'Ghi chú'
        ],
        colFooters: ['', '', '', '', '', '', '', '', '', ''
        ],
        columns: [
            {
                name: 'RowGuid',
                type: 'text', ReadOnly: true

            },
            {
                name: 'ItemName',
                type: 'text'
                , ReadOnly: true
            },
            {
                name: 'NumberOrder',
                type: 'text'
                , ReadOnly: true
            },
            {
                name: 'UnitID',
                type: 'autocomplete',
                source: $rootScope.litsUnitID,
                sourceReload: $scope.dropdownUnitID,
                searchReload: true,
                onlyCode: true,
                ReadOnly: true, ReadOnly: true
            },
            {
                name: 'UnitName',
                type: 'text',
                ReadOnly: true
            },
            {
                name: 'Quantity',//Xếp hạng 3
                type: 'numeric', formatNumber: 'VN-NUMERIC', ReadOnly: true

            },
            {
                name: 'UnitPrice',//Xếp hạng 15
                type: 'numeric', formatNumber: 'VN-NUMERIC', ReadOnly: true

            },
            {
                name: 'AmountOc',//Xếp hạng 15
                type: 'numeric', formatNumber: 'VN-NUMERIC', ReadOnly: true

            },
            {
                name: 'StatusOrder',//Xếp hạng 15
                type: 'autocomplete',
                url: '/Sale/Accountants/StatusOrder/',
                updateReadOnly: true, ReadOnly: true
            }
            ,
            {
                name: 'Method',
                type: 'autocomplete',
                url: '/Sale/Logistics/BookingNumber/',
                updateReadOnly: true, ReadOnly: true
            },
            {
                name: 'Note',
                type: 'text', ReadOnly: true
            }
        ],

        columnShow: [false, true, true, true, true, true, true, true, true, true, true],
        initDataRows: ['', '', '', '', '', '', '', '', '', '', ''],
        colWidths: [50, 300, 200, 100, 150, 150, 150, 150, 100, 150, 300],
        allrowevent: true,
        allowInsertRow: false,
        deleterowconfirm: $scope.deleteItem,
        tableOverflow: true,
        csvHeaders: true,
        changeCell: $scope.rowchange,
        tableHeight: '200px',
        onpaste: $scope.onpaste1,
        onload: $scope.onload1,
        contextMenu: function () { return ""; },

    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
    var uploader = $scope.uploader = new FileUploader({
        // url: App.baseService() + '/upload'
    });
    uploader.filters.push({
        name: 'customFilter',
        fn: function (item /*{File|FileLikeObject}*/, options) {
            return this.queue.length < 10;
        }
    });
    $scope.initData = function () {
        dataservice.getById(para, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {
                $scope.model = JSON.parse(rs.Table1)[0];
                $scope.model.DateSX = $scope.model.DateSX !== null ? "/Date(" + new Date($scope.model.DateSX).getTime() + ")/" : null;
                $scope.lstDetail = JSON.parse(rs.Table2);
                var ApplyOutsideDetails = [];
                angular.forEach($scope.lstDetail, function (value, key) {
                    ApplyOutsideDetails.push([
                        value.RowGuid,
                        value.ItemName,
                        value.NumberOrder,
                        value.UnitID,
                        value.UnitName,
                        value.Quantity,
                        value.UnitPrice,
                        value.AmountOc,
                        value.StatusOrder,
                        value.Method,
                        value.Note
                    ]);
                });
                $scope.jxcelAddDetailInit.data = ApplyOutsideDetails;
                $scope.onload1();
                if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                    $scope.$apply();
                }
            }
        })
    }
    $scope.initData();
});
//Thông tin chứng từ xuất khẩu
app.controller('AccountantsOthers_index', function ($http, $scope, $rootScope, $compile, $confirm, $uibModal, $uibModalInstance, DTOptionsBuilder, DTColumnBuilder, DTInstances, keyhandle, FileUploader, dataservice, para) {
    var vm = $scope;
    $scope.TitleIndex = "Danh sách khoản thu khác";
    var ctrl = $scope;
    $scope.statusDisable = false;
    $scope.changeEdit = function (data) {
        if (data === 0) {
            $scope.statusDisable = false;
            $("div.SumoSelect").removeClass("SumoSelect disabled").addClass("SumoSelect");
        } else {
            $scope.statusDisable = true;
        }
    }
    ctrl.nextRow = function (d) {
        if (ctrl.ElementRowCheck === -1) {
            if (ctrl.allRow.length > 1) {
                ctrl.ElementRowCheck = ctrl.allRow[0].row;
                $('td', ctrl.ElementRowCheck).addClass("es-iSeleted");
                ctrl.iSelected = ctrl.allRow[0].data;
                $scope.getById_Customers(ctrl.iSelected.CustomerGuid);

            }
        } else
            for (var i = 0; i < ctrl.allRow.length; i++) {
                if (ctrl.allRow[i].row === ctrl.ElementRowCheck) {
                    if (i === ctrl.allRow.length - 1) break;
                    $('td', ctrl.ElementRowCheck).removeClass('es-iSeleted');
                    ctrl.ElementRowCheck = ctrl.allRow[i + 1].row;
                    $('td', ctrl.ElementRowCheck).addClass("es-iSeleted");
                    ctrl.iSelected = ctrl.allRow[i + 1].data;
                    $scope.getById_Customers(ctrl.iSelected.CustomerGuid);
                    break;
                }
            }
    }
    ctrl.upRow = function (d) {
        if (ctrl.ElementRowCheck === -1) {
            if (ctrl.allRow.length > 1) {
                ctrl.ElementRowCheck = ctrl.allRow[ctrl.allRow.length - 1].row;
                $('td', ctrl.ElementRowCheck).addClass("es-iSeleted");
                ctrl.iSelected = ctrl.allRow[ctrl.allRow.length - 1].row;
                $scope.getById_Customers(ctrl.iSelected.CustomerGuid);
            }
        } else
            for (var i = 0; i < ctrl.allRow.length; i++) {
                if (ctrl.allRow[i].row === ctrl.ElementRowCheck) {
                    if (i === 0) break;
                    $('td', ctrl.ElementRowCheck).removeClass('es-iSeleted');
                    ctrl.ElementRowCheck = ctrl.allRow[i - 1].row;
                    $('td', ctrl.ElementRowCheck).addClass("es-iSeleted");
                    ctrl.iSelected = ctrl.allRow[i - 1].data;
                    $scope.getById_Customers(ctrl.iSelected.CustomerGuid);

                    break;
                }
            }
    }
    keyhandle.eventCtrl(40, ctrl.nextRow);
    keyhandle.eventCtrl(38, ctrl.upRow);
    ctrl.ElementRowCheck = -1;
    ctrl.allRow = [];
    ctrl.iSelected = {};
    ctrl.dataScrollPage = [];
    $scope.selected = [];
    $scope.selectAll = false;
    $scope.toggleAll = toggleAll;
    $scope.toggleOne = toggleOne;
    $scope.dataload = [];
    $scope.CheckReload = false;
    $scope.staticParam = {
        totalItems: 0,
        CurrentPage: 1,
        pageSize: 25,
        maxSize: 5,
        Keyword: "",
        EmployeeId: null,
        CustomerGuid: null
    };
    $scope.txtComment = "";

    $scope.lstInt = [];
    $rootScope.TongGT = 0;
    $scope.selected = [];
    $scope.selectAll = false;
    $scope.toggleAll = toggleAll;
    $scope.toggleOne = toggleOne;
    var titleHtml = '<label class="mt-checkbox"><input type="checkbox" ng-model="selectAll" ng-click="toggleAll(selectAll, selected)"/><span></span></label>';

    vm.dtOptions = DTOptionsBuilder.newOptions()
        .withOption('serverSide', true)
        .withOption('order', [1, 'desc'])
        .withDataProp('data')
        .withOption('pageLength', 25)
        .withOption('initComplete', function (settings, json) {
            $('.dataTables_scrollBody').css({
                'overflow-y': 'scroll',
                'overflow-x': 'hidden',
                'border': 'none'
            });
            //hiền scroll footer
            $('#tblData_wrapper .dataTables_scrollFoot').css('overflow', 'auto');
            //cuộn toàn bảng theo footer
            $('#tblData_wrapper .dataTables_scrollFoot').on('scroll', function () {
                $('#tblData_wrapper .dataTables_scrollBody').scrollLeft($(this).scrollLeft());
            });

            //thêm sự kiện scroll phân trang
            $('#tblData').parent().attr("onscroll", "angular.element(this).scope().LoadScroll(this)");
        })
        .withOption('searching', false)
        .withOption('createdRow', function (row, data, dataIndex) {
            const contextScope = $scope.$new(true);
            contextScope.data = data;
            $scope.data = data;
            var obj = {
                STT: data._STT.toString(),
                Id: data.CustomerGuid
            }
            $scope.lstInt.push(obj);
            $compile(row)($scope);

            contextScope.contextMenu = $scope.contextMenu;
            $compile(angular.element(row).find('input'))($scope);
            $compile(angular.element(row).attr('context-menu', 'contextMenu'))(contextScope);
        });
    vm.dtOptions.withOption('ajax', function (data, callback, settings) {
        $scope.staticParam.CurrentPage = $scope.dtInstance.DataTable.page() + 1;
        if ($scope.CheckReload) {
            data.Start = 0;
        }
        App.blockUI({
            target: "#table_load",
            boxed: true,
            message: 'Đang tải...'
        });
        $http.post('/Accountants/JTable_AccountantsOthers', data)
            .success(function (res) {

                $scope.selected = [];
                if (data.start === 0) {
                    $scope.dataload = [];
                    angular.forEach(res.data, function (val, key) {
                        $scope.dataload.push(val);
                    });
                    callback({
                        data: $scope.dataload,
                        recordsTotal: res.recordsTotal,
                        recordsFiltered: res.recordsFiltered
                    });
                } else {
                    if (res.recordsTotal > $scope.dataload.length) {
                        angular.forEach(res.data, function (val, key) {
                            $scope.dataload.push(val);
                        });
                        callback({
                            data: $scope.dataload,
                            recordsTotal: res.recordsTotal,
                            recordsFiltered: res.recordsFiltered
                        });
                    }
                }
                if ($scope.dataload.length === 0) {
                    $scope.clear_Contracts();
                }
                App.unblockUI("#table_load");

            });
    });
    //scroll theo tỷ lệ màn hình
    vm.dtOptions.withOption('scrollY', '40vh')
        .withOption('scrollX', '100%') //mặc định 100%
        .withOption('scrollCollapse', true) //xóa khoảng trắng khi dữ liệu trong bảng ít
        .withOption('rowCallback', rowCallback) //sự kiên click vào 1 dòng
        .withOption('scroller', {
            loadingIndicator: true
        }).withFixedColumns({
            leftColumns: 0 //fix bên trái 2 cột
            //rightColumns: 1 //fix bên phải 1 cột
        });

    vm.dtColumns = [];

    vm.dtColumns.push(DTColumnBuilder.newColumn('_STT').withTitle('STT').notSortable().withOption('sClass', 'tcenter').withOption('sWidth', '5px').renderWith(function (data, type, full, meta) {
        return meta.row + 1;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('OrderID').withTitle('Số đơn hàng').withOption('sWidth', '60px').renderWith(function (data, type, full, meta) {
        return data;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('DatePS').withTitle('Ngày').withOption('sWidth', '60px').renderWith(function (data, type, full, meta) {
        return full.DatePSString;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('Title').withTitle('Số IV/MÔ TẢ').withOption('sWidth', '150px').renderWith(function (data, type, full, meta) {
        return data;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('UnitName').withTitle('Đơn vị tính').withOption('sWidth', '150px').renderWith(function (data, type, full, meta) {
        return data;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('Quantity').withTitle('Số lượng').withOption('sWidth', '120px').renderWith(function (data, type, full, meta) {
        return $rootScope.addPeriod(data);
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('UnitPrice').withTitle('Đơn giá').withOption('sWidth', '120px').renderWith(function (data, type, full, meta) {
        return $rootScope.addPeriod(data);
    }));

    vm.dtColumns.push(DTColumnBuilder.newColumn('AmountOc').withTitle('Thành tiền').withOption('sWidth', '100px').renderWith(function (data, type, full, meta) {
        return $rootScope.addPeriod(data);
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('EmployeeID').withTitle('Người yêu cầu').withOption('sWidth', '60px').renderWith(function (data, type, full, meta) {
        return data;
    }));
    //vm.dtColumns.push(DTColumnBuilder.newColumn('Note').withTitle('Lưu ý khác').withOption('sWidth', '120px').renderWith(function (data, type, full, meta) {
    //    return data;
    //}));

    vm.reloadData = reloadData;
    vm.dtInstance = {};
    function reloadData(resetPaging) {
        ctrl.ElementRowCheck = -1;
        $scope.staticParam.currentPage = resetPaging;
        vm.dtInstance.reloadData(callback, resetPaging);
        vm.dtInstance.DataTable.draw();
    }
    function callback(json) {

    }
    function toggleAll(selectAll, selectedItems) {
        for (var id in selectedItems) {
            if (selectedItems.hasOwnProperty(id)) {
                selectedItems[id] = selectAll;
            }
        }
    }
    function toggleOne(selectedItems) {
        for (var id in selectedItems) {
            if (selectedItems.hasOwnProperty(id)) {
                if (!selectedItems[id]) {
                    vm.selectAll = false;
                    return;
                }
            }
        }
        vm.selectAll = true;
    }
    $scope.total = 0;
    $scope.check = false;
    $scope.LoadScroll = function (obj) {
        var total = obj.offsetHeight + obj.scrollTop;
        if (obj.offsetHeight + obj.scrollTop >= obj.scrollHeight && $scope.check && $scope.total <= total) {
            $scope.check = false;
            try {
                $scope.CheckReload = false;
                vm.dtInstance.DataTable.page('next').draw('page');
            }
            catch (ex) {
            }
        }
        if (obj.offsetHeight + obj.scrollTop >= obj.scrollHeight) {
            $scope.total = obj.offsetHeight + obj.scrollTop;
        }
        else {
            $scope.check = true;
        }

    };
    function rowCallback(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        if (iDisplayIndexFull === 0) ctrl.allRow = [];
        $('td', nRow).unbind('click');
        $('td', nRow).bind('click', function () {
            if (vm.ElementRowCheck === nRow) return;
            $('td', nRow).addClass("es-iSeleted");
            $('td', vm.ElementRowCheck).removeClass('es-iSeleted');
            vm.ElementRowCheck = nRow;
            ctrl.ElementRowCheck = nRow;
            ctrl.iSelected = aData;
            vm.staticParam.CustomerGuid = aData.RowGuid;
            $scope.getById_Customers(vm.staticParam.CustomerGuid);
            $scope.changeEdit(1);
        });
        $('td', nRow).unbind('dblclick');
        $('td', nRow).bind('dblclick', function () {
            $scope.$apply(function () {
                // $scope.getById_Customers(aData.CustomerGuid);
            });
        });
        ctrl.allRow.push({ row: nRow, data: aData });
        return nRow;
    }
    $scope.reload = function () {
        ctrl.iSelected = [];
        $scope.lstInt = [];
        $scope.dataload = [];
        reloadData(true);
    };
    $scope.addPeriod = function (nStr1) {
        if (nStr1 !== null && nStr1 !== undefined && nStr1 !== "") {
            var nStr = nStr1.split('.');
            nStr += '';
            x = nStr.split(',');
            x1 = x[0];
            x2 = x.length > 1 ? ',' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
            }
            return x1;
        } else {
            return "";
        }
    }
    $scope.contextMenu = [
        [function ($itemScope) {
            return '<i class="fa fa-remove"></i> Xóa';
        }, function ($itemScope, $event, model) {
            ESConfirm([{ 'value': 1, 'title': 'Xác nhận', 'type': 'info' }, { 'value': 2, 'title': 'Đóng', 'type': 'danger' }], { 'description': 'bản ghi này không?', 'class': 'eswarning' }, function (rs) {
                if (rs === '1') {
                    App.blockUI({
                        target: "#contentMain",
                        boxed: true,
                        message: 'Đang tải...'
                    });
                    dataservice.Delete_AccountantsOthers($itemScope.data.RowGuid, function (result) {
                        if (result.Error) {
                            App.notifyDanger(result.Title);
                        } else {
                            App.notifyInfo(result.Title);
                            $scope.clear_Contracts();
                            $scope.reload();
                        }
                        App.unblockUI("#contentMain");
                    });
                }
                if (rs === '2') {
                    console.log('Không chấp nhận');
                }
            }, function () {
                //some error in this function
                console.log('Không lựa chọn');
            });

        }, function ($itemScope, $event, model) {
            return true;
        }],
        [function ($itemScope) {
            return '<i class="mdi mdi-attachment"></i> Đính kèm';
        }, function ($itemScope, $event, model) {
            $scope.AttchmentOrder($itemScope.data.RowGuid);
        }, function ($itemScope, $event, model) {
            return true;
        }]
    ];

    $scope.ConvertNumber = function (number) {
        if (number != null && number != "") {
            return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
        return null;
    }

    $rootScope.validationOptions = {
        rules: {
            DatePS: {
                required: true              
            }
        },
        messages: {
            DatePS: {
                required: "Yêu cầu nhập ngày"
               
            }
        }
    }
    $rootScope.StatusData = [{
        value: 1,
        text: 'Sử dụng'
    }, {
        value: 0,
        text: 'Không sử dụng'
    }];
    $rootScope.ListMethod = [{
        value: "C",
        text: 'Chuyển khoản'
    }, {
        value: "T",
        text: 'Tiền mặt'
    }

    ];
    $('#tblData').parent().removeAttr("onscroll");
    $scope.cancel = function () {
        $('#tblData').parent().removeAttr("onscroll");
        $('#tblData_Salaries').parent().removeAttr("onscroll");
        $('#tblData_Insurances').parent().removeAttr("onscroll");
        $('#tblData_Contracts').parent().removeAttr("onscroll");
        $('#tblData_Histories').parent().removeAttr("onscroll");
        $('#tblData_Certificates').parent().removeAttr("onscroll");
        $('#tblData_Reward').parent().removeAttr("onscroll");
        $('#tblData_TrainingRequirement').parent().removeAttr("onscroll");
        $uibModalInstance.dismiss('cancel');
    }

    //================Thêm hợp đồng===========================
    $scope.model = {};
    $scope.ChangeBusinessTypeName = function (data) {
        $scope.model.BusinessTypeName = data;
    }
    $scope.ChangeMethod = function (data) {
        $scope.model.Method = data;
    }
    $scope.ChangeOrderID = function (data) {
        $scope.model.OrderID = data;
    }
    $scope.AttchmentOrder = function (data) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/addAttchment.html',
            controller: 'addattchment',
            backdrop: 'static',
            size: '80',
            resolve: {
                para: function () {
                    return data;
                },
                data: function () {
                    return "";
                },
                para1: function () {
                    return $scope.openAttach;
                }
            }
        });
    }


    $scope.model.FileAttachments = [];
    var uploader = $scope.uploader = new FileUploader({
        // url: App.baseService() + '/upload'
    });
    uploader.filters.push({
        name: 'customFilter',
        fn: function (item /*{File|FileLikeObject}*/, options) {
            return this.queue.length < 10;
        }
    });
    uploader.onSuccessItem = function (fileItem, response, status, headers) {
        $scope.model.FileAttachments.push(response);
    };

    uploader.onCompleteAll = function () {
        $scope.commit();
    };

    $scope.getById_Customers = function (data) {
        dataservice.GetItem_AccountantsOthers(data, function (rs) {
            if (rs !== null) {
                $scope.model = rs;
                $scope.model.DatePS !== null ? $scope.model.DatePS = "/Date(" + new Date($scope.model.DatePS).getTime() + ")/" : undefined;
                $scope.statusDisable = false;

            } else {
                $scope.clear_Contracts();
            }
        });
    }
    $scope.clear_Contracts = function () {
        $scope.model = {};
        $scope.model.RowGuid = undefined;
        $scope.model.Title = "";
        $scope.model.CustomerName = "";
        $scope.model.Address = "";
        $scope.model.WorkPlace = "";
        $scope.model.ActivityFieldName = "";
        $scope.model.BusinessTypeName = "";
        $scope.model.LeadName = "";
        $scope.model.Method = "T";
        $scope.model.Note = "";
        $scope.model.DatePS = $rootScope.convertToJSONDate(new Date());
        $scope.lstDeleteFile = [];
        uploader.queue = [];
        $scope.jdataattach = [];
        $scope.statusDisable = false;
        $("div.SumoSelect").removeClass("SumoSelect disabled").addClass("SumoSelect");
    }
    $scope.ChangeNumber = function () {
        $scope.model.Quantity = $scope.model.Quantity > 0 ? $scope.model.Quantity : 0;
        $scope.model.UnitPrice = $scope.model.UnitPrice > 0 ? $scope.model.UnitPrice : 0;
        $scope.model.AmountOc = $scope.model.Quantity * $scope.model.UnitPrice;
    }

    $scope.lstDeleteFile = [];
    $scope.submit_Contracts = function () {
        if ($scope.addform.validate()) {           
            var fd = new FormData();

            $scope.model.IsLeads = true;
            fd.append('submit', JSON.stringify($scope.model));
            $.ajax({
                type: "POST",
                url: "/Accountants/Submit_AccountantsOthers",
                contentType: false,
                processData: false,
                data: fd,
                success: function (rs) {
                    if (rs.Error) {
                        App.notifyDanger(rs.Title);
                    } else {
                        if ($scope.model.RowGuid != undefined && $scope.model.RowGuid != null) {
                            App.notifyInfo("Cập nhật thành công");
                        } else {
                            App.notifyInfo("Thêm mới thành công");
                        }
                        $scope.reload();
                    }
                }
            });
        }
    };


});
app.controller('AccountantsManagement_index', function ($http, $scope, $rootScope, $compile, $confirm, $uibModal, $uibModalInstance, DTOptionsBuilder, DTColumnBuilder, DTInstances, keyhandle, FileUploader, dataservice, para) {
    var vm = $scope;
    $scope.TitleIndex = "Danh sách chi phí quản lý và nhân sự";
    var ctrl = $scope;
    $scope.statusDisable = false;
    $scope.changeEdit = function (data) {
        if (data === 0) {
            $scope.statusDisable = false;
            $("div.SumoSelect").removeClass("SumoSelect disabled").addClass("SumoSelect");
        } else {
            $scope.statusDisable = true;
        }
    }
    ctrl.nextRow = function (d) {
        if (ctrl.ElementRowCheck === -1) {
            if (ctrl.allRow.length > 1) {
                ctrl.ElementRowCheck = ctrl.allRow[0].row;
                $('td', ctrl.ElementRowCheck).addClass("es-iSeleted");
                ctrl.iSelected = ctrl.allRow[0].data;
                $scope.getById_Customers(ctrl.iSelected.CustomerGuid);

            }
        } else
            for (var i = 0; i < ctrl.allRow.length; i++) {
                if (ctrl.allRow[i].row === ctrl.ElementRowCheck) {
                    if (i === ctrl.allRow.length - 1) break;
                    $('td', ctrl.ElementRowCheck).removeClass('es-iSeleted');
                    ctrl.ElementRowCheck = ctrl.allRow[i + 1].row;
                    $('td', ctrl.ElementRowCheck).addClass("es-iSeleted");
                    ctrl.iSelected = ctrl.allRow[i + 1].data;
                    $scope.getById_Customers(ctrl.iSelected.CustomerGuid);
                    break;
                }
            }
    }
    ctrl.upRow = function (d) {
        if (ctrl.ElementRowCheck === -1) {
            if (ctrl.allRow.length > 1) {
                ctrl.ElementRowCheck = ctrl.allRow[ctrl.allRow.length - 1].row;
                $('td', ctrl.ElementRowCheck).addClass("es-iSeleted");
                ctrl.iSelected = ctrl.allRow[ctrl.allRow.length - 1].row;
                $scope.getById_Customers(ctrl.iSelected.CustomerGuid);
            }
        } else
            for (var i = 0; i < ctrl.allRow.length; i++) {
                if (ctrl.allRow[i].row === ctrl.ElementRowCheck) {
                    if (i === 0) break;
                    $('td', ctrl.ElementRowCheck).removeClass('es-iSeleted');
                    ctrl.ElementRowCheck = ctrl.allRow[i - 1].row;
                    $('td', ctrl.ElementRowCheck).addClass("es-iSeleted");
                    ctrl.iSelected = ctrl.allRow[i - 1].data;
                    $scope.getById_Customers(ctrl.iSelected.CustomerGuid);

                    break;
                }
            }
    }
    keyhandle.eventCtrl(40, ctrl.nextRow);
    keyhandle.eventCtrl(38, ctrl.upRow);
    ctrl.ElementRowCheck = -1;
    ctrl.allRow = [];
    ctrl.iSelected = {};
    ctrl.dataScrollPage = [];
    $scope.selected = [];
    $scope.selectAll = false;
    $scope.toggleAll = toggleAll;
    $scope.toggleOne = toggleOne;
    $scope.dataload = [];
    $scope.CheckReload = false;
    var date = new Date();
    $scope.staticParam = {
        totalItems: 0,
        CurrentPage: 1,
        pageSize: 25,
        maxSize: 5,
        Keyword: "",
        EmployeeId: null,
        CustomerGuid: null,
        Month: [(date.getMonth() + 1).toString()],
        Year: date.getFullYear()
    };
   
    $scope.txtComment = "";

    $scope.lstInt = [];
    $rootScope.TongGT = 0;
    $scope.selected = [];
    $scope.selectAll = false;
    $scope.toggleAll = toggleAll;
    $scope.toggleOne = toggleOne;
    var titleHtml = '<label class="mt-checkbox"><input type="checkbox" ng-model="selectAll" ng-click="toggleAll(selectAll, selected)"/><span></span></label>';

    vm.dtOptions = DTOptionsBuilder.newOptions()
        .withOption('serverSide', true)
        .withOption('order', [2, 'desc'])
        .withDataProp('data')
        .withOption('pageLength', 25)
        .withOption('initComplete', function (settings, json) {
            $('.dataTables_scrollBody').css({
                'overflow-y': 'scroll',
                'overflow-x': 'hidden',
                'border': 'none'
            });
            //hiền scroll footer
            $('#tblData_wrapper .dataTables_scrollFoot').css('overflow', 'auto');
            //cuộn toàn bảng theo footer
            $('#tblData_wrapper .dataTables_scrollFoot').on('scroll', function () {
                $('#tblData_wrapper .dataTables_scrollBody').scrollLeft($(this).scrollLeft());
            });

            //thêm sự kiện scroll phân trang
            $('#tblData').parent().attr("onscroll", "angular.element(this).scope().LoadScroll(this)");
        })
        .withOption('searching', false)
        .withOption('createdRow', function (row, data, dataIndex) {
            const contextScope = $scope.$new(true);
            contextScope.data = data;
            $scope.data = data;
            var obj = {
                STT: data._STT.toString(),
                Id: data.CustomerGuid
            }
            $scope.lstInt.push(obj);
            $compile(row)($scope);

            contextScope.contextMenu = $scope.contextMenu;
            $compile(angular.element(row).find('input'))($scope);
            $compile(angular.element(row).attr('context-menu', 'contextMenu'))(contextScope);
        });
    vm.dtOptions.withOption('ajax', function (data, callback, settings) {
        $scope.staticParam.CurrentPage = $scope.dtInstance.DataTable.page() + 1;
        data.lstString = $scope.staticParam.Month != "" ? $scope.staticParam.Month : [];
        data.Years = $scope.staticParam.Year;
        if ($scope.CheckReload) {
            data.Start = 0;
        }
        App.blockUI({
            target: "#table_load",
            boxed: true,
            message: 'Đang tải...'
        });
        $http.post('/Accountants/JTable_AccountantsManagement', data)
            .success(function (res) {

                $scope.selected = [];
                if (data.start === 0) {
                    $scope.dataload = [];
                    angular.forEach(res.data, function (val, key) {
                        $scope.dataload.push(val);
                    });
                    callback({
                        data: $scope.dataload,
                        recordsTotal: res.recordsTotal,
                        recordsFiltered: res.recordsFiltered
                    });
                } else {
                    if (res.recordsTotal > $scope.dataload.length) {
                        angular.forEach(res.data, function (val, key) {
                            $scope.dataload.push(val);
                        });
                        callback({
                            data: $scope.dataload,
                            recordsTotal: res.recordsTotal,
                            recordsFiltered: res.recordsFiltered
                        });
                    }
                }
                if ($scope.dataload.length === 0) {
                    $scope.clear_Contracts();
                }
                App.unblockUI("#table_load");

            });
    });
    //scroll theo tỷ lệ màn hình
    vm.dtOptions.withOption('scrollY', '40vh')
        .withOption('scrollX', '100%') //mặc định 100%
        .withOption('scrollCollapse', true) //xóa khoảng trắng khi dữ liệu trong bảng ít
        .withOption('rowCallback', rowCallback) //sự kiên click vào 1 dòng
        .withOption('scroller', {
            loadingIndicator: true
        }).withFixedColumns({
            leftColumns: 0 //fix bên trái 2 cột
            //rightColumns: 1 //fix bên phải 1 cột
        });

    vm.dtColumns = [];

    vm.dtColumns.push(DTColumnBuilder.newColumn('_STT').withTitle('STT').notSortable().withOption('sClass', 'tcenter').withOption('sWidth', '5px').renderWith(function (data, type, full, meta) {
        return meta.row + 1;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('DatePS').withTitle('Ngày').withOption('sWidth', '60px').renderWith(function (data, type, full, meta) {
        return full.DatePSString;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('Title').withTitle('Tên chi phí').withOption('sWidth', '150px').renderWith(function (data, type, full, meta) {
        return data;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('Description').withTitle('Mô tả').withOption('sWidth', '150px').renderWith(function (data, type, full, meta) {
        return data;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('UnitName').withTitle('Đơn vị tính').withOption('sWidth', '120px').renderWith(function (data, type, full, meta) {
        return data;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('Quantity').withTitle('Số lượng').withOption('sWidth', '120px').renderWith(function (data, type, full, meta) {
        return $rootScope.addPeriod(data);
    }));

    vm.dtColumns.push(DTColumnBuilder.newColumn('UnitPrice').withTitle('Đơn giá').withOption('sWidth', '100px').renderWith(function (data, type, full, meta) {
        return $rootScope.addPeriod(data);
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('AmountOc').withTitle('Thành tiền').withOption('sWidth', '100px').renderWith(function (data, type, full, meta) {
        return $rootScope.addPeriod(data);
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('EmployeeID').withTitle('Người yêu cầu').withOption('sWidth', '60px').renderWith(function (data, type, full, meta) {
        return data;
    }));
    //vm.dtColumns.push(DTColumnBuilder.newColumn('Note').withTitle('Lưu ý khác').withOption('sWidth', '120px').renderWith(function (data, type, full, meta) {
    //    return data;
    //}));

    vm.reloadData = reloadData;
    vm.dtInstance = {};
    function reloadData(resetPaging) {
        ctrl.ElementRowCheck = -1;
        $scope.staticParam.currentPage = resetPaging;
        vm.dtInstance.reloadData(callback, resetPaging);
        vm.dtInstance.DataTable.draw();
    }
    function callback(json) {

    }
    function toggleAll(selectAll, selectedItems) {
        for (var id in selectedItems) {
            if (selectedItems.hasOwnProperty(id)) {
                selectedItems[id] = selectAll;
            }
        }
    }
    function toggleOne(selectedItems) {
        for (var id in selectedItems) {
            if (selectedItems.hasOwnProperty(id)) {
                if (!selectedItems[id]) {
                    vm.selectAll = false;
                    return;
                }
            }
        }
        vm.selectAll = true;
    }
    $scope.total = 0;
    $scope.check = false;
    $scope.LoadScroll = function (obj) {
        var total = obj.offsetHeight + obj.scrollTop;
        if (obj.offsetHeight + obj.scrollTop >= obj.scrollHeight && $scope.check && $scope.total <= total) {
            $scope.check = false;
            try {
                $scope.CheckReload = false;
                vm.dtInstance.DataTable.page('next').draw('page');
            }
            catch (ex) {
            }
        }
        if (obj.offsetHeight + obj.scrollTop >= obj.scrollHeight) {
            $scope.total = obj.offsetHeight + obj.scrollTop;
        }
        else {
            $scope.check = true;
        }

    };
    function rowCallback(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        if (iDisplayIndexFull === 0) ctrl.allRow = [];
        $('td', nRow).unbind('click');
        $('td', nRow).bind('click', function () {
            if (vm.ElementRowCheck === nRow) return;
            $('td', nRow).addClass("es-iSeleted");
            $('td', vm.ElementRowCheck).removeClass('es-iSeleted');
            vm.ElementRowCheck = nRow;
            ctrl.ElementRowCheck = nRow;
            ctrl.iSelected = aData;
            vm.staticParam.CustomerGuid = aData.RowGuid;
            $scope.getById_Customers(vm.staticParam.CustomerGuid);
            $scope.changeEdit(1);
        });
        $('td', nRow).unbind('dblclick');
        $('td', nRow).bind('dblclick', function () {
            $scope.$apply(function () {
                $scope.getById_Customers(aData.RowGuid);
            });
        });
        ctrl.allRow.push({ row: nRow, data: aData });
        return nRow;
    }
    $scope.reload = function () {
        ctrl.iSelected = [];
        $scope.lstInt = [];
        $scope.dataload = [];
        reloadData(true);
    };
    $scope.addPeriod = function (nStr1) {
        if (nStr1 !== null && nStr1 !== undefined && nStr1 !== "") {
            var nStr = nStr1.split('.');
            nStr += '';
            x = nStr.split(',');
            x1 = x[0];
            x2 = x.length > 1 ? ',' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
            }
            return x1;
        } else {
            return "";
        }
    }
    $scope.contextMenu = [
        [function ($itemScope) {
            return '<i class="fa fa-remove"></i> Xóa';
        }, function ($itemScope, $event, model) {
            ESConfirm([{ 'value': 1, 'title': 'Xác nhận', 'type': 'info' }, { 'value': 2, 'title': 'Đóng', 'type': 'danger' }], { 'description': 'bản ghi này không?', 'class': 'eswarning' }, function (rs) {
                if (rs === '1') {
                    App.blockUI({
                        target: "#contentMain",
                        boxed: true,
                        message: 'Đang tải...'
                    });
                    dataservice.Delete_AccountantsManagement($itemScope.data.RowGuid, function (result) {
                        if (result.Error) {
                            App.notifyDanger(result.Title);
                        } else {
                            App.notifyInfo(result.Title);
                            $scope.clear_Contracts();
                            $scope.reload();
                        }
                        App.unblockUI("#contentMain");
                    });
                }
                if (rs === '2') {
                    console.log('Không chấp nhận');
                }
            }, function () {
                //some error in this function
                console.log('Không lựa chọn');
            });

        }, function ($itemScope, $event, model) {
            return true;
        }],
        [function ($itemScope) {
            return '<i class="mdi mdi-attachment"></i> Đính kèm';
        }, function ($itemScope, $event, model) {
            $scope.AttchmentOrder($itemScope.data.RowGuid);
        }, function ($itemScope, $event, model) {
            return true;
        }]
    ];
    $scope.AttchmentOrder = function (data) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/addAttchment.html',
            controller: 'addattchment',
            backdrop: 'static',
            size: '80',
            resolve: {
                para: function () {
                    return data;
                },
                data: function () {
                    return "";
                },
                para1: function () {
                    return $scope.openAttach;
                }
            }
        });
    }

    $scope.ConvertNumber = function (number) {
        if (number != null && number != "") {
            return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
        return null;
    }
    $rootScope.validationOptions = {
        rules: {
            CodeID: {
                required: true,
                maxlength: 20
            },
            DatePS: {
                required: true
            },
        },
        messages: {
            CodeID: {
                required: "Yêu cầu nhập mã chứng từ.",
                maxlength: "Mã chứng từ không vượt quá 20 ký tự."
            },
            DatePS: {
                required: "Yêu cầu nhập ngày."
            }
        }
    }
    $rootScope.StatusData = [{
        value: 1,
        text: 'Sử dụng'
    }, {
        value: 0,
        text: 'Không sử dụng'
    }];
    $rootScope.ListMethod = [{
        value: "C",
        text: 'Chuyển khoản'
    }, {
        value: "T",
        text: 'Tiền mặt'
    }

    ];
    $('#tblData').parent().removeAttr("onscroll");
    $scope.cancel = function () {
        $('#tblData').parent().removeAttr("onscroll");
        $('#tblData_Salaries').parent().removeAttr("onscroll");
        $('#tblData_Insurances').parent().removeAttr("onscroll");
        $('#tblData_Contracts').parent().removeAttr("onscroll");
        $('#tblData_Histories').parent().removeAttr("onscroll");
        $('#tblData_Certificates').parent().removeAttr("onscroll");
        $('#tblData_Reward').parent().removeAttr("onscroll");
        $('#tblData_TrainingRequirement').parent().removeAttr("onscroll");
        $uibModalInstance.dismiss('cancel');
    }

    //================Thêm hợp đồng===========================
    $scope.model = {};
    $scope.ChangeUnitID = function (data) {
        $scope.model.UnitID = data;
    }
    $scope.ChangeStatusOrder = function (data) {
        $scope.model.StatusOrder = data;
    }
    $scope.ChangeMethod = function (data) {
        $scope.model.Method = data;
    }
    $scope.ChangeMonth = function (data) {
        var filtered = data.filter(function (el) {
            return el != "null";
        });
        $scope.staticParam.Month = filtered;
        if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
            $scope.$apply();
        }
        $scope.reload();
    };
    $scope.ChangeYear = function (data) {
        $scope.staticParam.Year = data;
        if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
            $scope.$apply();
        }
        $scope.reload();
    };
    $scope.ChangeNumber = function () {
        $scope.model.Quantity = $scope.model.Quantity > 0 ? $scope.model.Quantity : 0;
        $scope.model.UnitPrice = $scope.model.UnitPrice > 0 ? $scope.model.UnitPrice : 0;
        $scope.model.AmountOc = $scope.model.Quantity * $scope.model.UnitPrice;
    }
    $scope.model.FileAttachments = [];
    var uploader = $scope.uploader = new FileUploader({
        // url: App.baseService() + '/upload'
    });
    uploader.filters.push({
        name: 'customFilter',
        fn: function (item /*{File|FileLikeObject}*/, options) {
            return this.queue.length < 10;
        }
    });
    uploader.onSuccessItem = function (fileItem, response, status, headers) {
        $scope.model.FileAttachments.push(response);
    };

    uploader.onCompleteAll = function () {
        $scope.commit();
    };

    $scope.getById_Customers = function (data) {
        dataservice.GetItem_AccountantsManagement(data, function (rs) {
            if (rs !== null) {
                $scope.model = rs;
                $scope.model.DatePS !== null ? $scope.model.DatePS = "/Date(" + new Date($scope.model.DatePS).getTime() + ")/" : undefined;
                $scope.statusDisable = false;

            } else {
                $scope.clear_Contracts();
            }
        });
    }
    $scope.clear_Contracts = function () {
        $scope.model = {};
        $scope.model.RowGuid = undefined;
        $scope.model.Title = "";
        $scope.model.CustomerName = "";
        $scope.model.Address = "";
        $scope.model.WorkPlace = "";
        $scope.model.ActivityFieldName = "";
        $scope.model.BusinessTypeName = "";
        $scope.model.LeadName = "";
        $scope.model.Method = "T";
        $scope.model.Note = "";
        $scope.model.DatePS = $rootScope.convertToJSONDate(new Date());
        $scope.lstDeleteFile = [];
        uploader.queue = [];
        $scope.jdataattach = [];
        $scope.statusDisable = false;
        $("div.SumoSelect").removeClass("SumoSelect disabled").addClass("SumoSelect");
    }
    $scope.lstDeleteFile = [];
    $scope.submit_Contracts = function () {
        if ($scope.addform.validate()) {
            var fd = new FormData();
            $scope.model.IsLeads = true;
            fd.append('submit', JSON.stringify($scope.model));
            $.ajax({
                type: "POST",
                url: "/Accountants/Submit_AccountantsManagement",
                contentType: false,
                processData: false,
                data: fd,
                success: function (rs) {
                    if (rs.Error) {
                        App.notifyDanger(rs.Title);
                    } else {
                        if ($scope.model.RowGuid != undefined && $scope.model.RowGuid != null) {
                            App.notifyInfo("Cập nhật thành công");
                        } else {
                            App.notifyInfo("Thêm mới thành công");
                        }
                        $scope.reload();
                    }
                }
            });
        }
    };


});
app.controller('AccountantsDebt_index', function ($http, $scope, $rootScope, $compile, $confirm, $uibModal, $uibModalInstance, DTOptionsBuilder, DTColumnBuilder, DTInstances, keyhandle, FileUploader, dataservice, para) {
    var vm = $scope;
    $scope.TitleIndex = "Danh sách công nợ giấy tờ";
    var ctrl = $scope;
    $scope.statusDisable = false;
    $scope.changeEdit = function (data) {
        if (data === 0) {
            $scope.statusDisable = false;
            $("div.SumoSelect").removeClass("SumoSelect disabled").addClass("SumoSelect");
        } else {
            $scope.statusDisable = true;
        }
    }
    ctrl.nextRow = function (d) {
        if (ctrl.ElementRowCheck === -1) {
            if (ctrl.allRow.length > 1) {
                ctrl.ElementRowCheck = ctrl.allRow[0].row;
                $('td', ctrl.ElementRowCheck).addClass("es-iSeleted");
                ctrl.iSelected = ctrl.allRow[0].data;
                $scope.getById_Customers(ctrl.iSelected.CustomerGuid);

            }
        } else
            for (var i = 0; i < ctrl.allRow.length; i++) {
                if (ctrl.allRow[i].row === ctrl.ElementRowCheck) {
                    if (i === ctrl.allRow.length - 1) break;
                    $('td', ctrl.ElementRowCheck).removeClass('es-iSeleted');
                    ctrl.ElementRowCheck = ctrl.allRow[i + 1].row;
                    $('td', ctrl.ElementRowCheck).addClass("es-iSeleted");
                    ctrl.iSelected = ctrl.allRow[i + 1].data;
                    $scope.getById_Customers(ctrl.iSelected.CustomerGuid);
                    break;
                }
            }
    }
    ctrl.upRow = function (d) {
        if (ctrl.ElementRowCheck === -1) {
            if (ctrl.allRow.length > 1) {
                ctrl.ElementRowCheck = ctrl.allRow[ctrl.allRow.length - 1].row;
                $('td', ctrl.ElementRowCheck).addClass("es-iSeleted");
                ctrl.iSelected = ctrl.allRow[ctrl.allRow.length - 1].row;
                $scope.getById_Customers(ctrl.iSelected.CustomerGuid);
            }
        } else
            for (var i = 0; i < ctrl.allRow.length; i++) {
                if (ctrl.allRow[i].row === ctrl.ElementRowCheck) {
                    if (i === 0) break;
                    $('td', ctrl.ElementRowCheck).removeClass('es-iSeleted');
                    ctrl.ElementRowCheck = ctrl.allRow[i - 1].row;
                    $('td', ctrl.ElementRowCheck).addClass("es-iSeleted");
                    ctrl.iSelected = ctrl.allRow[i - 1].data;
                    $scope.getById_Customers(ctrl.iSelected.CustomerGuid);

                    break;
                }
            }
    }
    keyhandle.eventCtrl(40, ctrl.nextRow);
    keyhandle.eventCtrl(38, ctrl.upRow);
    ctrl.ElementRowCheck = -1;
    ctrl.allRow = [];
    ctrl.iSelected = {};
    ctrl.dataScrollPage = [];
    $scope.selected = [];
    $scope.selectAll = false;
    $scope.toggleAll = toggleAll;
    $scope.toggleOne = toggleOne;
    $scope.dataload = [];
    $scope.CheckReload = false;
    var date = new Date();
    $scope.staticParam = {
        totalItems: 0,
        CurrentPage: 1,
        pageSize: 25,
        maxSize: 5,
        Keyword: "",
        EmployeeId: null,
        CustomerGuid: null,
        Month: [(date.getMonth() + 1).toString()],
        Year: date.getFullYear()
    };
    $scope.txtComment = "";

    $scope.lstInt = [];
    $rootScope.TongGT = 0;
    $scope.selected = [];
    $scope.selectAll = false;
    $scope.toggleAll = toggleAll;
    $scope.toggleOne = toggleOne;
    var titleHtml = '<label class="mt-checkbox"><input type="checkbox" ng-model="selectAll" ng-click="toggleAll(selectAll, selected)"/><span></span></label>';

    vm.dtOptions = DTOptionsBuilder.newOptions()
        .withOption('serverSide', true)
        .withOption('order', [1, 'desc'])
        .withDataProp('data')
        .withOption('pageLength', 25)
        .withOption('initComplete', function (settings, json) {
            $('.dataTables_scrollBody').css({
                'overflow-y': 'scroll',
                'overflow-x': 'hidden',
                'border': 'none'
            });
            //hiền scroll footer
            $('#tblData_wrapper .dataTables_scrollFoot').css('overflow', 'auto');
            //cuộn toàn bảng theo footer
            $('#tblData_wrapper .dataTables_scrollFoot').on('scroll', function () {
                $('#tblData_wrapper .dataTables_scrollBody').scrollLeft($(this).scrollLeft());
            });

            //thêm sự kiện scroll phân trang
            $('#tblData').parent().attr("onscroll", "angular.element(this).scope().LoadScroll(this)");
        })
        .withOption('searching', false)
        .withOption('createdRow', function (row, data, dataIndex) {
            const contextScope = $scope.$new(true);
            contextScope.data = data;
            $scope.data = data;
            var obj = {
                STT: data._STT.toString(),
                Id: data.CustomerGuid
            }
            $scope.lstInt.push(obj);
            $compile(row)($scope);

            contextScope.contextMenu = $scope.contextMenu;
            $compile(angular.element(row).find('input'))($scope);
            $compile(angular.element(row).attr('context-menu', 'contextMenu'))(contextScope);
        });
    vm.dtOptions.withOption('ajax', function (data, callback, settings) {
        $scope.staticParam.CurrentPage = $scope.dtInstance.DataTable.page() + 1;
        data.lstString = $scope.staticParam.Month != "" ? $scope.staticParam.Month : [];
        data.Years = $scope.staticParam.Year;
        if ($scope.CheckReload) {
            data.Start = 0;
        }
        App.blockUI({
            target: "#table_load",
            boxed: true,
            message: 'Đang tải...'
        });
        $http.post('/Accountants/JTable_AccountantsDebt', data)
            .success(function (res) {

                $scope.selected = [];
                if (data.start === 0) {
                    $scope.dataload = [];
                    angular.forEach(res.data, function (val, key) {
                        $scope.dataload.push(val);
                    });
                    callback({
                        data: $scope.dataload,
                        recordsTotal: res.recordsTotal,
                        recordsFiltered: res.recordsFiltered
                    });
                } else {
                    if (res.recordsTotal > $scope.dataload.length) {
                        angular.forEach(res.data, function (val, key) {
                            $scope.dataload.push(val);
                        });
                        callback({
                            data: $scope.dataload,
                            recordsTotal: res.recordsTotal,
                            recordsFiltered: res.recordsFiltered
                        });
                    }
                }
                if ($scope.dataload.length === 0) {
                    $scope.clear_Contracts();
                }
                App.unblockUI("#table_load");

            });
    });
    //scroll theo tỷ lệ màn hình
    vm.dtOptions.withOption('scrollY', '40vh')
        .withOption('scrollX', '100%') //mặc định 100%
        .withOption('scrollCollapse', true) //xóa khoảng trắng khi dữ liệu trong bảng ít
        .withOption('rowCallback', rowCallback) //sự kiên click vào 1 dòng
        .withOption('scroller', {
            loadingIndicator: true
        }).withFixedColumns({
            leftColumns: 0 //fix bên trái 2 cột
            //rightColumns: 1 //fix bên phải 1 cột
        });

    vm.dtColumns = [];

    vm.dtColumns.push(DTColumnBuilder.newColumn('_STT').withTitle('STT').notSortable().withOption('sClass', 'tcenter').withOption('sWidth', '5px').renderWith(function (data, type, full, meta) {
        return meta.row + 1;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('CodeID').withTitle('Số ký hiệu').withOption('sWidth', '60px').renderWith(function (data, type, full, meta) {
        return data;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('Title').withTitle('Tên loại giấy tờ').withOption('sWidth', '150px').renderWith(function (data, type, full, meta) {
        return data;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('DateVBString').withTitle('Ngày văn bản').withOption('sClass', 'tcenter').withOption('sWidth', '80px').renderWith(function (data, type, full, meta) {
        return data;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('EmployeeName').withTitle('Tên đối tác').withOption('sWidth', '120px').renderWith(function (data, type, full, meta) {
        return data;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('UnitName').withTitle('Đơn vị tính').withOption('sWidth', '120px').renderWith(function (data, type, full, meta) {
        return data;
    }));

    vm.dtColumns.push(DTColumnBuilder.newColumn('Quantity').withTitle('Số lượng').withOption('sWidth', '100px').renderWith(function (data, type, full, meta) {
        return $rootScope.addPeriod(data);
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('EmployeeID').withTitle('Người thực hiện').withOption('sWidth', '60px').renderWith(function (data, type, full, meta) {
        return data;
    }));
    //vm.dtColumns.push(DTColumnBuilder.newColumn('Note').withTitle('Lưu ý khác').withOption('sWidth', '120px').renderWith(function (data, type, full, meta) {
    //    return data;
    //}));

    vm.reloadData = reloadData;
    vm.dtInstance = {};
    function reloadData(resetPaging) {
        ctrl.ElementRowCheck = -1;
        $scope.staticParam.currentPage = resetPaging;
        vm.dtInstance.reloadData(callback, resetPaging);
        vm.dtInstance.DataTable.draw();
    }
    function callback(json) {

    }
    function toggleAll(selectAll, selectedItems) {
        for (var id in selectedItems) {
            if (selectedItems.hasOwnProperty(id)) {
                selectedItems[id] = selectAll;
            }
        }
    }
    function toggleOne(selectedItems) {
        for (var id in selectedItems) {
            if (selectedItems.hasOwnProperty(id)) {
                if (!selectedItems[id]) {
                    vm.selectAll = false;
                    return;
                }
            }
        }
        vm.selectAll = true;
    }
    $scope.total = 0;
    $scope.check = false;
    $scope.LoadScroll = function (obj) {
        var total = obj.offsetHeight + obj.scrollTop;
        if (obj.offsetHeight + obj.scrollTop >= obj.scrollHeight && $scope.check && $scope.total <= total) {
            $scope.check = false;
            try {
                $scope.CheckReload = false;
                vm.dtInstance.DataTable.page('next').draw('page');
            }
            catch (ex) {
            }
        }
        if (obj.offsetHeight + obj.scrollTop >= obj.scrollHeight) {
            $scope.total = obj.offsetHeight + obj.scrollTop;
        }
        else {
            $scope.check = true;
        }

    };
    function rowCallback(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        if (iDisplayIndexFull === 0) ctrl.allRow = [];
        $('td', nRow).unbind('click');
        $('td', nRow).bind('click', function () {
            if (vm.ElementRowCheck === nRow) return;
            $('td', nRow).addClass("es-iSeleted");
            $('td', vm.ElementRowCheck).removeClass('es-iSeleted');
            vm.ElementRowCheck = nRow;
            ctrl.ElementRowCheck = nRow;
            ctrl.iSelected = aData;
            vm.staticParam.CustomerGuid = aData.RowGuid;
            $scope.getById_Customers(vm.staticParam.CustomerGuid);
            $scope.changeEdit(1);
        });
        $('td', nRow).unbind('dblclick');
        $('td', nRow).bind('dblclick', function () {
            $scope.$apply(function () {
                // $scope.getById_Customers(aData.CustomerGuid);
            });
        });
        ctrl.allRow.push({ row: nRow, data: aData });
        return nRow;
    }
    $scope.reload = function () {
        ctrl.iSelected = [];
        $scope.lstInt = [];
        $scope.dataload = [];
        reloadData(true);
    };
    $scope.addPeriod = function (nStr1) {
        if (nStr1 !== null && nStr1 !== undefined && nStr1 !== "") {
            var nStr = nStr1.split('.');
            nStr += '';
            x = nStr.split(',');
            x1 = x[0];
            x2 = x.length > 1 ? ',' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
            }
            return x1;
        } else {
            return "";
        }
    }
    $scope.contextMenu = [
        [function ($itemScope) {
            return '<i class="fa fa-remove"></i> Xóa';
        }, function ($itemScope, $event, model) {
            ESConfirm([{ 'value': 1, 'title': 'Xác nhận', 'type': 'info' }, { 'value': 2, 'title': 'Đóng', 'type': 'danger' }], { 'description': 'bản ghi này không?', 'class': 'eswarning' }, function (rs) {
                if (rs === '1') {
                    App.blockUI({
                        target: "#contentMain",
                        boxed: true,
                        message: 'Đang tải...'
                    });
                    dataservice.Delete_AccountantsDebt($itemScope.data.RowGuid, function (result) {
                        if (result.Error) {
                            App.notifyDanger(result.Title);
                        } else {
                            App.notifyInfo(result.Title);
                            $scope.clear_Contracts();
                            $scope.reload();
                        }
                        App.unblockUI("#contentMain");
                    });
                }
                if (rs === '2') {
                    console.log('Không chấp nhận');
                }
            }, function () {
                //some error in this function
                console.log('Không lựa chọn');
            });

        }, function ($itemScope, $event, model) {
            return true;
        }], [function ($itemScope) {
            return '<i class="mdi mdi-attachment"></i> Đính kèm';
        }, function ($itemScope, $event, model) {
            $scope.AttchmentOrder($itemScope.data.RowGuid);
        }, function ($itemScope, $event, model) {
            return true;
        }]
    ];
    $scope.AttchmentOrder = function (data) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/addAttchment.html',
            controller: 'addattchment',
            backdrop: 'static',
            size: '80',
            resolve: {
                para: function () {
                    return data;
                },
                data: function () {
                    return "";
                },
                para1: function () {
                    return $scope.openAttach;
                }
            }
        });
    }
    $scope.ConvertNumber = function (number) {
        if (number != null && number != "") {
            return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
        return null;
    }

    $rootScope.validationOptions = {
        rules: {
            CodeID: {
                required: true,
                maxlength: 20
            }
        },
        messages: {
            CodeID: {
                required: "Yêu cầu nhập mã chứng từ.",
                maxlength: "Mã chứng từ không vượt quá 20 ký tự."
            }
        }
    }
    $rootScope.StatusData = [{
        value: 1,
        text: 'Sử dụng'
    }, {
        value: 0,
        text: 'Không sử dụng'
    }];
    $rootScope.ListMethod = [{
        value: "C",
        text: 'Chuyển khoản'
    }, {
        value: "T",
        text: 'Tiền mặt'
    }

    ];
    $('#tblData').parent().removeAttr("onscroll");
    $scope.cancel = function () {
        $('#tblData').parent().removeAttr("onscroll");
        $('#tblData_Salaries').parent().removeAttr("onscroll");
        $('#tblData_Insurances').parent().removeAttr("onscroll");
        $('#tblData_Contracts').parent().removeAttr("onscroll");
        $('#tblData_Histories').parent().removeAttr("onscroll");
        $('#tblData_Certificates').parent().removeAttr("onscroll");
        $('#tblData_Reward').parent().removeAttr("onscroll");
        $('#tblData_TrainingRequirement').parent().removeAttr("onscroll");
        $uibModalInstance.dismiss('cancel');
    }

    //================Thêm hợp đồng===========================
    $scope.model = {};
    $scope.ChangeBusinessTypeName = function (data) {
        $scope.model.BusinessTypeName = data;
    }
    $scope.ChangeMethod = function (data) {
        $scope.model.Method = data;
    }
    $scope.model.FileAttachments = [];
    var uploader = $scope.uploader = new FileUploader({
        // url: App.baseService() + '/upload'
    });
    uploader.filters.push({
        name: 'customFilter',
        fn: function (item /*{File|FileLikeObject}*/, options) {
            return this.queue.length < 10;
        }
    });
    uploader.onSuccessItem = function (fileItem, response, status, headers) {
        $scope.model.FileAttachments.push(response);
    };

    uploader.onCompleteAll = function () {
        $scope.commit();
    };
    $scope.ChangeMonth = function (data) {
        var filtered = data.filter(function (el) {
            return el != "null";
        });
        $scope.staticParam.Month = filtered;
        if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
            $scope.$apply();
        }
        $scope.reload();
    };
    $scope.ChangeYear = function (data) {
        $scope.staticParam.Year = data;
        if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
            $scope.$apply();
        }
        $scope.reload();
    };
    $scope.getById_Customers = function (data) {
        dataservice.GetItem_AccountantsDebt(data, function (rs) {
            if (rs !== null) {
                $scope.model = rs;
                $scope.model.DateVB !== null ? $scope.model.DateVB = "/Date(" + new Date($scope.model.DateVB).getTime() + ")/" : undefined;
                $scope.statusDisable = false;

            } else {
                $scope.clear_Contracts();
            }
        });
    }
    $scope.clear_Contracts = function () {
        $scope.model = {};
        $scope.model.RowGuid = undefined;
        $scope.model.Title = "";
        $scope.model.CustomerName = "";
        $scope.model.Address = "";
        $scope.model.WorkPlace = "";
        $scope.model.ActivityFieldName = "";
        $scope.model.BusinessTypeName = "";
        $scope.model.LeadName = "";
        $scope.model.Method = "T";
        $scope.model.Note = "";
        $scope.lstDeleteFile = [];
        uploader.queue = [];
        $scope.jdataattach = [];
        $scope.statusDisable = false;
        $("div.SumoSelect").removeClass("SumoSelect disabled").addClass("SumoSelect");
    }
    $scope.lstDeleteFile = [];
    $scope.submit_Contracts = function () {
        if ($scope.addform.validate()) {
            var fd = new FormData();
            $scope.model.IsLeads = true;
            fd.append('submit', JSON.stringify($scope.model));
            $.ajax({
                type: "POST",
                url: "/Accountants/Submit_AccountantsDebt",
                contentType: false,
                processData: false,
                data: fd,
                success: function (rs) {
                    if (rs.Error) {
                        App.notifyDanger(rs.Title);
                    } else {
                        if ($scope.model.RowGuid != undefined && $scope.model.RowGuid != null) {
                            App.notifyInfo("Cập nhật thành công");
                        } else {
                            App.notifyInfo("Thêm mới thành công");
                        }
                        $scope.reload();
                    }
                }
            });
        }
    };


});
app.controller('approve', function ($scope, $rootScope, $uibModalInstance, FileUploader, dataservice, para, Types) {
    $scope.model = { IsAccountant: 1 };
    $scope.Title = "Xử lý phiếu";
    $scope.initData = function () {
        dataservice.getById(para, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {
                $scope.Kmodel = JSON.parse(rs.Table1)[0];
                $scope.model.IsAccountant = 1;
                $scope.model.WorkFlowsContent = $scope.Kmodel.WorkFlowsContent;
                if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                    $scope.$apply();
                }
            }
        })
    }
    $scope.initData_Logistics = function () {
        dataservice.getById_Logistics(para, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {
                $scope.Kmodel = JSON.parse(rs.Table1)[0];
                $scope.model.IsAccountant = 1;
                $scope.model.WorkFlowsContent = $scope.Kmodel.WorkFlowsContent;
                if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                    $scope.$apply();
                }
            }
        })
    }
    if (Types == "1") {
        $scope.initData();
    } else {
        $scope.initData_Logistics();
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    //Cấu hình file đính kèm
    $scope.model.FileAttachments = [];
    var uploader = $scope.uploader = new FileUploader({
        // url: App.baseService() + '/upload'
    });
    uploader.filters.push({
        name: 'customFilter',
        fn: function (item /*{File|FileLikeObject}*/, options) {
            return this.queue.length < 10;
        }
    });
    uploader.onSuccessItem = function (fileItem, response, status, headers) {
        $scope.model.FileAttachments.push(response);
    };
    uploader.onCompleteAll = function () {
        $scope.commit();
    };
    $scope.ChangeIsAccountant = function (data) {
        $scope.model.IsAccountant = data;
    }

    $scope.insert = function () {
        var IsAccountant = $scope.model.IsAccountant == "1" ? true : false;
        var ob = {
            RowGuid: para,
            Types: Types,
            IsAccountant: IsAccountant,
            WorkFlowsContent: $scope.model.WorkFlowsContent
        }
        dataservice.approve(ob, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {
                App.notifyInfo(rs.Title);
                $uibModalInstance.close();
                $rootScope.reload();
            }
        })
    };
});
app.controller('addattchment', function ($scope, $rootScope, $compile, $uibModal, $confirm, $uibModalInstance, dataservice, para, para1, FileUploader) {
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
    $scope.model = { RecordGuid: para, STT: para1 };
    $scope.nameController = "";

    //Cấu hình file đính kèm
    var uploader = $scope.uploader = new FileUploader({
        // url: App.baseService() + '/upload'
    });
    uploader.filters.push({
        name: "customFilter",
        fn: function (item /*{File|FileLikeObject}*/, options) {
            return this.queue.length < 10;
        }
    });
    uploader.onCompleteAll = function () {
    };

    // Bảo hiểm
    $scope.getattachment = function () {
        var obj = {
            RecordGuid: $scope.model.RecordGuid
        }
        dataservice.getattachment(JSON.stringify(obj), function (result) {
            $scope.jdataattach = result;
            angular.forEach($scope.jdataattach, function (value, key) {
                if (value.CreatedBy !== null && value.CreatedBy !== undefined) {
                    if (value.CreatedBy.includes('#')) {
                        value.CreatedBy = value.CreatedBy.split('#')[1];
                    }
                }
            })
        });
    }
    $scope.getattachment(); // danh sách attach bảo hiểm

    $scope.lstDeleteFile = [];
    $scope.remoteAttach = function (item, number) {
        $scope.lstDeleteFile.push(item.AttachmentGuid);
        $scope.jdataattach.splice(number, 1);
        App.notifyInfo("Xóa thành công");
        if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
            $scope.$apply();
        }
    }
    $scope.submit = function () {
        var fd = new FormData();
        $scope.lstTitlefile = [];
        if (uploader.queue.length > 0) {
            for (i = 0; i < uploader.queue.length; i++) {
                var files = uploader.queue[i]._file;
                var titleName = "";
                if (uploader.queue[i].file.FileName === "" || uploader.queue[i].file.FileName === null || uploader.queue[i].file.FileName === undefined) {
                    titleName = uploader.queue[i].file.name.split('.')[0];
                } else {
                    titleName = uploader.queue[i].file.FileName;
                }
                $scope.lstTitlefile.push({ STT: i, Title: titleName });
                fd.append("file", files);
            }
        }
        App.blockUI({
            target: ".modal-content",
            boxed: true,
            message: 'Đang tải...'
        });
        fd.append('model', JSON.stringify($scope.model));
        fd.append('lstTitlefile', JSON.stringify($scope.lstTitlefile));
        fd.append('DeleteAttach', JSON.stringify($scope.lstDeleteFile));
        $.ajax({
            type: "POST",
            url: "/Accountants/UpdateFile",
            contentType: false,
            processData: false,
            data: fd,
            success: function (rs) {
                if (rs.Error === true) {
                    App.notifyDanger(rs.Title);
                    App.unblockUI(".modal-content");
                } else {
                    $scope.lstDeleteFile = [];
                    uploader.queue = [];
                    App.notifyInfo(rs.Title);
                    $scope.getattachment();
                    App.unblockUI(".modal-content");
                }
            },
            error: function (rs) {
                App.notifyDanger(rs.Title);
            }
        })
    }
});
