﻿var ctxfolder = "/views/Sale/Customers";
app.factory('dataservice', function ($http) {
    var headers = {
        "Content-Type": "application/json;odata=verbose",
        "Accept": "application/json;odata=verbose"
    };
    return {
        //------------------------*Store*-------------------------
        getById: function (data, callback) {
            $http.post('/Customers/GetItem/' + data).success(callback);
        },
        delete: function (data, callback) {
            $http.post('/Customers/Delete?Id=' + data).success(callback);
        },     
        getDepartment: function (callback) {
            $http.post('/Announcements/GetDepartment/').success(callback);
        },
        getLogin: function (callback) {
            $http.post('/Announcements/GetLogin').success(callback);
        },
        GetAttachments: function (data, callback) {
            $http.post('/Sale/Customers/GetAttachments/', data).success(callback);
        },
        GetEmpAnnoucement: function (callback) {
            $http.get('/Announcements/GetEmpAnnoucement').success(callback);
        },
        GetBylistEmp: function (data, callback) {
            $http.post('/Announcements/GetBylistEmp/' + data).success(callback);
        },
        getDepartmentAll: function (callback) {
            $http.get('/Announcements/GetDepartmentAll').success(callback);
        },
        GetAllCommentPage: function (data, callback) {
            $http.post('/Announcements/GetAllCommentPage/', data).success(callback);
        },
        ListEmpAnnouncement_Update: function (data, callback) {
            $http.post('/Announcements/ListEmpAnnouncement_Update/', data).success(callback);
        },
        getItemType: function (callback) {
            $http.get('/Sale/Customers/GetItemType').success(callback);
        },
        getTreeCustomerGroups: function (data, callback) {
            $http.post('/Customers/GetTreeGroups/' + data).success(callback);
        },
    };

});
app.filter('html', ['$sce', function ($sce) {
    return function (text) {
        return $sce.trustAsHtml(text);
    };
}]);

app.directive('format', ['$filter', function ($filter) {
    return {
        require: '?ngModel',
        link: function (scope, elem, attrs, ctrl) {
            if (!ctrl) return;
            ctrl.$formatters.unshift(function (a) {
                return $filter(attrs.format)(ctrl.$modelValue);
            });
            ctrl.$parsers.unshift(function (viewValue) {
                var plainNumber = viewValue.replace(/[^\d|\-+|\.+]/g, '');
                elem.val($filter(attrs.format)(plainNumber));
                return plainNumber;
            });
        }
    };
}]);
//định dạng ngày
app.filter('fdate', [
    '$filter', function ($filter) {
        return function (input, f) {
            if (input && input.toString().indexOf('Date') > -1) {
                return moment(input).format(f);
            } if (input && input.toString().indexOf('T') > -1) {
                return moment(input).format(f);
            } else return input;
        };
    }
]);
function onlickFileComment(d) {
    var __el = document.getElementById('file-comment-id');
    __el.setAttribute('accept', '.rar, .zip');
    d === 1 ? __el.setAttribute('accept', 'video/mp4,video/x-m4v,video/*') : '';
    d === 2 ? __el.setAttribute('accept', '.jpg, .png, .jpeg, .gif, .bmp, .tif, .tiff|images/*') : '';
    __el.click();
}
app.controller('Ctrl_Customers', function ($scope, $rootScope, $sce, $compile, $uibModal, dataservice) {
    $rootScope.Title = "";
    $rootScope.go = function (path) {
        $location.path(path); return false;
    };

    $rootScope.validationOptions = {
        rules: {         
            CustomerName: {
                required: true,
                maxlength: 255
            },
            CustomerId: {
                required: true,
                maxlength: 255
            }
          
        },
        messages: {          
            CustomerName: {
                required: "Yêu cầu nhập tên khách hàng.",
                maxlength: "Tên khách hàng không vượt quá 255 ký tự"
            },
            CustomerId: {
                required: "Yêu cầu nhập mã khách hàng.",
                maxlength: "Mã khách hàng không vượt quá 255 ký tự"
            },
        }
    };
    $rootScope.ListDepartments = []; $rootScope.ListDepartmentsIndex = [];
    $rootScope.GetDepartment = function () {
        dataservice.getDepartment(function (result) {
            $rootScope.ListDepartments = result;
            $rootScope.ListDepartmentsIndex = result;
            //$scope.ListDepartmentsIndex.push({
            //    'value': '',
            //    'text': 'Bỏ chọn'
            //});
            //$scope.ListDepartmentsIndex.reverse();

        });
    };
    $rootScope.GetDepartment();
    dataservice.getLogin(function (result) {
        $rootScope.ListLogin = result;
        if (result !== null) {
            $rootScope.userLogin = result.EmployeeId;
            $rootScope.loginName = result.LoginName;
        } else {
            $rootScope.userLogin = "";
        }

    });

    dataservice.GetEmpAnnoucement(function (rs) {
        $rootScope.ListEmpAnnoucement = [];
        $rootScope.ListEmpAnnoucement.push({ value: null, text: "---Mời bạn chọn---" })
        for (var i = 0; i < rs.length; i++) {
            rs[i].value = rs[i].EmployeeGuid;
            rs[i].text = "[" + rs[i].EmployeeId + "] " + rs[i].FullName;
        }
        $rootScope.ListEmpAnnoucement = rs;
    });
    $rootScope.dType = [];
    dataservice.getItemType(function (rs) {
        if (rs.Error) {

        } else if (rs.Object.length > 0) {
            $rootScope.dType = [{
                value: '',
                text: 'Bỏ chọn'
            }];
            for (var i = 0; i < rs.Object.length; i++) {
                $rootScope.dType.push(rs.Object[i]);
            }
        }
    });
    dataservice.getTreeCustomerGroups(null, function (rs9) {
        $rootScope.ListCustomerGroup = [{
            'value': null,
            'text': "Bỏ chọn"
        }];
        angular.forEach(rs9, function (value, key) {
            $rootScope.ListCustomerGroup.push({
                'value': value.GroupGuid,
                'text': value.Title
            });
        });
    });
    $rootScope.ListStatus = [{
        value: null,
        text: 'Chọn tất cả'
    }, {
        value: 'Y',
        text: 'Phát hành'
    }, {
        value: 'N',
        text: 'Chưa phát hành'
    }, {
        value: 'E',
        text: 'Hết hạn'
    }];
    $rootScope.StatusCombo = [{
        value: 'Y',
        text: 'Phát hành'
    }, {
        value: 'N',
        text: 'Chưa phát hành'
    }, {
        value: 'E',
        text: 'Hết hạn'
    }];
    $rootScope.ListIspriority = [{
        value: null,
        text: 'Chọn tất cả'
    }, {
        value: 'Q',
        text: 'Quan trọng'
    }, {
        value: 'B',
        text: 'Bình thường'
    }];
    $rootScope.StatusConfig = {
        placeholder: 'Trạng thái',
        search: true
    };
    $rootScope.LiIspriority = [{
        value: 'Q',
        text: 'Quan trọng'
    }, {
        value: 'B',
        text: 'Bình thường'
    }];
    $rootScope.StatusConfig = {
        placeholder: 'Trạng thái',
        search: true
    };
    $rootScope.IsPublicCombo = [{
        value: "1",
        text: 'Công khai'
    }, {
        value: "2",
        text: 'Nhân viên'
    },
    {
        value: "3",
        text: 'Phòng ban'
    }

    ];
    $rootScope.ListIsPublic = [{
        value: '',
        text: 'Chọn tất cả'
    }, {
        value: "1",
        text: 'Công khai'
    }, {
        value: "2",
        text: 'Nhân viên'
    }
        , {
        value: "3",
        text: 'Phòng ban'
    }

    ];

    $rootScope.IsActive = [{
        value: "N",
        text: 'Bản nháp'
    }, {
        value: "Y",
        text: 'Phát hành'
    }];
    $rootScope.ColumnMultiConfig = {
        selectAll: true,
        search: true,
        searchText: 'Tìm kiếm...',
        showTitle: 'true',
        csvSepChar: ',',
        captionFormat: '{0} lựa chọn',
        captionFormatAllSelected: '{0} lựa chọn',
        noMatch: 'Không có dữ liệu "{0}"',
        okCancelInMulti: true,
        locale: ['Đồng ý', 'Hủy', 'Chọn tất cả']
    };
    $rootScope.ColumnConfig = {
        search: true
    };
    $rootScope.CreateDate = function (date) {
        if (date !== null && date !== undefined) {
            var newdate = new Date(date);
            var month = newdate.getMonth() + 1;
            var day = newdate.getDate();
            var year = newdate.getFullYear();
            var hh = newdate.getHours();
            var mm = newdate.getMinutes();
            if (month < 10)
                month = "0" + month;
            if (day < 10)
                day = "0" + day;
            if (mm < 10)
                mm = "0" + mm;
            return todayDate = day + "/" + month + "/" + year;
        } else {
            return null;
        }
    };
    $rootScope.EditDate = function (datetime) {
        if (datetime !== null && datetime !== undefined) {
            var Str = datetime.toString();
            if (Str.indexOf("/Date") >= 0) {

                var newdate = new Date(parseInt(datetime.substr(6)));
                var month = newdate.getMonth() + 1;
                var day = newdate.getDate();
                var year = newdate.getFullYear();
                var hh = newdate.getHours();
                var mm = newdate.getMinutes();
                if (month < 10)
                    month = "0" + month;
                if (day < 10)
                    day = "0" + day;
                return year + "/" + month + "/" + day;
            } else {
                var value = datetime.split('/');

                return value[2] + '/' + value[1] + '/' + value[0];
            }
        }
        return null;
    };
    $rootScope.convertToJSONDate = function (datetime) {
        if (datetime !== null && datetime !== "") {
            var Str = datetime.toString();
            if (Str.indexOf("/Date") >= 0) {
                return Str;
            } else {
                var date = new Date(datetime);
                var month1 = date.getMonth() + 1;
                var day1 = date.getDate();
                var year1 = date.getFullYear();
                if (month1 < 10)
                    month1 = "0" + month1;
                if (day1 < 10)
                    day1 = "0" + day1;
                var newDate = new Date(year1 + '-' + month1 + '-' + day1);
                return '/Date(' + newDate.getTime() + ')/';
            }
        }
    };
    //Thêm phần tinymceOptions
    $rootScope.tinymceOptions = {
        selector: 'textarea',
        plugins: [
            'advlist autolink lists link charmap print preview hr anchor pagebreak image',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
        ],
        skin: 'lightgray', language: 'vi_VN',
        theme: 'modern', height: 70,
        toolbar1: 'codesample | bold italic sizeselect fontselect fontsizeselect | hr alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | insertfile undo redo | emoticons | code',
        fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
        toolbar2: 'print preview insert link image media | forecolor backcolor emoticons | codesample',
        image_advtab: true,

        file_browser_callback: function (field_name, url, type, win) {
            var connector = "/FileManager/Folder/File";
            var enableAutoTypeSelection = true;
            if (enableAutoTypeSelection && type) {
                connector += "&Type=" + type;
            }
            connector += "&Field=" + field_name;
            App.openPage(connector, "File Manager", 1150, 600);
        },
        relative_urls: false,
        remove_script_host: true,
        convert_urls: true
    };
    $rootScope.CalenderReturnDate = function (item) {
        //xác định tuần hiện tại
        var d = new Date();
        if (item === '0') {
            var day = d.getDay(),
                fday = d.getDate() - day,
                lday = fday + 6;
            var data = {
                sdate: moment(new Date(d.getFullYear(), d.getMonth(), fday)).format(),
                edate: moment(new Date(d.getFullYear(), d.getMonth(), lday)).format()
            };
            return data;
        }
        //Tháng Hiện tại
        if (item === '1') {
            var data1 = {
                sdate: moment(new Date(d.getFullYear(), d.getMonth(), 1)).format(),
                edate: moment(new Date(d.getFullYear(), d.getMonth() + 1, 0)).format()
            };
            return data1;
        }
        //Quý Hiện tại
        if (item === '2') {
            var motnth = d.getMonth();
            var year = d.getFullYear();
            if (d.getMonth() === 0 || d.getMonth() === 1 || d.getMonth() === 2) {
                motnth = 1;
                year = year;
            }
            else if (d.getMonth() === 3 || d.getMonth() === 4 || d.getMonth() === 5)
                motnth = 4;
            else if (d.getMonth() === 6 || d.getMonth() === 7 || d.getMonth() === 8)
                motnth = 7;
            else
                motnth = 10;
            var data2 = {
                sdate: moment(new Date(year, motnth - 1, 1)).format(),
                edate: moment(new Date(year, motnth + 2, 0)).format()
            };
            return data2;
        }
        //Năm Hiện tại
        if (item === '3') {
            var data3 = {
                sdate: moment(new Date(d.getFullYear(), 0, 1)).format(),
                edate: moment(new Date(d.getFullYear(), 12, 0)).format()
            };
            return data3;
        }
        //xác định tuần trước
        var d4 = new Date();
        if (item === '4') {
            var day4 = d4.getDay(),
                fday4 = d4.getDate() - day4 + (day4 === 0 ? -6 : 1) - 7,
                lday4 = d4.getDate() - day4;
            var data4 = {
                sdate: moment(new Date(d4.getFullYear(), d4.getMonth(), fday4)).format(),
                edate: moment(new Date(d4.getFullYear(), d4.getMonth(), lday4)).format()
            };
            return data4;
        }
        //Tháng trước
        if (item === '5') {
            var data5 = {
                sdate: moment(new Date(d.getFullYear(), d.getMonth() - 1, 1)).format(),
                edate: moment(new Date(d.getFullYear(), d.getMonth(), 0)).format()
            };
            return data5;
        }
        //Quý trước
        if (item === '6') {
            var motnth6 = d.getMonth();
            var year6 = d.getFullYear();
            if (d.getMonth() === 0 || d.getMonth() === 1 || d.getMonth() === 2) {
                motnth6 = 10;
                year6 = year6 - 1;
            }
            else if (d.getMonth() === 3 || d.getMonth() === 4 || d.getMonth() === 5)
                motnth6 = 1;
            else if (d.getMonth() === 6 || d.getMonth() === 7 || d.getMonth() === 8)
                motnth6 = 4;
            else
                motnth6 = 7;
            var data6 = {
                sdate: moment(new Date(year6, motnth6 - 1, 1)).format(),
                edate: moment(new Date(year6, motnth6 + 2, 0)).format()
            };
            return data6;
        }
        //Năm trước
        if (item === '7') {
            var data7 = {
                sdate: moment(new Date(d.getFullYear() - 1, 0, 1)).format(),
                edate: moment(new Date(d.getFullYear() - 1, 12, 0)).format()
            };
            return data7;
        }
    };
    $rootScope.ConDate = function (data, number) {
        try {
            if (data == null || data == "") {
                return '';
            }
            if (data !== null && data != "" && data != undefined) {
                try {
                    if (data.indexOf("SA") != -1 || data.indexOf("CH") != -1) {
                        if (data.indexOf("SA") != -1) { }
                        if (data.indexOf("CH") != -1) { }
                    }

                    if (data.indexOf('Date') != -1) {
                        data = data.substring(data.indexOf("Date") + 5);
                        data = parseInt(data);
                    }
                }
                catch (ex) { }
                var newdate = new Date(data);
                if (number == 3) {
                    return newdate;
                }
                if (number == 2) {
                    return "/Date(" + newdate.getTime() + ")/";
                }
                var month = newdate.getMonth() + 1;
                var day = newdate.getDate();
                var year = newdate.getFullYear();
                var hh = newdate.getHours();
                var mm = newdate.getMinutes();
                if (month < 10)
                    month = "0" + month;
                if (day < 10)
                    day = "0" + day;
                if (mm < 10)
                    mm = "0" + mm;
                if (number == 0) {
                    return todayDate = day + "/" + month + "/" + year;
                }
                if (number == 1) {
                    return todayDate = day + "/" + month + "/" + year + " " + hh + ":" + mm;
                }
                if (number == 4) {
                    return new Date(year, month - 1, day);
                }
            } else {
                return '';
            }
        } catch (ex) {
            return "";
        }
    }
});
app.config(function ($routeProvider, $validatorProvider) {
    $routeProvider
        .when('/', {
            templateUrl: ctxfolder + '/index.html',
            controller: 'index'
        })
        .when('/edit/:id', {
            templateUrl: ctxfolder + '/edit.html',
            controller: 'edit'
        })
        .when('/add/', {
            templateUrl: ctxfolder + '/add.html',
            controller: 'add'
        });
    $validatorProvider.setDefaults({
        errorElement: 'span',
        errorClass: 'help-block',
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        success: function (label) {
            label.closest('.form-group').removeClass('has-error');
        }
    });
});
//controller hiển thị, tìm kiếm
app.controller('index', function ($scope, $rootScope, $compile, $http, $confirm, $ngConfirm, $uibModal, DTOptionsBuilder, DTColumnBuilder, DTInstances, dataservice, utility) {
    $scope.utility = utility;
    $rootScope.TableName = "/Customers";
    $scope.model = {};
    var vm = $scope;
    var ctrl = $scope;
    $scope.staticParam = {
        totalItems: 0,
        currentPage: 1,
        pageSize: 25,
        maxSize: 5,
        Search: "",
        IsPublic: null,
        Status: null,
        StartDate: $rootScope.convertToJSONDate($rootScope.CalenderReturnDate("1").sdate),
        EndDate: $rootScope.convertToJSONDate($rootScope.CalenderReturnDate("1").edate),
        Ispriority: null,
        DepartmentGuid: "",
        RowGuid: ""
    };
    ctrl.changeOptionDate = function (d) {
        if (d.length === 0) {
            ctrl.staticParam.StartDate = null;
            ctrl.staticParam.EndDate = null;
        } else {
            ctrl.staticParam.StartDate = $rootScope.ConDate(d[0], 2);
            ctrl.staticParam.EndDate = $rootScope.ConDate(d[1], 2);
        }
    }
    $scope.$watch('staticParam.StarDate', function (newvalue, oldvalue) {
        if (newvalue !== undefined && newvalue !== oldvalue) {
            reloadData(true);
        }
    });
    $scope.$watch('staticParam.EndDate', function (newvalue, oldvalue) {

        if (newvalue !== undefined && newvalue !== oldvalue) {
            reloadData(true);
        }
    });
    $rootScope.EmpConfig = {
        placeholder: 'Chọn phòng ban',
        search: true,
        change: function () {
            $scope.reload();
        }
    };
    dataservice.getDepartmentAll(function (result) {
        $rootScope.ListDepartmentGuid = result;
    });
    $rootScope.ListDepartmentGuid = [];
    $scope.ListDepartment = [];

    $scope.StatusChange = function (data) {
        $scope.staticParam.Status = data !== 'null' ? data : "";
        $scope.selectAll = false;
        $scope.Search();
    };
    $scope.IsPublicChange = function (data) {
        $scope.staticParam.IsPublic = data !== 'null' ? data : "";
        $scope.selectAll = false;
        $scope.Search();
    };
    $scope.IspriorityChange = function (data) {
        $scope.staticParam.Ispriority = data !== 'null' ? data : "";
        $scope.selectAll = false;
        $scope.Search();
    };

    $scope.selected = [];
    $scope.selectAll = false;
    $scope.toggleOne = toggleOne;
    $scope.toggleAll = toggleAll;
    $scope.$watch('staticParam.StartDate', function (newvalue, oldvalue) {
        if (newvalue !== oldvalue) {
            $scope.staticParam.StartDate = newvalue;
            if (parseInt(moment(newvalue).format("YYYYMMDD")) > parseInt(moment($scope.staticParam.EndDate).format("YYYYMMDD"))) {
                App.notifyDanger('Yêu cầu ngày bắt đầu nhỏ hơn ngày kết thúc');
                $scope.staticParam.StartDate = oldvalue;
                return;
            } else {
                $scope.reload(true);
            }
        }
    });
    $scope.$watch('staticParam.EndDate', function (newvalue, oldvalue) {
        if (newvalue !== oldvalue) {
            $scope.staticParam.EndDate = newvalue;
            if (parseInt(moment(newvalue).format("YYYYMMDD")) < parseInt(moment($scope.staticParam.StartDate).format("YYYYMMDD"))) {
                App.notifyDanger('Yêu cầu ngày bắt đầu nhỏ hơn ngày kết thúc');
                $scope.staticParam.EndDate = oldvalue;
                return;
            } else {
                $scope.reload(true);

            }
        }
    });
    var titleHtml = '<label style="text-align: center;" class="mt-checkbox" ><input type="checkbox" ng-model="selectAll" ng-click="toggleAll(selectAll, selected)"/><span></span></label>';
    //if (App.Permissions.LISTVIEW)
    vm.dtOptions = DTOptionsBuilder.newOptions()
        .withPaginationType('full_numbers')
        .withDataProp('data').withDisplayLength(15)
        .withOption('order', [6, 'desc'])
        .withOption('serverSide', true)
        .withOption('pageLength', 25)
        .withOption('info', false)
        .withOption('stateLoadParams', function (settings, data) {
            data.search.search = '';
        })
        .withOption('headerCallback', function (header) {
            if (!$scope.headerCompiled) {
                $scope.headerCompiled = true;
                $compile(angular.element(header).contents())($scope);
            }
        })
        .withOption('initComplete', function (settings, json) {
            //thêm sự kiện scroll phân trang
            //  $('#tblData').parent().attr("onscroll", "angular.element(this).scope().LoadScroll(this)");
            PLUGIN_DATATABLE_TABLE_SCROLL.INIT_EVENT('tblData', $scope.LoadScroll);

        })
        .withOption('createdRow', function (row, data, dataIndex) {
            const contextScope = $scope.$new(true);
            contextScope.data = data;
            $scope.data = data;
            $compile(row)($scope);
            contextScope.contextMenu = $scope.contextMenu;
            $compile(angular.element(row).find('input'))($scope);
            $compile(angular.element(row).attr('context-menu', 'contextMenu'))(contextScope);
        });
    // if (App.Permissions.LISTVIEW)
    vm.dtOptions.withOption('ajax', function (data, callback, settings) {
        data.Keyword = $scope.staticParam.Search;
        data.IsBussiness = $scope.staticParam.IsBussiness;
        data.Status = $scope.staticParam.Status;
        data.IsProvince = $scope.staticParam.IsProvince;

        App.blockUI({
            target: "#tblData",
            boxed: true,
            message: 'Đang tải...'
        });
        $http.post('/Customers/JTable', data)
            .success(function (res) {
                $scope.selected = [];
                if (data.start === 0) {
                    $scope.dataload = [];
                    angular.forEach(res.data, function (val, key) {
                        $scope.dataload.push(val);
                    });
                    callback({
                        data: $scope.dataload,
                        recordsTotal: res.recordsTotal,
                        recordsFiltered: res.recordsFiltered
                    });
                } else {
                    if (res.recordsTotal > $scope.dataload.length) {
                        angular.forEach(res.data, function (val, key) {
                            $scope.dataload.push(val);
                        });
                        callback({
                            data: $scope.dataload,
                            recordsTotal: res.recordsTotal,
                            recordsFiltered: res.recordsFiltered
                        });
                    }
                }
                PLUGIN_DATATABLE_TABLE_SCROLL.UNLOCK_EVENT('tblData');
                App.unblockUI("#tblData");
            });
    });
    //scroll theo tỷ lệ màn hình
    vm.dtOptions.withOption('scrollY', '70vh')
        .withOption('scrollX', '100%') //mặc định 100%
        .withOption('scrollCollapse', true) //xóa khoảng trắng khi dữ liệu trong bảng ít
        .withOption('rowCallback', rowCallback) //sự kiên click vào 1 dòng
        .withOption('scroller', {
            loadingIndicator: true
        });
    //.withFixedColumns({
    //    leftColumns: 0 //fix bên trái 2 cột
    //    //rightColumns: 1 //fix bên phải 1 cột
    //}
    //);
    //table
    vm.dtColumns = [];
    //vm.dtColumns.push(DTColumnBuilder.newColumn("AnnouncementGuid").withTitle(titleHtml).notSortable().renderWith(function (data, type, full, meta) {
    //    $scope.selected[full.AnnouncementGuid] = false;
    //    return '<label class="mt-checkbox"><input type="checkbox" ng-model="selected[\'' + full.AnnouncementGuid + '\']" ng-click="toggleOne(selected)"/><span></span></label>';
    //}).withOption('sWidth', '5px').withOption('sClass', 'tcenter tcenter-header'));
    vm.dtColumns.push(DTColumnBuilder.newColumn('AnnouncementGuid').withTitle('STT').notSortable().withOption('sWidth', '5px').renderWith(function (data, type, full, meta) {
        return meta.row + 1;
    }).withOption('sClass', 'tcenter tcenter-header'));
    vm.dtColumns.push(DTColumnBuilder.newColumn('CustomerId').withTitle('Mã khách hàng').withOption('sWidth', '80px').withOption('sClass', 'tcenter-header').renderWith(function (data, type, full, meta) {
        return data;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('CustomerName').withTitle('Tên khách hàng').withOption('sWidth', '200px').withOption('sClass', 'tcenter-header').renderWith(function (data, type, full, meta) {
        return '<a  style="color: #4285f4;" href="" ng-click="open(\'' + full.CustomerGuid + '\')" >' + data + '</a>';
    }));

    //vm.dtColumns.push(DTColumnBuilder.newColumn('StartDateString').withTitle('Ngày bắt đầu').withOption('sClass', 'mw250').withOption('sClass', 'es-tright tcenter-header').renderWith(function (data, type, full, meta) {
    //    return data;
    //}));
    //vm.dtColumns.push(DTColumnBuilder.newColumn('EndDateString').withTitle('Ngày kết thúc').withOption('sClass', 'mw250').withOption('sClass', 'es-tright tcenter-header').renderWith(function (data, type, full, meta) {
    //    return data;
    //}));
    vm.dtColumns.push(DTColumnBuilder.newColumn('WorkPhone').withTitle('Số điện thoại').withTitleDescription('Số điện thoại').withOption('sClass', 'tright').withOption('sWidth', '40px').renderWith(function (data, type, full) {
        return data;

    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('GroupName').withTitle('Nhóm khách hàng').withOption('sWidth', '80px').withOption('sClass', 'tcenter-header').renderWith(function (data, type, full, meta) {
        return data;
    }));

    vm.dtColumns.push(DTColumnBuilder.newColumn('WorkEmail').withTitle('Email').withOption('sWidth', '80px').withOption('sClass', 'tcenter-header').renderWith(function (data, type, full, meta) {
        return data;
    }));

    vm.dtColumns.push(DTColumnBuilder.newColumn('TypeName').withTitle('Mảng kinh doanh').withOption('sWidth', '80px').withOption('sClass', 'tcenter-header').renderWith(function (data, type, full, meta) {
        return data;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('CreatedDate').withTitle('Ngày tạo').withOption('sWidth', '60px').withOption('sClass', 'tright').renderWith(function (data, type, full, meta) {
        return full.CreatedDateString;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('CustomerGuid').notSortable().withTitle('Điều khiển').notSortable().withOption('sWidth', '80px').withOption('sClass', 'tcenter').renderWith(function (data, type, full, meta) {

        return '<div class="table__cell-actions-wrap">' +
            '<a ng-click="open(\'' + full.CustomerGuid + '\')" class="table__cell-actions-item table__cell-actions-icon"><span class="navbar-notify es-cursor" ><span><span class="navbar-notify__icon mdi mdi-eye"></span></span></a > '
            +
            '<a href="javascript:;" ng-click="edit(\'' + full.CustomerGuid + '\')" class="table__cell-actions-item table__cell-actions-icon es-cursor"> <span class="mdi mdi-table-edit" title="Sửa"></span> </a>' +
            '<a ng-click="delete (\'' + full.CustomerGuid + '\')" class="table__cell-actions-item table__cell-actions-icon"><span class="mdi mdi-delete" title="Xóa"></span> </a>';

    }));
    vm.reloadData = reloadData;
    vm.dtInstance = {};
    function reloadData(resetPaging) {
        $scope.staticParam.currentPage = resetPaging;
        vm.dtInstance.reloadData(callback, resetPaging);
     
    }
    function callback(json) {

    }
    function toggleOne(selectedItems) {
        for (var id in selectedItems) {
            if (selectedItems.hasOwnProperty(id)) {
                if (!selectedItems[id]) {
                    vm.selectAll = false;
                    return;
                }
            }
        }
        vm.selectAll = true;
    }
    function toggleAll(selectAll, selectedItems) {
        for (var id in selectedItems) {
            if (selectedItems.hasOwnProperty(id)) {
                selectedItems[id] = selectAll;
            }
        }
    }
    function rowCallback(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        $('td', nRow).unbind('dblclick');
        $('td', nRow).bind('dblclick', function () {
            $scope.$apply(function () {
                $scope.open(aData.AnnouncementGuid);
            });
        });
        return nRow;

    }
    //Load dữ liệu cuộn
    $scope.total = 0;
    $scope.check = false;
    $scope.LoadScroll = function (obj) {
        vm.dtInstance.DataTable.page('next').draw('page');
    };
    $('#minize-menuleft').click(function () {
        vm.dtInstance.DataTable.draw();
    });
    //tìm kiếm
    $scope.reload = function () {
        $rootScope.IsPublic = $scope.staticParam.IsActive;
        $rootScope.Status = $scope.staticParam.Status;
        $scope.dtInstance.DataTable.search($scope.staticParam.DepartmentGuid);
        $scope.dtInstance.DataTable.search($scope.staticParam.Search);
        $scope.dtInstance.DataTable.search($scope.staticParam.Search).draw();
    };
    $scope.reloadAll = function () {
        $scope.staticParam.Search = "";
        $scope.selectAll = false;
        $scope.staticParam.IsPublic = null;
        $scope.staticParam.Status = null;
        $scope.staticParam.Ispriority = null;
        $scope.staticParam.DepartmentGuid = "";
        reloadData(true);
        $scope.dtInstance.DataTable.search($scope.staticParam.Search);
        $scope.dtInstance.DataTable.search($scope.staticParam.Search).draw();
        $scope.staticParam.StartDate = $rootScope.convertToJSONDate($rootScope.CalenderReturnDate("1").sdate);
        $scope.staticParam.EndDate = $rootScope.convertToJSONDate($rootScope.CalenderReturnDate("1").edate);
    };
    $rootScope.Search = function () {
        $scope.dataload = [];
        reloadData(true);
    };
    // thêm mới
    $scope.add = function () {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/add.html',
            controller: 'add',
            backdrop: 'static',
            keyboard: false,
            size: '100'
        });
        modalInstance.result.then(function (d) {
        }, function () {
        });
    };
    // chi tiết
    $scope.open = function (temp) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/open.html',
            controller: 'open',
            backdrop: 'static',
            size: '100',
            resolve: {
                para: function () {
                    return temp;
                }
            }
        });
        modalInstance.result.then(function (d) {
        }, function () {
        });

    };
    // sửa 
    $scope.edit = function (temp) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/edit.html',
            controller: 'edit',
            backdrop: 'static',
            size: '100',
            resolve: {
                para: function () {
                    return temp;
                }
            }
        });
        modalInstance.result.then(function (d) {
            $rootScope.Search();
        }, function () {

        });

    };
    //Xóa 1 bản ghi
    $scope.delete = function (temp) {
        dataservice.getById(temp, function (rs) {
            ESConfirm([{ 'value': 1, 'title': 'Xác nhận', 'type': 'info' }, { 'value': 2, 'title': 'Đóng', 'type': 'danger' }], { 'description': 'Bạn có muốn xóa ' + rs.CustomerName, 'class': 'eswarning_v2' }, function (rs) {
                if (rs === '1') {
                    App.blockUI({
                        target: "#contentMain",
                        boxed: true,
                        message: 'Đang tải...'
                    });
                    dataservice.delete(temp, function (result) {
                        if (result.Error) {
                            App.notifyDanger(result.Title);
                        } else {
                            App.notifyInfo(result.Title);
                            $rootScope.Search();
                        }
                        App.unblockUI("#contentMain");
                    });
                }
            }, function () {
            });
        });
    };
    // xóa nhiều bản ghi
    $scope.deleteChecked = function () {

        var deleteItems = [];
        for (var id in $scope.selected) {
            if ($scope.selected.hasOwnProperty(id)) {
                if ($scope.selected[id]) {
                    deleteItems.push(id);
                }
            }
        }
        if (deleteItems.length > 0) {
            ESConfirm([{ 'value': 1, 'title': 'Xác nhận', 'type': 'info' }, { 'value': 2, 'title': 'Đóng', 'type': 'danger' }], { 'description': 'Bạn có muốn xóa những thông báo này?', 'class': 'eswarning_v2' }, function (rs) {
                if (rs === '1') {
                    App.blockUI({
                        target: "#contentMain",
                        boxed: true,
                        message: 'Đang tải...'
                    });
                    dataservice.deleteItems(deleteItems, function (result) {
                        if (result.Error) {
                            App.notifyDanger(result.Title);
                        } else {
                            App.notifyInfo(result.Title);
                            $scope.selectAll = false;
                            $rootScope.Search();
                        }
                        App.unblockUI("#contentMain");
                    });
                }
            }, function () {
            });
        } else {
            App.notifyDanger("Không có bản ghi nào được chọn");
        }
    };
    //thay đổi trạng thái
    var da = false;
    $scope.checkbox = function (Id, IsActive, Name) {
        var _item = [];

        if (IsActive === "True") {
            _item.push({ Id: Id, IsActive: false });
        } else if (IsActive === "False") {
            _item.push({ Id: Id, IsActive: true });
        }
        else {
            _item.push({ Id: Id, IsActive: false });
        }
        if (da === false) {
            da = true;
            dataservice.update(_item, function (result) {
                if (result.Error) {
                    App.notifyDanger(result.Title);
                } else {
                    da = false;
                    App.notifyInfo(result.Title);
                    $rootScope.Search();
                }
                App.unblockUI("#contentMain");
            });
        }

    };
    //thêm sưa xóa menu chuột phải
    $scope.contextMenu = [
        //xem chi tiết trong menu
        [function ($itemScope) {
            return '<i class="mdi mdi-eye"></i> Xem';
        }, function ($itemScope, $event, model) {
            $scope.open($itemScope.data.CustomerGuid);
        }],
        //sửa phiếu
        [function ($itemScope) {
            return '<i class="mdi mdi-table-edit"></i> Sửa';
        }, function ($itemScope, $event, model) {
            $scope.edit($itemScope.data.CustomerGuid);

        }, function ($itemScope, $event, model) {
            return true;
        }],
        //xóa trong menu
        [function ($itemScope) {
            return '<i class="mdi mdi-delete"></i> Xóa';
        }, function ($itemScope, $event, model) {
            $scope.delete($itemScope.data.CustomerGuid);
        }, function ($itemScope, $event, model) {

            return true;

        }]

    ];

});
function gotoBottom(id) {
    var element = document.getElementById(id);
    element.scrollTop = 10;/* element.scrollHeight - element.clientHeight;*/
}
// controller Add
app.controller('add', function ($scope, $rootScope, $uibModalInstance, FileUploader, dataservice) {
    $scope.Title = "Thêm mới khách hàng";
    $scope.model = { IsBussiness: 1, imageLink: "/images/doanhnghiep.png" };
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
    $scope.ListReplate = {
        EmployeeGuid: [],
        EmployeeName: null,
    }
    $scope.ClickTab = function (item) {
        for (var i = 1; i <= 11; i++) {
            if (item == i) {
                $("#tab-" + i.toString()).addClass("active show");
                $("#tabs-" + i.toString()).addClass("active show");
            } else {
                $("#tab-" + i.toString()).removeClass("active show");
                $("#tabs-" + i.toString()).removeClass("active show");
            }

        }
    }
    $scope.ChangeIsProvider = function (data) {
        $scope.model.IsProvider = data;
    }
    $scope.CustomerGroupChange = function (data) {
        $scope.model.PolicyGroupGuid = data == "null" ? "" : data;
    };
    $scope.ChangeType = function (data) {
        $scope.model.Type = data == "null" ? "" : data;
    };
    $scope.ChangeIspriority = function (data) {
        $scope.model.Ispriority = data;
    };
    $scope.newImage = function () {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#image').attr('src', e.target.result);
        }
        reader.readAsDataURL($("#loadImage").prop('files')[0]);
        $scope.changeImage = true;
    }
    var reg1 = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    $scope.model.FileAttachments = [];
    var uploader = $scope.uploader = new FileUploader({
        // url: App.baseService() + '/upload'
    });
    uploader.filters.push({
        name: 'customFilter',
        fn: function (item /*{File|FileLikeObject}*/, options) {
            return this.queue.length < 10;
        }
    });
    uploader.onSuccessItem = function (fileItem, response, status, headers) {
        $scope.model.FileAttachments.push(response);
    };

    uploader.onCompleteAll = function () {
        $scope.commit();
    };
    $scope.submit = function () {
        if ($scope.addform.validate()) {

            var fd = new FormData();
            var imageTemp = $("#loadImage").prop('files')[0];
            $scope.lstTitlefile = [];
            if (uploader.queue.length > 0) {
                for (i = 0; i < uploader.queue.length; i++) {
                    var files = uploader.queue[i]._file;
                    fd.append("2#" + uploader.queue[i].file.FileName, files);
                }
            }
            if (imageTemp !== null && imageTemp !== undefined && imageTemp !== '') {
                fd.append("1#" + imageTemp.name, imageTemp);
            }
            //var list = [];
            //for (var i = 0; i < $scope.ListReplate.EmployeeGuid.length; i++) {
            //    for (var j = 0; j < $rootScope.ListEmpAnnoucement.length; j++) {
            //        if ($rootScope.ListEmpAnnoucement[j].EmployeeGuid == $scope.ListReplate.EmployeeGuid[i]) {
            //            list.push({
            //                EmployeeGuid: $rootScope.ListEmpAnnoucement[j].EmployeeGuid,
            //                EmployeeName: $rootScope.ListEmpAnnoucement[j].FullName,
            //            });
            //            break;
            //        }
            //    }
            //}
            $scope.model.IsBussiness = $scope.model.IsBussiness == 1 || $scope.model.IsBussiness == true ? true : false;
            fd.append('submit', JSON.stringify($scope.model));
            $.ajax({
                type: "POST",
                url: "/Customers/Insert",
                contentType: false,
                processData: false,
                data: fd,
                success: function (rs) {
                    if (rs.Error) {
                        App.notifyDanger(rs.Title);
                    } else {
                        App.notifyInfo(rs.Title);
                        $uibModalInstance.close();
                        $rootScope.Search();
                    }
                }
            });
        }
    };
});
// controller edit
app.controller('edit', function ($scope, $rootScope, $uibModalInstance, dataservice, para, FileUploader) {
    $scope.Title = "Sửa khách hàng";
    $scope.model = { IsBussiness: 1, imageLink: "/images/doanhnghiep.png" };
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
    $scope.ClickTab = function (item) {
        for (var i = 1; i <= 11; i++) {
            if (item == i) {
                $("#tab-" + i.toString()).addClass("active show");
                $("#tabs-" + i.toString()).addClass("active show");
            } else {
                $("#tab-" + i.toString()).removeClass("active show");
                $("#tabs-" + i.toString()).removeClass("active show");
            }

        }
    }
    $scope.newImage = function () {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#image').attr('src', e.target.result);
        }
        reader.readAsDataURL($("#loadImage").prop('files')[0]);
        $scope.changeImage = true;
    }
    $scope.ChangeIsProvider = function (data) {
        $scope.model.IsProvider = data;
    }
    $scope.CustomerGroupChange = function (data) {
        $scope.model.PolicyGroupGuid = data == "null" ? "" : data;
    };
    $scope.ChangeType = function (data) {
        $scope.model.Type = data == "null" ? "" : data;
    };
    $scope.ChangeIspriority = function (data) {
        $scope.model.Ispriority = data;
    };
    $scope.initData = function () {
        dataservice.getById(para, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {
                $scope.model = rs;
                if ($scope.model.Image !== null && $scope.model.Image !== "") {
                    $scope.model.imageLink = "/Customers/GetPic/" + rs.CustomerGuid;
                }
                //Lấy file đính kèm
                var ob = {
                    RecordGuid: $scope.model.CustomerGuid
                }
                dataservice.GetAttachments(ob, function (rs1) {
                    $scope.jdataattach = rs1;
                });
                $scope.model.StartDate !== null ? $scope.model.StartDate = "/Date(" + new Date($scope.model.StartDate).getTime() + ")/" : undefined;
                $scope.model.EndDate !== null ? $scope.model.EndDate = "/Date(" + new Date($scope.model.EndDate).getTime() + ")/" : undefined;
                $scope.model.BirthDate !== null ? $scope.model.BirthDate = "/Date(" + new Date($scope.model.BirthDate).getTime() + ")/" : undefined;
                $scope.model.IssueIddate !== null ? $scope.model.IssueIddate = "/Date(" + new Date($scope.model.IssueIddate).getTime() + ")/" : undefined;
                if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                    $scope.$apply();
                }
            }
        });
    };
    $scope.initData();
    //Cấu hình file đính kèm
    var uploader = $scope.uploader = new FileUploader({
        // url: App.baseService() + '/upload'
    });
    uploader.filters.push({
        name: "customFilter",
        fn: function (item /*{File|FileLikeObject}*/, options) {
            return this.queue.length < 10;
        }
    });
    $scope.lstDeleteFile = [];
    //Xóa file đính kèm
    $scope.remoteAttach = function (item, number) {
        $scope.lstDeleteFile.push(item.AttachmentGuid);
        $scope.jdataattach.splice(number, 1);
        App.notifyInfo("Xóa thành công");
        if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
            $scope.$apply();
        };
    }

        //store
    $scope.submit = function () {
        if ($scope.addform.validate()) {

            var fd = new FormData();
            var imageTemp = $("#loadImage").prop('files')[0];
            $scope.lstTitlefile = [];
            if (uploader.queue.length > 0) {
                for (i = 0; i < uploader.queue.length; i++) {
                    var files = uploader.queue[i]._file;
                    fd.append("2#" + uploader.queue[i].file.FileName, files);
                }
            }
            if (imageTemp !== null && imageTemp !== undefined && imageTemp !== '') {
                fd.append("1#" + imageTemp.name, imageTemp);
            }
           
            $scope.model.IsBussiness = $scope.model.IsBussiness == 1 || $scope.model.IsBussiness == true ? true : false;
            fd.append('submit', JSON.stringify($scope.model));
            fd.append('DeleteAttach', JSON.stringify($scope.lstDeleteFile));
            $.ajax({
                type: "POST",
                url: "/Customers/Update",
                contentType: false,
                processData: false,
                data: fd,
                success: function (rs) {
                    if (rs.Error) {
                        App.notifyDanger(rs.Title);
                    } else {
                        App.notifyInfo(rs.Title);
                        $uibModalInstance.close();
                        $rootScope.Search();
                    }
                }
            });
        }
    };
});
// controller open
app.controller('open', function ($scope, $rootScope, $uibModalInstance, dataservice, para, FileUploader) {
    $scope.Title = "Xem khách hàng";
    $scope.model = { IsBussiness: 1, imageLink: "/images/doanhnghiep.png" };
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
    $scope.ClickTab = function (item) {
        for (var i = 1; i <= 11; i++) {
            if (item == i) {
                $("#tab-" + i.toString()).addClass("active show");
                $("#tabs-" + i.toString()).addClass("active show");
            } else {
                $("#tab-" + i.toString()).removeClass("active show");
                $("#tabs-" + i.toString()).removeClass("active show");
            }

        }
    }
    var uploader = $scope.uploader = new FileUploader({
        // url: App.baseService() + '/upload'
    });
    uploader.filters.push({
        name: 'customFilter',
        fn: function (item /*{File|FileLikeObject}*/, options) {
            return this.queue.length < 10;
        }
    });
    $scope.loadData = function () {

        dataservice.getById(para, function (rs) {
            $scope.model = rs;
            if ($scope.model.Image !== null && $scope.model.Image !== "") {
                $scope.model.imageLink = "/Customers/GetPic/" + rs.CustomerGuid;
            }
            $scope.model.StartDate !== null ? $scope.model.StartDate = "/Date(" + new Date($scope.model.StartDate).getTime() + ")/" : undefined;
            $scope.model.EndDate !== null ? $scope.model.EndDate = "/Date(" + new Date($scope.model.EndDate).getTime() + ")/" : undefined;
            $scope.model.DueDate !== null ? $scope.model.DueDate = "/Date(" + new Date($scope.model.DueDate).getTime() + ")/" : undefined;
            if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                $scope.$apply();
            }
            var ob = {
                RecordGuid: $scope.model.CustomerGuid
            }
            dataservice.GetAttachments(ob, function (result) {
                $scope.jdataattach = result;
            });

        });
    };
    $scope.loadData();
});
