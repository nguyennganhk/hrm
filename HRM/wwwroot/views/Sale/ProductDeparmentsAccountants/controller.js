﻿var ctxfolder = "/views/Sale/ProductDeparmentsAccountants";
app.factory('dataservice', function ($http) {
    var headers = {
        "Content-Type": "application/json;odata=verbose",
        "Accept": "application/json;odata=verbose"
    };
    return {
        //------------------------*Store*-------------------------
        getById: function (data, callback) {
            $http.post('/ProductDeparments/GetItem?Id=' + data).success(callback);
        },
        delete: function (data, callback) {
            $http.post('/ProductDeparments/Delete?Id=' + data).success(callback);
        },
        getItemALL: function (data, callback) {
            $http.post('/ProductDeparments/getItemjexcel/', data).success(callback);
        },
        getItem_by_ItemId: function (data, callback) {
            $http.post('/ProductDeparments/getItem_by_ItemId/', data).success(callback);
        },
        getUnitJexcel: function (data, callback) {
            $http.post('/ProductDeparments/getUnitJexcel/', data).success(callback);
        },
        getUnit_by_ItemId: function (data, callback) {
            $http.post('/ProductDeparments/getUnit_by_ItemId/', data).success(callback);
        },
        GetProductionManager: function (callback) {
            $http.post('/ProductDeparments/GetProductionManager/').success(callback);
        },
        GetAllCommentPage: function (data, callback) {
            $http.post('/ProductDeparments/GetAllCommentPage', data).success(callback);
        },
        getattachment: function (data, callback) {
            $http.post('/ProductDeparments/GetItemAttachment/', data).success(callback);
        },
       
        GetAllPaymentOrders: function (data, callback) {
            $http.post('/ProductDeparments/GetAllPaymentOrders/', data).success(callback);
        },
        Update_VATAmountOC: function (data, callback) {
            $http.post('/ProductDeparmentsAccountants/Update_VATAmountOC/', data).success(callback);
        },
        
    };

});
app.filter('html', ['$sce', function ($sce) {
    return function (text) {
        return $sce.trustAsHtml(text);
    };
}]);
app.filter('fdate', [
    '$filter', function ($filter) {
        return function (input, f) {
            if (input && input.toString().indexOf('Date') > -1) {
                return moment(input).format(f);
            } if (input && input.toString().indexOf('T') > -1) {
                return moment(input).format(f);
            } else return input;
        };
    }
]);
var spassAresSlug = function (number, isview) {
    try {
        // khai bao bien su dung       
        var numbers = number; var length = numbers.length; if (isview) {
            // khi dang la hien thi        
            if (numbers.indexOf(".") != -1) {
                length = numbers.indexOf(".");
                numbers = number.replace(".", ",");
            }
        } else {
            // khi dang la input textbox           
            if (numbers.indexOf(",") != -1) length = numbers.indexOf(",");
        }
        // loai bo nhung ky tu khong can thiet       
        // check so 0         
        var isTrue = true; while (isTrue) {
            if (numbers[0] == "0") numbers = numbers.substr(1, numbers.length);
            else isTrue = false;
        };
        // check dau phay      
        numbers = numbers.split(",");
        var string = "";
        for (var i = 0; i < numbers.length; i++) {
            if (i == 0) string += numbers[i];
            else if (i == 1) string += "," + numbers[i];
            else string += numbers[i];
        } numbers = string;
        // dung chung cho 2 truong hop       
        var no = 3, index = length - no;
        while (index > 0) { numbers = numbers.substr(0, index) + '.' + numbers.substr(index); index -= no; };
        return numbers == "" ? 0 : numbers;
    } catch (ex) { }
    return numbers == "" ? 0 : numbers;
}

app.directive('formatsub', ['$filter', function ($filter) {
    return {
        require: '?ngModel', link: function (scope, elem, attrs, ctrl) {
            if (!ctrl) return; ctrl.$formatters.unshift(function (viewValue) {
                try {
                    plainNumber = viewValue.replace('.', ','); elem.val(spassAresSlug(plainNumber));
                    return spassAresSlug(plainNumber);
                } catch (ex) { return undefined; }
            }); ctrl.$parsers.unshift(function (viewValue) {
                plainNumber = viewValue.replace(/[^-0-9,]/g, '');
                elem.val(spassAresSlug(plainNumber)); return plainNumber.replace(/[,]/g, '.');
            });
        }
    };
}]);
function onlickFileComment(d) {
    var __el = document.getElementById('file-comment-id');
    __el.setAttribute('accept', '.rar, .zip');
    d === 1 ? __el.setAttribute('accept', 'video/mp4,video/x-m4v,video/*') : '';
    d === 2 ? __el.setAttribute('accept', '.jpg, .png, .jpeg, .gif, .bmp, .tif, .tiff|images/*') : '';
    __el.click();
}
app.controller('Ctrl_ES_VCM_ProductDeparmentsAccountants', function ($scope, $rootScope, $sce, $compile, $uibModal, dataservice, utility) {
    $rootScope.Title = "";
    $scope.utility = utility;
    $rootScope.TableName = "/ProductDeparments";
    $rootScope.go = function (path) {
        $location.path(path); return false;
    };
    //$rootScope.ListProductionManager = [];
    //$rootScope.GetProductionManager = function () {
    //    dataservice.GetProductionManager(function (rs) {
    //        $rootScope.ListProductionManager = rs;
    //    });
    //};
    //$rootScope.GetProductionManager();
    var obj = {
        Keyword: ""
    }
    $rootScope.LoadItemALL = function () {
        dataservice.getItemALL(obj, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {
                $rootScope.listItemId = rs;
            }
        })
    }
    $rootScope.LoadItemALL();

    dataservice.getUnitJexcel(obj, function (rs) {
        if (rs.Error) {
            App.notifyDanger(rs.Title);
        } else {
            $rootScope.litsUnitID = rs;
        }
    })

    $rootScope.validationOptions = {
        rules: {
            OrderID: {
                required: true,
                maxlength: 50
            }
        },
        messages: {
            OrderID: {
                required: "Yêu cầu nhập mã đơn hàng.",
                maxlength: "Mã đơn hàng không vượt quá 50 ký tự"
            }
        }
    };


    $rootScope.dOrderStatus = [
        {
            value: "",
            text: 'Tất cả'
        },
        {
            value: "0",
            text: 'Đang thực hiện'
        }, {
            value: "1",
            text: 'Đã giao hàng'
        }];
    $rootScope.OrderStatusData = [
        {
            value: "0",
            text: 'Đang thực hiện'
        }, {
            value: "1",
            text: 'Đã giao hàng'
        }];
    $rootScope.ColumnMultiConfig = {
        selectAll: true,
        search: true,
        searchText: 'Tìm kiếm...',
        showTitle: 'true',
        csvSepChar: ',',
        captionFormat: '{0} lựa chọn',
        captionFormatAllSelected: '{0} lựa chọn',
        noMatch: 'Không có dữ liệu "{0}"',
        okCancelInMulti: true,
        locale: ['Đồng ý', 'Hủy', 'Chọn tất cả']
    };
    $rootScope.ColumnConfig = {
        search: true
    };

    //định dạng số xóa dấu chấm phẩy
    $rootScope.addPeriod = function (nStr) {
        if (nStr !== null && nStr !== "" && nStr !== undefined) {
            nStr = Math.round(parseFloat(nStr) * 100) / 100;

            nStr += "";
            if (nStr.indexOf(",") >= 0) {
                var x = nStr.split(",");
            } else {
                var x = nStr.split(".");
            }
            var x1 = x[0];
            var x2 = x.length > 1 && parseInt(x[1]) > 0 ? "," + x[1] : "";
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, "$1" + "." + "$2");
            }
            var Total = x1 + x2;
            return Total;
        }
        else {
            return 0;
        }
    };
    //định dạng số xóa dấu chấm Chấm
    $rootScope.ConvertCham = function (nStr) {
        if (nStr !== null && nStr !== "" && nStr !== undefined && nStr !="0") {
            nStr += ""; var x1 = ""; var x2 = "";
            if (nStr.indexOf(".") >= 0) {
                var x = nStr.split(".");
                angular.forEach(x, function (value, key) {
                    if (value.indexOf(",") >= 0) {
                        var x3 = nStr.split(",");
                        x2 += x3[1];
                    } else {
                        x1 += value;
                    }                   
                });
            } else {
                var x = nStr.split(",");
                x1 += x[0] + "." + x[1];              
            }                
            var Total = x1 + x2;
            return Total;
        }
        else {
            return 0;
        }
    };

    //Thêm phần tinymceOptions
    $rootScope.tinymceOptions = {
        selector: 'textarea',
        plugins: [
            'advlist autolink lists link charmap print preview hr anchor pagebreak image',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
        ],
        skin: 'lightgray', language: 'vi_VN',
        theme: 'modern', height: 70,
        toolbar1: 'codesample | bold italic sizeselect fontselect fontsizeselect | hr alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | insertfile undo redo | emoticons | code',
        fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
        toolbar2: 'print preview insert link image media | forecolor backcolor emoticons | codesample',
        image_advtab: true,

        file_browser_callback: function (field_name, url, type, win) {
            var connector = "/FileManager/Folder/File";
            var enableAutoTypeSelection = true;
            if (enableAutoTypeSelection && type) {
                connector += "&Type=" + type;
            }
            connector += "&Field=" + field_name;
            App.openPage(connector, "File Manager", 1150, 600);
        },
        relative_urls: false,
        remove_script_host: true,
        convert_urls: true
    };

    $rootScope.ConDate = function (data, number) {
        try {
            if (data == null || data == "") {
                return '';
            }
            if (data !== null && data != "" && data != undefined) {
                try {
                    if (data.indexOf("SA") != -1 || data.indexOf("CH") != -1) {
                        if (data.indexOf("SA") != -1) { }
                        if (data.indexOf("CH") != -1) { }
                    }

                    if (data.indexOf('Date') != -1) {
                        data = data.substring(data.indexOf("Date") + 5);
                        data = parseInt(data);
                    }
                }
                catch (ex) { }
                var newdate = new Date(data);
                if (number == 3) {
                    return newdate;
                }
                if (number == 2) {
                    return "/Date(" + newdate.getTime() + ")/";
                }
                var month = newdate.getMonth() + 1;
                var day = newdate.getDate();
                var year = newdate.getFullYear();
                var hh = newdate.getHours();
                var mm = newdate.getMinutes();
                if (month < 10)
                    month = "0" + month;
                if (day < 10)
                    day = "0" + day;
                if (mm < 10)
                    mm = "0" + mm;
                if (number == 0) {
                    return todayDate = day + "/" + month + "/" + year;
                }
                if (number == 1) {
                    return todayDate = day + "/" + month + "/" + year + " " + hh + ":" + mm;
                }
                if (number == 4) {
                    return new Date(year, month - 1, day);
                }
            } else {
                return '';
            }
        } catch (ex) {
            return "";
        }
    }
});
app.config(function ($routeProvider, $validatorProvider) {
    $routeProvider
        .when('/', {
            templateUrl: ctxfolder + '/index.html',
            controller: 'index'
        })
        .when('/edit/:id', {
            templateUrl: ctxfolder + '/edit.html',
            controller: 'edit'
        })
        .when('/add/', {
            templateUrl: ctxfolder + '/add.html',
            controller: 'add'
        });
    $validatorProvider.setDefaults({
        errorElement: 'span',
        errorClass: 'help-block',
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        success: function (label) {
            label.closest('.form-group').removeClass('has-error');
        }
    });
});
//controller hiển thị, tìm kiếm
app.controller('index', function ($http, $scope, $timeout, $uibModal, $rootScope, $compile, $confirm, $ngConfirm, $uibModal, DTOptionsBuilder, DTColumnBuilder, DTInstances, dataservice, keyhandle, utility) {
    $scope.utility = utility;
    $scope.model = {};
    var vm = $scope;
    var ctrl = $scope;
    ctrl.nextRow = function (d) {
        if (ctrl.ElementRowCheck === -1) {
            if (ctrl.allRow.length > 1) {
                ctrl.ElementRowCheck = ctrl.allRow[0].row;
                $('td', ctrl.ElementRowCheck).addClass("es-iSeleted");
                ctrl.iSelected = ctrl.allRow[0].data;
                ctrl.initData(ctrl.iSelected.RowGuid);

            }
        } else
            for (var i = 0; i < ctrl.allRow.length; i++) {
                if (ctrl.allRow[i].row === ctrl.ElementRowCheck) {
                    if (i === ctrl.allRow.length - 1) break;
                    $('td', ctrl.ElementRowCheck).removeClass('es-iSeleted');
                    ctrl.ElementRowCheck = ctrl.allRow[i + 1].row;
                    $('td', ctrl.ElementRowCheck).addClass("es-iSeleted");
                    ctrl.iSelected = ctrl.allRow[i + 1].data;
                    ctrl.initData(ctrl.iSelected.RowGuid);
                    break;
                }
            }
    }
    ctrl.upRow = function (d) {
        if (ctrl.ElementRowCheck === -1) {
            if (ctrl.allRow.length > 1) {
                ctrl.ElementRowCheck = ctrl.allRow[ctrl.allRow.length - 1].row;
                $('td', ctrl.ElementRowCheck).addClass("es-iSeleted");
                ctrl.iSelected = ctrl.allRow[ctrl.allRow.length - 1].row;
                ctrl.initData(ctrl.iSelected.RowGuid);
            }
        } else
            for (var i = 0; i < ctrl.allRow.length; i++) {
                if (ctrl.allRow[i].row === ctrl.ElementRowCheck) {
                    if (i === 0) break;
                    $('td', ctrl.ElementRowCheck).removeClass('es-iSeleted');
                    ctrl.ElementRowCheck = ctrl.allRow[i - 1].row;
                    $('td', ctrl.ElementRowCheck).addClass("es-iSeleted");
                    ctrl.iSelected = ctrl.allRow[i - 1].data;
                    ctrl.initData(ctrl.iSelected.RowGuid);
                    break;
                }
            }
    }
    keyhandle.eventCtrl(40, ctrl.nextRow);
    keyhandle.eventCtrl(38, ctrl.upRow);
    ctrl.ElementRowCheck = -1;
    ctrl.allRow = [];
    ctrl.iSelected = {};
    ctrl.dataScrollPage = [];
    $scope.staticParam = {
        totalItems: 0,
        currentPage: 1,
        pageSize: 25,
        maxSize: 5,
        Search: "",
        Status: "0",
        RowGuid: "",
        OrderID: ""
    };

    $scope.selected = [];
    $scope.selectAll = false;
    $scope.toggleOne = toggleOne;
    $scope.toggleAll = toggleAll;

    var titleHtml = '<label style="text-align: center;" class="mt-checkbox" ><input type="checkbox" ng-model="selectAll" ng-click="toggleAll(selectAll, selected)"/><span></span></label>';
    //if (App.Permissions.LISTVIEW)
    vm.dtOptions = DTOptionsBuilder.newOptions()
        .withPaginationType('full_numbers')
        .withDataProp('data').withDisplayLength(15)
        .withOption('order', [2, 'desc'])
        .withOption('serverSide', true)
        .withOption('pageLength', 25)
        .withOption('info', false)
        .withOption('stateLoadParams', function (settings, data) {
            data.search.search = '';
        })
        .withOption('headerCallback', function (header) {
            if (!$scope.headerCompiled) {
                $scope.headerCompiled = true;
                $compile(angular.element(header).contents())($scope);
            }
        })
        .withOption('initComplete', function (settings, json) {
            //thêm sự kiện scroll phân trang
            //  $('#tblData').parent().attr("onscroll", "angular.element(this).scope().LoadScroll(this)");
            PLUGIN_DATATABLE_TABLE_SCROLL.INIT_EVENT('tblData', $scope.LoadScroll);

        })
        .withOption('createdRow', function (row, data, dataIndex) {
            const contextScope = $scope.$new(true);
            contextScope.data = data;
            $scope.data = data;
            if (data._STT.toString() === "1") {
                if (ctrl.allRow.length === 0) {
                    $('td', row).addClass("es-iSeleted");
                    vm.ElementRowCheck = row;
                    ctrl.ElementRowCheck = row;
                    ctrl.iSelected = data;
                } else {
                    if (ctrl.allRow[0] !== null) {
                        $('td', ctrl.allRow[0].row).addClass("es-iSeleted");
                        ctrl.ElementRowCheck = ctrl.allRow[0].row;
                        ctrl.iSelected = ctrl.allRow[0].data;
                        vm.staticParam.RowGuid = ctrl.allRow[0].data.RowGuid;
                    }
                }

            }
            $compile(row)($scope);
          /*  if (App.Permissions.FULLCONTROL) {*/
                contextScope.contextMenu = $scope.contextMenuFull;
            //} else if (App.Permissions.ADD) {
            //    contextScope.contextMenu = $scope.contextMenu2;
            //} else if (App.Permissions.EDIT) {
            //    contextScope.contextMenu = $scope.contextMenu3;
            //} else if (App.Permissions.DELETE) {
            //    contextScope.contextMenu = $scope.contextMenu3;
            //} else {
            //    contextScope.contextMenu = $scope.contextMenu;
            //}
           $compile(angular.element(row).find('input'))($scope);
            $compile(angular.element(row).attr('context-menu', 'contextMenu'))(contextScope);
        });
    // if (App.Permissions.LISTVIEW)
    vm.dtOptions.withOption('ajax', function (data, callback, settings) {
        data.Keyword = $scope.staticParam.Search;
        data.StartDate = $scope.staticParam.StartDate;
        data.EndDate = $scope.staticParam.EndDate;
        data.Status = $scope.staticParam.Status;
        App.blockUI({
            target: "#tblData",
            boxed: true,
            message: 'Đang tải...'
        });
        $http.post('/ProductDeparments/JTable', data)
            .success(function (res) {
                $scope.selected = [];
                if (data.start === 0) {
                    $scope.dataload = [];
                    angular.forEach(res.data, function (val, key) {
                        $scope.dataload.push(val);
                    });
                    callback({
                        data: $scope.dataload,
                        recordsTotal: res.recordsTotal,
                        recordsFiltered: res.recordsFiltered
                    });
                } else {
                    if (res.recordsTotal > $scope.dataload.length) {
                        angular.forEach(res.data, function (val, key) {
                            $scope.dataload.push(val);
                        });
                        callback({
                            data: $scope.dataload,
                            recordsTotal: res.recordsTotal,
                            recordsFiltered: res.recordsFiltered
                        });
                    }
                }
                for (var i = 0; i < ctrl.allRow.length; i++) {
                    if (ctrl.allRow[i].data.RowGuid === ctrl.iSelected.RowGuid) {
                        $('td', ctrl.allRow[i].row).addClass("es-iSeleted");
                        ctrl.ElementRowCheck = ctrl.allRow[i].row;
                        ctrl.initData(ctrl.iSelected.RowGuid);
                        break;
                    }
                }
                if ($scope.dataload.length > 0) {
                    ctrl.initData($scope.dataload[0].RowGuid);
                } else {
                    ctrl.initData(null);
                }
                PLUGIN_DATATABLE_TABLE_SCROLL.UNLOCK_EVENT('tblData');
                App.unblockUI("#tblData");
            });
    });
    //scroll theo tỷ lệ màn hình
    vm.dtOptions.withOption('scrollY', '70vh')
        .withOption('scrollX', '100%') //mặc định 100%
        .withOption('scrollCollapse', true) //xóa khoảng trắng khi dữ liệu trong bảng ít
        .withOption('rowCallback', rowCallback) //sự kiên click vào 1 dòng
        .withOption('scroller', {
            loadingIndicator: true
        });
    //.withFixedColumns({
    //    leftColumns: 0 //fix bên trái 2 cột
    //    //rightColumns: 1 //fix bên phải 1 cột
    //}
    //);
    //table
    vm.dtColumns = [];

    vm.dtColumns.push(DTColumnBuilder.newColumn('RowGuid').withTitle('STT').notSortable().withOption('sWidth', '5px').renderWith(function (data, type, full, meta) {
        return meta.row + 1;
    }).withOption('sClass', 'tcenter tcenter-header'));
    vm.dtColumns.push(DTColumnBuilder.newColumn('OrderID').withTitle('Mã đơn hàng').withOption('sWidth', '60px').withOption('sClass', 'tcenter-header').renderWith(function (data, type, full, meta) {
        return '<a  style="color: #4285f4;" href="" ng-click="open(\'' + full.RowGuid + '\')" >' + data + '</a>';
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('CustomerName').withTitle('Khách hàng').withOption('sWidth', '200px').withOption('sClass', 'tcenter-header').renderWith(function (data, type, full, meta) {
        return data;
    }));

    vm.dtColumns.push(DTColumnBuilder.newColumn('DeliveryDate').withTitle('Ngày giao hàng').withOption('sWidth', '60px').withOption('sClass', 'tcenter').renderWith(function (data, type, full, meta) {
        return full.DeliveryDateString;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('EmployeeName').withTitle('Người thực hiện').withOption('sClass', 'tleft').withOption('sWidth', '40px').renderWith(function (data, type, full) {
        return data;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('OrderStatus').withTitle('Trạng thái').withOption('sWidth', '40px').withOption('sClass', 'tcenter').renderWith(function (data, type, full, meta) {
        return data;

    }));

    vm.dtColumns.push(DTColumnBuilder.newColumn('RowGuid').withTitle('Điều khiển').notSortable().withOption('sWidth', '100px').withOption('sClass', 'tcenter').renderWith(function (data, type, full, meta) {
        return '<div class="table__cell-actions-wrap">' +           
            '<a href="javascript:;" ng-click="comment(\'' + full.RowGuid + '\')" class="table__cell-actions-item table__cell-actions-icon"><span class="navbar-notify es-cursor" ><span><span class="navbar-notify__icon mdi mdi-comment-multiple-outline"></span></span><span class="navbar-notify__amount">' + full.CountComment + '</span></span ></a > ';
    }));
    vm.reloadData = reloadData;
    vm.dtInstance = {};
    function reloadData(resetPaging) {
        $scope.staticParam.currentPage = resetPaging;
        vm.dtInstance.reloadData(callback, resetPaging);
    }
    function callback(json) {

    }
    function toggleOne(selectedItems) {
        for (var id in selectedItems) {
            if (selectedItems.hasOwnProperty(id)) {
                if (!selectedItems[id]) {
                    vm.selectAll = false;
                    return;
                }
            }
        }
        vm.selectAll = true;
    }
    function toggleAll(selectAll, selectedItems) {
        for (var id in selectedItems) {
            if (selectedItems.hasOwnProperty(id)) {
                selectedItems[id] = selectAll;
            }
        }
    }
    $('#minize-menuleft').click(function () {
        vm.dtInstance.DataTable.draw();
    });
    function rowCallback(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        if (iDisplayIndexFull === 0) ctrl.allRow = [];
        $('td', nRow).unbind('click');
        $('td', nRow).bind('click', function () {
            if (vm.ElementRowCheck === nRow) return;
            $('td', nRow).addClass("es-iSeleted");
            $('td', vm.ElementRowCheck).removeClass('es-iSeleted');
            vm.ElementRowCheck = nRow;
            ctrl.ElementRowCheck = nRow;
            ctrl.iSelected = aData;
            vm.staticParam.RowGuid = aData.RowGuid;
            vm.staticParam.OrderID = aData.OrderID;
            $scope.initData(vm.staticParam.RowGuid);
        });
        $('td', nRow).unbind('dblclick');
        $('td', nRow).bind('dblclick', function () {
            $scope.$apply(function () {
                $scope.open(aData.RowGuid);
            });
        });
        ctrl.allRow.push({ row: nRow, data: aData });
        return nRow;
    }
    //Load dữ liệu cuộn
    $scope.total = 0;
    $scope.check = false;
    $scope.LoadScroll = function (obj) {
        vm.dtInstance.DataTable.page('next').draw('page');
    };

    $scope.reloadAll = function () {
        $scope.staticParam.Search = "";
        $scope.selectAll = false;
        reloadData(true);
    };
    $rootScope.reload = function () {
        $scope.dataload = [];
        reloadData(true);
    };
    ctrl.changeOptionDate = function (d) {
        if (d.length === 0) {
            ctrl.staticParam.StartDate = null;
            ctrl.staticParam.EndDate = null;
        } else {
            ctrl.staticParam.StartDate = $rootScope.ConDate(d[0], 2);
            ctrl.staticParam.EndDate = $rootScope.ConDate(d[1], 2);
        }
    }
    $scope.$watch('staticParam.StartDate', function (newvalue, oldvalue) {
        if (newvalue !== undefined && newvalue !== oldvalue) {
            $scope.reload();
        }
    });
    $scope.$watch('staticParam.EndDate', function (newvalue, oldvalue) {
        if (newvalue !== undefined && newvalue !== oldvalue) {
            $scope.reload();
        }
    });
    // thêm mới
    $scope.add = function () {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/add.html',
            controller: 'add',
            backdrop: 'static',
            keyboard: false,
            size: '100',
            resolve: {
                para: function () {
                    return "";
                }
            }
        });
        modalInstance.result.then(function (d) {
        }, function () {
        });
    };
    $scope.open = function (temp) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/open.html',
            controller: 'open',
            backdrop: 'static',
            size: '100',
            resolve: {
                para: function () {
                    return temp;
                }
            }
        });
        modalInstance.result.then(function (d) {
        }, function () {
        });

    };
    // sửa 
    $scope.edit = function (temp) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/add.html',
            controller: 'add',
            backdrop: 'static',
            size: '100',
            resolve: {
                para: function () {
                    return temp;
                }
            }
        });
    };
   
    // tình trạng thanh toán 
    $scope.addPaymentOrders = function (temp) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/PaymentOrders.html',
            controller: 'PaymentOrders',
            backdrop: 'static',
            size: '80',
            resolve: {
                para: function () {
                    return temp;
                }
            }
        });
    };
    $scope.roundNumber = function (number) {
        if (number > 0) {
            return parseFloat(number);
        } else return 0;

    }
    $scope.onload1 = function () {
        if ($('#myhtindex').jexcel('getData') !== undefined) {
            $scope.Quantity = 0;
            $scope.AmountOc = 0;
            $scope.VATPercent = 0;
            $scope.VATAmountOC = 0;
            $scope.AmountNoVAT = 0;
            $scope.ListData = $('#myhtindex').jexcel('getData');
            angular.forEach($scope.ListData, function (val, key) {
                if (val.Quantity !== null && val.Quantity !== "" && val.Quantity !== undefined) {
                    $scope.Quantity += $scope.roundNumber(parseFloat(val.Quantity));
                }
                if (val.VATPercent !== null && val.VATPercent !== "" && val.VATPercent !== undefined) {
                    $scope.VATPercent += $scope.roundNumber(parseFloat(val.VATPercent));
                }
                if (val.AmountOc !== null && val.AmountOc !== "" && val.AmountOc !== undefined) {
                    $scope.AmountOc += $scope.roundNumber(parseFloat(val.AmountOc));
                }
                if (val.VATAmountOC !== null && val.VATAmountOC !== "" && val.VATAmountOC !== undefined) {
                    $scope.VATAmountOC += $scope.roundNumber(parseFloat(val.VATAmountOC));
                }
                if (val.AmountNoVAT !== null && val.AmountNoVAT !== "" && val.AmountNoVAT !== undefined) {
                    $scope.AmountNoVAT += $scope.roundNumber(parseFloat(val.AmountNoVAT));
                }

            });
            $('#myhtindex #colfoot-8').html($rootScope.addPeriod($scope.Quantity));
            $('#myhtindex #colfoot-10').html($rootScope.addPeriod($scope.VATPercent));
            $('#myhtindex #colfoot-9').html($rootScope.addPeriod($scope.AmountOc));
            $('#myhtindex #colfoot-11').html($rootScope.addPeriod($scope.VATAmountOC));
            $('#myhtindex #colfoot-12').html($rootScope.addPeriod($scope.AmountNoVAT));
            if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                $scope.$apply();
            }
        }
    }
   
    $scope.initData = function (data) {
        dataservice.getById(data, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {
                $scope.lstDetail = JSON.parse(rs.Table5);               
                var ApplyOutsideDetails = [];
                angular.forEach($scope.lstDetail, function (value, key) {
                    ApplyOutsideDetails.push([
                        value.RowGuid,
                        value.ItemID,
                        value.ItemName,
                        value.Packing,
                        value.UnitID,
                        value.UnitName,
                        value.UnitPrice,
                        value.ExchangeRate,
                        value.Quantity,
                        value.AmountOc,
                        value.VATPercent1,
                        value.VATAmountOC1,
                        value.AmountNoVAT1,
                        value.Note
                    ]);
                });
                if ($scope.lstDetail == null) {
                    $scope.lstDetail1 = JSON.parse(rs.Table2);
                    var ApplyOutsideDetails = [];
                    angular.forEach($scope.lstDetail1, function (value, key) {
                        ApplyOutsideDetails.push([
                            value.RowGuid,
                            value.ItemID,
                            value.ItemName,
                            value.Packing,
                            value.UnitID,
                            value.UnitName,
                            value.UnitPrice,
                            value.ExchangeRate,
                            value.Quantity,
                            value.AmountOc,
                            0,
                           0,
                           0,
                           ''
                        ]);
                    });
                }
                $scope.jxcelAddDetailInit.data = ApplyOutsideDetails;
                $scope.onload1();
               
                if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                    $scope.$apply();
                }
            }
        })
    }
    $scope.contextMenuFull = [
        [function ($itemScope) {
            return '<i class="mdi mdi-eye"></i> Xem';
        }, function ($itemScope, $event, model) {
            $scope.open($itemScope.data.RowGuid);
        }],       
        [function ($itemScope) {
            return '<i class="mdi mdi-comment-outline"></i> Ý Kiến';
        }, function ($itemScope, $event, model) {
            loadTMStart();
            vm.staticParam.RowGuid = $itemScope.data.RowGuid;
            $scope.txtComment = $itemScope.data.OrderID;
            $scope.liComment = [];
            $scope.initPara = { Page: 1, ItemPage: 15, RecordGuid: vm.staticParam.RowGuid };
            $scope.loadDataComment();
        }],
        [function ($itemScope) {
            return '<i class="mdi mdi-attachment"></i> Đính kèm';
        }, function ($itemScope, $event, model) {
            $scope.AttchmentOrder($itemScope.data.RowGuid);
        }, function ($itemScope, $event, model) {
            return true;
        }],
    ];
    
    //comment
    $scope.comment = function (Id) {
        dataservice.getById(Id, function (rs) {
            loadTMStart();
            vm.staticParam.RowGuid = Id;
            var model = JSON.parse(rs.Table1)[0];
            $scope.txtComment = model.OrderID;
            $scope.liComment = [];
            $scope.initPara = { Page: 1, ItemPage: 15, RecordGuid: vm.staticParam.RowGuid };
            $scope.loadDataComment();
        });

    };
    $scope.liComment = [];
    $scope.loadDataComment = function () {
        if ($scope.initPara.RecordGuid !== "" && $scope.initPara.RecordGuid !== null && $scope.initPara.RecordGuid !== undefined) {
            dataservice.GetAllCommentPage($scope.initPara, function (rs) {
                if (rs.Error) {
                    App.notifyDanger(rs.Title);
                } else {
                    $scope.IconLoad = false;
                    for (var i = 0; i < rs.Object.length; i++) {
                        $scope.liComment.unshift(rs.Object[i]);
                    }
                    $scope.total_comment = $scope.liComment.length;

                    angular.forEach($scope.liComment, function (value, key) {
                        if (value.CreatedBy.split('#')[0] === $rootScope.loginName) {
                            value.Class = "is-interlocutor";
                        } else {
                            value.Class = "is-self";
                        }

                        if (value.CommentAttachments !== null) {
                            var listAttachments = [];
                            if (value.CommentAttachments.includes('|')) {
                                var data = value.CommentAttachments.split('|');

                                var obj = {
                                    AttachmentGuid: data[0],
                                    FileName: data[1],
                                    checksrc: false
                                };
                                if (data[1].includes('.')) {
                                    obj.FileExtension = data[1].split('.')[1];
                                } else {
                                    obj.FileExtension = data[1];
                                }
                                if (!obj.FileExtension.includes('xlsx') && !obj.FileExtension.includes('xls') && !obj.FileExtension.includes('png') && !obj.FileExtension.includes('docx') && !obj.FileExtension.includes('doc') && !obj.FileExtension.includes('pdf') && !obj.FileExtension.includes('txt') && !obj.FileExtension.includes('png') && !obj.FileExtension.includes('jpg')) {
                                    obj.checksrc = false;
                                } else {
                                    obj.checksrc = true;
                                }
                                listAttachments.push(obj);
                            } else {
                                var data1 = value.CommentAttachments;
                                angular.forEach(data1, function (val, keys) {
                                    var obj = {
                                        AttachmentGuid: val.AttachmentGuid,
                                        FileName: val.FileName
                                    }
                                    if (val.FileExtension.includes('.')) {
                                        obj.FileExtension = val.FileExtension.split('.')[1];
                                    } else {
                                        obj.FileExtension = val.FileExtension;
                                    }
                                    if (!obj.FileExtension.includes('xlsx') && !obj.FileExtension.includes('xls') && !obj.FileExtension.includes('png') && !obj.FileExtension.includes('docx') && !obj.FileExtension.includes('doc') && !obj.FileExtension.includes('pdf') && !obj.FileExtension.includes('txt') && !obj.FileExtension.includes('png') && !obj.FileExtension.includes('jpg')) {
                                        obj.checksrc = false;
                                    } else {
                                        obj.checksrc = true;
                                    }
                                    listAttachments.push(obj);
                                });
                            }
                            value.CommentAttachments = listAttachments;

                        }
                    });
                    gotoBottom('setting-content-scroll');
                }
            })
        }

    }
    //  $scope.loadDataComment();
    $scope.insertcomment = function () {
        if ($scope.model.Comment === undefined || $scope.model.Comment === "") {
            App.notifyDanger("Bạn phải nhập ý kiến");
            return true;
        }
        var fd = new FormData();
        $scope.model.RecordGuid = vm.staticParam.RowGuid;
        var file = $('#file-comment-id').prop('files')[0];
        var message = $('#message').val().replace(/(\n)+/g, '<br />');
        $scope.model.Comment = message;
        fd.append('insert', JSON.stringify($scope.model));
        fd.append('file', file);
        $.ajax({
            type: "POST",
            url: "/ProductDeparments/InsertComments",
            contentType: false,
            processData: false,
            data: fd,
            success: function (rs) {
                if (rs.Error) {
                    App.notifyDanger(rs.Title);
                }
                else {
                    $('#message').val('');
                    $scope.model.Comment = "";
                    $scope.NameFile = undefined;
                    $("#file-comment-id").val('');
                    $scope.liComment = [];
                    $scope.initPara = { Page: 1, ItemPage: 15, RecordGuid: vm.staticParam.RowGuid };
                    $scope.loadDataComment();
                }
            }
        });
    }
    $scope.FileNameChnage = '';
    $scope.addNameFile = function () {
        if ($('#file-comment-id').prop('files')[0] !== undefined)
            if ($scope.model !== undefined && $scope.model.Comment !== '') {
                $scope.model.Comment += " " + $('#file-comment-id').prop('files')[0].name;
            } else {
                $scope.model.Comment = " " + $('#file-comment-id').prop('files')[0].name;
            }
        if (!$scope.$$phase) {
            $scope.$apply();
        }
    }
    function loadTMStart() {
        setTimeout(_loadTMStart, 100);
    }
    function _loadTMStart() {
        var _el = document.getElementsByClassName('settings-panel')[0];
        _el.classList.add('is-opened');
    }
    $scope.ChangeStatusCompleted = function (data) {
        $scope.staticParam.Status = data;
        $rootScope.reload();
    }
    $scope.AttchmentOrder = function (data) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/addAttchment.html',
            controller: 'addattchment',
            backdrop: 'static',
            size: '80',
            resolve: {
                para: function () {
                    return data;
                },
                data: function () {
                    return "";
                },
                para1: function () {
                    return $scope.openAttach;
                }
            }
        });
    }

    $scope.rowchange = function (obj, row, cell, columnName) {
        var rowindex = parseInt(row + 1); // dòng muốn thay đổi      
        $scope.ListData = $scope.jxcelAddDetail.jexcel('getData');
        
        if (columnName === "VATAmountOC") {
            var VATAmountOC =$scope.ListData[row].VATAmountOC;
            var obj = {
                RowGuid: $scope.ListData[row].RowGuid,
                VATAmountOC: VATAmountOC
            }          
            dataservice.Update_VATAmountOC(obj, function (rs) {
                if (rs.Error) {
                    App.notifyDanger(rs.Title);
                } else {
                    App.notifyInfo(rs.Title);
                    $scope.initData(vm.staticParam.RowGuid);
                    if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                        $scope.$apply();
                    }
                }
            });
        }

       // $scope.onload1();
    };
    $scope.Attchment = function (obj, row) {
        var datarow = $('#myhtindex').jexcel('getData')[parseInt(obj.data.row)];
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/addAttchment.html',
            controller: 'addattchment',
            backdrop: 'static',
            size: '80',
            resolve: {
                para: function () {
                    return datarow.RowGuid;
                },
                data: function () {
                    return "";
                },
                para1: function () {
                    return $scope.openAttach;
                }
            }
        });
    }
    // tình trạng thanh toán 
    $scope.addPaymentOrders1 = function (obj, row) {
        var datarow = $('#myhtmuavtindex').jexcel('getData')[parseInt(obj.data.row)];
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/PaymentOrders.html',
            controller: 'PaymentOrders',
            backdrop: 'static',
            size: '80',
            resolve: {
                para: function () {
                    return datarow.RowGuid;
                }
            }
        });
    };
    $scope.Attchment1 = function (obj, row) {
        var datarow = $('#myhtmuavtindex').jexcel('getData')[parseInt(obj.data.row)];
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/addAttchment.html',
            controller: 'addattchment',
            backdrop: 'static',
            size: '80',
            resolve: {
                para: function () {
                    return datarow.RowGuid;
                },
                data: function () {
                    return "";
                },
                para1: function () {
                    return $scope.openAttach;
                }
            }
        });
    }

    $scope.jxcelAddDetailInit = {
        dataObject: true,
        reloadData: {},
        data: [],
        colHeaders: ['', 'Mã sản phẩm', 'Tên sản phẩm', "Quy cách đóng gói", 'Mã đơn vị tính', 'Tên đơn vị tính', 'Đơn giá', "Tỷ giá", 'Số lượng', 'Thành tiền', 'Chi phí', 'Tăng/Giảm', 'Lợi nhuận gộp', 'Ghi chú'
        ],
        colTitles: ['', 'Mã sản phẩm', 'Tên sản phẩm', "Quy cách đóng gói", 'Mã đơn vị tính', 'Tên đơn vị tính', 'Đơn giá', "Tỷ giá", 'Số lượng', 'Thành tiền',  'Chi phí', 'Tăng/Giảm', 'Lợi nhuận gộp', 'Ghi chú'
        ],
        colFooters: ['', '', '', '', '', '', '', '', '', '', '', '', '', ''
        ],
        columns: [
            {
                name: 'RowGuid',
                type: 'text',
                readOnly: true,
            },
            {
                name: 'ItemID',
                type: 'text', readOnly: true,
            },
            {
                name: 'ItemName',
                type: 'text',
                readOnly: true,
            },
            {
                name: 'Packing',
                type: 'text', readOnly: true,
            },
            {
                name: 'UnitID',
                type: 'autocomplete',
                source: $rootScope.litsUnitID,
                sourceReload: $scope.dropdownUnitID,
                searchReload: true,
                onlyCode: true,
                readOnly: true,
            },
            {
                name: 'UnitName',
                type: 'text',
                readOnly: true,
            },
            {
                name: 'UnitPrice',//Xếp hạng 15
                type: 'numeric', formatNumber: 'VN-NUMERIC', readOnly: true

            },
            {
                name: 'ExchangeRate',//Xếp hạng 15
                type: 'numeric', formatNumber: 'VN-NUMERIC', readOnly: true

            },
            {
                name: 'Quantity',//Xếp hạng 3
                type: 'numeric', formatNumber: 'VN-NUMERIC', readOnly: true
            },
            {
                name: 'AmountOc',//Xếp hạng 15
                type: 'numeric', formatNumber: 'VN-NUMERIC', readOnly: true

            },
            {
                name: 'VATPercent',//Xếp hạng 3
                type: 'numeric', formatNumber: 'VN-NUMERIC', readOnly: true
            },
            {
                name: 'VATAmountOC',//Xếp hạng 15
                type: 'numeric', formatNumber: 'VN-NUMERIC'

            },
            {
                name: 'AmountNoVAT',//Xếp hạng 3
                type: 'numeric', formatNumber: 'VN-NUMERIC', readOnly: true
            },            
            {
                name: 'Note',
                type: 'text', readOnly: true
            },
            {
                name: 'SortOrder',
                type: 'text',
                readOnly: false,
                updateReadOnly: true
            }
        ],

        columnShow: [false, true, true, true, true, true, true, true, true, true, true, true, true, true, true, false],
        initDataRows: ['', '', '', '', '', '', '', '', '', '', '', '', '', '', ''],
        colWidths: [50, 150, 200, 200, 100, 100, 150, 100, 100, 150, 150, 150, 100, 200, 200],
        allrowevent: true,
        tableOverflow: true,
        csvHeaders: true,
        tableHeight: '200px',
        changeCell: $scope.rowchange,
        oninsertrow: $scope.oninsertrow1,
        onload: $scope.onload1,
        allowInsertRow: false,
        contextMenu: function () { return ""; },
        contextMenuOption: [
            {
                title: 'Đính kèm',
                event: {
                    name: 'click',
                    callback: $scope.Attchment
                },
                element: "<i style='font-size: 17px;' class='mdi mdi-attachment'></i>"
            }
        ]
    };

    $scope.check = false;
    $scope.fullscreen = function (Check) {
        if (Check == false) {
            $scope.check = true;
        } else {
            $scope.check = false;
        }
    }
});
function gotoBottom(id) {
    var element = document.getElementById(id);
    element.scrollTop = 10;/* element.scrollHeight - element.clientHeight;*/
}
app.controller('add', function ($scope, $rootScope, $uibModalInstance, FileUploader, dataservice, para) {
    $scope.model = { Temps: "" };
    $scope.disColumn = false;
    $scope.check = false;
    $scope.fullscreen = function (Check) {
        if (Check == false) {
            $scope.check = true;
        } else {
            $scope.check = false;
        }
    }

    $scope.ListDetailDelete = [];
    $scope.ListDetailDelete1 = [];
    $scope.ListDetailDelete2 = [];
    $scope.ListDetailDelete3 = [];
    $scope.deleteItem = function (element, row) {
        $scope.ListData = $scope.jxcelAddDetail.jexcel('getData');
        if ($scope.ListData.length > 1) {
            ESConfirm([{ 'value': 1, 'title': 'Xác nhận', 'type': 'info' }, { 'value': 2, 'title': 'Đóng', 'type': 'danger' }], { 'description': 'dữ liệu này không ?', 'class': 'eswarning' }, function (rs) {
                if (rs === '1') {
                    var cell = 'A' + (parseInt(row) + 1);
                    var id = $scope.jxcelAddDetail.jexcel('getValue', cell);
                    if (id !== "" && id !== null) {
                        var sta = false;
                        for (var i = 0; i < $scope.ListDetailDelete.length; i++) {
                            if ($scope.ListDetailDelete[i] === id) {
                                sta = true;
                            }
                        }
                        if (!sta) {
                            $scope.ListDetailDelete.push(id);
                        }
                    }
                    $scope.jxcelAddDetail.jexcel('deleteRow', row);
                    $scope.onload1();

                    App.notifyInfo('Xóa thành công');
                }
                if (rs === '2') {
                    console.log('Không chấp nhận');
                }
            }, function () {
                //some error in this function
                console.log('Không lựa chọn');
            });
        }
        else {
            App.notifyDanger('Không xóa được dòng cuối cùng này');
            return;
        }
    };


    $scope.rowchange = function (obj, row, cell, columnName) {
        var rowindex = parseInt(row + 1); // dòng muốn thay đổi      
        $scope.ListData = $scope.jxcelAddDetail.jexcel('getData');
        //if (columnName === "ItemID") {
        //    dataservice.getItem_by_ItemId({ IdS: [$scope.ListData[row].ItemID] }, function (rs) {
        //        if (rs.Error) {
        //            App.notifyDanger(rs.Title);
        //        } else {
        //            $scope.jxcelAddDetail.jexcel("setValueByName", "ItemName", row, rs.ItemName);
        //            $scope.jxcelAddDetail.jexcel("setValueByName", "UnitID", row, rs.UnitID);
        //            dataservice.getUnit_by_ItemId({ IdS: [rs.UnitID] }, function (rs) {
        //                if (rs.Error) {
        //                    App.notifyDanger(rs.Title);
        //                } else {

        //                    $scope.jxcelAddDetail.jexcel("setValueByName", "UnitName", row, rs.UnitName);
        //                    if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
        //                        $scope.$apply();
        //                    }
        //                }
        //            });
        //            if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
        //                $scope.$apply();
        //            }
        //        }
        //    })
        //}
        if (columnName === "UnitID") {
            if ($scope.ListData[row].UnitID != "") {
                dataservice.getUnit_by_ItemId({ IdS: [$scope.ListData[row].UnitID] }, function (rs) {
                    if (rs.Error) {
                        App.notifyDanger(rs.Title);
                    } else {
                        $scope.jxcelAddDetail.jexcel("setValueByName", "UnitName", row, rs.UnitName);
                        if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                            $scope.$apply();
                        }
                    }
                });
            } else {
                $scope.jxcelAddDetail.jexcel("setValueByName", "UnitName", row, "");
                if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                    $scope.$apply();
                }
            }


        }
        if (columnName === "Quantity" || columnName === "UnitPrice") {
            var Quantity = parseFloat($scope.ListData[row].Quantity);
            var UnitPrice = parseFloat($scope.ListData[row].UnitPrice);
            var ExchangeRate = parseFloat($scope.ListData[row].ExchangeRate);
            var AmountOc = Quantity * UnitPrice * ExchangeRate;
            $scope.jxcelAddDetail.jexcel("setValueByName", "AmountOc", row, AmountOc);
        }

        $scope.onload1();
    };
    $scope.onpaste1 = function (obj, row) {

        $scope.ListData = $('#myht').jexcel('getData');
        $scope.ListData = $scope.ListData.filter(a => a.ItemID !== '');
        var list = $scope.ListData.map(a => a.ItemID);

        var fd = new FormData();
        fd.append('Insert', JSON.stringify($scope.model));
        fd.append('Detail', JSON.stringify(list));

        $.ajax({
            url: '/ProductionMaterials/GetItemsByItemlistId',
            data: fd,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (rs) {
                if (rs.Error) {
                    App.notifyDanger(rs.Title);
                }
                else {
                    $scope.ListData1 = rs;
                    $scope.lstdata = [];
                    for (var j = 0; j < $scope.ListData.length; j++) {
                        let val1 = $scope.ListData1.find(o => o.ItemID.toUpperCase() === $scope.ListData[j].ItemID.toUpperCase());
                        var value = $scope.ListData[j];
                        value.ItemName = val1.ItemName !== null ? val1.ItemName : "";
                        value.UnitID = val1.UnitID !== null ? val1.UnitID : 0;
                        value.UnitName = val1.UnitName !== null ? val1.UnitName : 0;
                        var obj = [
                            value.RowGuid,
                            value.ItemID,
                            value.ItemName,
                            value.UnitID,
                            value.UnitName,
                            value.Quantity,
                            value.UnitPrice,
                            value.AmountOc,
                            value.VATPercent,
                            value.VATAmountOC,
                            value.Note];
                        $scope.lstdata.push(obj);
                    }
                    $scope.jxcelAddDetailInit.data = $scope.lstdata;
                    $scope.onload1();
                }
            },
            error: function (rs) {
                App.notifyInfo(rs.Title);
            }
        });
    }
    $scope.roundNumber = function (number) {
        if (number > 0) {
            return parseFloat(number);
        } else return 0;

    }
    $scope.onload1 = function () {
        if ($('#myht').jexcel('getData') !== undefined) {
            $rootScope.Quantity = 0;
            $rootScope.AmountOc = 0;
            $rootScope.UnitPrice = 0;
            $rootScope.VATAmountOC = 0;
            $scope.ListData = $('#myht').jexcel('getData');
            angular.forEach($scope.ListData, function (val, key) {
                if (val.Quantity !== null && val.Quantity !== "" && val.Quantity !== undefined) {
                    $rootScope.Quantity += $scope.roundNumber(parseFloat(val.Quantity));
                }
                if (val.UnitPrice !== null && val.UnitPrice !== "" && val.UnitPrice !== undefined) {
                    $rootScope.UnitPrice += $scope.roundNumber(parseFloat(val.UnitPrice));
                }
                if (val.AmountOc !== null && val.AmountOc !== "" && val.AmountOc !== undefined) {
                    $rootScope.AmountOc += $scope.roundNumber(parseFloat(val.AmountOc));
                }
                if (val.VATAmountOC !== null && val.VATAmountOC !== "" && val.VATAmountOC !== undefined) {
                    $rootScope.VATAmountOC += $scope.roundNumber(parseFloat(val.VATAmountOC));
                }
            });
            $('#myht #colfoot-8').html($rootScope.addPeriod($scope.Quantity));
            $('#myht #colfoot-6').html($rootScope.addPeriod($scope.UnitPrice));
            $('#myht #colfoot-9').html($rootScope.addPeriod($scope.AmountOc));
            if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                $scope.$apply();
            }
        }
    }

    $scope.dropdownItemId = function (d1, d2, d3) {
        var _tem1 = d1.ItemID;
        if (_tem1 === "" || _tem1 === null) {
            _tem1 = "";
        }
        var obj = {
            Keyword: _tem1
        }
        dataservice.getItemALL(obj, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {

                d3.source = rs;
                d2(rs, "");
            }
        })
    }
    $scope.dropdownUnitID = function (d1, d2, d3) {
        var _tem1 = d1.UnitID;
        if (_tem1 === "" || _tem1 === null) {
            _tem1 = "";
        }
        var obj = {
            Keyword: _tem1
        }
        dataservice.getUnitJexcel(obj, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {

                d3.source = rs;
                d2(rs, "");
            }
        })
    }

    $scope.jxcelAddDetailInit = {
        dataObject: true,
        reloadData: {},
        data: [],
        colHeaders: ['', 'Mã sản phẩm', 'Tên sản phẩm', "Quy cách đóng gói", 'Mã đơn vị tính', 'Tên đơn vị tính', 'Đơn giá', "Tỷ giá", 'Số lượng', 'Thành tiền', 'Ghi chú'
        ],
        colTitles: ['', 'Mã sản phẩm', 'Tên sản phẩm', "Quy cách đóng gói", 'Mã đơn vị tính', 'Tên đơn vị tính', 'Đơn giá', "Tỷ giá", 'Số lượng', 'Thành tiền', 'Ghi chú'
        ],
        colFooters: ['', '', '', '', '', '', '', '', '', '', ''
        ],
        columns: [
            {
                name: 'RowGuid',
                type: 'text',
                isFixColumn: true,
            },
            {
                name: 'ItemID',
                type: 'text',
                isFixColumn: true
            },
            {
                name: 'ItemName',
                type: 'text',
                isFixColumn: true,
            },
            {
                name: 'Packing',
                type: 'text'
            },
            {
                name: 'UnitID',
                type: 'autocomplete',
                source: $rootScope.litsUnitID,
                sourceReload: $scope.dropdownUnitID,
                searchReload: true,
                onlyCode: true,
                isFixColumn: true,
            },
            {
                name: 'UnitName',
                type: 'text',
                isFixColumn: true,
            },
            {
                name: 'UnitPrice',//Xếp hạng 15
                type: 'numeric', formatNumber: 'VN-NUMERIC'

            },
            {
                name: 'ExchangeRate',//Xếp hạng 15
                type: 'numeric', formatNumber: 'VN-NUMERIC'

            },
            {
                name: 'Quantity',//Xếp hạng 3
                type: 'numeric', formatNumber: 'VN-NUMERIC'
            },
            {
                name: 'AmountOc',//Xếp hạng 15
                type: 'numeric', formatNumber: 'VN-NUMERIC'

            },
            {
                name: 'Note',
                type: 'text'
            }
        ],

        columnShow: [false, true, true, true, true, true, true, true, true, true, true],
        initDataRows: ['', '', '', '', '', '', '', '', '', '', ''],
        colWidths: [50, 150, 200, 200, 150, 150, 150, 150, 100, 150, 300],
        allrowevent: true,
        deleterowconfirm: $scope.deleteItem,
        tableOverflow: true,
        csvHeaders: true,
        changeCell: $scope.rowchange,
        tableHeight: '200px',
        onpaste: $scope.onpaste1,
        onload: $scope.onload1
    };


    if (para != "") {
        $scope.Title = "Sửa đơn hàng";
        $scope.model.Temps = "2";
        $scope.disColumn = true;
        $scope.initData = function () {
            dataservice.getById(para, function (rs) {
                if (rs.Error) {
                    App.notifyDanger(rs.Title);
                } else {
                    $scope.model = JSON.parse(rs.Table1)[0];
                    $scope.model.DeliveryDate = $scope.model.DeliveryDate !== null ? "/Date(" + new Date($scope.model.DeliveryDate).getTime() + ")/" : null;
                    $scope.lstDetail = JSON.parse(rs.Table2);
                    var ApplyOutsideDetails = [];
                    angular.forEach($scope.lstDetail, function (value, key) {
                        ApplyOutsideDetails.push([
                            value.RowGuid,
                            value.ItemID,
                            value.ItemName,
                            value.Packing,
                            value.UnitID,
                            value.UnitName,
                            value.UnitPrice,
                            value.ExchangeRate,
                            value.Quantity,
                            value.AmountOc,
                            value.Note
                        ]);
                    });
                    $scope.jxcelAddDetailInit.data = ApplyOutsideDetails;
                    $scope.onload1();
                    if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                        $scope.$apply();
                    }
                }
            })
        }
        $scope.initData();
    } else {
        $scope.Title = "Thêm mới đơn hàng";
        $scope.model.Temps = "1";
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    //Cấu hình file đính kèm
    $scope.model.FileAttachments = [];
    var uploader = $scope.uploader = new FileUploader({
        // url: App.baseService() + '/upload'
    });
    uploader.filters.push({
        name: 'customFilter',
        fn: function (item /*{File|FileLikeObject}*/, options) {
            return this.queue.length < 10;
        }
    });
    uploader.onSuccessItem = function (fileItem, response, status, headers) {
        $scope.model.FileAttachments.push(response);
    };
    uploader.onCompleteAll = function () {
        $scope.commit();
    };
    $scope.ChangeProductionManagerGuid = function (data) {
        $scope.model.ProductionManagerGuid = data;
    }
    $scope.insert = function () {
        if ($scope.addform.validate()) {
            var fd = new FormData();
            $scope.lstTitlefile = [];
            if (uploader.queue.length > 0) {
                for (i = 0; i < uploader.queue.length; i++) {
                    var files = uploader.queue[i]._file;
                    if (files.size > 30720000) {
                        App.notifyDanger("Yêu cầu file đính kèm không vượt quá 3MB");
                        return;
                    }
                    fd.append("file", files);
                }
            }
            var datatab1 = $scope.jxcelAddDetail.jexcel('getData');
            var ApplyOutsideDetails = []; var ApplyOutsideDetails1 = []; var ApplyOutsideDetails2 = [];
            var ApplyOutsideDetails3 = [];
            for (var i = 0; i < datatab1.length; i++) {
                var value = datatab1[i];
                if (value.RowGuid != null && value.RowGuid != undefined && value.RowGuid != "") {
                    if (value.ItemID != null && value.ItemID != undefined && value.ItemID != "") {
                        ApplyOutsideDetails.push({
                            RowGuid: value.RowGuid,
                            ItemID: value.ItemID,
                            ItemName: value.ItemName,
                            Packing: value.Packing,
                            UnitID: value.UnitID,
                            UnitName: value.UnitName,
                            Quantity: value.Quantity,
                            UnitPrice: value.UnitPrice,
                            ExchangeRate: value.ExchangeRate,
                            AmountOc: value.AmountOc,
                            Note: value.Note,
                            SortOrder: i
                        });
                    }
                }
                else {
                    if (value.ItemID != null && value.ItemID != undefined && value.ItemID != "") {
                        ApplyOutsideDetails.push({
                            ItemID: value.ItemID,
                            ItemName: value.ItemName,
                            Packing: value.Packing,
                            UnitID: value.UnitID,
                            UnitName: value.UnitName,
                            Quantity: value.Quantity,
                            UnitPrice: value.UnitPrice,
                            ExchangeRate: value.ExchangeRate,
                            AmountOc: value.AmountOc,
                            Note: value.Note,
                            SortOrder: i
                        });
                    }
                }

            };
            if (ApplyOutsideDetails.length == 0) {
                App.notifyDanger("Yêu cầu nhập chi tiết");
                return;
            }

            fd.append('submit', JSON.stringify($scope.model));
            fd.append('detail', JSON.stringify(ApplyOutsideDetails));
            fd.append('detailDelete', JSON.stringify($scope.ListDetailDelete));

            fd.append('detail1', JSON.stringify(ApplyOutsideDetails1));
            fd.append('detailDelete1', JSON.stringify($scope.ListDetailDelete1));

            fd.append('detail2', JSON.stringify(ApplyOutsideDetails2));
            fd.append('detailDelete2', JSON.stringify($scope.ListDetailDelete2));

            fd.append('detail3', JSON.stringify(ApplyOutsideDetails3));
            fd.append('detailDelete3', JSON.stringify($scope.ListDetailDelete3));
            $.ajax({
                type: "POST",
                url: "/ProductDeparments/Submit",
                contentType: false,
                processData: false,
                data: fd,
                success: function (rs) {
                    if (rs.Error) {
                        App.notifyDanger(rs.Title);
                    } else {
                        App.notifyInfo(rs.Title);
                        $uibModalInstance.close();
                        $rootScope.reload();
                    }
                }
            });
        }
    };
});
// controller open
app.controller('open', function ($scope, $rootScope, $uibModalInstance, dataservice, para, FileUploader) {
    $scope.Title = "Xem đơn hàng";
    $scope.model = { Temps: "" };
    $scope.disColumn = false;
    $scope.check = false;
    $scope.fullscreen = function (Check) {
        if (Check == false) {
            $scope.check = true;
        } else {
            $scope.check = false;
        }
    }

    $scope.deleteItem = function (element, row) {
        if (row === 0) {
            $scope.jxcelAddDetail.jexcel("setValueByName", "Id", row, "");
            $scope.jxcelAddDetail.jexcel("setValueByName", "ItemID", row, "");
            $scope.jxcelAddDetail.jexcel("setValueByName", "ItemName", row, "");
            $scope.jxcelAddDetail.jexcel("setValueByName", "UnitID", row, "");
            $scope.jxcelAddDetail.jexcel("setValueByName", "UnitName", row, "");
            $scope.jxcelAddDetail.jexcel("setValueByName", "Quantity", row, "");
            $scope.jxcelAddDetail.jexcel("setValueByName", "UnitPrice", row, "");
            $scope.jxcelAddDetail.jexcel("setValueByName", "AmountOc", row, "");
            $scope.jxcelAddDetail.jexcel("setValueByName", "VATPercent", row, "");
            $scope.jxcelAddDetail.jexcel("setValueByName", "VATAmountOC", row, "");
            $scope.jxcelAddDetail.jexcel("setValueByName", "Note", row, "");
        }
        $scope.jxcelAddDetail.jexcel('deleteRow', row);
    };
    $scope.rowchange = function (obj, row, cell, columnName) {
        var rowindex = parseInt(row + 1); // dòng muốn thay đổi      
        $scope.ListData = $scope.jxcelAddDetail.jexcel('getData');
        if (columnName === "ItemID") {
            dataservice.getItem_by_ItemId({ IdS: [$scope.ListData[row].ItemID] }, function (rs) {
                if (rs.Error) {
                    App.notifyDanger(rs.Title);
                } else {
                    $scope.jxcelAddDetail.jexcel("setValueByName", "ItemName", row, rs.ItemName);
                    $scope.jxcelAddDetail.jexcel("setValueByName", "UnitID", row, rs.UnitID);
                    dataservice.getUnit_by_ItemId({ IdS: [rs.UnitID] }, function (rs) {
                        if (rs.Error) {
                            App.notifyDanger(rs.Title);
                        } else {

                            $scope.jxcelAddDetail.jexcel("setValueByName", "UnitName", row, rs.UnitName);
                            if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                                $scope.$apply();
                            }
                        }
                    });
                    if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                        $scope.$apply();
                    }
                }
            })
        }
        if (columnName === "UnitID") {
            if ($scope.ListData[row].UnitID != "") {
                dataservice.getUnit_by_ItemId({ IdS: [$scope.ListData[row].UnitID] }, function (rs) {
                    if (rs.Error) {
                        App.notifyDanger(rs.Title);
                    } else {
                        $scope.jxcelAddDetail.jexcel("setValueByName", "UnitName", row, rs.UnitName);
                        if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                            $scope.$apply();
                        }
                    }
                });
            } else {
                $scope.jxcelAddDetail.jexcel("setValueByName", "UnitName", row, "");
                if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                    $scope.$apply();
                }
            }


        }
        if (columnName === "Quantity" || columnName === "UnitPrice") {
            var Quantity = parseFloat($scope.ListData[row].Quantity);
            var UnitPrice = parseFloat($scope.ListData[row].UnitPrice);
            var AmountOc = Quantity * UnitPrice;
            $scope.jxcelAddDetail.jexcel("setValueByName", "AmountOc", row, AmountOc);
            var VATPercent = parseFloat($scope.ListData[row].VATPercent);
            var VATAmountOC = AmountOc - parseFloat(AmountOc) * VATPercent / 100;
            $scope.jxcelAddDetail.jexcel("setValueByName", "VATAmountOC", row, VATAmountOC);
        }
        if (columnName === "VATPercent") {
            var AmountOc = parseFloat($scope.ListData[row].AmountOc);
            var VATPercent = parseFloat($scope.ListData[row].VATPercent);
            var VATAmountOC = AmountOc - parseFloat(AmountOc) * VATPercent / 100;
            $scope.jxcelAddDetail.jexcel("setValueByName", "VATAmountOC", row, VATAmountOC);
        }
        $scope.onload1();
    };
    $scope.onpaste1 = function (obj, row) {

        $scope.ListData = $('#myht').jexcel('getData');
        $scope.ListData = $scope.ListData.filter(a => a.ItemID !== '');
        var list = $scope.ListData.map(a => a.ItemID);

        var fd = new FormData();
        fd.append('Insert', JSON.stringify($scope.model));
        fd.append('Detail', JSON.stringify(list));

        $.ajax({
            url: '/ProductionMaterials/GetItemsByItemlistId',
            data: fd,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (rs) {
                if (rs.Error) {
                    App.notifyDanger(rs.Title);
                }
                else {
                    $scope.ListData1 = rs;
                    $scope.lstdata = [];
                    for (var j = 0; j < $scope.ListData.length; j++) {
                        let val1 = $scope.ListData1.find(o => o.ItemID.toUpperCase() === $scope.ListData[j].ItemID.toUpperCase());
                        var value = $scope.ListData[j];
                        value.ItemName = val1.ItemName !== null ? val1.ItemName : "";
                        value.UnitID = val1.UnitID !== null ? val1.UnitID : 0;
                        value.UnitName = val1.UnitName !== null ? val1.UnitName : 0;
                        var obj = [
                            value.RowGuid,
                            value.ItemID,
                            value.ItemName,
                            value.UnitID,
                            value.UnitName,
                            value.Quantity,
                            value.UnitPrice,
                            value.AmountOc,
                            value.VATPercent,
                            value.VATAmountOC,
                            value.Note];
                        $scope.lstdata.push(obj);
                    }
                    $scope.jxcelAddDetailInit.data = $scope.lstdata;
                    $scope.onload1();
                }
            },
            error: function (rs) {
                App.notifyInfo(rs.Title);
            }
        });
    }
    $scope.roundNumber = function (number) {
        if (number > 0) {
            return parseFloat(number);
        } else return 0;

    }
    $scope.onload1 = function () {
        if ($('#myht').jexcel('getData') !== undefined) {
            $rootScope.Quantity = 0;
            $rootScope.AmountOc = 0;
            $rootScope.UnitPrice = 0;
            $rootScope.VATAmountOC = 0;
            $scope.ListData = $('#myht').jexcel('getData');
            angular.forEach($scope.ListData, function (val, key) {
                if (val.Quantity !== null && val.Quantity !== "" && val.Quantity !== undefined) {
                    $rootScope.Quantity += $scope.roundNumber(parseFloat(val.Quantity));
                }
                if (val.UnitPrice !== null && val.UnitPrice !== "" && val.UnitPrice !== undefined) {
                    $rootScope.UnitPrice += $scope.roundNumber(parseFloat(val.UnitPrice));
                }
                if (val.AmountOc !== null && val.AmountOc !== "" && val.AmountOc !== undefined) {
                    $rootScope.AmountOc += $scope.roundNumber(parseFloat(val.AmountOc));
                }

            });
            $('#myht #colfoot-8').html($rootScope.addPeriod($scope.Quantity));
           /* $('#myht #colfoot-6').html($rootScope.addPeriod($scope.UnitPrice));*/
            $('#myht #colfoot-9').html($rootScope.addPeriod($scope.AmountOc));
            if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                $scope.$apply();
            }
        }
    }

    $scope.dropdownItemId = function (d1, d2, d3) {
        var _tem1 = d1.ItemID;
        if (_tem1 === "" || _tem1 === null) {
            _tem1 = "";
        }
        var obj = {
            Keyword: _tem1
        }
        dataservice.getItemALL(obj, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {

                d3.source = rs;
                d2(rs, "");
            }
        })
    }
    $scope.dropdownUnitID = function (d1, d2, d3) {
        var _tem1 = d1.UnitID;
        if (_tem1 === "" || _tem1 === null) {
            _tem1 = "";
        }
        var obj = {
            Keyword: _tem1
        }
        dataservice.getUnitJexcel(obj, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {

                d3.source = rs;
                d2(rs, "");
            }
        })
    }
    $scope.jxcelAddDetailInit = {
        dataObject: true,
        reloadData: {},
        data: [],
        colHeaders: ['', 'Mã sản phẩm', 'Tên sản phẩm', "Quy cách đóng gói", 'Mã đơn vị tính', 'Tên đơn vị tính', 'Đơn giá', "Tỷ giá", 'Số lượng', 'Thành tiền', 'Ghi chú'
        ],
        colTitles: ['', 'Mã sản phẩm', 'Tên sản phẩm', "Quy cách đóng gói", 'Mã đơn vị tính', 'Tên đơn vị tính', 'Đơn giá', "Tỷ giá", 'Số lượng', 'Thành tiền', 'Ghi chú'
        ],
        colFooters: ['', '', '', '', '', '', '', '', '', '', ''
        ],
        columns: [
            {
                name: 'RowGuid',
                type: 'text',
                isFixColumn: true, readOnly: true,
            },
            {
                name: 'ItemID',
                type: 'text',
                isFixColumn: true, readOnly: true
            },
            {
                name: 'ItemName',
                type: 'text',
                isFixColumn: true, readOnly: true,
            },
            {
                name: 'Packing',
                type: 'text', readOnly: true
            },
            {
                name: 'UnitID',
                type: 'autocomplete',
                source: $rootScope.litsUnitID,
                sourceReload: $scope.dropdownUnitID,
                searchReload: true,
                onlyCode: true,
                isFixColumn: true, readOnly: true,
            },
            {
                name: 'UnitName',
                type: 'text',
                isFixColumn: true, readOnly: true,
            },
            {
                name: 'UnitPrice',//Xếp hạng 15
                type: 'numeric', formatNumber: 'VN-NUMERIC', readOnly: true

            },
            {
                name: 'ExchangeRate',//Xếp hạng 15
                type: 'numeric', formatNumber: 'VN-NUMERIC', readOnly: true

            },
            {
                name: 'Quantity',//Xếp hạng 3
                type: 'numeric', formatNumber: 'VN-NUMERIC', readOnly: true
            },
            {
                name: 'AmountOc',//Xếp hạng 15
                type: 'numeric', formatNumber: 'VN-NUMERIC', readOnly: true

            },
            {
                name: 'Note',
                type: 'text', readOnly: true
            }
        ],

        columnShow: [false, true, true, true, true, true, true, true, true, true, true],
        initDataRows: ['', '', '', '', '', '', '', '', '', '', ''],
        colWidths: [50, 150, 200, 200, 150, 150, 150, 150, 100, 150, 300],
        allrowevent: true,
        allowInsertRow: false,
        deleterowconfirm: $scope.deleteItem,
        tableOverflow: true,
        csvHeaders: true,
        changeCell: $scope.rowchange,
        tableHeight: '200px',
        onpaste: $scope.onpaste1,
        onload: $scope.onload1,
        contextMenu: function () { return ""; },

    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
    var uploader = $scope.uploader = new FileUploader({
        // url: App.baseService() + '/upload'
    });
    uploader.filters.push({
        name: 'customFilter',
        fn: function (item /*{File|FileLikeObject}*/, options) {
            return this.queue.length < 10;
        }
    });
    $scope.initData = function () {
        dataservice.getById(para, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {
                $scope.model = JSON.parse(rs.Table1)[0];
                $scope.model.DeliveryDate = $scope.model.DeliveryDate !== null ? "/Date(" + new Date($scope.model.DeliveryDate).getTime() + ")/" : null;
                $scope.lstDetail = JSON.parse(rs.Table2);
                var ApplyOutsideDetails = [];
                angular.forEach($scope.lstDetail, function (value, key) {
                    ApplyOutsideDetails.push([
                        value.RowGuid,
                        value.ItemID,
                        value.ItemName,
                        value.Packing,
                        value.UnitID,
                        value.UnitName,
                        value.UnitPrice,
                        value.ExchangeRate,
                        value.Quantity,
                        value.AmountOc,
                        value.Note
                    ]);
                });
                $scope.jxcelAddDetailInit.data = ApplyOutsideDetails;
                $scope.onload1();
                if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                    $scope.$apply();
                }
            }
        })
    }
    $scope.initData();
});
app.controller('addattchment', function ($scope, $rootScope, $compile, $uibModal, $confirm, $uibModalInstance, dataservice, para, para1, FileUploader) {
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
    $scope.model = { RecordGuid: para, STT: para1 };
    $scope.nameController = "";

    //Cấu hình file đính kèm
    var uploader = $scope.uploader = new FileUploader({
        // url: App.baseService() + '/upload'
    });
    uploader.filters.push({
        name: "customFilter",
        fn: function (item /*{File|FileLikeObject}*/, options) {
            return this.queue.length < 10;
        }
    });
    uploader.onCompleteAll = function () {
    };

    // Bảo hiểm
    $scope.getattachment = function () {
        var obj = {
            RecordGuid: $scope.model.RecordGuid
        }
        dataservice.getattachment(JSON.stringify(obj), function (result) {
            $scope.jdataattach = result;
            angular.forEach($scope.jdataattach, function (value, key) {
                if (value.CreatedBy !== null && value.CreatedBy !== undefined) {
                    if (value.CreatedBy.includes('#')) {
                        value.CreatedBy = value.CreatedBy.split('#')[1];
                    }
                }
            })
        });
    }
    $scope.getattachment(); // danh sách attach bảo hiểm

    $scope.lstDeleteFile = [];
    $scope.remoteAttach = function (item, number) {
        $scope.lstDeleteFile.push(item.AttachmentGuid);
        $scope.jdataattach.splice(number, 1);
        App.notifyInfo("Xóa thành công");
        if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
            $scope.$apply();
        }
    }
    $scope.submit = function () {
        var fd = new FormData();
        $scope.lstTitlefile = [];
        if (uploader.queue.length > 0) {
            for (i = 0; i < uploader.queue.length; i++) {
                var files = uploader.queue[i]._file;
                var titleName = "";
                if (uploader.queue[i].file.FileName === "" || uploader.queue[i].file.FileName === null || uploader.queue[i].file.FileName === undefined) {
                    titleName = uploader.queue[i].file.name.split('.')[0];
                } else {
                    titleName = uploader.queue[i].file.FileName;
                }
                $scope.lstTitlefile.push({ STT: i, Title: titleName });
                fd.append("file", files);
            }
        }
        App.blockUI({
            target: ".modal-content",
            boxed: true,
            message: 'Đang tải...'
        });
        fd.append('model', JSON.stringify($scope.model));
        fd.append('lstTitlefile', JSON.stringify($scope.lstTitlefile));
        fd.append('DeleteAttach', JSON.stringify($scope.lstDeleteFile));
        $.ajax({
            type: "POST",
            url: "/ProductDeparments/UpdateFile",
            contentType: false,
            processData: false,
            data: fd,
            success: function (rs) {
                if (rs.Error === true) {
                    App.notifyDanger(rs.Title);
                    App.unblockUI(".modal-content");
                } else {
                    $scope.lstDeleteFile = [];
                    uploader.queue = [];
                    App.notifyInfo(rs.Title);
                    $scope.getattachment();
                    App.unblockUI(".modal-content");
                }
            },
            error: function (rs) {
                App.notifyDanger(rs.Title);
            }
        })
    }
});

app.controller('Buoc2MuaNguyenlieuVT', function ($scope, $rootScope, $uibModalInstance, FileUploader, dataservice, para) {
    $scope.model = { Temps: "" };
    $scope.disColumn = false;
    $scope.check = false;
    $scope.fullscreen = function (Check) {
        if (Check == false) {
            $scope.check = true;
        } else {
            $scope.check = false;
        }
    }

    $scope.ListDetailDelete = [];
    $scope.ListDetailDelete1 = [];
    $scope.ListDetailDelete2 = [];
    $scope.ListDetailDelete3 = [];
    $scope.deleteItem = function (element, row) {
        $scope.ListData = $scope.jxcelAddDetail.jexcel('getData');
        if ($scope.ListData.length > 1) {
            ESConfirm([{ 'value': 1, 'title': 'Xác nhận', 'type': 'info' }, { 'value': 2, 'title': 'Đóng', 'type': 'danger' }], { 'description': 'dữ liệu này không ?', 'class': 'eswarning' }, function (rs) {
                if (rs === '1') {
                    var cell = 'A' + (parseInt(row) + 1);
                    var id = $scope.jxcelAddDetail.jexcel('getValue', cell);
                    if (id !== "" && id !== null) {
                        var sta = false;
                        for (var i = 0; i < $scope.ListDetailDelete1.length; i++) {
                            if ($scope.ListDetailDelete1[i] === id) {
                                sta = true;
                            }
                        }
                        if (!sta) {
                            $scope.ListDetailDelete1.push(id);
                        }
                    }
                    $scope.jxcelAddDetail.jexcel('deleteRow', row);
                    $scope.onload1();

                    App.notifyInfo('Xóa thành công');
                }
                if (rs === '2') {
                    console.log('Không chấp nhận');
                }
            }, function () {
                //some error in this function
                console.log('Không lựa chọn');
            });
        }
        else {
            App.notifyDanger('Không xóa được dòng cuối cùng này');
            return;
        }
    };


    $scope.rowchange = function (obj, row, cell, columnName) {
        var rowindex = parseInt(row + 1); // dòng muốn thay đổi      
        $scope.ListData = $scope.jxcelAddDetail.jexcel('getData');
        if (columnName === "ItemID") {
            dataservice.getItem_by_ItemId({ IdS: [$scope.ListData[row].ItemID] }, function (rs) {
                if (rs.Error) {
                    App.notifyDanger(rs.Title);
                } else {
                    $scope.jxcelAddDetail.jexcel("setValueByName", "ItemName", row, rs.ItemName);
                    $scope.jxcelAddDetail.jexcel("setValueByName", "UnitID", row, rs.UnitID);
                    $scope.jxcelAddDetail.jexcel("setValueByName", "Quantity", row, rs.QuantityExist);
                    $scope.jxcelAddDetail.jexcel("setValueByName", "UnitName", row, rs.UnitName);

                    //dataservice.getUnit_by_ItemId({ IdS: [rs.UnitID] }, function (rs) {
                    //    if (rs.Error) {
                    //        App.notifyDanger(rs.Title);
                    //    } else {

                    //        $scope.jxcelAddDetail.jexcel("setValueByName", "UnitName", row, rs.UnitName);
                    //        if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                    //            $scope.$apply();
                    //        }
                    //    }
                    //});
                    if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                        $scope.$apply();
                    }
                }
            })
        }
        if (columnName === "UnitID") {
            if ($scope.ListData[row].UnitID != "") {
                dataservice.getUnit_by_ItemId({ IdS: [$scope.ListData[row].UnitID] }, function (rs) {
                    if (rs.Error) {
                        App.notifyDanger(rs.Title);
                    } else {
                        $scope.jxcelAddDetail.jexcel("setValueByName", "UnitName", row, rs.UnitName);
                        if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                            $scope.$apply();
                        }
                    }
                });
            } else {
                $scope.jxcelAddDetail.jexcel("setValueByName", "UnitName", row, "");
                if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                    $scope.$apply();
                }
            }


        }
        if (columnName === "Quantity" || columnName === "UnitPrice") {
            var Quantity = parseFloat($scope.ListData[row].Quantity);
            var UnitPrice = parseFloat($scope.ListData[row].UnitPrice);
            var AmountOc = Quantity * UnitPrice;
            $scope.jxcelAddDetail.jexcel("setValueByName", "AmountOc", row, AmountOc);
        }

        $scope.onload1();
    };
    $scope.roundNumber = function (number) {
        if (number > 0) {
            return parseFloat(number);
        } else return 0;

    }
    $scope.onload1 = function () {
        if ($('#myht').jexcel('getData') !== undefined) {
            $rootScope.Quantity = 0;
            $rootScope.AmountOc = 0;
            $rootScope.UnitPrice = 0;
            $rootScope.VATAmountOC = 0;
            $scope.ListData = $('#myht').jexcel('getData');
            angular.forEach($scope.ListData, function (val, key) {
                if (val.Quantity !== null && val.Quantity !== "" && val.Quantity !== undefined) {
                    $rootScope.Quantity += $scope.roundNumber(parseFloat(val.Quantity));
                }
                if (val.UnitPrice !== null && val.UnitPrice !== "" && val.UnitPrice !== undefined) {
                    $rootScope.UnitPrice += $scope.roundNumber(parseFloat(val.UnitPrice));
                }
                if (val.AmountOc !== null && val.AmountOc !== "" && val.AmountOc !== undefined) {
                    $rootScope.AmountOc += $scope.roundNumber(parseFloat(val.AmountOc));
                }

            });
            $('#myht #colfoot-7').html($rootScope.addPeriod($scope.Quantity));
            $('#myht #colfoot-8').html($rootScope.addPeriod($scope.UnitPrice));
            $('#myht #colfoot-9').html($rootScope.addPeriod($scope.AmountOc));
            if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                $scope.$apply();
            }
        }
    }

    $scope.dropdownItemId = function (d1, d2, d3) {
        var _tem1 = d1.ItemID;
        if (_tem1 === "" || _tem1 === null) {
            _tem1 = "";
        }
        var obj = {
            Keyword: _tem1
        }
        dataservice.getItemALL(obj, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {

                d3.source = rs;
                d2(rs, "");
            }
        })
    }
    $scope.dropdownUnitID = function (d1, d2, d3) {
        var _tem1 = d1.UnitID;
        if (_tem1 === "" || _tem1 === null) {
            _tem1 = "";
        }
        var obj = {
            Keyword: _tem1
        }
        dataservice.getUnitJexcel(obj, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {

                d3.source = rs;
                d2(rs, "");
            }
        })
    }

    $scope.jxcelAddDetailInit = {
        dataObject: true,
        reloadData: {},
        data: [],
        colHeaders: ['', 'Mã sản phẩm chọn', 'Mã sản phẩm', 'Tên sản phẩm', 'Mã đơn vị tính', 'Tên đơn vị tính', 'Số lượng nhập', 'Số lượng dùng', 'Đơn giá', 'Thành tiền', 'Người thực hiện', 'Ghi chú'
        ],
        colTitles: ['', 'Mã sản phẩm chọn', 'Mã sản phẩm', 'Tên sản phẩm', 'Mã đơn vị tính', 'Tên đơn vị tính', 'Số lượng nhập', 'Số lượng dùng', 'Đơn giá', 'Thành tiền', 'Người thực hiện', 'Ghi chú'
        ],
        colFooters: ['', '', '', '', '', '', '', '', '', '', '', ''
        ],
        columns: [
            {
                name: 'RowGuid',
                type: 'text'
            },
            {
                name: 'ItemID',
                type: 'autocomplete',
                source: $rootScope.listItemId,
                sourceReload: $scope.dropdownItemId,
                searchReload: true,
                onlyCode: true
            },
            {
                name: 'ItemID1',
                type: 'text'
            },
            {
                name: 'ItemName',
                type: 'text'
            },
            {
                name: 'UnitID',
                type: 'autocomplete',
                source: $rootScope.litsUnitID,
                sourceReload: $scope.dropdownUnitID,
                searchReload: true,
                onlyCode: true
            },
            {
                name: 'UnitName',
                type: 'text'
            },
            {
                name: 'QuantityTotal',//Xếp hạng 15
                type: 'numeric', formatNumber: 'VN-NUMERIC'

            },
            {
                name: 'Quantity',//Xếp hạng 3
                type: 'numeric', formatNumber: 'VN-NUMERIC'
            },
            {
                name: 'UnitPrice',//Xếp hạng 15
                type: 'numeric', formatNumber: 'VN-NUMERIC'
            },
            {
                name: 'AmountOc',//Xếp hạng 15
                type: 'numeric', formatNumber: 'VN-NUMERIC'
            },
            {
                name: 'EmployeeName',
                type: 'text'
            },
            {
                name: 'Note',
                type: 'text'
            }
        ],

        columnShow: [false, true, true, true, true, true, true, true, true, true, true, true],
        initDataRows: ['', '', '', '', '', '', '', '', '', '', '', ''],
        colWidths: [50, 150, 200, 200, 150, 150, 150, 150, 100, 150, 150, 300],
        allrowevent: true,
        deleterowconfirm: $scope.deleteItem,
        tableOverflow: true,
        csvHeaders: true,
        changeCell: $scope.rowchange,
        tableHeight: '200px',
        onload: $scope.onload1
    };


    if (para != "") {
        $scope.Title = "BƯỚC 2: MUA NGUYÊN LIỆU VẬT TƯ";
        $scope.model.Temps = "2";
        $scope.disColumn = true;
        $scope.initData = function () {
            dataservice.getById(para, function (rs) {
                if (rs.Error) {
                    App.notifyDanger(rs.Title);
                } else {
                    $scope.model = JSON.parse(rs.Table1)[0];
                    $scope.model.DeliveryDate = $scope.model.DeliveryDate !== null ? "/Date(" + new Date($scope.model.DeliveryDate).getTime() + ")/" : null;
                    $scope.lstDetail = JSON.parse(rs.Table3);
                    var ApplyOutsideDetails = [];
                    angular.forEach($scope.lstDetail, function (value, key) {
                        ApplyOutsideDetails.push([
                            value.RowGuid,
                            value.ItemID,
                            "",
                            value.ItemName,
                            value.UnitID,
                            value.UnitName,
                            0,
                            value.Quantity,
                            value.UnitPrice,
                            value.AmountOc,
                            value.EmployeeName,
                            value.Note
                        ]);
                    });
                    if ($scope.lstDetail == null) {
                        ApplyOutsideDetails.push([
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            0,
                            0,
                            0,
                            0,
                            "",
                            ""]);
                    }
                    $scope.jxcelAddDetailInit.data = ApplyOutsideDetails;
                    $scope.onload1();
                    if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                        $scope.$apply();
                    }
                }
            })
        }
        $scope.initData();
    } else {
        $scope.Title = "BƯỚC 2: MUA NGUYÊN LIỆU VẬT TƯ";
        $scope.model.Temps = "1";
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    //Cấu hình file đính kèm
    $scope.model.FileAttachments = [];
    var uploader = $scope.uploader = new FileUploader({
        // url: App.baseService() + '/upload'
    });
    uploader.filters.push({
        name: 'customFilter',
        fn: function (item /*{File|FileLikeObject}*/, options) {
            return this.queue.length < 10;
        }
    });
    uploader.onSuccessItem = function (fileItem, response, status, headers) {
        $scope.model.FileAttachments.push(response);
    };
    uploader.onCompleteAll = function () {
        $scope.commit();
    };
    $scope.ChangeProductionManagerGuid = function (data) {
        $scope.model.ProductionManagerGuid = data;
    }
    $scope.insert = function () {
        var fd = new FormData();
        $scope.lstTitlefile = [];
        if (uploader.queue.length > 0) {
            for (i = 0; i < uploader.queue.length; i++) {
                var files = uploader.queue[i]._file;
                if (files.size > 30720000) {
                    App.notifyDanger("Yêu cầu file đính kèm không vượt quá 3MB");
                    return;
                }
                fd.append("file", files);
            }
        }
        var datatab1 = $scope.jxcelAddDetail.jexcel('getData');
        var ApplyOutsideDetails = []; var ApplyOutsideDetails1 = []; var ApplyOutsideDetails2 = [];
        var ApplyOutsideDetails3 = [];
        for (var i = 0; i < datatab1.length; i++) {
            var value = datatab1[i];
            if (value.RowGuid != null && value.RowGuid != undefined && value.RowGuid != "") {
                ApplyOutsideDetails1.push({
                    RowGuid: value.RowGuid,
                    ItemID: value.ItemID,
                    ItemID1: value.ItemID1,
                    ItemName: value.ItemName,
                    UnitID: value.UnitID,
                    UnitName: value.UnitName,
                    QuantityTotal: value.QuantityTotal,
                    Quantity: value.Quantity,
                    UnitPrice: value.UnitPrice,
                    AmountOc: value.AmountOc,
                    EmployeeName: value.EmployeeName,
                    Note: value.Note,
                    SortOrder: i
                });

            }
            else {
                ApplyOutsideDetails1.push({
                    ItemID: value.ItemID,
                    ItemID1: value.ItemID1,
                    ItemName: value.ItemName,
                    UnitID: value.UnitID,
                    UnitName: value.UnitName,
                    QuantityTotal: value.QuantityTotal,
                    Quantity: value.Quantity,
                    UnitPrice: value.UnitPrice,
                    AmountOc: value.AmountOc,
                    EmployeeName: value.EmployeeName,
                    Note: value.Note,
                    SortOrder: i
                });
            }

        };
        if (ApplyOutsideDetails1.length == 0) {
            App.notifyDanger("Yêu cầu nhập chi tiết");
            return;
        }

        fd.append('submit', JSON.stringify($scope.model));
        fd.append('detail', JSON.stringify(ApplyOutsideDetails));
        fd.append('detailDelete', JSON.stringify($scope.ListDetailDelete));

        fd.append('detail1', JSON.stringify(ApplyOutsideDetails1));
        fd.append('detailDelete1', JSON.stringify($scope.ListDetailDelete1));

        fd.append('detail2', JSON.stringify(ApplyOutsideDetails2));
        fd.append('detailDelete2', JSON.stringify($scope.ListDetailDelete2));

        fd.append('detail3', JSON.stringify(ApplyOutsideDetails3));
        fd.append('detailDelete3', JSON.stringify($scope.ListDetailDelete3));
        $.ajax({
            type: "POST",
            url: "/ProductDeparments/Submit",
            contentType: false,
            processData: false,
            data: fd,
            success: function (rs) {
                if (rs.Error) {
                    App.notifyDanger(rs.Title);
                } else {
                    App.notifyInfo(rs.Title);
                    $uibModalInstance.close();
                    $rootScope.LoadItemALL();
                    $rootScope.reload();
                }
            }
        });

    };
});
app.controller('Buoc3LOGISTICS', function ($scope, $rootScope, $uibModalInstance, FileUploader, dataservice, para) {
    $scope.model = { Temps: "" };
    $scope.disColumn = false;

    $scope.ListDetailDelete = [];
    $scope.ListDetailDelete1 = [];
    $scope.ListDetailDelete2 = [];
    $scope.ListDetailDelete3 = [];
    $scope.deleteItem = function (element, row) {
        $scope.ListData = $scope.jxcelAddDetail.jexcel('getData');
        if ($scope.ListData.length > 1) {
            ESConfirm([{ 'value': 1, 'title': 'Xác nhận', 'type': 'info' }, { 'value': 2, 'title': 'Đóng', 'type': 'danger' }], { 'description': 'dữ liệu này không ?', 'class': 'eswarning' }, function (rs) {
                if (rs === '1') {
                    var cell = 'A' + (parseInt(row) + 1);
                    var id = $scope.jxcelAddDetail.jexcel('getValue', cell);
                    if (id !== "" && id !== null) {
                        var sta = false;
                        for (var i = 0; i < $scope.ListDetailDelete2.length; i++) {
                            if ($scope.ListDetailDelete2[i] === id) {
                                sta = true;
                            }
                        }
                        if (!sta) {
                            $scope.ListDetailDelete2.push(id);
                        }
                    }
                    $scope.jxcelAddDetail.jexcel('deleteRow', row);
                    $scope.onload1();

                    App.notifyInfo('Xóa thành công');
                }
                if (rs === '2') {
                    console.log('Không chấp nhận');
                }
            }, function () {
                //some error in this function
                console.log('Không lựa chọn');
            });
        }
        else {
            App.notifyDanger('Không xóa được dòng cuối cùng này');
            return;
        }
    };


    $scope.rowchange = function (obj, row, cell, columnName) {
        var rowindex = parseInt(row + 1); // dòng muốn thay đổi      
        $scope.ListData = $scope.jxcelAddDetail.jexcel('getData');
        if (columnName === "ItemID") {
            dataservice.getItem_by_ItemId({ IdS: [$scope.ListData[row].ItemID] }, function (rs) {
                if (rs.Error) {
                    App.notifyDanger(rs.Title);
                } else {
                    $scope.jxcelAddDetail.jexcel("setValueByName", "ItemName", row, rs.ItemName);
                    $scope.jxcelAddDetail.jexcel("setValueByName", "UnitID", row, rs.UnitID);
                    $scope.jxcelAddDetail.jexcel("setValueByName", "Quantity", row, rs.QuantityExist);
                    $scope.jxcelAddDetail.jexcel("setValueByName", "UnitName", row, rs.UnitName);


                    if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                        $scope.$apply();
                    }
                }
            })
        }
        if (columnName === "UnitID") {
            if ($scope.ListData[row].UnitID != "") {
                dataservice.getUnit_by_ItemId({ IdS: [$scope.ListData[row].UnitID] }, function (rs) {
                    if (rs.Error) {
                        App.notifyDanger(rs.Title);
                    } else {
                        $scope.jxcelAddDetail.jexcel("setValueByName", "UnitName", row, rs.UnitName);
                        if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                            $scope.$apply();
                        }
                    }
                });
            } else {
                $scope.jxcelAddDetail.jexcel("setValueByName", "UnitName", row, "");
                if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                    $scope.$apply();
                }
            }
        }
        if (columnName === "Quantity" || columnName === "UnitPrice") {
            var Quantity = parseFloat($scope.ListData[row].Quantity);
            var UnitPrice = parseFloat($scope.ListData[row].UnitPrice);
            var AmountOc = Quantity * UnitPrice;
            $scope.jxcelAddDetail.jexcel("setValueByName", "AmountOc", row, AmountOc);
        }

        $scope.onload1();
    };
    $scope.roundNumber = function (number) {
        if (number > 0) {
            return parseFloat(number);
        } else return 0;

    }
    $scope.onload1 = function () {
        if ($('#myht').jexcel('getData') !== undefined) {
            $rootScope.Quantity = 0;
            $rootScope.AmountOc = 0;
            $rootScope.UnitPrice = 0;
            $rootScope.VATAmountOC = 0;
            $scope.ListData = $('#myht').jexcel('getData');
            angular.forEach($scope.ListData, function (val, key) {
                if (val.Quantity !== null && val.Quantity !== "" && val.Quantity !== undefined) {
                    $rootScope.Quantity += $scope.roundNumber(parseFloat(val.Quantity));
                }
                if (val.UnitPrice !== null && val.UnitPrice !== "" && val.UnitPrice !== undefined) {
                    $rootScope.UnitPrice += $scope.roundNumber(parseFloat(val.UnitPrice));
                }
                if (val.AmountOc !== null && val.AmountOc !== "" && val.AmountOc !== undefined) {
                    $rootScope.AmountOc += $scope.roundNumber(parseFloat(val.AmountOc));
                }

            });
            $('#myht #colfoot-9').html($rootScope.addPeriod($scope.Quantity));
            $('#myht #colfoot-10').html($rootScope.addPeriod($scope.UnitPrice));
            $('#myht #colfoot-11').html($rootScope.addPeriod($scope.AmountOc));
            if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                $scope.$apply();
            }
        }
    }

    $scope.dropdownItemId = function (d1, d2, d3) {
        var _tem1 = d1.ItemID;
        if (_tem1 === "" || _tem1 === null) {
            _tem1 = "";
        }
        var obj = {
            Keyword: _tem1
        }
        dataservice.getItemALL(obj, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {

                d3.source = rs;
                d2(rs, "");
            }
        })
    }
    $scope.dropdownUnitID = function (d1, d2, d3) {
        var _tem1 = d1.UnitID;
        if (_tem1 === "" || _tem1 === null) {
            _tem1 = "";
        }
        var obj = {
            Keyword: _tem1
        }
        dataservice.getUnitJexcel(obj, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {

                d3.source = rs;
                d2(rs, "");
            }
        })
    }

    $scope.jxcelAddDetailInit = {
        dataObject: true,
        reloadData: {},
        data: [],
        colHeaders: ['', 'Mã sản phẩm chọn', 'Mã sản phẩm', 'Tên sản phẩm', 'ETD/ETA', 'Booking number', 'Mã đơn vị tính', 'Tên đơn vị tính', 'Số lượng nhập', 'Số lượng dùng', 'Đơn giá', 'Thành tiền', 'Người thực hiện', 'Ghi chú'
        ],
        colTitles: ['', 'Mã sản phẩm chọn', 'Mã sản phẩm', 'Tên sản phẩm', 'ETD/ETA', 'Booking number', 'Mã đơn vị tính', 'Tên đơn vị tính', 'Số lượng nhập', 'Số lượng dùng', 'Đơn giá', 'Thành tiền', 'Người thực hiện', 'Ghi chú'
        ],
        colFooters: ['', '', '', '', '', '', '', '', '', '', '', '', '', ''
        ],
        columns: [
            {
                name: 'RowGuid',
                type: 'text'
            },
            {
                name: 'ItemID',
                type: 'autocomplete',
                source: $rootScope.listItemId,
                sourceReload: $scope.dropdownItemId,
                searchReload: true,
                onlyCode: true
            },
            {
                name: 'ItemID1',
                type: 'text'
            },
            {
                name: 'ItemName',
                type: 'text'
            },
            {
                name: 'ETD',
                type: 'text'
            },
            {
                name: 'BookingNumber',
                type: 'text'
            },
            {
                name: 'UnitID',
                type: 'autocomplete',
                source: $rootScope.litsUnitID,
                sourceReload: $scope.dropdownUnitID,
                searchReload: true,
                onlyCode: true
            },
            {
                name: 'UnitName',
                type: 'text'
            },
            {
                name: 'QuantityTotal',//Xếp hạng 15
                type: 'numeric', formatNumber: 'VN-NUMERIC'

            },
            {
                name: 'Quantity',//Xếp hạng 3
                type: 'numeric', formatNumber: 'VN-NUMERIC'
            },
            {
                name: 'UnitPrice',//Xếp hạng 15
                type: 'numeric', formatNumber: 'VN-NUMERIC'
            },
            {
                name: 'AmountOc',//Xếp hạng 15
                type: 'numeric', formatNumber: 'VN-NUMERIC'
            },
            {
                name: 'EmployeeName',
                type: 'text'
            },
            {
                name: 'Note',
                type: 'text'
            }
        ],

        columnShow: [false, true, true, true, true, true, true, true, true, true, true, true, true, true],
        initDataRows: ['', '', '', '', '', '', '', '', '', '', '', ''],
        colWidths: [50, 150, 200, 200, 150, 150, 150, 150, 150, 150, 100, 150, 150, 300],
        allrowevent: true,
        deleterowconfirm: $scope.deleteItem,
        tableOverflow: true,
        csvHeaders: true,
        changeCell: $scope.rowchange,
        tableHeight: '200px',
        onload: $scope.onload1
    };


    if (para != "") {
        $scope.Title = "BƯỚC 3: LOGISTICS-CHỨNG TỪ";
        $scope.model.Temps = "2";
        $scope.disColumn = true;
        $scope.initData = function () {
            dataservice.getById(para, function (rs) {
                if (rs.Error) {
                    App.notifyDanger(rs.Title);
                } else {
                    $scope.model = JSON.parse(rs.Table1)[0];
                    $scope.model.DeliveryDate = $scope.model.DeliveryDate !== null ? "/Date(" + new Date($scope.model.DeliveryDate).getTime() + ")/" : null;
                    $scope.lstDetail = JSON.parse(rs.Table4);
                    var ApplyOutsideDetails = [];
                    angular.forEach($scope.lstDetail, function (value, key) {
                        ApplyOutsideDetails.push([
                            value.RowGuid,
                            value.ItemID,
                            "",
                            value.ItemName,
                            value.ETD,
                            value.BookingNumber,
                            value.UnitID,
                            value.UnitName,
                            0,
                            value.Quantity,
                            value.UnitPrice,
                            value.AmountOc,
                            value.EmployeeName,
                            value.Note
                        ]);
                    });
                    if ($scope.lstDetail == null) {
                        ApplyOutsideDetails.push([
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            0,
                            0,
                            0,
                            0,
                            "",
                            ""]);
                    }
                    $scope.jxcelAddDetailInit.data = ApplyOutsideDetails;
                    $scope.onload1();
                    if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                        $scope.$apply();
                    }
                }
            })
        }
        $scope.initData();
    } else {
        $scope.Title = "BƯỚC 3: LOGISTICS-CHỨNG TỪ";
        $scope.model.Temps = "1";
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    //Cấu hình file đính kèm
    $scope.model.FileAttachments = [];
    var uploader = $scope.uploader = new FileUploader({
        // url: App.baseService() + '/upload'
    });
    uploader.filters.push({
        name: 'customFilter',
        fn: function (item /*{File|FileLikeObject}*/, options) {
            return this.queue.length < 10;
        }
    });
    uploader.onSuccessItem = function (fileItem, response, status, headers) {
        $scope.model.FileAttachments.push(response);
    };
    uploader.onCompleteAll = function () {
        $scope.commit();
    };
    $scope.ChangeProductionManagerGuid = function (data) {
        $scope.model.ProductionManagerGuid = data;
    }
    $scope.insert = function () {
        var fd = new FormData();
        $scope.lstTitlefile = [];
        if (uploader.queue.length > 0) {
            for (i = 0; i < uploader.queue.length; i++) {
                var files = uploader.queue[i]._file;
                if (files.size > 30720000) {
                    App.notifyDanger("Yêu cầu file đính kèm không vượt quá 3MB");
                    return;
                }
                fd.append("file", files);
            }
        }
        var datatab1 = $scope.jxcelAddDetail.jexcel('getData');
        var ApplyOutsideDetails = []; var ApplyOutsideDetails1 = []; var ApplyOutsideDetails2 = [];
        var ApplyOutsideDetails3 = [];
        for (var i = 0; i < datatab1.length; i++) {
            var value = datatab1[i];
            if (value.RowGuid != null && value.RowGuid != undefined && value.RowGuid != "") {
                ApplyOutsideDetails2.push({
                    RowGuid: value.RowGuid,
                    ItemID: value.ItemID,
                    ItemID1: value.ItemID1,
                    ItemName: value.ItemName,
                    ETD: value.ETD,
                    BookingNumber: value.BookingNumber,
                    UnitID: value.UnitID,
                    UnitName: value.UnitName,
                    QuantityTotal: value.QuantityTotal,
                    Quantity: value.Quantity,
                    UnitPrice: value.UnitPrice,
                    AmountOc: value.AmountOc,
                    EmployeeName: value.EmployeeName,
                    Note: value.Note,
                    SortOrder: i
                });

            }
            else {
                ApplyOutsideDetails2.push({
                    ItemID: value.ItemID,
                    ItemID1: value.ItemID1,
                    ItemName: value.ItemName,
                    ETD: value.ETD,
                    BookingNumber: value.BookingNumber,
                    UnitID: value.UnitID,
                    UnitName: value.UnitName,
                    QuantityTotal: value.QuantityTotal,
                    Quantity: value.Quantity,
                    UnitPrice: value.UnitPrice,
                    AmountOc: value.AmountOc,
                    EmployeeName: value.EmployeeName,
                    Note: value.Note,
                    SortOrder: i
                });
            }

        };
        if (ApplyOutsideDetails2.length == 0) {
            App.notifyDanger("Yêu cầu nhập chi tiết");
            return;
        }

        fd.append('submit', JSON.stringify($scope.model));
        fd.append('detail', JSON.stringify(ApplyOutsideDetails));
        fd.append('detailDelete', JSON.stringify($scope.ListDetailDelete));

        fd.append('detail1', JSON.stringify(ApplyOutsideDetails1));
        fd.append('detailDelete1', JSON.stringify($scope.ListDetailDelete1));

        fd.append('detail2', JSON.stringify(ApplyOutsideDetails2));
        fd.append('detailDelete2', JSON.stringify($scope.ListDetailDelete2));

        fd.append('detail3', JSON.stringify(ApplyOutsideDetails3));
        fd.append('detailDelete3', JSON.stringify($scope.ListDetailDelete3));
        $.ajax({
            type: "POST",
            url: "/ProductDeparments/Submit",
            contentType: false,
            processData: false,
            data: fd,
            success: function (rs) {
                if (rs.Error) {
                    App.notifyDanger(rs.Title);
                } else {
                    App.notifyInfo(rs.Title);
                    $uibModalInstance.close();
                    $rootScope.LoadItemALL();
                    $rootScope.reload();
                }
            }
        });

    };
});
app.controller('Buoc4KSC', function ($scope, $rootScope, $uibModalInstance, FileUploader, dataservice, para) {
    $scope.model = { Temps: "" };
    $scope.disColumn = false;
    $scope.check = false;
    $scope.fullscreen = function (Check) {
        if (Check == false) {
            $scope.check = true;
        } else {
            $scope.check = false;
        }
    }
    $scope.fullscreen($scope.check)
    $scope.ListDetailDelete = [];
    $scope.ListDetailDelete1 = [];
    $scope.ListDetailDelete2 = [];
    $scope.ListDetailDelete3 = [];
    $scope.deleteItem = function (element, row) {
        $scope.ListData = $scope.jxcelAddDetail.jexcel('getData');
        if ($scope.ListData.length > 1) {
            ESConfirm([{ 'value': 1, 'title': 'Xác nhận', 'type': 'info' }, { 'value': 2, 'title': 'Đóng', 'type': 'danger' }], { 'description': 'dữ liệu này không ?', 'class': 'eswarning' }, function (rs) {
                if (rs === '1') {
                    var cell = 'A' + (parseInt(row) + 1);
                    var id = $scope.jxcelAddDetail.jexcel('getValue', cell);
                    if (id !== "" && id !== null) {
                        var sta = false;
                        for (var i = 0; i < $scope.ListDetailDelete3.length; i++) {
                            if ($scope.ListDetailDelete3[i] === id) {
                                sta = true;
                            }
                        }
                        if (!sta) {
                            $scope.ListDetailDelete3.push(id);
                        }
                    }
                    $scope.jxcelAddDetail.jexcel('deleteRow', row);
                    $scope.onload1();

                    App.notifyInfo('Xóa thành công');
                }
                if (rs === '2') {
                    console.log('Không chấp nhận');
                }
            }, function () {
                //some error in this function
                console.log('Không lựa chọn');
            });
        }
        else {
            App.notifyDanger('Không xóa được dòng cuối cùng này');
            return;
        }
    };

    $scope.OrderStatusChange = function (data) {
        $scope.model.OrderStatus = data !== "null" ? data : "";
    }
    $scope.rowchange = function (obj, row, cell, columnName) {
        var rowindex = parseInt(row + 1); // dòng muốn thay đổi      
        $scope.ListData = $scope.jxcelAddDetail.jexcel('getData');
        if (columnName === "ItemID") {
            dataservice.getItem_by_ItemId({ IdS: [$scope.ListData[row].ItemID] }, function (rs) {
                if (rs.Error) {
                    App.notifyDanger(rs.Title);
                } else {
                    $scope.jxcelAddDetail.jexcel("setValueByName", "ItemName", row, rs.ItemName);
                    $scope.jxcelAddDetail.jexcel("setValueByName", "UnitID", row, rs.UnitID);
                    $scope.jxcelAddDetail.jexcel("setValueByName", "Quantity", row, rs.QuantityExist);
                    $scope.jxcelAddDetail.jexcel("setValueByName", "UnitName", row, rs.UnitName);

                    $scope.onload1();
                    if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                        $scope.$apply();
                    }
                }
            })
        }
        if (columnName === "UnitID") {
            if ($scope.ListData[row].UnitID != "") {
                dataservice.getUnit_by_ItemId({ IdS: [$scope.ListData[row].UnitID] }, function (rs) {
                    if (rs.Error) {
                        App.notifyDanger(rs.Title);
                    } else {
                        $scope.jxcelAddDetail.jexcel("setValueByName", "UnitName", row, rs.UnitName);
                        if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                            $scope.$apply();
                        }
                    }
                });
            } else {
                $scope.jxcelAddDetail.jexcel("setValueByName", "UnitName", row, "");
                if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                    $scope.$apply();
                }
            }
        }
        //if (columnName === "Quantity" || columnName === "UnitPrice") {
        //    var Quantity = parseFloat($scope.ListData[row].Quantity);
        //    var UnitPrice = parseFloat($scope.ListData[row].UnitPrice);
        //    var AmountOc = Quantity * UnitPrice;
        //    $scope.jxcelAddDetail.jexcel("setValueByName", "AmountOc", row, AmountOc);
        //}
        $scope.onload1();

    };
    $scope.roundNumber = function (number) {
        if (number > 0) {
            return parseFloat(number);
        } else return 0;

    }
    $scope.onload1 = function () {
        if ($('#myht').jexcel('getData') !== undefined) {
            $scope.Quantity = 0;
            $scope.ListData = $('#myht').jexcel('getData');
            angular.forEach($scope.ListData, function (val, key) {
                if (val.Quantity !== null && val.Quantity !== "" && val.Quantity !== undefined) {
                    $scope.Quantity += $scope.roundNumber(parseFloat(val.Quantity));
                }
            });
            $('#myht #colfoot-9').html($rootScope.addPeriod($scope.Quantity));
            if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                $scope.$apply();
            }
        }
    }

    $scope.dropdownItemId = function (d1, d2, d3) {
        var _tem1 = d1.ItemID;
        if (_tem1 === "" || _tem1 === null) {
            _tem1 = "";
        }
        var obj = {
            Keyword: _tem1
        }
        dataservice.getItemALL(obj, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {

                d3.source = rs;
                d2(rs, "");
            }
        })
    }
    $scope.dropdownUnitID = function (d1, d2, d3) {
        var _tem1 = d1.UnitID;
        if (_tem1 === "" || _tem1 === null) {
            _tem1 = "";
        }
        var obj = {
            Keyword: _tem1
        }
        dataservice.getUnitJexcel(obj, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {

                d3.source = rs;
                d2(rs, "");
            }
        })
    }

    $scope.jxcelAddDetailInit = {
        dataObject: true,
        reloadData: {},
        data: [],
        colHeaders: ['', 'Mã sản phẩm chọn', 'Mã sản phẩm', 'Tên sản phẩm', 'Quy cách đóng gói', 'Mã đơn vị tính', 'Tên đơn vị tính', 'Số lượng đã sx', 'Số lượng nhập', 'Số lượng dùng', 'TT chất lượng/đóng gói', 'Tình trạng bảo quản', 'Người thực hiện', 'Ghi chú'
        ],
        colTitles: ['', 'Mã sản phẩm chọn', 'Mã sản phẩm', 'Tên sản phẩm', 'Quy cách đóng gói', 'Mã đơn vị tính', 'Tên đơn vị tính', 'Số lượng đã sx', 'Số lượng nhập', 'Số lượng dùng', 'TT chất lượng/đóng gói', 'Tình trạng bảo quản', 'Người thực hiện', 'Ghi chú'
        ],
        colFooters: ['', '', '', '', '', '', '', '', '', '', '', '', '', ''
        ],
        columns: [
            {
                name: 'RowGuid',
                type: 'text'
            },
            {
                name: 'ItemID',
                type: 'autocomplete',
                source: $rootScope.listItemId,
                sourceReload: $scope.dropdownItemId,
                searchReload: true,
                onlyCode: true
            },
            {
                name: 'ItemID1',
                type: 'text'
            },
            {
                name: 'ItemName',
                type: 'text'
            },
            {
                name: 'Packing',
                type: 'text'
            },
            {
                name: 'UnitID',
                type: 'autocomplete',
                source: $rootScope.litsUnitID,
                sourceReload: $scope.dropdownUnitID,
                searchReload: true,
                onlyCode: true
            },
            {
                name: 'UnitName',
                type: 'text'
            },
            {
                name: 'QuantitySX',//Xếp hạng 15
                type: 'numeric', formatNumber: 'VN-NUMERIC'

            },
            {
                name: 'QuantityTotal',//Xếp hạng 15
                type: 'numeric', formatNumber: 'VN-NUMERIC'

            },
            {
                name: 'Quantity',//Xếp hạng 3
                type: 'numeric', formatNumber: 'VN-NUMERIC'
            },
            {
                name: 'QualityStatus',
                type: 'text'
            },
            {
                name: 'PreservationStatus',
                type: 'text'
            },
            {
                name: 'EmployeeName',
                type: 'text'
            },
            {
                name: 'Note',
                type: 'text'
            }
        ],
        columnShow: [false, true, true, true, true, true, true, true, true, true, true, true, true, true],
        initDataRows: ['', '', '', '', '', '', '', '', '', '', '', ''],
        colWidths: [50, 150, 200, 200, 150, 150, 150, 150, 150, 150, 100, 150, 150, 300],
        allrowevent: true,
        deleterowconfirm: $scope.deleteItem,
        tableOverflow: true,
        csvHeaders: true,
        changeCell: $scope.rowchange,
        tableHeight: '200px',
        onload: $scope.onload1
    };


    if (para != "") {
        $scope.Title = "BƯỚC 4: KSC-GIAO HÀNG";
        $scope.model.Temps = "2";
        $scope.disColumn = true;
        $scope.initData = function () {
            dataservice.getById(para, function (rs) {
                if (rs.Error) {
                    App.notifyDanger(rs.Title);
                } else {
                    $scope.model = JSON.parse(rs.Table1)[0];
                    $scope.model.DeliveryDate = $scope.model.DeliveryDate !== null ? "/Date(" + new Date($scope.model.DeliveryDate).getTime() + ")/" : null;
                    $scope.lstDetail = JSON.parse(rs.Table5);
                    $scope.lstDetail = JSON.parse(rs.Table5);
                    var ApplyOutsideDetails = [];
                    angular.forEach($scope.lstDetail, function (value, key) {
                        ApplyOutsideDetails.push([
                            value.RowGuid,
                            value.ItemID,
                            "",
                            value.ItemName,
                            value.Packing,
                            value.UnitID,
                            value.UnitName,
                            value.QuantitySX,
                            0,
                            value.Quantity,
                            value.QualityStatus,
                            value.PreservationStatus,
                            value.EmployeeName,
                            value.Note
                        ]);
                    });
                    if ($scope.lstDetail == null) {
                        ApplyOutsideDetails.push([
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            0,
                            "",
                            "",
                            "",
                            "",
                            ""]);
                    }
                    $scope.jxcelAddDetailInit.data = ApplyOutsideDetails;
                    $scope.onload1();
                    if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                        $scope.$apply();
                    }
                }
            })
        }
        $scope.initData();
    } else {
        $scope.Title = "BƯỚC 4: KSC- GIAO HÀNG";
        $scope.model.Temps = "1";
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    //Cấu hình file đính kèm
    $scope.model.FileAttachments = [];
    var uploader = $scope.uploader = new FileUploader({
        // url: App.baseService() + '/upload'
    });
    uploader.filters.push({
        name: 'customFilter',
        fn: function (item /*{File|FileLikeObject}*/, options) {
            return this.queue.length < 10;
        }
    });
    uploader.onSuccessItem = function (fileItem, response, status, headers) {
        $scope.model.FileAttachments.push(response);
    };
    uploader.onCompleteAll = function () {
        $scope.commit();
    };
    $scope.ChangeProductionManagerGuid = function (data) {
        $scope.model.ProductionManagerGuid = data;
    }
    $scope.insert = function () {
        var fd = new FormData();
        $scope.lstTitlefile = [];
        if (uploader.queue.length > 0) {
            for (i = 0; i < uploader.queue.length; i++) {
                var files = uploader.queue[i]._file;
                if (files.size > 30720000) {
                    App.notifyDanger("Yêu cầu file đính kèm không vượt quá 3MB");
                    return;
                }
                fd.append("file", files);
            }
        }
        var datatab1 = $scope.jxcelAddDetail.jexcel('getData');
        var ApplyOutsideDetails = []; var ApplyOutsideDetails1 = []; var ApplyOutsideDetails2 = [];
        var ApplyOutsideDetails3 = [];
        for (var i = 0; i < datatab1.length; i++) {
            var value = datatab1[i];
            if (value.RowGuid != null && value.RowGuid != undefined && value.RowGuid != "") {
                ApplyOutsideDetails3.push({
                    RowGuid: value.RowGuid,
                    ItemID: value.ItemID,
                    ItemID1: value.ItemID1,
                    ItemName: value.ItemName,
                    Packing: value.Packing,
                    UnitID: value.UnitID,
                    UnitName: value.UnitName,
                    QuantitySX: value.QuantitySX,
                    QuantityTotal: value.QuantityTotal,
                    Quantity: value.Quantity,
                    QualityStatus: value.QualityStatus,
                    PreservationStatus: value.PreservationStatus,
                    EmployeeName: value.EmployeeName,
                    Note: value.Note,
                    SortOrder: i
                });

            }
            else {
                ApplyOutsideDetails3.push({
                    ItemID: value.ItemID,
                    ItemID1: value.ItemID1,
                    ItemName: value.ItemName,
                    Packing: value.Packing,
                    UnitID: value.UnitID,
                    UnitName: value.UnitName,
                    QuantitySX: value.QuantitySX,
                    QuantityTotal: value.QuantityTotal,
                    Quantity: value.Quantity,
                    QualityStatus: value.QualityStatus,
                    PreservationStatus: value.PreservationStatus,
                    EmployeeName: value.EmployeeName,
                    Note: value.Note,
                    SortOrder: i
                });
            }

        };
        if (ApplyOutsideDetails3.length == 0) {
            App.notifyDanger("Yêu cầu nhập chi tiết");
            return;
        }

        fd.append('submit', JSON.stringify($scope.model));
        fd.append('detail', JSON.stringify(ApplyOutsideDetails));
        fd.append('detailDelete', JSON.stringify($scope.ListDetailDelete));

        fd.append('detail1', JSON.stringify(ApplyOutsideDetails1));
        fd.append('detailDelete1', JSON.stringify($scope.ListDetailDelete1));

        fd.append('detail2', JSON.stringify(ApplyOutsideDetails2));
        fd.append('detailDelete2', JSON.stringify($scope.ListDetailDelete2));

        fd.append('detail3', JSON.stringify(ApplyOutsideDetails3));
        fd.append('detailDelete3', JSON.stringify($scope.ListDetailDelete3));
        $.ajax({
            type: "POST",
            url: "/ProductDeparments/Submit",
            contentType: false,
            processData: false,
            data: fd,
            success: function (rs) {
                if (rs.Error) {
                    App.notifyDanger(rs.Title);
                } else {
                    App.notifyInfo(rs.Title);
                    $uibModalInstance.close();
                    $rootScope.LoadItemALL();
                    $rootScope.reload();
                }
            }
        });

    };
});
app.controller('PaymentOrders', function ($scope, $rootScope, $uibModalInstance, FileUploader, dataservice, para) {
    $scope.model = { Temps: "" };
    $scope.disColumn = false;
    $scope.check = false;
    $scope.fullscreen = function (Check) {
        if (Check == false) {
            $scope.check = true;
        } else {
            $scope.check = false;
        }
    }

    $scope.ListDetailDelete = [];
    $scope.ListDetailDelete1 = [];
    $scope.ListDetailDelete2 = [];
    $scope.ListDetailDelete3 = [];
    $scope.deleteItem = function (element, row) {
        $scope.ListData = $scope.jxcelAddDetail.jexcel('getData');
        if ($scope.ListData.length > 1) {
            ESConfirm([{ 'value': 1, 'title': 'Xác nhận', 'type': 'info' }, { 'value': 2, 'title': 'Đóng', 'type': 'danger' }], { 'description': 'dữ liệu này không ?', 'class': 'eswarning' }, function (rs) {
                if (rs === '1') {
                    var cell = 'A' + (parseInt(row) + 1);
                    var id = $scope.jxcelAddDetail.jexcel('getValue', cell);
                    if (id !== "" && id !== null) {
                        var sta = false;
                        for (var i = 0; i < $scope.ListDetailDelete1.length; i++) {
                            if ($scope.ListDetailDelete1[i] === id) {
                                sta = true;
                            }
                        }
                        if (!sta) {
                            $scope.ListDetailDelete1.push(id);
                        }
                    }
                    $scope.jxcelAddDetail.jexcel('deleteRow', row);
                    $scope.onload1();

                    App.notifyInfo('Xóa thành công');
                }
                if (rs === '2') {
                    console.log('Không chấp nhận');
                }
            }, function () {
                //some error in this function
                console.log('Không lựa chọn');
            });
        }
        else {
            App.notifyDanger('Không xóa được dòng cuối cùng này');
            return;
        }
    };


    $scope.rowchange = function (obj, row, cell, columnName) {
        var rowindex = parseInt(row + 1); // dòng muốn thay đổi      
        $scope.ListData = $scope.jxcelAddDetail.jexcel('getData');
      
        $scope.onload1();
    };
    $scope.roundNumber = function (number) {
        if (number > 0) {
            return parseFloat(number);
        } else return 0;

    }
    $scope.onload1 = function () {
        if ($('#myht').jexcel('getData') !== undefined) {
            $scope.AmountOc = 0;         
            $scope.ListData = $('#myht').jexcel('getData');
            angular.forEach($scope.ListData, function (val, key) {
                if (val.PaymentMoney !== null && val.PaymentMoney !== "" && val.PaymentMoney !== undefined) {
                    $scope.PaymentMoney += $scope.roundNumber(parseFloat(val.PaymentMoney));
                }              
            });
            $('#myht #colfoot-7').html($rootScope.addPeriod($scope.PaymentMoney));           
            if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                $scope.$apply();
            }
        }
    }

    $scope.dropdownItemId = function (d1, d2, d3) {
        var _tem1 = d1.ItemID;
        if (_tem1 === "" || _tem1 === null) {
            _tem1 = "";
        }
        var obj = {
            Keyword: _tem1
        }
        dataservice.getItemALL(obj, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {

                d3.source = rs;
                d2(rs, "");
            }
        })
    }
    $scope.dropdownUnitID = function (d1, d2, d3) {
        var _tem1 = d1.UnitID;
        if (_tem1 === "" || _tem1 === null) {
            _tem1 = "";
        }
        var obj = {
            Keyword: _tem1
        }
        dataservice.getUnitJexcel(obj, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {

                d3.source = rs;
                d2(rs, "");
            }
        })
    }

    $scope.jxcelAddDetailInit = {
        dataObject: true,
        reloadData: {},
        data: [['', '', 'VND', '', '', '', '', '', '', '', '', '']],
        colHeaders: ['', 'Ngày thanh toán', 'Loại tiền', 'Số tiền','% thanh toán', 'UNC', 'Ghi chú'
        ],
        colTitles: ['', 'Ngày thanh toán', 'Loại tiền', 'Số tiền', '% thanh toán', 'UNC', 'Ghi chú'
        ],
        colFooters: ['', '', '', '', '', ''],
        columns: [
            {
                name: 'RowGuid',
                type: 'text'
            },
            {
                name: 'PaymentDate',
                type: 'calendar', options: { format: 'DD/MM/YYYY' },
                readOnly: false,
                updateReadOnly: true
            },
            {
                name: 'MoneyType',
                type: 'autocomplete',
                url: '/Sale/ProductDeparments/MoneyType/',
                readOnly: false,
                updateReadOnly: true
            },
            {
                name: 'PaymentMoney',
                type: 'numeric', formatNumber: 'VN-NUMERIC'
            },
            {
                name: 'PaymentPercent',
                type: 'numeric', formatNumber: 'VN-NUMERIC'
            },
            {
                name: 'UNC',
                type: 'text'
            },           
            {
                name: 'Note',
                type: 'text'
            }
           
        ],

        columnShow: [false, true, true, true, true, true, true],
        initDataRows: ['', '', 'VND', '', '', '', '', '', '', '', '', ''],
        colWidths: [50, 150, 200, 200, 150, 300, 300],
        allrowevent: true,
        deleterowconfirm: $scope.deleteItem,
        tableOverflow: true,
        csvHeaders: true,
        changeCell: $scope.rowchange,
        tableHeight: '200px',
        onload: $scope.onload1
    };


    if (para != "") {
        $scope.Title = "Trình trạng thanh toán";
        $scope.model.Temps = "2";
        $scope.disColumn = true;
        $scope.initData = function () {
            dataservice.GetAllPaymentOrders({ RecordGuid: para }, function (rs) {
                if (rs.Error) {
                    App.notifyDanger(rs.Title);
                } else {                    
                    $scope.lstDetail = rs;
                    var ApplyOutsideDetails = [];
                    angular.forEach($scope.lstDetail, function (value, key) {
                        ApplyOutsideDetails.push([
                            value.RowGuid,
                            value.PaymentDate !== null ? moment(value.PaymentDate).format("YYYY-MM-DD") : null,
                            value.MoneyType,
                            value.PaymentMoney,
                            value.PaymentPercent,                          
                            value.UNC,
                            value.Note                           
                        ]);
                    });
                    if ($scope.lstDetail.length == 0) {
                        ApplyOutsideDetails.push([
                            "",
                            null,
                           0,
                           0,
                            "",
                            "",
                            ""]);
                    }
                    $scope.jxcelAddDetailInit.data = ApplyOutsideDetails;
                    $scope.onload1();
                    if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                        $scope.$apply();
                    }
                }
            })
        }
        $scope.initData();
    } else {
        $scope.Title = "Trình trạng thanh toán";
        $scope.model.Temps = "1";
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    //Cấu hình file đính kèm
    $scope.model.FileAttachments = [];
    var uploader = $scope.uploader = new FileUploader({
        // url: App.baseService() + '/upload'
    });
    uploader.filters.push({
        name: 'customFilter',
        fn: function (item /*{File|FileLikeObject}*/, options) {
            return this.queue.length < 10;
        }
    });
    uploader.onSuccessItem = function (fileItem, response, status, headers) {
        $scope.model.FileAttachments.push(response);
    };
    uploader.onCompleteAll = function () {
        $scope.commit();
    };
    $scope.ChangeProductionManagerGuid = function (data) {
        $scope.model.ProductionManagerGuid = data;
    }
    $scope.insert = function () {
        var fd = new FormData();
        $scope.lstTitlefile = [];
        if (uploader.queue.length > 0) {
            for (i = 0; i < uploader.queue.length; i++) {
                var files = uploader.queue[i]._file;
                if (files.size > 30720000) {
                    App.notifyDanger("Yêu cầu file đính kèm không vượt quá 3MB");
                    return;
                }
                fd.append("file", files);
            }
        }
        var datatab1 = $scope.jxcelAddDetail.jexcel('getData');
        var ApplyOutsideDetails = []; 
        for (var i = 0; i < datatab1.length; i++) {
            var value = datatab1[i];
            if (value.RowGuid != null && value.RowGuid != undefined && value.RowGuid != "") {
                ApplyOutsideDetails.push({
                    RowGuid: value.RowGuid,
                    RecordGuid: para,
                    PaymentDate: value.PaymentDate,
                    PaymentMoney: value.PaymentMoney,
                    PaymentPercent: value.PaymentPercent,
                    MoneyType: value.MoneyType,
                    UNC: value.UNC,
                    Note: value.Note,                   
                    SortOrder: i
                });

            }
            else {
                ApplyOutsideDetails.push({
                    RecordGuid: para,
                    PaymentDate: value.PaymentDate,
                    PaymentMoney: value.PaymentMoney,
                    PaymentPercent: value.PaymentPercent,
                    MoneyType: value.MoneyType,
                    UNC: value.UNC,
                    Note: value.Note,
                    SortOrder: i
                });
            }
        };
        if (ApplyOutsideDetails.length == 0) {
            App.notifyDanger("Yêu cầu nhập tình trạng thanh toán");
            return;
        }
        fd.append('detail', JSON.stringify(ApplyOutsideDetails));     
        $.ajax({
            type: "POST",
            url: "/ProductDeparments/Submit_PaymentOrders",
            contentType: false,
            processData: false,
            data: fd,
            success: function (rs) {
                if (rs.Error) {
                    App.notifyDanger(rs.Title);
                } else {
                    App.notifyInfo(rs.Title);
                    $scope.initData();
                }
            }
        });

    };
});




