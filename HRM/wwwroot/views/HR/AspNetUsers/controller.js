﻿var ctxfolder = "/Areas/Admin/Aroot/views/AspNetUsers";
app.factory('dataservice', function ($http) {
    var headers = {
        "Content-Type": "Countries/json;odata=verbose",
        "Accept": "Countries/json;odata=verbose",
    }
    return {
        insert: function (data, callback) {
            $http.post('/AspNetUsers/insert', data).success(callback);
        },
        update: function (data, callback) {
            $http.post('/AspNetUsers/update', data).success(callback);
        },
        deleteItems: function (data, callback) {
            $http.post('/AspNetUsers/IsDeletedItems', data).success(callback);
        },
        delete: function (data, callback) {
            $http.post('/AspNetUsers/IsDeleted/' + data).success(callback);
        },
        getItem: function (data, callback) {
            $http.post('/AspNetUsers/getitem/' + data).success(callback);
        },
        resort: function (data, callback) {
            $http.post('/Countries/resort', data).success(callback);
        },
        getAll: function (callback) {
            $http.post('/AspNetUsers/getAll/').success(callback);
        },
        getAllAspNetRoles: function (callback) {
            $http.post('/AspNetUsers/getAllAspNetRoles/').success(callback);
        }
    }
});
app.directive('format', ['$filter', function ($filter) {
    return {
        require: '?ngModel',
        link: function (scope, elem, attrs, ctrl) {
            if (!ctrl) return;


            ctrl.$formatters.unshift(function (a) {
                return $filter(attrs.format)(ctrl.$modelValue)
            });


            ctrl.$parsers.unshift(function (viewValue) {
                var plainNumber = viewValue.replace(/[^\d|\-+|\.+]/g, '');
                elem.val($filter(attrs.format)(plainNumber));
                return plainNumber;
            });
        }
    };
}]);
app.filter('fdate', [
    '$filter', function ($filter) {
        return function (input, f) {
            if (input && input.toString().indexOf('Date') > -1) {
                return moment(input).format(f);
            } if (input && input.toString().indexOf('T') > -1) {
                return moment(input).format(f);
            } else return input;
        };
    }
]);

app.filter('html', ['$sce', function ($sce) {
    return function (text) {
        return $sce.trustAsHtml(text);
    };
}]);
app.controller('Ctrl_ES_CSDL_AspNetUsers', function ($scope, $rootScope, $sce, $compile, $uibModal, dataservice) {
    $rootScope.Title = "";
    $rootScope.go = function (path) {
        $location.path(path); return false;
    };
    jQuery.validator.addMethod("alphanumeric", function (value, element) {
        return this.optional(element) || /^\w+$/i.test(value);
    }, "Letters, numbers, and underscores only please");
    jQuery.validator.addMethod("alphanumerics", function (value, element) {
        var data = /[(!@#$%^&*\)]+$/i.test(value);
        return this.optional(element) || data === false ? true : false;
    }, "Letters, numbers, and underscores only please");
    $rootScope.validationOptions = {
        rules: {
            Email: {
                required: true,
                maxlength: 255,
                email: true
            },
            
            Description: {
                maxlength: 250
            }
        },
        messages: {
            Email: {
                required: "Yêu cầu nhập Email.",
                maxlength: "Email không được vượt quá 255 ký tự.",
                email:"Email không đúng"
            },
           
            Description: {
                maxlength: "Mô tả không vượt quá 250 ký tự."
            }
        }
    }
    dataservice.getAllAspNetRoles(function (rs) {
        if (rs.length > 0) {
            $rootScope.listAspNetRoles = rs;
        }
    })
    $rootScope.EmailConfirmedData = [{
        Value: 1,
        Name: 'Xác nhận'
    }, {
        Value: 0,
        Name: 'Chưa xác nhận'
    }];
    $rootScope.CreateDate = function (date) {
        if (date !== null && date !== undefined) {
            var newdate = new Date(date);
            var month = newdate.getMonth() + 1;
            var day = newdate.getDate();
            var year = newdate.getFullYear();
            var hh = newdate.getHours();
            var mm = newdate.getMinutes();
            if (month < 10)
                month = "0" + month;
            if (day < 10)
                day = "0" + day;
            if (mm < 10)
                mm = "0" + mm;
            return todayDate = day + "/" + month + "/" + year;
        } else {
            return null;
        }
    }
    $rootScope.EditDate = function (datetime) {
        if (datetime !== null && datetime !== undefined) {
            var Str = datetime.toString();
            if (Str.indexOf("/Date") >= 0) {

                var newdate = new Date(parseInt(datetime.substr(6)));
                var month = newdate.getMonth() + 1;
                var day = newdate.getDate();
                var year = newdate.getFullYear();
                var hh = newdate.getHours();
                var mm = newdate.getMinutes();
                if (month < 10)
                    month = "0" + month;
                if (day < 10)
                    day = "0" + day;
                return year + "/" + month + "/" + day;
            } else {
                var value = datetime.split('/');

                return value[2] + '/' + value[1] + '/' + value[0];
            }
        }
        return null;
    };
    //Thêm phần tinymceOptions cho phần trợ giúp
    $rootScope.tinymceOptions = {
        selector: 'textarea',
        plugins: [
            'advlist autolink lists link  charmap print preview hr anchor pagebreak image',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime  media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
        ],
        skin: 'lightgray', language: 'vi_VN',
        theme: 'modern', height: 500,
        toolbar1: 'codesample | bold italic sizeselect fontselect fontsizeselect | hr alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | insertfile undo redo | emoticons | code',
        fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
        toolbar2: 'print preview insert link image media | forecolor backcolor emoticons | codesample',
        image_advtab: true,

        file_browser_callback: function (field_name, url, type, win) {
            console.log(type + "|" + win);
            var connector = "/_FM/Index.aspx?f=L1B1Ymxpc2hpbmdJbWFnZXM=";
            var enableAutoTypeSelection = true;
            if (enableAutoTypeSelection && type) {
                connector += "&Type=" + type;
            }
            connector += "&Field=" + field_name;
            appHelp.openPage(connector, "File Manager", 1150, 600);
        },
        relative_urls: false,
        remove_script_host: true,
        convert_urls: true
    };
});
app.config(function ($routeProvider, $validatorProvider) {
    $routeProvider
        .when('/', {
            templateUrl: '/AspNetUsers/getFile/index.html',
            controller: 'index'
        })
        .when('/edit/:id', {
            templateUrl:'/AspNetUsers/getFile/edit.html',
            controller: 'edit'
        })
        .when('/add/', {
            templateUrl:'/AspNetUsers/getFile/add.html',
            controller: 'add'
        })
    $validatorProvider.setDefaults({
        errorElement: 'span',
        errorClass: 'help-block',
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        success: function (label) {
            label.closest('.form-group').removeClass('has-error');
        }
    });
});
app.controller('index', function ($scope, $rootScope, $compile, $confirm, $uibModal, DTOptionsBuilder, DTColumnBuilder, DTInstances, dataservice) {

    var vm = $scope;
    $scope.selected = [];
    $scope.selectAll = false;
    $scope.toggleAll = toggleAll;
    $scope.toggleOne = toggleOne;
    $scope.staticParam = {
        totalItems: 0,
        currentPage: 1,
        pageSize: 15,
        maxSize: 5,
        Search: ""
    };
    $scope.lstInt = [];
    var titleHtml = '<label class="mt-checkbox"><input type="checkbox" ng-model="selectAll" ng-click="toggleAll(selectAll, selected)"/><span></span></label>';
    vm.dtOptions = DTOptionsBuilder.newOptions()
        .withPaginationType('full_numbers').withDOM("t<'table-scrollable't>ip")
        .withDataProp('data').withDisplayLength(15)
        .withOption('order', [2, 'asc'])
        .withOption('serverSide', true)
        .withOption('stateLoadParams', function (settings, data) {
            data.search.search = '';
        })
        .withOption('headerCallback', function (header) {
            if (!$scope.headerCompiled) {
                $scope.headerCompiled = true;
                $compile(angular.element(header).contents())($scope);

            }
        })
        .withOption('initComplete', function (settings, json) {
        })
        .withOption('createdRow', function (row, data, dataIndex) {
            const contextScope = $scope.$new(true);
            contextScope.data = data;
            $scope.data = data;
            var obj = {
                STT: data._STT.toString(),
                Id: data.Id
            }
            $scope.lstInt.push(obj);
            $compile(row)($scope);
            contextScope.contextMenu = $scope.contextMenu;
            $compile(angular.element(row).find('input'))($scope);
            $compile(angular.element(row).attr('context-menu', 'contextMenu'))(contextScope);
        });
       
        vm.dtOptions.withOption('ajax', {
            url: "/AspNetUsers/jtable",
        beforeSend: function (jqXHR, settings) {
            App.blockUI({
                target: "#contentMain",
                boxed: true,
                message: 'Đang tải...'
            });
        },
        type: 'POST',
            data: function (d) {
            $scope.STT = d.start;
                d.search.value = $scope.staticParam.Search;
            d.IsActive = $scope.model.IsActive;
            //d.RegionId = $scope.model.RegionId;
            $scope.staticParam.currentPage = $scope.dtInstance.DataTable.page() + 1;
        },
        complete: function () {
            App.unblockUI("#contentMain");
        }
    }).withOption('serverSide', true);
    vm.dtColumns = [];
    vm.dtColumns.push(DTColumnBuilder.newColumn("_STT").withTitle(titleHtml).notSortable()
        .renderWith(function (data, type, full, meta) {
            $scope.selected[full._STT] = false;
            return '<label class="mt-checkbox"><input type="checkbox" ng-model="selected[' + full._STT + ']" ng-click="toggleOne(selected)"/><span></span></label>';
        }).withOption('sWidth', '5px').withOption('sClass', 'tcenter'));
    vm.dtColumns.push(DTColumnBuilder.newColumn('_STT').withTitle('STT').withOption('sWidth', '5px').renderWith(function (data, type, full, meta) {
        return ++$scope.STT;
    }).withOption('sClass', 'tcenter'));

    vm.dtColumns.push(DTColumnBuilder.newColumn('Email').withTitle('Email').withOption('sWidth', '50px').renderWith(function (data, type, full, meta) {
        return '<a href="" ng-click="open(\'' + full.Id + '\')" >' + data + '</a>';
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('EmailConfirmed').withTitle('Xác nhận').withOption('sWidth', '600px').renderWith(function (data, type, full, meta) {
        if (data === "True") {
            return "Xác nhận";
        } else {
            return "Chưa xác nhận";
        }               
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('HoTen').withTitle('Họ tên').withOption('sWidth', '600px').renderWith(function (data, type, full, meta) {
        return data;        
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('DiaChi').withTitle('Địa chỉ').withOption('sWidth', '600px').renderWith(function (data, type, full, meta) {
        return data;
    }));
    //vm.dtColumns.push(DTColumnBuilder.newColumn('IsActive').withTitle('Trạng thái').withOption('sWidth', '50px').renderWith(function (data, type) {
    //    if (data === "True") return "Sử dụng";
    //    if (data === "False") return "Không sử dụng";
    //    return "";
    //}));
    vm.dtColumns.push(DTColumnBuilder.newColumn('Id').withTitle('Điều khiển').withOption('sWidth', '70px').withOption('sClass', 'tcenter').renderWith(function (data, type, full, meta) {
        return "<a href='' data-action='OPEN' ng-click='open(\"" + full.Id + "\")' data-toggle='modal' class='btn btn-icon-only btn-default btn-custom' title='Xem tài khoản'><i class='fa fa-eye'></i></a>" +
            "<a href='' data-action='EDIT' ng-click='edit(\"" + full.Id + "\")' data-toggle='modal' class='btn btn-icon-only btn-default btn-custom' title='Sửa tài khoản'><i class='fa fa-edit'></i></a>" +
            "<a href='' data-action='DELETE' ng-click='delete(\"" + full.Id + "\")' class='btn btn-icon-only btn-default btn-custom' title='Xóa tài khoản'><i class='fa fa-trash-o'></i></a>";
    }));

    vm.reloadData = reloadData;
    vm.dtInstance = {};
    function reloadData(resetPaging) {
        $scope.staticParam.currentPage = resetPaging;
        vm.dtInstance.reloadData(callback, resetPaging);
    }
    function callback(json) {

    }
    function toggleAll(selectAll, selectedItems) {
        for (var id in selectedItems) {
            if (selectedItems.hasOwnProperty(id)) {
                selectedItems[id] = selectAll;
            }
        }
    }
    function toggleOne(selectedItems) {
        for (var id in selectedItems) {
            if (selectedItems.hasOwnProperty(id)) {
                if (!selectedItems[id]) {
                    vm.selectAll = false;
                    return;
                }
            }
        }
        vm.selectAll = true;
    }
    $rootScope.IsActive = 1;
    $scope.reload = function () {
        $rootScope.IsActive = $scope.model.IsActive;
        reloadData(true);

    }
    $scope.reloadALL = function () {
        $scope.staticParam.Search = "";
        $scope.model.IsActive = 1;
        reloadData(true);

    }
    $rootScope.reload = function () {
        $scope.staticParam.Search = "";
        reloadData(true);

    }
    $scope.add = function () {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: '/AspNetUsers/getFile/add.html',
            controller: 'add',
            backdrop: true,
            size: '90'
        });
        modalInstance.result.then(function (d) {
            $scope.reload();
        }, function () {
        });
    }
    $scope.open = function (temp) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl:'/AspNetUsers/getFile/open.html',
            controller: 'open',
            backdrop: true,
            size: '90',
            resolve: {
                para: function () {
                    return temp;
                }
            }
        });
        modalInstance.result.then(function (d) {
            $scope.reload();
        }, function () {
        });
    }
    $scope.edit = function (temp) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: '/AspNetUsers/getFile/edit.html',
            controller: 'edit',
            backdrop: true,
            size: '90',
            resolve: {
                para: function () {
                    return temp;
                }
            }
        });
        modalInstance.result.then(function (d) {
            $scope.reload();
        }, function () {
        });
    }
    $scope.delete = function (temp) {
        dataservice.getItem(temp, function (rs) {
            $confirm({ text: 'Bạn có đồng ý xóa [' + rs.Email + '] ?', title: 'Xác nhận', cancel: ' Hủy ', ok: ' Đồng ý' })
                .then(function () {
                    App.blockUI({
                        target: "#contentMain",
                        boxed: true,
                        message: 'Đang tải...'
                    });
                    dataservice.delete(temp, function (result) {
                        if (result.Error) {
                            App.notifyDanger(result.Title);
                        } else {
                            App.notifyInfo(result.Title);
                            $scope.reload();
                        }
                        App.unblockUI("#contentMain");
                    });
                });
        });
    }
   
    $scope.deleteChecked = function () {
        var deleteItems = [];
        for (var id in $scope.selected) {
            if ($scope.selected.hasOwnProperty(id)) {
                if ($scope.selected[id]) {
                    angular.forEach($scope.lstInt, function (value, key) {
                        if (value.STT === id) {
                            deleteItems.push(value.Id);
                        }
                        
                    });
                  
                }
            }
        }
        if (deleteItems.length > 0) {
            $confirm({ text: 'Bạn có chắc chắn muốn xóa các bản ghi đã chọn?', title: 'Xác nhận', ok: 'Đồng ý', cancel: ' Hủy ' })
                .then(function () {
                    App.blockUI({
                        target: "#contentMain",
                        boxed: true,
                        message: 'Đang tải...'
                    });
                    dataservice.deleteItems(deleteItems, function (result) {
                        if (result.Error) {
                            App.notifyDanger(result.Title);
                        } else {
                            App.notifyInfo(result.Title);
                            $scope.reload();
                        }
                        App.unblockUI("#contentMain");
                    });

                });
        } else {
            App.notifyDanger("Không có bản ghi nào được chọn");
        }
    }
    $scope.contextMenu = [
        [function ($itemScope) {
            return '<i class="fa fa-eye"></i> Xem tài khoản';
        }, function ($itemScope, $event, model) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: '/AspNetUsers/getFile/open.html',
                controller: 'open',
                backdrop: true,
                size: '80',
                resolve: {
                    para: function () {
                        return $itemScope.data.Id;
                    }
                }
            });
            modalInstance.result.then(function (d) {
                $scope.reload();
            }, function () {
            });
            }, function ($itemScope, $event, model) {
                return true;
        }],
        [function ($itemScope) {
            return '<i class="fa fa-edit"></i> Sửa tài khoản';
        }, function ($itemScope, $event, model) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: '/AspNetUsers/getFile/edit.html',
                controller: 'edit',
                backdrop: true,
                size: '80',
                resolve: {
                    para: function () {
                        return $itemScope.data.Id;
                    }
                }
            });
            modalInstance.result.then(function (d) {
                $scope.reload();
            }, function () {
            });
        }, function ($itemScope, $event, model) {
            return true;
        }],
        [function ($itemScope) {
            return '<i class="fa fa-remove"></i> Xóa tài khoản';
        }, function ($itemScope, $event, model) {

            $confirm({ text: 'Bạn có đồng ý xóa [' + $itemScope.data.Email + '] ?', title: 'Xác nhận', ok: 'Đồng ý', cancel: ' Hủy ' })
                .then(function () {
                    App.blockUI({
                        target: "#contentMain",
                        boxed: true,
                        message: 'Đang tải...'
                    });
                    dataservice.delete($itemScope.data.Id, function (result) {
                        if (result.Error) {
                            App.notifyDanger(result.Title);
                        } else {
                            App.notifyInfo(result.Title);
                            $scope.reload();
                        }
                        App.unblockUI("#contentMain");
                    });
                });
        }, function ($itemScope, $event, model) {
            return true;
        }]
    ];
});

app.controller('add', function ($scope, $rootScope, $compile, $uibModal, $confirm, $uibModalInstance, dataservice) {
    $scope.addTitle = "Thêm tài khoản";
    $scope.model = { EmailConfirmed: 1 };
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    }
    $scope.submit = function () {
        if ($scope.addform.validate()) {
            dataservice.insert($scope.model, function (rs) {
                if (rs.Error) {
                    App.notifyDanger(rs.Title);
                } else {
                    App.notifyInfo(rs.Title);
                    $uibModalInstance.close();
                }
            });
        }
    }
});
app.controller('sort', function ($scope, $rootScope, $compile, $uibModal, $confirm, $uibModalInstance, dataservice) {

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    }

    $scope.initData = function () {
        dataservice.getAll(function (rs) {
            $scope.model = rs;
        });
    }
    $scope.initData();
    $scope.resort = function (item, index) {
        $scope.model.splice(index, 1);
        $scope.model.splice(item.Ord - 1, 0, item);
        $.each($scope.model, function (index, item) {
            item.Ord = index + 1;
        });
    }
    $scope.submit = function () {
        dataservice.resort($scope.model, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {
                App.notifyInfo(rs.Title);
                $uibModalInstance.close();
            }
        });
    }
});
app.controller('edit', function ($scope, $rootScope, $compile, $uibModal, $confirm, $uibModalInstance, dataservice, para) {
    $scope.editTitle = "Sửa tài khoản";
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    }
    $scope.initData = function () {
        dataservice.getItem(para, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {
                $scope.model = rs;
                if ($scope.model.EmailConfirmed === true) {
                    $scope.model.EmailConfirmed = 1;
                } else { $scope.model.EmailConfirmed = 0; }
            }
        });
    }
    $scope.initData();
    $scope.submit = function () {
        if ($scope.editform.validate()) {
            dataservice.update($scope.model, function (rs) {
                if (rs.Error) {
                    App.notifyDanger(rs.Title);
                } else {
                    App.notifyInfo(rs.Title);
                    $uibModalInstance.close();
                }
            });
        }
    }
});
app.controller('open', function ($scope, $rootScope, $compile, $uibModal, $confirm, $uibModalInstance, dataservice, para) {
    $scope.openTitle = "Xem tài khoản";
    $scope.model = {};
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    }
    $scope.loadData = function () {

        dataservice.getItem(para, function (rs) {
            $scope.model = rs;
            //if (rs.CreatedBy !== null) {
            //    $scope.model.CreatedBy = $scope.model.CreatedBy.split('#')[1];
            //}
            //if (rs.ModifiedBy !== null) {
            //    $scope.model.ModifiedBy = $scope.model.ModifiedBy.split('#')[1];
            //}
            if ($scope.model.EmailConfirmed === true) {
                $scope.model.EmailConfirmed = "Xác nhận";
            } else { $scope.model.EmailConfirmed = "Chưa xác nhận"; }
        });
    }
    $scope.loadData();
}); 
