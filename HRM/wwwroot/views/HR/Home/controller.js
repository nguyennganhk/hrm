﻿var ctxfolder = "/views/HR/Home";
//var app = angular.module('App_ESHRM_Contracts', ["ui.bootstrap", "ngRoute", "ngValidate", "datatables", "datatables.bootstrap", 'datatables.colvis', "ui.bootstrap.contextMenu", 'datatables.colreorder', 'angular-confirm', 'ui.select', 'angularjs-datetime-picker', 'angularFileUpload', 'summernote']);

app.factory('dataservice', function ($http) {
    var headers = {
        "Content-Type": "application/json;odata=verbose",
        "Accept": "application/json;odata=verbose"
    };
    return {
        //------------------------*Store*-------------------------     
        getDashboard: function (data, callback) {
            $http.post('/Hr/Home/getDashboard', data).success(callback);
        },
        getItems: function (callback) {
            $http.post('/Home/getItems/').success(callback);
        },
    };
});

app.config(function ($routeProvider, $validatorProvider) {
    $routeProvider
        .when('/', {
            templateUrl: ctxfolder + '/index.html',
            controller: 'index'
        })
        .when('/edit/:id', {
            templateUrl: ctxfolder + '/edit.html',
            controller: 'edit'
        }).when('/open/:id', {
            templateUrl: ctxfolder + '/open.html',
            controller: 'open'
        })
        .when('/add/', {
            templateUrl: ctxfolder + '/add.html',
            controller: 'add'
        })
    $validatorProvider.setDefaults({
        errorElement: 'span',
        errorClass: 'help-block',
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        success: function (label) {
            label.closest('.form-group').removeClass('has-error');
        }
    });
});
app.controller('CtrlHRMHome', function ($scope, $rootScope, $compile, dataservice) {
    $rootScope.StatusData = [{
        value: 1,
        text: 'Dự án mới'
    }, {
        value: 2,
        text: 'Đang triển khai'
    }
        , {
        value: 3,
        text: 'Hoàn thành'
    }
    ];


    $rootScope.CreateDate = function (date) {
        if (date !== null && date !== undefined) {
            var newdate = new Date(date);
            var month = newdate.getMonth() + 1;
            var day = newdate.getDate();
            var year = newdate.getFullYear();
            var hh = newdate.getHours();
            var mm = newdate.getMinutes();
            if (month < 10)
                month = "0" + month;
            if (day < 10)
                day = "0" + day;
            if (mm < 10)
                mm = "0" + mm;
            return todayDate = day + "/" + month + "/" + year;
        } else {
            return null;
        }
    }

    //0 11/11/2017
    //1 11/11/2017 22:22
    //2 123123123
    //3 type date
    //4 type data year month day
    $rootScope.ConDate = function (data, number) {
        try {
            if (number ==7) {
                const date = new Date();
                let day = date.getDate();
                let month = date.getMonth() + 1;
                let years = date.getFullYear().toString();
                let days = day.toString(); let months = month.toString();
                if (day <= 9) days ="0"+ day.toString();
                if (month <= 9) months = "0" + month.toString();
                // This arrangement can be altered based on how we want the date's format to appear.
                let currentDate = days + '/' + months + '/' + years;
                return currentDate;
            }
            if (data == null || data == "") {
                return '';
            }           
            if (data !== null && data != "" && data != undefined) {
                try {
                    if (data.indexOf("SA") != -1 || data.indexOf("CH") != -1) {
                        if (data.indexOf("SA") != -1) { }
                        if (data.indexOf("CH") != -1) { }
                    }

                    if (data.indexOf('Date') != -1) {
                        data = data.substring(data.indexOf("Date") + 5);
                        data = parseInt(data);
                    }
                }
                catch (ex) { }
                if (number == 5) {
                    var d = data.split('/');
                    return d[2].toString() + d[1].toString() + d[0].toString();
                }
                var newdate = new Date(data);
                if (number == 3) {
                    return newdate;
                }
                if (number == 2) {
                    return "/Date(" + newdate.getTime() + ")/";
                }
                var month = newdate.getMonth() + 1;
                var day = newdate.getDate();
                var year = newdate.getFullYear();
                var hh = newdate.getHours();
                var mm = newdate.getMinutes();
                if (month < 10)
                    month = "0" + month;
                if (day < 10)
                    day = "0" + day;
                if (mm < 10)
                    mm = "0" + mm;
                if (number == 0) {
                    return todayDate = day + "/" + month + "/" + year;
                }
                
                if (number == 1) {
                    return todayDate = day + "/" + month + "/" + year + " " + hh + ":" + mm;
                }
                if (number == 4) {
                    return new Date(year, month - 1, day);
                }
            } else {
                return '';
            }
        } catch (ex) {
            return "";
        }
    }
    //định dạng số xóa dấu chấm phẩy
    $rootScope.addPeriod = function (nStr) {
        if (nStr !== null && nStr !== "" && nStr !== undefined) {
            nStr = Math.round(parseFloat(nStr) * 100) / 100;

            nStr += "";
            if (nStr.indexOf(",") >= 0) {
                var x = nStr.split(",");
            } else {
                var x = nStr.split(".");
            }
            var x1 = x[0];
            var x2 = x.length > 1 && parseInt(x[1]) > 0 ? "," + x[1] : "";
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, "$1" + "." + "$2");
            }
            var Total = x1 + x2;
            return Total;
        }
        else {
            return 0;
        }
    };
    //----------Bắt đầu lấy tuần trong năm-------------------
    // lấy số tuần từ 1 năm
    $rootScope.getISOWeeks = function (y) {
        var d, isLeap;

        d = new Date(y, 0, 1);
        isLeap = new Date(y, 1, 29).getMonth() === 1;

        //check for a Jan 1 that's a Thursday or a leap year that has a 
        //Wednesday jan 1. Otherwise it's 52
        return d.getDay() === 4 || isLeap && d.getDay() === 3 ? 53 : 52
    }
    // lấy danh sách tuần từ 1 năm
    $rootScope.getWeek = function (data) {
        $rootScope.WeekData = [];
        //$rootScope.WeekData.push({ value: null, text: "Bỏ chọn" });
        for (var i = 1; i <= data; i++) {
            var ob = { value: i, text: 'Tuần ' + i.toString() };
            $rootScope.WeekData.push(ob);
        }
        if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
            $scope.$apply();
        }
    }
    // lấy danh sách tuần từ 1 năm
    $rootScope.getDateOfISOWeek = function (w, y) {
        var simple = new Date(y, 0, 1 + (w - 1) * 7);
        var dow = simple.getDay();
        var ISOweekStart = simple;
        if (dow <= 4)
            ISOweekStart.setDate(simple.getDate() - simple.getDay() + 1);
        else
            ISOweekStart.setDate(simple.getDate() + 8 - simple.getDay());
        return ISOweekStart;
    }

    $rootScope.YearData = []; $rootScope.MonthData = [];
    //$rootScope.MonthData.push({ value: null, text: "Bỏ chọn" });
    for (var i = 2018; i < 2050; i++) {
        var ob = { value: i, text: 'Năm ' + i.toString() };
        $rootScope.YearData.push(ob);
    }
    for (var i = 1; i <= 12; i++) {
        var ob = { value: i, text: 'Tháng ' + i.toString() };
        $rootScope.MonthData.push(ob);
    }
    //----------Kết thúc lấy tuần trong năm-------------------
});
app.controller('index', function ($http, $scope, $rootScope, $compile, $confirm, $uibModal, DTOptionsBuilder, DTColumnBuilder, DTInstances, dataservice, keyhandle, utility) {
    $scope.model = {}; $scope.jxcelAddDetail = {};
    var vm = $scope;
    var ctrl = $scope;
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    }

    ctrl.nextRow = function (d) {
        if (ctrl.ElementRowCheck === -1) {
            if (ctrl.allRow.length > 1) {
                ctrl.ElementRowCheck = ctrl.allRow[0].row;
                $('td', ctrl.ElementRowCheck).addClass("es-iSeleted");
                ctrl.iSelected = ctrl.allRow[0].data;
                ctrl.open_Detail(ctrl.iSelected.QuotationNo, ctrl.iSelected.CurrencyRate);

            }
        } else
            for (var i = 0; i < ctrl.allRow.length; i++) {
                if (ctrl.allRow[i].row === ctrl.ElementRowCheck) {
                    if (i === ctrl.allRow.length - 1) break;
                    $('td', ctrl.ElementRowCheck).removeClass('es-iSeleted');
                    ctrl.ElementRowCheck = ctrl.allRow[i + 1].row;
                    $('td', ctrl.ElementRowCheck).addClass("es-iSeleted");
                    ctrl.iSelected = ctrl.allRow[i + 1].data;
                    ctrl.open_Detail(ctrl.iSelected.QuotationNo, ctrl.iSelected.CurrencyRate);
                    break;
                }
            }
    }
    ctrl.upRow = function (d) {
        if (ctrl.ElementRowCheck === -1) {
            if (ctrl.allRow.length > 1) {
                ctrl.ElementRowCheck = ctrl.allRow[ctrl.allRow.length - 1].row;
                $('td', ctrl.ElementRowCheck).addClass("es-iSeleted");
                ctrl.iSelected = ctrl.allRow[ctrl.allRow.length - 1].row;
                ctrl.open_Detail(ctrl.iSelected.QuotationNo, ctrl.iSelected.CurrencyRate);
            }
        } else
            for (var i = 0; i < ctrl.allRow.length; i++) {
                if (ctrl.allRow[i].row === ctrl.ElementRowCheck) {
                    if (i === 0) break;
                    $('td', ctrl.ElementRowCheck).removeClass('es-iSeleted');
                    ctrl.ElementRowCheck = ctrl.allRow[i - 1].row;
                    $('td', ctrl.ElementRowCheck).addClass("es-iSeleted");
                    ctrl.iSelected = ctrl.allRow[i - 1].data;
                    ctrl.open_Detail(ctrl.iSelected.QuotationNo, ctrl.iSelected.CurrencyRate);
                    break;
                }
            }
    }
    keyhandle.eventCtrl(40, ctrl.nextRow);
    keyhandle.eventCtrl(38, ctrl.upRow);
    ctrl.ElementRowCheck = -1;
    ctrl.allRow = [];
    ctrl.iSelected = {};
    ctrl.dataScrollPage = [];
    var today = new Date();
    var day = today.getDate();
    var month = today.getMonth() + 1;
    var yyyy = today.getFullYear();

    $scope.staticParam = {
        totalItems: 0,
        currentPage: 1,
        pageSize: 15,
        maxSize: 5,
        Title: "",
        lstDepartmentID: [],
        Day: "" + day,
        Month: "" + month,
        Years: "" + yyyy,
        DepartmentID: [],
        TimeKeepingGuid: null,
        EmployeeName: null,
        IPMachine: ""
    };
    $scope.txtComment = "";
    $scope.dataload = [];
    $scope.CheckReload = false;
    $scope.lstInt = [];
    $rootScope.TongGT = 0;
    $rootScope.TongRows = 0;
    $scope.selected = [];
    $scope.selectAll = false;
    $scope.toggleAll = toggleAll;
    $scope.toggleOne = toggleOne;
    var titleHtml = '<label class="mt-checkbox"><input type="checkbox" ng-model="selectAll" ng-click="toggleAll(selectAll, selected)"/><span></span></label>';

    vm.dtOptions = DTOptionsBuilder.newOptions()
        .withOption('serverSide', true)
        .withOption('order', [1, 'asc'])
        .withDataProp('data')
        .withOption('pageLength', 25)
        .withOption('initComplete', function (settings, json) {
            //chỉ dùng khi có footer
            //ẩn scroll-x body
            $('.dataTables_scrollBody').css({
                'overflow-y': 'scroll',
                'overflow-x': 'hidden',
                'border': 'none'
            });
            //hiền scroll footer
            $('#tblData_wrapper .dataTables_scrollFoot').css('overflow', 'auto');
            //cuộn toàn bảng theo footer
            $('#tblData_wrapper .dataTables_scrollFoot').on('scroll', function () {
                $('#tblData_wrapper .dataTables_scrollBody').scrollLeft($(this).scrollLeft());
            });

            //thêm sự kiện scroll phân trang
            $('#tblData').parent().attr("onscroll", "angular.element(this).scope().LoadScroll(this)");
        })
        .withOption('searching', false)
        .withOption('createdRow', function (row, data, dataIndex) {
            const contextScope = $scope.$new(true);
            contextScope.data = data;
            $scope.data = data;
            var obj = {
                STT: data._STT.toString(),
                Id: data.CandidateGuid
            }
            $scope.lstInt.push(obj);
            $compile(row)($scope);
            contextScope.contextMenu = "";// $scope.contextMenu;
            //$compile(angular.element(row).find('input'))($scope);
            //$compile(angular.element(row).attr('context-menu', 'contextMenu'))(contextScope);
        });
    vm.dtOptions.withOption('ajax', function (data, callback, settings) {
        data.Title = $scope.staticParam.Title;
        data.StartDate = null;
        data.EndDate = null;
        data.Status = "0";

        /* data.IPMachine = $scope.staticParam.IPMachine; */
        $scope.staticParam.currentPage = $scope.dtInstance.DataTable.page() + 1;
        if ($scope.CheckReload) {
            data.Start = 0;
        }
        App.blockUI({
            target: "#table_load",
            boxed: true,
            message: 'Đang tải...'
        });
        $http.post('/ProductDeparments/JTable', data)
            .success(function (res) {
                if (data.start === 0) {
                    $scope.dataload = [];
                    angular.forEach(res.data, function (val, key) {
                        $scope.dataload.push(val);
                    });
                    callback({
                        data: $scope.dataload,
                        recordsTotal: res.recordsTotal,
                        recordsFiltered: res.recordsFiltered
                    });
                }
                else if (res.recordsTotal > $scope.dataload.length) {
                    angular.forEach(res.data, function (val, key) {
                        $scope.dataload.push(val);
                    });

                    callback({
                        data: $scope.dataload,
                        recordsTotal: res.recordsTotal,
                        recordsFiltered: res.recordsFiltered
                    });
                }
                $rootScope.TongRows = res.recordsTotal;
                App.unblockUI("#table_load");

            });
    });
    //scroll theo tỷ lệ màn hình
    vm.dtOptions.withOption('scrollY', '20vh')
        .withOption('scrollX', '100%') //mặc định 100%
        .withOption('scrollCollapse', true) //xóa khoảng trắng khi dữ liệu trong bảng ít
        .withOption('rowCallback', rowCallback) //sự kiên click vào 1 dòng
        .withOption('scroller', {
            loadingIndicator: true
        }).withFixedColumns({
            leftColumns: 0 //fix bên trái 2 cột
            //rightColumns: 1 //fix bên phải 1 cột
        });

    vm.dtColumns = [];

    vm.dtColumns.push(DTColumnBuilder.newColumn('_STT').withTitle('STT').notSortable().withOption('sClass', 'tcenter').withOption('sWidth', '10px').renderWith(function (data, type, full, meta) {
        return meta.row + 1;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('OrderID').withTitle('Mã đơn hàng').withOption('sWidth', '100px').renderWith(function (data, type, full, meta) {
        return data;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('DeliveryDate').withTitle('Ngày giao hàng').withOption('sWidth', '90px').withOption('sClass', 'tcenter').renderWith(function (data, type, full, meta) {       
        return full.DeliveryDateString;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('CustomerName').withOption('sWidth', '300px').withTitle('Khách hàng').renderWith(function (data, type, full, meta) {
        return data;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('DeliveryDate').withOption('sWidth', '60px').withTitle('Trạng thái').renderWith(function (data, type, full, meta) {
        if (full.DeliveryDateString !="") {
            var starDate = full.DeliveryDateString;
            var endDate = $rootScope.ConDate("",7);
            var numberDay = (datediff(parseDate(starDate), parseDate(endDate)));
            if (numberDay <= 7 && numberDay >= 0) {               
                return "<div class='tred'><span class=''><=7 ngày</span></div>";
            }
            else if (numberDay <= 10 && numberDay >= 0) {
                return "<div class='tyellow'><span class=''><=10 ngày</span></div>";
            }
            else if (numberDay <= 15 && numberDay >= 0) {
                return "<div class='tblue'><span class=''><=15 ngày</span></div>";
            } else if (numberDay < 0) {
                return "<div class='tquahan'><span class=''>Quá hạn</span></div>";
            } else {
                return "";
            } 
        } else {
            return "";
        }
        
    }));

    vm.reloadData = reloadData;
    vm.dtInstance = {};
    function reloadData(resetPaging) {
        $scope.staticParam.currentPage = resetPaging;
        vm.dtInstance.reloadData(callback, resetPaging);
    }
    function callback(json) {

    }
    function toggleAll(selectAll, selectedItems) {
        for (var id in selectedItems) {
            if (selectedItems.hasOwnProperty(id)) {
                selectedItems[id] = selectAll;
            }
        }
    }
    function toggleOne(selectedItems) {
        for (var id in selectedItems) {
            if (selectedItems.hasOwnProperty(id)) {
                if (!selectedItems[id]) {
                    vm.selectAll = false;
                    return;
                }
            }
        }
        vm.selectAll = true;
    }
    $scope.total = 0;
    $scope.check = false;
    $scope.LoadScroll = function (obj) {
        var total = obj.offsetHeight + obj.scrollTop;
        if (obj.offsetHeight + obj.scrollTop >= obj.scrollHeight && $scope.check && $scope.total <= total) {
            $scope.check = false;
            try {
                $scope.CheckReload = false;
                vm.dtInstance.DataTable.page('next').draw('page');
            }
            catch (ex) {
            }
        }
        if (obj.offsetHeight + obj.scrollTop >= obj.scrollHeight) {
            $scope.total = obj.offsetHeight + obj.scrollTop;
        }
        else {
            $scope.check = true;
        }

    };
    function datediff(first, second) {
        return Math.round((second - first) / (1000 * 60 * 60 * 24));
    }

    /**
     * new Date("dateString") is browser-dependent and discouraged, so we'll write
     * a simple parse function for U.S. date format (which does no error checking)
     */
    function parseDate(str) {
        var mdy = str.split('/');
        return new Date(mdy[2], mdy[1], mdy[0]);
    }
    $('#minize-menuleft').click(function () {
        vm.dtInstance.DataTable.draw();
    });
    function rowCallback(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        if (iDisplayIndexFull === 0) ctrl.allRow = [];
        $('td', nRow).unbind('click');
        $('td', nRow).bind('click', function () {
            if (vm.ElementRowCheck === nRow) return;
            $('td', nRow).addClass("es-iSeleted");
            $('td', vm.ElementRowCheck).removeClass('es-iSeleted');
            vm.ElementRowCheck = nRow;
            ctrl.ElementRowCheck = nRow;
            ctrl.iSelected = aData;
            vm.staticParam.TimeKeepingGuid = aData.TimeKeepingGuid;
            vm.staticParam.EmployeeName = aData.EmployeeName;

        });
        $('td', nRow).unbind('dblclick');
        $('td', nRow).bind('dblclick', function () {
            $scope.$apply(function () {
                //$scope.open(aData.CandidateGuid);
            });
        });
        ctrl.allRow.push({ row: nRow, data: aData });
        return nRow;
    }

    $rootScope.reloadData = function () {
        $scope.dataload = [];
        reloadData(true);
    }
    //------------------------------------------------
    // danh sách chi tiết
    $scope.selected_hhdv = [];
    $scope.selectAll_hhdv = false;
    $scope.toggleAll_hhdv = toggleAll_hhdv;
    $scope.toggleOne_hhdv = toggleOne_hhdv;
    $scope.dataload_hhdv = [];
    $scope.staticParam_hhdv = {
        totalItems: 0,
        currentPage: 1,
        pageSize: 15,
        maxSize: 5,
        SearchWards: "",
        IsActive: 0,
        QuotationNo: "",
        Start: 0

    };

    $scope.CheckReload_hhdv = false;
    $rootScope.TongSL = 0; $rootScope.TongThanhtien = 0; $rootScope.TongThue = 0; $rootScope.TongChietkhau = 0; $rootScope.TongTienTT = 0;
    $scope.lstInt_hhdv = [];
    // var titleHtml = '<label class="mt-checkbox"><input type="checkbox" ng-model="selectAll_hhdv" ng-click="toggleAll_hhdv(selectAl_hhdvl, selected_hhdv)"/><span></span></label>';

    vm.dtOptions_hhdv = DTOptionsBuilder.newOptions()
        .withOption('serverSide', true)
        .withOption('order', [1, 'desc'])
        .withDataProp('data')
        .withOption('pageLength', 15)
        .withOption('initComplete', function (settings, json) {
            //chỉ dùng khi có footer
            //ẩn scroll-x body
            $('.dataTables_scrollBody').css({
                'overflow-y': 'scroll',
                'overflow-x': 'hidden',
                'border': 'none'
            });
            //hiền scroll footer
            $('#tblData_hhdv_wrapper .dataTables_scrollFoot').css('overflow', 'auto');
            //cuộn toàn bảng theo footer
            $('#tblData_hhdv_wrapper .dataTables_scrollFoot').on('scroll', function () {
                $('#tblData_hhdv_wrapper .dataTables_scrollBody').scrollLeft($(this).scrollLeft());
            });

            //thêm sự kiện scroll phân trang
            $('#tblData_hhdv').parent().attr("onscroll", "angular.element(this).scope().LoadScroll_hhdv(this)");
        })
        .withOption('searching', false)
        .withOption('createdRow', function (row, data, dataIndex) {
            const contextScope = $scope.$new(true);
            contextScope.data = data;
            $scope.data = data;
            $compile(row)($scope);
            contextScope.contextMenu = "";// $scope.contextMenu;
            //$compile(angular.element(row).find('input'))($scope);
            //$compile(angular.element(row).attr('context-menu', 'contextMenu'))(contextScope);
        });
    //if (App.Permissions.LISTVIEW)
    vm.dtOptions_hhdv.withOption('ajax', function (data, callback, settings) {
        data.IsPublic = "1";
        data.Ispriority = "Q";
        data.StartDate = null;
        data.EndDate = null;
        if ($scope.CheckReload_hhdv) {
            data.start = 0;
        }

        $http.post('/Home/JTable_Announcements', data)
            .success(function (res) {
                if (data.start === 0) {
                    $scope.dataload_hhdv = [];
                    angular.forEach(res.data, function (val, key) {
                        $scope.dataload_hhdv.push(val);
                    });
                    callback({
                        data: $scope.dataload_hhdv,
                        recordsTotal: res.recordsTotal,
                        recordsFiltered: res.recordsFiltered
                    });
                }
                else {
                    if (res.recordsTotal > $scope.dataload_hhdv.length) {
                        angular.forEach(res.data, function (val, key) {
                            $scope.dataload_hhdv.push(val);
                        });
                        callback({
                            data: $scope.dataload_hhdv,
                            recordsTotal: res.recordsTotal,
                            recordsFiltered: res.recordsFiltered
                        });
                    }
                }


            });
    });
    //scroll theo tỷ lệ màn hình
    vm.dtOptions_hhdv.withOption('scrollY', '30vh')
        .withOption('scrollX', '100%') //mặc định 100%
        .withOption('scrollCollapse', true) //xóa khoảng trắng khi dữ liệu trong bảng ít
        .withOption('rowCallback', rowCallback2) //sự kiên click vào 1 dòng
        .withOption('scroller', {
            loadingIndicator: true
        }).withFixedColumns({
            leftColumns: 0 //fix bên trái 2 cột
            //rightColumns: 1 //fix bên phải 1 cột
        });

    vm.dtColumns_hhdv = [];

    vm.dtColumns_hhdv.push(DTColumnBuilder.newColumn('_STT').withTitle('STT').notSortable().withOption('sWidth', '10px').withOption('sClass', 'tcenter').renderWith(function (data, type, full, meta) {
        return meta.row + 1;
    }));
    vm.dtColumns_hhdv.push(DTColumnBuilder.newColumn('Title').withTitle('Tên thông báo').withOption('sClass', 'tleft').renderWith(function (data, type, full, meta) {
        return data;
    }));


    vm.reloadData_hhdv = reloadData_hhdv;
    vm.dtInstance_hhdv = {};
    function reloadData_hhdv(resetPaging) {
        //$scope.staticParam_hhdv.currentPage = resetPaging;
        $scope.CheckReload_hhdv = true;
        $scope.dataload_hhdv = [];
        $scope.total_hhdv = 0;
        $scope.check_hhdv = false;
        vm.dtInstance_hhdv.reloadData(callback_hhdv, resetPaging);
        vm.dtInstance_hhdv.DataTable.draw();
    }
    function callback_hhdv(json) {

    }
    function toggleAll_hhdv(selectAll, selectedItems) {
        for (var id in selectedItems) {
            if (selectedItems.hasOwnProperty(id)) {
                selectedItems[id] = selectAll;
            }
        }
    }
    function toggleOne_hhdv(selectedItems) {
        for (var id in selectedItems) {
            if (selectedItems.hasOwnProperty(id)) {
                if (!selectedItems[id]) {
                    vm.selectAll_hhdv = false;
                    return;
                }
            }
        }
        vm.selectAll_hhdv = true;
    }
    $scope.total_hhdv = 0;
    $scope.check_hhdv = false;
    $scope.LoadScroll_hhdv = function (obj) {
        var total = obj.offsetHeight + obj.scrollTop;
        if (obj.offsetHeight + obj.scrollTop >= obj.scrollHeight && $scope.check_hhdv && $scope.total_hhdv < total) {
            $scope.check_hhdv = false;
            $scope.CheckReload_hhdv = false;
            try {
                vm.dtInstance_hhdv.DataTable.page('next').draw('page');
            }
            catch (ex) {
            }
        }
        if (obj.offsetHeight + obj.scrollTop >= obj.scrollHeight) {
            $scope.total_hhdv = obj.offsetHeight + obj.scrollTop;
        }
        else {
            $scope.check_hhdv = true;
        }
    };

    //Hàm trả về khi click vào 1 dòng
    function rowCallback2(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        $('td', nRow).unbind('dblclick');
        $('td', nRow).bind('dblclick', function () {
            $scope.$apply(function () {
                //$scope.open(aData.BankGuid);
            });
        });
        return nRow;
    }

    $scope.reload_hhdv = function () {
        $scope.dataload_hhdv = [];

        reloadData_hhdv(true);

    };
    $scope.reloadALL_hhdv = function () {
        $scope.staticParam_hhdv.Search = "";
        $scope.model_hhdv.IsActive = 1;
        $scope.dataload_hhdv = [];
        reloadData_hhdv(true);
    };

    // --------------------------------
    //Thống kê TỒN KHO
    $rootScope.reloadbySLDH = function () {
        dataservice.getItems(function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {
                $scope.dataProvider1 = [];
                angular.forEach(rs, function (value, key) {
                    value.GTĐH = value.value !== null && value.value !== undefined ? parseInt(value.value) : 0;
                    var ob = {
                        "year1": value.text,
                        "year": value.text,
                        "income": $rootScope.addPeriod(value.GTĐH),
                        "expenses": $rootScope.addPeriod(value.GTĐH)
                    };
                    $scope.dataProvider1.push(ob);
                    ob = {};
                    var chart = AmCharts.makeChart("chartdiv2", {
                        "type": "serial",
                        "addClassNames": true,
                        "theme": "none",
                        "autoMargins": false,
                        "marginLeft": 30,
                        "marginRight": 8,
                        "marginTop": 10,
                        "marginBottom": 26,
                        "balloon": {
                            "adjustBorderColor": false,
                            "horizontalPadding": 10,
                            "verticalPadding": 8,
                            "color": "#ffffff",
                            "font-size": 9
                        },
                        "dataProvider": $scope.dataProvider1,
                        "valueAxes": [{
                            "axisAlpha": 0,
                            "position": "left"
                        }],
                        "startDuration": 1,
                        "graphs": [{
                            "alphaField": "alpha",
                            "balloonText": "<span style='font-size:12px;'>[[expenses]]</span>",
                            "fillAlphas": 1,
                            "title": "Income",
                            "type": "column",
                            "valueField": "income",
                            "dashLengthField": "dashLengthColumn",
                            "labelText": "[[value]]",
                        }],
                        "categoryField": "year",
                        "categoryAxis": {
                            //"labelRotation": 90,
                            "fontSize": 7
                        },
                        "numberFormatter": {
                            "precision": -1,
                            "decimalSeparator": ",",
                            "thousandsSeparator": "."
                        },
                        "export": {
                            "enabled": true
                        }
                    });
                });
            }
        })

    };
    $rootScope.reloadbySLDH();

    $scope.open_PE = function (temp) {
        if (temp !== "" && temp !== undefined && temp !== null) {
            window.open("/HR/Employees/Index#/?" + "OPEN#" + temp, "_parent");
        } else {
            App.notifyDanger("Không có nhân viên nào được chọn");
            return;
        }
    };


});

