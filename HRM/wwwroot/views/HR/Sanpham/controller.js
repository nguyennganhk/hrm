﻿var ctxfolder = "/views/HR/SanPham";
app.factory('dataservice', function ($http) {
    var headers = {
        "Content-Type": "application/json;odata=verbose",
        "Accept": "application/json;odata=verbose",
    }
    return {
        insert: function (data, callback) {
            $http.post('/Sanpham/Insert', data).success(callback);
        },
        Update: function (data, callback) {
            $http.post('/Sanpham/Update', data).success(callback);
        },
        deleteItems: function (data, callback) {
            $http.post('/Sanpham/DeleteItems', data).success(callback);
        },
        IsDelete: function (data, callback) {
            $http.post('/Sanpham/IsDelete', data).success(callback);
        },
        getItem: function (data, callback) {
            $http.post('/Sanpham/GetItem', data).success(callback);
        },
        Getlogin: function (callback) {
            $http.get('/ProjectManage/Getlogin').success(callback);
        },
        getReport1: function (data, callback) {
            $http.post('/ProjectManage/getReport1', data).success(callback);
        },
        GetHangSanXuat: function (callback) {
            $http.post('/Sanpham/GetHangSanXuat').success(callback);
        },
        GetGroupSanPham: function (callback) {
            $http.post('/Sanpham/GetGroupSanPham').success(callback);
        },

    }
});
app.filter('fdate', [
    '$filter', function ($filter) {
        return function (input, f) {
            if (input && input.toString().indexOf('Date') > -1) {
                return moment(input).format(f);
            } if (input && input.toString().indexOf('T') > -1) {
                return moment(input).format(f);
            } else return input;
        };
    }
]);
app.directive('format', ['$filter', function ($filter) {
    return {
        require: '?ngModel',
        link: function (scope, elem, attrs, ctrl) {
            if (!ctrl) return;
            ctrl.$formatters.unshift(function (a) {
                return $filter(attrs.format)(ctrl.$modelValue)
            });

            ctrl.$parsers.unshift(function (viewValue) {
                var plainNumber = viewValue.replace(/[^\d|\-+|\.+]/g, '');
                elem.val($filter(attrs.format)(plainNumber));
                return plainNumber;
            });
        }
    };
}]);
app.filter('html', ['$sce', function ($sce) {
    return function (text) {
        return $sce.trustAsHtml(text);
    };
}])
var spassAresSlug = function (number, isview) {
    try {
        // khai bao bien su dung       
        var numbers = number; var length = numbers.length; if (isview) {
            // khi dang la hien thi        
            if (numbers.indexOf(".") != -1) {
                length = numbers.indexOf(".");
                numbers = number.replace(".", ",");
            }
        } else {
            // khi dang la input textbox           
            if (numbers.indexOf(",") != -1) length = numbers.indexOf(",");
        }
        // loai bo nhung ky tu khong can thiet       
        // check so 0         
        var isTrue = true; while (isTrue) {
            if (numbers[0] == "0") numbers = numbers.substr(1, numbers.length);
            else isTrue = false;
        };
        // check dau phay      
        numbers = numbers.split(",");
        var string = "";
        for (var i = 0; i < numbers.length; i++) {
            if (i == 0) string += numbers[i];
            else if (i == 1) string += "," + numbers[i];
            else string += numbers[i];
        } numbers = string;
        // dung chung cho 2 truong hop       
        var no = 3, index = length - no;
        while (index > 0) { numbers = numbers.substr(0, index) + '.' + numbers.substr(index); index -= no; };
        return numbers == "" ? 0 : numbers;
    } catch (ex) { }
    return numbers == "" ? 0 : numbers;
}

app.directive('formatsub', ['$filter', function ($filter) {
    return {
        require: '?ngModel', link: function (scope, elem, attrs, ctrl) {
            if (!ctrl) return; ctrl.$formatters.unshift(function (viewValue) {
                try {
                    plainNumber = viewValue.replace('.', ','); elem.val(spassAresSlug(plainNumber));
                    return spassAresSlug(plainNumber);
                } catch (ex) { return undefined; }
            }); ctrl.$parsers.unshift(function (viewValue) {
                plainNumber = viewValue.replace(/[^-0-9,]/g, '');
                elem.val(spassAresSlug(plainNumber)); return plainNumber.replace(/[,]/g, '.');
            });
        }
    };
}]);

app.controller('Ctrl_ES_CSDL_SanPham', function ($scope, $rootScope, $compile, dataservice) {
    $rootScope.go = function (path) {
        $location.path(path); return false;
    };
    $rootScope.LoginName = $("#loginName").val();
    //$rootScope.DepartmentId = $("#DepartmentId").val();

    dataservice.Getlogin(function (rs) {
        $rootScope.EmployeeLogin = rs;
    });
    dataservice.GetHangSanXuat(function (rs) {
        $rootScope.ListHangSanXuat = rs;
    });
    dataservice.GetGroupSanPham(function (rs) {
        $rootScope.ListGroupSanPham = rs;
    });

    $rootScope.ListIsActive = [
        {
            value: null,
            text: 'Bỏ chọn'
        },
        {
            value: '1',
            text: 'Sử dụng'
        }, {
            value: '0',
            text: 'Không sử dụng'
        }
    ];

    $rootScope.validationOptions = {
        rules: {
            MaSanPham: {
                required: true,
                maxlength: 50
            },
            TenSanPham: {
                required: true,
                maxlength: 250
            }
        },
        messages: {
            MaSanPham: {
                required: "Yêu cầu nhập mã sản phẩm",
                maxlength: "Mã sản phẩm không vượt quá 50 ký tự."
            }, TenSanPham: {
                required: "Yêu cầu nhập tên sản phẩm",
                maxlength: "Tên sản phẩm không vượt quá 50 ký tự."
            }
        }
    }
    $rootScope.TypeData = [{
        value: 1,
        text: 'Khả thi'
    }, {
        value: 0,
        text: 'Phấn đấu'
    }];
    $rootScope.StatusData = [{
        value: 1,
        text: 'Dự án mới'
    }, {
        value: 2,
        text: 'Đang triển khai'
    }
        , {
        value: 3,
        text: 'Hoàn thành'
    }
    ];


    $rootScope.CreateDate = function (date) {
        if (date !== null && date !== undefined) {
            var newdate = new Date(date);
            var month = newdate.getMonth() + 1;
            var day = newdate.getDate();
            var year = newdate.getFullYear();
            var hh = newdate.getHours();
            var mm = newdate.getMinutes();
            if (month < 10)
                month = "0" + month;
            if (day < 10)
                day = "0" + day;
            if (mm < 10)
                mm = "0" + mm;
            return todayDate = day + "/" + month + "/" + year;
        } else {
            return null;
        }
    }

    //0 11/11/2017
    //1 11/11/2017 22:22
    //2 123123123
    //3 type date
    //4 type data year month day
    $rootScope.ConDate = function (data, number) {
        try {
            if (data == null || data == "") {
                return '';
            }
            if (data !== null && data != "" && data != undefined) {
                try {
                    if (data.indexOf("SA") != -1 || data.indexOf("CH") != -1) {
                        if (data.indexOf("SA") != -1) { }
                        if (data.indexOf("CH") != -1) { }
                    }

                    if (data.indexOf('Date') != -1) {
                        data = data.substring(data.indexOf("Date") + 5);
                        data = parseInt(data);
                    }
                }
                catch (ex) { }
                var newdate = new Date(data);
                if (number == 3) {
                    return newdate;
                }
                if (number == 2) {
                    return "/Date(" + newdate.getTime() + ")/";
                }
                var month = newdate.getMonth() + 1;
                var day = newdate.getDate();
                var year = newdate.getFullYear();
                var hh = newdate.getHours();
                var mm = newdate.getMinutes();
                if (month < 10)
                    month = "0" + month;
                if (day < 10)
                    day = "0" + day;
                if (mm < 10)
                    mm = "0" + mm;
                if (number == 0) {
                    return todayDate = day + "/" + month + "/" + year;
                }
                if (number == 1) {
                    return todayDate = day + "/" + month + "/" + year + " " + hh + ":" + mm;
                }
                if (number == 4) {
                    return new Date(year, month - 1, day);
                }
            } else {
                return '';
            }
        } catch (ex) {
            return "";
        }
    }
    //định dạng số xóa dấu chấm phẩy
    $rootScope.addPeriod = function (nStr) {
        if (nStr !== null && nStr !== "" && nStr !== undefined) {
            nStr = Math.round(parseFloat(nStr) * 100) / 100;

            nStr += "";
            if (nStr.indexOf(",") >= 0) {
                var x = nStr.split(",");
            } else {
                var x = nStr.split(".");
            }
            var x1 = x[0];
            var x2 = x.length > 1 && parseInt(x[1]) > 0 ? "," + x[1] : "";
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, "$1" + "." + "$2");
            }
            var Total = x1 + x2;
            return Total;
        }
        else {
            return 0;
        }
    };
    //Thêm phần tinymceOptions
    $rootScope.tinymceOptions = {
        selector: 'textarea',
        plugins: [
            'advlist autolink lists link charmap print preview hr anchor pagebreak image',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
        ],
        skin: 'lightgray', language: 'vi_VN',
        theme: 'modern', height: 70,
        toolbar1: 'codesample | bold italic sizeselect fontselect fontsizeselect | hr alignleft aligncenter alignright alignjustify | bullist numlist outdent indent',
        fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
        //toolbar2: 'print preview insert link image media | forecolor backcolor emoticons | codesample',
        image_advtab: true,

        file_browser_callback: function (field_name, url, type, win) {
            var connector = "/FileManager/Folder/File";
            var enableAutoTypeSelection = true;
            if (enableAutoTypeSelection && type) {
                connector += "&Type=" + type;
            }
            connector += "&Field=" + field_name;
            App.openPage(connector, "File Manager", 1150, 600);
        },
        relative_urls: false,
        remove_script_host: true,
        convert_urls: true
    };   

    $rootScope.YearData = []; $rootScope.MonthData = [];
    //$rootScope.MonthData.push({ value: null, text: "Bỏ chọn" });
    for (var i = 2018; i < 2050; i++) {
        var ob = { value: i, text: 'Năm ' + i.toString() };
        $rootScope.YearData.push(ob);
    }
    for (var i = 1; i <= 12; i++) {
        var ob = { value: i, text: 'Tháng ' + i.toString() };
        $rootScope.MonthData.push(ob);
    }

    $rootScope.ColumnMultiConfig = {
        selectAll: true,
        search: true,
        searchText: 'Tìm kiếm...',
        showTitle: 'true',
        csvSepChar: ',',
        captionFormat: '{0} lựa chọn',
        captionFormatAllSelected: '{0} lựa chọn',
        noMatch: 'Không có dữ liệu "{0}"',
        okCancelInMulti: true,
        locale: ['Đồng ý', 'Hủy', 'Chọn tất cả']
    };
    $rootScope.ColumnConfig = {
        search: true
    };

});

app.config(function ($routeProvider, $validatorProvider) {
    $routeProvider
        .when('/', {
            templateUrl: ctxfolder + '/index.html',
            controller: 'index'
        })
        .when('/edit/:id', {
            templateUrl: ctxfolder + '/edit.html',
            controller: 'edit'
        }).when('/open/:id', {
            templateUrl: ctxfolder + '/open.html',
            controller: 'open'
        })
        .when('/add/', {
            templateUrl: ctxfolder + '/add.html',
            controller: 'add'
        })
    $validatorProvider.setDefaults({
        errorElement: 'span',
        errorClass: 'help-block',
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        success: function (label) {
            label.closest('.form-group').removeClass('has-error');
        }
    });
});
app.controller('index', function ($http, $scope, $rootScope, $compile, $confirm, $uibModal, DTOptionsBuilder, DTColumnBuilder, DTInstances, dataservice, keyhandle, utility) {

    var vm = $scope;
    $scope.selected = [];
    $scope.selectAll = false;
    $scope.toggleAll = toggleAll;
    $scope.toggleOne = toggleOne;
    $scope.staticParam = {
        IsActive: "1",
        Search: "",
        TotalRow: 0,
        startdate: null,
        endate: null,
        lstHangSanXuatGuid: [],
        lstGroupSanPhamGuid: []

    };
    $scope.TotalRow = 0;
    var titleHtml = '<label class="mt-checkbox"><input type="checkbox" ng-model="selectAll" ng-click="toggleAll(selectAll, selected)"/><span></span></label>';
    vm.dtOptions = DTOptionsBuilder.newOptions()
        .withPaginationType('full_numbers').withDOM("t<'table-scrollable't>ip")
        .withDataProp('data').withDisplayLength(12)
        .withOption('order', [2, 'desc'])
        .withOption('serverSide', true)
        .withOption('stateLoadParams', function (settings, data) {
            data.search.search = '';
        })
        .withOption('headerCallback', function (header) {
            if (!$scope.headerCompiled) {
                $scope.headerCompiled = true;
                $compile(angular.element(header).contents())($scope);
            }
        })
        .withOption('rowCallback', rowCallback)
        .withOption('initComplete', function (settings, json) {
        })
        .withOption('createdRow', function (row, data, dataIndex) {
            const contextScope = $scope.$new(true);
            contextScope.data = data;
            $scope.data = data;
            $compile(row)($scope);
            contextScope.contextMenu = $scope.contextMenu;
            $compile(angular.element(row).find('input'))($scope);
            $compile(angular.element(row).attr('context-menu', 'contextMenu'))(contextScope);
        });
    vm.dtOptions.withOption('ajax', {
        url: "/Sanpham/jtable",
        beforeSend: function (jqXHR, settings) {
            App.blockUI({
                target: "#contentMain",
                boxed: true,
                message: 'Đang tải...'
            });
        },
        type: 'POST',
        data: function (d) {
            d.search.value = $scope.staticParam.Search;
            d.StartDate = $scope.staticParam.startdate;
            d.EndDate = $scope.staticParam.endate;
            d.lstHangSanXuatGuid = $scope.staticParam.lstHangSanXuatGuid;
            d.lstGroupSanPhamGuid = $scope.staticParam.lstGroupSanPhamGuid;

            $scope.staticParam.currentPage = $scope.dtInstance.DataTable.page() + 1;
        },
        complete: function (res) {
            $scope.staticParam.TotalRow = res.responseJSON.recordsTotal;
            $scope.totalCountExpired = 0;
            angular.forEach(res.responseJSON.data, function (val, key) {
                $scope.totalCountExpired += val.CountExpired != null && val.CountExpired != "" ? parseFloat(val.CountExpired) : 0;
            });
            App.unblockUI("#contentMain");
           
        }
    }).withOption('serverSide', true);
    vm.dtColumns = [];
    //vm.dtColumns.push(DTColumnBuilder.newColumn("ProjectGuid").withTitle(titleHtml).notSortable()
    //    .renderWith(function (data, type, full, meta) {
    //        $scope.selected[data] = false;
    //        var dm = "selected[\'" + data + "\']";
    //        return '<label class="mt-checkbox"><input type="checkbox" ng-model="' + dm + '" ng-click="toggleOne(selected)"/><span></span></label>';
    //    }).withOption('sWidth', '5px').withOption('sClass', 'tcenter'));
    vm.dtColumns.push(DTColumnBuilder.newColumn('_STT').withTitle('STT').notSortable().withOption('sWidth', '5px').renderWith(function (data, type, full, meta) {
        return ($scope.staticParam.currentPage - 1) * 12 + data;
    }).withOption('sClass', 'tcenter'));
    vm.dtColumns.push(DTColumnBuilder.newColumn('MaSanPham').withTitle('Ảnh').notSortable().withOption('sWidth', '40px').withOption('sClass', 'tcenter').renderWith(function (data, type, full, meta) {
        if (full.Title1 !== null && full.Title1 !== "") {
            return '<img width="30px" height = "35px" src="' + "/SanPham/GetPic/" + full.RowGuid + '" />';

        } else {           
                return '<img width="30px" height = "35px" src="/images/user.jpg" />';
            
        }

    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('MaSanPham').withTitle('Mã sản phẩm').withOption('sWidth', '80px').renderWith(function (data, type, full, meta) {
        var open = "open(\'" + full.RowGuid + "\')";
        return '<a href="" ng-click="' + open + '" >' + full.MaSanPham + '</a>';
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('TenSanPham').withTitle('Tên sản phẩm').renderWith(function (data, type, full, meta) {
        return full.TenSanPham;
    }));

    vm.dtColumns.push(DTColumnBuilder.newColumn('DonGia').withTitle('Đơn giá').withOption('sWidth', '60px').renderWith(function (data, type, full, meta) {
        return $rootScope.addPeriod(full.DonGia);
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('TenHang').withTitle('Hãng sản xuất').withOption('sWidth', '100px').renderWith(function (data, type, full, meta) {
        return full.TenHang;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('TenNhom').withTitle('Nhóm sản phẩm').withOption('sWidth', '100px').renderWith(function (data, type, full, meta) {
        return full.TenNhom;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('Giamgia').withTitle('Giảm giá').withOption('sWidth', '60px').renderWith(function (data, type, full, meta) {
        return $rootScope.addPeriod(full.Giamgia);
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('MetaTitle').withTitle('Tiêu đề thẻ').withOption('sWidth', '160px').renderWith(function (data, type, full, meta) {
        return full.MetaTitle;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('Location').withTitle('Hiển thị trang chủ').withOption('sWidth', '60px').withOption('sClass', 'tcenter').renderWith(function (data, type, full, meta) {
        if (full.Location === "True") {
            return '<label class="mt-checkbox"><input type="checkbox" checked  /><span></span></label>';
        } else {
            return '<label class="mt-checkbox"><input type="checkbox" ng-disabled="true" /><span></span></label>';

        }
    }));
    vm.reloadData = reloadData;
    vm.dtInstance = {};
    function reloadData(resetPaging) {
        $scope.staticParam.currentPage = resetPaging;
        vm.dtInstance.reloadData(callback, resetPaging);
    }
    function callback(json) {

    }
    function toggleAll(selectAll, selectedItems) {
        for (var id in selectedItems) {
            if (selectedItems.hasOwnProperty(id)) {
                selectedItems[id] = selectAll;
            }
        }
    }
    function toggleOne(selectedItems) {
        for (var id in selectedItems) {
            if (selectedItems.hasOwnProperty(id)) {
                if (!selectedItems[id]) {
                    vm.selectAll = false;
                    return;
                }
            }
        }
        vm.selectAll = true;
    }
    function rowCallback(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        $('td', nRow).unbind('dblclick');
        $('td', nRow).bind('dblclick', function () {
            $scope.$apply(function () {
                //$scope.open(aData.ProjectGuid);
            });
        });
        return nRow;
    }

    var date = new Date();


    $scope.$watch('staticParam.startdate', function (newvalue, oldvalue) {
        if (newvalue !== undefined && newvalue !== oldvalue) {
            $rootScope.reload();
        }
    });
    $scope.$watch('staticParam.endate', function (newvalue, oldvalue) {
        if (newvalue !== undefined && newvalue !== oldvalue) {
            $rootScope.reload();
        }
    });
    $scope.Status = 0;
    $scope.Type = 1;


    $scope.getReportResult = function (type) {
        $scope.Type = type;
        var ob = {
            DepartmentId: $rootScope.DepartmentId,
            StartDate: $scope.staticParam.startdate,
            EndDate: $scope.staticParam.endate,
            Type: type
        }
        dataservice.getReportResult(ob, function (rs) {
            $scope.lstdata = []; $scope.lstdata1 = []; $scope.TotalSLDA = 0; $scope.TotalSL = 0;
            $scope.TotalKH1 = 0; $scope.TotalKH2 = 0;
            $scope.TotalKH3 = 0; $scope.TotalKH4 = 0;
            $scope.TotalKH5 = 0; $scope.TotalKH = 0;
            angular.forEach(rs, function (value, key) {
                if (type === 1) {
                    if (value.SLDA > 0) {
                        value.PTHT = (value.HT / value.SLDA) * 100;
                        value.PTKHT = (value.KHT / value.SLDA) * 100;
                    } else {
                        value.PTHT = 0; value.PTKHT = 0;
                    }
                    var obj1 = [key, value.Note, value.SLDA, value.HT, value.KHT, value.PTHT, value.PTKHT];
                    $scope.TotalKH1 += value.SLDA;
                    $scope.TotalKH2 += value.HT;
                    $scope.TotalKH3 += value.KHT;
                    $scope.TotalKH4 += value.PTHT;
                    $scope.TotalKH5 += value.PTKHT;
                    $scope.lstdata1.push(obj1);
                }
                else if (type === 2) {
                    if (value.SLCVc1 > 0) {
                        value.PTHT = (value.HT / value.SLCVc1) * 100;
                        value.PTKHT = (value.KHT / value.SLCVc1) * 100;
                    } else {
                        value.PTHT = 0; value.PTKHT = 0;
                    }
                    var obj1 = [key, value.FullName, value.SLDA, value.SLCVc1, value.HT, value.KHT, value.PTHT, value.PTKHT];
                    $scope.TotalKH1 += value.SLDA;
                    $scope.TotalKH += value.SLCVc1;
                    $scope.TotalKH2 += value.HT;
                    $scope.TotalKH3 += value.KHT;
                    $scope.TotalKH4 += value.PTHT;
                    $scope.TotalKH5 += value.PTKHT;
                    $scope.lstdata1.push(obj1);
                }
            })
            if (type === 1) {
                $scope.jxcelAddDetailInitkq1.data = $scope.lstdata1;
                $('#myhtindexkq1 #colfoot-2').html($rootScope.addPeriod($scope.TotalKH1));
                $('#myhtindexkq1 #colfoot-3').html($rootScope.addPeriod($scope.TotalKH2));
                $('#myhtindexkq1 #colfoot-4').html($rootScope.addPeriod($scope.TotalKH3));
                $('#myhtindexkq1 #colfoot-5').html($rootScope.addPeriod($scope.TotalKH4));
                $('#myhtindexkq1 #colfoot-6').html($rootScope.addPeriod($scope.TotalKH5));
            } else if (type === 2) {
                $scope.jxcelAddDetailInitkq2.data = $scope.lstdata1;
                $('#myhtindexkq1 #colfoot-2').html($rootScope.addPeriod($scope.TotalKH1));
                $('#myhtindexkq1 #colfoot-3').html($rootScope.addPeriod($scope.TotalKH));
                $('#myhtindexkq1 #colfoot-4').html($rootScope.addPeriod($scope.TotalKH3));
                $('#myhtindexkq1 #colfoot-5').html($rootScope.addPeriod($scope.TotalKH4));
                $('#myhtindexkq1 #colfoot-6').html($rootScope.addPeriod($scope.TotalKH5));
            }
        })
    }
   
    $scope.reloadALL = function () {
        $scope.staticParam.Search = "";
        $scope.Status = 0;
        $scope.selectAll = false;
        reloadData(true);
    };
    $rootScope.reload = function () {
        reloadData(true);
    };

    $scope.add = function () {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/add.html',
            controller: 'add',
            backdrop: 'static',
            keyboard: false,
            size: '100'
        });
        modalInstance.result.then(function (d) {
            $scope.reload();
        }, function () {
        });
    }
    $scope.edit = function (Id) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/edit.html',
            controller: 'edit',
            backdrop: 'static',
            keyboard: false,
            size: '100',
            resolve: {
                para: function () {
                    return Id;
                }
            }
        });
        modalInstance.result.then(function (d) {
            $scope.reload();
        }, function () {
        });
    }
    $scope.open = function (Id) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/open.html',
            controller: 'open',
            backdrop: 'static',
            keyboard: false,
            size: '100',
            resolve: {
                para: function () {
                    return Id;
                }
               
            }
        });
        modalInstance.result.then(function (d) {
            $scope.reload();
        }, function () {
        });
    }
    $scope.delete = function (Id) {
        dataservice.GetC2({ IdS: [Id] }, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {
                $confirm({ text: 'Bạn có chắc chắn xóa ' + rs.Note + ' không ?', title: 'Xác nhận', ok: 'Đồng ý', cancel: ' Đóng' })
                    .then(function () {
                        App.blockUI({
                            target: "#contentMain",
                            boxed: true,
                            message: 'Đang tải...'
                        });
                        dataservice.IsDelete({ IdS: [Id] }, function (result) {
                            if (result.Error) {
                                App.notifyDanger(result.Title);
                            } else {
                                App.notifyInfo(result.Title);
                                $rootScope.reload();
                            }
                            App.unblockUI("#contentMain");
                        });
                    });
            }
        })
    }
    $scope.deleteChecked = function () {
        var deleteItems = [];
        for (var id in $scope.selected) {
            if ($scope.selected.hasOwnProperty(id)) {
                if ($scope.selected[id]) {
                    deleteItems.push(id);
                }
            }
        }
        if (deleteItems.length > 0) {
            $confirm({ text: 'Bạn có chắc chắn muốn xóa các khoản mục đã chọn?', title: 'Xác nhận', ok: 'Đồng ý', cancel: ' Đóng' })
                .then(function () {
                    App.blockUI({
                        target: "#contentMain",
                        boxed: true,
                        message: 'Đang tải...'
                    });
                    dataservice.deleteItems(deleteItems, function (result) {
                        if (result.Error) {
                            App.notifyDanger(result.Title);
                        } else {
                            App.notifyInfo(result.Title);
                            $scope.reload();
                        }
                        App.unblockUI("#contentMain");
                    });

                });
        } else {
            App.notifyDanger("Không có khoản mục nào được chọn");
        }
    }

    $scope.lstHangSanXuatGuidChange = function (data) {
        
        $scope.staticParam.lstHangSanXuatGuid = data;
        if ($scope.staticParam.lstHangSanXuatGuid.includes("null")) {
            $scope.staticParam.lstHangSanXuatGuid.splice(0,1);
        }
        $rootScope.reload();
    }
    $scope.lstGroupSanPhamGuidChange = function (data) {
        $scope.staticParam.lstGroupSanPhamGuid = data;
        if ($scope.staticParam.lstGroupSanPhamGuid.includes("null")) {
            $scope.staticParam.lstGroupSanPhamGuid.splice(0, 1);
        }
        $rootScope.reload();
    }


    $scope.contextMenu = [
        [function ($itemScope) {
            return '<i class="mdi mdi-eye"></i> Xem';
        }, function ($itemScope, $event, model) {
                $scope.open($itemScope.data.RowGuid);
        }, function ($itemScope, $event, model) {
            return App.Permissions.OPEN;
        }],
        [function ($itemScope) {
            return '<i class="mdi mdi-table-edit"></i> Sửa';
        }, function ($itemScope, $event, model) {          
                $scope.edit($itemScope.data.RowGuid);           
        }, function ($itemScope, $event, model) {
            return App.Permissions.EDIT;
        }],
        [function ($itemScope) {
            return '<i class="mdi mdi-delete"></i> Xóa';
        }, function ($itemScope, $event, model) {          
                $scope.delete($itemScope.data.RowGuid);            
        }, function ($itemScope, $event, model) {
            return App.Permissions.DELETE;
        }]
    ];
});

app.controller('add', function ($scope, $rootScope, $compile, $uibModal, $confirm, $uibModalInstance, FileUploader, dataservice) {
    $scope.Title = "Thêm sản phẩm";
    $scope.model = {
        imageLink: "/images/male.jpeg",
        Location: false
    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    }
    $scope.model.FileAttachments = [];
    var uploader = $scope.uploader = new FileUploader({
        // url: App.baseService() + '/upload'
    });
    uploader.filters.push({
        name: "customFilter",
        fn: function (item /*{File|FileLikeObject}*/, options) {
            return this.queue.length < 10;
        }
    });
    uploader.onSuccessItem = function (fileItem, response, status, headers) {
        $scope.jmodel.FileAttachments.push(response);
    };
    $scope.jmodelPer = {};
    uploader.onCompleteAll = function () {
        $scope.commit();
    };
    $scope.newImage = function () {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#image').attr('src', e.target.result);
        }
        reader.readAsDataURL($("#loadImage").prop('files')[0]);
        $scope.changeImage = true;
    }
    $scope.newImage2 = function () {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#image2').attr('src', e.target.result);
        }
        reader.readAsDataURL($("#loadImage2").prop('files')[0]);
        $scope.changeImage = true;
    }
    $scope.newImage3 = function () {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#image3').attr('src', e.target.result);
        }
        reader.readAsDataURL($("#loadImage3").prop('files')[0]);
        $scope.changeImage = true;
    }
    $scope.newImage4 = function () {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#image4').attr('src', e.target.result);
        }
        reader.readAsDataURL($("#loadImage4").prop('files')[0]);
        $scope.changeImage = true;
    }
    $scope.GroupSanPhamGuidChange = function (data) {
        $scope.model.GroupSanPhamGuid = data;
    }
    $scope.HangSanXuatGuidChange = function (data) {
        $scope.model.HangSanXuatGuid = data;
    }
    $scope.submit = function () {

        if ($scope.addform.validate()) {
            var fd = new FormData();
            var imageTemp = $("#loadImage").prop('files')[0];
            var imageTemp2 = $("#loadImage2").prop('files')[0];
            var imageTemp3 = $("#loadImage3").prop('files')[0];
            var imageTemp4 = $("#loadImage4").prop('files')[0];
            if (imageTemp !== undefined) {
                fd.append("1#" + imageTemp.name, imageTemp);
            }
            if (imageTemp2 !== undefined) {
                fd.append("2#" + imageTemp2.name, imageTemp2);
            }
            if (imageTemp3 !== undefined) {
                fd.append("3#" + imageTemp3.name, imageTemp3);
            }
            if (imageTemp4 !== undefined) {
                fd.append("4#" + imageTemp4.name, imageTemp4);
            }
            if (uploader.queue.length > 0) {
                for (i = 0; i < uploader.queue.length; i++) {
                    var files = uploader.queue[i]._file;
                    if (uploader.queue[i].file.name === undefined) {
                        uploader.queue[i].file.name = "";
                    }
                    if (uploader.queue[i].SortOrder != "" && uploader.queue[i].SortOrder != null && uploader.queue[i].SortOrder != undefined) {
                        fd.append(uploader.queue[i].SortOrder + "#" + uploader.queue[i].file.FileName + "#" + uploader.queue[i].file.IsReadonly, files);
                    } else {
                        fd.append("0#" + uploader.queue[i].file.FileName + "#false", files);
                    }
                }
            }
            App.blockUI({
                target: "#table_load",
                boxed: true,
                message: 'Đang tải...'
            });

            fd.append('Insert', JSON.stringify($scope.model));

            $.ajax({
                url: '/Sanpham/insert',
                data: fd,
                processData: false,
                contentType: false,
                type: 'POST',
                success: function (rs) {
                    if (rs.Error) {
                        App.unblockUI("#table_load");
                        App.notifyDanger(rs.Title);
                    } else {
                        App.notifyInfo(rs.Title);
                        $rootScope.reload();
                        App.unblockUI("#table_load");
                        $uibModalInstance.close();
                    }
                },
                error: function (rs) {
                    App.notifyInfo(rs.Title);
                }
            });


        }
    }

});
app.controller('edit', function ($scope, $rootScope, $compile, $uibModal, $confirm, $uibModalInstance, FileUploader, dataservice, para) {
    $scope.Title = "Sửa sản phẩm";
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    }
    $scope.model = { };
  
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    }
    $scope.model.FileAttachments = [];
    var uploader = $scope.uploader = new FileUploader({
        // url: App.baseService() + '/upload'
    });
    uploader.filters.push({
        name: "customFilter",
        fn: function (item /*{File|FileLikeObject}*/, options) {
            return this.queue.length < 10;
        }
    });
    uploader.onSuccessItem = function (fileItem, response, status, headers) {
        $scope.jmodel.FileAttachments.push(response);
    };
    $scope.jmodelPer = {};
    uploader.onCompleteAll = function () {
        $scope.commit();
    };
    $scope.newImage = function () {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#image').attr('src', e.target.result);
        }
        reader.readAsDataURL($("#loadImage").prop('files')[0]);
        $scope.changeImage = true;
    }
    $scope.newImage2 = function () {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#image2').attr('src', e.target.result);
        }
        reader.readAsDataURL($("#loadImage2").prop('files')[0]);
        $scope.changeImage = true;
    }
    $scope.newImage3 = function () {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#image3').attr('src', e.target.result);
        }
        reader.readAsDataURL($("#loadImage3").prop('files')[0]);
        $scope.changeImage = true;
    }
    $scope.newImage4 = function () {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#image4').attr('src', e.target.result);
        }
        reader.readAsDataURL($("#loadImage4").prop('files')[0]);
        $scope.changeImage = true;
    }
    $scope.GroupSanPhamGuidChange = function (data) {
        $scope.model.GroupSanPhamGuid = data;
    }
    $scope.HangSanXuatGuidChange = function (data) {
        $scope.model.HangSanXuatGuid = data;
    }
    dataservice.getItem({ IdS: [para] }, function (rs) {
        if (rs.Error) {
            App.notifyDanger(rs.Title);
        } else {
            $scope.model = rs.SanPhams;
            $scope.AnhSanPhamViews = rs.AnhSanPhamViews;
            if ($scope.AnhSanPhamViews.Title1 !== null && $scope.AnhSanPhamViews.Title1 !== "") {
                $scope.model.Title1 = "/SanPham/GetPic/" + $scope.model.RowGuid;
            } else {
                $scope.model.Title1 = "/images/user.jpg";
            }
            if ($scope.AnhSanPhamViews.Title2 !== null && $scope.AnhSanPhamViews.Title2 !== "") {
                $scope.model.Title2 = "/SanPham/GetPic2/" + $scope.model.RowGuid;
            } else {
                $scope.model.Title2 = "/images/user.jpg";
            }
            if ($scope.AnhSanPhamViews.Title3 !== null && $scope.AnhSanPhamViews.Title3 !== "") {
                $scope.model.Title3 = "/SanPham/GetPic3/" + $scope.model.RowGuid;
            } else {
                $scope.model.Title3 = "/images/user.jpg";
            }
            if ($scope.AnhSanPhamViews.Title4 !== null && $scope.AnhSanPhamViews.Title4 !== "") {
                $scope.model.Title4 = "/SanPham/GetPic4/" + $scope.model.RowGuid;
            } else {
                $scope.model.Title4 = "/images/user.jpg";
            }
        }
    })
    $scope.removeImage = [];
    $scope.clearFile1 = function () {      
            $('#image').val(undefined);
            $('#loadImage').val(undefined);
        $scope.model.Title1 = "/images/user.jpg";
        if (!$scope.removeImage.includes("1")) {
            $scope.removeImage.push("1");
        }
        
    }
    $scope.clearFile2 = function () {
        $('#image2').val(undefined);
        $('#loadImage2').val(undefined);
        $scope.model.Title2 = "/images/user.jpg";
        if (!$scope.removeImage.includes("2")) {
            $scope.removeImage.push("2");
        }
    }
    $scope.clearFile3 = function () {
        $('#image3').val(undefined);
        $('#loadImage3').val(undefined);
        $scope.model.Title3 = "/images/user.jpg";
        if (!$scope.removeImage.includes("3")) {
            $scope.removeImage.push("3");
        }
    }
    $scope.clearFile4 = function () {
        $('#image4').val(undefined);
        $('#loadImage4').val(undefined);
        $scope.model.Title4 = "/images/user.jpg";
        if (!$scope.removeImage.includes("4")) {
            $scope.removeImage.push("4");
        }
    }
    $scope.submit = function () {
        if ($scope.addform.validate()) {
        var fd = new FormData();
        var imageTemp = $("#loadImage").prop('files')[0];
        var imageTemp2 = $("#loadImage2").prop('files')[0];
        var imageTemp3 = $("#loadImage3").prop('files')[0];
        var imageTemp4 = $("#loadImage4").prop('files')[0];
        if (imageTemp !== undefined) {
            fd.append("1#" + imageTemp.name, imageTemp);
        }
        if (imageTemp2 !== undefined) {
            fd.append("2#" + imageTemp2.name, imageTemp2);
        }
        if (imageTemp3 !== undefined) {
            fd.append("3#" + imageTemp3.name, imageTemp3);
        }
        if (imageTemp4 !== undefined) {
            fd.append("4#" + imageTemp4.name, imageTemp4);
        }
        if (uploader.queue.length > 0) {
            for (i = 0; i < uploader.queue.length; i++) {
                var files = uploader.queue[i]._file;
                if (uploader.queue[i].file.name === undefined) {
                    uploader.queue[i].file.name = "";
                }
                if (uploader.queue[i].SortOrder != "" && uploader.queue[i].SortOrder != null && uploader.queue[i].SortOrder != undefined) {
                    fd.append(uploader.queue[i].SortOrder + "#" + uploader.queue[i].file.FileName + "#" + uploader.queue[i].file.IsReadonly, files);
                } else {
                    fd.append("0#" + uploader.queue[i].file.FileName + "#false", files);
                }
            }
        }
        App.blockUI({
            target: "#table_load",
            boxed: true,
            message: 'Đang tải...'
        });

            fd.append('Insert', JSON.stringify($scope.model));
            fd.append('removeImage', JSON.stringify($scope.removeImage));
            $.ajax({
                url: '/Sanpham/Update',
                data: fd,
                processData: false,
                contentType: false,
                type: 'POST',
                success: function (rs) {
                    if (rs.Error) {
                        App.unblockUI("#table_load");
                        App.notifyDanger(rs.Title);
                    } else {
                        App.notifyInfo(rs.Title);
                        $rootScope.reload();
                        App.unblockUI("#table_load");
                        $uibModalInstance.close();
                    }
                },
                error: function (rs) {
                    App.notifyInfo(rs.Title);
                }
            });
        }
    }


});
app.controller('open', function ($scope, $rootScope, $compile, $uibModal, $confirm, $uibModalInstance, dataservice, para) {
    $scope.Title = "Xem sản phẩm";
    $scope.model = {};
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    }
   
    dataservice.getItem({ IdS: [para] }, function (rs) {
        if (rs.Error) {
            App.notifyDanger(rs.Title);
        } else {
            $scope.model = rs.SanPhams;
            $scope.AnhSanPhamViews = rs.AnhSanPhamViews;
            if ($scope.AnhSanPhamViews.Title1 !== null && $scope.AnhSanPhamViews.Title1 !== "") {
                $scope.model.Title1 = "/SanPham/GetPic/" + $scope.model.RowGuid;
            } else {
                $scope.model.Title1 = "/images/user.jpg";
            }
            if ($scope.AnhSanPhamViews.Title2 !== null && $scope.AnhSanPhamViews.Title2 !== "") {
                $scope.model.Title2 = "/SanPham/GetPic2/" + $scope.model.RowGuid;
            } else {
                $scope.model.Title2 = "/images/user.jpg";
            }
            if ($scope.AnhSanPhamViews.Title3 !== null && $scope.AnhSanPhamViews.Title3 !== "") {
                $scope.model.Title3 = "/SanPham/GetPic3/" + $scope.model.RowGuid;
            } else {
                $scope.model.Title3 = "/images/user.jpg";
            }
            if ($scope.AnhSanPhamViews.Title4 !== null && $scope.AnhSanPhamViews.Title4 !== "") {
                $scope.model.Title4 = "/SanPham/GetPic4/" + $scope.model.RowGuid;
            } else {
                $scope.model.Title4 = "/images/user.jpg";
            }
        }
    })



});
//Thêm mới ND cấp 2
app.controller('add_NDC2', function ($scope, $rootScope, $compile, $uibModal, $confirm, $uibModalInstance, dataservice, para, para1, data, FileUploader) {
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
    $scope.Title = "Nội dung cấp 2";
    $scope.modeljexcel = { Jexcelc2: {}, Jexcelc2read: {} };
    $scope.openAttach = para1;
    //Cấu hình file đính kèm
    var uploader = $scope.uploader = new FileUploader({
        // url: App.baseService() + '/upload'
    });
    uploader.filters.push({
        name: "customFilter",
        fn: function (item /*{File|FileLikeObject}*/, options) {
            if ($scope.uploader.queue.length === 10) {
                App.notifyDanger("Không vượt quá 10 file đính kèm");
                return;
            } else
                if (parseFloat(item.size / 1024 / 1024) <= 10) {
                    return this.queue.length < 10;
                } else {
                    App.notifyDanger("File đính kèm không vượt quá 10 MB");
                }

        }
    });
    uploader.onCompleteAll = function () {
    };

    $scope.rowchange = function (obj, row, cell, columnName) {
        var rowindex = parseInt(row); // dòng muốn thay đổi              
        $scope.ListData = $scope.modeljexcel.Jexcelc2.jexcel('getData');
    }
    $scope.ListDetailDelete = [];
    $scope.deletees = function (element, item) {
        $scope.ListData = $('#myhtndc2').jexcel('getData');
        if ($scope.ListData.length > 1) {
            var cell = 'A' + (parseInt(item) + 1);
            var id = $('#myhtndc2').jexcel('getValue', cell);
            if (id !== "" && id !== null) {
                var sta = false;
                for (var i = 0; i < $scope.ListDetailDelete.length; i++) {
                    if ($scope.ListDetailDelete[i] === id) {
                        sta = true;
                    }
                }
                if (!sta) {
                    $scope.ListDetailDelete.push(id);
                }
            }
            $('#myhtndc2').jexcel('deleteRow', item);
            App.notifyInfo('Xóa thành công');
        }
        else {
            App.notifyDanger('Không xóa được dòng cuối cùng này !');
            return;
        }

    };

    $scope.jxcel = {
        deleterowconfirm: $scope.deletees,
        dataObject: true,
        reloadData: {},
        data: [['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '']],
        colHeaders: ['id', 'NỘI DUNG CẤP 2', 'BẮT ĐẦU', 'KẾT THÚC', 'BAN KT', 'NỘI DUNG PH-KT', 'KẾT THÚC-KT', 'BAN TMQT', 'NỘI DUNG PH-TMQT', 'KẾT THÚC-TMQT', 'NỘI DUNG PH-B.TCKT', 'KẾT THÚC-TCKT', 'TÊN KH', 'NỘI DUNG PH-KH', 'KẾT THÚC-KH', 'PHÂN LOẠI CQNN', 'TÊN CQNN', 'NỘI DUNG PH-CQNN', 'KẾT THÚC-CQNN', 'TÊN TP', 'NỘI DUNG PH-TP', 'KẾT THÚC-TP', 'PHÂN LOẠI TV', 'TÊN TV', 'NỘI DUNG PH-TV', 'KẾT THÚC-TV', 'TÊN KB', 'KẾT THÚC-KB', 'NỘI DUNG PH-KB'],
        colTitles: ['', 'NỘI DUNG CẤP 2', 'BẮT ĐẦU', 'KẾT THÚC', 'BAN KT', 'NỘI DUNG PH-KT', 'KẾT THÚC-KT', 'BAN TMQT', 'NỘI DUNG PH-TMQT', 'KẾT THÚC-TMQT', 'NỘI DUNG PH-B.TCKT', 'KẾT THÚC-TCKT', 'TÊN KH', 'NỘI DUNG PH-KH', 'KẾT THÚC-KH', 'PHÂN LOẠI CQNN', 'TÊN CQNN', 'NỘI DUNG PH-CQNN', 'KẾT THÚC-CQNN', 'TÊN TP', 'NỘI DUNG PH-TP', 'KẾT THÚC-TP', 'PHÂN LOẠI TV', 'TÊN TV', 'NỘI DUNG PH-TV', 'KẾT THÚC-TV', 'TÊN KB', 'KẾT THÚC-KB', 'NỘI DUNG PH-KB'],
        colFooters: ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''],
        columnShow: [false, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true],
        columns: [
            {
                name: 'Id',
                type: 'text'
            },
            { name: 'Note', type: 'text' },

            { name: 'StartDate', type: 'calendar', options: { format: 'DD/MM/YYYY' } },
            { name: 'EndDate', type: 'calendar', options: { format: 'DD/MM/YYYY' } },
            {
                name: 'Bkt', type: 'autocomplete', // đơn vị tính
                url: '/Project/ProjectManage/GetKPBkt',
            },
            { name: 'NotePhkt', type: 'text' },
            { name: 'EndDateKt', type: 'calendar', options: { format: 'DD/MM/YYYY' } },
            {
                name: 'Btmqt', type: 'autocomplete', // đơn vị tính
                url: '/Project/ProjectManage/GetBtmqt',
            },
            { name: 'NotePhtmqt', type: 'text' },
            { name: 'EndDateTmqt', type: 'calendar', options: { format: 'DD/MM/YYYY' } },
            { name: 'NoteBtckt', type: 'text' },
            { name: 'EndDateBtckt', type: 'calendar', options: { format: 'DD/MM/YYYY' } },
            { name: 'NameKh', type: 'text' },
            { name: 'NoteKh', type: 'text' },
            { name: 'EndDateKh', type: 'calendar', options: { format: 'DD/MM/YYYY' } },
            {
                name: 'ClassifyCqnn', type: 'autocomplete', // đơn vị tính
                url: '/Project/ProjectManage/GetKPCQNN',
            },
            { name: 'NameCqnn', type: 'text' },
            { name: 'NotePhcqnn', type: 'text' },
            { name: 'EndDateCqnn', type: 'calendar', options: { format: 'DD/MM/YYYY' } },
            { name: 'NameTp', type: 'text' },
            { name: 'NotePhtp', type: 'text' },
            { name: 'EndDateTp', type: 'calendar', options: { format: 'DD/MM/YYYY' } },
            {
                name: 'ClassifyTv', type: 'autocomplete', // đơn vị tính
                url: '/Project/ProjectManage/GetPLTV',
            },
            { name: 'NameTv', type: 'text' },
            { name: 'NotePhtv', type: 'text' },
            { name: 'EndDateTv', type: 'calendar', options: { format: 'DD/MM/YYYY' } },
            {
                name: 'NameKb', type: 'autocomplete', // đơn vị tính
                url: '/Project/ProjectManage/GetTreasury',
            },
            { name: 'EndDateKb', type: 'calendar', options: { format: 'DD/MM/YYYY' } },
            { name: 'NotePhkb', type: 'text' },



        ],
        initDataRows: ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''],
        colWidths: [100, 350, 100, 100, 100, 350, 100, 90, 350, 100, 350, 100, 200, 350, 100, 90, 90, 350, 100, 100, 350, 100, 100, 100, 350, 90, 150, 90, 350],
        allrowevent: true,
        tableOverflow: true,
        csvHeaders: true,
        tableHeight: '200px',
        changeCell: $scope.rowchange
    };
    $scope.jxcelread = {
        dataObject: true,
        reloadData: {},
        data: [['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '']],
        colHeaders: ['id', 'NỘI DUNG CẤP 2', 'BẮT ĐẦU', 'KẾT THÚC', 'BAN KT', 'NỘI DUNG PH-KT', 'KẾT THÚC-KT', 'BAN TMQT', 'NỘI DUNG PH-TMQT', 'KẾT THÚC-TMQT', 'NỘI DUNG PH-B.TCKT', 'KẾT THÚC-TCKT', 'TÊN KH', 'NỘI DUNG PH-KH', 'KẾT THÚC-KH', 'PHÂN LOẠI CQNN', 'TÊN CQNN', 'NỘI DUNG PH-CQNN', 'KẾT THÚC-CQNN', 'TÊN TP', 'NỘI DUNG PH-TP', 'KẾT THÚC-TP', 'PHÂN LOẠI TV', 'TÊN TV', 'NỘI DUNG PH-TV', 'KẾT THÚC-TV', 'TÊN KB', 'KẾT THÚC-KB', 'NỘI DUNG PH-KB'],
        colTitles: ['', 'NỘI DUNG CẤP 2', 'BẮT ĐẦU', 'KẾT THÚC', 'BAN KT', 'NỘI DUNG PH-KT', 'KẾT THÚC-KT', 'BAN TMQT', 'NỘI DUNG PH-TMQT', 'KẾT THÚC-TMQT', 'NỘI DUNG PH-B.TCKT', 'KẾT THÚC-TCKT', 'TÊN KH', 'NỘI DUNG PH-KH', 'KẾT THÚC-KH', 'PHÂN LOẠI CQNN', 'TÊN CQNN', 'NỘI DUNG PH-CQNN', 'KẾT THÚC-CQNN', 'TÊN TP', 'NỘI DUNG PH-TP', 'KẾT THÚC-TP', 'PHÂN LOẠI TV', 'TÊN TV', 'NỘI DUNG PH-TV', 'KẾT THÚC-TV', 'TÊN KB', 'KẾT THÚC-KB', 'NỘI DUNG PH-KB'],
        colFooters: ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''],
        columnShow: [false, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true],
        columns: [
            {
                name: 'Id',
                type: 'text', readOnly: true
            },
            { name: 'Note', type: 'text', readOnly: true },

            { name: 'StartDate', type: 'calendar', options: { format: 'DD/MM/YYYY' }, readOnly: true },
            { name: 'EndDate', type: 'calendar', options: { format: 'DD/MM/YYYY' }, readOnly: true },
            {
                name: 'Bkt', type: 'autocomplete', // đơn vị tính
                url: '/Project/ProjectManage/GetKPBkt', readOnly: true
            },
            { name: 'NotePhkt', type: 'text', readOnly: true },
            { name: 'EndDateKt', type: 'calendar', options: { format: 'DD/MM/YYYY' }, readOnly: true },
            {
                name: 'Btmqt', type: 'autocomplete', // đơn vị tính
                url: '/Project/ProjectManage/GetBtmqt', readOnly: true
            },
            { name: 'NotePhtmqt', type: 'text', readOnly: true },
            { name: 'EndDateTmqt', type: 'calendar', options: { format: 'DD/MM/YYYY' }, readOnly: true },
            { name: 'NoteBtckt', type: 'text', readOnly: true },
            { name: 'EndDateBtckt', type: 'calendar', options: { format: 'DD/MM/YYYY' }, readOnly: true },
            { name: 'NameKh', type: 'text', readOnly: true },
            { name: 'NoteKh', type: 'text', readOnly: true },
            { name: 'EndDateKh', type: 'calendar', options: { format: 'DD/MM/YYYY' }, readOnly: true },
            {
                name: 'ClassifyCqnn', type: 'autocomplete', // đơn vị tính
                url: '/Project/ProjectManage/GetKPCQNN', readOnly: true
            },
            { name: 'NameCqnn', type: 'text', readOnly: true },
            { name: 'NotePhcqnn', type: 'text', readOnly: true },
            { name: 'EndDateCqnn', type: 'calendar', options: { format: 'DD/MM/YYYY' }, readOnly: true },
            { name: 'NameTp', type: 'text', readOnly: true },
            { name: 'NotePhtp', type: 'text', readOnly: true },
            { name: 'EndDateTp', type: 'calendar', options: { format: 'DD/MM/YYYY' }, readOnly: true },
            {
                name: 'ClassifyTv', type: 'autocomplete', // đơn vị tính
                url: '/Project/ProjectManage/GetPLTV', readOnly: true
            },
            { name: 'NameTv', type: 'text', readOnly: true },
            { name: 'NotePhtv', type: 'text', readOnly: true },
            { name: 'EndDateTv', type: 'calendar', options: { format: 'DD/MM/YYYY' }, readOnly: true },
            {
                name: 'NameKb', type: 'autocomplete', // đơn vị tính
                url: '/Project/ProjectManage/GetTreasury', readOnly: true
            },
            { name: 'EndDateKb', type: 'calendar', options: { format: 'DD/MM/YYYY' }, readOnly: true },
            { name: 'NotePhkb', type: 'text', readOnly: true },

        ],
        initDataRows: ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''],
        colWidths: [100, 350, 100, 100, 100, 350, 100, 90, 350, 100, 350, 100, 200, 350, 100, 90, 90, 350, 100, 100, 350, 100, 100, 100, 350, 90, 150, 90, 350],
        allrowevent: true,
        tableOverflow: true,
        csvHeaders: true,
        tableHeight: '200px',
        allowInsertRow: false,
        contextMenu: function () { return ""; },
        changeCell: $scope.rowchange
    };
    //Lấy danh sách đính kèm
    if (para.Id !== "" && para.Id !== undefined) {
        dataservice.GetItemC2({ IdS: [para.Id] }, function (rs) {
            if (rs.length > 0) {
                $scope.lstdata = [];
                angular.forEach(rs, function (value, key) {
                    var num = key + 1;
                    var StartDate = value.StartDate !== null && value.StartDate !== "" ? moment(value.StartDate).format("YYYY-MM-DD") : null;
                    var EndDate = value.EndDate !== null && value.EndDate !== "" ? moment(value.EndDate).format("YYYY-MM-DD") : null;
                    var EndDateKt = value.EndDateKt !== null && value.EndDateKt !== "" ? moment(value.EndDateKt).format("YYYY-MM-DD") : null;
                    var EndDateTmqt = value.EndDateTmqt !== null && value.EndDateTmqt !== "" ? moment(value.EndDateTmqt).format("YYYY-MM-DD") : null;
                    var EndDateBtckt = value.EndDateBtckt !== null && value.EndDateBtckt !== "" ? moment(value.EndDateBtckt).format("YYYY-MM-DD") : null;
                    var EndDateKh = value.EndDateKh !== null && value.EndDateKh !== "" ? moment(value.EndDateKh).format("YYYY-MM-DD") : null;
                    var EndDateCqnn = value.EndDateCqnn !== null && value.EndDateCqnn !== "" ? moment(value.EndDateCqnn).format("YYYY-MM-DD") : null;
                    var EndDateTp = value.EndDateTp !== null && value.EndDateTp !== "" ? moment(value.EndDateTp).format("YYYY-MM-DD") : null;
                    var EndDateTv = value.EndDateTv !== null && value.EndDateTv !== "" ? moment(value.EndDateTv).format("YYYY-MM-DD") : null;
                    var EndDateKb = value.EndDateKb !== null && value.EndDateKb !== "" ? moment(value.EndDateKb).format("YYYY-MM-DD") : null;

                    var obj = [value.ProjectDetailGuidC2, value.Note, StartDate, EndDate, value.Bkt, value.NotePhkt, EndDateKt, value.Btmqt,
                    value.NotePhtmqt, EndDateTmqt, value.NoteBtckt, EndDateBtckt, value.NameKh, value.NoteKh, EndDateKh, value.ClassifyCqnn != null ? value.ClassifyCqnn.toUpperCase() : null, value.NameCqnn, value.NotePhcqnn, EndDateCqnn, value.NameTp, value.NotePhtp, EndDateTp, value.ClassifyTv != null ? value.ClassifyTv.toUpperCase() : null, value.NameTv, value.NotePhtv, EndDateTv, value.NameKb != null ? value.NameKb.toUpperCase() : null, EndDateKb, value.NotePhkb];
                    $scope.lstdata.push(obj);

                })
                if ($scope.openAttach === 1) {
                    $scope.jxcelread.data = $scope.lstdata;
                } else {
                    $scope.jxcel.data = $scope.lstdata;
                }

            }

        });
    } else {
        if (data.length > 0) {
            $scope.lstdata = [];
            angular.forEach(data, function (value, key) {
                var num = key + 1;
                var StartDate = value.StartDate !== null && value.StartDate !== "" ? moment(value.StartDate).format("YYYY-MM-DD") : null;
                var EndDate = value.EndDate !== null && value.EndDate !== "" ? moment(value.EndDate).format("YYYY-MM-DD") : null;
                var EndDateKt = value.EndDateKt !== null && value.EndDateKt !== "" ? moment(value.EndDateKt).format("YYYY-MM-DD") : null;
                var EndDateTmqt = value.EndDateTmqt !== null && value.EndDateTmqt !== "" ? moment(value.EndDateTmqt).format("YYYY-MM-DD") : null;
                var EndDateBtckt = value.EndDateBtckt !== null && value.EndDateBtckt !== "" ? moment(value.EndDateBtckt).format("YYYY-MM-DD") : null;
                var EndDateKh = value.EndDateKh !== null && value.EndDateKh !== "" ? moment(value.EndDateKh).format("YYYY-MM-DD") : null;
                var EndDateCqnn = value.EndDateCqnn !== null && value.EndDateCqnn !== "" ? moment(value.EndDateCqnn).format("YYYY-MM-DD") : null;
                var EndDateTp = value.EndDateTp !== null && value.EndDateTp !== "" ? moment(value.EndDateTp).format("YYYY-MM-DD") : null;
                var EndDateTv = value.EndDateTv !== null && value.EndDateTv !== "" ? moment(value.EndDateTv).format("YYYY-MM-DD") : null;
                var EndDateKb = value.EndDateKb !== null && value.EndDateKb !== "" ? moment(value.EndDateKb).format("YYYY-MM-DD") : null;

                var obj = [value.ProjectDetailGuidC2, value.Note, StartDate, EndDate, value.Bkt, value.NotePhkt, EndDateKt, value.Btmqt,
                value.NotePhtmqt, EndDateTmqt, value.NoteBtckt, EndDateBtckt, value.NameKh, value.NoteKh, EndDateKh, value.ClassifyCqnn, value.NameCqnn, value.NotePhcqnn, EndDateCqnn, value.NameTp, value.NotePhtp, EndDateTp, value.ClassifyTv, value.NameTv, value.NotePhtv, EndDateTv, value.NameKb, EndDateKb, value.NotePhkb];
                $scope.lstdata.push(obj);

            })
            $scope.jxcel.data = $scope.lstdata;
            //$scope.jxcel.reloadData.reloadJexcel();
        }
    }
    $scope.ListDeleteFile = [];
    $scope.remoteAttach = function (item, number) {
        if (item.AttachmentGuid === undefined) {
            $scope.jdataattach.splice(number, 1);
        } else {
            $scope.jdataattach.splice(number, 1);
            $scope.ListDeleteFile.push(item.AttachmentGuid);
        }
    }
    $scope.submit = function () {
        $scope.ListData = $scope.modeljexcel.Jexcelc2.jexcel('getData');
        $uibModalInstance.close({ File: $scope.ListData, Delete: $scope.ListDetailDelete });
    }
});
//Thêm mới gia hạn ND cấp 2
app.controller('add_ExpiredNDC2', function ($scope, $rootScope, $compile, $uibModal, $confirm, $uibModalInstance, dataservice, para, para1, FileUploader) {
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
    $scope.Title = "Gia hạn công việc";
    $scope.modeljexcel = { Jexcelc2: {}, Jexcelc2read: {} };
    $scope.openAttach = para1;
    //Cấu hình file đính kèm
    var uploader = $scope.uploader = new FileUploader({
        // url: App.baseService() + '/upload'
    });
    uploader.filters.push({
        name: "customFilter",
        fn: function (item /*{File|FileLikeObject}*/, options) {
            if ($scope.uploader.queue.length === 10) {
                App.notifyDanger("Không vượt quá 10 file đính kèm");
                return;
            } else
                if (parseFloat(item.size / 1024 / 1024) <= 10) {
                    return this.queue.length < 10;
                } else {
                    App.notifyDanger("File đính kèm không vượt quá 10 MB");
                }

        }
    });
    uploader.onCompleteAll = function () {
    };

    $scope.rowchange = function (obj, row, cell, columnName) {
        var rowindex = parseInt(row); // dòng muốn thay đổi              
        $scope.ListData = $scope.modeljexcel.Jexcelc2.jexcel('getData');
    }
    $scope.ListDetailDelete = [];
    $scope.deletees = function (element, item) {
        $scope.ListData = $('#myhtexpiredndc2').jexcel('getData');
        if ($scope.ListData.length > 1) {
            var cell = 'A' + (parseInt(item) + 1);
            var id = $('#myhtexpiredndc2').jexcel('getValue', cell);
            if (id !== "" && id !== null) {
                var sta = false;
                for (var i = 0; i < $scope.ListDetailDelete.length; i++) {
                    if ($scope.ListDetailDelete[i] === id) {
                        sta = true;
                    }
                }
                if (!sta) {
                    $scope.ListDetailDelete.push(id);
                }
            }
            $('#myhtexpiredndc2').jexcel('deleteRow', item);
            App.notifyInfo('Xóa thành công');
        }
        else {
            App.notifyDanger('Không xóa được dòng cuối cùng này !');
            return;
        }

    };

    $scope.jxcel = {
        deleterowconfirm: $scope.deletees,
        dataObject: true,
        reloadData: {},
        data: [['', '', '', '']],
        colHeaders: ['id', 'TG kết thúc', 'Đơn vị', 'Nguyên nhân'],
        colTitles: ['', 'Thời gian kết thúc', 'Đơn vị', 'Nguyên nhân'],
        colFooters: ['', '', '', ''],
        columnShow: [false, true, true, true],
        columns: [
            {
                name: 'Id',
                type: 'text'
            },
            { name: 'ExpiredDate', type: 'calendar', options: { format: 'DD/MM/YYYY' } },
            {
                name: 'ObjectiveGuid', type: 'autocomplete', // đơn vị tính
                url: '/Project/ProjectManage/GetObjective',
            },
            { name: 'Reason', type: 'text' }
        ],
        initDataRows: ['', '', '', ''],
        colWidths: [100, 100, 200, 460],
        allrowevent: true,
        tableOverflow: true,
        csvHeaders: true,
        tableHeight: '200px',
        changeCell: $scope.rowchange
    };
    $scope.jxcelread = {
        dataObject: true,
        reloadData: {},
        data: [['', '', '', '']],
        colHeaders: ['id', 'TG kết thúc', 'Đơn vị', 'Nguyên nhân'],
        colTitles: ['', 'Thời gian kết thúc', 'Đơn vị', 'Nguyên nhân'],
        colFooters: ['', '', '', ''],
        columnShow: [false, true, true, true],
        columns: [
            {
                name: 'Id',
                type: 'text', readOnly: true
            },
            { name: 'ExpiredDate', type: 'calendar', options: { format: 'DD/MM/YYYY' }, readOnly: true },
            {
                name: 'ObjectiveGuid', type: 'autocomplete', // đơn vị tính
                url: '/Project/ProjectManage/GetObjective', readOnly: true
            },
            { name: 'Reason', type: 'text', readOnly: true }
        ],
        initDataRows: ['', '', '', ''],
        colWidths: [100, 100, 200, 460],
        allrowevent: true,
        tableOverflow: true,
        csvHeaders: true,
        tableHeight: '200px',
        allowInsertRow: false,
        contextMenu: function () { return ""; },
        changeCell: $scope.rowchange
    };
    //Lấy danh sách đính kèm
    $scope.GetProjectManageExpired = function () {
        dataservice.GetProjectManageExpired({ IdS: [para.ProjectDetailGuidC2] }, function (rs) {
            $scope.lstdata = [];
            if (rs.length > 0) {
                angular.forEach(rs, function (value, key) {
                    var num = key + 1;
                    var ExpiredDate = value.ExpiredDate !== null && value.ExpiredDate !== "" ? moment(value.ExpiredDate).format("YYYY-MM-DD") : null;
                    var obj = [value.ProjectManageExpiredGuid, ExpiredDate, value.ObjectiveGuid, value.Reason];
                    $scope.lstdata.push(obj);
                })
            }
            if ($scope.lstdata.length == 0) {
                $scope.lstdata.push(['', '', '']);
            }
            if ($scope.openAttach === 1) {
                $scope.jxcelread.data = $scope.lstdata;
            } else {
                $scope.jxcel.data = $scope.lstdata;
            }
        });
    }
    $scope.GetProjectManageExpired();

    $scope.submit = function () {
        $scope.ListData = $scope.modeljexcel.Jexcelc2.jexcel('getData');
        var listgh = [];
        var fd = new FormData();
        for (var i = 0; i < $scope.ListData.length; i++) {
            if ($scope.ListData[i].Id != null && $scope.ListData[i].Id != "" && $scope.ListData[i].Id != undefined) {
                var ob = {
                    ProjectManageExpiredGuid: $scope.ListData[i].Id,
                    ProjectDetailGuidC2: para.ProjectDetailGuidC2,
                    ExpiredDate: $scope.ListData[i].ExpiredDate,
                    ObjectiveGuid: $scope.ListData[i].ObjectiveGuid,
                    Reason: $scope.ListData[i].Reason
                }
            } else {
                var ob = {
                    ProjectDetailGuidC2: para.ProjectDetailGuidC2,
                    ExpiredDate: $scope.ListData[i].ExpiredDate,
                    ObjectiveGuid: $scope.ListData[i].ObjectiveGuid,
                    Reason: $scope.ListData[i].Reason
                }
            }

            listgh.push(ob);
        }

        App.blockUI({
            target: "#table_load",
            boxed: true,
            message: 'Đang tải...'
        });
        fd.append('Insert', JSON.stringify(listgh));
        fd.append('DetailDelete', JSON.stringify($scope.ListDetailDelete));
        $.ajax({
            url: '/ProjectManage/UpdateProjectManageExpired',
            data: fd,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (rs) {
                if (rs.Error) {
                    App.unblockUI("#table_load");
                    App.notifyDanger(rs.Title);
                } else {
                    App.notifyInfo(rs.Title);
                    $scope.cancel();
                    $rootScope.reload();
                    $scope.GetProjectManageExpired();
                    App.unblockUI("#table_load");
                }
            },
            error: function (rs) {
                App.notifyInfo(rs.Title);
            }
        });

    }
});
