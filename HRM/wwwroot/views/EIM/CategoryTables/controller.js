﻿var ctxfolder = "/views/EIM/CategoryTables";
app.factory('dataservice', function ($http) {
    var headers = {
        "Content-Type": "CategoryTables/json;odata=verbose",
        "Accept": "CategoryTables/json;odata=verbose",
    }
    return {
        insert: function (data, callback) {
            $http.post('/CategoryTables/insert', data).success(callback);
        },
        update: function (data, callback) {
            $http.post('/CategoryTables/update', data).success(callback);
        },
        deleteItems: function (data, callback) {
            $http.post('/CategoryTables/IsDeletedItems', data).success(callback);
        },
        delete: function (data, callback) {
            $http.post('/CategoryTables/IsDeleted/' + data).success(callback);
        },
        getItem: function (data, callback) {
            $http.post('/CategoryTables/getitem/' + data).success(callback);
        },
        getItemView: function (data, callback) {
            $http.post('/CategoryTables/getitemview/' + data).success(callback);
        },
        resort: function (data, callback) {
            $http.post('/CategoryTables/resort', data).success(callback);
        },
        getProvinces: function (callback) {
            $http.post('/CategoryTables/GetProvinces/').success(callback);
        },
        getObject: function (data, callback) {
            $http.post('/CategoryTables/GetObject/' + data).success(callback);
        }
    };
});
app.directive('format', ['$filter', function ($filter) {
    return {
        require: '?ngModel',
        link: function (scope, elem, attrs, ctrl) {
            if (!ctrl) return;
            ctrl.$formatters.unshift(function (a) {
                return $filter(attrs.format)(ctrl.$modelValue)
            });
            ctrl.$parsers.unshift(function (viewValue) {
                var plainNumber = viewValue.replace(/[^\d|\-+|\.+]/g, '');
                elem.val($filter(attrs.format)(plainNumber));
                return plainNumber;
            });
        }
    };
}]);
app.filter('fdate', [
    '$filter', function ($filter) {
        return function (input, f) {
            if (input && input.toString().indexOf('Date') > -1) {
                return moment(input).format(f);
            } if (input && input.toString().indexOf('T') > -1) {
                return moment(input).format(f);
            } else return input;
        };
    }
]);

app.filter('html', ['$sce', function ($sce) {
    return function (text) {
        return $sce.trustAsHtml(text);
    };
}]);
app.controller('Ctrl_CategoryTables', function ($scope, $rootScope, $sce, $compile, $uibModal, dataservice) {
    $rootScope.Title = "";
    $rootScope.go = function (path) {
        $location.path(path); return false;
    };
    $rootScope.StatusData = [{
        Value: 1,
        Name: 'Sử dụng'
    }, {
        Value: 0,
        Name: 'Không sử dụng'
    }];

    jQuery_V.validator.addMethod("alphanumeric", function (value, element) {
        return this.optional(element) || /^\w+$/i.test(value);
    }, "Letters, numbers, and underscores only please");
    jQuery_V.validator.addMethod("alphanumerics", function (value, element) {
        var data = /[(!@#$%^&*\)]+$/i.test(value);
        return this.optional(element) || data == false ? true : false;
    }, "Letters, numbers, and underscores only please");
    jQuery_V.validator.addMethod("checknumber", function (value, element) {
        var data = /[(0-9)]+$/i.test(value);
        return this.optional(element) || data ? true : false;
    }, "Letters, numbers, and underscores only please");
    jQuery_V.validator.addMethod("noSpace", function (value, element) {
        return value.indexOf(" ") < 0 && value != "";
    }, "No space please and don't leave it empty");
    $rootScope.validationOptions = {
        rules: {
            TableId: {
                required: true
            },
            TableName: {
                required: 255
            }
        },
        messages: {
            TableName: {
                required: "Mô tả không vượt quá 255 ký tự."
            },
            TableId: {
                required: "Địa chỉ không vượt quá 255 ký tự."
            }
        }
    };

    $rootScope.CreateDate = function (date) {
        if (date !== null && date !== undefined) {
            var newdate = new Date(date);
            var month = newdate.getMonth() + 1;
            var day = newdate.getDate();
            var year = newdate.getFullYear();
            var hh = newdate.getHours();
            var mm = newdate.getMinutes();
            if (month < 10)
                month = "0" + month;
            if (day < 10)
                day = "0" + day;
            if (mm < 10)
                mm = "0" + mm;
            return todayDate = day + "/" + month + "/" + year;
        } else {
            return null;
        }
    }
    $rootScope.EditDate = function (datetime) {
        if (datetime !== null && datetime !== undefined) {
            var Str = datetime.toString();
            if (Str.indexOf("/Date") >= 0) {

                var newdate = new Date(parseInt(datetime.substr(6)));
                var month = newdate.getMonth() + 1;
                var day = newdate.getDate();
                var year = newdate.getFullYear();
                var hh = newdate.getHours();
                var mm = newdate.getMinutes();
                if (month < 10)
                    month = "0" + month;
                if (day < 10)
                    day = "0" + day;
                return year + "/" + month + "/" + day;
            } else {
                var value = datetime.split('/');

                return value[2] + '/' + value[1] + '/' + value[0];
            }
        }
        return null;
    };
    //Thêm phần tinymceOptions cho phần trợ giúp
    $rootScope.tinymceOptions = {
        selector: 'textarea',
        plugins: [
            'advlist autolink lists link  charmap print preview hr anchor pagebreak image',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime  media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
        ],
        skin: 'lightgray', language: 'vi_VN',
        theme: 'modern', height: 500,
        toolbar1: 'codesample | bold italic sizeselect fontselect fontsizeselect | hr alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | insertfile undo redo | emoticons | code',
        fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
        toolbar2: 'print preview insert link image media | forecolor backcolor emoticons | codesample',
        image_advtab: true,

        file_browser_callback: function (field_name, url, type, win) {
            console.log(type + "|" + win);
            var connector = "/_FM/Index.aspx?f=L1B1Ymxpc2hpbmdJbWFnZXM=";
            var enableAutoTypeSelection = true;
            if (enableAutoTypeSelection && type) {
                connector += "&Type=" + type;
            }
            connector += "&Field=" + field_name;
            appHelp.openPage(connector, "File Manager", 1150, 600);
        },
        relative_urls: false,
        remove_script_host: true,
        convert_urls: true
    };
});
app.config(function ($routeProvider, $validatorProvider) {
    $routeProvider
        .when('/', {
            templateUrl: ctxfolder + '/index.html',
            controller: 'index'
        })
        .when('/edit/:id', {
            templateUrl: ctxfolder + '/edit.html',
            controller: 'edit'
        })
        .when('/add/', {
            templateUrl: ctxfolder + '/add.html',
            controller: 'add'
        });
    $validatorProvider.setDefaults({
        errorElement: 'span',
        errorClass: 'help-block',
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        success: function (label) {
            label.closest('.form-group').removeClass('has-error');
        }
    });
});
app.controller('index', function ($scope, $rootScope, $compile, $confirm, $ngConfirm, $uibModal, DTOptionsBuilder, DTColumnBuilder, DTInstances, dataservice) {

    var vm = $scope;
    $scope.selected = [];
    $scope.selectAll = false;
    $scope.toggleAll = toggleAll;
    $scope.toggleOne = toggleOne;
    $scope.staticParam = {
        totalItems: 0,
        currentPage: 1,
        pageSize: 15,
        maxSize: 5,
        Search: ""
    };
    var titleHtml = '<label class="mt-checkbox"><input type="checkbox" ng-model="selectAll" ng-click="toggleAll(selectAll, selected)"/><span></span></label>';
    vm.dtOptions = DTOptionsBuilder.newOptions()
        .withPaginationType('full_numbers').withDOM("t<'table-scrollable't>ip")
        .withDataProp('data').withDisplayLength(15)
        .withOption('order', [3, 'asc'])
        .withOption('serverSide', true)
        .withOption('stateLoadParams', function (settings, data) {
            data.search.search = '';
        })
        .withOption('headerCallback', function (header) {
            if (!$scope.headerCompiled) {
                $scope.headerCompiled = true;
                $compile(angular.element(header).contents())($scope);
            }
        })
        .withOption('rowCallback', rowCallback)
        .withOption('initComplete', function (settings, json) {
        })
        .withOption('createdRow', function (row, data, dataIndex) {
            const contextScope = $scope.$new(true);
            contextScope.data = data;
            $scope.data = data;
            $compile(row)($scope);
            contextScope.contextMenu = $scope.contextMenu;
            $compile(angular.element(row).find('input'))($scope);
            $compile(angular.element(row).attr('context-menu', 'contextMenu'))(contextScope);
        });
    //if (App.Permissions.LISTVIEW)
    vm.dtOptions.withOption('ajax', {
        url: "/CategoryTables/jtable",
        beforeSend: function (jqXHR, settings) {
            App.blockUI({
                target: "#contentMain",
                boxed: true,
                message: 'Đang tải...'
            });
        },
        type: 'POST',
        data: function (d) {
            d.search.value = $scope.staticParam.Search;
            if ($scope.staticParam.IsActive === 'true')
                d.IsActive = true;
            else
                d.IsActive = false;
            $scope.staticParam.currentPage = $scope.dtInstance.DataTable.page() + 1;
        },
        complete: function () {
            App.unblockUI("#contentMain");
        }
    }).withOption('serverSide', true);
    vm.dtColumns = [];
    vm.dtColumns.push(DTColumnBuilder.newColumn("TableGuid").withTitle(titleHtml).notSortable()
        .renderWith(function (data, type, full, meta) {
            $scope.selected[data] = false;
            var dm = "selected['" + data + "']";
            return '<label class="mt-checkbox"><input type="checkbox" ng-model="' + dm + '" ng-click="toggleOne(selected)"/><span></span></label>';
        }).withOption('sWidth', '5px').withOption('sClass', 'tcenter'));
    vm.dtColumns.push(DTColumnBuilder.newColumn('_STT').withTitle('STT').notSortable().withOption('sWidth', '5px').renderWith(function (data, type, full, meta) {
        return ($scope.staticParam.currentPage - 1) * 15 + data;
    }).withOption('sClass', 'tcenter'));

    vm.dtColumns.push(DTColumnBuilder.newColumn('TableId').withTitle('Mã bảng').withOption('sWidth', '150px').renderWith(function (data, type, full, meta) {
        return data;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('TableName').withTitle('Tên bảng').withOption('sWidth', '300px').renderWith(function (data, type, full, meta) {
        var open = 'open("' + full.TableGuid + '")';
        return "<a href='' ng-click='" + open + "' >" + data + "</a>";
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('TableType').withTitle('Loại bảng').withOption('sWidth', '350px').renderWith(function (data, type, full, meta) {
        if (data === 'F')
            return 'Cố định';
        if (data === 'D')
            return 'Động';
    }));

    vm.dtColumns.push(DTColumnBuilder.newColumn('ModuleName').withTitle('Tên module').withOption('sWidth', '200px').renderWith(function (data, type, full, meta) {
        return data;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('TableGuid').notSortable().withTitle('Điều khiển').withOption('sWidth', '50px').withOption('sClass', 'tcenter').renderWith(function (data, type, full, meta) {
        var open = "open('" + data + "')";
        var edit = "edit('" + data + "')";
        var delete1 = "delete('" + data + "')";
        return '<div class="table__cell-actions-wrap"><a ng-click=' + open + ' class="table__cell-actions-item table__cell-actions-icon"> <span class="mdi mdi-eye" title="Xem chi tiết"></span> </a>' +
            '<a ng-click=' + edit + ' class="table__cell-actions-item table__cell-actions-icon"><span class="mdi mdi-table-edit" title="Sửa"></span> </a>' +
            '<a ng-click=' + delete1 + ' class="table__cell-actions-item table__cell-actions-icon"><span class="mdi mdi-delete" title="Xóa"></span> </a></div>';
    }));

    vm.reloadData = reloadData;
    vm.dtInstance = {};
    function reloadData(resetPaging) {
        $scope.staticParam.currentPage = resetPaging;
        vm.dtInstance.reloadData(callback, resetPaging);
    }
    function callback(json) {

    }
    function toggleAll(selectAll, selectedItems) {
        for (var id in selectedItems) {
            if (selectedItems.hasOwnProperty(id)) {
                selectedItems[id] = selectAll;
            }
        }
    }
    function toggleOne(selectedItems) {
        for (var id in selectedItems) {
            if (selectedItems.hasOwnProperty(id)) {
                if (!selectedItems[id]) {
                    vm.selectAll = false;
                    return;
                }
            }
        }
        vm.selectAll = true;
    }
    function rowCallback(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        $('td', nRow).unbind('dblclick');
        $('td', nRow).bind('dblclick', function () {
            $scope.$apply(function () {
                $scope.open(aData.TableGuid);
            });
        });
        return nRow;
    }
    $scope.reload = function () {
        //$rootScope.IsActive = $scope.model.IsActive;
        reloadData(true);

    };
    //Trạng thái
    $scope.staticParam.IsActive = 'true';
    $scope.StatusConfig = {
        placeholder: 'Chọn trạng thái',
        okCancelInMulti: true,
        selected: ['true']
    };
    $scope.ListStatus = [{ value: 'true', text: 'Sử dụng' }, { value: 'false', text: 'Không sử dụng' }];
    $scope.ChangeStatus = function (data) {
        $scope.staticParam.IsActive = data;
        $scope.reload();
    };

    $scope.reloadALL = function () {
        $scope.staticParam.Search = "";
        $scope.staticParam.IsActive = 'true';
        $scope.selectAll = false;
        reloadData(true);
    };
    $rootScope.reload = function () {
        //$scope.staticParam.Search = "";
        reloadData(true);
    };
    $scope.add = function () {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/add.html',
            controller: 'add',
            backdrop: true,
            size: '70'
        });
        modalInstance.result.then(function (d) {
            $scope.reload();
        }, function () {
        });
    };
    $scope.open = function (temp) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/open.html',
            controller: 'open',
            backdrop: true,
            size: '70',
            resolve: {
                para: function () {
                    return temp;
                }
            }
        });
        modalInstance.result.then(function (d) {
            $scope.reload();
        }, function () {
        });
    };
    $scope.edit = function (temp) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/edit.html',
            controller: 'edit',
            backdrop: true,
            size: '70',
            resolve: {
                para: function () {
                    return temp;
                }
            }
        });
        modalInstance.result.then(function (d) {
            $scope.reload();
        }, function () {
        });
    };
    $scope.delete = function (temp) {
        dataservice.getItem(temp, function (rs) {
            ESConfirm([{ 'value': 1, 'title': 'Xác nhận', 'type': 'info' }, { 'value': 2, 'title': 'Đóng', 'type': 'danger' }], { 'description': '[' + rs.TableName + ']', 'class': 'eswarning' }, function (rs) {
                if (rs === '1') {
                    App.blockUI({
                        target: "#contentMain",
                        boxed: true,
                        message: 'Đang tải...'
                    });
                    dataservice.delete(temp, function (result) {
                        if (result.Error) {
                            App.notifyDanger(result.Title);
                        } else {
                            App.notifyInfo(result.Title);
                            $scope.reload();
                        }
                        App.unblockUI("#contentMain");
                    });
                }
                if (rs === '2') {
                    console.log('Không chấp nhận');
                }
            }, function () {
                //some error in this function
                console.log('Không lựa chọn');
            });
        });
    };
    $scope.deleteChecked = function () {
        var deleteItems = [];
        for (var id in $scope.selected) {
            if ($scope.selected.hasOwnProperty(id)) {
                if ($scope.selected[id]) {
                    deleteItems.push(id);
                }
            }
        }
        if (deleteItems.length > 0) {
            ESConfirm([{ 'value': 1, 'title': 'Xác nhận', 'type': 'info' }, { 'value': 2, 'title': 'Đóng', 'type': 'danger' }], { 'description': 'các tài khoản đã chọn?', 'class': 'eswarning' }, function (rs) {
                if (rs === '1') {
                    App.blockUI({
                        target: "#contentMain",
                        boxed: true,
                        message: 'Đang tải...'
                    });
                    dataservice.deleteItems(deleteItems, function (result) {
                        if (result.Error) {
                            App.notifyDanger(result.Title);
                        } else {
                            App.notifyInfo(result.Title);
                            $scope.reload();
                        }
                        App.unblockUI("#contentMain");
                    });
                }
                if (rs === '2') {
                    console.log('Không chấp nhận');
                }
            }, function () {
                //some error in this function
                console.log('Không lựa chọn');
            });
        } else {
            App.notifyDanger("Không có tài khoản nào được chọn");
        }
    };
    $scope.contextMenu = [
        [function ($itemScope) {
            return '<i class="mdi mdi-eye"></i> Xem';
        }, function ($itemScope, $event, model) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: ctxfolder + '/open.html',
                controller: 'open',
                backdrop: true,
                size: '70',
                resolve: {
                    para: function () {
                        return $itemScope.data.TableGuid;
                    }
                }
            });
            modalInstance.result.then(function (d) {
                $scope.reload();
            }, function () {
            });
        }, function ($itemScope, $event, model) {
            //return App.Permissions.OPEN;
            return true;
        }],
        [function ($itemScope) {
            return '<i class="mdi mdi-table-edit"></i> Sửa';
        }, function ($itemScope, $event, model) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: ctxfolder + '/edit.html',
                controller: 'edit',
                backdrop: true,
                size: '70',
                resolve: {
                    para: function () {
                        return $itemScope.data.TableGuid;
                    }
                }
            });
            modalInstance.result.then(function (d) {
                $scope.reload();
            }, function () {
            });
        }, function ($itemScope, $event, model) {
            //return App.Permissions.EDIT;
            return true;
        }],
        [function ($itemScope) {
            return '<i class="mdi mdi-delete"></i> Xóa';
        }, function ($itemScope, $event, model) {
                ESConfirm([{ 'value': 1, 'title': 'Xác nhận', 'type': 'info' }, { 'value': 2, 'title': 'Đóng', 'type': 'danger' }], { 'description': '[' + $itemScope.data.TableName + ']', 'class': 'eswarning' }, function (rs) {
                if (rs === '1') {
                    App.blockUI({
                        target: "#contentMain",
                        boxed: true,
                        message: 'Đang tải...'
                    });
                    dataservice.delete($itemScope.data.TableGuid, function (result) {
                        if (result.Error) {
                            App.notifyDanger(result.Title);
                        } else {
                            App.notifyInfo(result.Title);
                            $scope.reload();
                        }
                        App.unblockUI("#contentMain");
                    });
                }
                if (rs === '2') {
                    console.log('Không chấp nhận');
                }
            }, function () {
                //some error in this function
                console.log('Không lựa chọn');
            });
        }, function ($itemScope, $event, model) {
            return true;
            //return App.Permissions.DELETE;
        }]
    ];
});

app.controller('add', function ($scope, $rootScope, $compile, $uibModal, $confirm, $uibModalInstance, dataservice) {
    $scope.Title = "Thêm bảng";
    $scope.model = { IsActive: $rootScope.IsActive };
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
    $scope.Init = function () {
        //start combox 
        //Loại bảng
        $scope.TableTypeConfig = {
            placeholder: 'Chọn loại bảng',
            okCancelInMulti: true,
            selected: ['F']
        };
        $scope.ListTableType = [{ value: 'F', text: 'Cố định' }, { value: 'D', text: 'Động' }];
        $scope.ChangeTableType = function (data) {
            $scope.objectType = data;
        };
        //Trạng thái
        $scope.model.IsActive = 1;
        $scope.StatusConfig = {
            placeholder: 'Chọn trạng thái',
            okCancelInMulti: true,
            selected: [1]
        };
        $scope.ListStatus = [{ value: 1, text: 'Sử dụng' }, { value: 0, text: 'Không sử dụng' }];
        $scope.ChangeStatus = function (data) {
            $scope.model.IsActive = data;
        };
    };
    $scope.Init();
    $scope.submit = function () {
        if ($scope.addform.validate()) {
            dataservice.insert($scope.model, function (rs) {
                if (rs.Error) {
                    App.notifyDanger(rs.Title);
                } else {
                    App.notifyInfo(rs.Title);
                    $uibModalInstance.close();
                }
            });
        }
    };
});
app.controller('edit', function ($scope, $rootScope, $compile, $uibModal, $confirm, $uibModalInstance, dataservice, para) {
    $scope.Title = "Sửa bảng";
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
    $scope.model = {};
    $scope.initData = function () {
        $scope.ListObject = [];
        //start combox 
        //Loại bảng
        $scope.TableTypeConfig = {
            placeholder: 'Chọn loại bảng',
            okCancelInMulti: true,
            selected: ['F']
        };
        $scope.ListTableType = [{ value: 'F', text: 'Cố định' }, { value: 'D', text: 'Động' }];
        $scope.ChangeTableType = function (data) {
            $scope.objectType = data;
        };
        //Trạng thái
        $scope.model.IsActive = 1;
        $scope.StatusConfig = {
            placeholder: 'Chọn trạng thái',
            okCancelInMulti: true,
            selected: [1]
        };
        $scope.ListStatus = [{ value: 1, text: 'Sử dụng' }, { value: 0, text: 'Không sử dụng' }];
        $scope.ChangeStatus = function (data) {
            $scope.model.IsActive = data;
        };
        dataservice.getItem(para, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {
                $scope.model = rs;
            }
        });
    };
    $scope.initData();
    $scope.submit = function () {
        if ($scope.editform.validate()) {
                dataservice.update($scope.model, function (rs) {
                    if (rs.Error) {
                        App.notifyDanger(rs.Title);
                    } else {
                        App.notifyInfo(rs.Title);
                        $uibModalInstance.close();
                    }
                });
        }
    };
});
app.controller('open', function ($scope, $rootScope, $compile, $uibModal, $confirm, $uibModalInstance, dataservice, para) {
    $scope.Title = "Xem bảng";
    $scope.model = {};
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
    $scope.loadData = function () {
        dataservice.getItem(para, function (rs) {
            $scope.model = rs;
            if ($scope.model.TableType === 'F') $scope.model.TableType = 'Cố định';
            else if ($scope.model.TableType === 'D') $scope.model.TableType = 'Động';
            else $scope.model.TableType = '';
            if ($scope.model.IsActive === true) {
                $scope.model.IsActive = "Sử dụng";
            } else { $scope.model.IsActive = "Không sử dụng"; }
        });
    };
    $scope.loadData();
}); 
