﻿var ctxfolder = "/views/EIM/Workflows";
app.factory('dataservice', function ($http) {
    var headers = {
        "Content-Type": "Workflows/json;odata=verbose",
        "Accept": "Workflows/json;odata=verbose",
    };
    return {
        insert: function (data, callback) {
            $http.post('/Workflows/insert', data).success(callback);
        },
        update: function (data, callback) {
            $http.post('/Workflows/update', data).success(callback);
        },
        deleteSAVouchers: function (data, callback) {
            $http.post('/Workflows/IsDeletedSAVouchers', data).success(callback);
        },
        delete: function (data, callback) {
            $http.post('/Workflows/IsDeleted/' + data).success(callback);
        },
        getItem: function (data, callback) {
            $http.post('/Workflows/GetItem/' + data).success(callback);
        },
        getItems: function (data, callback) {
            $http.post('/Workflows/GetItems/' + data).success(callback);
        },
        copy: function (data, callback) {
            $http.post('/Workflows/Copy', data).success(callback);
        },
        getOrganizations: function (callback) {
            $http.get('/Workflows/GetOrganizations/').success(callback);
        }
    };
});
app.directive('format', ['$filter', function ($filter) {
    return {
        require: '?ngModel',
        link: function (scope, elem, attrs, ctrl) {
            if (!ctrl) return;
            ctrl.$formatters.unshift(function (viewValue) {
                try {
                    plainNumber = viewValue.replace('.', ',');
                    elem.val(spassAresSlug(plainNumber));
                    return spassAresSlug(plainNumber);
                } catch (ex) {
                    return undefined;
                }
            });
            ctrl.$parsers.unshift(function (viewValue) {
                plainNumber = viewValue.replace(/[^0123456789,]/g, '');
                elem.val(spassAresSlug(plainNumber));
                return plainNumber.replace(/[,]/g, '.');
            });
        }
    };
}]);
app.filter('fdate', [
    '$filter', function ($filter) {
        return function (input, f) {
            if (input && input.toString().indexOf('Date') > -1) {
                return moment(input).format(f);
            } if (input && input.toString().indexOf('T') > -1) {
                return moment(input).format(f);
            } else return input;
        };
    }
]);

app.filter('html', ['$sce', function ($sce) {
    return function (text) {
        return $sce.trustAsHtml(text);
    };
}]);
app.controller('Ctrl_Workflow', function ($scope, $rootScope, $sce, $compile, $uibModal, dataservice) {
    $rootScope.Title = "";
    $rootScope.go = function (path) {
        $location.path(path); return false;
    };
    jQuery_V.validator.addMethod("alphanumerics", function (value, element) {
        var data = /[(!@#$%^&*\)]+$/i.test(value);
        return this.optional(element) || data === false ? true : false;
    }, "Letters, numbers, and underscores only please");
    jQuery_V.validator.addMethod("Number", function (value, element) {
        return this.optional(element) || parseInt(value.split('.').join('')) >= 0 ? true : false;
    }, "Letters, numbers, and underscores only please");
    $rootScope.validationOptions = {
        ignore: [],
        rules: {
            Date: {
                required: true
            },
            VoucherDate: {
                required: true
            },
            VoucherNo: {
                required: true
            },
            InvoucherNo: {
                required: true
            },
            InvoucherDate: {
                required: true
            },
            DueDay: {
                Number: true
            }
        },
        messages: {
            Date: {
                required: 'Yêu cầu nhập ngày hạch toán.'
            },
            VoucherDate: {
                required: 'Yêu cầu nhập ngày chứng từ.'
            },
            VoucherNo: {
                required: "Yêu cầu nhập số chứng từ."
            },
            InvoucherNo: {
                required: "Yêu cầu nhập số phiếu xuất."
            },
            InvoucherDate: {
                required: "Yêu cầu nhập ngày phiếu xuất."
            },
            DueDay: {
                Number: "Ngày được nợ không được âm."
            }

        }
    };
    $rootScope.StatusData = [{
        Value: 1,
        Name: 'Sử dụng'
    }, {
        Value: 0,
        Name: 'Không sử dụng'
    }];
    $rootScope.CalenderReturnDate = function (item) {
        //xác định tuần trước
        var d = new Date();
        if (item === '0') {
            var day = d.getDay(),
                fday = d.getDate() - day + (day === 0 ? -6 : 1) - 7,
                lday = d.getDate() - day;
            var data = {
                sdate: moment(new Date(d.getFullYear(), d.getMonth(), fday)).format(),
                edate: moment(new Date(d.getFullYear(), d.getMonth(), lday)).format()
            };
            return data;
        }
        //Tháng trước
        if (item === '1') {
            var data1 = {
                sdate: moment(new Date(d.getFullYear(), d.getMonth() - 1, 1)).format(),
                edate: moment(new Date(d.getFullYear(), d.getMonth(), 0)).format()
            };
            return data1;
        }
        //Quý trước
        if (item === '2') {
            var motnth = d.getMonth();
            var year = d.getFullYear();
            if (d.getMonth() === 0 || d.getMonth() === 1 || d.getMonth() === 2) {
                motnth = 10;
                year = year - 1;
            }
            else if (d.getMonth() === 3 || d.getMonth() === 4 || d.getMonth() === 5)
                motnth = 1;
            else if (d.getMonth() === 6 || d.getMonth() === 7 || d.getMonth() === 8)
                motnth = 4;
            else
                motnth = 7;
            var data2 = {
                sdate: moment(new Date(year, motnth - 1, 1)).format(),
                edate: moment(new Date(year, motnth + 2, 0)).format()
            };
            return data2;
        }
        //Năm trước
        if (item === '3') {
            var data3 = {
                sdate: moment(new Date(d.getFullYear() - 1, 0, 1)).format(),
                edate: moment(new Date(d.getFullYear() - 1, 12, 0)).format()
            };
            return data3;
        }
        //Tuần sau
        if (item === '4') {
            var nday = d.getDay(),
                nfday = d.getDate() - nday + (day === 0 ? -6 : 1) + 7,
                nlday = d.getDate() - nday + 14;
            var data4 = {
                sdate: moment(new Date(d.getFullYear(), d.getMonth(), nfday)).format(),
                edate: moment(new Date(d.getFullYear(), d.getMonth(), nlday)).format()
            };
            return data4;
        }
        //Tháng sau
        if (item === '5') {
            var data5 = {
                sdate: moment(new Date(d.getFullYear(), d.getMonth() + 1, 1)).format(),
                edate: moment(new Date(d.getFullYear(), d.getMonth() + 2, 0)).format()
            };
            return data5;
        }
        //Quý sau
        if (item === '6') {
            var nmotnth = d.getMonth();
            var nyear = d.getFullYear();
            if (d.getMonth() === 0 || d.getMonth() === 1 || d.getMonth() === 2)
                nmotnth = 4;
            else if (d.getMonth() === 3 || d.getMonth() === 4 || d.getMonth() === 5)
                nmotnth = 7;
            else if (d.getMonth() === 6 || d.getMonth() === 7 || d.getMonth() === 8)
                nmotnth = 10;
            else {
                nmotnth = 1;
                nyear = nyear + 1;
            }
            var data6 = {
                sdate: moment(new Date(nyear, nmotnth - 1, 1)).format(),
                edate: moment(new Date(nyear, nmotnth + 2, 0)).format()
            };
            return data6;
        }
        //Năm sau
        if (item === '7') {
            var data7 = {
                sdate: moment(new Date(d.getFullYear() + 1, 0, 1)).format(),
                edate: moment(new Date(d.getFullYear() + 1, 12, 0)).format()
            };
            return data7;
        }
    };
    $rootScope.ViewDateTime = function (datetime) {
        if (datetime !== null && datetime !== undefined && datetime !== 0) {
            var year = datetime.toString().substring(0, 4);
            var month = datetime.toString().substring(4, 6);
            var day = datetime.toString().substring(6);
            return day + "/" + month + "/" + year;
        }
        return null;
    };
    $rootScope.ConvertDateEdit = function (date) {
        if (date && date.toString().indexOf('Date') > -1)
            return moment(date).format();
        if (date && date.toString().indexOf('T') > -1)
            return moment(date).format();
        else {
            if (date !== null && date !== undefined) {
                var d = date.split('/');
                return moment(d[2] + '-' + d[1] + '-' + d[0]).format();
            }
            else
                return null;
        }
    };
    $rootScope.addPeriod = function (nStr) {
        if (nStr !== null && nStr !== "" && nStr !== undefined) {
            nStr = Math.round(parseFloat(nStr) * 100) / 100;
            nStr += "";
            if (nStr.indexOf(".") >= 0) {
                var x = nStr.split(".");
            } else {
                var x = nStr.split(",");
            }
            var x1 = x[0];
            var x2 = x.length > 1 && parseInt(x[1]) > 0 ? "," + x[1] : "";
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, "$1" + "." + "$2");
            }
            var Total = x1 + x2;
            return Total;
        }
        else {
            return 0;
        }
    };
    $rootScope.ListOrganizations = [];
    dataservice.getOrganizations(function (rs) {
        $rootScope.ListOrganizations = rs;
    });
});
app.config(function ($routeProvider, $validatorProvider) {
    $routeProvider
        .when('/', {
            templateUrl: ctxfolder + '/index.html',
            controller: 'index'
        })
        .when('/edit/:id', {
            templateUrl: ctxfolder + '/edit.html',
            controller: 'edit'
        })
        .when('/add/', {
            templateUrl: ctxfolder + '/add.html',
            controller: 'add'
        });
    $validatorProvider.setDefaults({
        errorElement: 'span',
        errorClass: 'help-block',
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        success: function (label) {
            label.closest('.form-group').removeClass('has-error');
        }
    });
});
app.controller('index', function ($http, $scope, $rootScope, $compile, $confirm, $ngConfirm, $uibModal, DTOptionsBuilder, DTColumnBuilder, DTInstances, dataservice, keyhandle) {
    $scope.model = {};
    var vm = $scope;
    var ctrl = $scope;
    ctrl.nextRow = function (d) {
        if (ctrl.ElementRowCheck === -1) {
            if (ctrl.allRow.length > 1) {
                ctrl.ElementRowCheck = ctrl.allRow[0].row;
                $('td', ctrl.ElementRowCheck).addClass("es-iSeleted");
                ctrl.iSelected = ctrl.allRow[0].data;
            }
        } else
            for (var i = 0; i < ctrl.allRow.length; i++) {
                if (ctrl.allRow[i].row === ctrl.ElementRowCheck) {
                    if (i === ctrl.allRow.length - 1) break;
                    $('td', ctrl.ElementRowCheck).removeClass('es-iSeleted');
                    ctrl.ElementRowCheck = ctrl.allRow[i + 1].row;
                    $('td', ctrl.ElementRowCheck).addClass("es-iSeleted");
                    ctrl.iSelected = ctrl.allRow[i + 1].data;
                    break;
                }
            }
    };
    ctrl.upRow = function (d) {
        if (ctrl.ElementRowCheck === -1) {
            if (ctrl.allRow.length > 1) {
                ctrl.ElementRowCheck = ctrl.allRow[ctrl.allRow.length - 1].row;
                $('td', ctrl.ElementRowCheck).addClass("es-iSeleted");
                ctrl.iSelected = ctrl.allRow[ctrl.allRow.length - 1].row;
            }
        } else
            for (var i = 0; i < ctrl.allRow.length; i++) {
                if (ctrl.allRow[i].row === ctrl.ElementRowCheck) {
                    if (i === 0) break;
                    $('td', ctrl.ElementRowCheck).removeClass('es-iSeleted');
                    ctrl.ElementRowCheck = ctrl.allRow[i - 1].row;
                    $('td', ctrl.ElementRowCheck).addClass("es-iSeleted");
                    ctrl.iSelected = ctrl.allRow[i - 1].data;

                    break;
                }
            }
    };
    keyhandle.eventCtrl(40, ctrl.nextRow);
    keyhandle.eventCtrl(38, ctrl.upRow);
    ctrl.ElementRowCheck = -1;
    ctrl.allRow = [];
    ctrl.iSelected = {};
    ctrl.dataScrollPage = [];
    $scope.staticParam = {
        totalItems: 0,
        currentPage: 1,
        pageSize: 10,
        maxSize: 5,
        Keyword: ""
    };

    $scope.dataload = [];

    $scope.lstDepartmentID = [];
    $scope.CheckReload = false;
    $scope.lstInt = [];
    $rootScope.TongGT = 0;
    $scope.selected = [];
    $scope.selectAll = false;
    $scope.toggleAll = toggleAll;
    $scope.toggleOne = toggleOne;
    // var titleHtml = '<label class="mt-checkbox"><input type="checkbox" ng-model="selectAll" ng-click="toggleAll(selectAll, selected)"/><span></span></label>';

    vm.dtOptions = DTOptionsBuilder.newOptions()
        .withOption('serverSide', true)
        .withOption('order', [1, 'desc'])
        .withDataProp('data')
        .withOption('pageLength', 25)
        .withOption('initComplete', function (settings, json) {
            //chỉ dùng khi có footer
            //ẩn scroll-x body
            $('.dataTables_scrollBody').css({
                'overflow-y': 'scroll',
                'overflow-x': 'hidden',
                'border': 'none'
            });
            //hiền scroll footer
            $('#tblData_wrapper .dataTables_scrollFoot').css('overflow', 'auto');
            //cuộn toàn bảng theo footer
            $('#tblData_wrapper .dataTables_scrollFoot').on('scroll', function () {
                $('#tblData_wrapper .dataTables_scrollBody').scrollLeft($(this).scrollLeft());
            });

            //thêm sự kiện scroll phân trang
            PLUGIN_DATATABLE_TABLE_SCROLL.INIT_EVENT('tblData', $scope.LoadScroll);
        })
        .withOption('searching', false)
        .withOption('createdRow', function (row, data, dataIndex) {
            const contextScope = $scope.$new(true);
            contextScope.data = data;
            $scope.data = data;
            $compile(row)($scope);
            contextScope.contextMenu = $scope.contextMenu;
            $compile(angular.element(row).find('input'))($scope);
            $compile(angular.element(row).attr('context-menu', 'contextMenu'))(contextScope);
        });
    vm.dtOptions.withOption('ajax', function (data, callback, settings) {
        data.search.value = $scope.staticParam.Keyword;
        if ($scope.CheckReload) {
            data.Start = 0;
        }
        App.blockUI({
            target: "#table_load",
            boxed: true,
            message: 'Đang tải...'
        });
        $http.post('/Workflows/JTable_WF', data)
            .success(function (res) {
                if (data.start === 0) {
                    $scope.dataload = [];
                    angular.forEach(res.data, function (val, key) {
                        $scope.dataload.push(val);
                    });
                    callback({
                        data: $scope.dataload,
                        recordsTotal: res.recordsTotal,
                        recordsFiltered: res.recordsFiltered
                    });
                }
                else if (res.recordsTotal > $scope.dataload.length) {
                    angular.forEach(res.data, function (val, key) {
                        $scope.dataload.push(val);
                    });
                    callback({
                        data: $scope.dataload,
                        recordsTotal: res.recordsTotal,
                        recordsFiltered: res.recordsFiltered
                    });
                }
                PLUGIN_DATATABLE_TABLE_SCROLL.UNLOCK_EVENT('tblData');
                App.unblockUI("#table_load");
            });
    });
    //scroll theo tỷ lệ màn hình
    vm.dtOptions.withOption('scrollY', '60vh')
        .withOption('scrollX', '100%') //mặc định 100%
        .withOption('scrollCollapse', true) //xóa khoảng trắng khi dữ liệu trong bảng ít
        .withOption('rowCallback', rowCallback) //sự kiên click vào 1 dòng
        .withOption('scroller', {
            loadingIndicator: true
        }).withFixedColumns({
            leftColumns: 0 //fix bên trái 2 cột
            //rightColumns: 1 //fix bên phải 1 cột
        });

    vm.dtColumns = [];

    vm.dtColumns.push(DTColumnBuilder.newColumn('_STT').withTitle('STT').notSortable().withOption('sClass', 'tcenter').withOption('sWidth', '5px').renderWith(function (data, type, full, meta) {
        return meta.row + 1;
    }));

    vm.dtColumns.push(DTColumnBuilder.newColumn('WorkflowId').withTitle('Mã quy trình').withTitleDescription('Mã quy trình').withOption('sWidth', '150px').renderWith(function (data, type, full, meta) {
        return data;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('WorkflowName').withTitle('Tên quy trình').renderWith(function (data, type, full, meta) {
        return data;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('WorkflowGuid').withTitle('Thao tác').withOption('sClass', 'tcenter').withOption('sWidth', '100px').renderWith(function (data, type, full, meta) {
        var edit = "edit('" + data + "')";
        var delete1 = "delete('" + data + "')";
        return '<div class="table__cell-actions-wrap"><a ng-click=' + edit + ' class="table__cell-actions-item table__cell-actions-icon"><span class="mdi mdi-table-edit" title="Sửa"></span> </a>' +
            '<a ng-click=' + delete1 + ' class="table__cell-actions-item table__cell-actions-icon"><span class="mdi mdi-delete" title="Xóa"></span> </a></div>';
    }));
    vm.reloadData = reloadData;
    vm.dtInstance = {};
    function reloadData(resetPaging) {
        ctrl.ElementRowCheck = -1;
        $scope.staticParam.currentPage = resetPaging;
        vm.dtInstance.reloadData(callback, resetPaging);
        vm.dtInstance.DataTable.draw();
    }
    function callback(json) {

    }
    function toggleAll(selectAll, selectedItems) {
        for (var id in selectedItems) {
            if (selectedItems.hasOwnProperty(id)) {
                selectedItems[id] = selectAll;
            }
        }
    }
    function toggleOne(selectedItems) {
        for (var id in selectedItems) {
            if (selectedItems.hasOwnProperty(id)) {
                if (!selectedItems[id]) {
                    vm.selectAll = false;
                    return;
                }
            }
        }
        vm.selectAll = true;
    }
    $scope.total = 0;
    $scope.check = false;
    $scope.LoadScroll = function (obj) {
        $scope.dtInstance.DataTable.page('next').draw('page');
    };
    function rowCallback(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        if (iDisplayIndexFull === 0) ctrl.allRow = [];
        $('td', nRow).unbind('click');
        $('td', nRow).bind('click', function () {
            if (vm.ElementRowCheck === nRow) return;
            $('td', nRow).addClass("es-iSeleted");
            $('td', vm.ElementRowCheck).removeClass('es-iSeleted');
            vm.ElementRowCheck = nRow;
            ctrl.ElementRowCheck = nRow;
            ctrl.iSelected = aData;

        });

        ctrl.allRow.push({ row: nRow, data: aData });
        return nRow;
    }
    $('#minize-menuleft').click(function () {
        vm.dtInstance.DataTable.draw();
    });
    $scope.reload = function () {
        $scope.dataload = [];
        reloadData(true);

    };
    $scope.reloadAll = function () {
        $scope.dataload = [];
        $scope.staticParam.Keyword = "";
        reloadData(true);

    };


    $scope.add = function () {
        window.location.href = '/EIM/DesignWorkflows/Index';
    };
    $scope.open = function (temp) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/view.html',
            controller: 'view',
            backdrop: true,
            size: 'md',
            resolve: {
                para: function () {
                    return temp;
                }
            }
        });
    };
    $scope.edit = function (temp) {
        window.location.href = '/EIM/DesignWorkflows/Index?id=' + temp;
    };
    $scope.delete = function (temp) {
        $.ajax({
            type: 'post',
            url: '/Workflows/GetItem/',
            data: { guid: temp },
            success: function (rs1) {
                if (rs1.Error) {
                    App.notifyDanger(rs1.Title);
                } else {
                    ESConfirm([{ 'value': 1, 'title': 'Xác nhận', 'type': 'info' }, { 'value': 2, 'title': 'Đóng', 'type': 'danger' }], { 'description': rs1.WorkflowName + ' không ?', 'class': 'eswarning' }, function (rs) {
                        if (rs === '1') {
                            App.blockUI({
                                target: "#contentMain",
                                boxed: true,
                                message: 'Đang tải...'
                            });
                            $.ajax({
                                type: 'post',
                                url: '/Workflows/IsDeleted/',
                                data: { guid: temp },
                                success: function (rs) {
                                    if (rs.Error) {
                                        App.notifyDanger(rs.Title);
                                        App.unblockUI("#contentMain");
                                    } else {
                                        App.notifyInfo(rs.Title);
                                        App.unblockUI("#contentMain");
                                        $scope.reload();

                                    }
                                }
                            });
                        }
                    }, function () {
                    });
                }
            }
        });
    };


    $scope.contextMenu = [
        [function ($itemScope) {
            return '<i class="mdi mdi-table-edit"></i> Sửa quy trình';
        }, function ($itemScope, $event, model) {
            $scope.edit($itemScope.data.WorkflowGuid);
        }, function ($itemScope, $event, model) {
            return true; // App.Permissions.true;
        }],
        [function ($itemScope) {
            return '<i class="mdi mdi-delete"> </i> Xóa quy trình';
        }, function ($itemScope, $event, model) {
            $scope.delete($itemScope.data.WorkflowGuid);
        }, function ($itemScope, $event, model) {
            return true; //App.Permissions.DELETE;
        }]
    ];
    $scope.copy = function (OrganizationGuid) {
        if (ctrl.iSelected.WorkflowGuid === undefined) {
            App.notifyDanger('Yêu cầu chọn quy trình');
        }
        else {
            App.blockUI({
                target: "#contentMain",
                boxed: true,
                message: 'Đang tải...'
            });
            var obj = {
                OrganizationGuid: OrganizationGuid,
                WorkflowGuid: ctrl.iSelected.WorkflowGuid
            };
            dataservice.copy(obj, function (result) {
                if (result.Error) {
                    App.notifyDanger(result.Title);
                } else {
                    App.notifyInfo(result.Title);
                    reloadData(true);
                }
                App.unblockUI("#contentMain");
            });
        }
    };
});