﻿var app = angular.module('e-app', ["ui.bootstrap", "ngValidate", "ui.select", "ng-aimodu"]);
app.factory('dataservice', function ($http) {
    var headers = {
        "Content-Type": "SAVouchers/json;odata=verbose",
        "Accept": "SAVouchers/json;odata=verbose"
    };
    return {
        insert: function (data, callback) {
            $http.post('/Workflows/insert', data).success(callback);
        },
        getTable: function (callback) {
            $http.get('/Workflows/GetTable').success(callback);
        }
    };
});
app.controller('workflowcontroller', ['$scope', '$parse', '$interpolate', '$compile', '$rootScope',
    function ($scope, $parse, $interpolate, $compile, $rootScope, dataservice) {
        $scope.ApproverConfig = {
            placeholder: 'Chọn người duyệt',
            search: true
        };
        // trạng thái WF
        $scope.ListWFStatusIdfig = {
            placeholder: 'Chọn trạng thái xử lý',
            search: true
        };
        // điều kiện rẽ nhánh bước tiếp theo WF
        $scope.WFConditionIdconfig = {
            placeholder: 'Chọn trạng thái',
            search: true
        };
        // trạng thái trả lại WF
        $scope.WFStatusBackIdConfig = {
            placeholder: 'Chọn trạng thái xử lý',
            search: true
        };
        // điều kiện rẽ nhánh trả lại WF
        $scope.WFConditionBackIdconfig = {
            placeholder: 'Chọn trạng thái xử lý',
            search: true
        };
        $scope.ApproveTypeConfig = {
            placeholder: 'Chọn',
            search: true
        };
        $.validator.addMethod("alphanumerics", function (value, element) {
            var data = /[(!@#$%^&*\)]+$/i.test(value);
            return this.optional(element) || data === false ? true : false;
        }, "Letters, numbers, and underscores only please");
        $.validator.addMethod("Number", function (value, element) {
            return this.optional(element) || parseInt(value.split('.').join('')) >= 0 ? true : false;
        }, "Letters, numbers, and underscores only please");
        $.validator.addMethod("valueNotEquals", function (value, element, arg) {
            // I use element.value instead value here, value parameter was always null
            return arg !== element.value;
        }, "Value must not equal arg.");
        $scope.validationOptions = {
            //ignore: [],
            rules: {
                WorkflowId: {
                    required: true
                },
                WorkflowName: {
                    required: true
                },
                TableGuid: {
                    valueNotEquals: "null"
                }
            },
            messages: {
                WorkflowId: {
                    required: 'Yêu cầu nhập mã quy trình.'
                },
                WorkflowName: {
                    required: 'Yêu cầu nhập tên quy trình.'
                },
                TableGuid: {
                    valueNotEquals: 'Yêu cầu chọn quy trình.'
                }
            }
        };
        $scope.model = { Conditions: [], Activities: [] };
        $scope.model1 = {};
        $scope.model2 = {};
        $scope.initData = function () {
            //Bảng
            $scope.TableConfig = {
                placeholder: '',
                okCancelInMulti: false,
                selected: []
            };
            $scope.ListTable = [{ value: null, text: 'chọn' }];
            $.ajax({
                type: 'get',
                url: '/Workflows/GetTable',
                success: function (rs) {
                    if (rs.Error) {
                        toastr["error"](rs.Title);
                    } else {
                        angular.forEach(rs, function (val, key) {
                            $scope.ListTable.push(val);
                        });
                        $scope.$apply();
                    }
                }
            });
            $scope.ChangeTable = function (data) {
                $scope.model.TableGuid = data;
            };
        };
        $scope.initData();

        angular.element(document).ready(function () {
            $scope.Conditions = [];
            var url = window.location.href;
            var check = url.indexOf('?');
            if (check !== -1) {
                var url2 = window.location.href;
                var url1 = url2.split('=');
                $.ajax({
                    type: 'post',
                    url: '/Workflows/LoadWorkFlow',
                    data: { guid: url1[1] },
                    success: function (rs) {
                        editor.parse(JSON.parse(rs.data.WorkFlowData));
                        $scope.model.WorkFlowName = rs.data.WorkflowName;
                        $scope.model.TableGuid = rs.data.TableGuid;
                        $scope.model.WorkFlowId = rs.data.WorkflowId;
                        $scope.model.WorkflowGuid = rs.data.WorkflowGuid;
                        $scope.model.Activities = [];
                        $scope.model.Conditions = [];
                        angular.forEach(rs.Activities, function (val, key) {
                            $scope.Activity = {
                                ActivityGuid: val.ActivityGuid, ActivityName: val.ActivityName, uidActivity: val.ActivityUid, stepStart: val.StartStep, conditionStep: val.ModifiedBy
                            };
                            $scope.model.Activities.push($scope.Activity);
                        });
                        $scope.Conditions = [];
                        angular.forEach(rs.Conditions, function (val, key) {
                            $scope.Condition = {};
                            $scope.Condition.ConditionGuid = val.ConditionGuid;
                            $scope.Condition.ConditionData = val.ConditionData;
                            $scope.Condition.ConditionTrue = val.ConditionTrue;
                            $scope.Condition.ConditionFalse = val.ConditionFalse;
                            $scope.Condition.uidCodition = val.ConditionUid;
                            $scope.Condition.StepContinue = val.StepContinue;
                            $scope.Condition.StepCallback = val.StepCallback;
                            $scope.Condition.WFStatusId = val.WFStatusId;
                            $scope.Condition.WFStatusBackId = val.WFStatusBackId;
                            $scope.Condition.WFConditionId = val.WFConditionId;
                            $scope.Condition.WFConditionBackId = val.WFConditionBackId;
                            $scope.model.Conditions.push($scope.Condition);
                        });
                        $scope.$apply();
                    }
                });
            }

            $("#myModalFormPoperty").modal({ backdrop: 'static' }, 'show');
            if (check !== -1) {
                $("#myModalFormPoperty").modal('hide');
            }
            var editor = new dhx.DiagramEditor(document.body, {
                select: true
            });
            var a = '[]';
            editor.parse(JSON.parse(a));
            editor.events.on("ApplyButton", function () {
                $scope.WorkFlowData = JSON.stringify(editor.serialize("Json"));
                var fd = new FormData();
                fd.append('data', JSON.stringify($scope.model));
                fd.append('flowjson', $scope.WorkFlowData);
                if (check === -1) {
                    if ($scope.model.WorkFlowCode !== null && $scope.model.WorkFlowCode !== ""
                        && $scope.model.WorkFlowName !== null && $scope.model.WorkFlowName !== "") {
                        $.ajax({
                            type: 'post',
                            url: '/Workflows/InsertWorkFlow',
                            processData: false,
                            contentType: false,
                            data: fd,
                            success: function (rs) {
                                if (rs.Error) {
                                    toastr["error"]("Thêm mới không thành công!");
                                } else {
                                    toastr["success"]("Thêm mới thành công!");
                                    //window.location.href = '/Home/Workflows?id=' + rs.id;
                                    $scope.$apply();
                                }
                            }
                        });
                        console.log($scope.model);
                    }
                    else if ($scope.model.WorkFlowCode !== null || $scope.model.WorkFlowCode !== "") {
                        toastr["error"]("Mã quy trình không được bỏ trống!");
                    }
                    else if ($scope.model.WorkFlowName !== null || $scope.model.WorkFlowName !== "") {
                        toastr["error"]("Tên quy trình không được bỏ trống!");
                    }
                }
                else {
                    if ($scope.model.WorkFlowCode !== null && $scope.model.WorkFlowCode !== ""
                        && $scope.model.WorkFlowName !== null && $scope.model.WorkFlowName !== "") {
                        $.ajax({
                            type: 'post',
                            url: '/Workflows/UpdateWorkFlow',
                            processData: false,
                            contentType: false,
                            data: fd,
                            success: function (rs) {
                                if (rs.Error) {
                                    toastr["error"](rs.Title);
                                } else {
                                    toastr["success"](rs.Title);
                                    $scope.$apply();
                                }
                            }
                        });
                    }
                    else if ($scope.model.WorkFlowCode !== null || $scope.model.WorkFlowCode !== "") {
                        toastr["error"]("Mã quy trình không được bỏ trống!");
                    }
                    else if ($scope.model.WorkFlowName !== null || $scope.model.WorkFlowName !== "") {
                        toastr["error"]("Tên quy trình không được bỏ trống!");
                    }
                }
            });
            editor.events.on("ShapeResize", function (id) {
                console.log('An item double-clicked');
            });

            setTimeout(function () {
                $(document).trigger('afterready');
            }, 1);
            $scope.Settings = [];

            $(document).on("click", ".dhx_diagram_connector", function (e) {
                return false;
            });

            $(document).on('click', '#vn_add', function () {
                $("#myModalFormPoperty").modal({ backdrop: 'static' }, 'show');
            });
            $scope.count = 1;
            $scope.ColumnType = 'chon';
            $(document).on('dblclick', '.dhx_diagram_flow_item', function () {
                var countConditonData = 1;
                var countConditionTrue = 1;
                var countConditionFalse = 1;
                $scope.Step = [];
                $('#addCondition').children('.row:not(:first)').remove();
                $scope.model2 = {};
                //Cột
                $scope.ColumneConfig = {
                    placeholder: '',
                    okCancelInMulti: false,
                    selected: []
                };
                $scope.ListColumn = [{ value: null, text: 'chọn' }];
                $.ajax({
                    type: 'post',
                    async: false,
                    url: '/Workflows/GetColumn',
                    data: { Id: $scope.model.TableGuid },
                    success: function (rs) {
                        if (rs.Error) {
                            toastr["error"](rs.Title);
                        } else {
                            angular.forEach(rs, function (val, key) {
                                $scope.ListColumn.push(val);
                            });
                            $scope.$apply();
                        }
                    }
                });
                $scope.ChangeColumn = function (data) {
                    $scope.model2.ColumnGuid = data;
                    $scope.ConditionDataJson = $scope.ListColumn.find(x => x.value === $scope.model2.ColumnGuid);
                    var a = JSON.parse($scope.ConditionDataJson.ConditionOfColumn);
                    $scope.ListConditionValue = [{ value: null, text: 'chọn' }];
                    angular.forEach(a, function (val, key) {
                        $scope.ListConditionValue.push(val);
                    });
                    //if ($scope.ConditionDataJson.DataType === 'varchar' || $scope.ConditionDataJson.DataType === 'nvarchar'
                    //    || $scope.ConditionDataJson.DataType === 'int' || $scope.ConditionDataJson.DataType === 'decimal') {
                    //    $scope.ColumnType = 'nhapgiatri';
                    //}
                    //if (($scope.ConditionDataJson.DataType === 'int' || $scope.ConditionDataJson.DataType === 'decimal') && $scope.model2.Condition === '7') {
                    //    $scope.ColumnType = 'trongkhoang';
                    //}
                    //if (($scope.ConditionDataJson.DataType === 'int' || $scope.ConditionDataJson.DataType === 'decimal') && $scope.model2.Condition === '7') {
                    //    $scope.ColumnType = 'trongkhoang';
                    //}
                    //if ($scope.ConditionDataJson.DataType === 'datetime' || $scope.ConditionDataJson.DataType === 'date') {
                    //    $scope.ColumnType = 'ngaythang';
                    //}
                    //console.log($scope.ConditionDataJson);
                };
                //Điều kiện toán tử
                $scope.ConditionConfig = {
                    placeholder: '',
                    okCancelInMulti: false,
                    selected: []
                };

                $scope.ListCondition = [
                    { value: null, text: 'chọn' },
                    { value: 1, text: '> (Lớn hơn)' },
                    { value: 2, text: '< (Nhỏ hơn)' },
                    { value: 3, text: '>= (Lớn hơn hoặc bằng)' },
                    { value: 4, text: '<= (Nhỏ hơn hoặc bằng)' },
                    { value: 5, text: '= (Bằng)' },
                    { value: 6, text: '<> (Khác)' },
                    { value: 7, text: 'Beteen (Trong khoảng)' }];

                $scope.ChangeCondition = function (data) {
                    $scope.model2.Condition = data;
                };


                $scope.ListConditionWConfig = {
                    placeholder: '',
                    okCancelInMulti: false,
                    selected: []
                };
                $scope.ListConditionW = [
                    { value: "W", text: 'Chờ duyệt' },
                    { value: "Y", text: 'Đã duyệt' },
                    { value: "N", text: 'Không duyệt' },
                    { value: "F", text: 'Đã chuyển tiếp' },
                    { value: "R", text: 'Đã trả lại' },
                    { value: "C", text: 'Bị hủy' },
                    { value: "D", text: 'Lưu nháp' },
                    { value: "O", text: 'Khác' }];

                $scope.ChangeListConditionW = function (data) {
                    $scope.model2.ConditionValue = data;
                };


                $scope.ChangeWFStatusId = function (data) {
                    $scope.model2.WFStatusId = data;
                };


                $scope.ChangeWFConditionId = function (data) {
                    $scope.model2.WFConditionId = data;
                };


                $.ajax({
                    type: 'get',
                    async: false,
                    url: '/Workflows/GetWorkFlowStatus',
                    data: {},
                    success: function (rs) {
                        if (rs.Error) {
                            toastr["error"](rs.Title);
                        } else {
                            $scope.ListWFStatusId = rs;
                            $scope.$apply();
                        }
                    }
                });
                // trạng thái trả lại WF
                $scope.WFStatusBackIdConfig = {
                    placeholder: '',
                    okCancelInMulti: false,
                    selected: []
                };
                $scope.ChangeWFStatusBackId = function (data) {
                    $scope.model2.WFStatusBackId = data;
                };


                $scope.ChangeWFConditionBackId = function (data) {
                    $scope.model2.WFConditionBackId = data;
                };



                //giá trị điều kiện
                $scope.ConditionValueConfig = {
                    placeholder: '',
                    okCancelInMulti: false,
                    selected: []
                };
                $scope.ListConditionValue = [];
                $scope.ChangeConditionValue = function (data) {
                    $scope.model2.ConditionValue = data;
                };
                //Điều kiện
                $scope.AndOrConfig = {
                    placeholder: '',
                    okCancelInMulti: false,
                    selected: []
                };
                $scope.ListAndOr = [
                    { value: null, text: 'chọn' },
                    { value: 'AND', text: 'AND' },
                    { value: 'OR', text: 'OR' }
                ];
                //add conditon
                $scope.AddCondition = function () {
                    angular.element(document.getElementById('addCondition')).append($compile('<div class="row"><div class="col-md-1"><select ng-model="model2.AndOr' + countConditonData + '" ng-smselect se-data="ListAndOr" se-change="ChangeAndOr' + countConditonData + '(data)" se-init="AndOrConfig"></select></div>' +
                        '<div class="col-md-4"> <select ng-model="model2.ColumnGuid' + countConditonData + '" ng-smselect se-data="ListColumn" se-change="ChangeColumn' + countConditonData + '(data)" se-init="ColumneConfig"></select></div > ' +
                        '<div class="col-md-3"><select ng-model="model2.Condition' + countConditonData + '" ng-smselect se-data="ListCondition" se-change="ChangeCondition' + countConditonData + '(data)" se-init="ConditionConfig"></select></div>' +
                        '<div class="col-md-4"><input class="form-control" ng-model="model2.ConditionValue' + countConditonData + '" /></div>')($scope));
                    countConditonData++;
                };

                var fieldp1 = 'Condition' + countConditonData;
                var field2 = 'ChangeColumn' + countConditonData;
                $scope[field2] = function (data) {
                    var fieldp2 = 'ColumnGuid' + countConditonData;
                    $scope.model2[fieldp2] = data;
                    $scope.ConditionDataJson = $scope.ListColumn.find(x => x.value === $scope.model2[fieldp2]);
                    var a = JSON.parse($scope.ConditionDataJson.ConditionOfColumn);
                    $scope.ListConditionValue = a;
                    $scope.model2[fieldp1] = 'null';
                };

                var field1 = 'ChangeCondition' + countConditonData;
                $scope[field1] = function (data) {
                    $scope.model2[fieldp1] = data;
                };

                var field3 = 'ChangeConditionValue' + countConditonData;
                $scope[field3] = function (data) {
                    var fieldp3 = 'ConditionValue' + countConditonData;
                    $scope.model2[fieldp3] = data;
                };
                var field4 = 'ChangeAndOr' + countConditonData;
                $scope[field4] = function (data) {
                    var fieldp4 = 'ChangeAndOr' + countConditonData;
                    $scope.model2[fieldp4] = data;
                };
                //End add condition

                var mail = "model2.Activity == 'mail'";
                var pd = "model2.Activity == 'pd'";
                var pdF = "model2.ActivityF == 'pd'";
                var mailF = "model2.ActivityF == 'mail'";
                //Add Activity True
                $scope.AddActivityTrue = function () {
                    angular.element(document.getElementById('condionTrue')).after($compile('<div class="row"><div class="col-md-4"><select ng-model="model2.Activity' + countConditionTrue + '" ng-smselect se-data="ListActivity" se-change="ChangeActivity' + countConditionTrue + '(data)" se-init="ActivityConfig"></select>' +
                        '<div ng-show="' + mail + '"><select ng-model="model2.Approver' + countConditionTrue + '" ng-smselect se-data="ListApprover" multiple se-change="ChangeApprover' + countConditionTrue + '(data)" se-init="ApproverConfig"></select> </div></div >' +
                        '<div class="col-md-4" ng-show="' + pd + '"><select ng-model="model2.ApproveType' + countConditionTrue + '" ng-smselect se-data="ListApproveType" se-change="ChangeApproveType' + countConditionTrue + '(data)" se-init="ApproveTypeConfig"></select></div>' +
                        '<div class="col-md-4" ng-show="' + pd + '" > <select ng-model="model2.Approver' + countConditionTrue + '" ng-smselect se-data="ListApprover" multiple se-change="ChangeApprover' + countConditionTrue + '(data)" se-init="ApproverConfig" ng-disabled="ChoseApproveType"></select></div >' +
                        '<div class="col-md-8" ng-show="' + mail + '"><textarea class="form-control" rows="3" ng-model="model2.ContentEmail' + countConditionTrue + '" placeholder="Nhập nội dung mẫu"></textarea></div></div>')($scope));
                    countConditionTrue++;
                };

                var fieldT = 'ChangeApprover' + countConditionTrue;
                $scope[fieldT] = function (data) {
                    var fielTdp2 = 'Approver' + countConditionTrue;
                    $scope.model2[fielTdp2] = data;
                };
                var fieldTT = 'ChangeApproveType' + countConditionTrue;
                $scope[fieldTT] = function (data) {
                    var fieldTTdp2 = 'ApproveType' + countConditionTrue;
                    $scope.model2[fieldTTdp2] = data;
                };
                var fieldAT = 'ChangeActivity' + countConditionTrue;
                $scope[fieldAT] = function (data) {
                    var fieldATdp2 = 'Activity' + countConditionTrue;
                    $scope.model2[fieldATdp2] = data;
                };

                //End add Activity True
                //Add Activity false
                $scope.AddActivityFalse = function () {
                    angular.element(document.getElementById('condionFalse')).after($compile('<div class="row"><div class="col-md-4"><select ng-model="model2.ActivityF' + countConditionFalse + '" ng-smselect se-data="ListActivity" se-change="ChangeActivityF' + countConditionTrue + '(data)" se-init="ActivityConfig"></select>' +
                        '<div ng-show="' + mailF + '"><select ng-model="model2.ApproverF' + countConditionFalse + '" ng-smselect se-data="ListApprover" multiple se-change="ChangeApproverF' + countConditionFalse + '(data)" se-init="ApproverConfig"></select></div></div>' +
                        '<div class="col-md-4" ng-show="' + pdF + '"> <select ng-model="model2.ApproveTypeF' + countConditionFalse + '" ng-smselect se-data="ListApproveType" se-change="ChangeApproveTypeF' + countConditionFalse + '(data)" se-init="ApproveTypeConfig"></select></div>' +
                        '<div class="col-md-4" ng-show="' + pdF + '"><select ng-model="model2.ApproverF' + countConditionFalse + '" ng-smselect se-data="ListApprover" multiple se-change="ChangeApproverF' + countConditionFalse + '(data)" se-init="ApproverConfig" ng-disabled="ChoseApproveType"></select></div>' +
                        '<div class="col-md-8" ng-show="' + mailF + '"> <textarea class="form-control" rows="3" ng-model="model2.ContentEmailF' + countConditionFalse + '" placeholder="Nhập nội dung mẫu"></textarea></div></div>')($scope));
                    countConditionFalse++;
                };
                var fieldF = 'ChangeApprover' + countConditionFalse;
                $scope[fieldF] = function (data) {
                    var fielFdp2 = 'Approver' + countConditionFalse;
                    $scope.model2[fielFdp2] = data;
                };
                var fieldFT = 'ChangeApproveType' + countConditionFalse;
                $scope[fieldFT] = function (data) {
                    var fieldFTdp2 = 'ApproveType' + countConditionFalse;
                    $scope.model2[fieldFTdp2] = data;
                };
                var fieldAF = 'ChangeActivityF' + countConditionTrue;
                $scope[fieldAT] = function (data) {
                    var fieldAFdp2 = 'ActivityF' + countConditionTrue;
                    $scope.model2[fieldAFdp2] = data;
                };
                //End Activity

                //Hành động
                $scope.model2.Activity = 'pd';
                $scope.model2.ActivityF = 'mail';
                $scope.ActivityConfig = {
                    placeholder: '',
                    okCancelInMulti: false,
                    selected: []
                };
                $scope.ListActivity = [
                    { value: null, text: 'chọn' },
                    { value: 'pd', text: 'Phê duyệt' },
                    { value: 'mail', text: 'Gửi Email' }
                ];

                $scope.ChangeActivity = function (data) {
                    $scope.model2.Activity = data;
                };
                $scope.ChangeActivityF = function (data) {
                    $scope.model2.ActivityF = data;
                };
                //Loại duyệt
                $scope.model2.ApproveType = 'null';
                if ($scope.model2.ApproveType === 'null') {
                    $scope.ChoseApproveType = true;
                }

                $scope.ListApproveType = [
                    { value: null, text: 'chọn' },
                    { value: '1', text: 'Chỉ định người duyệt' },
                    { value: '2', text: 'Người dùng tự nhập' }
                ];

                $scope.ChangeApproveType = function (data) {
                    $scope.model2.ApproveType = data;
                    if (data === '2' || data === 2 || data === 'null' || data === null) {
                        $scope.model2.Approver = [];
                        $scope.ChoseApproveType = true;
                        $scope.$apply();
                    }
                    if (data === '1' || data === 1) {
                        $scope.model2.Approver = [];
                        $scope.ChoseApproveType = false;
                        $scope.$apply();
                    }
                };
                //Người duyệt                                      
                $.ajax({
                    type: 'get',
                    async: false,
                    url: '/Workflows/GetAccountEmployees',
                    data: {},
                    success: function (rs) {
                        if (rs.Error) {
                            toastr["error"](rs.Title);
                        } else {
                            $scope.ListApprover = rs;
                            $scope.$apply();
                        }
                    }
                });
                $scope.ChangeApprover = function (data) {
                    $scope.model2.Approver = data;
                };
                $scope.ChangeApproverF = function (data) {
                    $scope.model2.ApproverF = data;
                };
                var jsonData = editor.serialize("Json");
                if ($(this).children('path').attr('d') === 'M 0 45 L 70 0 L 140 45 L 70 90 Z') {
                    var uidCodition = $(this).attr('dhx_id');
                    //if ($scope.model.Conditions.find(x => x.uidCodition === uidCodition) === undefined) {
                    var itemCondition = $scope.model.Conditions.find(x => x.uidCodition === uidCodition);
                    if (itemCondition !== null && itemCondition !== undefined) {
                        var ConditionGuid = itemCondition.ConditionGuid;
                        countConditonData = 1;
                        countConditionTrue = 1;
                        countConditionFalse = 1;
                        var dataJson = JSON.parse(itemCondition.ConditionData);
                        $scope.model2.ColumnGuid = dataJson[0].ColumnGuid;
                        $scope.model2.Condition = dataJson[0].Condition;
                        $scope.model2.ConditionValue = dataJson[0].ConditionValue;
                        for (var i = 1; i < dataJson.length; i++) {
                            var AndOr = 'AndOr' + countConditonData;
                            var ColumnGuid = 'ColumnGuid' + countConditonData;
                            var Condition = 'Condition' + countConditonData;
                            var ConditionValue = 'ConditionValue' + countConditonData;
                            $scope.model2[AndOr] = dataJson[i].AndOr;
                            $scope.model2[ColumnGuid] = dataJson[i].ColumnGuid;
                            $scope.model2[Condition] = dataJson[i].Condition;
                            $scope.model2[ConditionValue] = dataJson[i].ConditionValue;
                            $scope.AddCondition();
                        }
                        var trueJson = JSON.parse(itemCondition.ConditionTrue);
                        $scope.model2.Activity = trueJson[0].Activity;
                        $scope.model2.ApproveType = trueJson[0].ApproveType;
                        $scope.model2.Approver = trueJson[0].Approver;
                        $scope.model2.StepContinue = trueJson[0].StepContinue;
                        $scope.model2.WFStatusId = itemCondition.WFStatusId;
                        $scope.model2.WFStatusBackId = itemCondition.WFStatusBackId;
                        $scope.model2.WFConditionId = itemCondition.WFConditionId;
                        $scope.model2.WFConditionBackId = itemCondition.WFConditionBackId;
                        for (var j = 1; j < trueJson.length; i++) {
                            var Activity = 'Activity' + countConditionTrue;
                            var ApproveType = 'ApproveType' + countConditionTrue;
                            var Approver = 'Approver' + countConditionTrue;
                            var StepContinue = 'StepContinue' + countConditionTrue;
                            $scope.model2[Activity] = trueJson[i].Activity;
                            $scope.model2[ApproveType] = trueJson[i].ApproveType;
                            $scope.model2[Approver] = trueJson[i].Approver;
                            $scope.model2[StepContinue] = trueJson[i].StepContinue;
                            $scope.AddActivityTrue();
                        }
                        $scope.$apply();
                    }
                    var lstActionInCondition = jsonData.filter(x => x.from === uidCodition);
                    $scope.model2.StepContinue = '';
                    $scope.model2.StepCallback = '';

                    angular.forEach(lstActionInCondition, function (val, key) {
                        if (val.callBacks === 'previous')
                            $scope.model2.StepCallback += val.to + ',';
                        if (val.callBacks === 'next')
                            $scope.model2.StepContinue += val.to + ',';
                    });
                    $('#myModalTransition').modal({ backdrop: 'static' }, 'show');

                    $scope.SaveTransition = function () {
                        $scope.Conditions = {};
                        var itemData = [];
                        var itemTrue = [];
                        var itemFalse = [];
                        itemData.push({ ColumnGuid: $scope.model2.ColumnGuid, Condition: $scope.model2.Condition, ConditionValue: $scope.model2.ConditionValue, AndOr: $scope.model2.AndOr });
                        for (var i = 1; i < countConditonData; i++) {
                            var coulumn = 'ColumnGuid' + i;
                            var condition = 'Condition' + i;
                            var conditionValue = 'ConditionValue' + i;
                            var andOr = 'AndOr' + i;
                            itemData.push({ ColumnGuid: $scope.model2[coulumn], Condition: $scope.model2[condition], ConditionValue: $scope.model2[conditionValue], AndOr: $scope.model2[andOr] });
                        }
                        itemTrue.push({ Activity: $scope.model2.Activity, ApproveType: $scope.model2.ApproveType, Approver: $scope.model2.Approver, StepContinue: $scope.model2.StepContinue });
                        for (var j = 1; j < countConditionTrue; j++) {
                            var activity = 'Activity' + j;
                            var approveType = 'ApproveType' + j;
                            var approver = 'Approver' + j;
                            itemTrue.push({ Activity: $scope.model2[activity], ApproveType: $scope.model2[approveType], Approver: $scope.model2[approver], StepContinue: $scope.model2.StepContinue });
                        }
                        itemFalse.push({ Activity: $scope.model2.ActivityF, Approver: $scope.model2.ApproverF, ContentEmail: $scope.model2.ContentEmailF, StepCallback: $scope.model2.StepCallback });
                        for (var k = 1; k < countConditionTrue; k++) {
                            var activityF = 'ActivityF' + k;
                            var contentEmailF = 'ContentEmailF' + k;
                            var approverF = 'ApproverF' + k;
                            itemFalse.push({ Activity: $scope.model2[activityF], Approver: $scope.model2[approverF], ContentEmail: $scope.model2[contentEmailF], StepCallback: $scope.model2.StepCallback });
                        }
                        $scope.Conditions.ConditionData = JSON.stringify(itemData);
                        $scope.Conditions.ConditionTrue = JSON.stringify(itemTrue);
                        $scope.Conditions.ConditionFalse = JSON.stringify();
                        $scope.Conditions.uidCodition = uidCodition;
                        $scope.Conditions.ConditionGuid = ConditionGuid;
                        $scope.Conditions.StepContinue = $scope.model2.StepContinue;
                        $scope.Conditions.StepCallback = $scope.model2.StepCallback;
                        $scope.Conditions.WFStatusId = $scope.model2.WFStatusId;
                        $scope.Conditions.WFStatusBackId = $scope.model2.WFStatusBackId;
                        $scope.Conditions.WFConditionId = $scope.model2.WFConditionId;
                        $scope.Conditions.WFConditionBackId = $scope.model2.WFConditionBackId;
                        if (itemCondition !== null && itemCondition !== undefined) {
                            var index = $scope.model.Conditions.indexOf(itemCondition);
                            if (index !== -1) $scope.model.Conditions.splice(index, 1);

                        }
                        $scope.model.Conditions.push($scope.Conditions);
                        $('#myModalTransition').modal('hide');
                        console.log($scope.model);

                    };
                    //}
                    $scope.$apply();
                }
                else if ($(this).children('path').attr('d') === 'M 0,0 L 0,90 L 140,90 L 140,0 Z') {
                    $('#myModalActivity').modal('show');
                    var uidActivity = $(this).attr('dhx_id');
                    var uid = $scope.model.Activities.find(x => x.uidActivity === uidActivity);

                    $scope.stepStart = false;
                    var item = jsonData.filter(x => x.to === uidActivity);
                    var item1 = jsonData.filter(x => x.from === uidActivity);
                    if (item !== undefined) {
                        angular.forEach(item, function (val, key) {
                            var checkStart = jsonData.find(x => x.id === val.from);
                            if (checkStart !== undefined && checkStart.type === 'start')
                                $scope.stepStart = true;
                        });
                        angular.forEach(item1, function (val, key) {
                            var checkStart = jsonData.find(x => x.id === val.to);
                            if (checkStart !== undefined && checkStart.type === 'decision')
                                $scope.conditionStep = checkStart.id;
                        });
                    }
                    if (uid !== null && uid !== undefined) {
                        $scope.model2.ActivityName = uid.ActivityName;
                        $scope.$apply();
                    }
                    else {
                        $scope.model2.ActivityName = "";
                        $scope.$apply();
                    }

                    $scope.SaveActivity = function () {
                        if (uid !== null && uid !== undefined) {
                            $scope.model.Activities.ActivityName = uid.ActivityName;
                            $scope.model.Activities.find(x => x.uidActivity === uidActivity).ActivityName = $scope.model2.ActivityName;
                        } else {
                            $scope.Activity = {};
                            $scope.Activity = { ActivityName: $scope.model2.ActivityName, uidActivity: uidActivity, stepStart: $scope.stepStart, conditionStep: $scope.conditionStep };
                            $scope.model.Activities.push($scope.Activity);

                        }
                        $('#myModalActivity').modal('hide');
                    };
                }
            });
            $scope.Locaticon = function (item) {
                $scope.model.ActivityState = angular.uppercase(xoa_dau(item));
            };

        });

        $(document).bind('afterready', function () {
            var html = '<div class="vn_custom_block"><div id="custom"><span class="btn btn-default">Thêm thuộc tính</span></div></div>';
            var target = angular.element('.dhx_state_block');
            $(target).after(html);
        });

        $(document).on('click', '#custom', function () {
            $("#myModalFormPoperty").modal({ backdrop: 'static' }, 'show');
        });
        $scope.SaveProperty = function () {
            if ($scope.propertyform.validate()) {
                $("#myModalFormPoperty").modal('hide');
            }
        };
    }
]);

