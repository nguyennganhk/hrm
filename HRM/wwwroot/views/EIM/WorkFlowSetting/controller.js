﻿
var ctxfolder = "/views/EIM/WorkFlowSetting";
app.factory('dataservice', function ($http) {
    var headers = {
        "Content-Type": "application/json;odata=verbose",
        "Accept": "application/json;odata=verbose"
    };
    return {
        //------------------------*Store*-------------------------
        getItem: function (data, callback) {
            $http.post('/WorkFlowSetting/GetItem/' + data).success(callback);
        },
        delete: function (data, callback) {
            $http.post('/WorkFlowSetting/IsDelete?id=' + data).success(callback);
        },
        deleteItems: function (data, callback) {
            $http.post('/WorkFlowSetting/DeleteItems/', data).success(callback);
        },
        getWorkFlows: function (callback) {
            $http.post('/WorkFlowSetting/GetWorkFlows/').success(callback);
        }
    };
});
app.filter('html', ['$sce', function ($sce) {
    return function (text) {
        return $sce.trustAsHtml(text);
    };
}]);

app.directive('format', ['$filter', function ($filter) {
    return {
        require: '?ngModel',
        link: function (scope, elem, attrs, ctrl) {
            if (!ctrl) return;
            ctrl.$formatters.unshift(function (a) {
                return $filter(attrs.format)(ctrl.$modelValue);
            });
            ctrl.$parsers.unshift(function (viewValue) {
                var plainNumber = viewValue.replace(/[^\d|\-+|\.+]/g, '');
                elem.val($filter(attrs.format)(plainNumber));
                return plainNumber;
            });
        }
    };
}]);
//định dạng ngày
app.filter('fdate', [
    '$filter', function ($filter) {
        return function (input, f) {
            if (input && input.toString().indexOf('Date') > -1) {
                return moment(input).format(f);
            } if (input && input.toString().indexOf('T') > -1) {
                return moment(input).format(f);
            } else return input;
        };
    }
]);

app.controller('Ctrl_WorkFlowSetting', function ($scope, $rootScope, $sce, $compile, $uibModal, dataservice) {
    $rootScope.Title = "";
    $rootScope.go = function (path) {
        $location.path(path); return false;
    };

    $rootScope.validationOptions = {
        rules: {
            WorkFlowGuid: {
                required: true
            },
            TicketID: {
                required: true,
                maxlength: 50
            },
            TicketName: {
                required: true,
                maxlength: 250
            }
        },
        messages: {
            WorkFlowGuid: {
                required: "Yêu cầu chọn quy trình"
            },
            TicketID: {
                required: "Yêu cầu nhập mã phiếu",
                maxlength: "Mã phiếu không vượt quá 50 ký tự"
            },
            Description: {
                required: "Yêu cầu nhập tên phiếu",
                maxlength: "Tên phiếu không vượt quá 250 ký tự"
            }
        }
    };
    $rootScope.ListWorkFlows = [];
    dataservice.getWorkFlows(function (result) {
        $rootScope.ListWorkFlows = result;
    });
    $rootScope.WorkFlowsConfig = {
        placeholder: 'Chọn quy trình',
        search: true
    };
    $rootScope.CreateDate = function (date) {
        if (date !== null && date !== undefined) {
            var newdate = new Date(date);
            var month = newdate.getMonth() + 1;
            var day = newdate.getDate();
            var year = newdate.getFullYear();
            var hh = newdate.getHours();
            var mm = newdate.getMinutes();
            if (month < 10)
                month = "0" + month;
            if (day < 10)
                day = "0" + day;
            if (mm < 10)
                mm = "0" + mm;
            return todayDate = day + "/" + month + "/" + year;
        } else {
            return null;
        }
    };
    $rootScope.EditDate = function (datetime) {
        if (datetime !== null && datetime !== undefined) {
            var Str = datetime.toString();
            if (Str.indexOf("/Date") >= 0) {

                var newdate = new Date(parseInt(datetime.substr(6)));
                var month = newdate.getMonth() + 1;
                var day = newdate.getDate();
                var year = newdate.getFullYear();
                var hh = newdate.getHours();
                var mm = newdate.getMinutes();
                if (month < 10)
                    month = "0" + month;
                if (day < 10)
                    day = "0" + day;
                return year + "/" + month + "/" + day;
            } else {
                var value = datetime.split('/');

                return value[2] + '/' + value[1] + '/' + value[0];
            }
        }
        return null;
    };
});
app.config(function ($routeProvider, $validatorProvider) {
    $routeProvider
        .when('/', {
            templateUrl: ctxfolder + '/index.html',
            controller: 'index'
        })
        .when('/edit/:id', {
            templateUrl: ctxfolder + '/edit.html',
            controller: 'edit'
        })
        .when('/add/', {
            templateUrl: ctxfolder + '/add.html',
            controller: 'add'
        });
    $validatorProvider.setDefaults({
        errorElement: 'span',
        errorClass: 'help-block',
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        success: function (label) {
            label.closest('.form-group').removeClass('has-error');
        }
    });
});
//controller hiển thị, tìm kiếm
app.controller('index', function ($scope, $rootScope, $compile, $http, $confirm, $ngConfirm, $uibModal, DTOptionsBuilder, DTColumnBuilder, DTInstances, dataservice) {
    $scope.model = {};
    var vm = $scope;
    $scope.staticParam = {
        totalItems: 0,
        currentPage: 1,
        pageSize: 25,
        maxSize: 5,
        Search: "",
        IsActive: '1',
        Type: 'I'
    };
    $scope.TypeChange = function () {
        $scope.selectAll = false;
        $scope.Search();
    };
    $scope.IsActiveChange = function () {
        $scope.selectAll = false;
        $scope.Search();
    };
    $scope.selected = [];
    $scope.selectAll = false;
    $scope.toggleOne = toggleOne;
    $scope.toggleAll = toggleAll;
    var titleHtml = '<label class="mt-checkbox" ><input type="checkbox" ng-model="selectAll" ng-click="toggleAll(selectAll, selected)"/><span></span></label>';
    //if (App.Permissions.LISTVIEW)
    vm.dtOptions = DTOptionsBuilder.newOptions()
        .withPaginationType('full_numbers')
        .withDataProp('data').withDisplayLength(15)
        .withOption('order', [2, 'asc'])
        .withOption('serverSide', true)
        .withOption('pageLength', 25)
        .withOption('info', false)
        .withOption('stateLoadParams', function (settings, data) {
            data.search.search = '';
        })
        .withOption('headerCallback', function (header) {
            if (!$scope.headerCompiled) {
                $scope.headerCompiled = true;
                $compile(angular.element(header).contents())($scope);
            }
        })
        .withOption('initComplete', function (settings, json) {
            //thêm sự kiện scroll phân trang
            $('#tblData').parent().attr("onscroll", "angular.element(this).scope().LoadScroll(this)");
        })
        .withOption('createdRow', function (row, data, dataIndex) {
            const contextScope = $scope.$new(true);
            contextScope.data = data;
            $scope.data = data;
            $compile(row)($scope);
            contextScope.contextMenu = $scope.contextMenu;
            $compile(angular.element(row).find('input'))($scope);
            $compile(angular.element(row).attr('context-menu', 'contextMenu'))(contextScope);
        });
    // if (App.Permissions.LISTVIEW)
    vm.dtOptions.withOption('ajax', function (data, callback, settings) {
        data.search.value = $scope.staticParam.Search;
        if ($scope.staticParam.Type === "null") {
            data.Type = null;
        } else {
            data.Type = $scope.staticParam.Type;
        }
        if ($scope.staticParam.IsActive === "null") {
            data.IsActive = null;
        } else {
            data.IsActive = parseInt($scope.staticParam.IsActive);
        }
        App.blockUI({
            target: "#tblData",
            boxed: true,
            message: 'Đang tải...'
        });
        $http.post('/WorkFlowSetting/JTable', data)
            .success(function (res) {
                $scope.selected = [];
                if (data.start === 0) {
                    $scope.dataload = [];
                    angular.forEach(res.data, function (val, key) {
                        $scope.dataload.push(val);
                    });
                    callback({
                        data: $scope.dataload,
                        recordsTotal: res.recordsTotal,
                        recordsFiltered: res.recordsFiltered
                    });
                } else {
                    if (res.recordsTotal > $scope.dataload.length) {
                        angular.forEach(res.data, function (val, key) {
                            $scope.dataload.push(val);
                        });
                        callback({
                            data: $scope.dataload,
                            recordsTotal: res.recordsTotal,
                            recordsFiltered: res.recordsFiltered
                        });
                    }
                }
                App.unblockUI("#tblData");
            });
    });
    //scroll theo tỷ lệ màn hình
    vm.dtOptions.withOption('scrollY', '70vh')
        .withOption('scrollX', '100%') //mặc định 100%
        .withOption('scrollCollapse', true) //xóa khoảng trắng khi dữ liệu trong bảng ít
        .withOption('rowCallback', rowCallback) //sự kiên click vào 1 dòng
        .withOption('scroller', {
            loadingIndicator: true
        });
    //.withFixedColumns({
    //    leftColumns: 0 //fix bên trái 2 cột
    //    //rightColumns: 1 //fix bên phải 1 cột
    //}
    //);
    //table
    vm.dtColumns = [];
    vm.dtColumns.push(DTColumnBuilder.newColumn("RowGuid").withTitle(titleHtml).notSortable().renderWith(function (data, type, full, meta) {
        $scope.selected[full.RowGuid] = false;
        return '<label class="mt-checkbox"><input type="checkbox" ng-model="selected[\'' + full.RowGuid + '\']" ng-click="toggleOne(selected)"/><span></span></label>';
    }).withOption('sWidth', '5px').withOption('sClass', 'tcenter').withOption('sClass', 'tcenter-header'));
    vm.dtColumns.push(DTColumnBuilder.newColumn('RowGuid').withTitle('STT').notSortable().withOption('sWidth', '50px').renderWith(function (data, type, full, meta) {
        return meta.row + 1;
    }).withOption('sClass', 'tcenter tcenter-header'));
    vm.dtColumns.push(DTColumnBuilder.newColumn('TicketId').withTitle('Mã phiếu').withOption('sClass', 'mw130').withOption('sWidth', '150px').withOption('sClass', 'tcenter-header').renderWith(function (data, type, full, meta) {
        return '<a  style="color: #4285f4;" href="" ng-click="open(\'' + full.RowGuid + '\')" >' + data + '</a>';
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('TicketName').withTitle('Tên phiếu').withOption('sClass', 'mw130').withOption('sWidth', '350px').withOption('sClass', 'tcenter-header').renderWith(function (data, type, full, meta) {
        return data;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('WorkFlowName').withTitle('Quy trình').withOption('sClass', 'mw130').withOption('sClass', 'tcenter-header').renderWith(function (data, type, full, meta) {
        return data;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('OrderId').withTitle('Điều khiển').notSortable().withOption('sWidth', '100px').withOption('sClass', 'tcenter').renderWith(function (data, type, full, meta) {
        return '<div class="table__cell-actions-wrap">' +
            '<a ng-click="open(\'' + full.RowGuid + '\')" class="table__cell-actions-item table__cell-actions-icon"> <span class="mdi mdi-eye" title="Xem chi tiết"></span> </a>' +
            '<a ng-click="edit(\'' + full.RowGuid + '\')" class="table__cell-actions-item table__cell-actions-icon"> <span class="mdi mdi-table-edit" title="Sửa"></span> </a>' +
            '<a ng-click="delete (\'' + full.RowGuid + '\')" class="table__cell-actions-item table__cell-actions-icon"><span class="mdi mdi-delete" title="Xóa"></span> </a>';
    }));
    vm.reloadData = reloadData;
    vm.dtInstance = {};
    function reloadData(resetPaging) {
        $scope.staticParam.currentPage = resetPaging;
        vm.dtInstance.reloadData(callback, resetPaging);
    }
    function callback(json) {

    }
    function toggleOne(selectedItems) {
        for (var id in selectedItems) {
            if (selectedItems.hasOwnProperty(id)) {
                if (!selectedItems[id]) {
                    vm.selectAll = false;
                    return;
                }
            }
        }
        vm.selectAll = true;
    }
    function toggleAll(selectAll, selectedItems) {
        for (var id in selectedItems) {
            if (selectedItems.hasOwnProperty(id)) {
                selectedItems[id] = selectAll;
            }
        }
    }
    function rowCallback(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        $('td', nRow).unbind('dblclick');
        $('td', nRow).bind('dblclick', function () {
            $scope.$apply(function () {
                $scope.open(aData.RowGuid);
            });
        });
        return nRow;

    }
    //Load dữ liệu cuộn
    $scope.total = 0;
    $scope.check = false;
    $scope.LoadScroll = function (obj) {
        var total = obj.offsetHeight + obj.scrollTop;
        if (0.5 + obj.offsetHeight + obj.scrollTop >= obj.scrollHeight && $scope.check && $scope.total < (total + 10)) {
            $scope.check = false;
            vm.dtInstance.DataTable.page('next').draw('page');
        }
        if (obj.offsetHeight + obj.scrollTop >= obj.scrollHeight) {
            $scope.total = obj.offsetHeight + obj.scrollTop;
        }
        else {
            $scope.check = true;
        }
    };
    //tìm kiếm
    $scope.reload = function () {
        $rootScope.IsActive = $scope.staticParam.IsActive;
        $rootScope.Type = $scope.staticParam.Type;
        $scope.dtInstance.DataTable.search($scope.staticParam.Search);
        $scope.dtInstance.DataTable.search($scope.staticParam.Search).draw();
    };
    $scope.reloadAll = function () {
        $scope.staticParam.Search = "";
        $scope.selectAll = false;
        $scope.staticParam.IsActive = '1';
        $scope.staticParam.Type = 'I';
        reloadData(true);
        $scope.dtInstance.DataTable.search($scope.staticParam.Search);
        $scope.dtInstance.DataTable.search($scope.staticParam.Search).draw();
    };
    $rootScope.Search = function () {
        reloadData(true);
    };
    // thêm mới
    $scope.add = function () {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/add.html',
            controller: 'add',
            backdrop: 'static',
            keyboard: false,
            size: '60'
        });
        modalInstance.result.then(function (d) {
        }, function () {
        });
    };
    // chi tiết
    $scope.open = function (temp) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/open.html',
            controller: 'open',
            backdrop: 'static',
            size: '60',
            resolve: {
                para: function () {
                    return temp;
                }
            }
        });
        modalInstance.result.then(function (d) {
        }, function () {
        });

    };
    // sửa 
    $scope.edit = function (temp) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/edit.html',
            controller: 'edit',
            backdrop: 'static',
            size: '60',
            resolve: {
                para: function () {
                    return temp;
                }
            }
        });
        modalInstance.result.then(function (d) {
        }, function () {
        });

    };
    //Xóa 1 bản ghi
    $scope.delete = function (temp) {
        dataservice.getItem(temp, function (rs) {
            ESConfirm([{ 'value': 1, 'title': 'Xác nhận', 'type': 'info' }, { 'value': 2, 'title': 'Đóng', 'type': 'danger' }], { 'description': 'Bạn có muốn xóa ' + rs.PositionName, 'class': 'eswarning_v2' }, function (rs) {
                if (rs === '1') {
                    App.blockUI({
                        target: "#contentMain",
                        boxed: true,
                        message: 'Đang tải...'
                    });
                    dataservice.delete(temp, function (result) {
                        if (result.Error) {
                            App.notifyDanger(result.Title);
                        } else {
                            App.notifyInfo(result.Title);
                            $rootScope.Search();
                        }
                        App.unblockUI("#contentMain");
                    });
                }
            }, function () {
            });
        });
    };
    // xóa nhiều bản ghi
    $scope.deleteChecked = function () {
        var deleteItems = [];
        for (var id in $scope.selected) {
            if ($scope.selected.hasOwnProperty(id)) {
                if ($scope.selected[id]) {
                    deleteItems.push(id);
                }
            }
        }
        if (deleteItems.length > 0) {
            ESConfirm([{ 'value': 1, 'title': 'Xác nhận', 'type': 'info' }, { 'value': 2, 'title': 'Đóng', 'type': 'danger' }], { 'description': 'Bạn có muốn xóa những cài đặt này không?', 'class': 'eswarning_v2' }, function (rs) {
                if (rs === '1') {
                    App.blockUI({
                        target: "#contentMain",
                        boxed: true,
                        message: 'Đang tải...'
                    });
                    dataservice.deleteItems(deleteItems, function (result) {
                        if (result.Error) {
                            App.notifyDanger(result.Title);
                        } else {
                            App.notifyInfo(result.Title);
                            $scope.selectAll = false;
                            $rootScope.Search();
                        }
                        App.unblockUI("#contentMain");
                    });
                }
            }, function () {
            });
        } else {
            App.notifyDanger("Không có bản ghi nào được chọn");
        }
    };
    //thêm sưa xóa menu chuột phải
    $scope.contextMenu = [
        //xem chi tiết trong menu
        [function ($itemScope) {
            return '<i class="mdi mdi-eye"></i> Xem cài đặt';
        }, function ($itemScope, $event, model) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: ctxfolder + '/open.html',
                controller: 'open',
                backdrop: true,
                size: '60',
                resolve: {
                    para: function () {
                        return $itemScope.data.RowGuid;
                    }
                }
            });
            modalInstance.result.then(function (d) {
                $rootScope.Search();
            }, function () {
            });
        }],
        //sửa trong menu
        [function ($itemScope) {
            return '<i class="mdi mdi-table-edit"></i> Sửa cài đặt';
        }, function ($itemScope, $event, model) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: ctxfolder + '/edit.html',
                controller: 'edit',
                backdrop: 'static',
                size: '60',
                resolve: {
                    para: function () {
                        return $itemScope.data.RowGuid;
                    }
                }
            });
            modalInstance.result.then(function (d) {
                $rootScope.Search();
            }, function () {
            });
        }],
        //xóa trong menu
        [function ($itemScope) {
            return '<i class="mdi mdi-delete"></i> Xóa cài đặt';
        }, function ($itemScope, $event, model) {
            ESConfirm([{ 'value': 1, 'title': 'Xác nhận', 'type': 'info' }, { 'value': 2, 'title': 'Đóng', 'type': 'danger' }], { 'description': 'Bạn có muốn xóa cài đặt này không?', 'class': 'eswarning_v2' }, function (rs) {
                if (rs === '1') {
                    App.blockUI({
                        target: "#contentMain",
                        boxed: true,
                        message: 'Đang tải...'
                    });
                    dataservice.delete($itemScope.data.RowGuid, function (result) {
                        if (result.Error) {
                            App.notifyDanger(result.Title);
                        } else {
                            App.notifyInfo(result.Title);
                            $rootScope.Search();
                        }
                        App.unblockUI("#contentMain");
                    });
                }
            }, function () {
            });
        }]
    ];
});
// controller Add
app.controller('add', function ($scope, $rootScope, $uibModalInstance, ) {
    $scope.Title = "Thêm mới cài đặt";
    $scope.model = { IsActive: 1 };
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
    $scope.ChangeWorkFlows = function (data) {
        $scope.model.WorkFlowGuid = data;
        var rs = $scope.ListWorkFlows.find(x => x.value === data);
        if (rs !== undefined) {
            $scope.model.WorkFlowName = rs.text;
        }
    };
    //Store
    $scope.insert = function () {
        if ($scope.addform.validate()) {
            if ($scope.model.TicketName.trim() === "") {
                App.notifyDanger("Yêu cầu nhập tên phiếu");
                return;
            }
            var fd = new FormData();
            fd.append('insert', JSON.stringify($scope.model));
            $.ajax({
                type: "POST",
                url: "/WorkFlowSetting/Insert",
                contentType: false,
                processData: false,
                data: fd,
                success: function (rs) {
                    if (rs.Error) {
                        App.notifyDanger(rs.Title);
                    } else {
                        App.notifyInfo(rs.Title);
                        $uibModalInstance.close();
                        $rootScope.Search();
                    }
                }
            });
        }
    };
});
// controller edit
app.controller('edit', function ($scope, $rootScope, $uibModalInstance, dataservice, para) {
    $scope.Title = "Sửa cài đặt";
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
    $scope.ChangeWorkFlows = function (data) {
        $scope.model.WorkFlowGuid = data;
        var rs = $scope.ListWorkFlows.find(x => x.value === data);
        if (rs !== undefined) {
            $scope.model.WorkFlowName = rs.text;
        }
    };
    $scope.initData = function () {
        dataservice.getItem(para, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {
                $scope.model = rs;
            }
        });
    };
    $scope.initData();
    //store
    $scope.edit = function () {
        if ($scope.editform.validate()) {
            if ($scope.model.TicketName.trim() === "") {
                App.notifyDanger("Yêu cầu nhập tên phiếu");
                return;
            }
            var fd = new FormData();
            fd.append('update', JSON.stringify($scope.model));
            $.ajax({
                type: "POST",
                url: "/WorkFlowSetting/Update",
                contentType: false,
                processData: false,
                data: fd,
                success: function (rs) {
                    if (rs.Error) {
                        App.notifyDanger(rs.Title);
                    } else {
                        App.notifyInfo(rs.Title);
                        $uibModalInstance.close();
                        $rootScope.Search();
                    }
                }
            });
        }
    };
});
// controller open
app.controller('open', function ($scope, $uibModalInstance, dataservice, para) {
    $scope.Title = "Xem cài đặt";
    $scope.model = {};
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
    $scope.loadData = function () {

        dataservice.getItem(para, function (rs) {
            $scope.model = rs;
            if (rs.CreatedBy !== null) {
                $scope.model.CreatedBy = $scope.model.CreatedBy.split('#')[1];
            }
            if (rs.ModifiedBy !== null) {
                $scope.model.ModifiedBy = $scope.model.ModifiedBy.split('#')[1];
            }
        });
    };
    $scope.loadData();
}); 
