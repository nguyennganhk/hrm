﻿var ctxfolder = "/views/Client/Product";
app.factory('dataservice', function ($http) {
    var headers = {
        "Content-Type": "application/json;odata=verbose",
        "Accept": "application/json;odata=verbose"
    };
    return {
        //------------------------*Store*-------------------------     
        getDashboard: function (data, callback) {
            $http.post('/Hr/Home/getDashboard', data).success(callback);
        },
        getItemReason: function (callback) {
            $http.post('/Hr/Home/ReportResonApply/').success(callback);
        },
    };
});

app.config(function ($routeProvider, $validatorProvider) {
    $routeProvider
        .when('/', {
            templateUrl: ctxfolder + '/index.html',
            controller: 'index'
        })
        .when('/edit/:id', {
            templateUrl: ctxfolder + '/edit.html',
            controller: 'edit'
        }).when('/open/:id', {
            templateUrl: ctxfolder + '/open.html',
            controller: 'open'
        })
        .when('/add/', {
            templateUrl: ctxfolder + '/add.html',
            controller: 'add'
        })
    $validatorProvider.setDefaults({
        errorElement: 'span',
        errorClass: 'help-block',
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        success: function (label) {
            label.closest('.form-group').removeClass('has-error');
        }
    });
});
app.controller('Ctrl_Client_Product', function ($scope, $rootScope, $compile, dataservice) {
    $rootScope.StatusData = [{
        value: 1,
        text: 'Dự án mới'
    }, {
        value: 2,
        text: 'Đang triển khai'
    }
        , {
        value: 3,
        text: 'Hoàn thành'
    }
    ];


    $rootScope.CreateDate = function (date) {
        if (date !== null && date !== undefined) {
            var newdate = new Date(date);
            var month = newdate.getMonth() + 1;
            var day = newdate.getDate();
            var year = newdate.getFullYear();
            var hh = newdate.getHours();
            var mm = newdate.getMinutes();
            if (month < 10)
                month = "0" + month;
            if (day < 10)
                day = "0" + day;
            if (mm < 10)
                mm = "0" + mm;
            return todayDate = day + "/" + month + "/" + year;
        } else {
            return null;
        }
    }

    //0 11/11/2017
    //1 11/11/2017 22:22
    //2 123123123
    //3 type date
    //4 type data year month day
    $rootScope.ConDate = function (data, number) {
        try {
            if (data == null || data == "") {
                return '';
            }
            if (data !== null && data != "" && data != undefined) {
                try {
                    if (data.indexOf("SA") != -1 || data.indexOf("CH") != -1) {
                        if (data.indexOf("SA") != -1) { }
                        if (data.indexOf("CH") != -1) { }
                    }

                    if (data.indexOf('Date') != -1) {
                        data = data.substring(data.indexOf("Date") + 5);
                        data = parseInt(data);
                    }
                }
                catch (ex) { }
                var newdate = new Date(data);
                if (number == 3) {
                    return newdate;
                }
                if (number == 2) {
                    return "/Date(" + newdate.getTime() + ")/";
                }
                var month = newdate.getMonth() + 1;
                var day = newdate.getDate();
                var year = newdate.getFullYear();
                var hh = newdate.getHours();
                var mm = newdate.getMinutes();
                if (month < 10)
                    month = "0" + month;
                if (day < 10)
                    day = "0" + day;
                if (mm < 10)
                    mm = "0" + mm;
                if (number == 0) {
                    return todayDate = day + "/" + month + "/" + year;
                }
                if (number == 1) {
                    return todayDate = day + "/" + month + "/" + year + " " + hh + ":" + mm;
                }
                if (number == 4) {
                    return new Date(year, month - 1, day);
                }
            } else {
                return '';
            }
        } catch (ex) {
            return "";
        }
    }
    //định dạng số xóa dấu chấm phẩy
    $rootScope.addPeriod = function (nStr) {
        if (nStr !== null && nStr !== "" && nStr !== undefined) {
            nStr = Math.round(parseFloat(nStr) * 100) / 100;

            nStr += "";
            if (nStr.indexOf(",") >= 0) {
                var x = nStr.split(",");
            } else {
                var x = nStr.split(".");
            }
            var x1 = x[0];
            var x2 = x.length > 1 && parseInt(x[1]) > 0 ? "," + x[1] : "";
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, "$1" + "." + "$2");
            }
            var Total = x1 + x2;
            return Total;
        }
        else {
            return 0;
        }
    };
    //----------Bắt đầu lấy tuần trong năm-------------------
    // lấy số tuần từ 1 năm
    $rootScope.getISOWeeks = function (y) {
        var d, isLeap;

        d = new Date(y, 0, 1);
        isLeap = new Date(y, 1, 29).getMonth() === 1;

        //check for a Jan 1 that's a Thursday or a leap year that has a 
        //Wednesday jan 1. Otherwise it's 52
        return d.getDay() === 4 || isLeap && d.getDay() === 3 ? 53 : 52
    }
    // lấy danh sách tuần từ 1 năm
    $rootScope.getWeek = function (data) {
        $rootScope.WeekData = [];
        //$rootScope.WeekData.push({ value: null, text: "Bỏ chọn" });
        for (var i = 1; i <= data; i++) {
            var ob = { value: i, text: 'Tuần ' + i.toString() };
            $rootScope.WeekData.push(ob);
        }
        if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
            $scope.$apply();
        }
    }
    // lấy danh sách tuần từ 1 năm
    $rootScope.getDateOfISOWeek = function (w, y) {
        var simple = new Date(y, 0, 1 + (w - 1) * 7);
        var dow = simple.getDay();
        var ISOweekStart = simple;
        if (dow <= 4)
            ISOweekStart.setDate(simple.getDate() - simple.getDay() + 1);
        else
            ISOweekStart.setDate(simple.getDate() + 8 - simple.getDay());
        return ISOweekStart;
    }

    $rootScope.YearData = []; $rootScope.MonthData = [];
    //$rootScope.MonthData.push({ value: null, text: "Bỏ chọn" });
    for (var i = 2018; i < 2050; i++) {
        var ob = { value: i, text: 'Năm ' + i.toString() };
        $rootScope.YearData.push(ob);
    }
    for (var i = 1; i <= 12; i++) {
        var ob = { value: i, text: 'Tháng ' + i.toString() };
        $rootScope.MonthData.push(ob);
    }
    //----------Kết thúc lấy tuần trong năm-------------------
});
app.controller('index', function ($http, $scope, $rootScope, $compile, $confirm, $uibModal, DTOptionsBuilder, DTColumnBuilder, DTInstances, dataservice, keyhandle, utility) {
  
    $rootScope.txtTitle22 = "SP ABCXYZ";
  
    // chi tiết
    $scope.openAnnouncements = function (temp) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: '/views/Cooperation/Announcements/open.html',
            controller: 'openAnnouncements',
            backdrop: 'static',
            size: '80',
            resolve: {
                para: function () {
                    return temp;
                }
            }
        });
        modalInstance.result.then(function (d) {
        }, function () {
        });

    };
  

});

