﻿var ctxfolder = "/views/Client/Home";
//var app = angular.module('App_ESHRM_Contracts', ["ui.bootstrap", "ngRoute", "ngValidate", "datatables", "datatables.bootstrap", 'datatables.colvis', "ui.bootstrap.contextMenu", 'datatables.colreorder', 'angular-confirm', 'ui.select', 'angularjs-datetime-picker', 'angularFileUpload', 'summernote']);

app.factory('dataservice', function ($http) {
    var headers = {
        "Content-Type": "application/json;odata=verbose",
        "Accept": "application/json;odata=verbose"
    };
    return {
        getDashboard: function (data, callback) {
            $http.post('/Hr/Home/getDashboard', data).success(callback);
        },
        GetSanPhams: function (data, callback) {
            $http.post('/Home/GetSanPhams/', data).success(callback);
        },
        GetGroupSanPham: function (callback) {
            $http.post('/Home/GetGroupSanPham/').success(callback);
        },
       
        getItem: function (data, callback) {
            $http.post('/Home/GetItem', data).success(callback);
        },
    };
});

app.config(function ($routeProvider, $validatorProvider) {
    $routeProvider
        .when('/', {
            templateUrl: ctxfolder + '/index.html',
            controller: 'index'
        })
        .when('/edit/:id', {
            templateUrl: ctxfolder + '/edit.html',
            controller: 'edit'
        }).when('/open/:id', {
            templateUrl: ctxfolder + '/open.html',
            controller: 'open'
        })
        .when('/add/', {
            templateUrl: ctxfolder + '/add.html',
            controller: 'add'
        })
    $validatorProvider.setDefaults({
        errorElement: 'span',
        errorClass: 'help-block',
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        success: function (label) {
            label.closest('.form-group').removeClass('has-error');
        }
    });
});
app.controller('Ctrl_Client_Home', function ($scope, $rootScope, $compile, dataservice) {
    $rootScope.StatusData = [{
        value: 1,
        text: 'Dự án mới'
    }, {
        value: 2,
        text: 'Đang triển khai'
    }
        , {
        value: 3,
        text: 'Hoàn thành'
    }
    ];


    $rootScope.CreateDate = function (date) {
        if (date !== null && date !== undefined) {
            var newdate = new Date(date);
            var month = newdate.getMonth() + 1;
            var day = newdate.getDate();
            var year = newdate.getFullYear();
            var hh = newdate.getHours();
            var mm = newdate.getMinutes();
            if (month < 10)
                month = "0" + month;
            if (day < 10)
                day = "0" + day;
            if (mm < 10)
                mm = "0" + mm;
            return todayDate = day + "/" + month + "/" + year;
        } else {
            return null;
        }
    }

    //0 11/11/2017
    //1 11/11/2017 22:22
    //2 123123123
    //3 type date
    //4 type data year month day
    $rootScope.ConDate = function (data, number) {
        try {
            if (data == null || data == "") {
                return '';
            }
            if (data !== null && data != "" && data != undefined) {
                try {
                    if (data.indexOf("SA") != -1 || data.indexOf("CH") != -1) {
                        if (data.indexOf("SA") != -1) { }
                        if (data.indexOf("CH") != -1) { }
                    }

                    if (data.indexOf('Date') != -1) {
                        data = data.substring(data.indexOf("Date") + 5);
                        data = parseInt(data);
                    }
                }
                catch (ex) { }
                var newdate = new Date(data);
                if (number == 3) {
                    return newdate;
                }
                if (number == 2) {
                    return "/Date(" + newdate.getTime() + ")/";
                }
                var month = newdate.getMonth() + 1;
                var day = newdate.getDate();
                var year = newdate.getFullYear();
                var hh = newdate.getHours();
                var mm = newdate.getMinutes();
                if (month < 10)
                    month = "0" + month;
                if (day < 10)
                    day = "0" + day;
                if (mm < 10)
                    mm = "0" + mm;
                if (number == 0) {
                    return todayDate = day + "/" + month + "/" + year;
                }
                if (number == 1) {
                    return todayDate = day + "/" + month + "/" + year + " " + hh + ":" + mm;
                }
                if (number == 4) {
                    return new Date(year, month - 1, day);
                }
            } else {
                return '';
            }
        } catch (ex) {
            return "";
        }
    }
    //định dạng số xóa dấu chấm phẩy
    $rootScope.addPeriod = function (nStr) {
        if (nStr !== null && nStr !== "" && nStr !== undefined) {
            nStr = Math.round(parseFloat(nStr) * 100) / 100;

            nStr += "";
            if (nStr.indexOf(",") >= 0) {
                var x = nStr.split(",");
            } else {
                var x = nStr.split(".");
            }
            var x1 = x[0];
            var x2 = x.length > 1 && parseInt(x[1]) > 0 ? "," + x[1] : "";
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, "$1" + "." + "$2");
            }
            var Total = x1 + x2;
            return Total;
        }
        else {
            return 0;
        }
    };
    //----------Bắt đầu lấy tuần trong năm-------------------
    // lấy số tuần từ 1 năm
    $rootScope.getISOWeeks = function (y) {
        var d, isLeap;

        d = new Date(y, 0, 1);
        isLeap = new Date(y, 1, 29).getMonth() === 1;

        //check for a Jan 1 that's a Thursday or a leap year that has a 
        //Wednesday jan 1. Otherwise it's 52
        return d.getDay() === 4 || isLeap && d.getDay() === 3 ? 53 : 52
    }
    // lấy danh sách tuần từ 1 năm
    $rootScope.getWeek = function (data) {
        $rootScope.WeekData = [];
        //$rootScope.WeekData.push({ value: null, text: "Bỏ chọn" });
        for (var i = 1; i <= data; i++) {
            var ob = { value: i, text: 'Tuần ' + i.toString() };
            $rootScope.WeekData.push(ob);
        }
        if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
            $scope.$apply();
        }
    }
    // lấy danh sách tuần từ 1 năm
    $rootScope.getDateOfISOWeek = function (w, y) {
        var simple = new Date(y, 0, 1 + (w - 1) * 7);
        var dow = simple.getDay();
        var ISOweekStart = simple;
        if (dow <= 4)
            ISOweekStart.setDate(simple.getDate() - simple.getDay() + 1);
        else
            ISOweekStart.setDate(simple.getDate() + 8 - simple.getDay());
        return ISOweekStart;
    }

    $rootScope.YearData = []; $rootScope.MonthData = [];
    //$rootScope.MonthData.push({ value: null, text: "Bỏ chọn" });
    for (var i = 2018; i < 2050; i++) {
        var ob = { value: i, text: 'Năm ' + i.toString() };
        $rootScope.YearData.push(ob);
    }
    for (var i = 1; i <= 12; i++) {
        var ob = { value: i, text: 'Tháng ' + i.toString() };
        $rootScope.MonthData.push(ob);
    }
    //----------Kết thúc lấy tuần trong năm-------------------
});
app.controller('index', function ($http, $scope, $rootScope, $compile, $confirm, $uibModal, DTOptionsBuilder, DTColumnBuilder, DTInstances, dataservice, keyhandle, utility) {
    $scope.model = { CurrentPage: 1, Length: 50, recordsTotal: 0, GroupSanPhamGuid: null, Keyword: "" };
    $rootScope.SPABC = "sản phẩm ABC";
    $scope.Searchproduct = false;
    $scope.Filterproduct = false;
    $scope.Filter = function () {
        if ($scope.Filterproduct == false) {
            $scope.Filterproduct = true;
        } else {
            $scope.Filterproduct = false;
        }
    };
    $scope.Search = function () {
        if ($scope.Searchproduct == false) {
            $scope.Searchproduct = true;
        } else {
            $scope.Searchproduct = false;
        }
    };
    $rootScope.lstSanpham = [];
    $scope.ConvertNumber = function (data) {
        return parseFloat(data);
    }
    $scope.GetSanPhams = function () {
        var ob = {
            GroupSanPhamGuid: $scope.model.GroupSanPhamGuid,
            Title: $scope.model.Keyword,
            CurrentPage: $scope.model.CurrentPage,
            Length: $scope.model.Length
        }
        dataservice.GetSanPhams(ob, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
                return;
            }
            else {
                $scope.model.recordsTotal = rs.recordsTotal;
                if ($scope.model.CurrentPage == 1) {
                    $rootScope.lstSanpham = [];
                    if (rs.data.length > 0) {
                        $rootScope.lstSanpham = rs.data;
                    }
                }
                else {
                    if (rs.recordsTotal > $rootScope.lstSanpham.length) {
                        angular.forEach(rs.data, function (value, key) {
                            $rootScope.lstSanpham.push(value);
                        });
                    }
                    if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                        $scope.$apply();
                    }
                }
            }
        });
    }
    $scope.LoadMore = function () {
        if ($scope.model.recordsTotal > $rootScope.lstSanpham.length) {
            $scope.model.CurrentPage += 1;
            $scope.GetSanPhams();
        }
    }

    //Lấy dữ liệu
    $scope.initData = function () {
        $scope.GetSanPhams();
        dataservice.GetGroupSanPham(function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
                return;
            }
            else {
                $rootScope.lstGroupSanPham = rs;
                var ob = {
                    RowGuid: null,
                    TenNhom: "Tất cả"
                }
                $rootScope.lstGroupSanPham.push(ob);
                $rootScope.lstGroupSanPham.reverse();
                if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                    $scope.$apply();
                }
            }
        });
    };
    $scope.initData();

    $scope.ClickGroup = function (data) {
        $scope.model.GroupSanPhamGuid = data.GroupSanPhamGuid;
        $scope.model.CurrentPage = 1;
        $scope.GetSanPhams();
    };

    
    $scope.open_PE = function (temp) {
        if (temp !== "" && temp !== undefined && temp !== null) {
            window.open("/HR/Employees/Index#/?" + "OPEN#" + temp, "_parent");
        } else {
            App.notifyDanger("Không có nhân viên nào được chọn");
            return;
        }
    };
    $scope.edit = function (Id) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/edit.html',
            controller: 'edit',
            backdrop: 'static',
            keyboard: false,
            size: '90',
            resolve: {
                para: function () {
                    return Id;
                }
            }
        });
        modalInstance.result.then(function (d) {
            $scope.reload();
        }, function () {
        });
    }
    $scope.InReport = function (item) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: '/views/HR/ViewSalaries/report.html',
            controller: 'report',
            backdrop: 'static',
            size: '90',
            resolve: {
                para: function () {
                    return {
                        DepartmentGuid: $scope.model.DepartmentGuid,
                        Year: $scope.model.Year,
                        Month: $scope.model.Month
                    };
                }
            }
        });
        modalInstance.result.then(function (d) {
        }, function () {
        });
    };
    $rootScope.removeLine = function (string) {
        if (string !== null && string !== "") {
            var Str = string.replace(/\n/g, ' ');
            Str = Str.replace(/<\/?[^>]+(>|$)/g, "");
            return Str;
        }
    };




    // chi tiết
    $scope.openAnnouncements = function (temp) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: '/views/Cooperation/Announcements/open.html',
            controller: 'openAnnouncements',
            backdrop: 'static',
            size: '80',
            resolve: {
                para: function () {
                    return temp;
                }
            }
        });
        modalInstance.result.then(function (d) {
        }, function () {
        });

    };
    // chi tiết 
    $scope.openCalendar = function (temp) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: '/views/Cooperation/CalendarLeaders/open.html',
            controller: 'openCalendar',
            backdrop: 'static',
            size: '80',
            resolve: {
                para: function () {
                    return temp;
                }
            }
        });
        modalInstance.result.then(function (d) {
        }, function () {
        });

    };
    ///////////////////////// chuyển đôit tab //////////////////////
    $scope.tab = function (item) {
        if (item === 1) {
            $("#settings").addClass("active");
            $("#changepass").removeClass("active");
        }
        else {
            $("#settings").removeClass("active");
            $("#changepass").addClass("active");
        }
    };

});

app.controller('edit', function ($scope, $rootScope, $compile, $uibModal, $confirm, $uibModalInstance, FileUploader, dataservice, para) {
    $scope.Title = "Sửa sản phẩm";
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    }
    $scope.model = {};

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    }
    $scope.model.FileAttachments = [];
    var uploader = $scope.uploader = new FileUploader({
        // url: App.baseService() + '/upload'
    });
    uploader.filters.push({
        name: "customFilter",
        fn: function (item /*{File|FileLikeObject}*/, options) {
            return this.queue.length < 10;
        }
    });
    uploader.onSuccessItem = function (fileItem, response, status, headers) {
        $scope.jmodel.FileAttachments.push(response);
    };
    $scope.jmodelPer = {};
    uploader.onCompleteAll = function () {
        $scope.commit();
    };
    $scope.numberNext = 0;
    $scope.btnNext = function () {
        if ($scope.numberNext == 3) {
            if ($scope.AnhSanPhamViews.Title1 !== null && $scope.AnhSanPhamViews.Title1 !== "") {
                $scope.model.Title1 = "/Home/GetPic3_Home/" + $scope.model.RowGuid;
            } else {
                $scope.model.Title1 = "/images/no-image-icon.jpg";
            }
            $scope.numberNext = 0;
        } else {
            $scope.numberNext += 1;
        }
        if ($scope.numberNext ==1) { 
            if ($scope.AnhSanPhamViews.Title1 !== null && $scope.AnhSanPhamViews.Title1 !== "") {
                $scope.model.Title1 = "/Home/GetPic1_Home/" + $scope.model.RowGuid;
            } else {
                $scope.model.Title1 = "/images/no-image-icon.jpg";
            }
       }
        else if ($scope.numberNext == 2) {
            if ($scope.AnhSanPhamViews.Title1 !== null && $scope.AnhSanPhamViews.Title1 !== "") {
                $scope.model.Title1 = "/Home/GetPic2_Home/" + $scope.model.RowGuid;
            } else {
                $scope.model.Title1 = "/images/no-image-icon.jpg";
            }
        }  
        if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
            $scope.$apply();
        }       
    }
    $scope.btnBack = function () {
        if ($scope.numberNext == 3) {
            if ($scope.AnhSanPhamViews.Title1 !== null && $scope.AnhSanPhamViews.Title1 !== "") {
                $scope.model.Title1 = "/Home/GetPic3_Home/" + $scope.model.RowGuid;
            } else {
                $scope.model.Title1 = "/images/no-image-icon.jpg";
            }
            $scope.numberNext = 0;
        } else {
            $scope.numberNext += 1;
        }
        if ($scope.numberNext == 1) {
            if ($scope.AnhSanPhamViews.Title1 !== null && $scope.AnhSanPhamViews.Title1 !== "") {
                $scope.model.Title1 = "/Home/GetPic1_Home/" + $scope.model.RowGuid;
            } else {
                $scope.model.Title1 = "/images/no-image-icon.jpg";
            }
        }
        else if ($scope.numberNext == 2) {
            if ($scope.AnhSanPhamViews.Title1 !== null && $scope.AnhSanPhamViews.Title1 !== "") {
                $scope.model.Title1 = "/Home/GetPic2_Home/" + $scope.model.RowGuid;
            } else {
                $scope.model.Title1 = "/images/no-image-icon.jpg";
            }
        }
        if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
            $scope.$apply();
        }
    }
    $scope.btnView = function (data) {

        if (data == 1) {
            if ($scope.AnhSanPhamViews.Title1 !== null && $scope.AnhSanPhamViews.Title1 !== "") {
                $scope.model.Title1 = "/Home/GetPic1_Home/" + $scope.model.RowGuid;
            } else {
                $scope.model.Title1 = "/images/no-image-icon.jpg";
            }
        }
        else if (data == 2) {
            if ($scope.AnhSanPhamViews.Title1 !== null && $scope.AnhSanPhamViews.Title1 !== "") {
                $scope.model.Title1 = "/Home/GetPic2_Home/" + $scope.model.RowGuid;
            } else {
                $scope.model.Title1 = "/images/no-image-icon.jpg";
            }
        }
        else if (data == 3) {
            if ($scope.AnhSanPhamViews.Title1 !== null && $scope.AnhSanPhamViews.Title1 !== "") {
                $scope.model.Title1 = "/Home/GetPic3_Home/" + $scope.model.RowGuid;
            } else {
                $scope.model.Title1 = "/images/no-image-icon.jpg";
            }
        }
        if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
            $scope.$apply();
        }

    }
    dataservice.getItem({ IdS: [para] }, function (rs) {
        if (rs.Error) {
            App.notifyDanger(rs.Title);
        } else {
            $scope.model = rs.SanPhams;
            $scope.AnhSanPhamViews = rs.AnhSanPhamViews;
            if ($scope.AnhSanPhamViews.Title1 !== null && $scope.AnhSanPhamViews.Title1 !== "") {
                $scope.model.Title1 = "/Home/GetPic1_Home/" + $scope.model.RowGuid;
                $scope.model.Title1_goc = "/Home/GetPic1_Home/" + $scope.model.RowGuid;
            } else {
                $scope.model.Title1 = "/images/no-image-icon.jpg";
                $scope.model.Title1_goc = "/images/no-image-icon.jpg";
            }
            if ($scope.AnhSanPhamViews.Title2 !== null && $scope.AnhSanPhamViews.Title2 !== "") {
                $scope.model.Title2 = "/Home/GetPic2_Home/" + $scope.model.RowGuid;
                $scope.model.Title2_goc = "/Home/GetPic2_Home/" + $scope.model.RowGuid;
            } else {
                $scope.model.Title2 = "/images/no-image-icon.jpg";
                $scope.model.Title2_goc = "/images/no-image-icon.jpg";
            }
            if ($scope.AnhSanPhamViews.Title3 !== null && $scope.AnhSanPhamViews.Title3 !== "") {
                $scope.model.Title3 = "/Home/GetPic3_Home/" + $scope.model.RowGuid;
                $scope.model.Title3_goc = "/Home/GetPic3_Home/" + $scope.model.RowGuid;
            } else {
                $scope.model.Title3 = "/images/no-image-icon.jpg";
                $scope.model.Title3_goc = "/images/no-image-icon.jpg";
            }
            if ($scope.AnhSanPhamViews.Title4 !== null && $scope.AnhSanPhamViews.Title4 !== "") {
                $scope.model.Title4 = "/Home/GetPic4_Home/" + $scope.model.RowGuid;
                $scope.model.Title4_goc = "/Home/GetPic4_Home/" + $scope.model.RowGuid;
            } else {
                $scope.model.Title4 = "/images/no-image-icon.jpg";
                $scope.model.Title4_goc = "/images/no-image-icon.jpg";
            }
        }
    })
    $scope.removeImage = [];
    $scope.clearFile1 = function () {
        $('#image').val(undefined);
        $('#loadImage').val(undefined);
        $scope.model.Title1 = "/images/no-image-icon.jpg";
        if (!$scope.removeImage.includes("1")) {
            $scope.removeImage.push("1");
        }

    }
    $scope.clearFile2 = function () {
        $('#image2').val(undefined);
        $('#loadImage2').val(undefined);
        $scope.model.Title2 = "/images/no-image-icon.jpg";
        if (!$scope.removeImage.includes("2")) {
            $scope.removeImage.push("2");
        }
    }
    $scope.clearFile3 = function () {
        $('#image3').val(undefined);
        $('#loadImage3').val(undefined);
        $scope.model.Title3 = "/images/no-image-icon.jpg";
        if (!$scope.removeImage.includes("3")) {
            $scope.removeImage.push("3");
        }
    }
    $scope.clearFile4 = function () {
        $('#image4').val(undefined);
        $('#loadImage4').val(undefined);
        $scope.model.Title4 = "/images/no-image-icon.jpg";
        if (!$scope.removeImage.includes("4")) {
            $scope.removeImage.push("4");
        }
    }
   

});