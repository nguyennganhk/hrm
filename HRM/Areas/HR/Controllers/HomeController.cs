﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using HRM.Utils;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.WsFederation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Rewrite.Internal.ApacheModRewrite;
using HRM.Models;
using System.Data.SqlClient;
using System.Data;
using HRM.Controllers;
using HRM.Models.Custom.Hr;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Math;
using FTU.Utils.HelperNet;
using DocumentFormat.OpenXml.Spreadsheet;

namespace HRM.Areas.HR.Controllers
{
    [Authorize]
    public class HomeController : BaseController
    {
        private UserManager<AppUser> UserMgr { get; }
        private SignInManager<AppUser> SignInMgr { get; }
        private readonly HRMDBContext _context;

        public HomeController(HRMDBContext context, UserManager<AppUser> userManager, SignInManager<AppUser> signInManager)
        {
            UserMgr = userManager;
            SignInMgr = signInManager;
            _context = context;
        }
        [Area("Hr")]
        [Route("Hr/[controller]/Index")]
        public IActionResult Index()
        {
            return View();
        }
        [Area("Hr")]
        public async Task<IActionResult> Logout()
        {
            await SignInMgr.SignOutAsync();
            return RedirectToAction("Login", "Account");
        }
        [Area("Hr")]
        [HttpPost]
        public object getDashboard([FromBody] JTableSearch jTable)
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var lstGuid = new List<Guid?>();
                if (jTable.StartDate != null && jTable.EndDate != null)
                {
                    jTable.StartDate = (DateTime)jTable.StartDate?.Date.AddDays(0) + new TimeSpan(23, 59, 59);
                    jTable.EndDate = (DateTime)jTable.EndDate?.Date.AddDays(0) + new TimeSpan(23, 59, 59);
                }
                else if (jTable.StartDate != null)
                {
                    jTable.StartDate = (DateTime)jTable.StartDate?.Date.AddDays(0) + new TimeSpan(23, 59, 59);
                }
                else if (jTable.EndDate != null)
                {
                    jTable.EndDate = (DateTime)jTable.EndDate?.Date.AddDays(0) + new TimeSpan(23, 59, 59);
                }

                string sql = "[Hr].[SP_HRM_ProjectManage_Dashboard] @DepartmentId,@StartDate,@EndDate,@Type";
                var _DepartmentId = new SqlParameter("@DepartmentId", SqlDbType.VarChar); CheckNullParameterStore(_DepartmentId, jTable.DepartmentId);
                var _StartDate = new SqlParameter("@StartDate", SqlDbType.DateTime); CheckNullParameterStore(_StartDate, jTable.StartDate);
                var _EndDate = new SqlParameter("@EndDate", SqlDbType.DateTime); CheckNullParameterStore(_EndDate, jTable.EndDate);
                var _Type = new SqlParameter("@Type", SqlDbType.Int); CheckNullParameterStore(_Type, jTable.Type);
                if (jTable.Type == "1")
                {
                    var rs = _context.DashboardView1.FromSql(sql, _DepartmentId, _StartDate, _EndDate, _Type).ToList();
                    return Json(rs);
                }
                else
                {//Thống kê SLĐH(Theo kế hoạch)
                    var rs = _context.DashboardView1.FromSql(sql, _DepartmentId, _StartDate, _EndDate, _Type).ToList();
                    return Json(rs);
                }

            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = "Có lỗi khi lấy dữ liệu." });
            }
        }

        public class DataJtable : JTableModel
        {
            public string IsPublic { get; set; }
            public string Status { get; set; }
            public string Ispriority { get; set; }
            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }
            public Guid? DepartmentGuid { get; set; }
        }
        [HttpPost]
        public object JTable_Announcements([FromBody] DataJtable jTablePara)
        {
            try
            {
                {
                    var s = GetEmployeeLogin(_context);

                    if (jTablePara.StartDate != null)
                    {
                        jTablePara.StartDate = jTablePara.StartDate.Value.ToLocalTime();
                    }
                    else
                    {
                        jTablePara.StartDate = null;
                    }
                    if (jTablePara.EndDate != null)
                    {
                        jTablePara.EndDate = jTablePara.EndDate.Value.ToLocalTime();
                    }
                    else
                    {
                        jTablePara.EndDate = null;
                    }
                    int intBeginFor = (jTablePara.CurrentPage - 1) * jTablePara.Length;
                    string sql = "[dbo].[SP_ESVCM_Home_Announcements]" +
                    "@Keyword, " +
                    "@IsPublic, " +
                    "@Status, " +
                    "@OrganizationGuid, " +
                    "@EmployeeGuid, " +
                    "@StartDate, " +
                    "@EndDate, " +
                    "@DepartmentGuid, " +
                    "@Ispriority, " +
                    "@LoginName, " +
                    "@Sort, " +
                    "@Skip, " +
                    "@Take," +
                    "@Count out ";
                    var _Keyword = new SqlParameter("@Keyword", SqlDbType.NVarChar); CheckNullParameterStore(_Keyword, jTablePara.search.value);
                    var _IsPublic = new SqlParameter("@IsPublic", SqlDbType.VarChar); CheckNullParameterStore(_IsPublic, jTablePara.IsPublic);
                    var _Status = new SqlParameter("@Status", SqlDbType.VarChar); CheckNullParameterStore(_Status, jTablePara.Status);
                    var _OrganizationGuid = new SqlParameter("@OrganizationGuid", SqlDbType.UniqueIdentifier); CheckNullParameterStore(_OrganizationGuid, s.OrganizationGuid);
                    var _EmployeeGuid = new SqlParameter("@EmployeeGuid", SqlDbType.UniqueIdentifier); CheckNullParameterStore(_EmployeeGuid, s.EmployeeGuid);
                    var StartDate = new SqlParameter("@StartDate", SqlDbType.Date); CheckNullParameterStore(StartDate, jTablePara.StartDate);
                    var EndDate = new SqlParameter("@EndDate", SqlDbType.Date); CheckNullParameterStore(EndDate, jTablePara.EndDate);
                    var DepartmentGuid = new SqlParameter("@DepartmentGuid", SqlDbType.UniqueIdentifier); CheckNullParameterStore(DepartmentGuid, jTablePara.DepartmentGuid);
                    var Ispriority = new SqlParameter("@Ispriority", SqlDbType.VarChar); CheckNullParameterStore(Ispriority, jTablePara.Ispriority);
                    var _LoginName = new SqlParameter("@LoginName", SqlDbType.VarChar); CheckNullParameterStore(_LoginName, s.LoginName);
                    var _Sort = new SqlParameter("@Sort", SqlDbType.VarChar); CheckNullParameterStore(_Sort, jTablePara.QueryOrderBy);
                    var _Skip = new SqlParameter("@Skip", SqlDbType.Int); CheckNullParameterStore(_Skip, intBeginFor);
                    var _Take = new SqlParameter("@Take", SqlDbType.Int); CheckNullParameterStore(_Take, jTablePara.Length);
                    var _output = new SqlParameter("@Count", SqlDbType.Int) { Direction = System.Data.ParameterDirection.Output };
                    var data = _context.AnnouncementsJtableView.FromSql(sql,
                        _Keyword,
                        _IsPublic,
                        _Status,
                        _OrganizationGuid,
                        _EmployeeGuid,
                        StartDate,
                        EndDate,
                        DepartmentGuid,
                        Ispriority,
                        _LoginName,
                        _Sort,
                        _Skip,
                        _Take,
                        _output).ToList();
                    var jdata = JTableHelper.JObjectTable(data, jTablePara.Draw, (int)_output.Value);
                    return Json(jdata);
                }

            }
            catch (Exception ex)
            {
                return Json(ex);
            }
        }
        [HttpPost]
        public object getItems()
        {
            try
            {
                var s = GetEmployeeLogin(_context);
                    string sql = "[dbo].[SP_ESVCM_Home_Items]" +
                    "@Keyword";
                var _Keyword = SqlPara("@Keyword","", SqlDbType.NVarChar);             
                var data = _context.Object_Combobox.FromSql(sql,_Keyword).ToList();
                return Json(data);
            }
            catch (Exception ex)
            {
                return Json(ex);
            }
        }

    }
}
