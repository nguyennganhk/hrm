﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using FTU.Utils.HelperNet;
using HRM.Controllers;
using HRM.Models;
using HRM.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using OfficeOpenXml;

namespace HRM.Areas.HR.Controllers
{
    public class DepartmentsController : BaseController
    {
        private readonly HRMDBContext _context;
        private readonly AppSettings _appSettings;
        private readonly IHostingEnvironment _environment;
        public DepartmentsController(IOptions<AppSettings> appSettings, HRMDBContext context, IHostingEnvironment environment)
        {
            _context = context;
            _appSettings = appSettings.Value;
            _environment = environment;
        }
        [Area("HR")]
        public IActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public object getLogin()
        {
            try
            {
                var _emp = GetEmployeeLogin(_context);
                return Json(_emp);
            }
            catch (Exception ex)
            {
                return new { Title = "Xóa không thành công.", Error = true, Object = ex };

            }
        }
        //lấy danh sách tổ chức
        [HttpGet]
        public object GetOrganizations()
        {
            try
            {

                var _rs = _context.Organizations.Select(x => new { x.OrganizationGuid, x.OrganizationName, x.ParentId, x.SortOrder }).OrderBy(x => x.SortOrder).ToList();
                return Json(_rs);
            }
            catch (Exception ex)
            {
                return new { Title = "Có lỗi khi lấy đơn vị.", Error = true, Object = ex };
            }
        }

        //lấy danh sách phòng ban
        [HttpGet]
        public object GetDepartments(Guid? guid)
        {
            var a = _context.Departments.Where(x => x.OrganizationGuid == guid).OrderBy(x => x.OrderId).ToList();
            var data = GetSubTreeData(a, null, new List<TreeView>(), "")
            .Select(x => new
            {
                value = x.DepartmentGuid,
                text = x.DepartmentName
            }).ToList();
            return Json(data);
        }

        private List<TreeView> GetSubTreeData(List<Departments> data, Guid? ParentId, List<TreeView> lstCategories, string tab)
        {
            tab += "- ";
            var contents = ParentId == null
                ? data.Where(x => x.ParentId == null).ToList()
                : data.Where(x => x.ParentId == ParentId).ToList();
            foreach (var item in contents)
            {
                var category = new TreeView
                {
                    DepartmentGuid = item.DepartmentGuid,
                    DepartmentName = tab + item.DepartmentName,
                    HasChild = data.Any(x => x.ParentId == item.DepartmentGuid),
                };
                lstCategories.Add(category);
                if (category.HasChild) GetSubTreeData(data, item.DepartmentGuid, lstCategories, tab);
            }
            return lstCategories;
        }

        //View
        public class DataJtable : JTableModel
        {
            public bool? IsActive { get; set; }
            public List<string> OrganizationGuid { get; set; }
            public Guid? DepartmentGuid { get; set; }
        }
        [HttpPost]
        public object JTable([FromBody] DataJtable jTablePara)
        {

            try
            {
                var _emp = GetEmployeeLogin(_context);
                int intBeginFor = (jTablePara.CurrentPage - 1) * jTablePara.Length;
                string sql = "[HR].[SP_ESVCM_Departments_ListAll]" +
                "@DepartmentGuid, " +
                "@OrganizationGuid, " +
                "@Sort, " +
                "@output out ";
                var stringOrg = "";
                if (jTablePara.OrganizationGuid.Count() > 0)
                {
                    foreach (var item in jTablePara.OrganizationGuid)
                    {
                        stringOrg = item + "," + stringOrg;
                    }
                }
                jTablePara.OrganizationGuid.Add(_emp.OrganizationGuid.ToString());
                var DepartmentGuid = new SqlParameter("@DepartmentGuid", SqlDbType.UniqueIdentifier); CheckNullParameterStore(DepartmentGuid, jTablePara.DepartmentGuid);
                var OrganizationGuid = new SqlParameter("@OrganizationGuid", SqlDbType.VarChar, 1000); OrganizationGuid.Value = jTablePara.OrganizationGuid.Count() > 0 ? String.Join(",", jTablePara.OrganizationGuid) : "";
                var Sort = new SqlParameter("@Sort", SqlDbType.VarChar); CheckNullParameterStore(Sort, "OrderID");
                var output = new SqlParameter("@output", SqlDbType.Int) { Direction = System.Data.ParameterDirection.Output };
                var data = _context.DepartmentsJtableViews.FromSql(sql, DepartmentGuid, OrganizationGuid, Sort, output).ToList();
                if (data.Count() == 1)
                {
                    return new JMessage() { Error = false, Title = "Lấy dữ liệu thành công.", Object = data.Select(x => new { x.DepartmentGuid, x.DepartmentId, x.DepartmentName, x.Description, x.OrderId, ParentId = "", x.IsActive }).ToList() };
                }
                return new JMessage() { Error = false, Title = "Lấy dữ liệu thành công.", Object = data };
            }
            catch (Exception ex)
            {
                return Json(ex);
            }
        }
        // lấy quanrn lý
        [HttpGet]
        public object GetManager()
        {
            var userlogin = GetEmployeeLogin(_context);
            var data = _context.Employees.Where(x => x.OrganizationGuid == userlogin.OrganizationGuid).Select(x => new { x.EmployeeGuid, x.EmployeeId, x.FullName }).ToList();
            return data;
        }
        //Add New
        [HttpPost]
        public object Submit()
        {
            var msg = new JMessage() { Error = false };
            var _emp = GetEmployeeLogin(_context);
            var _obj = Request.Form["submit"];
            Departments obj = JsonConvert.DeserializeObject<Departments>(_obj);
            var a = _context.Organizations.Where(x => x.OrganizationGuid == obj.OrganizationGuid).FirstOrDefault();
            try
            {
                var countID = _context.Departments.Where(x => x.DepartmentId == obj.DepartmentId).Count();
                if (countID > 0)
                {
                    return new { Title = "Mã phòng ban đã tồn tại", Error = true };
                }
                string sql = "[HR].[SP_ESVCM_Departments_Insert] @DepartmentGuid ," +
                "@DepartmentID," +
                "@DepartmentName," +
                "@ManagerGuid," +
                "@ManagerID," +
                "@ManagerName," +
                "@ParentID," +
                "@OrganizationGuid," +
                "@OrganizationName," +
                "@Description," +
                "@Content," +
                "@Address," +
                "@Email," +
                "@Phone," +
                "@Fax," +
                "@OrderID," +
                "@IsActive," +
                "@CreatedDate," +
                "@ModifiedDate," +
                "@CreatedBy," +
                "@ModifiedBy";
                var guid = Guid.NewGuid();
                var _DepartmentGuid = new SqlParameter("@DepartmentGuid ", SqlDbType.UniqueIdentifier);
                CheckNullParameterStore(_DepartmentGuid, guid);
                var _DepartmentId = new SqlParameter("@DepartmentID ", SqlDbType.VarChar, 20);
                CheckNullParameterStore(_DepartmentId, obj.DepartmentId);
                var _DepartmentName = new SqlParameter("@DepartmentName", System.Data.SqlDbType.NVarChar, 50);
                CheckNullParameterStore(_DepartmentName, obj.DepartmentName);
                var _ManagerGuid = new SqlParameter("@ManagerGuid", System.Data.SqlDbType.UniqueIdentifier);
                CheckNullParameterStore(_ManagerGuid, obj.ManagerGuid);
                var _ManagerID = new SqlParameter("@ManagerID", System.Data.SqlDbType.VarChar, 50);
                var ManagerID = _context.Employees.Where(x => x.EmployeeGuid == obj.ManagerGuid).Select(x => x.EmployeeId).FirstOrDefault();
                CheckNullParameterStore(_ManagerID, ManagerID);
                var _ManagerName = new SqlParameter("@ManagerName", System.Data.SqlDbType.NVarChar, 250);
                var ManagerName = _context.Employees.Where(x => x.EmployeeGuid == obj.ManagerGuid).Select(x => x.FullName).FirstOrDefault();
                CheckNullParameterStore(_ManagerName, ManagerName);
                var _ParentId = new SqlParameter("@ParentID", System.Data.SqlDbType.UniqueIdentifier);
                CheckNullParameterStore(_ParentId, obj.ParentId);
                var _OrganizationGuid = new SqlParameter("@OrganizationGuid", System.Data.SqlDbType.UniqueIdentifier);
                CheckNullParameterStore(_OrganizationGuid, _emp.OrganizationGuid);
                var _OrganizationName = new SqlParameter("@OrganizationName", System.Data.SqlDbType.NVarChar, 250);
                CheckNullParameterStore(_OrganizationName, _emp.OrganizationName);
                var _Description = new SqlParameter("@Description", System.Data.SqlDbType.NVarChar, 255);
                CheckNullParameterStore(_Description, obj.Description);
                var _Content = new SqlParameter("@Content", System.Data.SqlDbType.NText);
                CheckNullParameterStore(_Content, obj.Content);
                var _Address = new SqlParameter("@Address", System.Data.SqlDbType.NVarChar, 255);
                CheckNullParameterStore(_Address, obj.Address);
                var _Email = new SqlParameter("@Email", System.Data.SqlDbType.NVarChar, 255);
                CheckNullParameterStore(_Email, obj.Email);
                var _Phone = new SqlParameter("@Phone", System.Data.SqlDbType.NVarChar, 50);
                CheckNullParameterStore(_Phone, obj.Phone);
                var _Fax = new SqlParameter("@Fax", System.Data.SqlDbType.NVarChar, 50);
                CheckNullParameterStore(_Fax, obj.Fax);
                var _OrderId = new SqlParameter("@OrderID", System.Data.SqlDbType.Int);
                CheckNullParameterStore(_OrderId, obj.OrderId);
                var _IsActive = new SqlParameter("@IsActive", System.Data.SqlDbType.Bit);
                CheckNullParameterStore(_IsActive, obj.IsActive);
                var _CreatedDate = new SqlParameter("@CreatedDate", System.Data.SqlDbType.DateTime);
                _CreatedDate.Value = DateTime.Now;
                var _ModifiedDate = new SqlParameter("@ModifiedDate", System.Data.SqlDbType.DateTime);
                _ModifiedDate.Value = DateTime.Now;
                var _CreatedBy = new SqlParameter("@CreatedBy", System.Data.SqlDbType.NVarChar);
                _CreatedBy.Value = GetCreatedBy(_context);
                var _ModifiedBy = new SqlParameter("@ModifiedBy", System.Data.SqlDbType.NVarChar);
                _ModifiedBy.Value = GetModifiedBy(_context);
                var _rs = _context.SQLCOMMANDS.FromSql(sql, _DepartmentGuid, _DepartmentId, _DepartmentName, _ManagerGuid, _ManagerID, _ManagerName, _ParentId, _OrganizationGuid, _OrganizationName, _Description, _Content, _Address, _Email, _Phone, _Fax, _OrderId, _IsActive, _CreatedDate, _ModifiedDate, _CreatedBy, _ModifiedBy).FirstOrDefault();
                if (_rs.Error == "")
                {
                    return new { Title = "Thêm mới thành công.", Object = _emp.OrganizationGuid, Error = false };
                }
                else
                {
                    return new { Title = "Thêm mới không thành công.", Error = true, Object = _rs.Error };
                }
            }
            catch (Exception ex)

            {
                return new { Title = "Thêm mới không thành công.", Error = true };
            }
        }

        //Update
        public class DepartmentsCustom : Departments
        {
            public string id { get; set; }
        }
        [HttpPost]
        public object Edit()
        {
            var _obj = Request.Form["edit"];
            var _emp = GetEmployeeLogin(_context);
            DepartmentsCustom obj = JsonConvert.DeserializeObject<DepartmentsCustom>(_obj);
            var a = _context.Organizations.Where(x => x.OrganizationGuid == obj.OrganizationGuid).FirstOrDefault();
            try
            {
                if (obj.id != obj.DepartmentId)
                {
                    var countID = _context.Departments.Where(x => x.DepartmentId == obj.DepartmentId).Count();
                    if (countID > 0)
                    {
                        return new { Title = "Mã phòng ban đã tồn tại", Error = true };
                    }
                }

                string sql = "[HR].[SP_ESVCM_Departments_Update] @DepartmentGuid ," +
                "@DepartmentID," +
                "@DepartmentName," +
                "@ManagerGuid," +
                "@ManagerID," +
                "@ManagerName," +
                "@ParentID," +
                "@Description," +
                "@Content," +
                "@Address," +
                "@Email," +
                "@Phone," +
                "@Fax," +
                "@OrderID," +
                "@IsActive," +
                "@ModifiedDate," +
                "@ModifiedBy";
                var _DepartmentGuid = new SqlParameter("@DepartmentGuid ", SqlDbType.UniqueIdentifier);
                CheckNullParameterStore(_DepartmentGuid, obj.DepartmentGuid);
                var _DepartmentId = new SqlParameter("@DepartmentID ", SqlDbType.VarChar, 20);
                CheckNullParameterStore(_DepartmentId, obj.DepartmentId);
                var _DepartmentName = new SqlParameter("@DepartmentName", System.Data.SqlDbType.NVarChar, 50);
                CheckNullParameterStore(_DepartmentName, obj.DepartmentName);
                var _ManagerGuid = new SqlParameter("@ManagerGuid", System.Data.SqlDbType.UniqueIdentifier);
                CheckNullParameterStore(_ManagerGuid, obj.ManagerGuid);
                var _ManagerID = new SqlParameter("@ManagerID", System.Data.SqlDbType.VarChar, 50);
                var ManagerID = _context.Employees.Where(x => x.EmployeeGuid == obj.ManagerGuid).Select(x => x.EmployeeId).FirstOrDefault();
                CheckNullParameterStore(_ManagerID, ManagerID);
                var _ManagerName = new SqlParameter("@ManagerName", System.Data.SqlDbType.NVarChar, 250);
                var ManagerName = _context.Employees.Where(x => x.EmployeeGuid == obj.ManagerGuid).Select(x => x.FullName).FirstOrDefault();
                CheckNullParameterStore(_ManagerName, ManagerName);
                var _ParentId = new SqlParameter("@ParentID", System.Data.SqlDbType.UniqueIdentifier);
                CheckNullParameterStore(_ParentId, obj.ParentId);
                var _Description = new SqlParameter("@Description", System.Data.SqlDbType.NVarChar, 255);
                CheckNullParameterStore(_Description, obj.Description);
                var _Content = new SqlParameter("@Content", System.Data.SqlDbType.NText);
                CheckNullParameterStore(_Content, obj.Content);
                var _Address = new SqlParameter("@Address", System.Data.SqlDbType.NVarChar, 255);
                CheckNullParameterStore(_Address, obj.Address);
                var _Email = new SqlParameter("@Email", System.Data.SqlDbType.NVarChar, 255);
                CheckNullParameterStore(_Email, obj.Email);
                var _Phone = new SqlParameter("@Phone", System.Data.SqlDbType.NVarChar, 50);
                CheckNullParameterStore(_Phone, obj.Phone);
                var _Fax = new SqlParameter("@Fax", System.Data.SqlDbType.NVarChar, 50);
                CheckNullParameterStore(_Fax, obj.Fax);
                var _OrderId = new SqlParameter("@OrderID", System.Data.SqlDbType.Int);
                CheckNullParameterStore(_OrderId, obj.OrderId);
                var _IsActive = new SqlParameter("@IsActive", System.Data.SqlDbType.Bit);
                CheckNullParameterStore(_IsActive, obj.IsActive);
                var _ModifiedDate = new SqlParameter("@ModifiedDate", System.Data.SqlDbType.DateTime);
                _ModifiedDate.Value = DateTime.Now;
                var _ModifiedBy = new SqlParameter("@ModifiedBy", System.Data.SqlDbType.NVarChar);
                _ModifiedBy.Value = GetModifiedBy(_context);
                var _rs = _context.SQLCOMMANDS.FromSql(sql, _DepartmentGuid, _DepartmentId, _DepartmentName, _ManagerGuid, _ManagerID, _ManagerName, _ParentId, _Description, _Content, _Address, _Email, _Phone, _Fax, _OrderId, _IsActive, _ModifiedDate, _ModifiedBy).FirstOrDefault();
                if (_rs.Error == "")
                {
                    return new { Title = "Cập nhật thành công.", Object = _emp.OrganizationGuid, Error = false };
                }
                else
                {
                    return new { Title = "Cập nhật không thành công", Error = true, Object = _rs.Error };
                }
            }
            catch (Exception ex)

            {
                return new { Title = "Thay đổi không thành công!", Error = true };
            }
        }

        //[HttpGet]
        //public object checkPermission( Guid? id)
        //{
        //    var msg = new JMessage() { Error = false };
        //    try
        //    {

        //        var _emp = GetEmployeeLogin(_context);
        //        string sql = "[HR].[SP_ESVCM_Departments_checkPermission] @DepartmentGuid,@OrganizationGuid,@Count out";
        //        var _DepartmentGuid = new SqlParameter("@DepartmentGuid ", SqlDbType.UniqueIdentifier);
        //        CheckNullParameterStore(_DepartmentGuid, id);
        //        var _OrganizationGuid = new SqlParameter("@OrganizationGuid ", SqlDbType.UniqueIdentifier);
        //        CheckNullParameterStore(_OrganizationGuid, _emp.OrganizationGuid);
        //        var _Count = new SqlParameter("@Count ", SqlDbType.Int) {Direction= ParameterDirection.Output };
        //        CheckNullParameterStore(_Count,0);
        //        var rs = _context.Database.ExecuteSqlCommand(sql, _DepartmentGuid,_OrganizationGuid,_Count);
        //        if (rs == 0)
        //        {
        //            msg.Title = "Có lỗi khi lấy dữ liệu.";
        //            return Json(new JMessage() { Error = true, Title = msg.Title });
        //        }
        //        else
        //        {
        //            msg.Title = "Có lỗi khi lấy dữ liệu.";
        //            return Json(new JMessage() { Error = false, Object = _Count.Value, Title = msg.Title });
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        msg.Title = "Có lỗi khi lấy dữ liệu.";
        //        return Json(new JMessage() { Error = true, Object = ex, Title = msg.Title });
        //    }
        //}

        //GetById
        [HttpPost]
        public JsonResult GetById(Guid? id)
        {

            var msg = new JMessage() { Error = false };
            try
            {

                var _emp = GetEmployeeLogin(_context);
                string sql = "[HR].[SP_ESVCM_Departments_ViewItem] @DepartmentGuid";
                var _DepartmentGuid = new SqlParameter("@DepartmentGuid ", SqlDbType.UniqueIdentifier);
                CheckNullParameterStore(_DepartmentGuid, id);
                var rs = _context.DepartmentsViewItems.FromSql(sql, _DepartmentGuid).SingleOrDefault();
                return Json(rs);
            }
            catch (Exception ex)
            {
                msg.Title = "Có lỗi khi lấy dữ liệu.";
                return Json(new JMessage() { Error = true, Object = ex, Title = msg.Title });
            }
        }
        // lấy dữ liệu của bảng cha
        [HttpPost]
        public JsonResult GetParent(Guid? id)
        {

            var msg = new JMessage() { Error = false };
            try
            {
                var _emp = GetEmployeeLogin(_context);
                string sql = "[HR].[SP_ESVCM_Departments_ViewItem] @DepartmentGuid";
                var _DepartmentGuid = new SqlParameter("@DepartmentGuid ", SqlDbType.UniqueIdentifier);
                CheckNullParameterStore(_DepartmentGuid, id);
                var rs = _context.DepartmentsViewItems.FromSql(sql, _DepartmentGuid).Select(x => new { x.DepartmentGuid, x.OrganizationGuid }).SingleOrDefault();
                return Json(rs);
            }
            catch (Exception ex)
            {
                msg.Title = "Có lỗi khi lấy dữ liệu.";
                return Json(new JMessage() { Error = true, Object = ex, Title = msg.Title });
            }
        }

        //Delete
        [HttpPost]
        public object Remove(Guid? id)
        {
            try
            {
                var _emp = GetEmployeeLogin(_context);
                string sql = "[HR].[SP_ESVCM_Departments_Delete] @DepartmentGuid,@Error ";
                var _DepartmentGuid = new SqlParameter("@DepartmentGuid ", SqlDbType.UniqueIdentifier);
                CheckNullParameterStore(_DepartmentGuid, id);
                var _Error = new SqlParameter("@Error", SqlDbType.NVarChar);
                CheckNullParameterStore(_Error, null);
                var _rs = _context.SQLCOMMANDS.FromSql(sql, _DepartmentGuid, _Error).SingleOrDefault();
                if (_rs.Error == "")
                {
                    return new { Title = "Xóa thành công.", Error = false };
                }
                else
                {
                    return new { Title = "Bạn không thể xóa khi có đối tượng đang sử dụng tổ chức này.", Error = true, Object = _rs.Error };
                }
            }
            catch (Exception ex)

            {
                return new { Title = "Xóa không thành công.", Error = true };
            }
        }

        /// Lấy danh sách để sắp xếp
        [HttpPost]
        public object GetByParent(Guid? id)
        {
            return Json(_context.Departments.Where(x => x.OrganizationGuid == id && x.ParentId == null).OrderBy(x => x.OrderId).ToList());
        }
        /// Lấy danh sách để sắp xếp
        [HttpPost]
        public object GetParents(Guid? id)
        {
            return Json(_context.Departments.Where(x => (x.ParentId == id)).OrderBy(x => x.OrderId).ToList());
        }

        /// Sắp xếp thứ tự
        [HttpPost]
        public object Resort([FromBody] List<Departments> model)
        {

            var msg = new JMessage() { Error = false };
            try
            {
                foreach (var item in model)
                {
                    _context.Departments.Attach(item);
                    _context.Entry(item).Property(x => x.OrderId).IsModified = true;
                    _context.SaveChanges();
                }
                msg.Title = "Sắp xếp các Phòng ban thành công.";
            }
            catch (Exception)
            {
                msg.Error = true;
                msg.Title = "Có lỗi khi sắp xếp phòng ban";
            }
            return Json(msg);
        }
      
    }
}
