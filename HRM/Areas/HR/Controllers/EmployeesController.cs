﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using FTU.Utils.HelperNet;
using HRM.Controllers;
using HRM.Models;
using HRM.Models.Custom.Hr;
using HRM.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace HRM.Areas.HR.Controllers
{
    [Authorize]
    public class EmployeesController : BaseController
    {
        private readonly HRMDBContext _context;
        private readonly AppSettings _appSettings;
        private readonly IHostingEnvironment _environment;
        public EmployeesController(IOptions<AppSettings> appSettings, HRMDBContext context, IHostingEnvironment environment)
        {
            _context = context;
            _appSettings = appSettings.Value;
            _environment = environment;
         //  Stimulsoft.Base.StiLicense.LoadFromFile("License.key");
        }
        [Area("Hr")]
        public IActionResult Index()
        {
            return View();
        }

        //=========================start Employee (Bảng nhân viên )=====================
        #region

        [HttpGet]
        public object GetPicThumb(Guid? id)
        {
            try
            {
                var _emp = GetEmployeeLogin(_context);
                string sql = "[Hr].[SP_ESHR_GetPic_By_EmployeeGuid] @OrganizationGuid, @EmployeeGuid";
                var _OrganizationGuid = new SqlParameter("@OrganizationGuid", SqlDbType.UniqueIdentifier); _OrganizationGuid.Value = _emp.OrganizationGuid;
                var _EmployeeGuid = new SqlParameter("@EmployeeGuid", SqlDbType.UniqueIdentifier); _EmployeeGuid.Value = id;
                var x = _context.PhotoView.FromSql(sql, _OrganizationGuid, _EmployeeGuid).SingleOrDefault();
                byte[] ImageTemp = x.Thumb;

                FileContentResult hd = null;
                hd = File(ImageTemp, "image/jpeg");
                int gh = ImageTemp.Count();
                if (gh > 0)
                {
                    return hd;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }
        [HttpGet]
        public object GetPic(Guid? id)
        {
            try
            {
                var Photo = _context.Employees.Where(x => x.EmployeeGuid == id).Select(x => x.Photo).SingleOrDefault();
                byte[] ImageTemp = Photo;

                FileContentResult hd = null;
                hd = File(ImageTemp, "image/jpeg");
                int gh = ImageTemp.Count();
                if (gh > 0)
                {
                    return hd;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }
        [HttpGet]
        public object GetAll()
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var _emp = GetEmployeeLogin(_context);
                string sql = "[Hr].[SP_ESHR_GetAllEmployee] @OrganizationGuid";
                var _OrganizationGuid = new SqlParameter("@OrganizationGuid", SqlDbType.UniqueIdentifier); CheckNullParameterStore(_OrganizationGuid, _emp.OrganizationGuid);
                var rs = _context.EmployeesJtableView.FromSql(sql, _OrganizationGuid).ToList();
                return Json(rs);
            }
            catch (Exception ex)
            {
                msg.Object = ex;
                msg.Error = true;
                msg.Title = "Có lỗi khi lấy dữ liệu.";
                return Json(msg);
            }
        }
        [HttpGet]
        public object GetManager()
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var _emp = GetEmployeeLogin(_context);
                var rs = _context.Employees.Where(x => x.StatusOfWork == "OM" && x.OrganizationGuid == _emp.OrganizationGuid).Select(x => new Object_Combobox { value = x.EmployeeGuid.ToString(), text = x.FullName }).ToList();
                return Json(rs);
            }
            catch (Exception ex)
            {
                msg.Object = ex;
                msg.Error = true;
                msg.Title = "Có lỗi khi lấy dữ liệu.";
                return Json(msg);
            }
        }

        [HttpGet]
        public object GetDepartment()
        {
            try
            {
                var _emp = GetEmployeeLogin(_context);
                string sql = "[Hr].[SP_ESHR_GetAllDepartments] @OrganizationGuid";
                var _OrganizationGuid = new SqlParameter("@OrganizationGuid", SqlDbType.UniqueIdentifier); CheckNullParameterStore(_OrganizationGuid, _emp.OrganizationGuid);
                var rs = _context.DepartmentsTreeview.FromSql(sql, _OrganizationGuid).ToList();
                return Json(rs);
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = "Có lỗi khi lấy dữ liệu." });
            }
        }

        [HttpGet]
        public object GetEducation()
        {
            try
            {
                var _emp = GetEmployeeLogin(_context);
                string sql = "[Hr].[SP_ESHR_GetAllEducations] @OrganizationGuid";
                var _OrganizationGuid = new SqlParameter("@OrganizationGuid", SqlDbType.UniqueIdentifier); CheckNullParameterStore(_OrganizationGuid, _emp.OrganizationGuid);
                var rs = _context.Object_Combobox.FromSql(sql, _OrganizationGuid).ToList();
                return Json(rs);
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = "Có lỗi khi lấy dữ liệu." });
            }
        }
        [HttpGet]
        public object GetShift()
        {
            try
            {
                var _emp = GetEmployeeLogin(_context);
                string sql = "[Hr].[SP_ESHR_GetAllShifts] @OrganizationGuid";
                var _OrganizationGuid = new SqlParameter("@OrganizationGuid", SqlDbType.UniqueIdentifier); CheckNullParameterStore(_OrganizationGuid, _emp.OrganizationGuid);
                var rs = _context.Object_Combobox.FromSql(sql, _OrganizationGuid).ToList();
                return Json(rs);
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = "Có lỗi khi lấy dữ liệu." });
            }
        }
        [HttpGet]
        public object GetJobTitle()
        {
            try
            {
                var _emp = GetEmployeeLogin(_context);
                string sql = "[Hr].[SP_ESHR_GetAllJobTitles] @OrganizationGuid";
                var _OrganizationGuid = new SqlParameter("@OrganizationGuid", SqlDbType.UniqueIdentifier); CheckNullParameterStore(_OrganizationGuid, _emp.OrganizationGuid);
                var rs = _context.Object_Combobox.FromSql(sql, _OrganizationGuid).ToList();
                return Json(rs);
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = "Có lỗi khi lấy dữ liệu." });
            }
        }
        [Area("Hr")]
        public object GetJobTitle_jexcel()
        {
            try
            {
                var _emp = GetEmployeeLogin(_context);
                string sql = "[Hr].[SP_ESHR_GetAllJobTitles_jexcel] @OrganizationGuid";
                var _OrganizationGuid = new SqlParameter("@OrganizationGuid", SqlDbType.UniqueIdentifier); CheckNullParameterStore(_OrganizationGuid, _emp.OrganizationGuid);
                var rs = _context.Object_Jexcel.FromSql(sql, _OrganizationGuid).ToList();
                return Json(rs);
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = "Có lỗi khi lấy dữ liệu." });
            }
        }
        [HttpGet]
        public object GetNationality()
        {
            try
            {
                string sql = "[Hr].[SP_ESHR_GetAllCountries] @DBName_COMMON";
                var _DBName_COMMON = new SqlParameter("@DBName_COMMON", SqlDbType.NVarChar, 50); CheckNullParameterStore(_DBName_COMMON, _appSettings.DBName_HR);
                var rs = _context.Object_Combobox.FromSql(sql, _DBName_COMMON).ToList();
                return Json(rs);
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = "Có lỗi khi lấy dữ liệu." });
            }
        }
        [HttpGet]
        public object GetProvince()
        {
            try
            {
                string sql = "[Hr].[SP_ESHR_GetAllProvinces] @DBName_COMMON";
                var _DBName_COMMON = new SqlParameter("@DBName_COMMON", SqlDbType.NVarChar, 50); CheckNullParameterStore(_DBName_COMMON, _appSettings.DBName_HR);
                var rs = _context.ObjectNew_Combobox.FromSql(sql, _DBName_COMMON).ToList();
                return Json(rs);
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = "Có lỗi khi lấy dữ liệu." });
            }
        }
        [HttpGet]
        public object GetDistrict()
        {
            try
            {
                string sql = "[Hr].[SP_ESHR_GetAllWards] @DBName_COMMON";
                var _DBName_COMMON = new SqlParameter("@DBName_COMMON", SqlDbType.NVarChar, 50); CheckNullParameterStore(_DBName_COMMON, _appSettings.DBName_HR);
                var rs = _context.ObjectNew_Combobox.FromSql(sql, _DBName_COMMON).ToList();
                return Json(rs);
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = "Có lỗi khi lấy dữ liệu." });
            }
        }
        [HttpPost]
        public object getListReasonLeaves()
        {
            try
            {
                string sql = "[Hr].[SP_ESHR_GetAllReasonLeaveWorks]";
                var rs = _context.Object_Combobox.FromSql(sql).ToList();
                return Json(rs);
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = "Có lỗi khi lấy dữ liệu." });
            }
        }
        [HttpPost]
        public List<TreeView> GetTreeDataAdd(Guid? id)
        {
            var _emp = GetEmployeeLogin(_context);
            var temp = id;
            var data = _context.Departments.OrderBy(x => x.OrderId).Where(x => x.OrganizationGuid == _emp.OrganizationGuid && ((x.DepartmentGuid != id && x.ParentId != id && x.IsActive == true) || temp == null));
            var dataOrder = GetSubTreeDataAdd(data.ToList(), id, new List<TreeView>(), "");
            return dataOrder;

        }
        private List<TreeView> GetSubTreeDataAdd(List<Departments> data, Guid? parentid, List<TreeView> lstCategories, string tab)
        {
            tab += "- ";
            var contents = parentid == null
                ? data.Where(x => x.ParentId == null && x.IsActive == true).ToList()
                : data.Where(x => x.ParentId == parentid && x.IsActive == true).ToList();
            foreach (var item in contents)
            {
                var category = new TreeView
                {
                    DepartmentGuid = item.DepartmentGuid,
                    DepartmentId = item.DepartmentId,
                    DepartmentName = item.DepartmentName,
                    Title = tab + item.DepartmentName,
                    HasChild = data.Any(x => x.ParentId == item.DepartmentGuid)
                };
                lstCategories.Add(category);
                if (category.HasChild) GetSubTreeDataAdd(data, item.DepartmentGuid, lstCategories, tab);
            }
            return lstCategories;
        }
        [HttpGet]
        public object GetLanguages()
        {
            try
            {
                string sql = "[Hr].[SP_ESHR_GetAllLanguages]";
                var rs = _context.Object_Combobox.FromSql(sql).ToList();
                return Json(rs);
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = "Có lỗi khi lấy dữ liệu." });
            }
        }
        [HttpGet]
        public object GetCandidate()
        {
            try
            {
                var _emp = GetEmployeeLogin(_context);
                string sql = "[HR].[SP_ESHR_GetAllCandidates] @OrganizationGuid";
                var _OrganizationGuid = new SqlParameter("@OrganizationGuid", SqlDbType.UniqueIdentifier); CheckNullParameterStore(_OrganizationGuid, _emp.OrganizationGuid);
                var rs = _context.Object_Combobox.FromSql(sql, _OrganizationGuid).ToList();
                return Json(rs);
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = "Có lỗi khi lấy dữ liệu." });
            }
        }
        [HttpGet]
        public object GetEthnics()
        {
            try
            {
                string sql = "[Hr].[SP_ESHR_GetAllEthnics] @DBName_COMMON";
                var _DBName_COMMON = new SqlParameter("@DBName_COMMON", SqlDbType.NVarChar, 50); CheckNullParameterStore(_DBName_COMMON, _appSettings.DBName_HR);
                var rs = _context.Object_Combobox.FromSql(sql, _DBName_COMMON).ToList();
                return Json(rs);
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = "Có lỗi khi lấy dữ liệu." });
            }
        }
        [HttpGet]
        public object GetBanks()
        {
            var _emp = GetEmployeeLogin(_context);
            try
            {
                string sql = "[Hr].[SP_ESHR_GetAllBanks] @OrganizationGuid,@DBName_COMMON";
                var _OrganizationGuid = new SqlParameter("@OrganizationGuid", SqlDbType.UniqueIdentifier); CheckNullParameterStore(_OrganizationGuid, _emp.OrganizationGuid);
                var _DBName_COMMON = new SqlParameter("@DBName_COMMON", SqlDbType.NVarChar, 50); CheckNullParameterStore(_DBName_COMMON, _appSettings.DBName_HR);
                var rs = _context.Object_Combobox.FromSql(sql, _OrganizationGuid, _DBName_COMMON).ToList();
                return Json(rs);
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = "Có lỗi khi lấy dữ liệu." });
            }
        }
        [HttpGet]
        public object GetProfestionals()
        {
            try
            {
                var _emp = GetEmployeeLogin(_context);
                string sql = "[Hr].[SP_ESHR_GetAllProfestionals] @OrganizationGuid";
                var _OrganizationGuid = new SqlParameter("@OrganizationGuid", SqlDbType.UniqueIdentifier); CheckNullParameterStore(_OrganizationGuid, _emp.OrganizationGuid);
                var rs = _context.Object_Combobox.FromSql(sql, _OrganizationGuid).ToList();
                return Json(rs);
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = "Có lỗi khi lấy dữ liệu." });
            }
        }
        [HttpGet]
        public object GetSalaryTier()
        {
            try
            {
                var _emp = GetEmployeeLogin(_context);

                string sql = "[Hr].[SP_ESHR_GetSalaryTier] @OrganizationGuid,@DBName_COMMON";
                var _OrganizationGuid = new SqlParameter("@OrganizationGuid", SqlDbType.UniqueIdentifier); CheckNullParameterStore(_OrganizationGuid, _emp.OrganizationGuid);
                var _DBName_COMMON = new SqlParameter("@DBName_COMMON", SqlDbType.NVarChar, 50); CheckNullParameterStore(_DBName_COMMON, _appSettings.DBName_HR);
                var rs = _context.Object_Combobox.FromSql(sql, _OrganizationGuid, _DBName_COMMON).ToList();
                return Json(rs);
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = "Có lỗi khi lấy dữ liệu." });
            }
        }
        [HttpGet]
        public object GetSpecializes()
        {
            try
            {
                var userlogin = GetEmployeeLogin(_context);
                var rs = _context.Specializes.Where(x => x.OrganizationGuid == userlogin.OrganizationGuid && x.IsActive == true).Select(x => new { value = x.CategoryId, text = x.CategoryName }).ToList();
                return Json(rs);
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = "Có lỗi khi lấy dữ liệu." });
            }
        }
        [HttpGet]
        public object GetRank()
        {
            try
            {
                var userlogin = GetEmployeeLogin(_context);
                var rs = _context.Rank.Where(x => x.OrganizationGuid == userlogin.OrganizationGuid).Select(x => new { value = x.CategoryId, text = x.CategoryName }).ToList();
                return Json(rs);
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = "Có lỗi khi lấy dữ liệu." });
            }
        }
        [HttpGet]
        public object GetOrganizations()
        {
            var userlogin = GetEmployeeLogin(_context);
            var data = _context.Organizations.Where(x => x.OrganizationGuid == userlogin.OrganizationGuid).Select(x => new { x.OrganizationId, x.OrganizationName }).ToList();
            return data;
        }

        [HttpPost]
        public object GetShiftOfEmployees(Guid? id)
        {
            try
            {
                var rs = _context.ShiftOfEmployees.Where(x => x.EmployeeGuid == id).Select(x => x.ShiftId).ToList();
                return Json(rs);
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = "Có lỗi khi lấy dữ liệu." });
            }
        }

        [HttpPost]
        public object GetEmployees_By_LoginName([FromBody] Employees data)
        {
            try
            {
                if (data == null)
                {
                    return Json("");
                }
                var _emp = GetEmployeeLogin(_context);
                string sql = "[Hr].[SP_ESHR_GetEmployees_By_LoginName] @OrganizationGuid,@LoginName";
                var _OrganizationGuid = new SqlParameter("@OrganizationGuid", SqlDbType.UniqueIdentifier); CheckNullParameterStore(_OrganizationGuid, _emp.OrganizationGuid);
                var _LoginName = new SqlParameter("@LoginName", SqlDbType.NVarChar, 50); CheckNullParameterStore(_LoginName, _emp.LoginName);
                var rs = _context.Employees.FromSql(sql, _OrganizationGuid, _LoginName).ToList();
                return Json(rs);
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = "Có lỗi khi lấy thông tin." });
            }
        }
        [Area("Hr")]
        public object getKiemnghiem()
        {
            try
            {
                var lstItem = new List<Object_Jexcel>();
                var _obj = new Object_Jexcel() { id = 0.ToString(), name = "Ngừng kiêm nhiệm" };
                var _obj1 = new Object_Jexcel() { id = 1.ToString(), name = "Đang kiêm nhiệm" };

                lstItem.Add(_obj); lstItem.Add(_obj1);

                return Json(lstItem);
            }
            catch (Exception ex)
            {
                return Json(new JMessage { Title = "Lấy dữ liệu không thành công", Object = ex, Error = true });
            }
        }
        public class Data : JTableModel
        {
            public List<string> Status { get; set; }
            public List<string> lstDepartmentID { get; set; }
            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }
            public string Search { get; set; }
            public int? JobTitleId { get; set; }
            public string Keyword { get; set; }
            public string EmployeeId { get; set; }
            public bool? IsActive { get; set; }

        }
        [HttpPost]
        //[ActionAuthorize(EAction.LISTVIEW)]
        public object JTable([FromBody] Data jTable)
        {
            try
            {
                var _emp = GetEmployeeLogin(_context);
                if (jTable.StartDate != null)
                {
                    jTable.StartDate = (DateTime)jTable.StartDate?.Date.AddDays(0) + new TimeSpan(23, 59, 59);
                }
                if (jTable.EndDate != null)
                {
                    jTable.EndDate = (DateTime)jTable.EndDate?.Date.AddDays(0) + new TimeSpan(23, 59, 59);
                }
                var lstStringDepartmentId = new List<string>();
                if (jTable.lstDepartmentID.Count >= 1)
                {
                    lstStringDepartmentId = (from p in _context.Departments where jTable.lstDepartmentID.Contains(p.DepartmentGuid.ToString()) select p.DepartmentId).Distinct().ToList();
                }
                int intBeginFor = (jTable.CurrentPage - 1) * jTable.Length;
                string sql = "[Hr].[SP_ESHR_GetAllEmployees_viewJTable] " +
                   "@Keyword, " +
                  "@OrganizationGuid, " +
                   "@JobTitleId, " +
                   "@lstDepartmentID, " +
                   "@StartDate, " +
                   "@EndDate, " +
                   "@Status, " +
                   "@Sort, " +
                   "@Skip, " +
                   "@Take," +
                   "@Count out";
                var _Keyword = new SqlParameter("@Keyword", SqlDbType.NVarChar, 255); CheckNullParameterStore(_Keyword, jTable.Keyword);
                var _OrganizationGuid = new SqlParameter("@OrganizationGuid", SqlDbType.UniqueIdentifier); CheckNullParameterStore(_OrganizationGuid, _emp.OrganizationGuid);
                var _JobTitleId = new SqlParameter("@JobTitleId", SqlDbType.Int); CheckNullParameterStore(_JobTitleId, jTable.JobTitleId);
                var _lstDepartmentID = new SqlParameter("@lstDepartmentID", jTable.lstDepartmentID.Count >= 1 ? string.Join(",", lstStringDepartmentId) : "");
                var _StartDate = new SqlParameter("@StartDate", SqlDbType.DateTime); CheckNullParameterStore(_StartDate, jTable.StartDate);
                var _EndDate = new SqlParameter("@EndDate", SqlDbType.DateTime); CheckNullParameterStore(_EndDate, jTable.EndDate);
                var _Status = new SqlParameter("@Status", jTable.Status.Count >= 1 ? string.Join(",", jTable.Status) : "");
                var _Sort = new SqlParameter("@Sort", SqlDbType.VarChar); _Sort.Value = jTable.QueryOrderBy;
                var _Skip = new SqlParameter("@Skip", SqlDbType.Int); _Skip.Value = intBeginFor;
                var _Take = new SqlParameter("@Take", SqlDbType.Int); _Take.Value = jTable.Length;
                var _Count = new SqlParameter("@Count", SqlDbType.Int) { Direction = System.Data.ParameterDirection.Output };
                var data = _context.EmployeesJtableView.FromSql(sql, _Keyword, _OrganizationGuid, _JobTitleId, _lstDepartmentID, _StartDate, _EndDate, _Status, _Sort, _Skip, _Take, _Count).ToList();
                var jdata = JTableHelper.JObjectTable(data, jTable.Draw, (int)_Count.Value);
                return Json(jdata);
            }
            catch (Exception ex)
            {
                return new JMessage() { Error = true, Title = "Có lỗi trong quá trình lấy dữ liệu", Object = ex };
            }
        }
        [HttpPost]

        public object UpdatePhoto()
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var files = _context.Employees.ToList();
                foreach (var item in files)
                {
                    if (item.ImagePath != "" && item.ImagePath != null)
                    {
                        var stream = new MemoryStream(item.Photo);
                        System.Drawing.Image img = System.Drawing.Image.FromStream(stream);
                        decimal sizeimg = ((decimal)img.Height / (decimal)img.Width) * 100;
                        System.Drawing.Image thumb = img.GetThumbnailImage(100, (int)sizeimg, ThumbCallback, IntPtr.Zero);
                        img.Dispose();
                        using (var _ms = new MemoryStream())
                        {
                            thumb.Save(_ms, System.Drawing.Imaging.ImageFormat.Png);
                            item.Thumb = _ms.ToArray();
                        }
                        _context.Employees.Update(item);
                        _context.Entry(item).State = EntityState.Modified;
                        _context.Entry(item).Property(x => x.EmployeeGuid).IsModified = false;
                        _context.Entry(item).Property(x => x.EmployeeId).IsModified = false;
                        _context.SaveChanges();
                    }

                }
                return new JMessage() { Error = false, Title = "Cập nhật thành công." };
            }
            catch (Exception ex)
            {
                msg.Title = "Có lỗi khi cập nhật";
                msg.Object = ex;
                msg.Error = true;
            }
            return Json(msg);
        }

        public class objTitleFile
        {
            public int? STT { get; set; }
            public string Title { get; set; }
        }
        public bool ThumbCallback()
        {
            return true;
        }
        
        [HttpPost]
        public object Insert_Employees()
        {
            var msg = new JMessage() { Error = false };

            try
            {
                var files = Request.Form.Files.ToList();
                var tempRecrui = Request.Form["Insert"];
                var Detail = Request.Form["PositionsConcurrently"];
                var _ShiftOfEmployees = Request.Form["ShiftOfEmployees"];
                var _emp = GetEmployeeLogin(_context);
               Employees obj = JsonConvert.DeserializeObject<Employees>(tempRecrui);
                
                List<Concurrently> detail = JsonConvert.DeserializeObject<List<Concurrently>>(Detail);
                List<ShiftOfEmployees> ShiftOfEmployees = JsonConvert.DeserializeObject<List<ShiftOfEmployees>>(_ShiftOfEmployees);
                obj.OrganizationGuid = _emp.OrganizationGuid;
                var org = _context.Organizations.Where(x => x.OrganizationGuid == obj.OrganizationGuid).Select(x => new { x.OrganizationId, x.OrganizationName }).FirstOrDefault();
                if (org != null) { obj.OrganizationId = org.OrganizationId; }
                if (obj.BirthDate.HasValue) { obj.BirthDate = obj.BirthDate.Value.ToLocalTime(); } else { obj.BirthDate = null; }
                if (obj.HireDate.HasValue) { obj.HireDate = obj.HireDate.Value.ToLocalTime(); } else { obj.HireDate = null; }
                if (obj.EndDateWork.HasValue) { obj.EndDateWork = obj.EndDateWork.Value.ToLocalTime(); } else { obj.EndDateWork = null; }
                if (obj.StartDate.HasValue) { obj.StartDate = obj.StartDate.Value.ToLocalTime(); } else { obj.StartDate = null; }
                if (obj.EndDate.HasValue) { obj.EndDate = obj.EndDate.Value.ToLocalTime(); } else { obj.EndDate = null; }
                if (obj.IssueIddate.HasValue) { obj.IssueIddate = obj.IssueIddate.Value.ToLocalTime(); } else { obj.IssueIddate = null; }


                //var EmpId = _context.Employees.Where(x => x.EmployeeId == obj.EmployeeId && x.OrganizationGuid == obj.OrganizationGuid && x.IsActive == true).Count();
                //if (EmpId > 0)
                //{
                //    return Json(new JMessage() { Error = true, Title = "Mã nhân viên đã tồn tại, xin kiểm tra lại" });
                //}
                if (obj.TimeKeepingId != null && obj.TimeKeepingId != "")
                {
                    var TimeId = _context.Employees.Where(x => x.TimeKeepingId == obj.TimeKeepingId && x.OrganizationGuid == obj.OrganizationGuid).Count();
                    if (TimeId > 0)
                    {
                        return Json(new JMessage() { Error = true, Title = "Mã chấm công đã tồn tại, xin kiểm tra lại" });
                    }
                }
                if (files.Count > 0)
                {
                    var stream = files[0].OpenReadStream();
                    obj.Photo = Utilities.StreamToByteArray(stream);
                    System.Drawing.Image img = System.Drawing.Image.FromStream(stream);
                    decimal sizeimg = ((decimal)img.Height / (decimal)img.Width) * 100;
                    System.Drawing.Image thumb = img.GetThumbnailImage(100, (int)sizeimg, ThumbCallback, IntPtr.Zero);
                    img.Dispose();
                    using (var _ms = new MemoryStream())
                    {
                        thumb.Save(_ms, System.Drawing.Imaging.ImageFormat.Png);
                        obj.Thumb = _ms.ToArray();
                    }
                    obj.ImagePath = files[0].FileName;
                }


                string sql = "[Hr].[SP_ESHR_Insert_Employees] @EmployeeGuid,@CandidateGuid,@CandidateID,@EmployeeID,@OrganizationGuid,@OrganizationID,@DepartmentGuid,@DepartmentID,@Title,@FullName,@FirstName,@MiddleName,@LastName,@Identification,@IssueIDDate,@IssueIDBy,@LoginID,@LoginName,@JobTitleID,@JobTitleName,@ProfestionalD,@ProfestionaName,@SpecializesID,@BirthDate,@PlaceOfBirth,@MaritalStatus,@Gender,@HireDate,@SalaryPayBy,@SalaryMethod,@PermanentAddress,@CurrentAddress,@CountryID,@CountryName,@ProvinceID,@ProvinceName,@DistrictID,@DistrictName,@WardID,@WardName,@HomeNumber,@EthnicID,@EthnicName,@HomePhone,@HomeMobile,@HomeEmail,@WorkPhone,@WorkMobile,@WorkEmail,@EducationID,@EducationName,@ImagePath,@Photo,@TaxCode,@BankCode,@BankName,@BankAccount,@StatusOfWork,@EndDateWork,@Note,@StartDate,@EndDate,@ProbationPeriod,@Uniform,@IsConcurrently,@TimeKeepingID,@IsUseCar,@CarID,@IsActive,@CreatedDate,@CreatedBy,@ModifiedBy,@ModifiedDate,@GenitiveID,@SalaryTierID,@RankID,@ReasonLeaveID,@ManagerID,@Thumb,@DBName_COMMON,@Concurrently,@Error";
                obj.EmployeeGuid = Guid.NewGuid();

                var EmployeeGuid = new SqlParameter("@EmployeeGuid", SqlDbType.UniqueIdentifier); EmployeeGuid.Value = obj.EmployeeGuid;
                var CandiGuid = _context.Candidates.Where(x => x.CandidateId == obj.CandidateId).Select(x => x.CandidateGuid).FirstOrDefault();

                var CandidateGuid = new SqlParameter("@CandidateGuid", SqlDbType.UniqueIdentifier); CheckNullParameterStore(CandidateGuid, CandiGuid);
                var CandidateID = new SqlParameter("@CandidateID", SqlDbType.VarChar, 20); CheckNullParameterStore(CandidateID, obj.CandidateId);
                var EmployeeID = new SqlParameter("@EmployeeID", SqlDbType.VarChar, 20); CheckNullParameterStore(EmployeeID, obj.EmployeeId);
                var OrganizationGuid = new SqlParameter("@OrganizationGuid", SqlDbType.UniqueIdentifier); CheckNullParameterStore(OrganizationGuid, obj.OrganizationGuid);
                var OrganizationID = new SqlParameter("@OrganizationID", SqlDbType.VarChar, 20); CheckNullParameterStore(OrganizationID, obj.OrganizationId);
                var DepartmentGuid = new SqlParameter("@DepartmentGuid", SqlDbType.UniqueIdentifier); CheckNullParameterStore(DepartmentGuid, obj.DepartmentGuid);
                var DepartmentId = _context.Departments.Where(x => x.DepartmentGuid == obj.DepartmentGuid).Select(x => x.DepartmentId).FirstOrDefault();
                var DepartmentID = new SqlParameter("@DepartmentID", SqlDbType.VarChar, 20); CheckNullParameterStore(DepartmentID, DepartmentId);
                var Title = new SqlParameter("@Title", SqlDbType.NVarChar, 8); CheckNullParameterStore(Title, obj.Title);
                var FullName = new SqlParameter("@FullName", SqlDbType.NVarChar, 60); CheckNullParameterStore(FullName, obj.FullName);
                var FirstName = new SqlParameter("@FirstName", SqlDbType.NVarChar, 20); CheckNullParameterStore(FirstName, obj.FirstName);
                var MiddleName = new SqlParameter("@MiddleName", SqlDbType.NVarChar, 250); CheckNullParameterStore(MiddleName, obj.MiddleName);
                var LastName = new SqlParameter("@LastName", SqlDbType.NVarChar, 20); CheckNullParameterStore(LastName, obj.LastName);
                var Identification = new SqlParameter("@Identification", SqlDbType.VarChar, 20); CheckNullParameterStore(Identification, obj.Identification);
                var IssueIDDate = new SqlParameter("@IssueIDDate", SqlDbType.Date); CheckNullParameterStore(IssueIDDate, obj.IssueIddate);
                var IssueIDBy = new SqlParameter("@IssueIDBy", SqlDbType.NVarChar, 50); CheckNullParameterStore(IssueIDBy, obj.IssueIdby);
                var LoginID = new SqlParameter("@LoginID", SqlDbType.NVarChar, 50); CheckNullParameterStore(LoginID, obj.LoginId);
                var LoginName = new SqlParameter("@LoginName", SqlDbType.NVarChar, 50); CheckNullParameterStore(LoginName, obj.LoginName);
                var JobTitleID = new SqlParameter("@JobTitleID", SqlDbType.Int); CheckNullParameterStore(JobTitleID, obj.JobTitleId);
                var _JobTitleName = _context.JobTitles.Where(x => x.JobTitleId == obj.JobTitleId).Select(x => x.JobTitleName).FirstOrDefault();
                var JobTitleName = new SqlParameter("@JobTitleName", SqlDbType.NVarChar, 50); CheckNullParameterStore(JobTitleName, _JobTitleName);
                var ProfestionalD = new SqlParameter("@ProfestionalD", SqlDbType.Int); CheckNullParameterStore(ProfestionalD, obj.ProfestionalD);
                var _ProfestionaName = _context.Profestionals.Where(x => x.ProfestionalId == obj.ProfestionalD).Select(x => x.ProfestionaName).FirstOrDefault();

                var ProfestionaName = new SqlParameter("@ProfestionaName", SqlDbType.NVarChar, 50); CheckNullParameterStore(ProfestionaName, _ProfestionaName);
                var SpecializesID = new SqlParameter("@SpecializesID", SqlDbType.VarChar, 50); CheckNullParameterStore(SpecializesID, obj.SpecializesId);

                var BirthDate = new SqlParameter("@BirthDate", SqlDbType.Date); CheckNullParameterStore(BirthDate, obj.BirthDate);
                var PlaceOfBirth = new SqlParameter("@PlaceOfBirth", SqlDbType.NVarChar, 100); CheckNullParameterStore(PlaceOfBirth, obj.PlaceOfBirth);
                var MaritalStatus = new SqlParameter("@MaritalStatus", SqlDbType.VarChar, 1); CheckNullParameterStore(MaritalStatus, obj.MaritalStatus);
                var Gender = new SqlParameter("@Gender", SqlDbType.VarChar, 1); CheckNullParameterStore(Gender, obj.Gender);
                var HireDate = new SqlParameter("@HireDate", SqlDbType.Date); CheckNullParameterStore(HireDate, obj.HireDate);
                var SalaryPayBy = new SqlParameter("@SalaryPayBy", SqlDbType.VarChar, 1); CheckNullParameterStore(SalaryPayBy, obj.SalaryPayBy);
                var SalaryMethod = new SqlParameter("@SalaryMethod", SqlDbType.VarChar, 2); CheckNullParameterStore(SalaryMethod, obj.SalaryMethod);
                var PermanentAddress = new SqlParameter("@PermanentAddress", SqlDbType.NVarChar, 255); CheckNullParameterStore(PermanentAddress, obj.PermanentAddress);
                var CurrentAddress = new SqlParameter("@CurrentAddress", SqlDbType.NVarChar, 255); CheckNullParameterStore(CurrentAddress, obj.CurrentAddress);
                var CountryID = new SqlParameter("@CountryID", SqlDbType.VarChar, 20); CheckNullParameterStore(CountryID, obj.CountryId);

                var CountryName = new SqlParameter("@CountryName", SqlDbType.NVarChar, 50); CheckNullParameterStore(CountryName, obj.CountryName);
                var ProvinceID = new SqlParameter("@ProvinceID", SqlDbType.VarChar, 20); CheckNullParameterStore(ProvinceID, obj.ProvinceId);
                var ProvinceName = new SqlParameter("@ProvinceName", SqlDbType.NVarChar, 50); CheckNullParameterStore(ProvinceName, obj.ProvinceName);
                var DistrictID = new SqlParameter("@DistrictID", SqlDbType.VarChar, 20); CheckNullParameterStore(DistrictID, obj.DistrictId);
                var DistrictName = new SqlParameter("@DistrictName", SqlDbType.NVarChar, 50); CheckNullParameterStore(DistrictName, obj.DistrictName);
                var WardID = new SqlParameter("@WardID", SqlDbType.VarChar, 20); CheckNullParameterStore(WardID, obj.WardId);

                var WardName = new SqlParameter("@WardName", SqlDbType.NVarChar, 20); CheckNullParameterStore(WardName, obj.WardName);
                var HomeNumber = new SqlParameter("@HomeNumber", SqlDbType.NVarChar, 50); CheckNullParameterStore(HomeNumber, obj.HomeNumber);

                var EthnicID = new SqlParameter("@EthnicID", SqlDbType.Int); CheckNullParameterStore(EthnicID, obj.EthnicId);
                var EthnicName = new SqlParameter("@EthnicName", SqlDbType.NVarChar, 10); CheckNullParameterStore(EthnicName, obj.EthnicName);
                var HomePhone = new SqlParameter("@HomePhone", SqlDbType.VarChar, 15); CheckNullParameterStore(HomePhone, obj.HomePhone);
                var HomeMobile = new SqlParameter("@HomeMobile", SqlDbType.VarChar, 20); CheckNullParameterStore(HomeMobile, obj.HomeMobile);
                var HomeEmail = new SqlParameter("@HomeEmail", SqlDbType.NVarChar, 50); CheckNullParameterStore(HomeEmail, obj.HomeEmail);
                var WorkPhone = new SqlParameter("@WorkPhone", SqlDbType.VarChar, 15); CheckNullParameterStore(WorkPhone, obj.WorkPhone);
                var WorkMobile = new SqlParameter("@WorkMobile", SqlDbType.VarChar, 20); CheckNullParameterStore(WorkMobile, obj.WorkMobile);
                var WorkEmail = new SqlParameter("@WorkEmail", SqlDbType.NVarChar, 50); CheckNullParameterStore(WorkEmail, obj.WorkEmail);
                var EducationID = new SqlParameter("@EducationID", SqlDbType.Int); CheckNullParameterStore(EducationID, obj.EducationId);
                var _EducationName = _context.Educations.Where(x => x.EducationId == obj.EducationId).Select(x => x.EducationName).FirstOrDefault();
                var EducationName = new SqlParameter("@EducationName ", SqlDbType.NVarChar, 20); CheckNullParameterStore(EducationName, _EducationName);
                var ImagePath = new SqlParameter("@ImagePath", SqlDbType.NVarChar, 255); CheckNullParameterStore(ImagePath, obj.ImagePath);
                var Photo = new SqlParameter("@Photo", SqlDbType.VarBinary); CheckNullParameterStore(Photo, obj.Photo);
                var TaxCode = new SqlParameter("@TaxCode", SqlDbType.VarChar, 20); CheckNullParameterStore(TaxCode, obj.TaxCode);
                var BankCode = new SqlParameter("@BankCode", SqlDbType.VarChar, 20); CheckNullParameterStore(BankCode, obj.BankCode);
                var BankName = new SqlParameter("@BankName", SqlDbType.NVarChar, 255); CheckNullParameterStore(BankName, obj.BankName);
                var BankAccount = new SqlParameter("@BankAccount", SqlDbType.VarChar, 50); CheckNullParameterStore(BankAccount, obj.BankAccount);
                var StatusOfWork = new SqlParameter("@StatusOfWork", SqlDbType.VarChar, 2); CheckNullParameterStore(StatusOfWork, obj.StatusOfWork);
                var EndDateWork = new SqlParameter("@EndDateWork", SqlDbType.Date); CheckNullParameterStore(EndDateWork, obj.EndDateWork);
                var Note = new SqlParameter("@Note", SqlDbType.NVarChar, 255); CheckNullParameterStore(Note, obj.Note);
                var StartDate = new SqlParameter("@StartDate ", SqlDbType.Date); CheckNullParameterStore(StartDate, obj.StartDate);
                var EndDate = new SqlParameter("@EndDate ", SqlDbType.Date); CheckNullParameterStore(EndDate, obj.EndDate);
                var ProbationPeriod = new SqlParameter("@ProbationPeriod ", SqlDbType.Int); CheckNullParameterStore(ProbationPeriod, obj.ProbationPeriod);
                var Uniform = new SqlParameter("@Uniform ", SqlDbType.VarChar, 1); CheckNullParameterStore(Uniform, obj.Uniform);
                if (detail.Count > 0)
                {
                    obj.IsConcurrently = "Y";
                }
                else
                {
                    obj.IsConcurrently = "N";
                }
                var IsConcurrently = new SqlParameter("@IsConcurrently ", SqlDbType.VarChar, 1); CheckNullParameterStore(IsConcurrently, obj.IsConcurrently);
                var TimeKeepingID = new SqlParameter("@TimeKeepingID ", SqlDbType.VarChar, 20); CheckNullParameterStore(TimeKeepingID, obj.TimeKeepingId);
                var IsUseCar = new SqlParameter("@IsUseCar ", SqlDbType.Bit); CheckNullParameterStore(IsUseCar, obj.IsUseCar);
                var CarID = new SqlParameter("@CarID ", SqlDbType.VarChar, 10); CheckNullParameterStore(CarID, obj.CarId);
                var IsActive = new SqlParameter("@IsActive ", SqlDbType.Bit); CheckNullParameterStore(IsActive, obj.IsActive);
                var CreatedBy = new SqlParameter("@CreatedBy", SqlDbType.NVarChar, 50); CheckNullParameterStore(CreatedBy, GetCreatedBy(_context));
                var CreatedDate = new SqlParameter("@CreatedDate", SqlDbType.DateTime); CheckNullParameterStore(CreatedDate, DateTime.Now);
                var ModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.NVarChar, 50); CheckNullParameterStore(ModifiedBy, GetModifiedBy(_context));
                var ModifiedDate = new SqlParameter("@ModifiedDate", SqlDbType.DateTime); CheckNullParameterStore(ModifiedDate, DateTime.Now);
                var GenitiveID = new SqlParameter("@GenitiveID", SqlDbType.VarChar, 50); CheckNullParameterStore(GenitiveID, obj.GenitiveId);
                var SalaryTierID = new SqlParameter("@SalaryTierID", SqlDbType.VarChar, 50); CheckNullParameterStore(SalaryTierID, obj.SalaryTierId);
                var RankID = new SqlParameter("@RankID", SqlDbType.VarChar, 50); CheckNullParameterStore(RankID, obj.RankId);
                var ReasonLeaveID = new SqlParameter("@ReasonLeaveID", SqlDbType.Int); CheckNullParameterStore(ReasonLeaveID, obj.ReasonLeaveId);
                var ManagerId = new SqlParameter("@ManagerId", SqlDbType.UniqueIdentifier); CheckNullParameterStore(ManagerId, obj.ManagerId);
                var Thumb = new SqlParameter("@Thumb", SqlDbType.VarBinary); CheckNullParameterStore(Thumb, obj.Thumb);

                var _DBName_COMMON = new SqlParameter("@DBName_COMMON", SqlDbType.NVarChar, 50); CheckNullParameterStore(_DBName_COMMON, _appSettings.DBName_HR);

                var _Error = new SqlParameter("@Error", SqlDbType.NVarChar); _Error.Value = "";
                var _detail = new SqlParameter("@Concurrently", SqlDbType.Structured);
                /// table detail
                var _tem = new DataTable();
                _tem.Columns.Add("ConcurrentlyGuid");
                _tem.Columns.Add("EmployeeGuid");
                _tem.Columns.Add("EmployeeID");
                _tem.Columns.Add("OrganizationGuid");
                _tem.Columns.Add("DepartmentGuid");
                _tem.Columns.Add("DepartmentID");
                _tem.Columns.Add("DepartmentName");
                _tem.Columns.Add("JobTitleID");
                _tem.Columns.Add("JobTitleName");
                _tem.Columns.Add("StartDate", typeof(DateTime));
                _tem.Columns.Add("EndDate", typeof(DateTime));
                _tem.Columns.Add("IsActive");
                _tem.Columns.Add("CreatedDate");
                _tem.Columns.Add("ModifiedDate");
                _tem.Columns.Add("CreatedBy");
                _tem.Columns.Add("ModifiedBy");
                foreach (var _item in detail)
                {
                    _item.ConcurrentlyGuid = Guid.NewGuid();
                    if (_item.StartDate.HasValue) { _item.StartDate = _item.StartDate.Value.ToLocalTime(); } else { _item.StartDate = null; }
                    if (_item.EndDate.HasValue) { _item.EndDate = _item.EndDate.Value.ToLocalTime(); } else { _item.EndDate = null; }
                    _item.DepartmentId = _context.Departments.Where(x => x.DepartmentGuid == _item.DepartmentGuid).Select(x => x.DepartmentId).FirstOrDefault();
                    _item.JobTitleName = _context.JobTitles.Where(x => x.JobTitleId == _item.JobTitleId).Select(x => x.JobTitleName).FirstOrDefault();
                    _tem.Rows.Add(_item.ConcurrentlyGuid, obj.EmployeeGuid, obj.EmployeeId, obj.OrganizationGuid, _item.DepartmentGuid, _item.DepartmentId, _item.DepartmentName, _item.JobTitleId, _item.JobTitleName, _item.StartDate, _item.EndDate, _item.IsActive, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value);
                }

                _detail.Value = _tem;
                _detail.TypeName = "[hr].Concurrently";

                var __tem = _context.SQLCOMMANDS.FromSql(sql, EmployeeGuid, CandidateGuid, CandidateID, EmployeeID, OrganizationGuid, OrganizationID, DepartmentGuid, DepartmentID, Title, FullName, FirstName, MiddleName, LastName, Identification, IssueIDDate, IssueIDBy, LoginID, LoginName, JobTitleID, JobTitleName, ProfestionalD, ProfestionaName, SpecializesID, BirthDate, PlaceOfBirth, MaritalStatus, Gender, HireDate, SalaryPayBy, SalaryMethod, PermanentAddress, CurrentAddress, CountryID, CountryName, ProvinceID, ProvinceName, DistrictID, DistrictName, WardID, WardName, HomeNumber, EthnicID, EthnicName, HomePhone, HomeMobile, HomeEmail, WorkPhone, WorkMobile, WorkEmail, EducationID, EducationName, ImagePath, Photo, TaxCode, BankCode, BankName, BankAccount, StatusOfWork, EndDateWork, Note, StartDate, EndDate, ProbationPeriod, Uniform, IsConcurrently, TimeKeepingID, IsUseCar, CarID, IsActive, CreatedDate, CreatedBy, ModifiedBy, ModifiedDate, GenitiveID, SalaryTierID, RankID, ReasonLeaveID, ManagerId, Thumb, _DBName_COMMON, _detail, _Error).FirstOrDefault();


                if (__tem.Error == "")
                {
                    if (ShiftOfEmployees.Count > 0)
                    {
                        foreach (var item in ShiftOfEmployees)
                        {
                            var sh = new ShiftOfEmployees();
                            sh.ShiftId = item.ShiftId;
                            sh.EmployeeGuid = obj.EmployeeGuid;
                            _context.ShiftOfEmployees.Add(sh);
                            _context.SaveChanges();
                        }
                    }


                    return new JMessage() { Error = false, Title = "Thêm mới thành công." };
                }
                else
                {
                    return new JMessage() { Error = true, Title = __tem.Error, Object = __tem };
                }
            }
            catch (Exception ex)
            {

                msg.Title = "Có lỗi khi thêm";

                msg.Object = ex;
                msg.Error = true;

            }

            return Json(msg);
        }
        [HttpPost]
        public object Update_Employees()
        {
            var msg = new JMessage() { Error = false };

            try
            {
                var files = Request.Form.Files.ToList();
                var tempRecrui = Request.Form["Insert"];
                var Detail = Request.Form["PositionsConcurrently"];
                var _emp = GetEmployeeLogin(_context);
               Employees obj = JsonConvert.DeserializeObject<Employees>(tempRecrui);
                List<Concurrently> detail = JsonConvert.DeserializeObject<List<Concurrently>>(Detail);
                var _ShiftOfEmployees = Request.Form["ShiftOfEmployees"];
                List<ShiftOfEmployees> ShiftOfEmployees = JsonConvert.DeserializeObject<List<ShiftOfEmployees>>(_ShiftOfEmployees);
                var ListDetailDelete = Request.Form["ListDetailDelete"];
                List<Guid> pddatadelete = JsonConvert.DeserializeObject<List<Guid>>(ListDetailDelete);
                obj.OrganizationGuid = _emp.OrganizationGuid;
                var org = _context.Organizations.Where(x => x.OrganizationGuid == obj.OrganizationGuid).Select(x => new { x.OrganizationId, x.OrganizationName }).FirstOrDefault();
                if (org != null) { obj.OrganizationId = org.OrganizationId; }

                if (obj.BirthDate.HasValue) { obj.BirthDate = obj.BirthDate.Value.ToLocalTime(); } else { obj.BirthDate = null; }
                if (obj.HireDate.HasValue) { obj.HireDate = obj.HireDate.Value.ToLocalTime(); } else { obj.HireDate = null; }
                if (obj.EndDateWork.HasValue) { obj.EndDateWork = obj.EndDateWork.Value.ToLocalTime(); } else { obj.EndDateWork = null; }
                if (obj.StartDate.HasValue) { obj.StartDate = obj.StartDate.Value.ToLocalTime(); } else { obj.StartDate = null; }
                if (obj.EndDate.HasValue) { obj.EndDate = obj.EndDate.Value.ToLocalTime(); } else { obj.EndDate = null; }
                if (obj.IssueIddate.HasValue) { obj.IssueIddate = obj.IssueIddate.Value.ToLocalTime(); } else { obj.IssueIddate = null; }


                if (obj.TimeKeepingId != null && obj.TimeKeepingId != "")
                {
                    var TimeId = _context.Employees.Where(x => x.TimeKeepingId == obj.TimeKeepingId && x.OrganizationGuid == obj.OrganizationGuid && x.EmployeeGuid != obj.EmployeeGuid).Count();
                    if (TimeId > 0)
                    {
                        return Json(new JMessage() { Error = true, Title = "Mã chấm công đã tồn tại, xin kiểm tra lại" });
                    }
                }
                if (files.Count > 0)
                {
                    var stream = files[0].OpenReadStream();
                    obj.Photo = Utilities.StreamToByteArray(stream);
                    obj.ImagePath = files[0].FileName;
                    System.Drawing.Image img = System.Drawing.Image.FromStream(stream);
                    decimal sizeimg = ((decimal)img.Height / (decimal)img.Width) * 100;
                    System.Drawing.Image thumb = img.GetThumbnailImage(100, (int)sizeimg, ThumbCallback, IntPtr.Zero);
                    img.Dispose();
                    using (var _ms = new MemoryStream())
                    {
                        thumb.Save(_ms, System.Drawing.Imaging.ImageFormat.Png);
                        obj.Thumb = _ms.ToArray();
                    }
                }
               
                string sql = "[HR].[SP_ESHR_Update_Employees] @EmployeeGuid,@CandidateGuid,@CandidateID,@EmployeeID,@OrganizationGuid,@OrganizationID,@DepartmentGuid,@DepartmentID,@Title,@FullName,@FirstName,@MiddleName,@LastName,@Identification,@IssueIDDate,@IssueIDBy,@LoginID,@LoginName,@JobTitleID,@JobTitleName,@ProfestionalD,@ProfestionaName,@SpecializesID,@BirthDate,@PlaceOfBirth,@MaritalStatus,@Gender,@HireDate,@SalaryPayBy,@SalaryMethod,@PermanentAddress,@CurrentAddress,@CountryID,@CountryName,@ProvinceID,@ProvinceName,@DistrictID,@DistrictName,@WardID,@WardName,@HomeNumber,@EthnicID,@EthnicName,@HomePhone,@HomeMobile,@HomeEmail,@WorkPhone,@WorkMobile,@WorkEmail,@EducationID,@EducationName,@ImagePath,@Photo,@TaxCode,@BankCode,@BankName,@BankAccount,@StatusOfWork,@EndDateWork,@Note,@StartDate,@EndDate,@ProbationPeriod,@Uniform,@IsConcurrently,@TimeKeepingID,@IsUseCar,@CarID,@IsActive,@CreatedDate,@CreatedBy,@ModifiedBy,@ModifiedDate,@GenitiveID,@SalaryTierID,@RankID,@ReasonLeaveID,@ManagerID,@Thumb,@DBName_COMMON,@Concurrently,@lstDeleteDetail,@Error";

                var EmployeeGuid = new SqlParameter("@EmployeeGuid", SqlDbType.UniqueIdentifier); EmployeeGuid.Value = obj.EmployeeGuid;
                var CandiGuid = _context.Candidates.Where(x => x.CandidateId == obj.CandidateId).Select(x => x.CandidateGuid).FirstOrDefault();
                var CandidateGuid = new SqlParameter("@CandidateGuid", SqlDbType.UniqueIdentifier); CheckNullParameterStore(CandidateGuid, CandiGuid);
                var CandidateID = new SqlParameter("@CandidateID", SqlDbType.VarChar, 20); CheckNullParameterStore(CandidateID, obj.CandidateId);
                var EmployeeID = new SqlParameter("@EmployeeID", SqlDbType.VarChar, 20); CheckNullParameterStore(EmployeeID, obj.EmployeeId);
                var OrganizationGuid = new SqlParameter("@OrganizationGuid", SqlDbType.UniqueIdentifier); CheckNullParameterStore(OrganizationGuid, obj.OrganizationGuid);
                var OrganizationID = new SqlParameter("@OrganizationID", SqlDbType.VarChar, 20); CheckNullParameterStore(OrganizationID, obj.OrganizationId);
                var DepartmentGuid = new SqlParameter("@DepartmentGuid", SqlDbType.UniqueIdentifier); CheckNullParameterStore(DepartmentGuid, obj.DepartmentGuid);
                var DepartmentId = _context.Departments.Where(x => x.DepartmentGuid == obj.DepartmentGuid).Select(x => x.DepartmentId).FirstOrDefault();
                var DepartmentID = new SqlParameter("@DepartmentID", SqlDbType.VarChar, 20); CheckNullParameterStore(DepartmentID, DepartmentId);
                var Title = new SqlParameter("@Title", SqlDbType.NVarChar, 8); CheckNullParameterStore(Title, obj.Title);
                var FullName = new SqlParameter("@FullName", SqlDbType.NVarChar, 60); CheckNullParameterStore(FullName, obj.FullName);
                var FirstName = new SqlParameter("@FirstName", SqlDbType.NVarChar, 20); CheckNullParameterStore(FirstName, obj.FirstName);
                var MiddleName = new SqlParameter("@MiddleName", SqlDbType.NVarChar, 250); CheckNullParameterStore(MiddleName, obj.MiddleName);
                var LastName = new SqlParameter("@LastName", SqlDbType.NVarChar, 20); CheckNullParameterStore(LastName, obj.LastName);
                var Identification = new SqlParameter("@Identification", SqlDbType.VarChar, 20); CheckNullParameterStore(Identification, obj.Identification);
                var IssueIDDate = new SqlParameter("@IssueIDDate", SqlDbType.Date); CheckNullParameterStore(IssueIDDate, obj.IssueIddate);
                var IssueIDBy = new SqlParameter("@IssueIDBy", SqlDbType.NVarChar, 50); CheckNullParameterStore(IssueIDBy, obj.IssueIdby);
                var LoginID = new SqlParameter("@LoginID", SqlDbType.NVarChar, 50); CheckNullParameterStore(LoginID, obj.LoginId);
                var LoginName = new SqlParameter("@LoginName", SqlDbType.NVarChar, 50); CheckNullParameterStore(LoginName, obj.LoginName);
                var JobTitleID = new SqlParameter("@JobTitleID", SqlDbType.Int); CheckNullParameterStore(JobTitleID, obj.JobTitleId);
                var _JobTitleName = _context.JobTitles.Where(x => x.JobTitleId == obj.JobTitleId).Select(x => x.JobTitleName).FirstOrDefault();
                var JobTitleName = new SqlParameter("@JobTitleName", SqlDbType.NVarChar, 50); CheckNullParameterStore(JobTitleName, _JobTitleName);
                var ProfestionalD = new SqlParameter("@ProfestionalD", SqlDbType.Int); CheckNullParameterStore(ProfestionalD, obj.ProfestionalD);
                var _ProfestionaName = _context.Profestionals.Where(x => x.ProfestionalId == obj.ProfestionalD).Select(x => x.ProfestionaName).FirstOrDefault();
                var ProfestionaName = new SqlParameter("@ProfestionaName", SqlDbType.NVarChar, 50); CheckNullParameterStore(ProfestionaName, _ProfestionaName);
                var SpecializesID = new SqlParameter("@SpecializesID", SqlDbType.VarChar, 50); CheckNullParameterStore(SpecializesID, obj.SpecializesId);

                var BirthDate = new SqlParameter("@BirthDate", SqlDbType.Date); CheckNullParameterStore(BirthDate, obj.BirthDate);
                var PlaceOfBirth = new SqlParameter("@PlaceOfBirth", SqlDbType.NVarChar, 100); CheckNullParameterStore(PlaceOfBirth, obj.PlaceOfBirth);
                var MaritalStatus = new SqlParameter("@MaritalStatus", SqlDbType.VarChar, 1); CheckNullParameterStore(MaritalStatus, obj.MaritalStatus);
                var Gender = new SqlParameter("@Gender", SqlDbType.VarChar, 1); CheckNullParameterStore(Gender, obj.Gender);
                var HireDate = new SqlParameter("@HireDate", SqlDbType.Date); CheckNullParameterStore(HireDate, obj.HireDate);
                var SalaryPayBy = new SqlParameter("@SalaryPayBy", SqlDbType.VarChar, 1); CheckNullParameterStore(SalaryPayBy, obj.SalaryPayBy);
                var SalaryMethod = new SqlParameter("@SalaryMethod", SqlDbType.VarChar, 2); CheckNullParameterStore(SalaryMethod, obj.SalaryMethod);
                var PermanentAddress = new SqlParameter("@PermanentAddress", SqlDbType.NVarChar, 255); CheckNullParameterStore(PermanentAddress, obj.PermanentAddress);
                var CurrentAddress = new SqlParameter("@CurrentAddress", SqlDbType.NVarChar, 255); CheckNullParameterStore(CurrentAddress, obj.CurrentAddress);
                var CountryID = new SqlParameter("@CountryID", SqlDbType.VarChar, 20); CheckNullParameterStore(CountryID, obj.CountryId);

                var CountryName = new SqlParameter("@CountryName", SqlDbType.NVarChar, 50); CheckNullParameterStore(CountryName, obj.CountryName);
                var ProvinceID = new SqlParameter("@ProvinceID", SqlDbType.VarChar, 20); CheckNullParameterStore(ProvinceID, obj.ProvinceId);
                var ProvinceName = new SqlParameter("@ProvinceName", SqlDbType.NVarChar, 50); CheckNullParameterStore(ProvinceName, obj.ProvinceName);
                var DistrictID = new SqlParameter("@DistrictID", SqlDbType.VarChar, 20); CheckNullParameterStore(DistrictID, obj.DistrictId);
                var DistrictName = new SqlParameter("@DistrictName", SqlDbType.NVarChar, 50); CheckNullParameterStore(DistrictName, obj.DistrictName);
                var WardID = new SqlParameter("@WardID", SqlDbType.VarChar, 20); CheckNullParameterStore(WardID, obj.WardId);

                var WardName = new SqlParameter("@WardName", SqlDbType.NVarChar, 20); CheckNullParameterStore(WardName, obj.WardName);
                var HomeNumber = new SqlParameter("@HomeNumber", SqlDbType.NVarChar, 50); CheckNullParameterStore(HomeNumber, obj.HomeNumber);

                var EthnicID = new SqlParameter("@EthnicID", SqlDbType.Int); CheckNullParameterStore(EthnicID, obj.EthnicId);
                var EthnicName = new SqlParameter("@EthnicName", SqlDbType.NVarChar, 10); CheckNullParameterStore(EthnicName, obj.EthnicName);
                var HomePhone = new SqlParameter("@HomePhone", SqlDbType.VarChar, 15); CheckNullParameterStore(HomePhone, obj.HomePhone);
                var HomeMobile = new SqlParameter("@HomeMobile", SqlDbType.VarChar, 20); CheckNullParameterStore(HomeMobile, obj.HomeMobile);
                var HomeEmail = new SqlParameter("@HomeEmail", SqlDbType.NVarChar, 50); CheckNullParameterStore(HomeEmail, obj.HomeEmail);
                var WorkPhone = new SqlParameter("@WorkPhone", SqlDbType.VarChar, 15); CheckNullParameterStore(WorkPhone, obj.WorkPhone);
                var WorkMobile = new SqlParameter("@WorkMobile", SqlDbType.VarChar, 20); CheckNullParameterStore(WorkMobile, obj.WorkMobile);
                var WorkEmail = new SqlParameter("@WorkEmail", SqlDbType.NVarChar, 50); CheckNullParameterStore(WorkEmail, obj.WorkEmail);
                var EducationID = new SqlParameter("@EducationID", SqlDbType.Int); CheckNullParameterStore(EducationID, obj.EducationId);
                var _EducationName = _context.Educations.Where(x => x.EducationId == obj.EducationId).Select(x => x.EducationName).FirstOrDefault();
                var EducationName = new SqlParameter("@EducationName ", SqlDbType.NVarChar, 20); CheckNullParameterStore(EducationName, _EducationName);
                var ImagePath = new SqlParameter("@ImagePath", SqlDbType.NVarChar, 255); CheckNullParameterStore(ImagePath, obj.ImagePath);
                var Photo = new SqlParameter("@Photo", SqlDbType.VarBinary); CheckNullParameterStore(Photo, obj.Photo);
                var TaxCode = new SqlParameter("@TaxCode", SqlDbType.VarChar, 20); CheckNullParameterStore(TaxCode, obj.TaxCode);
                var BankCode = new SqlParameter("@BankCode", SqlDbType.VarChar, 20); CheckNullParameterStore(BankCode, obj.BankCode);
                var BankName = new SqlParameter("@BankName", SqlDbType.NVarChar, 255); CheckNullParameterStore(BankName, obj.BankName);
                var BankAccount = new SqlParameter("@BankAccount", SqlDbType.VarChar, 50); CheckNullParameterStore(BankAccount, obj.BankAccount);

                var StatusOfWork = new SqlParameter("@StatusOfWork", SqlDbType.VarChar, 2); CheckNullParameterStore(StatusOfWork, obj.StatusOfWork);
                var EndDateWork = new SqlParameter("@EndDateWork", SqlDbType.Date); CheckNullParameterStore(EndDateWork, obj.EndDateWork);
                var Note = new SqlParameter("@Note", SqlDbType.NVarChar, 255); CheckNullParameterStore(Note, obj.Note);
                var StartDate = new SqlParameter("@StartDate ", SqlDbType.Date); CheckNullParameterStore(StartDate, obj.StartDate);
                var EndDate = new SqlParameter("@EndDate ", SqlDbType.Date); CheckNullParameterStore(EndDate, obj.EndDate);
                var ProbationPeriod = new SqlParameter("@ProbationPeriod ", SqlDbType.Int); CheckNullParameterStore(ProbationPeriod, obj.ProbationPeriod);
                var Uniform = new SqlParameter("@Uniform ", SqlDbType.VarChar, 1); CheckNullParameterStore(Uniform, obj.Uniform);
                var IsConcurrently = new SqlParameter("@IsConcurrently ", SqlDbType.VarChar, 1); CheckNullParameterStore(IsConcurrently, obj.IsConcurrently);
                var TimeKeepingID = new SqlParameter("@TimeKeepingID ", SqlDbType.VarChar, 20); CheckNullParameterStore(TimeKeepingID, obj.TimeKeepingId);
                var IsUseCar = new SqlParameter("@IsUseCar ", SqlDbType.Bit); CheckNullParameterStore(IsUseCar, obj.IsUseCar);
                var CarID = new SqlParameter("@CarID ", SqlDbType.VarChar, 10); CheckNullParameterStore(CarID, obj.CarId);
                var IsActive = new SqlParameter("@IsActive ", SqlDbType.Bit); CheckNullParameterStore(IsActive, obj.IsActive);
                var CreatedBy = new SqlParameter("@CreatedBy", SqlDbType.NVarChar, 50); CheckNullParameterStore(CreatedBy, GetCreatedBy(_context));
                var CreatedDate = new SqlParameter("@CreatedDate", SqlDbType.DateTime); CheckNullParameterStore(CreatedDate, DateTime.Now);
                var ModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.NVarChar, 50); CheckNullParameterStore(ModifiedBy, GetModifiedBy(_context));
                var ModifiedDate = new SqlParameter("@ModifiedDate", SqlDbType.DateTime); CheckNullParameterStore(ModifiedDate, DateTime.Now);
                var GenitiveID = new SqlParameter("@GenitiveID", SqlDbType.VarChar, 50); CheckNullParameterStore(GenitiveID, obj.GenitiveId);
                var SalaryTierID = new SqlParameter("@SalaryTierID", SqlDbType.VarChar, 50); CheckNullParameterStore(SalaryTierID, obj.SalaryTierId);
                var RankID = new SqlParameter("@RankID", SqlDbType.VarChar, 50); CheckNullParameterStore(RankID, obj.RankId);
                var ReasonLeaveID = new SqlParameter("@ReasonLeaveID", SqlDbType.Int); CheckNullParameterStore(ReasonLeaveID, obj.ReasonLeaveId);
                var ManagerId = new SqlParameter("@ManagerId", SqlDbType.UniqueIdentifier); CheckNullParameterStore(ManagerId, obj.ManagerId);
                var Thumb = new SqlParameter("@Thumb", SqlDbType.VarBinary); CheckNullParameterStore(Thumb, obj.Thumb);

                var _DBName_COMMON = new SqlParameter("@DBName_COMMON", SqlDbType.NVarChar, 50); CheckNullParameterStore(_DBName_COMMON, _appSettings.DBName_HR);

                var _Error = new SqlParameter("@Error", SqlDbType.NVarChar); _Error.Value = "";
                var _lstDeleteDetail = new SqlParameter("@lstDeleteDetail", pddatadelete.Count >= 1 ? string.Join(",", pddatadelete) : "");
                var _detail = new SqlParameter("@Concurrently", SqlDbType.Structured);
                /// table detail
                var _tem = new DataTable();
                _tem.Columns.Add("ConcurrentlyGuid");
                _tem.Columns.Add("EmployeeGuid");
                _tem.Columns.Add("EmployeeID");
                _tem.Columns.Add("OrganizationGuid");
                _tem.Columns.Add("DepartmentGuid");
                _tem.Columns.Add("DepartmentID");
                _tem.Columns.Add("DepartmentName");
                _tem.Columns.Add("JobTitleID");
                _tem.Columns.Add("JobTitleName");
                _tem.Columns.Add("StartDate", typeof(DateTime));
                _tem.Columns.Add("EndDate", typeof(DateTime));
                _tem.Columns.Add("IsActive");
                _tem.Columns.Add("CreatedDate");
                _tem.Columns.Add("ModifiedDate");
                _tem.Columns.Add("CreatedBy");
                _tem.Columns.Add("ModifiedBy");
                foreach (var _item in detail)
                {
                    Guid idgui = new Guid("00000000-0000-0000-0000-000000000000");
                    if (_item.ConcurrentlyGuid == idgui)
                    {
                        _item.ConcurrentlyGuid = Guid.NewGuid();
                    }
                    if (_item.StartDate.HasValue) { _item.StartDate = _item.StartDate.Value.ToLocalTime(); } else { _item.StartDate = null; }
                    if (_item.EndDate.HasValue) { _item.EndDate = _item.EndDate.Value.ToLocalTime(); } else { _item.EndDate = null; }
                    _item.DepartmentId = _context.Departments.Where(x => x.DepartmentGuid == _item.DepartmentGuid).Select(x => x.DepartmentId).FirstOrDefault();
                    _item.JobTitleName = _context.JobTitles.Where(x => x.JobTitleId == _item.JobTitleId).Select(x => x.JobTitleName).FirstOrDefault();
                    _tem.Rows.Add(_item.ConcurrentlyGuid, obj.EmployeeGuid, obj.EmployeeId, obj.OrganizationGuid, _item.DepartmentGuid, _item.DepartmentId, _item.DepartmentName, _item.JobTitleId, _item.JobTitleName, _item.StartDate, _item.EndDate, _item.IsActive, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value);
                }

                _detail.Value = _tem;
                _detail.TypeName = "[hr].Concurrently";

                var __tem = _context.SQLCOMMANDS.FromSql(sql, EmployeeGuid, CandidateGuid, CandidateID, EmployeeID, OrganizationGuid, OrganizationID, DepartmentGuid, DepartmentID, Title, FullName, FirstName, MiddleName, LastName, Identification, IssueIDDate, IssueIDBy, LoginID, LoginName, JobTitleID, JobTitleName, ProfestionalD, ProfestionaName, SpecializesID, BirthDate, PlaceOfBirth, MaritalStatus, Gender, HireDate, SalaryPayBy, SalaryMethod, PermanentAddress, CurrentAddress, CountryID, CountryName, ProvinceID, ProvinceName, DistrictID, DistrictName, WardID, WardName, HomeNumber, EthnicID, EthnicName, HomePhone, HomeMobile, HomeEmail, WorkPhone, WorkMobile, WorkEmail, EducationID, EducationName, ImagePath, Photo, TaxCode, BankCode, BankName, BankAccount, StatusOfWork, EndDateWork, Note, StartDate, EndDate, ProbationPeriod, Uniform, IsConcurrently, TimeKeepingID, IsUseCar, CarID, IsActive, CreatedDate, CreatedBy, ModifiedBy, ModifiedDate, GenitiveID, SalaryTierID, RankID, ReasonLeaveID, ManagerId, Thumb, _DBName_COMMON, _detail, _lstDeleteDetail, _Error).FirstOrDefault();


                if (__tem.Error == "")
                {
                    if (ShiftOfEmployees.Count > 0)
                    {
                        var lstShip = _context.ShiftOfEmployees.Where(x => x.EmployeeGuid == obj.EmployeeGuid).ToList();
                        foreach (var i in lstShip)
                        {
                            _context.ShiftOfEmployees.Remove(i);
                            _context.SaveChanges();
                        }

                        foreach (var item in ShiftOfEmployees)
                        {
                            var sh = new ShiftOfEmployees();
                            sh.ShiftId = item.ShiftId;
                            sh.EmployeeGuid = obj.EmployeeGuid;
                            _context.ShiftOfEmployees.Add(sh);
                            _context.SaveChanges();
                        }
                    }
                    return new JMessage() { Error = false, Title = "Cập nhật thành công." };
                }
                else
                {
                    return new JMessage() { Error = true, Title = __tem.Error, Object = __tem };
                }
            }
            catch (Exception ex)
            {

                msg.Title = "Có lỗi khi cập nhật";

                msg.Object = ex;
                msg.Error = true;

            }

            return Json(msg);
        }
        [HttpPost]
        //[ActionAuthorize(ES_MODEL.Models.EAction.OPEN)]
        public JsonResult GetItem(Guid? id)
        {

            var msg = new JMessage() { Error = false };
            try
            {
                var _emp = GetEmployeeLogin(_context);
                string sql = "[Hr].[SP_ESHR_GetEmployees_By_EmployeeGuid] @OrganizationGuid, @EmployeeGuid";
                var _OrganizationGuid = new SqlParameter("@OrganizationGuid", SqlDbType.UniqueIdentifier); _OrganizationGuid.Value = _emp.OrganizationGuid;
                var _EmployeeGuid = new SqlParameter("@EmployeeGuid", SqlDbType.UniqueIdentifier); _EmployeeGuid.Value = id;
                var rs = _context.Employees.FromSql(sql, _OrganizationGuid, _EmployeeGuid).SingleOrDefault();
                return Json(rs);
            }
            catch (Exception ex)
            {
                msg.Title = "Có lỗi khi lấy dữ liệu.";
                return Json(new JMessage() { Error = true, Object = ex, Title = msg.Title });
            }
        }
        [HttpPost]
        //[ActionAuthorize(ES_MODEL.Models.EAction.OPEN)]
        public JsonResult GetItemOpen(Guid? id)
        {

            var msg = new JMessage() { Error = false };
            try
            {
                var _emp = GetEmployeeLogin(_context);
                string sql = "[Hr].[SP_ESHR_GetViewEmployees_By_EmployeeGuid] @OrganizationGuid, @EmployeeGuid";
                var _OrganizationGuid = new SqlParameter("@OrganizationGuid", SqlDbType.UniqueIdentifier); _OrganizationGuid.Value = _emp.OrganizationGuid;
                var _EmployeeGuid = new SqlParameter("@EmployeeGuid", SqlDbType.UniqueIdentifier); _EmployeeGuid.Value = id;
                var rs = _context.EmployeesView.FromSql(sql, _OrganizationGuid, _EmployeeGuid).SingleOrDefault();
                return Json(rs);
            }
            catch (Exception ex)
            {
                msg.Title = "Có lỗi khi lấy dữ liệu.";
                return Json(new JMessage() { Error = true, Object = ex, Title = msg.Title });
            }
        }
        [HttpPost]
        //[ActionAuthorize(ES_MODEL.Models.EAction.OPEN)]
        public JsonResult GetConcurrently(Guid? id)
        {

            var msg = new JMessage() { Error = false };
            try
            {
                var _emp = GetEmployeeLogin(_context);
                string sql = "[Hr].[SP_ESHR_GetConcurrently_By_EmployeeGuid] @OrganizationGuid, @EmployeeGuid";
                var _OrganizationGuid = new SqlParameter("@OrganizationGuid", SqlDbType.UniqueIdentifier); CheckNullParameterStore(_OrganizationGuid, _emp.OrganizationGuid);
                var _EmployeeGuid = new SqlParameter("@EmployeeGuid", SqlDbType.UniqueIdentifier); CheckNullParameterStore(_EmployeeGuid, id);
                var rs = _context.Concurrently.FromSql(sql, _OrganizationGuid, _EmployeeGuid).ToList();
                return Json(rs);
            }
            catch (Exception ex)
            {
                msg.Title = "Có lỗi khi lấy dữ liệu.";
                return Json(new JMessage() { Error = true, Object = ex, Title = msg.Title });
            }
        }
        [HttpPost]
        public object Approved(Guid? id)
        {
            try
            {
                var _emp = GetEmployeeLogin(_context);
                var departGui = _context.Departments.Where(x => x.DepartmentGuid == id).Select(x => x.DepartmentGuid).FirstOrDefault();
                var listchild2 = ListUpdate(departGui);

                return Json(new JMessage() { Error = false, Title = "ok.", Object = listchild2 });
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Title = "Có lỗi khi chuyển trạng thái.", Object = ex.Message });
            }
        }
        List<Departments> listde = new List<Departments>();
        private List<Departments> ListUpdate([FromBody] Guid? id)
        {
            var contents = _context.Departments.Where(x => x.ParentId == id).ToList();
            foreach (var item in contents)
            {
                var category = new Departments
                {
                    DepartmentGuid = item.DepartmentGuid,
                    DepartmentId = item.DepartmentId,
                    DepartmentName = item.DepartmentName,
                    ModifiedDate = item.ModifiedDate,
                    IsActive = item.IsActive,
                    ParentId = item.ParentId,
                };
                listde.Add(category);
                SubTreeUpdate(listde, category.DepartmentGuid);
            }
            return listde;
        }
        private List<Departments> SubTreeUpdate(List<Departments> data, Guid? id)
        {
            var contents = _context.Departments.Where(x => x.ParentId == id).ToList();
            foreach (var item in contents)
            {
                var category = new Departments
                {
                    DepartmentGuid = item.DepartmentGuid,
                    DepartmentId = item.DepartmentId,
                    DepartmentName = item.DepartmentName,
                    ModifiedDate = item.ModifiedDate,
                    IsActive = item.IsActive,
                    ParentId = item.ParentId,
                };
                listde.Add(category);
                var contents2 = _context.Departments.Where(x => x.ParentId == category.DepartmentGuid).ToList();
                if (contents2.Count > 0)
                {
                    foreach (var items in contents2)
                    {
                        SubTreeUpdate(contents2, items.DepartmentGuid);
                    }
                }
            }
            return listde;
        }

        [HttpPost]
        //[ActionAuthorize(EAction.DELETE)]
        public object IsDeleted_Employees(Guid? id)
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var lstGuid = new List<Guid?>();
                lstGuid.Add(id);
                var _emp = GetEmployeeLogin(_context);
                string sql = "[Hr].[SP_ESHR_Delete_Employees] @OrganizationGuid,@lstDeleteDetail";
                var _OrganizationGuid = new SqlParameter("@OrganizationGuid", SqlDbType.UniqueIdentifier); _OrganizationGuid.Value = _emp.OrganizationGuid;

                var _lstDeleteDetail = new SqlParameter("@lstDeleteDetail", lstGuid.Count >= 1 ? string.Join(",", lstGuid) : "");
                var __tem = _context.SQLCOMMANDS.FromSql(sql, _OrganizationGuid, _lstDeleteDetail).FirstOrDefault();

                if (__tem.Error == "")
                {
                    return new JMessage() { Error = false, Title = "Xóa thành công." };
                }
                else
                {
                    return new JMessage() { Error = true, Title = __tem.Error, Object = __tem };
                }

            }

            catch (Exception ex)
            {
                msg.Title = "Có lỗi khi xóa dữ liệu.";
                return Json(new JMessage() { Error = true, Object = ex, Title = msg.Title });
            }
        }
        [HttpPost]
        //[ActionAuthorize(EAction.DELETE)]
        public object deleteItems_Employees([FromBody] List<Guid?> lstGuid)
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var _emp = GetEmployeeLogin(_context);
                string sql = "[Hr].[SP_ESHR_Delete_Employees] @OrganizationGuid,@lstDeleteDetail";
                var _OrganizationGuid = new SqlParameter("@OrganizationGuid", SqlDbType.UniqueIdentifier); _OrganizationGuid.Value = _emp.OrganizationGuid;
                var _lstDeleteDetail = new SqlParameter("@lstDeleteDetail", lstGuid.Count >= 1 ? string.Join(",", lstGuid) : "");
                var __tem = _context.SQLCOMMANDS.FromSql(sql, _OrganizationGuid, _lstDeleteDetail).FirstOrDefault();

                if (__tem.Error == "")
                {
                    return new JMessage() { Error = false, Title = "Xóa thành công." };
                }
                else
                {
                    return new JMessage() { Error = true, Title = __tem.Error, Object = __tem };
                }
            }
            catch (Exception ex)
            {
                msg.Title = "Có lỗi khi xóa dữ liệu.";
                return Json(new JMessage() { Error = true, Object = ex, Title = msg.Title });
            }
        }

        // báo cáo tất cả nhân viên
        //[Area("Hr")]
        //public IActionResult IndexReport(string Keyword, string JobTitleId, List<string> lstDepartmentID, string StartDate, string EndDate, List<string> Status)
        //{
        //    if (Keyword == null)
        //    {
        //        Keyword = "";
        //    }
        //    if (JobTitleId == null)
        //    {
        //        JobTitleId = "";
        //    }
        //    var lstdepart = "";
        //    if (lstDepartmentID.Count > 0)
        //    {
        //        lstdepart = string.Join(",", lstDepartmentID);
        //    }

        //    if (StartDate == null)
        //    {
        //        StartDate = "";
        //    }
        //    if (EndDate == null)
        //    {
        //        EndDate = "";
        //    }
        //    var lststatus = "";
        //    if (Status.Count > 0)
        //    {
        //        lststatus = string.Join(",", Status);
        //    }

        //    HttpContext.Session.SetString("Keyword", Keyword);
        //    HttpContext.Session.SetString("JobTitleId", JobTitleId);
        //    HttpContext.Session.SetString("lstDepartmentID", lstdepart);
        //    HttpContext.Session.SetString("StartDate", StartDate);
        //    HttpContext.Session.SetString("EndDate", EndDate);
        //    HttpContext.Session.SetString("Status", lststatus);

        //    return View();
        //}
        //[Area("Hr")]
        //public IActionResult GetReport()
        //{
        //    var _emp = GetEmployeeLogin(_context);
        //    var Keyword = HttpContext.Session.GetString("Keyword");
        //    var JobTitleId = HttpContext.Session.GetString("JobTitleId");
        //    var lstDepartmentID = HttpContext.Session.GetString("lstDepartmentID");
        //    var StartDate = HttpContext.Session.GetString("StartDate");
        //    var EndDate = HttpContext.Session.GetString("EndDate");
        //    var Status = HttpContext.Session.GetString("Status");

        //    var report = new StiReport();
        //    //Truyền biến vào báo cáo
        //    //Begin truyền biến

        //    report.Load(StiNetCoreHelper.MapPath(this, "wwwroot//ExportTemplate//Report_GetAllEmployee.mrt"));

        //    var StaDate = ""; var endDate = "";
        //    //if (StartDate != null && StartDate != "")
        //    //{
        //    //    StaDate = Convert.ToDateTime(StartDate);
        //    //}
        //    //if (EndDate != null && EndDate != "")
        //    //{
        //    //    endDate = Convert.ToDateTime(EndDate);

        //    //}
        //    Stimulsoft.Report.Dictionary.StiVariable _Keyword = new Stimulsoft.Report.Dictionary.StiVariable("Keyword", typeof(string));
        //    _Keyword.Value = Keyword;
        //    report.Dictionary.Variables["Keyword"] = _Keyword;

        //    Stimulsoft.Report.Dictionary.StiVariable _OrganizationGuid = new Stimulsoft.Report.Dictionary.StiVariable("OrganizationGuid", typeof(Guid));
        //    _OrganizationGuid.Value = _emp.OrganizationGuid.ToString();
        //    report.Dictionary.Variables["OrganizationGuid"] = _OrganizationGuid;

        //    Stimulsoft.Report.Dictionary.StiVariable _JobTitleId = new Stimulsoft.Report.Dictionary.StiVariable("JobTitleId", typeof(int));
        //    _JobTitleId.Value = JobTitleId;
        //    report.Dictionary.Variables["JobTitleId"] = _JobTitleId;


        //    Stimulsoft.Report.Dictionary.StiVariable _lstDepartmentID = new Stimulsoft.Report.Dictionary.StiVariable("lstDepartmentID", typeof(string));
        //    _lstDepartmentID.Value = lstDepartmentID;
        //    report.Dictionary.Variables["lstDepartmentID"] = _lstDepartmentID;

        //    //Stimulsoft.Report.Dictionary.StiVariable _StartDate = new Stimulsoft.Report.Dictionary.StiVariable("StartDate", typeof(string));
        //    //_StartDate.Value = StaDate;
        //    //report.Dictionary.Variables["StartDate"] = _StartDate;

        //    //Stimulsoft.Report.Dictionary.StiVariable _EndDate = new Stimulsoft.Report.Dictionary.StiVariable("EndDate", typeof(string));
        //    //_EndDate.Value = endDate;
        //    //report.Dictionary.Variables["EndDate"] = _EndDate;

        //    Stimulsoft.Report.Dictionary.StiVariable _Status = new Stimulsoft.Report.Dictionary.StiVariable("Status", typeof(string));
        //    _Status.Value = Status;
        //    report.Dictionary.Variables["Status"] = _Status;
        //    report.Render();
        //    //End truyền biến
        //    HttpContext.Session.Remove("Keyword");
        //    HttpContext.Session.Remove("JobTitleId");
        //    HttpContext.Session.Remove("lstDepartmentID");
        //    //HttpContext.Session.Remove("StartDate");
        //    //HttpContext.Session.Remove("EndDate");
        //    HttpContext.Session.Remove("Status");

        //    return StiNetCoreViewer.GetReportResult(this, report);
        //}

        //[Area("Hr")]
        //public IActionResult ViewerEvent()
        //{
        //    return StiNetCoreViewer.ViewerEventResult(this);
        //}

        [HttpPost]
        //[ActionAuthorize(EAction.LISTVIEW)]
        public object ExportDSNhanvien([FromBody] Data jTable)
        {
            try
            {
                // Template File
                var textsheet = "Danhsachnhanvien";
                var pathFileOld = _environment.WebRootPath + "/DATA/Export/" + textsheet + ".xlsx";
                if (System.IO.File.Exists(pathFileOld))
                {
                    System.IO.File.Delete(pathFileOld);
                }
                string templateDocument = _environment.WebRootPath + "/DATA/TemplateExcelExport/DSNhanvien.xlsx";
                // Results Output
                MemoryStream output = new MemoryStream();
                // Read Template
                using (FileStream templateDocumentStream = System.IO.File.OpenRead(templateDocument))
                {
                    // Create Excel EPPlus Package based on template stream
                    using (ExcelPackage package = new ExcelPackage(templateDocumentStream))
                    {
                        // Grab the sheet with the template, sheet name is "BOL".
                        ExcelWorksheet sheet = package.Workbook.Worksheets["Employees"];

                        var _emp = GetEmployeeLogin(_context);
                        if (jTable.StartDate != null)
                        {
                            jTable.StartDate = (DateTime)jTable.StartDate?.Date.AddDays(0) + new TimeSpan(23, 59, 59);
                        }
                        if (jTable.EndDate != null)
                        {
                            jTable.EndDate = (DateTime)jTable.EndDate?.Date.AddDays(0) + new TimeSpan(23, 59, 59);
                        }
                        var lstStringDepartmentId = new List<string>();
                        if (jTable.lstDepartmentID.Count >= 1)
                        {
                            lstStringDepartmentId = (from p in _context.Departments where jTable.lstDepartmentID.Contains(p.DepartmentGuid.ToString()) select p.DepartmentId).Distinct().ToList();
                        }
                        int intBeginFor = (jTable.CurrentPage - 1) * jTable.Length;
                        string sql = "[Hr].[SP_ESHR_ExportEmployees_viewJTable] " +
                           "@Keyword, " +
                          "@OrganizationGuid, " +
                           "@JobTitleId, " +
                           "@lstDepartmentID, " +
                           "@StartDate, " +
                           "@EndDate, " +
                           "@Status, " +
                           "@Count out";
                        var _Keyword = new SqlParameter("@Keyword", SqlDbType.NVarChar, 255); CheckNullParameterStore(_Keyword, jTable.Keyword);
                        var _OrganizationGuid = new SqlParameter("@OrganizationGuid", SqlDbType.UniqueIdentifier); CheckNullParameterStore(_OrganizationGuid, _emp.OrganizationGuid);
                        var _JobTitleId = new SqlParameter("@JobTitleId", SqlDbType.Int); CheckNullParameterStore(_JobTitleId, jTable.JobTitleId);
                        var _lstDepartmentID = new SqlParameter("@lstDepartmentID", jTable.lstDepartmentID.Count >= 1 ? string.Join(",", lstStringDepartmentId) : "");
                        var _StartDate = new SqlParameter("@StartDate", SqlDbType.DateTime); CheckNullParameterStore(_StartDate, jTable.StartDate);
                        var _EndDate = new SqlParameter("@EndDate", SqlDbType.DateTime); CheckNullParameterStore(_EndDate, jTable.EndDate);
                        var _Status = new SqlParameter("@Status", jTable.Status.Count >= 1 ? string.Join(",", jTable.Status) : "");
                        var _Count = new SqlParameter("@Count", SqlDbType.Int) { Direction = System.Data.ParameterDirection.Output };
                        var data = _context.EmployeesExport.FromSql(sql, _Keyword, _OrganizationGuid, _JobTitleId, _lstDepartmentID, _StartDate, _EndDate, _Status, _Count).ToList();

                        int rowIndex = 8;
                        int stt = 0;

                        foreach (var item in data)
                        {
                            stt = stt + 1;

                            sheet.Cells[rowIndex, 1].Value = stt;

                            using (var range = sheet.Cells[rowIndex, 1, rowIndex, 37])
                            {
                                range.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                range.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                range.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                range.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                range.Style.Font.Size = 13;
                                range.Style.Font.Bold = false;
                            }
                            sheet.Cells[rowIndex, 1].Value = stt;
                            sheet.Cells[rowIndex, 2].Value = item.EmployeeID;
                            sheet.Cells[rowIndex, 3].Value = item.FullName;
                            sheet.Cells[rowIndex, 4].Value = item.BirthDateString;
                            sheet.Cells[rowIndex, 5].Value = item.Phone;
                            sheet.Cells[rowIndex, 6].Value = item.DepartmentName;
                            sheet.Cells[rowIndex, 7].Value = item.JobTitlesName;
                            sheet.Cells[rowIndex, 8].Value = item.GenderString;
                            sheet.Cells[rowIndex, 9].Value = item.StartDateString;
                            sheet.Cells[rowIndex, 10].Value = item.StatusOfWorkString;
                            sheet.Cells[rowIndex, 11].Value = "";
                            sheet.Cells[rowIndex, 12].Value = item.ProfestionaName;
                            sheet.Cells[rowIndex, 13].Value = "";
                            sheet.Cells[rowIndex, 14].Value = item.Identification;
                            sheet.Cells[rowIndex, 15].Value = item.TaxCode;
                            sheet.Cells[rowIndex, 16].Value = item.IssueIddateString;
                            sheet.Cells[rowIndex, 17].Value = item.IssueIdby;
                            sheet.Cells[rowIndex, 18].Value = item.CurrentAddress;
                            sheet.Cells[rowIndex, 19].Value = item.WorkEmail;
                            sheet.Cells[rowIndex, 20].Value = item.BankAccount;
                            sheet.Cells[rowIndex, 21].Value = item.BankName;
                            sheet.Cells[rowIndex, 22].Value = item.GenitiveId;
                            sheet.Cells[rowIndex, 23].Value = item.ContractTypeString;
                            sheet.Cells[rowIndex, 24].Value = item.ReasonName;
                            rowIndex = rowIndex + 1;
                        }
                        package.SaveAs(output);
                        var nameExcel = Utilities.ExportExcel(package, _environment.WebRootPath, textsheet);
                        return Json(new JMessage() { Error = false, Title = nameExcel });
                    }
                }
            }
            catch (Exception ex)
            {
               
                return Json(new JMessage() { Error = true, Object = ex, Title = "In không thành công." });
            }
        }

        public class Printobj
        {
            public string EmployeeID { get; set; }
            public Guid? ContractGuid { get; set; }
        }
      

        #endregion
    }
}
