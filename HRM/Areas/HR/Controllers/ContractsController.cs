﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FTU.Utils.HelperNet;
using HRM.Controllers;
using HRM.Models;
using HRM.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace HRM.Areas.HR.Controllers
{
    [Authorize]
    public class ContractsController : BaseController
    {
        public IConfigurationRoot Configuration { get; }
        private readonly HRMDBContext _context;
        private readonly IHostingEnvironment _environment;
        public ContractsController(HRMDBContext context, IHostingEnvironment environment)
        {
            _context = context;
            _environment = environment;
        }
       
        [Area("Hr")]
        public IActionResult Index()
        {
            return View();
        }
        [HttpPost]
        //[ActionAuthorize(EAction.LISTVIEW)]
        public object JTable([FromBody] JTableSearch jTablePara)
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var count = _context.Contracts.Where(x => x.Title.Contains(jTablePara.search.value) || x.ContractNo.Contains(jTablePara.search.value))
                .Where(x => jTablePara.IsActive != null ? x.IsActive == jTablePara.IsActive : true)
                .Count();
                int intBeginFor = (jTablePara.CurrentPage - 1) * jTablePara.Length;
                var data = _context.Contracts.Where(x => x.Title.Contains(jTablePara.search.value) || x.ContractNo.Contains(jTablePara.search.value))
                    .Where(x => jTablePara.IsActive != null ? x.IsActive == jTablePara.IsActive : true)
                    .Select(x => new { x.Id, x.Title, x.ContractNo, x.EmployeeId, x.ContractDate, x.ContractDateString, x.Salary, x.IsActive })
                    .OrderUsingSortExpression(jTablePara.QueryOrderBy).Skip(intBeginFor).Take(jTablePara.Length).ToList();
                var jdata = JTableHelper.JObjectTable(data, jTablePara.Draw, count, "Id", "Title", "EmployeeId", "ContractNumber", "ContractDate", "ContractDateString", "Salary", "IsActive");
                return Json(jdata);
            }
            catch (Exception ex)
            {
                return new JMessage() { Error = true, Title = "Có lỗi khi lấy dữ liệu", Object = ex.Message };
            }
        }

        [HttpPost]
        public JsonResult Insert([FromBody] Contracts obj)
        {
            var msg = new JMessage() { Error = false };
            _context.Database.BeginTransaction();
            try
            {
                var _emp = GetEmployeeLogin(_context);
                if (obj.ContractDate.HasValue) { obj.ContractDate = obj.ContractDate.Value.ToLocalTime(); } else { obj.ContractDate = null; }

                obj.CreatedDate = DateTime.Now;
                obj.ModifiedDate = DateTime.Now;
                obj.CreatedBy = GetCreatedBy(_context);
                obj.ModifiedBy = GetModifiedBy(_context);
                _context.Contracts.Add(obj);
                var a = _context.SaveChanges();
                _context.Database.CommitTransaction();
                msg.Title = "Thêm thành công";
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                msg.Title = "Có lỗi khi thêm";
                msg.Object = ex;
                msg.Error = true;
            }
            return Json(msg);
        }
        [HttpPost]
        public object Update([FromBody] Contracts data)
        {
            _context.Database.BeginTransaction();
            var _emp = GetEmployeeLogin(_context);
            try
            {
                if (data.ContractDate.HasValue) { data.ContractDate = data.ContractDate.Value.ToLocalTime(); } else { data.ContractDate = null; }

                data.ModifiedBy = GetModifiedBy(_context);
                data.ModifiedDate = DateTime.Now;
                _context.Contracts.Update(data);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return new JMessage() { Error = false, Title = "Cập nhật thành công" };
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return new JMessage() { Error = true, Title = "Cập nhật không thành công", Object = ex.Message };
            }
        }
        [HttpPost]
        public object Delete([FromBody] TempSub obj)
        {
            try
            {
                _context.Database.BeginTransaction();
                var data = _context.Contracts.Where(x => x.Id == obj.IdI[0]).FirstOrDefault();
                _context.Contracts.Remove(data);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Json(new { Error = false, Title = "Xóa thành công" });
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return Json(new { Error = true, Object = ex, Title = "Có lỗi khi xóa." });
            }
        }
        [HttpPost]
        public object DeleteItems([FromBody] List<int?> lstId)
        {
            _context.Database.BeginTransaction();
            try
            {
                foreach (var item in lstId)
                {
                    var Id = item;
                    var data = _context.Contracts.Where(x => x.Id == Id).FirstOrDefault();
                    _context.Contracts.Remove(data);
                    _context.SaveChanges();
                }
                _context.Database.CommitTransaction();
                return Json(new { Error = false, Title = "Xóa thành công" });
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return Json(new { Error = true, Object = ex, Title = "Có lỗi khi xóa." });
            }
        }
        [HttpPost]
        public object GetItem([FromBody] TempSub obj)
        {
            try
            {
                var data = _context.Contracts.Where(x => x.Id == obj.IdI[0]).FirstOrDefault();
                return Json(data);
            }
            catch (Exception ex)
            {
                return Json(new { Error = true, Object = ex, Title = "Lấy dữ liệu không thành công." });
            }
        }
       
        
    }
}
