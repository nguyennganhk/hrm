﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace HRM.Areas.EIM.Controllers
{
    public class DesignWorkflowsController : Controller
    {
        [Area("EIM")]
        public IActionResult Index()
        {
            return View();
        }
    }
}
