﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Aspose.Pdf;
using DocumentFormat.OpenXml.VariantTypes;
using FTU.Utils.HelperNet;
using HRM.Controllers;
using HRM.Models;
using HRM.Models.Custom.Hr;
using HRM.Utils;
using Microsoft.ApplicationInsights.AspNetCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using Attribute = HRM.Models.Attribute;

namespace HRM.Areas.EIM.Controllers
{
    public class ResourceController : BaseController
    {
        private readonly HRMDBContext _context;
        private readonly AppSettings _appSettings;
        private readonly IHostingEnvironment _environment;
        public ResourceController(IOptions<AppSettings> appSettings, HRMDBContext context, IHostingEnvironment environment)
        {
            _context = context;
            _appSettings = appSettings.Value;
            _environment = environment;
            //  Stimulsoft.Base.StiLicense.LoadFromFile("License.key");
        }
        [Area("Eim")]
        public IActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public object GetResource()
        {
            var rs = _context.Resource.OrderBy(x=>x.Ord).ToList();
            return Json(rs);
        }
        [HttpGet]
        public object GetParrent()
        {
            var rs = _context.Resource.Where(x=>x.ParentId ==null).OrderBy(x => x.Ord).Select(x=> new {id=x.Id,text=x.Title}).ToList();
            return Json(rs);
        }
        
        public class objData
        {
            public List<Privilege> Privilege { get; set; }
            public List<HRM.Models.Attribute> Attribute { get; set; }
            public List<string> ESUserPrivileges { get; set; }
            public List<int?> lstId { get; set; }
        }
        [HttpPost]
        public object GetPrivilege([FromBody] List<int?> lstId)
        {
            var item = new objData(); 
            var ResourceId = 0;
            if (lstId.Count >= 2)
            {
                ResourceId = _context.Resource.Where(x => lstId.Contains(x.Id) && x.ParentId != null).Select(x => x.Id).FirstOrDefault();
            }
            else
            {
                ResourceId = _context.Resource.Where(x => lstId.Contains(x.Id)).Select(x => x.Id).FirstOrDefault();
            }
            if (ResourceId != 0)
            {
                var rs = _context.Privilege.Where(x => x.ResourceId == ResourceId).ToList();
                var rs1 = _context.Attribute.Where(x => x.ResourceId == ResourceId).ToList();
                var lstPrivileges = rs.Select(x => x.Id).ToList();
                var rs2 = _context.ESUserPrivileges.Where(x => lstPrivileges.Contains(x.PrivilegeId)).Select(x => x.UserId).Distinct().ToList();

                item.Privilege = rs;
                item.Attribute = rs1;
                item.ESUserPrivileges = rs2;
                return item;
            }
            else
            {
                return null;
            }
        }

        [HttpGet]
        public object GetEmployees()
        {
            var rs = _context.Employees.Select(x => new { x.EmployeeGuid, x.EmployeeId, x.LoginName, x.FullName }).ToList();
            return Json(rs);
        }
        [HttpGet]
        public object GetESActions()
        {
            var rs = _context.ESActions.ToList();
            return Json(rs);
        }
        [HttpPost]
        public object deleteEmployees([FromBody] TempSub temp)
        {
            try
            {
                //  _context.Database.BeginTransaction();
                var ResourceId = _context.Resource.Where(x => temp.IdI.Contains(x.Id)).Select(x => x.Id).FirstOrDefault();
                if (ResourceId != 0)
                {
                    var lstPrivilege = _context.ESUserPrivileges.Where(x => x.UserId == temp.IdS[0]).Select(x => x.PrivilegeId).ToList();
                    var lstPrivilegeId = _context.Privilege.Where(x => lstPrivilege.Contains(x.Id) && x.ResourceId == ResourceId).ToList();

                    if (lstPrivilegeId.Count() > 0)
                    {
                        foreach (var item in lstPrivilegeId)
                        {
                            var notPrivileges = _context.ESUserPrivileges.Where(x => x.PrivilegeId == item.Id && x.UserId != temp.IdS[0]).FirstOrDefault();
                            if (notPrivileges == null)
                            {
                                _context.Privilege.Remove(item);
                                _context.SaveChanges();
                            }
                            var UserPrivileges = _context.ESUserPrivileges.Where(x => x.PrivilegeId == item.Id && x.UserId == temp.IdS[0]).FirstOrDefault();
                            if (UserPrivileges != null)
                            {
                                _context.ESUserPrivileges.Attach(UserPrivileges);
                                _context.ESUserPrivileges.Remove(UserPrivileges);
                                _context.SaveChanges();
                            }
                        }
                    }
                }
                //  _context.Database.CommitTransaction();
                return Json(new { Error = false, Title = "Xóa thành công" });
            }
            catch (Exception ex)
            {
                //  _context.Database.RollbackTransaction();
                return Json(new { Error = true, Object = ex, Title = "Có lỗi khi xóa." });
            }
        }
        [HttpPost]
        public object getQuyen([FromBody] TempSub temp)
        {

            //  _context.Database.BeginTransaction();
            var ResourceId = _context.Resource.Where(x => temp.IdI.Contains(x.Id)).Select(x => x.Id).FirstOrDefault();
            if (ResourceId != 0)
            {
                var lstPrivilege = _context.ESUserPrivileges.Where(x => x.UserId == temp.IdS[0]).Select(x => x.PrivilegeId).ToList();
                var lstPrivilegeId = _context.Privilege.Where(x => lstPrivilege.Contains(x.Id) && x.ResourceId == ResourceId).ToList();

                return lstPrivilegeId;
            }
            else
            {
                return null;
            }
        }

        [HttpPost]
        public object deleteActions([FromBody] TempSub temp)
        {
            try
            {
                _context.Database.BeginTransaction();

                var Id = temp.IdI[0];
                var User = temp.IdS[0];
                var data = _context.Privilege.Where(x => x.Id == Id).FirstOrDefault();
                if(User !="")
                {
                    var lstPrivilege = _context.ESUserPrivileges.Where(x => x.UserId == temp.IdS[0] && x.PrivilegeId == Id).FirstOrDefault();
                    _context.ESUserPrivileges.Remove(lstPrivilege);
                    _context.SaveChanges();
                }
               
                _context.Database.CommitTransaction();
                var Count = _context.ESUserPrivileges.Where(x => x.PrivilegeId == Id).Count();
                if (Count == 0 || User =="")
                {
                    _context.Privilege.Remove(data);
                    _context.SaveChanges();
                }
                return Json(new { Error = false, Title = "Xóa thành công" });
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return Json(new { Error = true, Object = ex, Title = "Có lỗi khi xóa." });
            }
        }
        [HttpPost]
        public JsonResult Insert([FromBody] Resource obj)
        {
            var msg = new JMessage() { Error = false };
            _context.Database.BeginTransaction();
            try
            {
                _context.Resource.Add(obj);
                _context.SaveChanges();
                var attribuid1 = new Attribute() { Id = 0, Key = "URL", Value = "", ResourceId = obj.Id };
                var attribuid2 = new Attribute() { Id = 0, Key = "ICON", Value = "", ResourceId = obj.Id };
                _context.Attribute.Add(attribuid1);
                _context.Attribute.Add(attribuid2);
                _context.SaveChanges();

                _context.SaveChanges();


                _context.Database.CommitTransaction();
                msg.Title = "Thêm thành công";
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                msg.Title = "Có lỗi khi thêm";
                msg.Object = ex;
                msg.Error = true;
            }
            return Json(msg);
        }
        [HttpPost]
        public object Update([FromBody] Resource data)
        {
            _context.Database.BeginTransaction();
            try
            {
                _context.Resource.Update(data);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return new JMessage() { Error = false, Title = "Cập nhật thành công" };
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return new JMessage() { Error = true, Title = "Cập nhật không thành công", Object = ex.Message };
            }
        }
        [HttpPost]
        public object GetItem([FromBody] List<int?> lstId)
        {
            try
            {
                var ResourceId = 0;
                if (lstId.Count >= 2)
                {
                    ResourceId = _context.Resource.Where(x => lstId.Contains(x.Id) && x.ParentId != null).Select(x => x.Id).FirstOrDefault();
                }
                else
                {
                    ResourceId = _context.Resource.Where(x => lstId.Contains(x.Id)).Select(x => x.Id).FirstOrDefault();
                }
                var data = _context.Resource.Where(x => x.Id == ResourceId).FirstOrDefault();
                return Json(data);
            }
            catch (Exception ex)
            {
                return Json(new { Error = true, Object = ex, Title = "Lấy dữ liệu không thành công." });
            }
        }
        [HttpPost]
        public object Delete([FromBody] TempSub obj)
        {
            try
            {
                _context.Database.BeginTransaction();
                var data = _context.Resource.Where(x => x.Id == obj.IdI[0]).FirstOrDefault();
                var Privilege = _context.Privilege.Where(x => x.ResourceId == data.Id).ToList();
                var Attribute = _context.Attribute.Where(x => x.ResourceId == data.Id).ToList();

                var lstPrivilege = Privilege.Select(x => x.Id).ToList();
                var ESUserPrivileges = _context.ESUserPrivileges.Where(x => lstPrivilege.Contains(x.PrivilegeId)).ToList();

                foreach (var item in ESUserPrivileges)
                {
                    _context.ESUserPrivileges.Remove(item);
                    _context.SaveChanges();
                }
                foreach (var item in Attribute)
                {
                    _context.Attribute.Remove(item);
                    _context.SaveChanges();
                }
                foreach (var item in Privilege)
                {
                    _context.Privilege.Remove(item);
                    _context.SaveChanges();
                }
                _context.Resource.Remove(data);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return Json(new { Error = false, Title = "Xóa thành công" });
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return Json(new { Error = true, Object = ex, Title = "Có lỗi khi xóa." });
            }
        }
        [HttpPost]
        public object InsertUserId()
        {
            try
            {
                var tempRecrui = Request.Form["Insert"];
                objData item = JsonConvert.DeserializeObject<objData>(tempRecrui);
                var lstResourceId = _context.Resource.Where(x => item.lstId.Contains(x.Id)).Select(x => x.Id).ToList();
                foreach (var value in lstResourceId)
                {
                    var ResourceId = value;
                    if (item.Privilege.Count > 0)
                    {
                        foreach (var i in item.Privilege)
                        {
                            var rs = _context.Privilege.Where(x => x.ResourceId == ResourceId && x.ActionId == i.ActionId).FirstOrDefault();
                            if (rs == null)
                            {
                                var ob = new Privilege() { ActionId = i.ActionId, ResourceId = ResourceId };
                                _context.Privilege.Add(ob);
                                _context.SaveChanges();
                                foreach (var j in item.ESUserPrivileges)
                                {
                                    var ob1 = new ESUserPrivileges() { Id = 0, UserId = j, PrivilegeId = ob.Id };
                                    _context.ESUserPrivileges.Add(ob1);
                                    _context.SaveChanges();
                                }
                            }
                            else
                            {
                                foreach (var j in item.ESUserPrivileges)
                                {
                                    var check = _context.ESUserPrivileges.Where(x => x.UserId == j && x.PrivilegeId == rs.Id).Count();
                                    if (check == 0)
                                    {
                                        var ob1 = new ESUserPrivileges() { Id = 0, UserId = j, PrivilegeId = rs.Id };
                                        _context.ESUserPrivileges.Add(ob1);
                                        _context.SaveChanges();
                                    }

                                }

                            }
                        }
                    }
                }
                return Json(new { Error = false, Title = "Cập nhật thành công" });
            }
            catch (Exception ex)
            {
                return Json(new { Error = true, Object = ex, Title = "Có lỗi khi cập nhật." });
            }
        }
        [HttpPost]
        public object UpdateAttribute([FromBody] Attribute data)
        {
            _context.Database.BeginTransaction();
            var _emp = GetEmployeeLogin(_context);
            try
            {
                var obj = _context.Attribute.Where(x => x.Id == data.Id).FirstOrDefault();
                obj.Value = data.Value;
                _context.Attribute.Update(obj);
                _context.SaveChanges();
                _context.Database.CommitTransaction();
                return new JMessage() { Error = false, Title = "Cập nhật thành công" };
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return new JMessage() { Error = true, Title = "Cập nhật không thành công", Object = ex.Message };
            }
        }
    }
}
