﻿using FTU.Utils.HelperNet;
using HRM.Controllers;
using HRM.Models;
using HRM.Utils;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace HRM.Areas.EIM.Controllers
{
    public class WorkFlowSettingController : BaseController
    {
        private readonly HRMDBContext _context;
        private readonly AppSettings _appSettings;
        public WorkFlowSettingController(IOptions<AppSettings> appSettings, HRMDBContext context)
        {
            _context = context;
            _appSettings = appSettings.Value;
        }
        [Area("EIM")]
        public IActionResult Index()
        {
            return View();
        }
        public class Data : JTableModel
        {
            public bool? IsActive { get; set; }
        }
        [HttpPost]
        public object JTable([FromBody] Data jTablePara)
        {
            var s = GetEmployeeLogin(_context);
            try
            {
                int intBeginFor = (jTablePara.CurrentPage - 1) * jTablePara.Length;
                string sql = "[HR].[SP_ESHR_WorkFlowSetting_ListAll]" +
                "@Keyword, " +
                "@OrganizationGuid, " +
                "@Sort, " +
                "@Skip, " +
                "@Take," +
                "@Count out ";
                var _Keyword = new SqlParameter("@Keyword", SqlDbType.NVarChar);
                CheckNullParameterStore(_Keyword, jTablePara.search.value);
                var _OrganizationGuid = new SqlParameter("@OrganizationGuid", System.Data.SqlDbType.UniqueIdentifier);
                CheckNullParameterStore(_OrganizationGuid, s.OrganizationGuid);
                var _Sort = new SqlParameter("@Sort", SqlDbType.VarChar);
                CheckNullParameterStore(_Sort, jTablePara.QueryOrderBy);
                var _Skip = new SqlParameter("@Skip", SqlDbType.Int);
                CheckNullParameterStore(_Skip, intBeginFor);
                var _Take = new SqlParameter("@Take", SqlDbType.Int);
                CheckNullParameterStore(_Take, jTablePara.Length);
                var _output = new SqlParameter("@Count", SqlDbType.Int) { Direction = System.Data.ParameterDirection.Output };
                var data = _context.WorkFlowSettingJTableView.FromSql(sql, _Keyword, _OrganizationGuid, _Sort, _Skip, _Take, _output).ToList();
                var jdata = JTableHelper.JObjectTable(data, jTablePara.Draw, (int)_output.Value);
                return Json(jdata);
            }
            catch (Exception ex)
            {
                return Json(ex);
            }
        }
        [HttpPost]
        public object GetWorkFlows()
        {
            var _emp = GetEmployeeLogin(_context);
            try
            {
                var rs = _context.Workflows.Where(x => x.IsActive == true && x.OrganizationGuid == _emp.OrganizationGuid)
                    .Select(x => new
                    {
                        value = x.WorkflowGuid,
                        text = x.WorkflowName
                    }).ToList();
                return Json(rs);
            }
            catch (Exception)
            {

                throw;
            }
        }
        [HttpPost]
        public Object Insert()
        {
            try
            {
                var s = GetEmployeeLogin(_context);
                var _obj = Request.Form["insert"];
                WorkFlowSetting obj = JsonConvert.DeserializeObject<WorkFlowSetting>(_obj);
                var countID = _context.WorkFlowSetting.Where(x => x.TicketId == obj.TicketId).Count();
                if (countID > 0)
                {
                    return new { Title = "Mã phiếu đã tồn tại", Error = true };
                }
                string sql = "[HR].[SP_ESHR_WorkFlowSetting_Insert]" +
                    "@OrganizationGuid," +
                    "@WorkFlowGuid," +
                    "@WorkFlowName," +
                    "@TicketID," +
                    "@TicketName," +
                    "@CreateDate," +
                    "@CreateBy," +
                    "@ModifiedBy," +
                    "@ModifiedDate";
                var _OrganizationGuid = new SqlParameter("@OrganizationGuid", System.Data.SqlDbType.UniqueIdentifier);
                CheckNullParameterStore(_OrganizationGuid, s.OrganizationGuid);
                var _WorkFlowGuid = new SqlParameter("@WorkFlowGuid", System.Data.SqlDbType.UniqueIdentifier);
                CheckNullParameterStore(_WorkFlowGuid, obj.WorkFlowGuid);
                var _WorkFlowName = new SqlParameter("@WorkFlowName", System.Data.SqlDbType.NVarChar);
                CheckNullParameterStore(_WorkFlowName, obj.WorkFlowName);
                var _TicketID = new SqlParameter("@TicketID", System.Data.SqlDbType.VarChar);
                CheckNullParameterStore(_TicketID, obj.TicketId);
                var _TicketName = new SqlParameter("@TicketName", System.Data.SqlDbType.NVarChar);
                CheckNullParameterStore(_TicketName, obj.TicketName);
                var _CreateDate = new SqlParameter("@CreateDate", System.Data.SqlDbType.DateTime);
                CheckNullParameterStore(_CreateDate, DateTime.Now);
                var _CreateBy = new SqlParameter("@CreateBy", System.Data.SqlDbType.NVarChar);
                CheckNullParameterStore(_CreateBy, GetCreatedBy(_context));
                var _ModifiedBy = new SqlParameter("@ModifiedBy", System.Data.SqlDbType.NVarChar);
                CheckNullParameterStore(_ModifiedBy, GetModifiedBy(_context));
                var _ModifiedDate = new SqlParameter("@ModifiedDate", System.Data.SqlDbType.DateTime);
                CheckNullParameterStore(_ModifiedDate, DateTime.Now);
                var _rs = _context.SQLCOMMANDS.FromSql(sql, _OrganizationGuid, _WorkFlowGuid, _WorkFlowName, _TicketID, _TicketName, _CreateDate, _CreateBy, _ModifiedBy, _ModifiedDate).FirstOrDefault();
                if (_rs.Error == "")
                {
                    return new { Title = "Thêm thành công", Error = false };
                }
                else
                {
                    return new { Title = "Thêm không thành công", Error = true, Object = _rs.Error };
                }
            }
            catch (Exception ex)
            {

                return new { Title = "Thêm không thành công", Error = true, Object = ex };
            }
        }
        [HttpPost]
        public JsonResult GetItem(Guid? id)
        {

            var msg = new JMessage() { Error = false };
            try
            {
                var _emp = GetEmployeeLogin(_context);
                string sql = "[HR].[SP_ESHR_WorkFlowSetting_GetItem] @RowGuid";
                var _RowGuid = new SqlParameter("@RowGuid", SqlDbType.UniqueIdentifier);
                CheckNullParameterStore(_RowGuid, id);
                var rs = _context.WorkFlowSetting.FromSql(sql, _RowGuid).SingleOrDefault();
                return Json(rs);
            }
            catch (Exception ex)
            {
                msg.Title = "Có lỗi khi lấy dữ liệu.";
                return Json(new JMessage() { Error = true, Object = ex, Title = msg.Title });
            }
        }
        [HttpPost]
        public object Update()
        {
            try
            {
                var s = GetEmployeeLogin(_context);
                var _obj = Request.Form["update"];
               WorkFlowSetting obj = JsonConvert.DeserializeObject<WorkFlowSetting>(_obj);
                var TicketIdOld = _context.WorkFlowSetting.Where(x => x.RowGuid == obj.RowGuid).Select(x => x.TicketId).FirstOrDefault();
                if (TicketIdOld != obj.TicketId)
                {
                    var countID = _context.WorkFlowSetting.Where(x => x.TicketId == obj.TicketId && x.RowGuid != obj.RowGuid).Count();
                    if (countID > 0)
                    {
                        return new { Title = "Mã phiếu đã tồn tại", Error = true };
                    }
                }
                string sql = "[HR].[SP_ESHR_WorkFlowSetting_Update]" +
                    "@RowGuid," +
                    "@WorkFlowGuid," +
                    "@WorkFlowName," +
                    "@TicketID," +
                    "@TicketName," +
                    "@ModifiedBy," +
                    "@ModifiedDate";
                var _RowGuid = new SqlParameter("@RowGuid", System.Data.SqlDbType.UniqueIdentifier);
                CheckNullParameterStore(_RowGuid, obj.RowGuid);
                var _WorkFlowGuid = new SqlParameter("@WorkFlowGuid", System.Data.SqlDbType.UniqueIdentifier);
                CheckNullParameterStore(_WorkFlowGuid, obj.WorkFlowGuid);
                var _WorkFlowName = new SqlParameter("@WorkFlowName", System.Data.SqlDbType.NVarChar);
                CheckNullParameterStore(_WorkFlowName, obj.WorkFlowName);
                var _TicketID = new SqlParameter("@TicketID", System.Data.SqlDbType.VarChar);
                CheckNullParameterStore(_TicketID, obj.TicketId);
                var _TicketName = new SqlParameter("@TicketName", System.Data.SqlDbType.NVarChar);
                CheckNullParameterStore(_TicketName, obj.TicketName);
                var _ModifiedBy = new SqlParameter("@ModifiedBy", System.Data.SqlDbType.NVarChar);
                CheckNullParameterStore(_ModifiedBy, GetModifiedBy(_context));
                var _ModifiedDate = new SqlParameter("@ModifiedDate", System.Data.SqlDbType.DateTime);
                CheckNullParameterStore(_ModifiedDate, DateTime.Now);
                var _rs = _context.SQLCOMMANDS.FromSql(sql, _RowGuid, _WorkFlowGuid, _WorkFlowName, _TicketID, _TicketName, _ModifiedDate, _ModifiedBy).FirstOrDefault();
                if (_rs.Error == "")
                {
                    return new { Title = "Cập nhật thành công", Error = false };
                }
                else
                {
                    return new { Title = "Cập nhật không thành công", Error = true, Object = _rs.Error };
                }
            }
            catch (Exception ex)
            {

                return new { Title = "Cập nhật không thành công", Error = true, Object = ex };
            }
        }
        [HttpPost]
        public object IsDelete(Guid? id)
        {
            try
            {
                string sql = "[HR].[SP_ESHR_WorkFlowSetting_Delete] @RowGuid";
                var _RowGuid = new SqlParameter("@RowGuid", SqlDbType.UniqueIdentifier);
                CheckNullParameterStore(_RowGuid, id);
                var _rs = _context.SQLCOMMANDS.FromSql(sql, _RowGuid).FirstOrDefault();
                if (_rs.Error == "")
                {
                    return new { Title = " Xóa thành công", Error = false };
                }
                else
                {
                    return new { Title = " Đã có bảng khác sử dụng bản ghi này", Error = true, Object = _rs.Error };
                };
            }
            catch (Exception ex)
            {
                return new { Title = "Xóa không thành công.", Error = true, Object = ex };
            }
        }
        [HttpPost]
        public object DeleteItems([FromBody] List<Guid?> listGuid)
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var _emp = GetEmployeeLogin(_context);
                string sql = "[HR].[SP_ESHR_WorkFlowSetting_DeleteItem] @lstDeleteDetail";
                var _lstDeleteDetail = new SqlParameter("@lstDeleteDetail", listGuid.Count >= 1 ? string.Join(",", listGuid) : "");
                var __tem = _context.SQLCOMMANDS.FromSql(sql, _lstDeleteDetail).FirstOrDefault();
                if (__tem.Error == "")
                {
                    return new JMessage() { Error = false, Title = "Xóa thành công." };
                }
                else
                {
                    return new JMessage() { Error = true, Title = __tem.Error, Object = __tem };
                }
            }
            catch (Exception ex)
            {
                msg.Title = "Có lỗi khi xóa dữ liệu.";
                return Json(new JMessage() { Error = true, Object = ex, Title = msg.Title });
            }
        }
    }
}
