﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FTU.Utils.HelperNet;
using HRM.Controllers;
using HRM.Models;
using HRM.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace HRM.Areas.EIM.Controllers
{
    [Authorize]
    public class WorkflowsController : BaseController
    {
        public IConfigurationRoot Configuration { get; }
        private readonly HRMDBContext _context;
        private readonly IHostingEnvironment _environment;
        public WorkflowsController(HRMDBContext context, IHostingEnvironment environment)
        {
            _context = context;
            _environment = environment;
        }
        public class JTableSearch : JTableModel
        {
            public string Title { set; get; }
            public List<int?> lstInt { set; get; }
            public List<string> lstString { set; get; }
            public bool? IsActive { set; get; }
            public DateTime? StartDate { set; get; }
            public DateTime? EndDate { set; get; }
        }
        [Area("EIM")]
        public IActionResult Index()
        {
            return View();
        }
        #region
        [HttpGet]
        public object GetTable()
        {
            try
            {
                var data = _context.CategoryTables.Select(x => new { value = x.TableGuid, text = x.TableName }).ToList();
                return Json(data);
            }
            catch (Exception e)
            {
                return Json(new { Error = true, Title = "Lấy dữ liệu không thành công." });
            }
        }
        [HttpPost]
        public object GetColumn(Guid? Id)
        {
            try
            {
                var data = _context.CategoryColumns.Where(x => x.TableGuid == Id).Select(x => new { value = x.ColumnGuid, text = x.ColumnName, x.ConditionOfColumn, x.DataType }).ToList();
                return Json(data);
            }
            catch (Exception e)
            {
                return Json(new { Error = true, Title = "Lấy dữ liệu không thành công." });
            }
        }
        [HttpGet]
        public object GetWorkFlowStatus()
        {
            try
            {
                var data = _context.WorkFlowStatus.Select(x => new { value = x.CategoryId, text = x.CategoryName }).ToList();
                return Json(data);
            }
            catch (Exception ex)
            {
                return Json(new { Error = true, Title = "Lấy dữ liệu không thành công.", Object = ex });
            }
        }
        [HttpGet]
        public object GetAccountEmployees()
        {
            var _emp = GetEmployeeLogin(_context);
            try
            {
                var data = _context.Employees.Where(x => x.IsActive == true && x.LoginName != "" && x.LoginName != null && x.FullName != "" && x.FullName != null && x.OrganizationGuid == _emp.OrganizationGuid).Select(x => new { value = x.LoginName, text = x.FullName }).ToList();
                return Json(data);
            }
            catch (Exception ex)
            {
                return Json(new { Error = true, Title = "Lấy dữ liệu không thành công.", Object = ex });
            }
        }
        #endregion
        #region Xử lý
        public object Jtable()
        {
            try
            {
                var data = _context.Workflows.ToList();
                return Json(data);
            }
            catch (Exception ex)
            {
                return Json(new { Error = true, Object = ex, Title = "Lấy dữ liệu không thành công." });
            }
        }
        public class Data : JTableModel
        {
            public List<string> Status { get; set; }
            public List<string> lstDepartmentID { get; set; }
            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }
            public string Search { get; set; }
            public int? JobTitleId { get; set; }
            public string Keyword { get; set; }
            public string EmployeeId { get; set; }
            public bool? IsActive { get; set; }
        }
        [HttpPost]
        public object JTable_WF([FromBody] Data jTablePara)
        {
            var _emp = GetEmployeeLogin(_context);
            try
            {
                var count = (from a in _context.Workflows
                             where ((a.WorkflowId.Contains(jTablePara.search.value) || a.WorkflowName.Contains(jTablePara.search.value)) && a.IsActive == true && a.OrganizationGuid == _emp.OrganizationGuid)
                             select new { a.WorkflowGuid }).Count();
                int intBeginFor = (jTablePara.CurrentPage - 1) * jTablePara.Length;
                var data = (from a in _context.Workflows
                            where ((a.WorkflowId.Contains(jTablePara.search.value) || a.WorkflowName.Contains(jTablePara.search.value)) && a.IsActive == true && a.OrganizationGuid == _emp.OrganizationGuid)
                            select new { a.WorkflowGuid, a.WorkflowId, a.IsActive, a.WorkflowName, }).OrderUsingSortExpression(jTablePara.QueryOrderBy).Skip(intBeginFor).Take(jTablePara.Length).ToList();
                var jdata = JTableHelper.JObjectTable(data, jTablePara.Draw, count, "WorkflowGuid", "WorkflowId", "IsActive", "WorkflowName");
                return Json(jdata);
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = "Có lỗi khi lấy dữ liệu." });
            }

        }
        public class ObjectWorkFlow
        {
            public List<ConditionObject> Conditions { get; set; }
            public List<ActivitieObject> Activities { get; set; }
            public string WorkFlowId { get; set; }
            public string WorkFlowName { get; set; }
            public Guid TableGuid { get; set; }
            public Guid? WorkflowGuid { get; set; }
        }
        public class ConditionObject
        {
            public Guid? ConditionGuid { get; set; }
            public string ConditionData { get; set; }
            public string ConditionTrue { get; set; }
            public string ConditionFalse { get; set; }
            public string uidCodition { get; set; }
            public string ApproveType { get; set; }
            public string StepContinue { get; set; }
            public string StepCallback { get; set; }
            public string WFStatusId { get; set; }
            public string WFStatusBackId { get; set; }
            public string WFConditionId { get; set; }
            public string WFConditionBackId { get; set; }
        }
        public class ActivitieObject
        {
            public Guid? ActivityGuid { get; set; }
            public string ActivityName { get; set; }
            public string uidActivity { get; set; }
            public bool? stepStart { get; set; }
            public string conditionStep { get; set; }
        }
        public class ConditionTrue
        {
            public string Activity { get; set; }
            public string ApproveType { get; set; }
            public string[] Approver { get; set; }
            public string StepContinue { get; set; }
        }
        public class WorkFlowData
        {
            public string id { get; set; }
            public string type { get; set; }
            public string text { get; set; }
            public string x { get; set; }
            public string y { get; set; }
            public string fill { get; set; }
            public string stroke { get; set; }
            public string extraLinesStroke { get; set; }
            public string width { get; set; }
            public string height { get; set; }
            public string strokeWidth { get; set; }
            public string fontColor { get; set; }
            public string fontSize { get; set; }
            public string textAlign { get; set; }
            public string lineHeight { get; set; }
            public string fontStyle { get; set; }
            public string textVerticalAlign { get; set; }
            public string angle { get; set; }
            public string strokeType { get; set; }
        }
        public object InsertWorkFlow()
        {
            _context.Database.BeginTransaction();
            var _emp = GetEmployeeLogin(_context);
            try
            {
                var data = Request.Form["data"];
                var flowjson = Request.Form["flowjson"];
                var objData = JsonConvert.DeserializeObject<ObjectWorkFlow>(data);

                //Insert data to wf 
                var workFlow = new Workflows();
                workFlow.WorkflowGuid = Guid.NewGuid();
                //string count = _context.Workflows.Count(x => x.CreatedDate == DateTime.Now) < 10 ? "0" + _context.Workflows.Count(x => x.CreatedDate == DateTime.Now) : _context.Workflows.Count(x => x.CreatedDate == DateTime.Now).ToString();

                string wfId = "WF" + DateTime.Now.ToString("yyyyMMddHHmmss");
                workFlow.WorkflowId = wfId;
                workFlow.WorkflowName = objData.WorkFlowName;
                workFlow.WorkFlowData = flowjson;
                workFlow.OrganizationGuid = _emp.OrganizationGuid;
                workFlow.TableGuid = objData.TableGuid;
                workFlow.IsActive = true;
                workFlow.CreatedDate = DateTime.Now;
                workFlow.CreatedBy = GetCreatedBy(_context);
                workFlow.ModifiedDate = DateTime.Now;
                workFlow.ModifiedBy = GetModifiedBy(_context);
                _context.Workflows.Add(workFlow);
                _context.SaveChanges();

                //Insert data to Activities
                foreach (var item in objData.Activities)
                {
                    if (item.stepStart == true)
                    {
                        var activity = new Activities();
                        activity.ActivityGuid = Guid.NewGuid();
                        activity.ActivityName = item.ActivityName;
                        activity.Location = Guid.NewGuid();
                        activity.IsValid = true;
                        activity.WorkflowGuid = workFlow.WorkflowGuid;
                        activity.StartStep = item.stepStart;
                        activity.ActivityUid = item.uidActivity;
                        activity.CreatedDate = DateTime.Now;
                        activity.CreatedBy = GetCreatedBy(_context);
                        activity.ModifiedDate = DateTime.Now;
                        activity.ModifiedBy = GetModifiedBy(_context);
                        _context.Activities.Add(activity);
                        _context.SaveChanges();
                    }
                }

                //Insert data tp Conditions
                foreach (var item in objData.Conditions)
                {
                    var condition = new Conditions();
                    condition.ConditionGuid = Guid.NewGuid();
                    condition.WorkflowGuid = workFlow.WorkflowGuid;
                    condition.ConditionData = item.ConditionData;
                    condition.ConditionTrue = item.ConditionTrue;
                    condition.ConditionFalse = item.ConditionFalse;
                    condition.LocationGuid = Guid.NewGuid();
                    condition.WorkflowType = true;
                    condition.StepContinue = item.StepContinue;
                    condition.StepCallback = item.StepCallback;
                    condition.ConditionUid = item.uidCodition;
                    condition.WFStatusId = item.WFStatusId;
                    condition.WFStatusBackId = item.WFStatusBackId;
                    condition.WFConditionId = item.WFConditionId;
                    condition.WFConditionBackId = item.WFConditionBackId;
                    condition.CreatedDate = DateTime.Now;
                    condition.CreatedBy = GetCreatedBy(_context);
                    condition.ModifiedDate = DateTime.Now;
                    condition.ModifiedBy = GetModifiedBy(_context);
                    _context.Conditions.Add(condition);
                    _context.SaveChanges();
                }
                var updateStartStep = _context.Activities.Where(x => x.WorkflowGuid == workFlow.WorkflowGuid && x.StartStep == true).FirstOrDefault();
                var updateActivities = objData.Activities.Where(x => x.uidActivity == updateStartStep.ActivityUid).FirstOrDefault();
                if (updateActivities != null)
                {
                    updateStartStep.ConditionGuidOfStep = _context.Conditions.Where(x => x.WorkflowGuid == workFlow.WorkflowGuid && x.ConditionUid == updateActivities.conditionStep).Select(x => x.ConditionGuid).FirstOrDefault();
                    _context.SaveChanges();
                }

                foreach (var item in objData.Conditions)
                {
                    string[] unext = item.StepContinue.Split(',');
                    string[] uprevious = item.StepCallback.Split(',');

                    foreach (var inext in unext)
                    {
                        var ndata = objData.Activities.Where(x => x.uidActivity == inext).FirstOrDefault();
                        if (ndata != null)
                        {
                            var activity = new Activities();
                            activity.ActivityGuid = Guid.NewGuid();
                            activity.ActivityName = ndata.ActivityName;
                            activity.Location = Guid.NewGuid();
                            activity.IsValid = true;
                            activity.ConditionGuidOfStep = _context.Conditions.Where(x => x.WorkflowGuid == workFlow.WorkflowGuid && x.ConditionUid == ndata.conditionStep).Select(x => x.ConditionGuid).FirstOrDefault();
                            activity.WorkflowGuid = workFlow.WorkflowGuid;
                            activity.StartStep = false;
                            activity.ActivityUid = ndata.uidActivity;
                            activity.CreatedDate = DateTime.Now;
                            activity.CreatedBy = GetCreatedBy(_context);
                            activity.ModifiedDate = DateTime.Now;
                            activity.ModifiedBy = GetModifiedBy(_context);
                            _context.Activities.Add(activity);
                            _context.SaveChanges();

                        }
                    }
                    foreach (var iprevious in uprevious)
                    {
                        var ndata = objData.Activities.Where(x => x.uidActivity == iprevious).FirstOrDefault();
                        if (ndata != null)
                        {
                            var activity = new Activities();
                            activity.ActivityGuid = Guid.NewGuid();
                            activity.ActivityName = ndata.ActivityName;
                            activity.Location = Guid.NewGuid();
                            activity.IsValid = true;
                            activity.ConditionGuidOfStep = _context.Conditions.Where(x => x.WorkflowGuid == workFlow.WorkflowGuid && x.ConditionUid == ndata.conditionStep).Select(x => x.ConditionGuid).FirstOrDefault();
                            activity.WorkflowGuid = workFlow.WorkflowGuid;
                            activity.StartStep = false;
                            activity.ActivityUid = ndata.uidActivity;
                            activity.CreatedDate = DateTime.Now;
                            activity.CreatedBy = GetCreatedBy(_context);
                            activity.ModifiedDate = DateTime.Now;
                            activity.ModifiedBy = GetModifiedBy(_context);
                            _context.Activities.Add(activity);
                            _context.SaveChanges();

                        }
                    }
                }
                var d = objData.Activities.Where(a => a.stepStart == true).Select(a => a.conditionStep).FirstOrDefault();
                var updateC = _context.Conditions.FirstOrDefault(x => x.WorkflowGuid == workFlow.WorkflowGuid && x.ConditionUid == d);
                var updateA = _context.Activities.FirstOrDefault(x => x.WorkflowGuid == workFlow.WorkflowGuid && x.StartStep == true);
                updateA.ConditionGuidOfStep = updateC.ConditionGuid;
                _context.SaveChanges();

                _context.Database.CommitTransaction();
                return Json(new { Error = false, Title = "Thêm mới thành công." });

            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return Json(new { Error = true, Object = ex, Title = ex.Message });
            }
        }
        public object UpdateWorkFlow()
        {
            _context.Database.BeginTransaction();
            try
            {
                var data = Request.Form["data"];
                var flowjson = Request.Form["flowjson"];
                var objData = JsonConvert.DeserializeObject<ObjectWorkFlow>(data);
                var _emp = GetEmployeeLogin(_context);
                //Update data to wf 
                var workFlows = _context.Workflows.Where(x => x.WorkflowGuid == objData.WorkflowGuid).FirstOrDefault();
                var _guid = workFlows.WorkflowGuid;
                var objactivitis = _context.Activities.Where(x => x.WorkflowGuid == _guid).ToList();
                var objcondition = _context.Conditions.Where(x => x.WorkflowGuid == _guid).ToList();
                foreach (var i in objactivitis)
                {
                    _context.Activities.Remove(i);
                    _context.SaveChanges();
                }
                foreach (var j in objcondition)
                {
                    _context.Conditions.Remove(j);
                    _context.SaveChanges();
                }
                //Update data to wf 
                var workFlow = _context.Workflows.Where(x => x.WorkflowGuid == objData.WorkflowGuid).FirstOrDefault();

                workFlow.WorkflowName = objData.WorkFlowName;
                workFlow.WorkFlowData = flowjson;
                workFlow.TableGuid = objData.TableGuid;
                workFlow.IsActive = true;

                workFlow.ModifiedDate = DateTime.Now;
                workFlow.ModifiedBy = GetModifiedBy(_context);
                _context.Entry(workFlow).State = EntityState.Modified;
                _context.Entry(workFlow).Property(x => x.WorkflowId).IsModified = false;
                _context.Entry(workFlow).Property(x => x.CreatedBy).IsModified = false;
                _context.SaveChanges();

                var apply = JsonConvert.DeserializeObject<List<WorkFlowData>>(flowjson);
                var lstWF = apply.Select(x => x.id).Distinct().ToList();
                objData.Conditions = objData.Conditions.Where(x => lstWF.Contains(x.uidCodition)).ToList();
                objData.Activities = objData.Activities.Where(x => lstWF.Contains(x.uidActivity)).ToList();

                //Insert data to Activities
                foreach (var item in objData.Activities)
                {
                    var activity = new Activities();
                    activity.ActivityGuid = Guid.NewGuid();
                    activity.ActivityName = item.ActivityName;
                    activity.Location = Guid.NewGuid();
                    activity.IsValid = true;
                    activity.WorkflowGuid = workFlow.WorkflowGuid;
                    activity.StartStep = item.stepStart;
                    activity.ActivityUid = item.uidActivity;
                    activity.CreatedDate = DateTime.Now;
                    activity.CreatedBy = GetCreatedBy(_context);
                    activity.ModifiedDate = DateTime.Now;
                    activity.ModifiedBy = GetModifiedBy(_context);
                    _context.Activities.Add(activity);
                    _context.SaveChanges();
                }
                var updateStartStep = _context.Activities.Where(x => x.WorkflowGuid == workFlow.WorkflowGuid && x.StartStep == true).FirstOrDefault();
                var updateActivities = objData.Activities.Where(x => x.uidActivity == updateStartStep.ActivityUid).FirstOrDefault();
                //Insert data tp Conditions
                foreach (var item in objData.Conditions)
                {
                    var condition = new Conditions();
                    condition.ConditionGuid = Guid.NewGuid();
                    condition.WorkflowGuid = workFlow.WorkflowGuid;
                    condition.ConditionData = item.ConditionData;

                    var ctrue = JsonConvert.DeserializeObject<List<ConditionTrue>>(item.ConditionTrue);
                    var lstSting = new List<string>();
                    if (ctrue != null)
                    {
                        foreach (var ap1 in ctrue)
                        {
                            if (ap1.Approver == null || ap1.Approver.Count() == 0)
                            {
                                _context.Database.RollbackTransaction();
                                return Json(new { Error = true, Title = "Yêu cầu chọn người duyệt ở bước " + updateActivities.ActivityName });
                            }

                            foreach (var ap in ap1.Approver)
                            {
                                var _employ = _context.Employees.Where(x => x.OrganizationGuid == _emp.OrganizationGuid && x.IsActive == true && x.LoginName == ap).Count();
                                if (_employ > 0)
                                {
                                    lstSting.Add(ap);
                                }
                            }
                        }
                    }
                    ctrue[0].Approver = null;
                    ctrue[0].Approver = lstSting.ToArray();
                    var sb = JsonConvert.SerializeObject(ctrue);
                    condition.ConditionTrue = sb;
                    condition.ConditionFalse = item.ConditionFalse;
                    condition.LocationGuid = Guid.NewGuid();
                    condition.WorkflowType = true;
                    condition.StepContinue = item.StepContinue;
                    condition.StepCallback = item.StepCallback;
                    condition.ConditionUid = item.uidCodition;
                    condition.WFStatusId = item.WFStatusId;
                    if (condition.WFStatusId == null || condition.WFStatusId == "")
                    {
                        _context.Database.RollbackTransaction();
                        return Json(new { Error = true, Title = "Yêu cầu chọn trạng thái ở bước " + updateActivities.ActivityName });
                    }
                    condition.WFStatusBackId = item.WFStatusBackId;
                    condition.WFConditionId = item.WFConditionId;
                    condition.WFConditionBackId = item.WFConditionBackId;
                    condition.CreatedDate = DateTime.Now;
                    condition.CreatedBy = GetCreatedBy(_context);
                    condition.ModifiedDate = DateTime.Now;
                    condition.ModifiedBy = GetModifiedBy(_context);
                    _context.Conditions.Add(condition);
                    _context.SaveChanges();
                }

                if (updateActivities != null)
                {
                    updateStartStep.ConditionGuidOfStep = _context.Conditions.Where(x => x.WorkflowGuid == workFlow.WorkflowGuid && x.ConditionUid == updateActivities.conditionStep).Select(x => x.ConditionGuid).FirstOrDefault();
                    _context.SaveChanges();
                }

                foreach (var item in objData.Conditions)
                {
                    string[] unext = item.StepContinue.Split(',');
                    string[] uprevious = item.StepCallback.Split(',');

                    foreach (var inext in unext)
                    {
                        var ndata = _context.Activities.Where(x => x.WorkflowGuid == workFlow.WorkflowGuid && x.ActivityUid == inext).FirstOrDefault();
                        if (ndata != null)
                        {
                            var nda = objData.Activities.Where(x => x.uidActivity == inext).FirstOrDefault();
                            if (nda != null)
                            {
                                ndata.ConditionGuidOfStep = _context.Conditions.Where(x => x.WorkflowGuid == workFlow.WorkflowGuid && x.ConditionUid == nda.conditionStep).Select(x => x.ConditionGuid).FirstOrDefault();
                                _context.SaveChanges();
                            }
                        }
                    }
                    foreach (var iprevious in uprevious)
                    {
                        var ndata = _context.Activities.Where(x => x.WorkflowGuid == workFlow.WorkflowGuid && x.ActivityUid == iprevious).FirstOrDefault();
                        if (ndata != null)
                        {
                            var nda = objData.Activities.Where(x => x.uidActivity == iprevious).FirstOrDefault();
                            if (nda != null)
                            {
                                ndata.ConditionGuidOfStep = _context.Conditions.Where(x => x.WorkflowGuid == workFlow.WorkflowGuid && x.ConditionUid == nda.conditionStep).Select(x => x.ConditionGuid).FirstOrDefault();
                                _context.SaveChanges();
                            }
                        }
                    }
                }
                var d = objData.Activities.Where(a => a.stepStart == true).Select(a => a.conditionStep).FirstOrDefault();

                var updateC = _context.Conditions.FirstOrDefault(x => x.WorkflowGuid == workFlow.WorkflowGuid && x.ConditionUid == d);
                var updateA = _context.Activities.FirstOrDefault(x => x.WorkflowGuid == workFlow.WorkflowGuid && x.StartStep == true);
                updateA.ConditionGuidOfStep = updateC.ConditionGuid;
                _context.SaveChanges();

                _context.Database.CommitTransaction();
                return Json(new { Error = false, Title = "Cập nhật thành công." });

            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return Json(new { Error = true, Object = ex, Title = ex.Message });
            }
        }


        public object LoadWorkFlow(string guid)
        {
            try
            {
                Guid _guid = new Guid(guid);
                var data = _context.Workflows.FirstOrDefault(x => x.WorkflowGuid == _guid);
                var Condition = _context.Conditions.Where(x => x.WorkflowGuid == _guid).ToList();
                var Activity = _context.Activities.Where(x => x.WorkflowGuid == _guid).ToList();

                foreach (var item in Activity)
                {
                    var conditionStep = _context.Conditions.Where(x => x.ConditionGuid == item.ConditionGuidOfStep).Select(x => x.ConditionUid).FirstOrDefault();
                    item.ModifiedBy = conditionStep;
                }
                return Json(new { data = data, Conditions = Condition, Activities = Activity });
            }
            catch (Exception ex)
            {
                return Json(new { Error = true, Object = ex, Title = "Lấy dữ liệu không thành công." });
            }
        }
        public object IsDeleted(string guid)
        {
            _context.Database.BeginTransaction();
            try
            {
                var _guid = new Guid(guid);
                var activity = _context.Activities.Where(x => x.WorkflowGuid == _guid).ToList();
                var condition = _context.Conditions.Where(x => x.WorkflowGuid == _guid).ToList();
                var wf = _context.Workflows.Where(x => x.WorkflowGuid == _guid).FirstOrDefault();

                _context.Activities.RemoveRange(activity);
                _context.SaveChanges();

                _context.Conditions.RemoveRange(condition);
                _context.SaveChanges();

                _context.Workflows.Remove(wf);
                _context.SaveChanges();

                _context.Database.CommitTransaction();
                return Json(new { Error = false, Title = "Xóa thành công." });

            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return Json(new { Error = true, Title = "Có lỗi khi xóa.", Object = ex });
            }
        }
        public object GetItem(string guid)
        {
            try
            {
                var _guid = new Guid(guid);
                var wf = _context.Workflows.Where(x => x.WorkflowGuid == _guid).FirstOrDefault();
                return Json(wf);
            }
            catch (Exception ex)
            {
                return Json(new { Error = true, Title = "Có lỗi khi lấy dữ liệu.", Object = ex });
            }
        }
        #endregion
        #region nhân bản quy trình
        [HttpGet]
        public object GetOrganizations()
        {
            try
            {
                var rs = _context.Organizations.Where(x => x.IsActive == true)
                    .Select(x => new
                    {
                        x.OrganizationGuid,
                        x.OrganizationName
                    }).ToList();
                return Json(rs);
            }
            catch (Exception ex)
            {
                return Json(new { Error = true, Title = "Lấy dữ liệu không thành công", Object = ex });
            }
        }
        public partial class DataCopy
        {
            public Guid? OrganizationGuid { get; set; }
            public Guid? WorkflowGuid { get; set; }
        }
        [HttpPost]
        public object Copy([FromBody] DataCopy obj)
        {
            var msg = new JMessage() { Error = false };
            var _emp = GetEmployeeLogin(_context);
            try
            {
                _context.Database.BeginTransaction();
                var lstCondition = _context.Conditions.Where(x => x.WorkflowGuid == obj.WorkflowGuid).ToList();
                var lstActivities = _context.Activities.Where(x => x.WorkflowGuid == obj.WorkflowGuid).ToList();
                var rs = _context.Workflows.Where(x => x.WorkflowGuid == obj.WorkflowGuid).FirstOrDefault();
                //insert workflows
                var workFlow = new Workflows();
                var _guidWF = Guid.NewGuid();
                workFlow.WorkflowGuid = _guidWF;
                string wfId = "WF" + DateTime.Now.ToString("yyyyMMddHHmmss");
                workFlow.WorkflowId = wfId;
                workFlow.WorkflowName = rs.WorkflowName;
                workFlow.OrganizationGuid = obj.OrganizationGuid;
                workFlow.ModuleId = rs.ModuleId;
                workFlow.ModuleName = rs.ModuleName;
                workFlow.WorkFlowData = rs.WorkFlowData;
                workFlow.TableGuid = rs.TableGuid;
                workFlow.TableName = rs.TableName;
                workFlow.IsActive = true;
                workFlow.CreatedDate = DateTime.Now;
                workFlow.CreatedBy = GetCreatedBy(_context);
                workFlow.ModifiedDate = DateTime.Now;
                workFlow.ModifiedBy = GetModifiedBy(_context);
                _context.Workflows.Add(workFlow);
                _context.SaveChanges();
                //Insert data tp Conditions
                foreach (var item in lstCondition)
                {
                    var condition = new Conditions();
                    var _guidCondition = Guid.NewGuid();
                    condition.ConditionGuid = _guidCondition;
                    condition.WorkflowGuid = _guidWF;
                    condition.ConditionData = item.ConditionData;
                    condition.ConditionTrue = item.ConditionTrue;
                    condition.ConditionFalse = item.ConditionFalse;
                    condition.LocationGuid = item.LocationGuid;
                    condition.WorkflowType = true;
                    condition.StepContinue = item.StepContinue;
                    condition.StepCallback = item.StepCallback;
                    condition.ConditionUid = item.ConditionUid;
                    condition.WFStatusId = item.WFStatusId;
                    condition.WFStatusBackId = item.WFStatusBackId;
                    condition.WFConditionId = item.WFConditionId;
                    condition.WFConditionBackId = item.WFConditionBackId;
                    condition.CreatedDate = DateTime.Now;
                    condition.CreatedBy = GetCreatedBy(_context);
                    condition.ModifiedDate = DateTime.Now;
                    condition.ModifiedBy = GetCreatedBy(_context);
                    _context.Conditions.Add(condition);
                    _context.SaveChanges();

                    //Insert data to Activities
                    var activity = _context.Activities.FirstOrDefault(x => x.ConditionGuidOfStep == item.ConditionGuid && x.WorkflowGuid == obj.WorkflowGuid);
                    if (activity != null)
                    {
                        var act = new Activities();
                        act.ActivityGuid = Guid.NewGuid();
                        act.ConditionGuid = activity.ConditionGuid;
                        act.ConditionGuidOfStep = _guidCondition;
                        act.WorkflowGuid = _guidWF;
                        act.ActivityName = activity.ActivityName;
                        act.IsActive = activity.IsActive;
                        act.ParentId = activity.ParentId;
                        act.Location = activity.Location;
                        act.StartStep = activity.StartStep;
                        act.IsValid = activity.IsValid;
                        act.ActivityUid = activity.ActivityUid;
                        act.CreatedDate = DateTime.Now;
                        act.CreatedBy = GetCreatedBy(_context);
                        act.ModifiedDate = DateTime.Now;
                        act.ModifiedBy = GetCreatedBy(_context);
                        _context.Activities.Add(act);
                        _context.SaveChanges();
                    }
                }
                _context.Database.CommitTransaction();
                return Json(new { Error = false, Title = "Nhân bản thành công" });
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return Json(new { Error = true, Title = "Nhân bản không thành công", Object = ex });
            }
        }
        #endregion
    }
}