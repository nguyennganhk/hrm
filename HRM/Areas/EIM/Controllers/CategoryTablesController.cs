﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using FTU.Utils.HelperNet;
using HRM.Controllers;
using HRM.Models;
using HRM.Utils;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace HRM.Areas.EIM.Controllers
{
    public class CategoryTablesController : BaseController
    {

        private readonly HRMDBContext _context;
        private readonly AppSettings _appSettings;
        private readonly IHostingEnvironment _environment;

        public CategoryTablesController(IOptions<AppSettings> appSettings, HRMDBContext context, IHostingEnvironment environment)
        {
            _context = context;
            _appSettings = appSettings.Value;
            _environment = environment;
        }
        [Area("EIM")]
        public IActionResult Index()
        {
            return View();
        }
        public class Data : JTableModel
        {
            public bool? IsActive { get; set; }
        }
        public object JTable([FromBody] Data jTablePara)
        {
            try
            {
                //var o = _context.Employees.Where(x => x.LoginName == User.Identity.Name).ToList().FirstOrDefault();
                var count = _context.CategoryTables.Where(x => x.IsActive == jTablePara.IsActive
                && (x.TableId.Contains(jTablePara.search.value) || x.TableName.Contains(jTablePara.search.value)
                || x.ModuleId.Contains(jTablePara.search.value) || x.ModuleName.Contains(jTablePara.search.value))).ToList();
                var countdata = count.Count();
                int intBeginFor = (jTablePara.CurrentPage - 1) * jTablePara.Length;
                var data = count.Skip(intBeginFor).Take(jTablePara.Length).ToList();
                var jdata = JTableHelper.JObjectTable(data, jTablePara.Draw, countdata, "TableGuid", "OrganizationGuid", "TableID", "TableName", "TableType", "ModuleID", "ModuleName", "IsActive", "CreatedDate", "CreatedBy", "ModifiedDate", "ModifiedDate");
                return Json(jdata);
            }
            catch (Exception ex)
            {
                return Json(ex);
            }

        }
        public JsonResult Insert([FromBody] CategoryTables obj)
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var emp = GetEmployeeLogin(_context);
                //obj.OrganizationGuid = emp.OrganizationGuid;
                obj.TableGuid = Guid.NewGuid();
                obj.CreatedDate = DateTime.Now;
                obj.ModifiedDate = DateTime.Now;
                obj.CreatedBy = GetCreatedBy(_context);
                obj.ModifiedBy = GetModifiedBy(_context);
                _context.CategoryTables.Add(obj);
                var a = _context.SaveChanges();
                msg.Title = "Thêm thành công";
            }
            catch (Exception ex)
            {
                if (ex.InnerException.Data["HelpLink.EvtID"].ToString() == "2627")
                {
                    msg.Title = "Bảng đã tồn tại";
                }
                else
                {
                    msg.Title = "Có lỗi khi thêm";
                }
                msg.Object = ex.InnerException;
                msg.Error = true;

            }
            return Json(msg);
        }
        //[ActionAuthorize(EAction.EDIT)]
        public JsonResult Update([FromBody] CategoryTables obj)
        {

            var msg = new JMessage() { Error = false };
            try
            {
                obj.ModifiedBy = GetModifiedBy(_context);
                obj.ModifiedDate = DateTime.Now;
                _context.Database.ExecuteSqlCommand("update WF.CategoryTables set TableID=@TableID,TableName=@TableName,TableType=@TableType,ModuleID=@ModuleID,ModuleName=@ModuleName,IsActive=@IsActive where TableGuid = @TableGuid ", parameters: new[]{
                        new SqlParameter("@TableGuid", (Object) obj.TableGuid?? DBNull.Value),
                        new SqlParameter("@OrganizationGuid",  (Object) obj.OrganizationGuid?? DBNull.Value),
                        new SqlParameter("@TableID",  (Object) obj.TableId?? DBNull.Value),
                        new SqlParameter("@TableName",  (Object) obj.TableName?? DBNull.Value),
                        new SqlParameter("@TableType",  (Object) obj.TableType?? DBNull.Value),
                        new SqlParameter("@ModuleID",  (Object) obj.ModuleId?? DBNull.Value),
                        new SqlParameter("@ModuleName",  (Object) obj.ModuleName?? DBNull.Value),
                        new SqlParameter("@IsActive",  (Object) obj.IsActive?? DBNull.Value),
                                                });
                msg.Title = "Cập nhật thành công";
            }
            catch (SqlException ex)
            {
                if (ex.Number == 2627)
                {
                    msg.Error = true;
                    msg.Title = "Bảng đã tồn tại";
                    return Json(msg);
                }
                msg.Error = true;
                msg.Title = "Có lỗi khi cập nhật";
            }
            return Json(msg);
        }
        [HttpPost]
        //[ActionAuthorize(EAction.DELETE)]
        public object IsDeleted(string Id)
        {
            try
            {
                var guid = new Guid(Id);
                var obj = _context.CategoryTables.First(m => m.TableGuid == guid);
                _context.CategoryTables.Remove(obj);
                _context.SaveChanges();
                return Json(new JMessage() { Error = false, Title = "Xóa thành công." });
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Title = "Có lỗi khi xóa .", Object = ex });
            }
        }

        [HttpPost]
        //[ActionAuthorize(EAction.DELETE)]
        public object IsDeletedItems([FromBody] List<Guid> Items)
        {
            try
            {
                for (int i = 0; i < Items.Count; i++)
                {
                    var obj = _context.CategoryTables.First(m => m.TableGuid == Items[i]);
                    _context.CategoryTables.Remove(obj);
                    _context.SaveChanges();
                }
                return Json(new JMessage() { Error = false, Title = "Xóa thành công." });
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Title = "Có lỗi khi xóa.", Object = ex });
            }
        }
        [HttpPost]
        public object GetItem(Guid? Id)
        {

            if (Id == null)
            {
                return Json("");
            }
            var a = _context.CategoryTables.FirstOrDefault(m => m.TableGuid == Id);
            return Json(a);
        }
    }
}
