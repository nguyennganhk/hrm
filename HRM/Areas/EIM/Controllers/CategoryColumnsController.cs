﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using FTU.Utils.HelperNet;
using HRM.Controllers;
using HRM.Models;
using HRM.Utils;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
namespace HRM.Areas.EIM.Controllers
{
    public class CategoryColumnsController : BaseController
    {

        private readonly HRMDBContext _context;
        private readonly AppSettings _appSettings;
        private readonly IHostingEnvironment _environment;

        public CategoryColumnsController(IOptions<AppSettings> appSettings, HRMDBContext context, IHostingEnvironment environment)
        {
            _context = context;
            _appSettings = appSettings.Value;
            _environment = environment;
        }
        [Area("EIM")]
        public IActionResult Index()
        {
            return View();
        }
        public object GetCategoryTable()
        {
            try
            {
                var data = _context.CategoryTables.ToList();
                return Json(data);
            }
            catch (Exception ex)
            {
                return Json(new { Error = true, Title = "Lấy dữ liệu không thành công.", Object = ex });
            }
        }
        public class Data : JTableModel
        {
            public bool? IsActive { get; set; }
            public string BankCodeS { get; set; }
        }
        public object JTable([FromBody] Data jTablePara)
        {
            try
            {
                //var o = _context.Employees.Where(x => x.LoginName == User.Identity.Name).ToList().FirstOrDefault();
                var count = _context.CategoryColumns.Where(x => x.TableId.Contains(jTablePara.search.value) || x.TableName.Contains(jTablePara.search.value)
                || x.ColumnName.Contains(jTablePara.search.value) || x.ColumnId.Contains(jTablePara.search.value)).ToList();
                var countdata = count.Count();
                int intBeginFor = (jTablePara.CurrentPage - 1) * jTablePara.Length;
                var data = count.Skip(intBeginFor).Take(jTablePara.Length).ToList();
                var jdata = JTableHelper.JObjectTable(data, jTablePara.Draw, countdata, "ColumnGuid", "TableGuid", "TableId", "TableName", "ColumnId", "ColumnName", "DataType", "CreatedDate", "CreatedBy", "ModifiedDate", "ModifiedDate");
                return Json(jdata);
            }
            catch (Exception ex)
            {
                return Json(ex);
            }

        }
        public JsonResult Insert([FromBody] CategoryColumns obj)
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var emp = GetEmployeeLogin(_context);
                //obj.OrganizationGuid = emp.OrganizationGuid;
                obj.ColumnGuid = Guid.NewGuid();
                obj.CreatedDate = DateTime.Now;
                obj.ModifiedDate = DateTime.Now;
                obj.CreatedBy = GetCreatedBy(_context);
                obj.ModifiedBy = GetModifiedBy(_context);
                _context.CategoryColumns.Add(obj);
                var a = _context.SaveChanges();
                msg.Title = "Thêm thành công";
            }
            catch (Exception ex)
            {
                if (ex.InnerException.Data["HelpLink.EvtID"].ToString() == "2627")
                {
                    msg.Title = "Cột đã tồn tại";
                }
                else
                {
                    msg.Title = "Có lỗi khi thêm";
                }
                msg.Object = ex.InnerException;
                msg.Error = true;

            }
            return Json(msg);
        }
        //[ActionAuthorize(EAction.EDIT)]
        public JsonResult Update([FromBody] CategoryColumns obj)
        {

            var msg = new JMessage() { Error = false };
            try
            {
                obj.ModifiedBy = GetModifiedBy(_context);
                obj.ModifiedDate = DateTime.Now;
                _context.Database.ExecuteSqlCommand("update WF.CategoryColumns set TableGuid=@TableGuid,TableID=@TableID,TableName=@TableName,ColumnID=@ColumnID,ColumnName=@ColumnName,ConditionOfColumn = @ConditionOfColumn,DataType=@DataType where ColumnGuid = @ColumnGuid ", parameters: new[]{
                        new SqlParameter("@ColumnGuid", (Object) obj.ColumnGuid?? DBNull.Value),
                        new SqlParameter("@TableGuid",  (Object) obj.TableGuid?? DBNull.Value),
                        new SqlParameter("@TableID",  (Object) obj.TableId?? DBNull.Value),
                        new SqlParameter("@TableName",  (Object) obj.TableName?? DBNull.Value),
                        new SqlParameter("@ColumnID",  (Object) obj.ColumnId?? DBNull.Value),
                        new SqlParameter("@ColumnName",  (Object) obj.ColumnName?? DBNull.Value),
                        new SqlParameter("@ConditionOfColumn",  (Object) obj.ConditionOfColumn?? DBNull.Value),
                        new SqlParameter("@DataType",  (Object) obj.DataType?? DBNull.Value)
                                                });
                msg.Title = "Cập nhật thành công";
            }
            catch (SqlException ex)
            {
                if (ex.Number == 2627)
                {
                    msg.Error = true;
                    msg.Title = "Cột đã tồn tại";
                    return Json(msg);
                }
                msg.Error = true;
                msg.Title = "Có lỗi khi cập nhật";
            }
            return Json(msg);
        }
        [HttpPost]
        //[ActionAuthorize(EAction.DELETE)]
        public object IsDeleted(string Id)
        {
            try
            {
                var guid = new Guid(Id);
                var obj = _context.CategoryColumns.First(m => m.ColumnGuid == guid);
                _context.CategoryColumns.Remove(obj);
                _context.SaveChanges();
                return Json(new JMessage() { Error = false, Title = "Xóa thành công." });
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Title = "Có lỗi khi xóa .", Object = ex });
            }
        }

        [HttpPost]
        //[ActionAuthorize(EAction.DELETE)]
        public object IsDeletedItems([FromBody] List<Guid> Items)
        {
            try
            {
                for (int i = 0; i < Items.Count; i++)
                {
                    var obj = _context.CategoryColumns.First(m => m.ColumnGuid == Items[i]);
                    _context.CategoryColumns.Remove(obj);
                    _context.SaveChanges();
                }
                return Json(new JMessage() { Error = false, Title = "Xóa thành công." });
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Title = "Có lỗi khi xóa.", Object = ex });
            }
        }
        [HttpPost]
        public object GetItem(Guid? Id)
        {

            if (Id == null)
            {
                return Json("");
            }
            var a = _context.CategoryColumns.FirstOrDefault(m => m.ColumnGuid == Id);
            return Json(a);
        }
    }
}
