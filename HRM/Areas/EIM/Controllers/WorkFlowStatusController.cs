﻿using FTU.Utils.HelperNet;
using HRM.Controllers;
using HRM.Utils;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System;
using Microsoft.AspNetCore.Hosting;
using HRM.Models;
using Newtonsoft.Json;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace HRM.Areas.EIM.Controllers
{
    public class WorkFlowStatusController : BaseController
    {
        private readonly HRMDBContext _context;
        private readonly AppSettings _appSettings;
        private readonly IHostingEnvironment _environment;

        public WorkFlowStatusController(IOptions<AppSettings> appSettings, HRMDBContext context, IHostingEnvironment environment)
        {
            _context = context;
            _appSettings = appSettings.Value;
            _environment = environment;
        }
        [Area("EIM")]
        public IActionResult Index()
        {
            return View();
        }
        public class DataJtable : JTableModel
        {
            public bool? IsActive { get; set; }
            public string Type { get; set; }
        }
        //View
        [HttpPost]
        public object JTable([FromBody] DataJtable jTablePara)
        {
            try
            {
                int intBeginFor = (jTablePara.CurrentPage - 1) * jTablePara.Length;
                string sql = "[HR].[SP_FITEK_HR_WorkFlowStatus_ListAll]" +
                "@Keyword, " +
                "@IsActive, " +
                "@Sort, " +
                "@Skip, " +
                "@Take," +
                "@output out ";
                var _Keyword = new SqlParameter("@Keyword", SqlDbType.NVarChar, 255); CheckNullParameterStore(_Keyword, jTablePara.search.value);
                var _Sort = new SqlParameter("@Sort", SqlDbType.VarChar); _Sort.Value = jTablePara.QueryOrderBy;
                var _IsActive = new SqlParameter("@IsActive", SqlDbType.VarChar); CheckNullParameterStore(_IsActive, jTablePara.IsActive);
                var _Skip = new SqlParameter("@Skip", SqlDbType.Int); _Skip.Value = intBeginFor;
                var _Take = new SqlParameter("@Take", SqlDbType.Int); _Take.Value = jTablePara.Length;
                var _output = new SqlParameter("@output", SqlDbType.Int) { Direction = System.Data.ParameterDirection.Output };
                var data = _context.WorkFlowStatusJtableView.FromSql(sql, _Keyword, _IsActive, _Sort, _Skip, _Take, _output).ToList();
                var jdata = JTableHelper.JObjectTable(data, jTablePara.Draw, (int)_output.Value);
                return Json(jdata);
            }
            catch (Exception ex)
            {
                return Json(ex);
            }
        }
        public object GetDepartment()
        {
            var userlogin = GetEmployeeLogin(_context);
            var data = _context.Departments.Select(x => new { x.DepartmentGuid, x.DepartmentId }).ToList();
            return data;
        }
        [HttpPost]
        public object Insert()
        {
            var msg = new JMessage() { Error = false };
            var data = Request.Form["WorkFlowStatus"];
            WorkFlowStatus obj = JsonConvert.DeserializeObject<WorkFlowStatus>(data);
            obj.ModifiedDate = DateTime.Now;
            obj.ModifiedBy = GetModifiedBy(_context);
            var s = GetEmployeeLogin(_context);
            try
            {
                var countID = _context.WorkFlowStatus.Where(x => x.CategoryId == obj.CategoryId).Count();
                if (countID > 0)
                {
                    return new { Title = "Mã trạng thái quy trình đã tồn tại", Error = true };
                }

                string sql = "[Common].[SP_ESVCM_WorkFlowStatus_Insert] @CategoryID,@CategoryName,@Description,@OrderID,@LocationID,@IsActive,@CreatedDate, @CreatedBy,@ModefiledDate,@ModefiledBy, @ReturnCode OUT";
                var _CategoryID = new SqlParameter("@CategoryID", SqlDbType.VarChar, 2);
                CheckNullParameterStore(_CategoryID, obj.CategoryId);
                var _CategoryName = new SqlParameter("@CategoryName", SqlDbType.NVarChar, 100);
                CheckNullParameterStore(_CategoryName, obj.CategoryName);
                var _Description = new SqlParameter("@Description", SqlDbType.NVarChar, 250);
                CheckNullParameterStore(_Description, obj.Description);
                var _OrderId = new SqlParameter("@OrderID", SqlDbType.Int);
                CheckNullParameterStore(_OrderId, obj.OrderId);
                var _LocationID = new SqlParameter("@LocationID", SqlDbType.VarChar, 20);
                CheckNullParameterStore(_LocationID, obj.LocationId);
                var _IsActive = new SqlParameter("@IsActive", SqlDbType.Bit);
                CheckNullParameterStore(_IsActive, obj.IsActive);
                var CreatedDate = new SqlParameter("@CreatedDate", SqlDbType.DateTime);
                CreatedDate.Value = DateTime.Now;
                var CreatedBy = new SqlParameter("@CreatedBy", SqlDbType.NVarChar, 250);
                CreatedBy.Value = GetCreatedBy(_context);
                var ModefiledDate = new SqlParameter("@ModefiledDate", SqlDbType.DateTime);
                ModefiledDate.Value = DateTime.Now;
                var ModefiledBy = new SqlParameter("@ModefiledBy", SqlDbType.NVarChar, 50);
                ModefiledBy.Value = obj.ModifiedBy;
                var ReturnCode = new SqlParameter("@ReturnCode", SqlDbType.NVarChar, 1000) { Direction = System.Data.ParameterDirection.Output };
                ReturnCode.Value = "";
                var rs = _context.Database.ExecuteSqlCommand(sql, _CategoryID, _CategoryName, _Description, _OrderId, _LocationID, _IsActive, CreatedDate, CreatedBy, ModefiledDate, ModefiledBy, ReturnCode);
                if (ReturnCode.Value.ToString() == "success")
                {
                    return new { Title = "Thêm mới thành công", Error = false };
                }
            }
            catch (Exception ex)
            {
                return new { Title = "Thêm mới không thành công!", Error = true, Object = ex };
            }
            return null;
        }

        //Update
        [HttpPost]
        public object Update()
        {
            var msg = new JMessage() { Error = false };
            var data = Request.Form["WorkFlowStatus"];
            WorkFlowStatus obj = JsonConvert.DeserializeObject<WorkFlowStatus>(data);
            obj.ModifiedDate = DateTime.Now;
            obj.ModifiedBy = GetModifiedBy(_context);
            var s = GetEmployeeLogin(_context);
            try
            {
                string sql = "[Common].[SP_ESVCM_WorkFlowStatus_Update] @CategoryID,@CategoryName,@Description,@OrderID,@LocationID,@IsActive,@CreatedDate, @CreatedBy,@ModefiledDate,@ModefiledBy, @ReturnCode OUT";
                var _CategoryID = new SqlParameter("@CategoryID", SqlDbType.VarChar, 2);
                CheckNullParameterStore(_CategoryID, obj.CategoryId);
                var _CategoryName = new SqlParameter("@CategoryName", SqlDbType.NVarChar, 100);
                CheckNullParameterStore(_CategoryName, obj.CategoryName);
                var _Description = new SqlParameter("@Description", SqlDbType.NVarChar, 250);
                CheckNullParameterStore(_Description, obj.Description);
                var _OrderId = new SqlParameter("@OrderID", SqlDbType.Int);
                CheckNullParameterStore(_OrderId, obj.OrderId);
                var _LocationID = new SqlParameter("@LocationID", SqlDbType.VarChar, 20);
                CheckNullParameterStore(_LocationID, obj.LocationId);
                var _IsActive = new SqlParameter("@IsActive", SqlDbType.Bit);
                CheckNullParameterStore(_IsActive, obj.IsActive);
                var CreatedDate = new SqlParameter("@CreatedDate", SqlDbType.DateTime);
                CreatedDate.Value = DateTime.Now;
                var CreatedBy = new SqlParameter("@CreatedBy", SqlDbType.NVarChar, 250);
                CreatedBy.Value = GetCreatedBy(_context);
                var ModefiledDate = new SqlParameter("@ModefiledDate", SqlDbType.DateTime);
                ModefiledDate.Value = DateTime.Now;
                var ModefiledBy = new SqlParameter("@ModefiledBy", SqlDbType.NVarChar, 50);
                ModefiledBy.Value = obj.ModifiedBy;
                var ReturnCode = new SqlParameter("@ReturnCode", SqlDbType.NVarChar, 1000) { Direction = System.Data.ParameterDirection.Output };
                ReturnCode.Value = "";
                var rs = _context.Database.ExecuteSqlCommand(sql, _CategoryID, _CategoryName, _Description, _OrderId, _LocationID, _IsActive, CreatedDate, CreatedBy, ModefiledDate, ModefiledBy, ReturnCode);
                if (ReturnCode.Value.ToString() == "success")
                {
                    return new { Title = "Cập nhật thành công", Error = false };
                }
            }
            catch (Exception ex)
            {
                return new { Title = "Thêm mới không thành công!", Error = true, Object = ex };
            }
            return null;
        }

        [HttpPost]
        public JsonResult GetItem(string Id)
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var _emp = GetEmployeeLogin(_context);
                string sql = "[Common].[SP_WorkFlowStatus_GetItems] @CategoryId";
                var _CategoryId = new SqlParameter("@CategoryId", SqlDbType.VarChar, 20); _CategoryId.Value = Id;
                var rs = _context.WorkFlowStatus.FromSql(sql, _CategoryId).SingleOrDefault();
                return Json(rs);
            }
            catch (Exception ex)
            {
                msg.Title = "Có lỗi khi lấy dữ liệu.";
                return Json(new JMessage() { Error = true, Object = ex, Title = msg.Title });
            }

        }

        //Delete
        [HttpPost]
        public object IsDelete(string id)
        {
            try
            {
                var _DocumentTypeID = new SqlParameter("@CategoryID", id.ToString().ToUpper() ?? (object)DBNull.Value);

                string sql = "[Common].[SP_ESVCM_WorkFlowStatus_Delete] @CategoryID";
                var rs = _context.SQLCOMMANDS.FromSql(sql, _DocumentTypeID).FirstOrDefault();
                if (rs.Error == "")
                {
                    return new { Title = "Xóa thành công.", Error = false };
                }
                else
                {
                    return new { Title = "Đã có bảng khác sử dụng bản ghi này.", Error = true, Object = rs.Error };
                }
            }
            catch (Exception ex)
            {
                return new { Title = "Xóa thành công." };
            }
        }
        [HttpPost]
        public object IsDeletedItems([FromBody] List<string> Items)
        {
            var msg = new JMessage() { Error = false };
            try
            {
                string sql = "[Common].[SP_ESVCM_WorkFlowStatus_IsDeleteItem] @lstDeleteDetail";
                var _lstDeleteDetail = new SqlParameter("@lstDeleteDetail", Items.Count >= 1 ? string.Join(",", Items) : "");
                var __tem = _context.SQLCOMMANDS.FromSql(sql, _lstDeleteDetail).FirstOrDefault();

                if (__tem.Error == "")
                {
                    return new JMessage() { Error = false, Title = "Xóa thành công." };
                }
                else
                {
                    return new JMessage() { Error = true, Title = __tem.Error, Object = __tem };
                }
            }
            catch (Exception ex)
            {
                msg.Title = "Có lỗi khi xóa dữ liệu.";
                return Json(new JMessage() { Error = true, Object = ex, Title = msg.Title });
            }
        }
        //CheckBox
        public class CheckData
        {
            public bool? IsActive { get; set; }
            public int Id { get; set; }
        }
        [HttpPost]
        public object CheckBox([FromBody] List<CheckData> Items)
        {
            try
            {
                string sql = "[dbo].[SP_Common_ChangeStatus] @scheme,@TableName,@RowGuids";
                var _Scheme = new SqlParameter("@scheme ", SqlDbType.NVarChar, 250); CheckNullParameterStore(_Scheme, "WF");
                var _TableName = new SqlParameter("@TableName ", SqlDbType.NVarChar, 50); CheckNullParameterStore(_TableName, "WorkFlowStatus");
                var _CategoryIDs = new SqlParameter("@RowGuids ", SqlDbType.Structured);


                DataTable _ar = new DataTable();
                _ar.Columns.Add("[Id]", typeof(int));
                _ar.Columns.Add("[IsActive]", typeof(bool));
                foreach (var _item in Items)
                {
                    _ar.Rows.Add(
                       _item.Id,
                       _item.IsActive
                       );
                }
                _CategoryIDs.Value = _ar;
                _CategoryIDs.TypeName = "dbo.[DATA_INT_IsActive]";

                var rs = _context.SQLCOMMANDS.FromSql(sql, _Scheme, _TableName, _CategoryIDs).FirstOrDefault();
                if (rs.Error == "")
                {
                    return new { Title = " Cập nhật trạng thái thành công", Error = false };
                }
                else
                {
                    return new { Title = " Cập nhật trạng thái không thành công", Error = true, Object = rs.Error };
                }
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Object = ex, Error = true, Title = "Có lỗi khi xóa." });
            }
        }
    }
}
