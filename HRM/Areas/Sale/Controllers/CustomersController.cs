﻿using FTU.Utils.HelperNet;
using HRM.Controllers;
using HRM.Models;
using HRM.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;
using System.Data;
using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Newtonsoft.Json;
using Grpc.Core;
using DocumentFormat.OpenXml.Drawing;
using ES_MODEL.HR.Custom.Wf;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using DocumentFormat.OpenXml.Drawing.Charts;
using ES_MODEL.Task;
using HRM.Filters;
using ES_MODEL.HR;
using Aspose.Pdf;
using DocumentFormat.OpenXml.Bibliography;
using DocumentFormat.OpenXml.Wordprocessing;
using Microsoft.CodeAnalysis;
using DocumentFormat.OpenXml.VariantTypes;
using static Aspose.Pdf.Operator;
using System.Threading;
using HRM.Models.Custom.Hr;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Math;
using System.Web;
using DocumentFormat.OpenXml.Spreadsheet;
using Stimulsoft.Report.Dashboard;

namespace HRM.Areas.Sale.Controllers
{
    [Authorize]
    public class CustomersController : BaseController
    {
        private readonly HRMDBContext _context;
        private readonly IHostingEnvironment environment;
        private readonly AppSettings _appSettings;
        public CustomersController(HRMDBContext _db, IHostingEnvironment _environment, IOptions<AppSettings> appSettings)
        {
            _context = _db;
            environment = _environment;
            _appSettings = appSettings.Value;
        }
        #region
        [Area("Sale")]
        [ActionAuthorize(EAction.ACCESS)]
        public IActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public object GetPic(Guid? id)
        {
            try
            {
                var Photo = _context.Customers.Where(x => x.CustomerGuid == id).Select(x => x.Image).SingleOrDefault();
                byte[] ImageTemp = Photo;

                FileContentResult hd = null;
                hd = File(ImageTemp, "image/jpeg");
                int gh = ImageTemp.Count();
                if (gh > 0)
                {
                    return hd;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }
        // danh sách mảng kinh doanh
        [Area("Sale")]
        [HttpGet]
        public object GetItemType()
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var _rs = GetEmployeeLogin(_context);
                string sql = "[Sale].[SP_AM_GetItemTypeAll]"
                + " @OrganizationGuid"
                + " ,@Type";
                var _OrganizationGuid = new SqlParameter("@OrganizationGuid", SqlDbType.UniqueIdentifier);
                CheckNullParameterStore(_OrganizationGuid, _rs.OrganizationGuid);
                var _Type = new SqlParameter("Type", SqlDbType.VarChar, 1);
                CheckNullParameterStore(_Type, "1");
                var rs = _context.Object_Combobox
                        .FromSql(sql,
                            _OrganizationGuid,
                            _Type
                            )
                        .ToList();
                return Json(new { Error = false, Title = "Success", Object = rs });
            }
            catch (Exception ex)
            {
                return new JMessage() { Error = true, Title = "Không thành công.", Object = ex };
            }
        }
        [HttpPost]
        public List<TreeView> GetTreeGroups(Guid? id)
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var _emp = GetEmployeeLogin(_context);
                var _OrganizationGuid = new SqlParameter("@OrganizationGuid", SqlDbType.UniqueIdentifier);
                CheckNullParameterStore(_OrganizationGuid, _emp.OrganizationGuid);
                var temp = Convert.ToInt32(id);
                string sql = "exec [Sale].[SP_ESSM_Groups_GetAll] @OrganizationGuid";
                var data = _context.GroupsView.FromSql(sql, _OrganizationGuid).ToList();
                //var data = _context.Groups.OrderBy(x => x.GroupGuid).Where(x => (x.GroupGuid != id && x.ParentId != id  && x.IsActive == true) || temp == 0);
                var dataOrder = GetSubTreeDataAddGroups(data.ToList(), null, new List<TreeView>(), "");
                return dataOrder;
            }
            catch (Exception ex)
            {
                msg.Title = "Có lỗi khi lấy dữ liệu";
                msg.Object = ex.Message;
                msg.Error = true;
            }
            return new List<TreeView>();
        }
        private List<TreeView> GetSubTreeDataAddGroups(List<GroupsView> data, Guid? ParentIDGroup, List<TreeView> lstCategories, string tab)
        {
            tab += "- ";
            var contents = ParentIDGroup == null
                ? data.Where(x => x.ParentId == null && x.IsActive == true).ToList()
                : data.Where(x => x.ParentId == ParentIDGroup && x.IsActive == true).ToList();
            foreach (var item in contents)
            {
                var category = new TreeView
                {
                    GroupGuid = item.GroupGuid,
                    Title = tab + item.GroupName,
                    HasChild = data.Any(x => x.ParentId == item.GroupGuid)

                };
                lstCategories.Add(category);
                if (category.HasChild) GetSubTreeDataAddGroups(data, item.GroupGuid, lstCategories, tab);
            }
            return lstCategories;
        }

        public class JTableCustomer : JTableModel
        {
            public string Keyword { set; get; }
            public string IsBussiness { set; get; }
            public string Status { set; get; }
            public string Type { set; get; }
            public string IsProvince { set; get; }
            public string ProvinceId { set; get; }
            public string TerritoryId { set; get; }
        }
        [HttpPost]
        public object JTable([FromBody] JTableCustomer jTable)
        {
            try
            {
                var _emp = GetEmployeeLogin(_context);
                int intBeginFor = (jTable.CurrentPage - 1) * jTable.Length;
                string sql = "[Sale].[SP_ESSM_Customers_GET_View] " +
                   "@Keyword, " +
                   "@IsBussiness, " +
                   "@Status, " +
                   "@OrganizationGuid, " +
                   "@LoginName, " +
                   "@EmployeeId, " +
                   "@IsProvince, " +
                   "@ProvinceId, " +
                   "@TerritoryId, " +
                   "@Type, " +
                    "@Permisstion, " +
                   "@Skip, " +
                   "@Take," +
                   "@Count out";

                var _Keyword = new SqlParameter("@Keyword", SqlDbType.NVarChar);
                CheckNullParameterStore(_Keyword, jTable.Keyword);
                var _IsBussiness = new SqlParameter("@IsBussiness", SqlDbType.VarChar);
                CheckNullParameterStore(_IsBussiness, jTable.IsBussiness);
                var _Status = new SqlParameter("@Status", SqlDbType.NVarChar);
                CheckNullParameterStore(_Status, jTable.Status);

                var _OrganizationGuid = new SqlParameter("@OrganizationGuid", SqlDbType.UniqueIdentifier);
                CheckNullParameterStore(_OrganizationGuid, _emp.OrganizationGuid);

                var _LoginName = new SqlParameter("@LoginName", SqlDbType.VarChar, 20); CheckNullParameterStore(_LoginName, _emp.LoginName);

                var _EmployeeId = new SqlParameter("@EmployeeId", SqlDbType.VarChar, 100); CheckNullParameterStore(_EmployeeId, _emp.EmployeeId);

                var _IsProvince = new SqlParameter("@IsProvince", SqlDbType.VarChar, 1); CheckNullParameterStore(_IsProvince, jTable.IsProvince);

                var _ProvinceId = new SqlParameter("@ProvinceId", SqlDbType.VarChar, 20); CheckNullParameterStore(_ProvinceId, jTable.ProvinceId);

                var _TerritoryId = new SqlParameter("@TerritoryId", SqlDbType.VarChar); CheckNullParameterStore(_TerritoryId, jTable.TerritoryId);

                var _Type = new SqlParameter("@Type", SqlDbType.VarChar, 1); CheckNullParameterStore(_Type, jTable.Type);

                var Permisstion = "0";
                var ListPermission = WebContext.CheckPermission(ControllerContext.RouteData.Values["controller"].ToString());
                var per = ListPermission.Contains(EAction.FULLCONTROL.ToString());
                if (per == true)
                {
                    Permisstion = "1";
                }
                var _Permisstion = new SqlParameter("@Permisstion", SqlDbType.VarChar); CheckNullParameterStore(_Permisstion, Permisstion);
                var _Skip = new SqlParameter("@Skip", SqlDbType.Int);
                CheckNullParameterStore(_Skip, intBeginFor);

                var _Take = new SqlParameter("@Take", SqlDbType.Int);
                CheckNullParameterStore(_Take, jTable.Length);

                var _output = new SqlParameter("@Count", SqlDbType.Int) { Direction = System.Data.ParameterDirection.Output };

                var data = _context.CustomerView.FromSql(
                            sql,
                            _Keyword,
                            _IsBussiness,
                            _Status,
                            _OrganizationGuid,
                            _LoginName,
                            _EmployeeId,
                            _IsProvince,
                            _ProvinceId,
                            _TerritoryId,
                            _Type,
                            _Permisstion,
                            _Skip,
                            _Take,
                            _output
                ).ToList();
                var jdata = JTableHelper.JObjectTable(data, jTable.Draw, (int)_output.Value);
                return Json(jdata);
            }
            catch (Exception ex)
            {
                return new JMessage() { Error = true, Title = ex.Message, Object = ex };
            }
        }
        [HttpPost]
        public JsonResult GetItem(Guid Id)
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var _emp = GetEmployeeLogin(_context);
                string sql = "[Sale].[SP_ESHR_Customers_GetItem] @CustomerGuid";
                var CustomerGuid = SqlPara("@CustomerGuid", Id, SqlDbType.UniqueIdentifier);
                var rs = _context.Customers.FromSql(sql, CustomerGuid).FirstOrDefault();
                return Json(rs);
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = ex.Message });
            }
        }
        
        [HttpPost]
        public JsonResult Insert()
        {

            var msg = new JMessage() { Error = false };
            try
            {

                var _emp = GetEmployeeLogin(_context);
                var files = Request.Form.Files.ToList();
                var submit = Request.Form["submit"];
                Customers obj = JsonConvert.DeserializeObject<Customers>(submit);
                obj.OrganizationGuid = _emp.OrganizationGuid;
                #endregion
                if (obj.DistrictId == "") { obj.DistrictId = null; }
                if (obj.ProvinceId == "") { obj.ProvinceId = null; }
                obj.CreatedBy =GetCreatedBy(_context);
                obj.CreatedDate = DateTime.Now;
                obj.ModifiedBy = GetCreatedBy(_context);
                obj.ModifiedDate = DateTime.Now;
                if (obj.BirthDate.HasValue)
                {
                    obj.BirthDate = obj.BirthDate.Value.ToLocalTime();
                }
                else
                {
                    obj.BirthDate = null;
                }
                if (obj.IssueIddate != null)
                {
                    obj.IssueIddate = Convert.ToDateTime(obj.IssueIddate).ToLocalTime();
                }
                else
                {
                    obj.IssueIddate = null;
                }
                if (obj.StartDatePP != null)
                {
                    obj.StartDatePP = Convert.ToDateTime(obj.StartDatePP).ToLocalTime();
                }
                else
                {
                    obj.StartDatePP = null;
                }
                obj.IsLocked = false;
                if (files.Count > 0)
                {
                    foreach (var file in files)
                    {
                        var SortOrder = file.Name.Split('#')[0];
                        if (SortOrder == "1")
                        {
                            var stream = file.OpenReadStream();
                            byte[] byteArr = Utilities.StreamToByteArray(stream);

                            obj.Image = byteArr;
                        }
                    }
                }

                string sql = "[Sale].[SP_ESSM_Insert_Customers]" +
               "@Database" +
               ",@DBName" +
               ",@ID" +
               ",@CustomerGuid" +
               ",@CustomerID" +
               ",@CustomerName" +
               ",@LeadName" +
               ",@OrganizationGuid" +
               ",@ParentID" +
               ",@DepartmentGuid" +
               ",@DepartmentName" +
               ",@IsBussiness" +
               ",@Representative" +
               ",@IDNumber" +
               ",@BirthPlace" +
               ",@IssueIDDate" +
               ",@IssueIDBy" +
               ",@JobTitleID" +
               ",@JobTitle" +
               ",@CountryID" +
               ",@CountryName" +
               ",@TerritoryID" +
               ",@TerritoryName" +
               ",@ProvinceID" +
               ",@ProvinceName" +
               ",@DistrictID" +
               ",@DistrictIName" +
               ",@Wards" +
               ",@Address" +
               ",@PermanentAddress" +
               ",@WorkEmail" +
               ",@WorkPhone" +
               ",@WorkMobile" +
               ",@WorkFax" +
               ",@Website" +
               ",@TaxCode" +
               ",@WorkPlace" +
               ",@ActivityFieldID" +
               ",@ActivityFieldName" +
               ",@BirthDate" +
               ",@Capital" +
               ",@StaffNumber" +
               ",@SourceType" +
               ",@Image" +
               ",@ImageUrl" +
               ",@Gender" +
               ",@MaritalStatus" +
               ",@HaveChildren" +
               ",@SuccessRate" +
               ",@RateByLevel" +
               ",@RateByScore" +
               ",@Status" +
               ",@IsActive" +
               ",@IsLocked" +
               ",@CompaignGuid" +
               ",@CompaignID" +
               ",@CompaignName" +
               ",@ResourceID" +
               ",@ResourceName" +
               ",@ResourceDetail" +
               ",@BusinessTypeID" +
               ",@BusinessTypeName" +
               ",@SortTitle" +
               ",@Description" +
               ",@CategoryID" +
               ",@AmountSalesForecas" +
               ",@AmountSales" +
               ",@StartDate" +
               ",@EndDate" +
               ",@UTMSource" +
               ",@UTMCampaign" +
               ",@UTMMedium" +
               ",@UTMContent" +
               ",@UTMTerm" +
               ",@LinkForm" +
               ",@DebtDay" +
               ",@CustomerAttributes" +
               ",@DateOfApplication" +
               ",@Feedback" +
               ",@Exchange" +
               ",@GroupGuid" +
               ",@CreatedDate" +
               ",@CreatedBy" +
               ",@ModifiedDate" +
               ",@ModifiedBy" +
               ",@IsLeads" +
               ",@IsProvider" +
                 ",@IsCustomer" +
               ",@OrganizationNameInvoices" +
               ",@AddressInvoices" +
               ",@StartDatePP" +
               ",@Monitor" +
               ",@Method" +
               ",@Type" +
               ",@PolicyGuid" +
               ",@PolicyID" +
               ",@PolicyName" +
               ",@PolicyGroupGuid" +
               ",@PolicyGroupID" +
               ",@PolicyGroupName" +
               ",@Note" +
               ",@IsProvince" +
               ",@Error";
                obj.CustomerGuid = Guid.NewGuid();
                var _Database = new SqlParameter("@Database", SqlDbType.NVarChar); _Database.Value = "SM";
                var _DBName = new SqlParameter("@DBName", SqlDbType.NVarChar); _DBName.Value = _appSettings.DBName_HR;
                var _ID = new SqlParameter("@ID", SqlDbType.Int); CheckNullParameterStore(_ID, null);
                var _CustomerGuid = new SqlParameter("@CustomerGuid", SqlDbType.UniqueIdentifier); CheckNullParameterStore(_CustomerGuid, obj.CustomerGuid);
                var _CustomerID = new SqlParameter("@CustomerID", SqlDbType.NVarChar, 20); CheckNullParameterStore(_CustomerID, obj.CustomerId);
                var _CustomerName = new SqlParameter("@CustomerName", SqlDbType.NVarChar, 255); CheckNullParameterStore(_CustomerName, obj.CustomerName);
                var _LeadName = new SqlParameter("@LeadName", SqlDbType.NVarChar, 255); CheckNullParameterStore(_LeadName, obj.LeadName);
                var _OrganizationGuid = SqlPara("@OrganizationGuid", obj.OrganizationGuid,SqlDbType.UniqueIdentifier);
                var _OrganizationName = SqlPara("@OrganizationName", _emp.OrganizationName, SqlDbType.NVarChar);
                var ParentID = new SqlParameter("@ParentID", SqlDbType.UniqueIdentifier); CheckNullParameterStore(ParentID, obj.ParentId);
                var _DepartmentGuid = new SqlParameter("@DepartmentGuid", SqlDbType.UniqueIdentifier); CheckNullParameterStore(_DepartmentGuid, obj.DepartmentGuid);
                var _DepartmentName = new SqlParameter("@DepartmentName", SqlDbType.NVarChar, 255); CheckNullParameterStore(_DepartmentName, obj.DepartmentName);
                var _IsBussiness = new SqlParameter("@IsBussiness", SqlDbType.Bit); CheckNullParameterStore(_IsBussiness, obj.IsBussiness);
                var _Representative = new SqlParameter("@Representative", SqlDbType.NVarChar, 50); CheckNullParameterStore(_Representative, obj.Representative);
                var _IDNumber = new SqlParameter("@IDNumber", SqlDbType.VarChar, 50); CheckNullParameterStore(_IDNumber, obj.Idnumber);
                var _BirthPlace = new SqlParameter("@BirthPlace", SqlDbType.NVarChar, 255); CheckNullParameterStore(_BirthPlace, obj.BirthPlace);
                var _IssueIDDate = new SqlParameter("@IssueIDDate", SqlDbType.DateTime); CheckNullParameterStore(_IssueIDDate, obj.IssueIddate);
                var _IssueIDBy = new SqlParameter("@IssueIDBy", SqlDbType.NVarChar, 50); CheckNullParameterStore(_IssueIDBy, obj.IssueIdby);
                var _JobTitleID = new SqlParameter("@JobTitleID", SqlDbType.VarChar, 20); CheckNullParameterStore(_JobTitleID, obj.JobTitleId);
                var _JobTitle = new SqlParameter("@JobTitle", SqlDbType.NVarChar, 255); CheckNullParameterStore(_JobTitle, obj.JobTitle);
                var _CountryID = new SqlParameter("@CountryID", SqlDbType.VarChar, 20); CheckNullParameterStore(_CountryID, obj.CountryId);
                var _CountryName = new SqlParameter("@CountryName", SqlDbType.NVarChar, 50); CheckNullParameterStore(_CountryName, obj.CountryName);
                var _TerritoryID = new SqlParameter("@TerritoryID", SqlDbType.Int); CheckNullParameterStore(_TerritoryID, obj.TerritoryId);
                var _TerritoryName = new SqlParameter("@TerritoryName", SqlDbType.NVarChar, 50); CheckNullParameterStore(_TerritoryName, obj.TerritoryName);
                var _ProvinceID = new SqlParameter("@ProvinceID", SqlDbType.VarChar, 20); CheckNullParameterStore(_ProvinceID, obj.ProvinceId);
                var _ProvinceName = new SqlParameter("@ProvinceName", SqlDbType.NVarChar, 50); CheckNullParameterStore(_ProvinceName, obj.ProvinceName);
                var _DistrictID = new SqlParameter("@DistrictID", SqlDbType.VarChar, 20); CheckNullParameterStore(_DistrictID, obj.DistrictId);
                var _DistrictIName = new SqlParameter("@DistrictIName", SqlDbType.NVarChar, 50); CheckNullParameterStore(_DistrictIName, obj.DistrictIname);
                var _Wards = new SqlParameter("@Wards", SqlDbType.VarChar, 20); CheckNullParameterStore(_Wards, obj.Wards);
                var _Address = new SqlParameter("@Address", SqlDbType.NVarChar, 500); CheckNullParameterStore(_Address, obj.Address);
                var _PermanentAddress = new SqlParameter("@PermanentAddress", SqlDbType.NVarChar, 200); CheckNullParameterStore(_PermanentAddress, obj.PermanentAddress);
                var _WorkEmail = new SqlParameter("@WorkEmail", SqlDbType.NVarChar, 500); CheckNullParameterStore(_WorkEmail, obj.WorkEmail);
                var _WorkPhone = new SqlParameter("@WorkPhone", SqlDbType.NVarChar, 20); CheckNullParameterStore(_WorkPhone, obj.WorkPhone);
                var _WorkMobile = new SqlParameter("@WorkMobile", SqlDbType.NVarChar, 20); CheckNullParameterStore(_WorkMobile, obj.WorkMobile);
                var _WorkFax = new SqlParameter("@WorkFax", SqlDbType.NVarChar, 20); CheckNullParameterStore(_WorkFax, obj.WorkFax);
                var _Website = new SqlParameter("@Website", SqlDbType.NVarChar, 50); CheckNullParameterStore(_Website, obj.Website);
                var _TaxCode = new SqlParameter("@TaxCode", SqlDbType.VarChar, 20); CheckNullParameterStore(_TaxCode, obj.TaxCode);
                var _WorkPlace = new SqlParameter("@WorkPlace", SqlDbType.NVarChar, 200); CheckNullParameterStore(_WorkPlace, obj.WorkPlace);
                var _ActivityFieldID = new SqlParameter("@ActivityFieldID", SqlDbType.Int); CheckNullParameterStore(_ActivityFieldID, obj.ActivityFieldId);
                var _ActivityFieldName = new SqlParameter("@ActivityFieldName", SqlDbType.NVarChar); CheckNullParameterStore(_ActivityFieldName, obj.ActivityFieldName);
                var _BirthDate = new SqlParameter("@BirthDate", SqlDbType.DateTime); CheckNullParameterStore(_BirthDate, obj.BirthDate);
                var _Capital = new SqlParameter("@Capital", SqlDbType.Float); CheckNullParameterStore(_Capital, Convert.ToDecimal(obj.Capital));
                var _StaffNumber = new SqlParameter("@StaffNumber", SqlDbType.Int); CheckNullParameterStore(_StaffNumber, obj.StaffNumber);
                var _SourceType = new SqlParameter("@SourceType", SqlDbType.VarChar, 1); CheckNullParameterStore(_SourceType, obj.SourceType);
                var _Image = new SqlParameter("@Image", SqlDbType.VarBinary); CheckNullParameterStore(_Image, obj.Image);
                var _ImageUrl = new SqlParameter("@ImageUrl", SqlDbType.NVarChar, 250); CheckNullParameterStore(_ImageUrl, obj.ImageUrl);
                var _Gender = new SqlParameter("@Gender", SqlDbType.VarChar, 1); CheckNullParameterStore(_Gender, obj.Gender);
                var _MaritalStatus = new SqlParameter("@MaritalStatus", SqlDbType.VarChar, 1); CheckNullParameterStore(_MaritalStatus, obj.MaritalStatus);
                var _HaveChildren = new SqlParameter("@HaveChildren", SqlDbType.Bit); CheckNullParameterStore(_HaveChildren, obj.HaveChildren);
                var _SuccessRate = new SqlParameter("@SuccessRate", SqlDbType.TinyInt); CheckNullParameterStore(_SuccessRate, Convert.ToByte(obj.SuccessRate));
                var _RateByLevel = new SqlParameter("@RateByLevel", SqlDbType.VarChar, 1); CheckNullParameterStore(_RateByLevel, obj.RateByLevel);
                var _RateByScore = new SqlParameter("@RateByScore", SqlDbType.Int); CheckNullParameterStore(_RateByScore, obj.RateByScore);
                var _Status = new SqlParameter("@Status", SqlDbType.VarChar, 1); CheckNullParameterStore(_Status, obj.Status);
                var _IsActive = new SqlParameter("@IsActive", SqlDbType.VarChar, 1); CheckNullParameterStore(_IsActive, obj.IsActive);
                var _IsLocked = new SqlParameter("@IsLocked", SqlDbType.Bit); CheckNullParameterStore(_IsLocked, obj.IsLocked);
                var _CompaignGuid = new SqlParameter("@CompaignGuid", SqlDbType.UniqueIdentifier); CheckNullParameterStore(_CompaignGuid, obj.CompaignGuid);
                var _CompaignID = new SqlParameter("@CompaignID", SqlDbType.VarChar, 20); CheckNullParameterStore(_CompaignID, obj.CompaignId);
                var _CompaignName = new SqlParameter("@CompaignName", SqlDbType.NVarChar, 50); CheckNullParameterStore(_CompaignName, obj.CompaignName);
                var _ResourceID = new SqlParameter("@ResourceID", SqlDbType.Int); CheckNullParameterStore(_ResourceID, obj.ResourceId);
                var _ResourceName = new SqlParameter("@ResourceName", SqlDbType.NVarChar, 50); CheckNullParameterStore(_ResourceName, obj.ResourceName);
                var _ResourceDetail = new SqlParameter("@ResourceDetail", SqlDbType.NVarChar, 255); CheckNullParameterStore(_ResourceDetail, obj.ResourceDetail);
                var _BusinessTypeID = new SqlParameter("@BusinessTypeID", SqlDbType.Int); CheckNullParameterStore(_BusinessTypeID, obj.BusinessTypeId);
                var _BusinessTypeName = new SqlParameter("@BusinessTypeName", SqlDbType.NVarChar, 50); CheckNullParameterStore(_BusinessTypeName, obj.BusinessTypeName);
                var _SortTitle = new SqlParameter("@SortTitle", SqlDbType.NVarChar, 255); CheckNullParameterStore(_SortTitle, obj.SortTitle);
                var _Description = new SqlParameter("@Description", SqlDbType.NVarChar); CheckNullParameterStore(_Description, obj.Description);
                var _CategoryID = new SqlParameter("@CategoryID", SqlDbType.Int); CheckNullParameterStore(_CategoryID, obj.CategoryId);
                var _AmountSalesForecas = new SqlParameter("@AmountSalesForecas", SqlDbType.Decimal); CheckNullParameterStore(_AmountSalesForecas, obj.AmountSalesForecas);
                var _AmountSales = new SqlParameter("@AmountSales", SqlDbType.Decimal); CheckNullParameterStore(_AmountSales, obj.AmountSales);
                var _StartDate = new SqlParameter("@StartDate", SqlDbType.DateTime); CheckNullParameterStore(_StartDate, obj.StartDate);
                var _EndDate = new SqlParameter("@EndDate", SqlDbType.DateTime); CheckNullParameterStore(_EndDate, obj.EndDate);
                var _UTMSource = new SqlParameter("@UTMSource", SqlDbType.NVarChar); CheckNullParameterStore(_UTMSource, obj.Utmsource);
                var _UTMCampaign = new SqlParameter("@UTMCampaign", SqlDbType.NVarChar); CheckNullParameterStore(_UTMCampaign, obj.Utmcampaign);
                var _UTMMedium = new SqlParameter("@UTMMedium", SqlDbType.NVarChar); CheckNullParameterStore(_UTMMedium, obj.Utmmedium);
                var _UTMContent = new SqlParameter("@UTMContent", SqlDbType.NVarChar); CheckNullParameterStore(_UTMContent, obj.Utmcontent);
                var _UTMTerm = new SqlParameter("@UTMTerm", SqlDbType.NVarChar); CheckNullParameterStore(_UTMTerm, obj.Utmterm);
                var _LinkForm = new SqlParameter("@LinkForm", SqlDbType.NVarChar); CheckNullParameterStore(_LinkForm, obj.LinkForm);
                var _DebtDay = new SqlParameter("@DebtDay", SqlDbType.Int); CheckNullParameterStore(_DebtDay, obj.DebtDay);
                var _CustomerAttributes = new SqlParameter("@CustomerAttributes", SqlDbType.VarChar, 1); CheckNullParameterStore(_CustomerAttributes, null);
                var _DateOfApplication = new SqlParameter("@DateOfApplication", SqlDbType.DateTime); CheckNullParameterStore(_DateOfApplication, null);
                var _Feedback = new SqlParameter("@Feedback", SqlDbType.NVarChar); CheckNullParameterStore(_Feedback, null);
                var _Exchange = new SqlParameter("@Exchange", SqlDbType.NVarChar); CheckNullParameterStore(_Exchange, null);
                var _GroupGuid = new SqlParameter("@GroupGuid", SqlDbType.UniqueIdentifier); CheckNullParameterStore(_GroupGuid, obj.PolicyGroupGuid);
                var _CreatedDate = new SqlParameter("@CreatedDate", SqlDbType.DateTime); CheckNullParameterStore(_CreatedDate, obj.CreatedDate);
                var _ModifiedDate = new SqlParameter("@ModifiedDate", SqlDbType.DateTime); CheckNullParameterStore(_ModifiedDate, obj.ModifiedDate);
                var _CreatedBy = new SqlParameter("@CreatedBy", SqlDbType.NVarChar, 50); CheckNullParameterStore(_CreatedBy, obj.CreatedBy);
                var _ModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.NVarChar, 50); CheckNullParameterStore(_ModifiedBy, obj.ModifiedBy);
                var _IsLeads = new SqlParameter("@IsLeads", SqlDbType.Bit); CheckNullParameterStore(_IsLeads, obj.IsLeads);
                var _IsProvider = new SqlParameter("@IsProvider", SqlDbType.Bit); CheckNullParameterStore(_IsProvider, obj.IsProvider);
                var IsCustomer = new SqlParameter("@IsCustomer", SqlDbType.Bit); CheckNullParameterStore(IsCustomer, obj.IsCustomer);

                var _OrganizationNameInvoices = new SqlParameter("@OrganizationNameInvoices", SqlDbType.NVarChar, 255); CheckNullParameterStore(_OrganizationNameInvoices, obj.OrganizationNameInvoices);
                var _AddressInvoices = new SqlParameter("@AddressInvoices", SqlDbType.NVarChar, 500); CheckNullParameterStore(_AddressInvoices, obj.AddressInvoices);
                var _StartDatePP = new SqlParameter("@StartDatePP", SqlDbType.DateTime); CheckNullParameterStore(_StartDatePP, obj.StartDatePP);
                var _Monitor = new SqlParameter("@Monitor", SqlDbType.VarChar, 1); CheckNullParameterStore(_Monitor, obj.Monitor);
                var _Method = new SqlParameter("@Method", SqlDbType.VarChar, 1); CheckNullParameterStore(_Method, obj.Method);
                var _Type = new SqlParameter("@Type", SqlDbType.VarChar, 20); CheckNullParameterStore(_Type, obj.Type);
                var _PolicyGuid = new SqlParameter("@PolicyGuid", SqlDbType.UniqueIdentifier); CheckNullParameterStore(_PolicyGuid, obj.PolicyGuid);
                var _PolicyID = new SqlParameter("@PolicyID", SqlDbType.VarChar, 20); CheckNullParameterStore(_PolicyID, obj.PolicyId);
                var _PolicyName = new SqlParameter("@PolicyName", SqlDbType.NVarChar, 255); CheckNullParameterStore(_PolicyName, obj.PolicyName);
                var _PolicyGroupGuid = new SqlParameter("@PolicyGroupGuid", SqlDbType.UniqueIdentifier); CheckNullParameterStore(_PolicyGroupGuid, obj.PolicyGroupGuid);
                var _PolicyGroupID = new SqlParameter("@PolicyGroupID", SqlDbType.VarChar, 20); CheckNullParameterStore(_PolicyGroupID, obj.PolicyGroupId);
                var _PolicyGroupName = new SqlParameter("@PolicyGroupName", SqlDbType.NVarChar, 255); CheckNullParameterStore(_PolicyGroupName, obj.PolicyGroupName);
                var _Note = new SqlParameter("@Note", SqlDbType.NVarChar, 255); CheckNullParameterStore(_Note, obj.Note);
                var _IsProvince = new SqlParameter("@IsProvince", SqlDbType.VarChar, 1); CheckNullParameterStore(_IsProvince, obj.IsProvince);

                var _Error = new SqlParameter("@Error", SqlDbType.NVarChar, 500) { Direction = System.Data.ParameterDirection.Output };

                var _rs = _context.SQLCOMMANDS.FromSql(sql, _Database, _DBName, _ID, _CustomerGuid, _CustomerID, _CustomerName, _LeadName, _OrganizationGuid, ParentID, _DepartmentGuid, _DepartmentName, _IsBussiness, _Representative, _IDNumber, _BirthPlace, _IssueIDDate, _IssueIDBy, _JobTitleID, _JobTitle, _CountryID, _CountryName, _TerritoryID, _TerritoryName, _ProvinceID, _ProvinceName, _DistrictID, _DistrictIName, _Wards, _Address, _PermanentAddress, _WorkEmail, _WorkPhone, _WorkMobile, _WorkFax, _Website, _TaxCode, _WorkPlace, _ActivityFieldID, _ActivityFieldName, _BirthDate, _Capital, _StaffNumber, _SourceType, _Image, _ImageUrl, _Gender, _MaritalStatus, _HaveChildren, _SuccessRate, _RateByLevel, _RateByScore, _Status, _IsActive, _IsLocked, _CompaignGuid, _CompaignID, _CompaignName, _ResourceID, _ResourceName, _ResourceDetail, _BusinessTypeID, _BusinessTypeName, _SortTitle, _Description, _CategoryID, _AmountSalesForecas, _AmountSales, _StartDate, _EndDate, _UTMSource, _UTMCampaign, _UTMMedium, _UTMContent, _UTMTerm, _LinkForm, _DebtDay, _CustomerAttributes, _DateOfApplication, _Feedback, _Exchange, _GroupGuid, _CreatedDate, _CreatedBy, _ModifiedDate, _ModifiedBy, _IsLeads, _IsProvider, IsCustomer, _OrganizationNameInvoices, _AddressInvoices, _StartDatePP, _Monitor, _Method, _Type, _PolicyGuid, _PolicyID, _PolicyName, _PolicyGroupGuid, _PolicyGroupID, _PolicyGroupName, _Note, _IsProvince, _Error).FirstOrDefault();
                if (_rs.Error != "")
                {
                    msg.Error = true;
                    return Json(new JMessage() { Error = true, Title = _rs.Error, Object = _rs.Error });
                }
                if (files.Count > 0)
                {
                    string sqlAttach = "[dbo].[SP_Insert_Attachment] @Database, @Scheme,@TableName,@TableAttachment";
                    var Database = new SqlParameter("@Database", SqlDbType.NVarChar, 50); CheckNullParameterStore(Database, _appSettings.DBName_HR);
                    var Scheme = new SqlParameter("@Scheme", SqlDbType.NVarChar, 50); CheckNullParameterStore(Scheme, "Sale");
                    var TableName = new SqlParameter("@TableName ", SqlDbType.NVarChar, 50); CheckNullParameterStore(TableName, "CustomerAttachments");
                    var _detailAttach = new SqlParameter("@TableAttachment", SqlDbType.Structured);
                    /// table detail
                    var _tematach = new System.Data.DataTable();
                    _tematach.Columns.Add("AttachmentGuid");
                    _tematach.Columns.Add("ParentGuid");
                    _tematach.Columns.Add("RecordGuid");
                    _tematach.Columns.Add("OrganizationGuid");
                    _tematach.Columns.Add("DepartmentGuid");
                    _tematach.Columns.Add("ModuleID");
                    _tematach.Columns.Add("Title");
                    _tematach.Columns.Add("FileName");
                    _tematach.Columns.Add("Attachment", typeof(byte[]));
                    _tematach.Columns.Add("FileExtension");
                    _tematach.Columns.Add("FileSize");
                    _tematach.Columns.Add("UrlExternal");
                    _tematach.Columns.Add("IsPublic");
                    _tematach.Columns.Add("PrivateLevel");
                    _tematach.Columns.Add("IsFolder");
                    _tematach.Columns.Add("IsHidden");
                    _tematach.Columns.Add("IsReadonly");
                    _tematach.Columns.Add("IsSync");
                    _tematach.Columns.Add("SyncTime");
                    _tematach.Columns.Add("CloudPath");
                    _tematach.Columns.Add("CreatedDate", typeof(DateTime));
                    _tematach.Columns.Add("ModifiedDate", typeof(DateTime));
                    _tematach.Columns.Add("CreatedBy");
                    _tematach.Columns.Add("ModifiedBy");
                    foreach (var file in files)
                    {
                        var SortOrder = file.Name.Split('#')[0];
                        if (SortOrder == "2")
                        {
                            var stream = file.OpenReadStream();
                            byte[] byteArr = Utilities.StreamToByteArray(stream);

                            Hrattachments _attach = new Hrattachments();
                            _attach.AttachmentGuid = Guid.NewGuid();
                            //var RecordGuid = detail.Select(x => new { x.SortOrder, x.EmployeeGuid }).Where(x => x.SortOrder.ToString() == SortOrder).Take(1).SingleOrDefault();
                            _attach.RecordGuid = obj.CustomerGuid;
                            _attach.OrganizationGuid = _emp.OrganizationGuid;
                            _attach.Title = file.Name.Split('#')[1];
                            _attach.FileName = file.FileName;
                            _attach.CreatedDate = DateTime.Now;
                            _attach.CreatedBy = GetCreatedBy(_context);
                            _attach.ModifiedDate = DateTime.Now;
                            _attach.ModifiedBy = GetModifiedBy(_context);
                            _attach.Attachment = byteArr;
                            _attach.ModuleId = "Quotations";
                            _attach.FileSize = byteArr.Length;
                            _attach.FileExtension = System.IO.Path.GetExtension(file.FileName);

                            _tematach.Rows.Add(_attach.AttachmentGuid,
                                _attach.ParentGuid,
                                _attach.RecordGuid,
                                _attach.OrganizationGuid,
                                _attach.DepartmentGuid,
                                _attach.ModuleId,
                                _attach.Title,
                                _attach.FileName,
                                _attach.Attachment,
                                _attach.FileExtension,
                                _attach.FileSize,
                                _attach.UrlExternal,
                                _attach.IsPublic,
                                _attach.PrivateLevel,
                                _attach.IsFolder,
                                _attach.IsHidden,
                                _attach.IsReadonly,
                                _attach.IsSync,
                                DBNull.Value,
                                _attach.CloudPath,
                                DBNull.Value,
                                DBNull.Value,
                                _attach.CreatedBy,
                                _attach.ModifiedBy
                                );
                        }
                    }
                    _detailAttach.Value = _tematach;
                    _detailAttach.TypeName = "[dbo].[Attachment]";
                    var __temattach = _context.SQLCOMMANDS.FromSql(sqlAttach, Database, Scheme, TableName, _detailAttach).FirstOrDefault();
                    if (__temattach.Error != "")
                    {
                        if (msg.Title == null)
                        {
                            msg.Title = "đính kèm";
                        }
                        else
                        {
                            msg.Title = msg.Title + ",đính kèm";
                        }
                    }

                }
                msg.Title = "Thêm mới thành công";
                msg.Object = obj.CustomerGuid;

            }
            catch (Exception ex)
            {
                msg.Error = true;
                msg.Object = ex;
                msg.Title = ex.Message;
                return Json(msg);
            }
            return Json(msg);
        }

        [HttpPost]
        //  // [ActionAuthorize(EAction.EDIT)]
        public JsonResult Update()
        {

            var msg = new JMessage() { Error = false };
            try
            {

                var _emp = GetEmployeeLogin(_context);
                var files = Request.Form.Files.ToList();
                var submit = Request.Form["submit"];
                var DeleteAttach = Request.Form["DeleteAttach"];
                Customers obj = JsonConvert.DeserializeObject<Customers>(submit);
                List<Guid> deleteFileattach = JsonConvert.DeserializeObject<List<Guid>>(DeleteAttach);             
                if (obj.DistrictId == "") { obj.DistrictId = null; }
                if (obj.ProvinceId == "") { obj.ProvinceId = null; }
                obj.ModifiedBy =GetCreatedBy(_context);
                obj.ModifiedDate = DateTime.Now;
                if (obj.BirthDate.HasValue)
                {
                    obj.BirthDate = obj.BirthDate.Value.ToLocalTime();
                }
                else
                {
                    obj.BirthDate = null;
                }
                if (obj.IssueIddate != null)
                {
                    obj.IssueIddate = Convert.ToDateTime(obj.IssueIddate).ToLocalTime();
                }
                else
                {
                    obj.IssueIddate = null;
                }
                if (obj.StartDatePP != null)
                {
                    obj.StartDatePP = Convert.ToDateTime(obj.StartDatePP).ToLocalTime();
                }
                else
                {
                    obj.StartDatePP = null;
                }
                obj.IsLocked = false;
                if (files.Count > 0)
                {
                    foreach (var file in files)
                    {
                        var SortOrder = file.Name.Split('#')[0];
                        if (SortOrder == "1")
                        {
                            var stream = file.OpenReadStream();
                            byte[] byteArr = Utilities.StreamToByteArray(stream);

                            obj.Image = byteArr;
                        }
                    }
                }
                string sql = "[Sale].[SP_ESSM_Update_Customers]" +
               "@Database" +
               ",@DBName" +
               ",@ID" +
               ",@CustomerGuid" +
               ",@CustomerID" +
               ",@CustomerName" +
               ",@LeadName" +
               ",@OrganizationGuid" +
               ",@ParentID" +
               ",@DepartmentGuid" +
               ",@DepartmentName" +
               ",@IsBussiness" +
               ",@Representative" +
               ",@IDNumber" +
               ",@BirthPlace" +
               ",@IssueIDDate" +
               ",@IssueIDBy" +
               ",@JobTitleID" +
               ",@JobTitle" +
               ",@CountryID" +
               ",@CountryName" +
               ",@TerritoryID" +
               ",@TerritoryName" +
               ",@ProvinceID" +
               ",@ProvinceName" +
               ",@DistrictID" +
               ",@DistrictIName" +
               ",@Wards" +
               ",@Address" +
               ",@PermanentAddress" +
               ",@WorkEmail" +
               ",@WorkPhone" +
               ",@WorkMobile" +
               ",@WorkFax" +
               ",@Website" +
               ",@TaxCode" +
               ",@WorkPlace" +
               ",@ActivityFieldID" +
               ",@ActivityFieldName" +
               ",@BirthDate" +
               ",@Capital" +
               ",@StaffNumber" +
               ",@SourceType" +
               ",@Image" +
               ",@ImageUrl" +
               ",@Gender" +
               ",@MaritalStatus" +
               ",@HaveChildren" +
               ",@SuccessRate" +
               ",@RateByLevel" +
               ",@RateByScore" +
               ",@Status" +
               ",@IsActive" +
               ",@IsLocked" +
               ",@CompaignGuid" +
               ",@CompaignID" +
               ",@CompaignName" +
               ",@ResourceID" +
               ",@ResourceName" +
               ",@ResourceDetail" +
               ",@BusinessTypeID" +
               ",@BusinessTypeName" +
               ",@SortTitle" +
               ",@Description" +
               ",@CategoryID" +
               ",@AmountSalesForecas" +
               ",@AmountSales" +
               ",@StartDate" +
               ",@EndDate" +
               ",@UTMSource" +
               ",@UTMCampaign" +
               ",@UTMMedium" +
               ",@UTMContent" +
               ",@UTMTerm" +
               ",@LinkForm" +
               ",@DebtDay" +
               ",@CustomerAttributes" +
               ",@DateOfApplication" +
               ",@Feedback" +
               ",@Exchange" +
               ",@GroupGuid" +
               ",@CreatedDate" +
               ",@CreatedBy" +
               ",@ModifiedDate" +
               ",@ModifiedBy" +
               ",@IsLeads" +
               ",@IsProvider" +
                ",@IsCustomer" +
               ",@OrganizationNameInvoices" +
               ",@AddressInvoices" +
               ",@StartDatePP" +
               ",@Monitor" +
               ",@Method" +
               ",@Type" +
               ",@PolicyGuid" +
               ",@PolicyID" +
               ",@PolicyName" +
               ",@PolicyGroupGuid" +
               ",@PolicyGroupID" +
               ",@PolicyGroupName" +
               ",@Note" +
               ",@IsProvince" +
               ",@Error";
                var _Database = new SqlParameter("@Database", SqlDbType.NVarChar); _Database.Value = "SM";
                var _DBName = new SqlParameter("@DBName", SqlDbType.NVarChar); _DBName.Value = _appSettings.DBName_HR;
                var _ID = new SqlParameter("@ID", SqlDbType.Int); CheckNullParameterStore(_ID, null);
                var _CustomerGuid = new SqlParameter("@CustomerGuid", SqlDbType.UniqueIdentifier); CheckNullParameterStore(_CustomerGuid, obj.CustomerGuid);
                var _CustomerID = new SqlParameter("@CustomerID", SqlDbType.NVarChar, 20); CheckNullParameterStore(_CustomerID, obj.CustomerId);
                var _CustomerName = new SqlParameter("@CustomerName", SqlDbType.NVarChar, 255); CheckNullParameterStore(_CustomerName, obj.CustomerName);
                var _LeadName = new SqlParameter("@LeadName", SqlDbType.NVarChar, 255); CheckNullParameterStore(_LeadName, obj.LeadName);
                var _OrganizationGuid = new SqlParameter("@OrganizationGuid", SqlDbType.UniqueIdentifier); _OrganizationGuid.Value = obj.OrganizationGuid;
                var _OrganizationName = new SqlParameter("@OrganizationName", SqlDbType.NVarChar, 255); _OrganizationName.Value = _emp.OrganizationName;
                var ParentID = new SqlParameter("@ParentID", SqlDbType.UniqueIdentifier); CheckNullParameterStore(ParentID, obj.ParentId);
                var _DepartmentGuid = new SqlParameter("@DepartmentGuid", SqlDbType.UniqueIdentifier); CheckNullParameterStore(_DepartmentGuid, obj.DepartmentGuid);
                var _DepartmentName = new SqlParameter("@DepartmentName", SqlDbType.NVarChar, 255); CheckNullParameterStore(_DepartmentName, obj.DepartmentName);
                var _IsBussiness = new SqlParameter("@IsBussiness", SqlDbType.Bit); CheckNullParameterStore(_IsBussiness, obj.IsBussiness);
                var _Representative = new SqlParameter("@Representative", SqlDbType.NVarChar, 50); CheckNullParameterStore(_Representative, obj.Representative);
                var _IDNumber = new SqlParameter("@IDNumber", SqlDbType.VarChar, 50); CheckNullParameterStore(_IDNumber, obj.Idnumber);
                var _BirthPlace = new SqlParameter("@BirthPlace", SqlDbType.NVarChar, 255); CheckNullParameterStore(_BirthPlace, obj.BirthPlace);
                var _IssueIDDate = new SqlParameter("@IssueIDDate", SqlDbType.DateTime); CheckNullParameterStore(_IssueIDDate, obj.IssueIddate);
                var _IssueIDBy = new SqlParameter("@IssueIDBy", SqlDbType.NVarChar, 50); CheckNullParameterStore(_IssueIDBy, obj.IssueIdby);
                var _JobTitleID = new SqlParameter("@JobTitleID", SqlDbType.VarChar, 20); CheckNullParameterStore(_JobTitleID, obj.JobTitleId);
                var _JobTitle = new SqlParameter("@JobTitle", SqlDbType.NVarChar, 255); CheckNullParameterStore(_JobTitle, obj.JobTitle);
                var _CountryID = new SqlParameter("@CountryID", SqlDbType.VarChar, 20); CheckNullParameterStore(_CountryID, obj.CountryId);
                var _CountryName = new SqlParameter("@CountryName", SqlDbType.NVarChar, 50); CheckNullParameterStore(_CountryName, obj.CountryName);
                var _TerritoryID = new SqlParameter("@TerritoryID", SqlDbType.Int); CheckNullParameterStore(_TerritoryID, obj.TerritoryId);
                var _TerritoryName = new SqlParameter("@TerritoryName", SqlDbType.NVarChar, 50); CheckNullParameterStore(_TerritoryName, obj.TerritoryName);
                var _ProvinceID = new SqlParameter("@ProvinceID", SqlDbType.VarChar, 20); CheckNullParameterStore(_ProvinceID, obj.ProvinceId);
                var _ProvinceName = new SqlParameter("@ProvinceName", SqlDbType.NVarChar, 50); CheckNullParameterStore(_ProvinceName, obj.ProvinceName);
                var _DistrictID = new SqlParameter("@DistrictID", SqlDbType.VarChar, 20); CheckNullParameterStore(_DistrictID, obj.DistrictId);
                var _DistrictIName = new SqlParameter("@DistrictIName", SqlDbType.NVarChar, 50); CheckNullParameterStore(_DistrictIName, obj.DistrictIname);
                var _Wards = new SqlParameter("@Wards", SqlDbType.VarChar, 20); CheckNullParameterStore(_Wards, obj.Wards);
                var _Address = new SqlParameter("@Address", SqlDbType.NVarChar, 500); CheckNullParameterStore(_Address, obj.Address);
                var _PermanentAddress = new SqlParameter("@PermanentAddress", SqlDbType.NVarChar, 200); CheckNullParameterStore(_PermanentAddress, obj.PermanentAddress);
                var _WorkEmail = new SqlParameter("@WorkEmail", SqlDbType.NVarChar, 500); CheckNullParameterStore(_WorkEmail, obj.WorkEmail);
                var _WorkPhone = new SqlParameter("@WorkPhone", SqlDbType.NVarChar, 20); CheckNullParameterStore(_WorkPhone, obj.WorkPhone);
                var _WorkMobile = new SqlParameter("@WorkMobile", SqlDbType.NVarChar, 20); CheckNullParameterStore(_WorkMobile, obj.WorkMobile);
                var _WorkFax = new SqlParameter("@WorkFax", SqlDbType.NVarChar, 20); CheckNullParameterStore(_WorkFax, obj.WorkFax);
                var _Website = new SqlParameter("@Website", SqlDbType.NVarChar, 50); CheckNullParameterStore(_Website, obj.Website);
                var _TaxCode = new SqlParameter("@TaxCode", SqlDbType.VarChar, 20); CheckNullParameterStore(_TaxCode, obj.TaxCode);
                var _WorkPlace = new SqlParameter("@WorkPlace", SqlDbType.NVarChar, 200); CheckNullParameterStore(_WorkPlace, obj.WorkPlace);
                var _ActivityFieldID = new SqlParameter("@ActivityFieldID", SqlDbType.Int); CheckNullParameterStore(_ActivityFieldID, obj.ActivityFieldId);
                var _ActivityFieldName = new SqlParameter("@ActivityFieldName", SqlDbType.NVarChar); CheckNullParameterStore(_ActivityFieldName, obj.ActivityFieldName);
                var _BirthDate = new SqlParameter("@BirthDate", SqlDbType.DateTime); CheckNullParameterStore(_BirthDate, obj.BirthDate);
                var _Capital = new SqlParameter("@Capital", SqlDbType.Float); CheckNullParameterStore(_Capital, Convert.ToDecimal(obj.Capital));
                var _StaffNumber = new SqlParameter("@StaffNumber", SqlDbType.Int); CheckNullParameterStore(_StaffNumber, obj.StaffNumber);
                var _SourceType = new SqlParameter("@SourceType", SqlDbType.VarChar, 1); CheckNullParameterStore(_SourceType, obj.SourceType);
                var _Image = new SqlParameter("@Image", SqlDbType.VarBinary); CheckNullParameterStore(_Image, obj.Image);
                var _ImageUrl = new SqlParameter("@ImageUrl", SqlDbType.NVarChar, 250); CheckNullParameterStore(_ImageUrl, obj.ImageUrl);
                var _Gender = new SqlParameter("@Gender", SqlDbType.VarChar, 1); CheckNullParameterStore(_Gender, obj.Gender);
                var _MaritalStatus = new SqlParameter("@MaritalStatus", SqlDbType.VarChar, 1); CheckNullParameterStore(_MaritalStatus, obj.MaritalStatus);
                var _HaveChildren = new SqlParameter("@HaveChildren", SqlDbType.Bit); CheckNullParameterStore(_HaveChildren, obj.HaveChildren);
                var _SuccessRate = new SqlParameter("@SuccessRate", SqlDbType.TinyInt); CheckNullParameterStore(_SuccessRate, Convert.ToByte(obj.SuccessRate));
                var _RateByLevel = new SqlParameter("@RateByLevel", SqlDbType.VarChar, 1); CheckNullParameterStore(_RateByLevel, obj.RateByLevel);
                var _RateByScore = new SqlParameter("@RateByScore", SqlDbType.Int); CheckNullParameterStore(_RateByScore, obj.RateByScore);
                var _Status = new SqlParameter("@Status", SqlDbType.VarChar, 1); CheckNullParameterStore(_Status, obj.Status);
                var _IsActive = new SqlParameter("@IsActive", SqlDbType.VarChar, 1); CheckNullParameterStore(_IsActive, obj.IsActive);
                var _IsLocked = new SqlParameter("@IsLocked", SqlDbType.Bit); CheckNullParameterStore(_IsLocked, obj.IsLocked);
                var _CompaignGuid = new SqlParameter("@CompaignGuid", SqlDbType.UniqueIdentifier); CheckNullParameterStore(_CompaignGuid, obj.CompaignGuid);
                var _CompaignID = new SqlParameter("@CompaignID", SqlDbType.VarChar, 20); CheckNullParameterStore(_CompaignID, obj.CompaignId);
                var _CompaignName = new SqlParameter("@CompaignName", SqlDbType.NVarChar, 50); CheckNullParameterStore(_CompaignName, obj.CompaignName);
                var _ResourceID = new SqlParameter("@ResourceID", SqlDbType.Int); CheckNullParameterStore(_ResourceID, obj.ResourceId);
                var _ResourceName = new SqlParameter("@ResourceName", SqlDbType.NVarChar, 50); CheckNullParameterStore(_ResourceName, obj.ResourceName);
                var _ResourceDetail = new SqlParameter("@ResourceDetail", SqlDbType.NVarChar, 255); CheckNullParameterStore(_ResourceDetail, obj.ResourceDetail);
                var _BusinessTypeID = new SqlParameter("@BusinessTypeID", SqlDbType.Int); CheckNullParameterStore(_BusinessTypeID, obj.BusinessTypeId);
                var _BusinessTypeName = new SqlParameter("@BusinessTypeName", SqlDbType.NVarChar, 50); CheckNullParameterStore(_BusinessTypeName, obj.BusinessTypeName);
                var _SortTitle = new SqlParameter("@SortTitle", SqlDbType.NVarChar, 255); CheckNullParameterStore(_SortTitle, obj.SortTitle);
                var _Description = new SqlParameter("@Description", SqlDbType.NVarChar); CheckNullParameterStore(_Description, obj.Description);
                var _CategoryID = new SqlParameter("@CategoryID", SqlDbType.Int); CheckNullParameterStore(_CategoryID, obj.CategoryId);
                var _AmountSalesForecas = new SqlParameter("@AmountSalesForecas", SqlDbType.Decimal); CheckNullParameterStore(_AmountSalesForecas, obj.AmountSalesForecas);
                var _AmountSales = new SqlParameter("@AmountSales", SqlDbType.Decimal); CheckNullParameterStore(_AmountSales, obj.AmountSales);
                var _StartDate = new SqlParameter("@StartDate", SqlDbType.DateTime); CheckNullParameterStore(_StartDate, obj.StartDate);
                var _EndDate = new SqlParameter("@EndDate", SqlDbType.DateTime); CheckNullParameterStore(_EndDate, obj.EndDate);
                var _UTMSource = new SqlParameter("@UTMSource", SqlDbType.NVarChar); CheckNullParameterStore(_UTMSource, obj.Utmsource);
                var _UTMCampaign = new SqlParameter("@UTMCampaign", SqlDbType.NVarChar); CheckNullParameterStore(_UTMCampaign, obj.Utmcampaign);
                var _UTMMedium = new SqlParameter("@UTMMedium", SqlDbType.NVarChar); CheckNullParameterStore(_UTMMedium, obj.Utmmedium);
                var _UTMContent = new SqlParameter("@UTMContent", SqlDbType.NVarChar); CheckNullParameterStore(_UTMContent, obj.Utmcontent);
                var _UTMTerm = new SqlParameter("@UTMTerm", SqlDbType.NVarChar); CheckNullParameterStore(_UTMTerm, obj.Utmterm);
                var _LinkForm = new SqlParameter("@LinkForm", SqlDbType.NVarChar); CheckNullParameterStore(_LinkForm, obj.LinkForm);
                var _DebtDay = new SqlParameter("@DebtDay", SqlDbType.Int); CheckNullParameterStore(_DebtDay, obj.DebtDay);
                var _CustomerAttributes = new SqlParameter("@CustomerAttributes", SqlDbType.VarChar, 1); CheckNullParameterStore(_CustomerAttributes, null);
                var _DateOfApplication = new SqlParameter("@DateOfApplication", SqlDbType.DateTime); CheckNullParameterStore(_DateOfApplication, null);
                var _Feedback = new SqlParameter("@Feedback", SqlDbType.NVarChar); CheckNullParameterStore(_Feedback, null);
                var _Exchange = new SqlParameter("@Exchange", SqlDbType.NVarChar); CheckNullParameterStore(_Exchange, null);
                var _GroupGuid = new SqlParameter("@GroupGuid", SqlDbType.UniqueIdentifier); CheckNullParameterStore(_GroupGuid, obj.PolicyGroupGuid);
                obj.CreatedDate = DateTime.Now;
                var _CreatedDate = new SqlParameter("@CreatedDate", SqlDbType.DateTime); CheckNullParameterStore(_CreatedDate, obj.CreatedDate);
                obj.ModifiedDate = DateTime.Now;
                var _ModifiedDate = new SqlParameter("@ModifiedDate", SqlDbType.DateTime); CheckNullParameterStore(_ModifiedDate, obj.ModifiedDate);
                var _CreatedBy = new SqlParameter("@CreatedBy", SqlDbType.NVarChar, 50); CheckNullParameterStore(_CreatedBy, obj.CreatedBy);
                var _ModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.NVarChar, 50); CheckNullParameterStore(_ModifiedBy, obj.ModifiedBy);
                var _IsLeads = new SqlParameter("@IsLeads", SqlDbType.Bit); CheckNullParameterStore(_IsLeads, obj.IsLeads);
                var _IsProvider = new SqlParameter("@IsProvider", SqlDbType.Bit); CheckNullParameterStore(_IsProvider, obj.IsProvider);
                var IsCustomer = new SqlParameter("@IsCustomer", SqlDbType.Bit); CheckNullParameterStore(IsCustomer, obj.IsCustomer);

                var _OrganizationNameInvoices = new SqlParameter("@OrganizationNameInvoices", SqlDbType.NVarChar, 255); CheckNullParameterStore(_OrganizationNameInvoices, obj.OrganizationNameInvoices);
                var _AddressInvoices = new SqlParameter("@AddressInvoices", SqlDbType.NVarChar, 500); CheckNullParameterStore(_AddressInvoices, obj.AddressInvoices);
                var _StartDatePP = new SqlParameter("@StartDatePP", SqlDbType.DateTime); CheckNullParameterStore(_StartDatePP, obj.StartDatePP);
                var _Monitor = new SqlParameter("@Monitor", SqlDbType.VarChar, 1); CheckNullParameterStore(_Monitor, obj.Monitor);
                var _Method = new SqlParameter("@Method", SqlDbType.VarChar, 1); CheckNullParameterStore(_Method, obj.Method);
                var _Type = new SqlParameter("@Type", SqlDbType.VarChar, 20); CheckNullParameterStore(_Type, obj.Type);
                var _PolicyGuid = new SqlParameter("@PolicyGuid", SqlDbType.UniqueIdentifier); CheckNullParameterStore(_PolicyGuid, obj.PolicyGuid);
                var _PolicyID = new SqlParameter("@PolicyID", SqlDbType.VarChar, 20); CheckNullParameterStore(_PolicyID, obj.PolicyId);
                var _PolicyName = new SqlParameter("@PolicyName", SqlDbType.NVarChar, 255); CheckNullParameterStore(_PolicyName, obj.PolicyName);
                var _PolicyGroupGuid = new SqlParameter("@PolicyGroupGuid", SqlDbType.UniqueIdentifier); CheckNullParameterStore(_PolicyGroupGuid, obj.PolicyGroupGuid);
                var _PolicyGroupID = new SqlParameter("@PolicyGroupID", SqlDbType.VarChar, 20); CheckNullParameterStore(_PolicyGroupID, obj.PolicyGroupId);
                var _PolicyGroupName = new SqlParameter("@PolicyGroupName", SqlDbType.NVarChar, 255); CheckNullParameterStore(_PolicyGroupName, obj.PolicyGroupName);
                var _Note = new SqlParameter("@Note", SqlDbType.NVarChar, 255); CheckNullParameterStore(_Note, obj.Note);
                var _IsProvince = new SqlParameter("@IsProvince", SqlDbType.VarChar, 1); CheckNullParameterStore(_IsProvince, obj.IsProvince);

                var _Error = new SqlParameter("@Error", SqlDbType.NVarChar, 500) { Direction = System.Data.ParameterDirection.Output };

                var _rs = _context.SQLCOMMANDS.FromSql(sql, _Database, _DBName, _ID, _CustomerGuid, _CustomerID, _CustomerName, _LeadName, _OrganizationGuid, ParentID, _DepartmentGuid, _DepartmentName, _IsBussiness, _Representative, _IDNumber, _BirthPlace, _IssueIDDate, _IssueIDBy, _JobTitleID, _JobTitle, _CountryID, _CountryName, _TerritoryID, _TerritoryName, _ProvinceID, _ProvinceName, _DistrictID, _DistrictIName, _Wards, _Address, _PermanentAddress, _WorkEmail, _WorkPhone, _WorkMobile, _WorkFax, _Website, _TaxCode, _WorkPlace, _ActivityFieldID, _ActivityFieldName, _BirthDate, _Capital, _StaffNumber, _SourceType, _Image, _ImageUrl, _Gender, _MaritalStatus, _HaveChildren, _SuccessRate, _RateByLevel, _RateByScore, _Status, _IsActive, _IsLocked, _CompaignGuid, _CompaignID, _CompaignName, _ResourceID, _ResourceName, _ResourceDetail, _BusinessTypeID, _BusinessTypeName, _SortTitle, _Description, _CategoryID, _AmountSalesForecas, _AmountSales, _StartDate, _EndDate, _UTMSource, _UTMCampaign, _UTMMedium, _UTMContent, _UTMTerm, _LinkForm, _DebtDay, _CustomerAttributes, _DateOfApplication, _Feedback, _Exchange, _GroupGuid, _CreatedDate, _CreatedBy, _ModifiedDate, _ModifiedBy, _IsLeads, _IsProvider, IsCustomer, _OrganizationNameInvoices, _AddressInvoices, _StartDatePP, _Monitor, _Method, _Type, _PolicyGuid, _PolicyID, _PolicyName, _PolicyGroupGuid, _PolicyGroupID, _PolicyGroupName, _Note, _IsProvince, _Error).FirstOrDefault();
                if (_rs.Error != "")
                {
                    msg.Error = true;
                    return Json(new JMessage() { Error = true, Title = _rs.Error, Object = _rs.Error });
                }
                if (deleteFileattach.Count > 0)
                {
                    foreach (var _iF in deleteFileattach)
                    {
                        string d_sql = "[dbo].[SP_DELETE_Attachment] @Database, @scheme, @TableName, @OrganizationGuid, @RowGuid, @Error";
                        var d_Database = new SqlParameter("@Database", SqlDbType.NVarChar, 50); CheckNullParameterStore(d_Database, _appSettings.DBName_HR);
                        var d_Scheme = new SqlParameter("@scheme", SqlDbType.VarChar); d_Scheme.Value = "Sale";
                        var d_TableName = new SqlParameter("@TableName", SqlDbType.VarChar); d_TableName.Value = "CustomerAttachments";
                        var d_OrganizationGuid = new SqlParameter("@OrganizationGuid", SqlDbType.UniqueIdentifier); d_OrganizationGuid.Value = _emp.OrganizationGuid;
                        var d_AttachmentGuid = new SqlParameter("@RowGuid", SqlDbType.NVarChar, 50); d_AttachmentGuid.Value = _iF.ToString();
                        var d_Error = new SqlParameter("@Error", SqlDbType.NVarChar); d_Error.Value = "";
                        var d_rs = _context.SQLCOMMANDS.FromSql(d_sql, d_Database, d_Scheme, d_TableName, d_OrganizationGuid, d_AttachmentGuid, d_Error).SingleOrDefault();
                    }
                }
                if (files.Count > 0)
                {
                    string sqlAttach = "[dbo].[SP_Insert_Attachment] @Database, @Scheme,@TableName,@TableAttachment";
                    var Database = new SqlParameter("@Database", SqlDbType.NVarChar, 50); CheckNullParameterStore(Database, _appSettings.DBName_HR);
                    var Scheme = new SqlParameter("@Scheme", SqlDbType.NVarChar, 50); CheckNullParameterStore(Scheme, "Sale");
                    var TableName = new SqlParameter("@TableName ", SqlDbType.NVarChar, 50); CheckNullParameterStore(TableName, "CustomerAttachments");
                    var _detailAttach = new SqlParameter("@TableAttachment", SqlDbType.Structured);
                    /// table detail
                    var _tematach = new System.Data.DataTable();
                    _tematach.Columns.Add("AttachmentGuid");
                    _tematach.Columns.Add("ParentGuid");
                    _tematach.Columns.Add("RecordGuid");
                    _tematach.Columns.Add("OrganizationGuid");
                    _tematach.Columns.Add("DepartmentGuid");
                    _tematach.Columns.Add("ModuleID");
                    _tematach.Columns.Add("Title");
                    _tematach.Columns.Add("FileName");
                    _tematach.Columns.Add("Attachment", typeof(byte[]));
                    _tematach.Columns.Add("FileExtension");
                    _tematach.Columns.Add("FileSize");
                    _tematach.Columns.Add("UrlExternal");
                    _tematach.Columns.Add("IsPublic");
                    _tematach.Columns.Add("PrivateLevel");
                    _tematach.Columns.Add("IsFolder");
                    _tematach.Columns.Add("IsHidden");
                    _tematach.Columns.Add("IsReadonly");
                    _tematach.Columns.Add("IsSync");
                    _tematach.Columns.Add("SyncTime");
                    _tematach.Columns.Add("CloudPath");
                    _tematach.Columns.Add("CreatedDate", typeof(DateTime));
                    _tematach.Columns.Add("ModifiedDate", typeof(DateTime));
                    _tematach.Columns.Add("CreatedBy");
                    _tematach.Columns.Add("ModifiedBy");
                    foreach (var file in files)
                    {
                        var SortOrder = file.Name.Split('#')[0];
                        if (SortOrder == "2")
                        {
                            var stream = file.OpenReadStream();
                            byte[] byteArr = Utilities.StreamToByteArray(stream);

                            Hrattachments _attach = new Hrattachments();
                            _attach.AttachmentGuid = Guid.NewGuid();
                            //var RecordGuid = detail.Select(x => new { x.SortOrder, x.EmployeeGuid }).Where(x => x.SortOrder.ToString() == SortOrder).Take(1).SingleOrDefault();
                            _attach.RecordGuid = obj.CustomerGuid;
                            _attach.OrganizationGuid = _emp.OrganizationGuid;
                            _attach.Title = file.Name.Split('#')[1];
                            _attach.FileName = file.FileName;
                            _attach.CreatedDate = DateTime.Now;
                            _attach.CreatedBy = GetCreatedBy(_context);
                            _attach.ModifiedDate = DateTime.Now;
                            _attach.ModifiedBy = GetModifiedBy(_context);
                            _attach.Attachment = byteArr;
                            _attach.ModuleId = "Quotations";
                            _attach.FileSize = byteArr.Length;
                            _attach.FileExtension = System.IO.Path.GetExtension(file.FileName);

                            _tematach.Rows.Add(_attach.AttachmentGuid,
                                _attach.ParentGuid,
                                _attach.RecordGuid,
                                _attach.OrganizationGuid,
                                _attach.DepartmentGuid,
                                _attach.ModuleId,
                                _attach.Title,
                                _attach.FileName,
                                _attach.Attachment,
                                _attach.FileExtension,
                                _attach.FileSize,
                                _attach.UrlExternal,
                                _attach.IsPublic,
                                _attach.PrivateLevel,
                                _attach.IsFolder,
                                _attach.IsHidden,
                                _attach.IsReadonly,
                                _attach.IsSync,
                                DBNull.Value,
                                _attach.CloudPath,
                                DBNull.Value,
                                DBNull.Value,
                                _attach.CreatedBy,
                                _attach.ModifiedBy
                                );
                        }
                    }
                    _detailAttach.Value = _tematach;
                    _detailAttach.TypeName = "[dbo].[Attachment]";
                    var __temattach = _context.SQLCOMMANDS.FromSql(sqlAttach, Database, Scheme, TableName, _detailAttach).FirstOrDefault();
                    if (__temattach.Error != "")
                    {
                        if (msg.Title == null)
                        {
                            msg.Title = "đính kèm";
                        }
                        else
                        {
                            msg.Title = msg.Title + ",đính kèm";
                        }
                    }

                }
                msg.Title = "Cập nhật thành công";
                msg.Object = obj.CustomerGuid;
            }
            catch (Exception ex)
            {
                msg.Error = true;             
                msg.Object = ex;
                msg.Title = ex.Message;
                return Json(msg);
            }
            return Json(msg);
        }
        
        [Area("Sale")]
        [HttpPost]
        public object GetAttachments([FromBody] Hrattachments obj)
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var username = GetEmployeeLogin(_context);
                string sql = "[dbo].[SP_GET_Attachment] @Database, @Scheme, @TableName, @OrganizationGuid, @VoucherGuid, @Error";
                var _Database = new SqlParameter("@Database", SqlDbType.NVarChar); _Database.Value = _appSettings.DBName_HR;
                var _Scheme = new SqlParameter("@Scheme", SqlDbType.NVarChar); _Scheme.Value = "Sale";
                var _TableName = new SqlParameter("@TableName", SqlDbType.NVarChar); _TableName.Value = "CustomerAttachments";
                var _OrganizationGuid = new SqlParameter("@OrganizationGuid", SqlDbType.UniqueIdentifier); _OrganizationGuid.Value = username.OrganizationGuid;
                var _VoucherGuid = new SqlParameter("@VoucherGuid", SqlDbType.NVarChar); CheckNullParameterStore(_VoucherGuid, obj.RecordGuid.ToString());
                var _Error = new SqlParameter("@Error", SqlDbType.NVarChar); _Error.Value = "";
                var rs = _context.OpenAttachmentsView.FromSql(sql, _Database, _Scheme, _TableName, _OrganizationGuid, _VoucherGuid, _Error).ToList();
                return rs;
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Title = ex.Message });
            }
        }
        [HttpGet]
        public FileResult Download(Guid? Id)
        {


            var msg = new JMessage() { Error = false };
            try
            {
                var username = GetEmployeeLogin(_context);
                var gui = new Guid();
                string sql = "[dbo].[SP_Download_Attachment] @Database,@Scheme, @TableName, @OrganizationGuid, @AttachmentGuid, @Error";
                var _Database = new SqlParameter("@Database", SqlDbType.VarChar); _Database.Value = _appSettings.DBName_HR;
                var _Scheme = new SqlParameter("@Scheme", SqlDbType.VarChar); _Scheme.Value = "Sale";
                var _TableName = new SqlParameter("@TableName", SqlDbType.VarChar); _TableName.Value = "CustomerAttachments";
                var _OrganizationGuid = new SqlParameter("@OrganizationGuid", SqlDbType.UniqueIdentifier); _OrganizationGuid.Value = username.OrganizationGuid;
                var _AttachmentGuid = new SqlParameter("@AttachmentGuid", SqlDbType.UniqueIdentifier); _AttachmentGuid.Value = Id == gui ? (Object)DBNull.Value : Id;
                var _Error = new SqlParameter("@Error", SqlDbType.NVarChar); _Error.Value = "";
                var rs = _context.AttachmentsDownloadView.FromSql(sql, _Database, _Scheme, _TableName, _OrganizationGuid, _AttachmentGuid, _Error).SingleOrDefault();
                if (rs != null)
                {
                    byte[] fileBytes = rs.Attachment;
                    var fileName = rs.FileName;
                    return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);

                }
                else return null;
            }
            catch (Exception ex)
            {
                msg.Title = "Có lỗi khi lấy dữ liệu.";
                return null;
            }
        }
        [HttpPost]
        public object Delete(Guid? Id)
        {
            var msg = new JMessage() { Error = false };
            try
            {
                string sql = "[HR].[SP_ESSM_Customers_Delete] @CustomerGuid";
                var CustomerGuid = SqlPara("@CustomerGuid", Id, SqlDbType.UniqueIdentifier);
                var __tem = _context.SQLCOMMANDS.FromSql(sql, CustomerGuid).FirstOrDefault();
                if (__tem.Error == "")
                {
                    return new JMessage() { Error = false, Title = "Xóa thành công." };
                }
                else
                {
                    return new JMessage() { Error = true, Title = __tem.Error, Object = __tem };
                }
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = ex.Message });
            }
        }
    }
}
