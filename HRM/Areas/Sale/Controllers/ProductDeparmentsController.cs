﻿using FTU.Utils.HelperNet;
using HRM.Controllers;
using HRM.Models;
using HRM.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;
using System.Data;
using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Newtonsoft.Json;
using Grpc.Core;
using DocumentFormat.OpenXml.Drawing;
using ES_MODEL.HR.Custom.Wf;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using DocumentFormat.OpenXml.Drawing.Charts;
using ES_MODEL.Task;
using HRM.Filters;
using ES_MODEL.HR;
using Aspose.Pdf;
using DocumentFormat.OpenXml.Bibliography;
using DocumentFormat.OpenXml.Wordprocessing;
using Microsoft.CodeAnalysis;
using DocumentFormat.OpenXml.VariantTypes;
using static Aspose.Pdf.Operator;
using System.Threading;
using HRM.Models.Custom.Hr;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Math;
using System.Web;
using DocumentFormat.OpenXml.Spreadsheet;
using Stimulsoft.Report.Dashboard;
using DocumentFormat.OpenXml.Drawing.Spreadsheet;
using static Stimulsoft.Report.StiOptions.Wpf;
using Stimulsoft.System.Web.UI.WebControls;
using Items = HRM.Models.Items;
using DocumentFormat.OpenXml.ExtendedProperties;
using DataTable = System.Data.DataTable;

namespace HRM.Areas.Sale.Controllers
{
    [Authorize]
    public class ProductDeparmentsController : BaseController
    {
        public IConfigurationRoot Configuration { get; }
        private readonly HRMDBContext _context;
        private readonly IHostingEnvironment _environment;
        private readonly AppSettings _appSettings;

        public ProductDeparmentsController(IOptions<AppSettings> appSettings, HRMDBContext context, IHostingEnvironment environment)
        {
            _context = context;
            _environment = environment;
            _appSettings = appSettings.Value;
        }
        [Area("Sale")]
        [ActionAuthorize(EAction.ACCESS)]
        public IActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public object GetPicfile(Guid? id)
        {
            try
            {
                var Photo = _context.ProductDeparmentsComments_DOC.Where(x => x.AttachmentGuid == id).Select(x => x.Attachment).FirstOrDefault();
                byte[] ImageTemp = Photo;

                FileContentResult hd = null;
                hd = File(ImageTemp, "image/jpeg");
                int gh = ImageTemp.Count();
                if (gh > 0)
                {
                    return hd;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }
        [HttpPost]
        public JsonResult GetUnit()
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var _emp = GetEmployeeLogin(_context);
                string sql = "[Sale].[SP_ESHR_Unit_GetAll] @OrganizationGuid";
                var OrganizationGuid = SqlPara("@OrganizationGuid", _emp.OrganizationGuid, SqlDbType.UniqueIdentifier);
                var rs = _context.Object_Combobox.FromSql(sql, OrganizationGuid).ToList();
                return Json(rs);
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = ex.Message });
            }
        }
        [HttpPost]
        public JsonResult GetProductionManager()
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var _emp = GetEmployeeLogin(_context);
                string sql = "[Sale].[SP_ESHR_ProductionManager_GetAll] @OrganizationGuid";
                var OrganizationGuid = SqlPara("@OrganizationGuid", _emp.OrganizationGuid, SqlDbType.UniqueIdentifier);
                var rs = _context.Object_Combobox.FromSql(sql, OrganizationGuid).ToList();
                return Json(rs);
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = ex.Message });
            }
        }
        [HttpPost]
        public object getItemjexcel([FromBody] JTableCustomer obj)
        {
            var msg = new JMessage() { Error = false };
            var _emp = GetEmployeeLogin(_context);
            try
            {
                string sql = "[Sale].[SP_ESHR_Items_jexcel] @Search,@OrganizationGuid";
                var _Search = new SqlParameter("@Search", SqlDbType.NVarChar); CheckNullParameterStore(_Search, obj.Keyword);
                var OrganizationGuid = SqlPara("@OrganizationGuid", _emp.OrganizationGuid, SqlDbType.UniqueIdentifier);
                var rs = _context.Object_Jexcel.FromSql(sql, _Search, OrganizationGuid).ToList();
                return Json(rs);
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = ex.Message });
            }
        }
        [HttpPost]
        public object getItem_by_ItemId([FromBody] TempSub _obj)
        {
            var msg = new JMessage() { Error = false };
            var _emp = GetEmployeeLogin(_context);
            try
            {
                var Keyword = _obj.IdS[0];
                string sql = "[Sale].[SP_ESHR_Items_by_ItemID] @Search,@OrganizationGuid";
                var _Search = new SqlParameter("@Search", SqlDbType.NVarChar); CheckNullParameterStore(_Search, Keyword);
                var OrganizationGuid = SqlPara("@OrganizationGuid", _emp.OrganizationGuid, SqlDbType.UniqueIdentifier);

                var rs = _context.Items.FromSql(sql, _Search, OrganizationGuid).FirstOrDefault();
                return Json(rs);
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = ex.Message });
            }
        }
        [HttpPost]
        public object getUnitJexcel([FromBody] JTableCustomer obj)
        {
            var msg = new JMessage() { Error = false };
            var _emp = GetEmployeeLogin(_context);
            try
            {
                string sql = "[Sale].[SP_ESHR_Unit_jexcel] @Search,@OrganizationGuid";
                var _Search = new SqlParameter("@Search", SqlDbType.NVarChar); CheckNullParameterStore(_Search, obj.Keyword);
                var OrganizationGuid = SqlPara("@OrganizationGuid", _emp.OrganizationGuid, SqlDbType.UniqueIdentifier);
                var rs = _context.Object_Jexcel.FromSql(sql, _Search, OrganizationGuid).ToList();
                return Json(rs);
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = ex.Message });
            }
        }
        [HttpPost]
        public object getUnit_by_ItemId([FromBody] TempSub _obj)
        {
            var msg = new JMessage() { Error = false };
            var _emp = GetEmployeeLogin(_context);
            try
            {
                var Keyword = _obj.IdS[0];
                string sql = "[Sale].[SP_ESHR_Unit_jexcel_by_ItemId] @Search,@OrganizationGuid";
                var _Search = new SqlParameter("@Search", SqlDbType.NVarChar); CheckNullParameterStore(_Search, Keyword);
                var OrganizationGuid = SqlPara("@OrganizationGuid", _emp.OrganizationGuid, SqlDbType.UniqueIdentifier);

                var rs = _context.Units.FromSql(sql, _Search, OrganizationGuid).FirstOrDefault();
                return Json(rs);
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = ex.Message });
            }
        }

        [HttpPost]
        public object GetItemsByItemlistId()
        {
            try
            {
                var _emp = GetEmployeeLogin(_context);
                var tempRecrui = Request.Form["Insert"];
                var Detail = Request.Form["Detail"];
                ProductionMaterials obj = JsonConvert.DeserializeObject<ProductionMaterials>(tempRecrui);
                List<string> detail = JsonConvert.DeserializeObject<List<string>>(Detail);

                string sql = "[Sale].[SP_ESAM_GetItems_By_listItemId] @OrganizationGuid,@ItemId";
                var _OrganizationGuid = new SqlParameter("@OrganizationGuid", SqlDbType.UniqueIdentifier); _OrganizationGuid.Value = _emp.OrganizationGuid;
                var _ItemId = new SqlParameter("@ItemId", detail.Count >= 1 ? string.Join(",", detail) : "");
                var rs = _context.Items.FromSql(sql, _OrganizationGuid, _ItemId).ToList();
                return Json(rs);
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = "Lấy dữ liệu không thành công" });
            }
        }


        public class JTableCustomer : JTableModel
        {
            public string Keyword { set; get; }
            public string IsBussiness { set; get; }
            public string Status { set; get; }
            public string Types { set; get; }
            public string IsProvince { set; get; }
            public string ProvinceId { set; get; }
            public string TerritoryId { set; get; }
            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }
        }
        [HttpPost]
        public object JTable([FromBody] JTableCustomer jTable)
        {
            try
            {
                var _emp = GetEmployeeLogin(_context);
                if (jTable.StartDate != null)
                {
                    jTable.StartDate = jTable.StartDate.Value.ToLocalTime();
                }
                else
                {
                    jTable.StartDate = null;
                }
                if (jTable.EndDate != null)
                {
                    jTable.EndDate = jTable.EndDate.Value.ToLocalTime();
                }
                else
                {
                    jTable.EndDate = null;
                }
                int intBeginFor = (jTable.CurrentPage - 1) * jTable.Length;
                string sql = "[Sale].[SP_ESSM_ProductDeparments_JTable] " +
                   "@Keyword, " +
                   "@Status, " +
                   "@OrganizationGuid, " +
                   "@StartDate, " +
                    "@EndDate, " +
                     "@Sort, " +
                   "@Skip, " +
                   "@Take," +
                   "@Count out";
                var _Keyword = SqlPara("@Keyword", jTable.Keyword, SqlDbType.NVarChar);
                var _Status = SqlPara("@Status", jTable.Status, SqlDbType.NVarChar);
                var _OrganizationGuid = SqlPara("@OrganizationGuid", _emp.OrganizationGuid, SqlDbType.UniqueIdentifier);
                var StartDate = new SqlParameter("@StartDate", SqlDbType.Date); CheckNullParameterStore(StartDate, jTable.StartDate);
                var EndDate = new SqlParameter("@EndDate", SqlDbType.Date); CheckNullParameterStore(EndDate, jTable.EndDate);
                var Sort = SqlPara("@Sort", jTable.QueryOrderBy, SqlDbType.VarChar);
                var _Skip = SqlPara("@Skip", intBeginFor, SqlDbType.Int);
                var _Take = SqlPara("@Take", jTable.Length, SqlDbType.Int);
                var _output = new SqlParameter("@Count", SqlDbType.Int) { Direction = System.Data.ParameterDirection.Output };

                var data = _context.ProductDeparmentsViews.FromSql(
                            sql,
                            _Keyword,
                            _Status,
                            _OrganizationGuid,
                              StartDate,
                              EndDate,
                              Sort,
                            _Skip,
                            _Take,
                            _output
                ).ToList();
                var jdata = JTableHelper.JObjectTable(data, jTable.Draw, (int)_output.Value);
                return Json(jdata);
            }
            catch (Exception ex)
            {
                return new JMessage() { Error = true, Title = ex.Message, Object = ex };
            }
        }
        [HttpPost]
        public JsonResult GetItem(Guid? Id)
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var _emp = GetEmployeeLogin(_context);
                string sql = "[Sale].[SP_ESSM_ProductDeparments_GetItem] @RowGuid";
                var RowGuid = SqlPara("@RowGuid", Id, SqlDbType.UniqueIdentifier);
                var rs = _context.Object_Table5.FromSql(sql, RowGuid).FirstOrDefault();
                return Json(rs);
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = ex.Message });
            }
        }

        public class ObjItems : ProductDeparments
        {
            public string Temps { set; get; }
        }
        [HttpPost]
        public JsonResult Submit()
        {

            var msg = new JMessage() { Error = false };
            try
            {
                var _emp = GetEmployeeLogin(_context);
                var files = Request.Form.Files.ToList();
                var submit = Request.Form["submit"];
                var detail = Request.Form["detail"];
                var detail1 = Request.Form["detail1"];
                var detail2 = Request.Form["detail2"];
                var detail3 = Request.Form["detail3"];
                var detailDelete = Request.Form["detailDelete"];
                var detailDelete1 = Request.Form["detailDelete1"];
                var detailDelete2 = Request.Form["detailDelete2"];
                var detailDelete3 = Request.Form["detailDelete3"];
                ObjItems obj = JsonConvert.DeserializeObject<ObjItems>(submit);
                List<ProductDeparmentDetails> lstdetail = JsonConvert.DeserializeObject<List<ProductDeparmentDetails>>(detail);
                List<ProductMaterialDetails> lstdetail1 = JsonConvert.DeserializeObject<List<ProductMaterialDetails>>(detail1);
                List<ProductLogisticDetails> lstdetail2 = JsonConvert.DeserializeObject<List<ProductLogisticDetails>>(detail2);
                List<ProductKSCDetails> lstdetail3 = JsonConvert.DeserializeObject<List<ProductKSCDetails>>(detail3);
                List<Guid> ListdetailDelete = JsonConvert.DeserializeObject<List<Guid>>(detailDelete);
                List<Guid> ListdetailDelete1 = JsonConvert.DeserializeObject<List<Guid>>(detailDelete1);
                List<Guid> ListdetailDelete2 = JsonConvert.DeserializeObject<List<Guid>>(detailDelete2);
                List<Guid> ListdetailDelete3 = JsonConvert.DeserializeObject<List<Guid>>(detailDelete3);
                string sql = "[Sale].[SP_ESSM_ProductDeparments_Insert] @RowGuid,@OrganizationGuid,@OrderID,@CustomerID,@CustomerName,@DeliveryDate,@Feedback,@EmployeeID,@EmployeeName,@Note,@ShippingStatus,@OrderStatus ,@EmployeeKSC,@NoteKSC,@CreatedDate ,@ModifiedDate,@CreatedBy,@ModifiedBy" +
               ",@Temps,@lstDeleteDetail,@lstDeleteDetail1,@lstDeleteDetail2,@lstDeleteDetail3,@ProductDeparmentDetails,@ProductMaterialDetails,@ProductLogisticDetails,@ProductKSCDetails";
                obj.OrganizationGuid = _emp.OrganizationGuid;
                obj.CreatedDate = DateTime.Now;
                obj.ModifiedDate = DateTime.Now;
                obj.CreatedBy = GetCreatedBy(_context);
                obj.ModifiedBy = GetCreatedBy(_context);
                if (obj.DeliveryDate != null)
                {
                    obj.DeliveryDate = obj.DeliveryDate.Value.ToLocalTime();
                }
                else
                {
                    obj.DeliveryDate = null;
                }
                var RowGuid = SqlPara("@RowGuid                     ", obj.RowGuid, SqlDbType.UniqueIdentifier);
                var OrganizationGuid = SqlPara("@OrganizationGuid            ", obj.OrganizationGuid, SqlDbType.UniqueIdentifier);
                var OrderID = SqlPara("@OrderID                      ", obj.OrderID, SqlDbType.NVarChar);
                var CustomerID = SqlPara("@CustomerID                      ", obj.CustomerID, SqlDbType.NVarChar);
                var CustomerName = SqlPara("@CustomerName                       ", obj.CustomerName, SqlDbType.NVarChar);
                var DeliveryDate = SqlPara("@DeliveryDate       ", obj.DeliveryDate, SqlDbType.Date);
                var Feedback = SqlPara("@Feedback                  ", obj.Feedback, SqlDbType.NVarChar);
                var EmployeeID = SqlPara("@EmployeeID                  ", obj.EmployeeID, SqlDbType.NVarChar);
                var EmployeeName = SqlPara("@EmployeeName                ", obj.EmployeeName, SqlDbType.NVarChar);

                var Note = SqlPara("@Note                  ", obj.Note, SqlDbType.NVarChar);
                var ShippingStatus = SqlPara("@ShippingStatus                      ", obj.ShippingStatus, SqlDbType.NVarChar);
                var OrderStatus = SqlPara("@OrderStatus                   ", obj.OrderStatus, SqlDbType.NVarChar);
                var EmployeeKSC = SqlPara("@EmployeeKSC                ", obj.EmployeeKSC, SqlDbType.NVarChar);
                var NoteKSC = SqlPara("@NoteKSC                   ", obj.NoteKSC, SqlDbType.NVarChar);
                var CreatedDate = SqlPara("@CreatedDate                 ", obj.CreatedDate, SqlDbType.DateTime);
                var ModifiedDate = SqlPara("@ModifiedDate                ", obj.ModifiedDate, SqlDbType.DateTime);
                var CreatedBy = SqlPara("@CreatedBy                   ", obj.CreatedBy, SqlDbType.NVarChar);
                var ModifiedBy = SqlPara("@ModifiedBy                  ", obj.ModifiedBy, SqlDbType.NVarChar);
                var Temps = SqlPara("@Temps", obj.Temps, SqlDbType.NVarChar);

                var _lstDeleteDetail = new SqlParameter("@lstDeleteDetail", ListdetailDelete.Count >= 1 ? string.Join(",", ListdetailDelete) : "");
                var _lstDeleteDetail1 = new SqlParameter("@lstDeleteDetail1", ListdetailDelete1.Count >= 1 ? string.Join(",", ListdetailDelete1) : "");
                var _lstDeleteDetail2 = new SqlParameter("@lstDeleteDetail2", ListdetailDelete2.Count >= 1 ? string.Join(",", ListdetailDelete2) : "");
                var _lstDeleteDetail3 = new SqlParameter("@lstDeleteDetail3", ListdetailDelete3.Count >= 1 ? string.Join(",", ListdetailDelete3) : "");

                var _detail = new SqlParameter("@ProductDeparmentDetails", SqlDbType.Structured);
                var _tem = new System.Data.DataTable();
                _tem.Columns.Add("RowGuid      ");
                _tem.Columns.Add("RecordGuid   ");
                _tem.Columns.Add("ItemID       ");
                _tem.Columns.Add("ItemName     ");
                _tem.Columns.Add("Packing        ");
                _tem.Columns.Add("UnitID       ");
                _tem.Columns.Add("UnitName     ");
                _tem.Columns.Add("Quantity     ");
                _tem.Columns.Add("UnitPrice    ");
                _tem.Columns.Add("ExchangeRate    ");
                _tem.Columns.Add("AmountOc     ");
                _tem.Columns.Add("VATPercent   ");
                _tem.Columns.Add("VATAmountOC  ");
                _tem.Columns.Add("AmountNoVAT  ");
                _tem.Columns.Add("AmountVAT    ");
                _tem.Columns.Add("Note         ");
                _tem.Columns.Add("CreatedDate  ");
                _tem.Columns.Add("ModifiedDate ");
                _tem.Columns.Add("CreatedBy    ");
                _tem.Columns.Add("ModifiedBy   ");
                _tem.Columns.Add("SortOrder    ");

                foreach (var _item in lstdetail)
                {
                    _tem.Rows.Add(
                            _item.RowGuid
                            , _item.RecordGuid
                            , _item.ItemID
                            , _item.ItemName
                            , _item.Packing
                            , _item.UnitID
                            , _item.UnitName
                            , _item.Quantity
                            , _item.UnitPrice
                            , _item.ExchangeRate
                            , _item.AmountOc
                            , _item.VATPercent
                            , _item.VATAmountOC
                            , _item.AmountNoVAT
                            , _item.AmountVAT
                            , _item.Note
                            , DBNull.Value
                            , DBNull.Value
                            , DBNull.Value
                            , DBNull.Value
                            , _item.SortOrder
                             );
                }
                _detail.Value = _tem;
                _detail.TypeName = "[Sale].[ProductDeparmentDetails]";

                var _detail1 = new SqlParameter("@ProductMaterialDetails", SqlDbType.Structured);
                var _tem1 = new System.Data.DataTable();
                _tem1.Columns.Add("RowGuid      ");
                _tem1.Columns.Add("RecordGuid   ");
                _tem1.Columns.Add("ItemID       ");
                _tem1.Columns.Add("ItemID1       ");
                _tem1.Columns.Add("ItemName     ");
                _tem1.Columns.Add("UnitID       ");
                _tem1.Columns.Add("UnitName     ");
                _tem1.Columns.Add("Quantity     ");
                _tem1.Columns.Add("QuantityTotal    ");
                _tem1.Columns.Add("UnitPrice    ");
                _tem1.Columns.Add("AmountOc     ");
                _tem1.Columns.Add("EmployeeName   ");
                _tem1.Columns.Add("Note         ");
                _tem1.Columns.Add("CreatedDate  ");
                _tem1.Columns.Add("ModifiedDate ");
                _tem1.Columns.Add("CreatedBy    ");
                _tem1.Columns.Add("ModifiedBy   ");
                _tem1.Columns.Add("SortOrder    ");

                foreach (var _item in lstdetail1)
                {
                    _tem1.Rows.Add(
                            _item.RowGuid
                            , _item.RecordGuid
                            , _item.ItemID
                             , _item.ItemID1
                            , _item.ItemName
                            , _item.UnitID
                            , _item.UnitName
                            , _item.Quantity
                             , _item.QuantityTotal
                            , _item.UnitPrice
                            , _item.AmountOc
                            , _item.EmployeeName
                            , _item.Note
                            , DBNull.Value
                            , DBNull.Value
                            , DBNull.Value
                            , DBNull.Value
                            , _item.SortOrder
                             );
                }
                _detail1.Value = _tem1;
                _detail1.TypeName = "[Sale].[ProductMaterialDetails]";

                var _detail2 = new SqlParameter("@ProductLogisticDetails", SqlDbType.Structured);
                var _tem2 = new System.Data.DataTable();
                _tem2.Columns.Add("RowGuid      ");
                _tem2.Columns.Add("RecordGuid   ");
                _tem2.Columns.Add("ItemID       ");
                _tem2.Columns.Add("ItemID1       ");
                _tem2.Columns.Add("ItemName     ");
                _tem2.Columns.Add("ETD       ");
                _tem2.Columns.Add("BookingNumber     ");
                _tem2.Columns.Add("UnitID       ");
                _tem2.Columns.Add("UnitName     ");
                _tem2.Columns.Add("Quantity     ");
                _tem2.Columns.Add("QuantityTotal    ");
                _tem2.Columns.Add("UnitPrice    ");
                _tem2.Columns.Add("AmountOc     ");
                _tem2.Columns.Add("EmployeeName   ");
                _tem2.Columns.Add("Note         ");
                _tem2.Columns.Add("CreatedDate  ");
                _tem2.Columns.Add("ModifiedDate ");
                _tem2.Columns.Add("CreatedBy    ");
                _tem2.Columns.Add("ModifiedBy   ");
                _tem2.Columns.Add("SortOrder    ");

                foreach (var _item in lstdetail2)
                {
                    _tem2.Rows.Add(
                            _item.RowGuid
                            , _item.RecordGuid
                            , _item.ItemID
                              , _item.ItemID1
                            , _item.ItemName
                              , _item.ETD
                            , _item.BookingNumber
                            , _item.UnitID
                            , _item.UnitName
                            , _item.Quantity
                             , _item.QuantityTotal
                            , _item.UnitPrice
                            , _item.AmountOc
                            , _item.EmployeeName
                            , _item.Note
                            , DBNull.Value
                            , DBNull.Value
                            , DBNull.Value
                            , DBNull.Value
                            , _item.SortOrder
                             );
                }
                _detail2.Value = _tem2;
                _detail2.TypeName = "[Sale].[ProductLogisticDetails]";

                var _detail3 = new SqlParameter("@ProductKSCDetails", SqlDbType.Structured);
                var _tem3 = new System.Data.DataTable();
                _tem3.Columns.Add("RowGuid      ");
                _tem3.Columns.Add("RecordGuid   ");
                _tem3.Columns.Add("ItemID       ");
                _tem3.Columns.Add("ItemID1       ");
                _tem3.Columns.Add("ItemName     ");
                _tem3.Columns.Add("Packing       ");
                _tem3.Columns.Add("UnitID       ");
                _tem3.Columns.Add("UnitName     ");
                _tem3.Columns.Add("QuantitySX     ");                
                _tem3.Columns.Add("Quantity     ");
                _tem3.Columns.Add("QuantityTotal    ");
                _tem3.Columns.Add("UnitPrice     ");
                _tem3.Columns.Add("AmountOc     ");
                _tem3.Columns.Add("ExchangeRate     ");
                _tem3.Columns.Add("QualityStatus    ");
                _tem3.Columns.Add("PreservationStatus     ");
                _tem3.Columns.Add("Note         ");
                _tem3.Columns.Add("CreatedDate  ");
                _tem3.Columns.Add("ModifiedDate ");
                _tem3.Columns.Add("CreatedBy    ");
                _tem3.Columns.Add("ModifiedBy   ");
                _tem3.Columns.Add("SortOrder    ");
                _tem3.Columns.Add("EmployeeName    ");


                foreach (var _item in lstdetail3)
                {
                    _tem3.Rows.Add(
                            _item.RowGuid
                            , _item.RecordGuid
                            , _item.ItemID
                            , _item.ItemID1
                            , _item.ItemName
                              , _item.Packing
                            , _item.UnitID
                            , _item.UnitName
                            , _item.QuantitySX
                            , _item.Quantity
                             , _item.QuantityTotal
                               , _item.UnitPrice
                                 , _item.AmountOc
                                   , _item.ExchangeRate
                            , _item.QualityStatus
                            , _item.PreservationStatus
                            , _item.Note
                            , DBNull.Value
                            , DBNull.Value
                            , DBNull.Value
                            , DBNull.Value
                            , _item.SortOrder,
                            _item.EmployeeName
                             );
                }
                _detail3.Value = _tem3;
                _detail3.TypeName = "[Sale].[ProductKSCDetails]";

                var _rs = _context.SQLCOMMANDS.FromSql(sql, RowGuid, OrganizationGuid, OrderID, CustomerID, CustomerName, DeliveryDate, Feedback, EmployeeID, EmployeeName, Note, ShippingStatus, OrderStatus, EmployeeKSC, NoteKSC, CreatedDate, ModifiedDate, CreatedBy, ModifiedBy,
                    Temps, _detail, _detail1, _detail2, _detail3, _lstDeleteDetail, _lstDeleteDetail1, _lstDeleteDetail2, _lstDeleteDetail3).FirstOrDefault();
                if (_rs.Error == "")
                {
                    if (obj.Temps == "1")
                    {
                        msg.Title = "Thêm mới thành công";
                    }
                    else
                    {
                        msg.Title = "Cập nhật thành công";
                    }
                }
                else
                {
                    return Json(new JMessage() { Error = true, Title = _rs.Error, Object = _rs.Error });
                }
            }
            catch (Exception ex)
            {
                msg.Error = true;
                msg.Object = ex;
                msg.Title = ex.Message;
            }
            return Json(msg);
        }

        [HttpPost]
        public object Delete(Guid? Id)
        {
            var msg = new JMessage() { Error = false };
            try
            {
                string sql = "[Sale].[SP_ESSM_ProductDeparments_Delete] @RowGuid";
                var RowGuid = SqlPara("@RowGuid", Id, SqlDbType.UniqueIdentifier);
                var __tem = _context.SQLCOMMANDS.FromSql(sql, RowGuid).FirstOrDefault();
                if (__tem.Error == "")
                {
                    return new JMessage() { Error = false, Title = "Xóa thành công." };
                }
                else
                {
                    return new JMessage() { Error = true, Title = __tem.Error, Object = __tem };
                }
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = ex.Message });
            }
        }
        [HttpPost]
        public JMessage InsertComments()
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var userlogin = GetEmployeeLogin(_context);
                var files = Request.Form.Files.ToList();
                var _obj = Request.Form["insert"];
                ProductionManagerComments obj = JsonConvert.DeserializeObject<ProductionManagerComments>(_obj);
                obj.CommentGuid = Guid.NewGuid();
                obj.OrganizationGuid = userlogin.OrganizationGuid;
                obj.EmployeeGuid = userlogin.EmployeeGuid;
                obj.EmployeeId = userlogin.EmployeeId;
                obj.EmployeeName = userlogin.FullName;
                obj.DepartmentGuid = userlogin.DepartmentGuid;
                obj.DepartmentId = userlogin.DepartmentId;
                obj.DepartmentName = userlogin.DepartmentName;
                obj.AvatarUrl = "";
                obj.CreatedDate = DateTime.Now;
                obj.CreatedBy = GetCreatedBy(_context);
                obj.ModifiedDate = DateTime.Now;
                obj.ModifiedBy = GetModifiedBy(_context);
                string sql = "[Hr].[SP_Insert_Comments_Hr] @Scheme, @TableName, @CommentGuid,@ParentGuid,@RecordGuid,@OrganizationGuid,@Comment,@EmployeeGuid,@EmployeeID,@EmployeeName,@LoginName,@DepartmentGuid,@DepartmentID,@DepartmentName,@AvatarUrl,@CreatedDate,@CreatedBy,@ModifiedDate,@ModifiedBy,@Error";
                var _Scheme = new SqlParameter("@Scheme", SqlDbType.VarChar);
                CheckNullParameterStore(_Scheme, "Sale");
                var _TableName = new SqlParameter("@TableName", SqlDbType.NVarChar);
                CheckNullParameterStore(_TableName, "ProductDeparmentsComments");
                var _CommentGuid = new SqlParameter("@CommentGuid", SqlDbType.UniqueIdentifier);
                CheckNullParameterStore(_CommentGuid, obj.CommentGuid);
                var _ParentGuid = new SqlParameter("@ParentGuid", SqlDbType.UniqueIdentifier);
                CheckNullParameterStore(_ParentGuid, obj.ParentGuid);
                var _RecordGuid = new SqlParameter("@RecordGuid", SqlDbType.UniqueIdentifier);
                CheckNullParameterStore(_RecordGuid, obj.RecordGuid);
                var _OrganizationGuid = new SqlParameter("@OrganizationGuid", SqlDbType.UniqueIdentifier);
                CheckNullParameterStore(_OrganizationGuid, obj.OrganizationGuid);
                var _Comment = new SqlParameter("@Comment", SqlDbType.NVarChar);
                CheckNullParameterStore(_Comment, obj.Comment);
                var _EmployeeGuid = new SqlParameter("@EmployeeGuid", SqlDbType.UniqueIdentifier);
                CheckNullParameterStore(_EmployeeGuid, obj.EmployeeGuid);
                var _EmployeeID = new SqlParameter("@EmployeeID", SqlDbType.VarChar, 20);
                CheckNullParameterStore(_EmployeeID, obj.EmployeeId);
                var _EmployeeName = new SqlParameter("@EmployeeName", SqlDbType.NVarChar, 50);
                CheckNullParameterStore(_EmployeeName, obj.EmployeeName);
                var _LoginName = new SqlParameter("@LoginName", SqlDbType.NVarChar, 50);
                CheckNullParameterStore(_LoginName, userlogin.LoginName);
                var _DepartmentGuid = new SqlParameter("@DepartmentGuid", SqlDbType.UniqueIdentifier);
                CheckNullParameterStore(_DepartmentGuid, obj.DepartmentGuid);
                var _DepartmentID = new SqlParameter("@DepartmentID", SqlDbType.VarChar, 20);
                CheckNullParameterStore(_DepartmentID, obj.DepartmentId);
                var _DepartmentName = new SqlParameter("@DepartmentName", SqlDbType.NVarChar, 50);
                CheckNullParameterStore(_DepartmentName, obj.DepartmentName);
                var _AvatarUrl = new SqlParameter("@AvatarUrl", SqlDbType.NVarChar, 255);
                CheckNullParameterStore(_AvatarUrl, obj.AvatarUrl);
                var _CreatedDate = new SqlParameter("@CreatedDate", SqlDbType.DateTime);
                CheckNullParameterStore(_CreatedDate, obj.CreatedDate);
                var _CreatedBy = new SqlParameter("@CreatedBy", SqlDbType.NVarChar, 250);
                CheckNullParameterStore(_CreatedBy, obj.CreatedBy);
                var _ModifiedDate = new SqlParameter("@ModifiedDate", SqlDbType.DateTime);
                CheckNullParameterStore(_ModifiedDate, obj.ModifiedDate);
                var _ModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.NVarChar, 250);
                CheckNullParameterStore(_ModifiedBy, obj.ModifiedBy);
                var _Error = new SqlParameter("@Error", SqlDbType.NVarChar); _Error.Value = "";
                var rs = _context.SQLCOMMANDS.FromSql(sql, _Scheme, _TableName, _CommentGuid, _ParentGuid, _RecordGuid, _OrganizationGuid, _Comment, _EmployeeGuid, _EmployeeID, _EmployeeName, _LoginName, _DepartmentGuid, _DepartmentID, _DepartmentName, _AvatarUrl, _CreatedDate, _CreatedBy, _ModifiedDate, _ModifiedBy, _Error).FirstOrDefault();

                if (files.Count > 0)
                {

                    string sqlAttach = "[dbo].[SP_Insert_Attachment] @Database, @Scheme,@TableName,@TableAttachment";
                    var Database = new SqlParameter("@Database", SqlDbType.NVarChar, 50); CheckNullParameterStore(Database, _appSettings.DBName_HR);
                    var scheme = new SqlParameter("@scheme ", SqlDbType.NVarChar, 250); CheckNullParameterStore(scheme, "Sale");
                    var TableName = new SqlParameter("@TableName ", SqlDbType.NVarChar, 50); CheckNullParameterStore(TableName, "ProductDeparmentsComments_DOC");
                    var _detailAttach = new SqlParameter("@TableAttachment", SqlDbType.Structured);

                    /// table detail
                    var _tematach = new System.Data.DataTable();
                    _tematach.Columns.Add("AttachmentGuid");
                    _tematach.Columns.Add("ParentGuid");
                    _tematach.Columns.Add("RecordGuid");
                    _tematach.Columns.Add("OrganizationGuid");
                    _tematach.Columns.Add("DepartmentGuid");
                    _tematach.Columns.Add("ModuleID");
                    _tematach.Columns.Add("Title");
                    _tematach.Columns.Add("FileName");
                    _tematach.Columns.Add("Attachment", typeof(byte[]));
                    _tematach.Columns.Add("FileExtension");
                    _tematach.Columns.Add("FileSize");
                    _tematach.Columns.Add("UrlExternal");
                    _tematach.Columns.Add("IsPublic");
                    _tematach.Columns.Add("PrivateLevel");
                    _tematach.Columns.Add("IsFolder");
                    _tematach.Columns.Add("IsHidden");
                    _tematach.Columns.Add("IsReadonly");
                    _tematach.Columns.Add("IsSync");
                    _tematach.Columns.Add("SyncTime");
                    _tematach.Columns.Add("CloudPath");

                    _tematach.Columns.Add("CreatedDate", typeof(DateTime));
                    _tematach.Columns.Add("ModifiedDate", typeof(DateTime));
                    _tematach.Columns.Add("CreatedBy");
                    _tematach.Columns.Add("ModifiedBy");

                    foreach (var file in files)
                    {
                        var stream = file.OpenReadStream();
                        byte[] byteArr = Utilities.StreamToByteArray(stream);
                        var name = file.FileName;
                        Hrattachments _attach = new Hrattachments()
                        {
                            AttachmentGuid = Guid.NewGuid(),

                            RecordGuid = obj.CommentGuid,
                            OrganizationGuid = userlogin.OrganizationGuid,
                            Title = name,
                            FileName = name,
                            CreatedDate = DateTime.Now,
                            CreatedBy = GetCreatedBy(_context),
                            ModifiedDate = DateTime.Now,
                            ModifiedBy = GetModifiedBy(_context),
                            Attachment = byteArr,
                            ModuleId = "ES_HR",
                            FileSize = byteArr.Length,
                            FileExtension = System.IO.Path.GetExtension(name)
                        };
                        _tematach.Rows.Add(_attach.AttachmentGuid,
                              _attach.ParentGuid,
                              _attach.RecordGuid,
                              _attach.OrganizationGuid,
                              _attach.DepartmentGuid,
                              _attach.ModuleId,
                              _attach.Title,
                              _attach.FileName,
                              _attach.Attachment,
                              _attach.FileExtension,
                              _attach.FileSize,
                              _attach.UrlExternal,
                              _attach.IsPublic,
                              _attach.PrivateLevel,
                              _attach.IsFolder,
                              _attach.IsHidden,
                              _attach.IsReadonly,

                              _attach.IsSync,
                             DBNull.Value,
                              _attach.CloudPath,
                              DateTime.Now, DateTime.Now, _attach.CreatedBy, _attach.ModifiedBy);
                    }

                    _detailAttach.Value = _tematach;
                    _detailAttach.TypeName = "[dbo].[Attachment]";
                    var __temattach = _context.SQLCOMMANDS.FromSql(sqlAttach, Database, scheme, TableName, _detailAttach).FirstOrDefault();

                    if (__temattach.Error == "")
                    {
                        var mesage = __temattach.Error;
                    }
                    else
                    {
                        return new JMessage() { Error = true, Title = "Thêm file không thành công", Object = __temattach };
                    }

                }
                if (rs.Error == "")
                {

                    return new JMessage() { Error = false, Title = "Thêm ý kiến thành công." };
                }
                else
                {
                    return new JMessage() { Error = true, Title = rs.Error };
                }
            }
            catch (SqlException ex)
            {
                return new JMessage() { Error = true, Object = ex, Title = ex.Message };
            }
        }
        [HttpGet]
        public FileResult DownloadfileCommentAttach(Guid? Id)
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var username = GetEmployeeLogin(_context);
                var gui = new Guid();
                string sql = "[dbo].[SP_Download_Attachment] @Database,@Scheme, @TableName, @OrganizationGuid, @AttachmentGuid, @Error";
                var Database = new SqlParameter("@Database", SqlDbType.VarChar); Database.Value = _appSettings.DBName_HR;
                var Scheme = new SqlParameter("@Scheme", SqlDbType.VarChar); Scheme.Value = "Sale";
                var TableName = new SqlParameter("@TableName", SqlDbType.VarChar); TableName.Value = "ProductDeparmentsComments_DOC";
                var OrganizationGuid = new SqlParameter("@OrganizationGuid", SqlDbType.UniqueIdentifier); OrganizationGuid.Value = username.OrganizationGuid;
                var AttachmentGuid = new SqlParameter("@AttachmentGuid", SqlDbType.UniqueIdentifier); AttachmentGuid.Value = Id == gui ? (Object)DBNull.Value : Id;
                var Error = new SqlParameter("@Error", SqlDbType.NVarChar); Error.Value = "";
                var rs = _context.AttachmentsDownloadView_.FromSql(sql, Database, Scheme, TableName, OrganizationGuid, AttachmentGuid, Error).SingleOrDefault();
                if (rs != null)
                {
                    byte[] fileBytes = rs.Attachment;
                    var fileName = rs.FileName;
                    return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);

                }
                else return null;
            }
            catch (Exception ex)
            {
                msg.Title = "Có lỗi khi lấy dữ liệu.";

                return null;

            }
        }
        public class CustomPage
        {
            public int Page { set; get; }
            public int ItemPage { set; get; }
            public int? Status { set; get; }
            public Guid? RecordGuid { set; get; }
        }
        /// Danh sách Comment
        [HttpPost]
        public object GetAllCommentPage([FromBody] CustomPage obj)
        {
            try
            {
                var _emp = GetEmployeeLogin(_context);

                var intBeginFor = (obj.Page - 1) * obj.ItemPage;
                string sql = "[Hr].[SP_GET_Comment] @DBName,@Scheme,@TableName,@DBNameDocument,@SchemeAt,@TableNameAt,@OrganizationGuid,@RecordGuid,@Skip,@Take,@Error";

                var DBName = new SqlParameter("@DBName", SqlDbType.NVarChar); CheckNullParameterStore(DBName, _appSettings.DBName_HR);
                var Scheme = new SqlParameter("@Scheme", SqlDbType.NVarChar); CheckNullParameterStore(Scheme, "Sale");
                var TableName = new SqlParameter("@TableName", SqlDbType.NVarChar); CheckNullParameterStore(TableName, "ProductDeparmentsComments");
                var DBNameDocument = new SqlParameter("@DBNameDocument", SqlDbType.NVarChar); CheckNullParameterStore(DBNameDocument, _appSettings.DBName_HR);
                var SchemeAt = new SqlParameter("@SchemeAt", SqlDbType.NVarChar); CheckNullParameterStore(SchemeAt, "Sale");
                var TableNameAt = new SqlParameter("@TableNameAt", SqlDbType.NVarChar); CheckNullParameterStore(TableNameAt, "ProductDeparmentsComments_DOC");
                var OrganizationGuid = new SqlParameter("@OrganizationGuid", SqlDbType.NVarChar); CheckNullParameterStore(OrganizationGuid, _emp.OrganizationGuid.ToString());
                var RecordGuid = new SqlParameter("@RecordGuid", SqlDbType.NVarChar); CheckNullParameterStore(RecordGuid, obj.RecordGuid.ToString());
                var Skip = new SqlParameter("@Skip", SqlDbType.NVarChar); Skip.Value = intBeginFor;
                var Take = new SqlParameter("@Take", SqlDbType.NVarChar); Take.Value = obj.ItemPage;
                var Error = new SqlParameter("@Error", SqlDbType.Int) { Direction = System.Data.ParameterDirection.Output };
                var rs = _context.CommentView.FromSql(sql, DBName, Scheme, TableName, DBNameDocument, SchemeAt, TableNameAt, OrganizationGuid, RecordGuid, Skip, Take, Error).ToList();

                return new JMessage() { Error = false, Title = "Success", Object = rs };

            }
            catch (Exception ex)
            {
                return new JMessage() { Error = true, Title = "Có lỗi khi lấy dữ liệu", Object = ex };
            }
        }
        public class objTitleFile
        {
            public int? STT { get; set; }
            public string Title { get; set; }
        }
        public class ObjAttach
        {
            public Guid RecordGuid { get; set; }
            public int? STT { get; set; }

        }
        [HttpPost]
        //[ActionAuthorize(EAction.EDIT)]
        public JsonResult UpdateFile()
        {
            var msg = new JMessage() { Error = false, Title = "Cập nhật thành công." };
            try
            {
                var lstTitlefile = Request.Form["lstTitlefile"];
                List<objTitleFile> lstTitle = JsonConvert.DeserializeObject<List<objTitleFile>>(lstTitlefile);
                var DeleteAttach = Request.Form["DeleteAttach"];
                var _emp = GetEmployeeLogin(_context);
                var files = Request.Form.Files.ToList();
                var _obj = Request.Form["model"];
                ObjAttach obj = JsonConvert.DeserializeObject<ObjAttach>(_obj);
                List<Guid> deleteFileattach = JsonConvert.DeserializeObject<List<Guid>>(DeleteAttach);

                if (deleteFileattach.Count > 0)
                {
                    foreach (var id in deleteFileattach)
                    {
                        string sqlAttach = "[HR].[SP_Delete_Attachment] @database, @scheme,@TableName,@OrganizationGuid,@AttachmentGuid,@Error out";
                        var database = new SqlParameter("@database ", SqlDbType.NVarChar, 250); CheckNullParameterStore(database, _appSettings.DBName_HR);
                        var scheme = new SqlParameter("@scheme ", SqlDbType.NVarChar, 250); CheckNullParameterStore(scheme, "Sale");
                        var OrganizationGuid = new SqlParameter("@OrganizationGuid ", SqlDbType.NVarChar); CheckNullParameterStore(OrganizationGuid, _emp.OrganizationGuid.ToString());
                        var TableName = new SqlParameter("@TableName ", SqlDbType.NVarChar, 50); CheckNullParameterStore(TableName, "ProductDeparments_DOC");
                        var AttachGuid = new SqlParameter("@AttachmentGuid ", SqlDbType.NVarChar); CheckNullParameterStore(AttachGuid, id.ToString());
                        var Error = new SqlParameter("@Error ", SqlDbType.NVarChar) { Direction = ParameterDirection.Output }; CheckNullParameterStore(Error, "");
                        var temattach = _context.SQLCOMMANDS.FromSql(sqlAttach, database, scheme, TableName, OrganizationGuid, AttachGuid, Error).FirstOrDefault();
                        if (Error.Value.ToString() == "")
                        {
                            msg.Title = "Xóa đính kèm thành công.";
                        }
                        else
                        {
                            msg.Title = "Xóa đính kèm không thành công."; msg.Error = true;
                        }
                    }
                }
                if (files.Count > 0)
                {
                    string sqlAttach = "[Hr].[SP_Insert_Attachment_for_Calendar] @database, @scheme,@TableName,@TableAttachment";
                    var database = new SqlParameter("@database ", SqlDbType.NVarChar, 250); CheckNullParameterStore(database, _appSettings.DBName_HR);
                    var scheme = new SqlParameter("@scheme ", SqlDbType.NVarChar, 250); CheckNullParameterStore(scheme, "Sale");
                    var TableName = new SqlParameter("@TableName ", SqlDbType.NVarChar, 50); CheckNullParameterStore(TableName, "ProductDeparments_DOC");
                    var _detailAttach = new SqlParameter("@TableAttachment", SqlDbType.Structured);
                    /// table detail
                    var _tematach = new DataTable();
                    _tematach.Columns.Add("AttachmentGuid");
                    _tematach.Columns.Add("ParentGuid");
                    _tematach.Columns.Add("RecordGuid");
                    _tematach.Columns.Add("OrganizationGuid");
                    _tematach.Columns.Add("DepartmentGuid");
                    _tematach.Columns.Add("ModuleID");
                    _tematach.Columns.Add("Title");
                    _tematach.Columns.Add("FileName");
                    _tematach.Columns.Add("Attachment", typeof(byte[]));
                    _tematach.Columns.Add("FileExtension");
                    _tematach.Columns.Add("FileSize");
                    _tematach.Columns.Add("UrlExternal");
                    _tematach.Columns.Add("IsPublic");
                    _tematach.Columns.Add("PrivateLevel");
                    _tematach.Columns.Add("IsFolder");
                    _tematach.Columns.Add("IsHidden");
                    _tematach.Columns.Add("IsReadonly");
                    _tematach.Columns.Add("IsSync");
                    _tematach.Columns.Add("SyncTime");
                    _tematach.Columns.Add("CloudPath");
                    _tematach.Columns.Add("CreatedDate");
                    _tematach.Columns.Add("ModifiedDate");
                    _tematach.Columns.Add("CreatedBy");
                    _tematach.Columns.Add("ModifiedBy");
                    for (var i = 0; i < files.Count; i++)
                    {
                        for (var j = 0; j < lstTitle.Count; j++)
                        {
                            if (i == lstTitle[j].STT)
                            {
                                var stream = files[i].OpenReadStream();
                                byte[] byteArr = Utilities.StreamToByteArray(stream);
                                var name = files[i].FileName;

                                ContractAttachments _attach = new ContractAttachments()
                                {
                                    AttachmentGuid = Guid.NewGuid(),
                                    RecordGuid = obj.RecordGuid,
                                    OrganizationGuid = _emp.OrganizationGuid,
                                    Title = lstTitle[j].Title,
                                    FileName = name,
                                    CreatedDate = DateTime.Now,
                                    CreatedBy = GetCreatedBy(_context),
                                    ModifiedDate = DateTime.Now,
                                    ModifiedBy = GetModifiedBy(_context),
                                    Attachment = byteArr,
                                    ModuleId = "HR",
                                    FileSize = byteArr.Length,
                                    FileExtension = System.IO.Path.GetExtension(files[i].FileName)
                                };
                                _tematach.Rows.Add(_attach.AttachmentGuid,
                                    _attach.ParentGuid,
                                    _attach.RecordGuid,
                                    _attach.OrganizationGuid,
                                    _attach.DepartmentGuid,
                                    _attach.ModuleId,
                                    _attach.Title,
                                    _attach.FileName,
                                    _attach.Attachment,
                                    _attach.FileExtension,
                                    _attach.FileSize,
                                    _attach.UrlExternal,
                                    _attach.IsPublic,
                                    _attach.PrivateLevel,
                                    _attach.IsFolder,
                                    _attach.IsHidden,
                                    _attach.IsReadonly,
                                    _attach.IsSync,
                                   DBNull.Value,
                                    _attach.CloudPath,
                                    DBNull.Value, DBNull.Value, _attach.CreatedBy, _attach.ModifiedBy);
                            }
                        }
                    }
                    _detailAttach.Value = _tematach;
                    _detailAttach.TypeName = "[dbo].[Attachment]";
                    var temattach = _context.SQLCOMMANDS.FromSql(sqlAttach, database, scheme, TableName, _detailAttach).FirstOrDefault();
                    if (temattach.Error == "")
                    {
                        msg.Title = "Thêm mới thành công.";
                    }
                    else
                    {
                        msg.Title = "Thêm mới không thành công."; msg.Error = true;
                    }
                }
            }
            catch (SqlException ex)
            {
                msg.Title = "Có lỗi khi đính kèm";
                msg.Object = ex;
                msg.Error = true;
            }
            return Json(msg);
        }
        [HttpGet]
        public FileResult DownloadItemAttachment(Guid? Id)
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var username = GetEmployeeLogin(_context);
                var gui = new Guid();
                string sql = "[dbo].[SP_Download_Attachment] @Database,@Scheme, @TableName, @OrganizationGuid, @AttachmentGuid, @Error";
                var Database = new SqlParameter("@Database", SqlDbType.VarChar); Database.Value = _appSettings.DBName_HR;
                var Scheme = new SqlParameter("@Scheme", SqlDbType.VarChar); Scheme.Value = "Sale";
                var TableName = new SqlParameter("@TableName", SqlDbType.VarChar); TableName.Value = "ProductDeparments_DOC";
                var OrganizationGuid = new SqlParameter("@OrganizationGuid", SqlDbType.UniqueIdentifier); OrganizationGuid.Value = username.OrganizationGuid;
                var AttachmentGuid = new SqlParameter("@AttachmentGuid", SqlDbType.UniqueIdentifier); AttachmentGuid.Value = Id == gui ? (Object)DBNull.Value : Id;
                var Error = new SqlParameter("@Error", SqlDbType.NVarChar); Error.Value = "";
                var rs = _context.AttachmentsDownloadView_.FromSql(sql, Database, Scheme, TableName, OrganizationGuid, AttachmentGuid, Error).SingleOrDefault();
                if (rs != null)
                {
                    byte[] fileBytes = rs.Attachment;
                    var fileName = rs.FileName;
                    return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);

                }
                else return null;
            }
            catch (Exception ex)
            {
                msg.Title = "Có lỗi khi lấy dữ liệu.";
                return null;
            }
        }
        [HttpPost]
        //  [Filters.ActionAuthorize(EAction.OPEN)]
        public JsonResult GetItemAttachment([FromBody] ContractAttachments obj)
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var username = GetEmployeeLogin(_context);
                string sql = "[HR].[SP_GET_Attachment_Calendar] @Database, @Scheme, @TableName, @OrganizationGuid, @RecordGuid, @Error";
                var database = new SqlParameter("@Database ", SqlDbType.NVarChar); CheckNullParameterStore(database, _appSettings.DBName_HR);
                var Scheme = new SqlParameter("@Scheme", SqlDbType.VarChar); Scheme.Value = "Sale";
                var TableName = new SqlParameter("@TableName", SqlDbType.VarChar); TableName.Value = "ProductDeparments_DOC";
                var OrganizationGuid = SqlPara("@OrganizationGuid", username.OrganizationGuid.ToString(), SqlDbType.NVarChar);
                var RecordGuid = SqlPara("@RecordGuid", obj.RecordGuid.ToString(), SqlDbType.NVarChar);
                var Error = new SqlParameter("@Error", SqlDbType.NVarChar); Error.Value = "";
                var rs = _context.AttachmentsView.FromSql(sql, database, Scheme, TableName, OrganizationGuid, RecordGuid, Error).ToList();
                return Json(rs);
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Title = ex.Message, Object = ex });
            }
        }
        [Area("Sale")]
        public object MoneyType(string q)
        {
            try
            {
                var lstItem = new List<Object_Jexcel>();
                var _obj = new Object_Jexcel() { id = "VND", name = "Tiền VND" };
                var _obj1 = new Object_Jexcel() { id = "USD", name = "Tiền USD" };
                var _obj2 = new Object_Jexcel() { id = "JPY", name = "Tiền JPY" };

                lstItem.Add(_obj); lstItem.Add(_obj1); lstItem.Add(_obj2);
                return Json(lstItem);
            }
            catch (Exception ex)
            {
                return Json(new JMessage { Title = "Lấy dữ liệu không thành công", Object = ex, Error = true });
            }
        }
        [HttpPost]
        public object GetAllPaymentOrders([FromBody] PaymentOrders obj)
        {
            try
            {
                string sql = "[Sale].[SP_ESSM_PaymentOrders_GetAll] @RecordGuid";
                var RecordGuid = SqlPara("@RecordGuid",obj.RecordGuid, SqlDbType.UniqueIdentifier);                             
                var rs = _context.PaymentOrders.FromSql(sql, RecordGuid).ToList();              
                return Json(rs);
            }
            catch (Exception ex)
            {
                return new JMessage() { Error = true, Title = ex.Message, Object = ex };
            }
        }
        [HttpPost]
        public JsonResult Submit_PaymentOrders()
        {

            var msg = new JMessage() { Error = false };
            try
            {
                var _emp = GetEmployeeLogin(_context);
                var files = Request.Form.Files.ToList();
                var detail = Request.Form["detail"];
                List<PaymentOrders> lstdetail = JsonConvert.DeserializeObject<List<PaymentOrders>>(detail);
                
                string sql = "[Sale].[SP_ESSM_PaymentOrders_Insert] @OrganizationGuid,@PaymentOrders";
          
                var OrganizationGuid = SqlPara("@OrganizationGuid            ", _emp.OrganizationGuid, SqlDbType.UniqueIdentifier);               
                var _detail = new SqlParameter("@PaymentOrders", SqlDbType.Structured);
                var _tem = new System.Data.DataTable();
                _tem.Columns.Add("RowGuid      ");
                _tem.Columns.Add("RecordGuid   ");
                _tem.Columns.Add("OrganizationGuid       ");
                _tem.Columns.Add("PaymentDate     ", typeof(DateTime));
                _tem.Columns.Add("PaymentMoney        ");
                _tem.Columns.Add("PaymentPercent       ");
                _tem.Columns.Add("MoneyType     ");
                _tem.Columns.Add("UNC     ");              
                _tem.Columns.Add("CreatedDate ");
                _tem.Columns.Add("ModifiedDate    ");
                _tem.Columns.Add("CreatedBy   ");
                _tem.Columns.Add("ModifiedBy   ");
                _tem.Columns.Add("SortOrder    ");
                _tem.Columns.Add("Note    ");
                foreach (var _item in lstdetail)
                {
                    _tem.Rows.Add(
                            _item.RowGuid
                            , _item.RecordGuid
                            , _item.OrganizationGuid
                            , _item.PaymentDate
                            , _item.PaymentMoney
                            , _item.PaymentPercent
                            , _item.MoneyType
                            , _item.UNC                          
                            , DBNull.Value
                            , DBNull.Value
                            , DBNull.Value
                            , DBNull.Value
                            , _item.SortOrder
                             , _item.Note
                             );
                }
                _detail.Value = _tem;
                _detail.TypeName = "[Sale].[PaymentOrders]";

                var _rs = _context.SQLCOMMANDS.FromSql(sql, OrganizationGuid, _detail).FirstOrDefault();
                if (_rs.Error == "")
                {
                    msg.Title = "Cập nhật thành công";
                }
                else
                {
                    return Json(new JMessage() { Error = true, Title = _rs.Error, Object = _rs.Error });
                }
            }
            catch (Exception ex)
            {
                msg.Error = true;
                msg.Object = ex;
                msg.Title = ex.Message;
            }
            return Json(msg);
        }

        [HttpPost]
        public object JTable_ProductDeparmentsProcess([FromBody] JTableSearch jTablePara)
        {
            var emp = GetEmployeeLogin(_context);
            if (jTablePara.Keyword !="" && jTablePara.Keyword !=null)
            {
                var count = _context.ProductDeparmentsProcess.Where(a =>( a.ProductID.Contains(jTablePara.Keyword)
                                || a.ProductName.Contains(jTablePara.Keyword))).Count();
                int intBeginFor = (jTablePara.CurrentPage - 1) * jTablePara.Length;
                var balance = _context.ProductDeparmentsProcess.Where(a => (a.ProductID.Contains(jTablePara.Keyword)
                                || a.ProductName.Contains(jTablePara.Keyword))).Select(a => new { a.RowGuid, a.ProductID, a.ProductName, a.Packing, a.ProductProcess, a.Preservation, a.Note, a.EmployeeName }).OrderUsingSortExpression(jTablePara.QueryOrderBy).Skip(intBeginFor).Take(jTablePara.Length).ToList();
                var jdata = JTableHelper.JObjectTable(balance, jTablePara.Draw, count, "RowGuid", "ProductID", "ProductName", "Packing", "ProductProcess", "Preservation", "Note", "EmployeeName");
                return Json(jdata);
            }
            else
            {
                var count = _context.ProductDeparmentsProcess.Count();
                int intBeginFor = (jTablePara.CurrentPage - 1) * jTablePara.Length;
                var balance = _context.ProductDeparmentsProcess.Select(a => new { a.RowGuid, a.ProductID, a.ProductName, a.Packing, a.ProductProcess, a.Preservation, a.Note, a.EmployeeName }).OrderUsingSortExpression(jTablePara.QueryOrderBy).Skip(intBeginFor).Take(jTablePara.Length).ToList();
                var jdata = JTableHelper.JObjectTable(balance, jTablePara.Draw, count, "RowGuid", "ProductID", "ProductName", "Packing", "ProductProcess", "Preservation", "Note", "EmployeeName");
                return Json(jdata);
            }
        }
        [HttpPost]
        public JsonResult Submit_ProductDeparmentsProcess()
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var _emp = GetEmployeeLogin(_context);
                var files = Request.Form.Files.ToList();
                var submit = Request.Form["submit"];
                ProductDeparmentsProcess obj = JsonConvert.DeserializeObject<ProductDeparmentsProcess>(submit);
                string sql = "[Sale].[SP_ESSM_ProductDeparmentsProcess_Insert] @RowGuid,@ProductID,@ProductName,@Packing,@ProductProcess,@Preservation,@Note,@EmployeeName,@CreatedDate,@ModifiedDate,@CreatedBy,@ModifiedBy";
                obj.CreatedDate = DateTime.Now;
                obj.ModifiedDate = DateTime.Now;
                obj.CreatedBy = GetCreatedBy(_context);
                obj.ModifiedBy = GetCreatedBy(_context);
                var RowGuid = SqlPara("@RowGuid                     ", obj.RowGuid, SqlDbType.UniqueIdentifier);
                var ProductID = SqlPara("@ProductID                      ", obj.ProductID, SqlDbType.NVarChar);
                var ProductName = SqlPara("@ProductName                      ", obj.ProductName, SqlDbType.NVarChar);
                var Packing = SqlPara("@Packing                      ", obj.Packing, SqlDbType.NVarChar);
                var ProductProcess = SqlPara("@ProductProcess                       ", obj.ProductProcess, SqlDbType.NVarChar);
                var Preservation = SqlPara("@Preservation                  ", obj.Preservation, SqlDbType.NVarChar);
                var Note = SqlPara("@Note             ", obj.Note, SqlDbType.NVarChar);
                var EmployeeName = SqlPara("@EmployeeName             ", obj.EmployeeName, SqlDbType.NVarChar);              
                var CreatedDate = SqlPara("@CreatedDate                 ", obj.CreatedDate, SqlDbType.DateTime);
                var ModifiedDate = SqlPara("@ModifiedDate                ", obj.ModifiedDate, SqlDbType.DateTime);
                var CreatedBy = SqlPara("@CreatedBy                   ", obj.CreatedBy, SqlDbType.NVarChar);
                var ModifiedBy = SqlPara("@ModifiedBy                  ", obj.ModifiedBy, SqlDbType.NVarChar);
                var _rs = _context.SQLCOMMANDS.FromSql(sql, RowGuid, ProductID, ProductName
                  , Packing
                  , ProductProcess
                  , Preservation
                  , Note
                  , EmployeeName
                  , CreatedDate
                  , ModifiedDate
                  , CreatedBy
                  , ModifiedBy).FirstOrDefault();
                if (_rs.Error == "")
                {
                    msg.Title = "Thêm mới thành công";
                }
                else
                {
                    return Json(new JMessage() { Error = true, Title = _rs.Error, Object = _rs.Error });
                }
            }
            catch (Exception ex)
            {
                msg.Error = true;
                msg.Object = ex;
                msg.Title = ex.Message;
            }
            return Json(msg);
        }

        [HttpPost]
        public JsonResult GetItem_ProductDeparmentsProcess(Guid Id)
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var _emp = GetEmployeeLogin(_context);
                var rs = _context.ProductDeparmentsProcess.Where(x => x.RowGuid == Id).FirstOrDefault();
                return Json(rs);
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = ex.Message });
            }
        }
        [HttpPost]
        public object Delete_ProductDeparmentsProcess(Guid Id)
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var _emp = GetEmployeeLogin(_context);
               
                var _obj = _context.ProductDeparmentsProcess.Where(x => x.RowGuid == Id).FirstOrDefault();
                _context.ProductDeparmentsProcess.Remove(_obj);
                _context.SaveChanges();
                return Json(new JMessage() { Error = false, Title = "Xóa thành công" });
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = ex.Message });
            }
        }

    }

}
