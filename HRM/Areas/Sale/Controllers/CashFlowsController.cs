﻿using FTU.Utils.HelperNet;
using HRM.Controllers;
using HRM.Models;
using HRM.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;
using System.Data;
using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Newtonsoft.Json;
using Grpc.Core;
using DocumentFormat.OpenXml.Drawing;
using ES_MODEL.HR.Custom.Wf;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using DocumentFormat.OpenXml.Drawing.Charts;
using ES_MODEL.Task;
using HRM.Filters;
using ES_MODEL.HR;
using Aspose.Pdf;
using DocumentFormat.OpenXml.Bibliography;
using DocumentFormat.OpenXml.Wordprocessing;
using Microsoft.CodeAnalysis;
using DocumentFormat.OpenXml.VariantTypes;
using static Aspose.Pdf.Operator;
using System.Threading;
using HRM.Models.Custom.Hr;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Math;
using System.Web;
using DocumentFormat.OpenXml.Spreadsheet;
using Stimulsoft.Report.Dashboard;
using DocumentFormat.OpenXml.Drawing.Spreadsheet;
using static Stimulsoft.Report.StiOptions.Wpf;
using Stimulsoft.System.Web.UI.WebControls;
using Items = HRM.Models.Items;

namespace HRM.Areas.Sale.Controllers
{
    [Authorize]
    public class CashFlowsController : BaseController
    {
        public IConfigurationRoot Configuration { get; }
        private readonly HRMDBContext _context;
        private readonly IHostingEnvironment _environment;
        private readonly AppSettings _appSettings;

        public CashFlowsController(IOptions<AppSettings> appSettings, HRMDBContext context, IHostingEnvironment environment)
        {
            _context = context;
            _environment = environment;
            _appSettings = appSettings.Value;
        }
        [Area("Sale")]
        [ActionAuthorize(EAction.ACCESS)]
        public IActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public JsonResult GetUnit()
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var _emp = GetEmployeeLogin(_context);
                string sql = "[Sale].[SP_ESHR_Unit_GetAll] @OrganizationGuid";
                var OrganizationGuid = SqlPara("@OrganizationGuid", _emp.OrganizationGuid, SqlDbType.UniqueIdentifier);
                var rs = _context.Object_Combobox.FromSql(sql, OrganizationGuid).ToList();
                return Json(rs);
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = ex.Message });
            }
        }

        public class JTableCustomer : JTableModel
        {
            public string Keyword { set; get; }
            public string IsBussiness { set; get; }
            public string Status { set; get; }
            public string Type { set; get; }
            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }

        }
        [HttpPost]
        public object JTable([FromBody] JTableCustomer jTable)
        {
            try
            {
                var _emp = GetEmployeeLogin(_context);
                int intBeginFor = (jTable.CurrentPage - 1) * jTable.Length;
                if (jTable.StartDate != null)
                {
                    jTable.StartDate = jTable.StartDate.Value.ToLocalTime();
                }
                else
                {
                    jTable.StartDate = null;
                }
                if (jTable.EndDate != null)
                {
                    jTable.EndDate = jTable.EndDate.Value.ToLocalTime();
                }
                else
                {
                    jTable.EndDate = null;
                }
                string sql = "[Sale].[SP_ESSM_CashFlows_viewJTable] " +
                   "@Keyword, " +
                   "@Status, " +
                  "@StartDate, " +
                    "@EndDate, " +
                     "@Type, " +
                      "@Sort, " +
                   "@Skip, " +
                   "@Take," +
                   "@Count out";
                var _Keyword = SqlPara("@Keyword", jTable.Keyword, SqlDbType.NVarChar);
                var _Status = SqlPara("@Status", jTable.Status, SqlDbType.NVarChar);
                var StartDate = new SqlParameter("@StartDate", SqlDbType.Date); CheckNullParameterStore(StartDate, jTable.StartDate);
                var EndDate = new SqlParameter("@EndDate", SqlDbType.Date); CheckNullParameterStore(EndDate, jTable.EndDate);
                var Type = SqlPara("@Type", jTable.Type, SqlDbType.NVarChar);
                var _Sort = new SqlParameter("@Sort", SqlDbType.VarChar); CheckNullParameterStore(_Sort, jTable.QueryOrderBy);

                var _Skip = SqlPara("@Skip", intBeginFor, SqlDbType.Int);
                var _Take = SqlPara("@Take", jTable.Length, SqlDbType.Int);
                var _output = new SqlParameter("@Count", SqlDbType.Int) { Direction = System.Data.ParameterDirection.Output };

                var data = _context.CashFlows.FromSql(
                            sql,
                            _Keyword,
                            _Status,
                            StartDate,
                              EndDate,
                                Type,
                                _Sort,
                            _Skip,
                            _Take,
                            _output
                ).ToList();
                var jdata = JTableHelper.JObjectTable(data, jTable.Draw, (int)_output.Value);
                return Json(jdata);
            }
            catch (Exception ex)
            {
                return new JMessage() { Error = true, Title = ex.Message, Object = ex };
            }
        }
        [HttpPost]
        public object getTotalDT([FromBody] JTableCustomer jTable)
        {
            try
            {
                if (jTable.StartDate != null)
                {
                    jTable.StartDate = jTable.StartDate.Value.ToLocalTime();
                }
                else
                {
                    jTable.StartDate = null;
                }
                if (jTable.EndDate != null)
                {
                    jTable.EndDate = jTable.EndDate.Value.ToLocalTime();
                }
                else
                {
                    jTable.EndDate = null;
                }
                string sql = "[Sale].[SP_ESSM_CashFlows_getTotalDT] " +
                   "@Keyword, " +
                   "@Status, " +
                  "@StartDate, " +
                    "@EndDate, " +
                     "@Type "  ;
                var _Keyword = SqlPara("@Keyword", jTable.Keyword, SqlDbType.NVarChar);
                var _Status = SqlPara("@Status", jTable.Status, SqlDbType.NVarChar);
                var StartDate = new SqlParameter("@StartDate", SqlDbType.Date); CheckNullParameterStore(StartDate, jTable.StartDate);
                var EndDate = new SqlParameter("@EndDate", SqlDbType.Date); CheckNullParameterStore(EndDate, jTable.EndDate);
                var Type = SqlPara("@Type", jTable.Type, SqlDbType.NVarChar);
                var data = _context.CashFlowsTotals.FromSql(sql, _Keyword,_Status,StartDate,EndDate,Type ).FirstOrDefault();
                return Json(data);
            }
            catch (Exception ex)
            {
                return new JMessage() { Error = true, Title = ex.Message, Object = ex };
            }
        }
        [HttpPost]
        public JsonResult GetItem(Guid Id)
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var _emp = GetEmployeeLogin(_context);
                string sql = "[Sale].[SP_ESHR_CashFlows_GetItem] @RowGuid";
                var RowGuid = SqlPara("@RowGuid", Id, SqlDbType.UniqueIdentifier);
                var rs = _context.Object_Table.FromSql(sql, RowGuid).FirstOrDefault();
                return Json(rs);
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult Submit()
        {

            var msg = new JMessage() { Error = false };
            try
            {
                var _emp = GetEmployeeLogin(_context);
                var files = Request.Form.Files.ToList();
                var submit = Request.Form["submit"];
                CashFlows obj = JsonConvert.DeserializeObject<CashFlows>(submit);
                if (obj.DateHD != null)
                {
                    obj.DateHD = obj.DateHD.Value.ToLocalTime();
                }
                else
                {
                    obj.DateHD = null;
                }
                string sql = "[Sale].[SP_ESSM_CashFlows_Insert]" +
               "@RowGuid" +
               ",@DateHD" +
               ",@Types" +
               ",@TypesDT" +
               ",@Description" +
               ",@AmountOc" +
               ",@Note" +
               ",@CreatedDate" +
               ",@CreatedBy" +
               ",@ModifiedDate" +
               ",@ModifiedBy";
                obj.CreatedDate = DateTime.Now;
                obj.ModifiedDate = DateTime.Now;
                obj.CreatedBy = GetCreatedBy(_context);
                obj.ModifiedBy = GetCreatedBy(_context);
                var RowGuid = SqlPara("@RowGuid", obj.RowGuid, SqlDbType.UniqueIdentifier);
                var DateHD = SqlPara("@DateHD        ", obj.DateHD, SqlDbType.Date);
                var Types = SqlPara("@Types      ", obj.Types, SqlDbType.NVarChar);
                var TypesDT = SqlPara("@TypesDT", obj.TypesDT, SqlDbType.NVarChar);
                var Description = SqlPara("@Description        ", obj.Description, SqlDbType.NVarChar);
                var AmountOc = SqlPara("@AmountOc      ", obj.AmountOc, SqlDbType.Decimal);
                var Note = SqlPara("@Note      ", obj.Note, SqlDbType.NVarChar);
                var CreatedDate = SqlPara("@CreatedDate   ", obj.CreatedDate, SqlDbType.DateTime);
                var CreatedBy = SqlPara("@CreatedBy     ", obj.CreatedBy, SqlDbType.NVarChar);
                var ModifiedDate = SqlPara("@ModifiedDate  ", obj.ModifiedDate, SqlDbType.DateTime);
                var ModifiedBy = SqlPara("@ModifiedBy    ", obj.ModifiedBy, SqlDbType.NVarChar);

                var _rs = _context.SQLCOMMANDS.FromSql(sql, RowGuid, DateHD, Types, TypesDT, Description, AmountOc, Note, CreatedDate, ModifiedDate, CreatedBy, ModifiedBy).FirstOrDefault();
                if (_rs.Error == "")
                {
                    msg.Title = "Cập nhật thành công";
                }
                else
                {
                    return Json(new JMessage() { Error = true, Title = _rs.Error, Object = _rs.Error });
                }
            }
            catch (Exception ex)
            {
                msg.Error = true;
                msg.Object = ex;
                msg.Title = ex.Message;
            }
            return Json(msg);
        }

        [HttpPost]
        public object Delete(Guid? Id)
        {
            var msg = new JMessage() { Error = false };
            try
            {
                string sql = "[Sale].[SP_ESSM_CashFlows_Delete] @RowGuid";
                var RowGuid = SqlPara("@RowGuid", Id, SqlDbType.UniqueIdentifier);
                var __tem = _context.SQLCOMMANDS.FromSql(sql, RowGuid).FirstOrDefault();
                if (__tem.Error == "")
                {
                    return new JMessage() { Error = false, Title = "Xóa thành công." };
                }
                else
                {
                    return new JMessage() { Error = true, Title = __tem.Error, Object = __tem };
                }
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = ex.Message });
            }
        }
    }
}
