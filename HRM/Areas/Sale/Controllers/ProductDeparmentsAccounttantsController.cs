﻿using FTU.Utils.HelperNet;
using HRM.Controllers;
using HRM.Models;
using HRM.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;
using System.Data;
using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Newtonsoft.Json;
using Grpc.Core;
using DocumentFormat.OpenXml.Drawing;
using ES_MODEL.HR.Custom.Wf;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using DocumentFormat.OpenXml.Drawing.Charts;
using ES_MODEL.Task;
using HRM.Filters;
using ES_MODEL.HR;
using Aspose.Pdf;
using DocumentFormat.OpenXml.Bibliography;
using DocumentFormat.OpenXml.Wordprocessing;
using Microsoft.CodeAnalysis;
using DocumentFormat.OpenXml.VariantTypes;
using static Aspose.Pdf.Operator;
using System.Threading;
using HRM.Models.Custom.Hr;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Math;
using System.Web;
using DocumentFormat.OpenXml.Spreadsheet;
using Stimulsoft.Report.Dashboard;
using DocumentFormat.OpenXml.Drawing.Spreadsheet;
using static Stimulsoft.Report.StiOptions.Wpf;
using Stimulsoft.System.Web.UI.WebControls;
using Items = HRM.Models.Items;
using DocumentFormat.OpenXml.ExtendedProperties;
using DataTable = System.Data.DataTable;

namespace HRM.Areas.Sale.Controllers
{
    [Authorize]
    public class ProductDeparmentsAccountantsController : BaseController
    {
        public IConfigurationRoot Configuration { get; }
        private readonly HRMDBContext _context;
        private readonly IHostingEnvironment _environment;
        private readonly AppSettings _appSettings;

        public ProductDeparmentsAccountantsController(IOptions<AppSettings> appSettings, HRMDBContext context, IHostingEnvironment environment)
        {
            _context = context;
            _environment = environment;
            _appSettings = appSettings.Value;
        }
        [Area("Sale")]
        [ActionAuthorize(EAction.ACCESS)]
        public IActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public object Update_VATAmountOC([FromBody] ProductDeparmentDetails obj)
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var _emp = GetEmployeeLogin(_context);
                var _ob = _context.ProductKSCDetails.Where(x => x.RowGuid == obj.RowGuid).FirstOrDefault();
                _ob.VATAmountOC = obj.VATAmountOC;
                _context.ProductKSCDetails.Update(_ob);
                _context.SaveChanges();
                return Json(new JMessage() { Error = false, Title = "Cập nhật thành công" });
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = ex.Message });
            }
        }
        
    }
}
