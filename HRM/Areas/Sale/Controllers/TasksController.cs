﻿using FTU.Utils.HelperNet;
using HRM.Controllers;
using HRM.Models;
using HRM.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;
using System.Data;
using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Newtonsoft.Json;
using Grpc.Core;
using DocumentFormat.OpenXml.Drawing;
using ES_MODEL.HR.Custom.Wf;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using DocumentFormat.OpenXml.Drawing.Charts;
using ES_MODEL.Task;
using HRM.Filters;
using ES_MODEL.HR;
using Aspose.Pdf;
using DocumentFormat.OpenXml.Bibliography;
using DocumentFormat.OpenXml.Wordprocessing;
using Microsoft.CodeAnalysis;
using DocumentFormat.OpenXml.VariantTypes;
using static Aspose.Pdf.Operator;
using System.Threading;
using HRM.Models.Custom.Hr;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Math;
using System.Web;

namespace HRM.Areas.Sale.Controllers
{
    [Authorize]
    public class TasksController : BaseController
    {
        private readonly HRMDBContext _context;
        private readonly IHostingEnvironment environment;
        private readonly AppSettings _appSettings;
        public TasksController(HRMDBContext _db, IHostingEnvironment _environment, IOptions<AppSettings> appSettings)
        {
            _context = _db;
            environment = _environment;
            _appSettings = appSettings.Value;
        }
        #region
        [Area("Sale")]
         [ActionAuthorize(EAction.ACCESS)]
        public IActionResult Index()
        {
            return View();
        }

        //#endregion
        //#region Function
        ///// <summary>
        ///// Thông tin nhân viên đăng nhập
        ///// </summary>
        ///// <returns></returns>
        [Area("Cooperation")]
        [HttpPost]
        public object GetEmployeeLogin()
        {
            try
            {
                var _rs = GetEmployeeLogin(_context);
                return new JMessage() { Error = false, Title = "Lấy thông tin người đăng nhập thành công", Object = _rs };
            }
            catch (Exception ex)
            {
                return new JMessage() { Error = true, Title = ex.Message, Object = ex.ToString() };
            }
        }

        //public class TaskCustomEmplioyee
        //{
        //    public int? Id { get; set; }
        //    public int? ID { get; set; }
        //    public string EmployeeId { get; set; }
        //    public string FullName { get; set; }
        //    public string LoginName { get; set; }
        //}
        ///// <summary>
        ///// Lấy danh sách nhân viên
        ///// </summary>
        ///// <returns></returns>
        [HttpPost]
        [Area("Cooperation")]
        public object GetEmployee()
        {
            try
            {
                var _emp = GetEmployeeLogin(_context);

                string _sql = "exec [Cooperation].[SP_ESHR_Tasks_GetEmployee] " +
                    "@OrganizationGuid";
                var _OrganizationGuid = new SqlParameter("@OrganizationGuid", SqlDbType.UniqueIdentifier);
                CheckNullParameterStore(_OrganizationGuid, _emp.OrganizationGuid);
                var _rs = _context.Object_Combobox.FromSql(_sql,
                    _OrganizationGuid
                    ).ToList();
                return _rs;
            }
            catch (Exception ex)
            {
                return new JMessage() { Error = true, Title = ex.Message, Object = ex.ToString() };
            }
        }
        public class CustomPage
        {
            public int Page { set; get; }
            public int ItemPage { set; get; }
            public string Search { set; get; }
            public DateTime? StartDate { set; get; }
            public DateTime? EndDate { set; get; }
            public string Priority { get; set; }
            public string Process { get; set; }
            public Guid? ProfileGuid { get; set; }
            public Guid? CategoryOfTaskGuid { get; set; }
            public string Assigned { get; set; }
            public string OtherId { get; set; }
            public string Sort { get; set; }
            public string TaskGuid { get; set; }
            public string CompaignId { get; set; }
            public string State { get; set; }
            public List<_filterData> filterData;
            public List<string> Tags { get; set; }
        }
        [HttpPost]
        [Area("Cooperation")]
        public object GetAll([FromBody] CustomPage obj)
        {
            List<string> Assigned = new List<string>() { "A", "B", "C" };
            try
            {
                var Permisstion = "0";
                var controller = ControllerContext.RouteData.Values["controller"].ToString();
                var ListPermission = WebContext.CheckPermission(ControllerContext.RouteData.Values["controller"].ToString());
                var per = ListPermission.Contains(EAction.FULLCONTROL.ToString());
                if (per == true)
                {
                    Permisstion = "1";
                }
                var startpoint = (obj.Page - 1) * obj.ItemPage;
                var _emp = GetEmployeeLogin(_context);
                string _sql = "exec [Cooperation].[SP_ESTM_TASK_GetTasks] " +
                        "@OrganizationGuid, " +
                        "@EmployeeGuid, " +
                        "@LoginName, " +
                        "@Page, " +
                        "@ItemPage, " +
                        "@Search, " +
                        "@StartDate, " +
                        "@EndDate, " +
                        "@Priority, " +
                        "@Process, " +
                        "@ProfileGuid, " +
                        "@CategoryOfTaskGuid, " +
                        "@Assigned, " +
                        "@OtherId, " +
                        "@Sort, " +
                        "@Skip, " +
                        "@Take, " +
                        "@LiTaskGuid," +
                        "@State," +
                        "@CompaignId," +
                         "@Tags," +
                          "@Permisstion," +
                        "@CountRow out," +
                        "@FilterTable";
                if (obj.StartDate.HasValue) { obj.StartDate = obj.StartDate.Value.ToLocalTime(); } else { obj.StartDate = null; }
                if (obj.EndDate.HasValue) { obj.EndDate = obj.EndDate.Value.ToLocalTime(); } else { obj.EndDate = null; }
                var _OrganizationGuid = new SqlParameter("@OrganizationGuid", SqlDbType.UniqueIdentifier);
                CheckNullParameterStore(_OrganizationGuid, _emp.OrganizationGuid);

                var _EmployeeGuid = new SqlParameter("@EmployeeGuid", SqlDbType.UniqueIdentifier);
                CheckNullParameterStore(_EmployeeGuid, _emp.EmployeeGuid);

                var _LoginName = new SqlParameter("@LoginName", SqlDbType.NVarChar);
                CheckNullParameterStore(_LoginName, _emp.LoginName);

                var _Page = new SqlParameter("@Page", SqlDbType.Int);
                CheckNullParameterStore(_Page, obj.Page);

                var _ItemPage = new SqlParameter("@ItemPage", SqlDbType.Int);
                CheckNullParameterStore(_ItemPage, obj.ItemPage);

                var _Search = new SqlParameter("@Search", SqlDbType.NVarChar);
                CheckNullParameterStore(_Search, obj.Search);
           
                var _StartDate = new SqlParameter("@StartDate", SqlDbType.DateTime);
                CheckNullParameterStore(_StartDate, obj.StartDate);

                var _EndDate = new SqlParameter("@EndDate", SqlDbType.DateTime);
                CheckNullParameterStore(_EndDate, obj.EndDate);

                var _Priority = new SqlParameter("@Priority", SqlDbType.NVarChar);
                CheckNullParameterStore(_Priority, obj.Priority);

                var _Process = new SqlParameter("@Process", SqlDbType.NVarChar);
                CheckNullParameterStore(_Process, obj.Process);

                var _ProfileGuid = new SqlParameter("@ProfileGuid", SqlDbType.UniqueIdentifier);
                CheckNullParameterStore(_ProfileGuid, obj.ProfileGuid);

                var _CategoryOfTaskGuid = new SqlParameter("@CategoryOfTaskGuid", SqlDbType.UniqueIdentifier);
                CheckNullParameterStore(_CategoryOfTaskGuid, obj.CategoryOfTaskGuid);

                var _Assigned = new SqlParameter("@Assigned", SqlDbType.NVarChar);
                CheckNullParameterStore(_Assigned, obj.Assigned);

                var _OtherId = new SqlParameter("@OtherId", SqlDbType.NVarChar);
                CheckNullParameterStore(_OtherId, obj.OtherId);

                var _Sort = new SqlParameter("@Sort", SqlDbType.NVarChar);
                CheckNullParameterStore(_Sort, obj.Sort);

                var _Skip = new SqlParameter("@Skip", SqlDbType.Int);
                CheckNullParameterStore(_Skip, startpoint);

                var _Take = new SqlParameter("@Take", SqlDbType.Int);
                CheckNullParameterStore(_Take, obj.ItemPage);

                var _LiTaskGuid = new SqlParameter("@LiTaskGuid", SqlDbType.NVarChar);
                CheckNullParameterStore(_LiTaskGuid, obj.TaskGuid);

                var _State = new SqlParameter("@State", SqlDbType.NVarChar);
                CheckNullParameterStore(_State, obj.State);

                var _CompaignId = new SqlParameter("@CompaignId", SqlDbType.NVarChar);
                CheckNullParameterStore(_CompaignId, obj.CompaignId);

                var _Tags = new SqlParameter("@Tags", SqlDbType.NVarChar);
                var _Permisstion =SqlPara("@Permisstion", Permisstion, SqlDbType.NVarChar);
                

                string _tag_list = "";
                if (obj.Tags != null)
                {
                    for (int i = 0; i < obj.Tags.Count(); i++)
                    {
                        _tag_list += obj.Tags[i] + ";";
                    }
                }
                CheckNullParameterStore(_Tags, _tag_list);

                var _CountRow = new SqlParameter("@CountRow", SqlDbType.Int) { Direction = ParameterDirection.Output };
                CheckNullParameterStore(_CountRow, "0");

                var _FilterTable = new SqlParameter("@FilterTable", SqlDbType.Structured);
                Fun_Table_Filter.Set_ColumnFilter(obj.filterData, _FilterTable);

                var o = _context.GetTasks.FromSql(_sql,
                        _OrganizationGuid,
                        _EmployeeGuid,
                        _LoginName,
                        _Page,
                        _ItemPage,
                        _Search,
                        _StartDate,
                        _EndDate,
                        _Priority,
                        _Process,
                        _ProfileGuid,
                        _CategoryOfTaskGuid,
                        _Assigned,
                        _OtherId,
                        _Sort,
                        _Skip,
                        _Take,
                        _LiTaskGuid,
                        _State,
                        _CompaignId,
                        _CountRow,
                        _Tags,
                        _Permisstion,
                        _FilterTable
                    )
                    .ToList();

                return new JMessage() { Error = false, Title = "Lấy dữ liệu thành công", Object = o, Data = _CountRow.Value /* RemoveChildTask(o)*/ };
            }
            catch (Exception ex)
            {
                return new JMessage() { Error = true, Title = "Lấy dữ liệu không thành công", Object = ex };
            }
        }
        /// Lấy thông tin của bản ghi
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        [Area("Cooperation")]
        public object GetItem([FromBody] TempSub obj)
        {
            JMessage _rs = new JMessage() { };
            try
            {
                Guid _guid = new Guid(obj.IdS[0]);
                var _emp = GetEmployeeLogin(_context);
                var _taskofUser = _context.TaskOfUsers
                        .Where(x => x.TaskGuid == _guid && x.LoginName.ToUpper() == _emp.LoginName.ToUpper())
                        .FirstOrDefault();
                if (_taskofUser != null)
                {
                    _taskofUser.IsRead = true;
                    _context.Entry(_taskofUser);
                    _context.Entry(_taskofUser).Property(x => x.IsRead).IsModified = true;
                    _context.SaveChanges();
                }
                string _sql = "exec [Cooperation].[SP_TM_Get_GetItem] " +
                        "@OrganizationGuid, " +
                        "@LoginName, " +
                        "@RecordGuid, " +
                        "@Error out";

                var _OrganizationGuid = new SqlParameter("@OrganizationGuid", SqlDbType.UniqueIdentifier);
                CheckNullParameterStore(_OrganizationGuid, _emp.OrganizationGuid);

                var _LoginName = new SqlParameter("@LoginName", SqlDbType.NVarChar);
                CheckNullParameterStore(_LoginName, _emp.LoginName);

                var _RecordGuid = new SqlParameter("@RecordGuid", SqlDbType.UniqueIdentifier);
                CheckNullParameterStore(_RecordGuid, _guid);

                var _Error = new SqlParameter("@Error", SqlDbType.NVarChar) { Direction = ParameterDirection.Output };
                CheckNullParameterStore(_Error, "");

                var o = _context.GetItems.FromSql(_sql,
                        _OrganizationGuid,
                        _LoginName,
                        _RecordGuid,
                        _Error
                    )
                    .FirstOrDefault();

                _rs.Error = false;
                _rs.Title = "Lấy dữ liệu thành công";
                _rs.Object = o;
            }
            catch (Exception ex)
            {
                _rs.Error = true;
                _rs.Title = "Lấy dữ liệu không thành công";
                _rs.Object = ex.ToString();
            }
            return _rs;
        }
        // Lấy danh sách file đính kèm
        [Area("Cooperation")]
        [HttpPost]
        public object GetAttachments([FromBody] TempSub obj)
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var _emp = GetEmployeeLogin(_context);
                string sql = "[dbo].[SP_GET_Attachment] " +
                    "@Database," +
                    "@Scheme, " +
                    "@TableName, " +
                    "@OrganizationGuid, " +
                    "@RecordGuid, " +
                    "@Error";
                var Database = new SqlParameter("@Database", SqlDbType.NVarChar); Database.Value = _appSettings.DBName_HR;
                var scheme = new SqlParameter("@scheme ", SqlDbType.NVarChar); CheckNullParameterStore(scheme, "Cooperation");
                var TableName = new SqlParameter("@TableName ", SqlDbType.NVarChar); CheckNullParameterStore(TableName, "DocumentTaskAttachments");
                var OrganizationGuid = new SqlParameter("@OrganizationGuid", SqlDbType.VarChar); OrganizationGuid.Value = _emp.OrganizationGuid.ToString();
                var RecordGuid = new SqlParameter("@RecordGuid", SqlDbType.VarChar); CheckNullParameterStore(RecordGuid, obj.IdS[0]);
                var Error = new SqlParameter("@Error", SqlDbType.NVarChar); Error.Value = "";
                var rs = _context.OpenAttachmentsView
                    .FromSql(sql,
                        Database,
                        scheme,
                        TableName,
                        OrganizationGuid,
                        RecordGuid,
                        Error)
                    .ToList();
                msg.Error = false;
                msg.Title = "Lấy dữ liệu thành công.";
                msg.Object = rs;
            }
            catch (Exception ex)
            {
                msg.Title = "Có lỗi khi lấy dữ liệu.";
                msg.Error = true;
                msg.Object = ex.ToString();
            }
            return msg;
        }
        /// <summary>
        /// Kiểm tra quyền cho công việc
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        [Area("Cooperation")]
        //[ActionAuthorize(EAction.ACCESS)]
        public object CheckPermission([FromBody] TempSub obj)
        {
            JMessage _rs = new JMessage();
            try
            {
                var _guid = new Guid(obj.IdS[0]);
                var _emp = GetEmployeeLogin(_context);
                string _sqlComment = "exec [Cooperation].[SP_TM_Get_CheckPermission] " +
                 "@OrganizationGuid," +
                 "@LoginName," +
                 "@RecordGuid," +
                 "@Error out";

                var _OrganizationGuid = new SqlParameter("@OrganizationGuid", SqlDbType.UniqueIdentifier);
                CheckNullParameterStore(_OrganizationGuid, _emp.OrganizationGuid);

                var _LoginName = new SqlParameter("@LoginName", SqlDbType.NVarChar);
                CheckNullParameterStore(_LoginName, _emp.LoginName);

                var _RecordGuid = new SqlParameter("@RecordGuid", SqlDbType.UniqueIdentifier);
                CheckNullParameterStore(_RecordGuid, _guid);

                var _Error = new SqlParameter("@Error", SqlDbType.NVarChar) { Direction = ParameterDirection.Output };
                CheckNullParameterStore(_Error, "");

                var _d = _context.CheckPermissions.FromSql(_sqlComment,
                        _OrganizationGuid,
                        _LoginName,
                        _RecordGuid,
                        _Error)
                    .FirstOrDefault();

                _rs.Error = false;
                _rs.Title = "Lấy danh sách tin nhắn thành công";
                _rs.Object = _d;
            }
            catch (Exception ex)
            {
                _rs.Error = true;
                _rs.Title = "Lấy danh sách tin nhắn không thành công";
                _rs.Object = ex.ToString();
            }
            return _rs;
        }
        [HttpPost]
        [Area("Cooperation")]
        public object GetActivitiesByTask([FromBody] CommentPaging obj)
        {
            JMessage _rs = new JMessage();
            try
            {
                var _emp = GetEmployeeLogin(_context);
                string _sql = "exec [Cooperation].[SP_TM_Get_GetActivitiesByTask] " +
                    "@OrganizationGuid," +
                    "@LoginName," +
                    "@RecordGuid," +
                    "@Skip," +
                    "@Take," +
                    "@Error out";

                var _OrganizationGuid = new SqlParameter("@OrganizationGuid", SqlDbType.UniqueIdentifier);
                CheckNullParameterStore(_OrganizationGuid, _emp.OrganizationGuid);

                var _LoginName = new SqlParameter("@LoginName", SqlDbType.NVarChar);
                CheckNullParameterStore(_LoginName, _emp.LoginName);

                var _RecordGuid = new SqlParameter("@RecordGuid", SqlDbType.UniqueIdentifier);
                CheckNullParameterStore(_RecordGuid, obj.RecordGuid);

                var _Skip = new SqlParameter("@Skip", SqlDbType.Int);
                CheckNullParameterStore(_Skip, obj.Skip);

                var _Take = new SqlParameter("@Take", SqlDbType.Int);
                CheckNullParameterStore(_Take, obj.Take);

                var _Error = new SqlParameter("@Error", SqlDbType.NVarChar) { Direction = ParameterDirection.Output };
                CheckNullParameterStore(_Error, "");

                var _d = _context.ActivitiesCustoms.FromSql(_sql,
                        _OrganizationGuid,
                        _LoginName,
                        _RecordGuid,
                        _Skip,
                        _Take,
                        _Error)
                    .ToList();
                _rs.Error = false;
                _rs.Title = "Lấy dữ liệu thành công";
                _rs.Object = _d;
            }
            catch (Exception ex)
            {
                _rs.Error = true;
                _rs.Title = "Lấy dữ liệu không thành công";
                _rs.Object = ex.ToString();
            }
            return _rs;
        }
        [HttpPost]
        [Area("Cooperation")]
        public object ComList([FromBody] CommentPaging obj)
        {
            JMessage _rs = new JMessage();
            try
            {
                var _emp = GetEmployeeLogin(_context);
                string _sqlComment = "exec [Cooperation].[SP_TM_Get_GetTaskComments] " +
                 "@OrganizationGuid," +
                 "@LoginName," +
                 "@EmployeeGuid," +
                 "@RecordGuid," +
                 "@CommentGuid," +
                 "@Skip," +
                 "@Take," +
                 "@Count out," +
                 "@Error out";

                var _OrganizationGuid = new SqlParameter("@OrganizationGuid", SqlDbType.UniqueIdentifier);
                CheckNullParameterStore(_OrganizationGuid, _emp.OrganizationGuid);

                var _LoginName = new SqlParameter("@LoginName", SqlDbType.NVarChar);
                CheckNullParameterStore(_LoginName, _emp.LoginName);

                var _EmployeeGuid = new SqlParameter("@EmployeeGuid", SqlDbType.UniqueIdentifier);
                CheckNullParameterStore(_EmployeeGuid, _emp.EmployeeGuid);

                var _RecordGuid = new SqlParameter("@RecordGuid", SqlDbType.UniqueIdentifier);
                CheckNullParameterStore(_RecordGuid, obj.RecordGuid);

                var _CommentGuid = new SqlParameter("@CommentGuid", SqlDbType.UniqueIdentifier);
                CheckNullParameterStore(_CommentGuid, null);

                var _Skip = new SqlParameter("@Skip", SqlDbType.Int);
                CheckNullParameterStore(_Skip, obj.Skip);

                var _Take = new SqlParameter("@Take", SqlDbType.Int);
                CheckNullParameterStore(_Take, obj.Take);

                var _Count = new SqlParameter("@Count", SqlDbType.Int) { Direction = ParameterDirection.Output };
                CheckNullParameterStore(_Count, 0);

                var _Error = new SqlParameter("@Error", SqlDbType.NVarChar) { Direction = ParameterDirection.Output };
                CheckNullParameterStore(_Error, "");

                var _d = _context.GetTaskComments.FromSql(_sqlComment,
                        _OrganizationGuid,
                        _LoginName,
                        _EmployeeGuid,
                        _RecordGuid,
                        _CommentGuid,
                        _Skip,
                        _Take,
                        _Count,
                        _Error)
                    .ToList();

                _rs.Error = false;
                _rs.Title = "Lấy danh sách tin nhắn thành công";
                _rs.Object = new { Data = _d, Count = Convert.ToInt32(_Count.Value.ToString()) };
            }
            catch (Exception ex)
            {
                _rs.Error = true;
                _rs.Title = "Lấy danh sách tin nhắn không thành công";
                _rs.Object = ex.ToString();
            }
            return _rs;
        }
        [HttpPost]
        [Area("Cooperation")]
        public object GetAttachByType([FromBody] TempSub obj)
        {
            try
            {
                var _emp = GetEmployeeLogin(_context);
                string _sql = "exec [dbo].[SP_TM_GetAttachByType] " +
                    "@Database," +
                    "@scheme," +
                    "@TableName," +
                    "@OrganizationGuid," +
                    "@LoginName, " +
                    "@TaskGuid";

                var _database = new SqlParameter("@Database", SqlDbType.NVarChar, 50);
                _database.Value = _appSettings.DBName_HR;

                var _scheme = new SqlParameter("@scheme", SqlDbType.NVarChar, 250);
                CheckNullParameterStore(_scheme, "Cooperation");

                var _tableName = new SqlParameter("@TableName ", SqlDbType.NVarChar, 50);
                CheckNullParameterStore(_tableName, "TaskCommentAttachments");

                var _OrganizationGuid = new SqlParameter("@OrganizationGuid", SqlDbType.UniqueIdentifier);
                CheckNullParameterStore(_OrganizationGuid, _emp.OrganizationGuid);

                var _LoginName = new SqlParameter("@LoginName", SqlDbType.NVarChar);
                CheckNullParameterStore(_LoginName, _emp.LoginName);

                var _TaskGuid = new SqlParameter("@TaskGuid", SqlDbType.UniqueIdentifier);
                CheckNullParameterStore(_TaskGuid, new Guid(obj.IdS[0]));

                var _rs = _context.TaskAttachbyTypes.FromSql(_sql,
                        _database,
                        _scheme,
                        _tableName,
                        _OrganizationGuid,
                        _LoginName,
                        _TaskGuid)
                    .ToList();
                return new JMessage() { Error = false, Title = "Lấy dữ liệu thành công", Object = _rs };
            }
            catch (Exception ex)
            {
                return new JMessage() { Error = true, Title = "Lấy dữ liệu không thành công", Object = ex.ToString() };
            }
        }
        public class objTitleFile
        {
            public int? STT { get; set; }
            public string Title { get; set; }
        }
        [Area("Cooperation")]
        [HttpPost]
        public object Insert_CategoryOfTask()
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var _emp = GetEmployeeLogin(_context);
                var files = Request.Form.Files.ToList();
                var _Task = Request.Form["Task"];
                CategoryOfTasks obj = JsonConvert.DeserializeObject<CategoryOfTasks>(_Task);
                obj.CategoryOfTaskGuid = Guid.NewGuid();
                obj.OrganizationGuid = _emp.OrganizationGuid;
                obj.CreatedDate = DateTime.Now;
                obj.ModifiedDate = DateTime.Now;
                obj.CreatedBy = GetCreatedBy(_context);
                obj.ModifiedBy = GetCreatedBy(_context);
                _context.CategoryOfTasks.Add(obj);
                msg.Title = "Thêm mới thành công.";
            }
            catch (Exception ex)

            {
                return new { Object = ex, Title = ex.Message, Error = true };
            }
            return Json(msg);
        }
        [Area("Cooperation")]
        [HttpPost]
        public object Insert_Labels()
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var _emp = GetEmployeeLogin(_context);
                var files = Request.Form.Files.ToList();
                var _Task = Request.Form["Task"];
                Lables obj = JsonConvert.DeserializeObject<Lables>(_Task);
                obj.OrganizationGuid = _emp.OrganizationGuid;
                obj.CreatedDate = DateTime.Now;
                obj.ModifiedDate = DateTime.Now;
                obj.CreatedBy = GetCreatedBy(_context);
                obj.ModifiedBy = GetCreatedBy(_context);
                _context.Lables.Add(obj);
                msg.Title = "Thêm mới thành công.";
            }
            catch (Exception ex)

            {
                return new { Object = ex, Title = ex.Message, Error = true };
            }
            return Json(msg);
        }

        [Area("Cooperation")]
        [HttpPost]
        public object Insert()
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var _emp = GetEmployeeLogin(_context);
                var files = Request.Form.Files.ToList();
                var _Task = Request.Form["Task"];
                var _Lables = Request.Form["Lables"];
                var _TaskCheckLists = Request.Form["TaskCheckLists"];
                var lstTitlefile = Request.Form["lstTitlefile"];
                var _ListDeletefile = Request.Form["ListDeletefile"];
                Tasks obj = JsonConvert.DeserializeObject<Tasks>(_Task);
                List<TaskOfLabels> lstTaskOfLabels = JsonConvert.DeserializeObject<List<TaskOfLabels>>(_Lables);
                List<TaskCheckLists> lstTaskCheckLists = JsonConvert.DeserializeObject<List<TaskCheckLists>>(_TaskCheckLists);
                List<objTitleFile> lstTitle = JsonConvert.DeserializeObject<List<objTitleFile>>(lstTitlefile);
                List<string> lstDeletefile = JsonConvert.DeserializeObject<List<string>>(_ListDeletefile);
                var _TaskOfUsers = Request.Form["TaskOfUsers"];
                List<TaskOfUsers> lstTaskOfUsers = JsonConvert.DeserializeObject<List<TaskOfUsers>>(_TaskOfUsers);


                if (obj.StartDate.HasValue) { obj.StartDate = obj.StartDate.Value.ToLocalTime(); } else { obj.StartDate = null; }
                if (obj.EndDate.HasValue) { obj.EndDate = obj.EndDate.Value.ToLocalTime(); } else { obj.EndDate = null; }
                string sql = "[Sale].[SP_ESHR_Tasks_Insert]" +
                    "@TaskGuid, @ParentID, @ProjectGuid, @ProjectID, @OrganizationGuid, @DepartmentGuid, @DepartmentID, @DepartmentName, @EmployeeGuid, @EmployeeID, @EmployeeName, @CustomerGuid, @CustomerID, @CustomerName, @Subject, @TaskNumber, @TaskContent, @StartDate, @StartTime, @EndDate, @EndTime, @CompleteDate, @Priority, @SecurityLevel, @PercentComplete, @TimeReminder, @StatusCompleted, @Description, @Note, @Approval, @IsApproval, @ApprovalStatus, @IsRecall, @TaskLevel, @Mark, @KPIScore, @CategoryOfTaskGuid, @IsActive, @IsDraft, @IsLocked, @CreatedBy, @CreatedDate, @ModifiedBy, @ModifiedDate, @LoginName, @IsArchive, @TaskProcess, @AttachCount, @RecordGuid, @CompaignID, @ProcessDelete, @FinishDay,@lstTaskOfLabels,@lstTaskCheckLists,@lstDeleteFile,@TaskOfUsers,@Error out";
                obj.TaskGuid = Guid.NewGuid();
                var TaskGuid = SqlPara("@TaskGuid ", obj.TaskGuid, SqlDbType.UniqueIdentifier);
                var ParentID = SqlPara("@ParentID            ", obj.ParentId, SqlDbType.UniqueIdentifier);
                var ProjectGuid = SqlPara("@ProjectGuid         ", obj.ProjectGuid, SqlDbType.UniqueIdentifier);
                var ProjectID = SqlPara("@ProjectID           ", obj.ProjectId, SqlDbType.NVarChar);
                var OrganizationGuid = SqlPara("@OrganizationGuid    ", _emp.OrganizationGuid, SqlDbType.UniqueIdentifier);
                var DepartmentGuid = SqlPara("@DepartmentGuid      ", _emp.DepartmentGuid, SqlDbType.UniqueIdentifier);
                var DepartmentID = SqlPara("@DepartmentID        ", _emp.DepartmentId, SqlDbType.NVarChar);
                var DepartmentName = SqlPara("@DepartmentName      ", _emp.DepartmentName, SqlDbType.NVarChar);
                var EmployeeGuid = SqlPara("@EmployeeGuid        ", _emp.EmployeeGuid, SqlDbType.UniqueIdentifier);
                var EmployeeID = SqlPara("@EmployeeID          ", _emp.EmployeeId, SqlDbType.NVarChar);
                var EmployeeName = SqlPara("@EmployeeName        ", _emp.FullName, SqlDbType.NVarChar);
                var CustomerGuid = SqlPara("@CustomerGuid        ", obj.CustomerGuid, SqlDbType.UniqueIdentifier);
                var CustomerID = SqlPara("@CustomerID          ", obj.CustomerId, SqlDbType.NVarChar);
                var CustomerName = SqlPara("@CustomerName        ", obj.CustomerName, SqlDbType.NVarChar);
                var Subject = SqlPara("@Subject             ", obj.Subject, SqlDbType.NVarChar);
                var TaskNumber = SqlPara("@TaskNumber          ", obj.TaskNumber, SqlDbType.NVarChar);
                var TaskContent = SqlPara("@TaskContent         ", obj.TaskContent, SqlDbType.NVarChar);
                var StartDate = SqlPara("@StartDate           ", obj.StartDate, SqlDbType.Date);
                var StartTime = SqlPara("@StartTime           ", obj.StartTime, SqlDbType.Time);
                var EndDate = SqlPara("@EndDate             ", obj.EndDate, SqlDbType.Date);
                var EndTime = SqlPara("@EndTime             ", obj.EndTime, SqlDbType.Time);
                var CompleteDate = SqlPara("@CompleteDate        ", obj.CompleteDate, SqlDbType.Date);
                var Priority = SqlPara("@Priority            ", obj.Priority, SqlDbType.NVarChar);
                var SecurityLevel = SqlPara("@SecurityLevel       ", obj.SecurityLevel, SqlDbType.NVarChar);
                var PercentComplete = SqlPara("@PercentComplete     ", obj.PercentComplete, SqlDbType.Decimal);
                var TimeReminder = SqlPara("@TimeReminder        ", obj.TimeReminder, SqlDbType.DateTime);
                var StatusCompleted = SqlPara("@StatusCompleted     ", obj.StatusCompleted, SqlDbType.NVarChar);
                var Description = SqlPara("@Description         ", obj.Description, SqlDbType.NVarChar);
                var Note = SqlPara("@Note                ", obj.Note, SqlDbType.NVarChar);
                var Approval = SqlPara("@Approval            ", obj.Approval, SqlDbType.NVarChar);
                var IsApproval = SqlPara("@IsApproval          ", obj.IsApproval, SqlDbType.NVarChar);
                var ApprovalStatus = SqlPara("@ApprovalStatus      ", obj.ApprovalStatus, SqlDbType.NVarChar);
                var IsRecall = SqlPara("@IsRecall            ", obj.IsRecall, SqlDbType.Bit);
                var TaskLevel = SqlPara("@TaskLevel           ", obj.TaskLevel, SqlDbType.NVarChar);
                var Mark = SqlPara("@Mark                ", obj.Mark, SqlDbType.Decimal);
                var KPIScore = SqlPara("@KPIScore            ", obj.Kpiscore, SqlDbType.Decimal);
                var CategoryOfTaskGuid = SqlPara("@CategoryOfTaskGuid", obj.CategoryOfTaskGuid, SqlDbType.UniqueIdentifier);
                var IsActive = SqlPara("@IsActive            ", obj.IsActive, SqlDbType.Int);
                var IsDraft = SqlPara("@IsDraft             ", obj.IsDraft, SqlDbType.Bit);
                var IsLocked = SqlPara("@IsLocked            ", obj.IsLocked, SqlDbType.Bit);
                obj.CreatedBy = GetCreatedBy(_context); obj.CreatedDate = DateTime.Now;
                obj.ModifiedBy = GetCreatedBy(_context); obj.ModifiedDate = DateTime.Now;
                var CreatedBy = SqlPara("@CreatedBy           ", obj.CreatedBy, SqlDbType.NVarChar);
                var CreatedDate = SqlPara("@CreatedDate         ", obj.CreatedDate, SqlDbType.DateTime);
                var ModifiedBy = SqlPara("@ModifiedBy          ", obj.ModifiedBy, SqlDbType.NVarChar);
                var ModifiedDate = SqlPara("@ModifiedDate        ", obj.ModifiedDate, SqlDbType.DateTime);
                var LoginName = SqlPara("@LoginName           ", _emp.LoginName, SqlDbType.NVarChar);
                var IsArchive = SqlPara("@IsArchive           ", obj.IsArchive, SqlDbType.Bit);
                var TaskProcess = SqlPara("@TaskProcess         ", obj.TaskProcess, SqlDbType.UniqueIdentifier);
                var AttachCount = SqlPara("@AttachCount         ", obj.AttachCount, SqlDbType.Int);
                var RecordGuid = SqlPara("@RecordGuid          ", obj.RecordGuid, SqlDbType.UniqueIdentifier);
                var CompaignID = SqlPara("@CompaignID          ", obj.CompaignId, SqlDbType.NVarChar);
                var ProcessDelete = SqlPara("@ProcessDelete       ", obj.ProcessDelete, SqlDbType.Bit);
                var FinishDay = SqlPara("@FinishDay           ", obj.FinishDay, SqlDbType.DateTime);
                var Error = new SqlParameter("@Error", SqlDbType.NVarChar, 4000) { Direction = System.Data.ParameterDirection.Output };
                var _lstDeletefile = new SqlParameter("@lstDeleteFile", lstDeletefile.Count >= 1 ? string.Join(",", lstDeletefile) : "");

                var _detail = new SqlParameter("@lstTaskOfLabels", SqlDbType.Structured);
                var _tem = new System.Data.DataTable();
                _tem.Columns.Add("RowGuid");
                _tem.Columns.Add("TaskGuid");
                _tem.Columns.Add("LabelId");

                foreach (var _item in lstTaskOfLabels)
                {
                    _item.RowGuid = Guid.NewGuid();
                    _tem.Rows.Add(_item.RowGuid,
                            obj.TaskGuid,
                            _item.LabelId
                             );
                }
                _detail.Value = _tem;
                _detail.TypeName = "[Cooperation].[TaskOfLabels]";
                var _detail1 = new SqlParameter("@lstTaskCheckLists", SqlDbType.Structured);
                var _tem1 = new System.Data.DataTable();
                _tem1.Columns.Add("CheckListGuid");
                _tem1.Columns.Add("TaskGuid");
                _tem1.Columns.Add("CheckListTitle");
                _tem1.Columns.Add("OrderId");
                _tem1.Columns.Add("Status");
                _tem1.Columns.Add("CreatedDate");
                _tem1.Columns.Add("CreatedBy");
                _tem1.Columns.Add("ModifiedBy");
                _tem1.Columns.Add("ModifiedDate");
                var orderId = 0;
                foreach (var _item in lstTaskCheckLists)
                {
                    _item.CheckListGuid = Guid.NewGuid();
                    orderId = orderId + 1;
                    _tem1.Rows.Add(_item.CheckListGuid,
                            obj.TaskGuid,
                            _item.CheckListTitle,
                            orderId,
                            _item.Status,
                            DBNull.Value,
                            DBNull.Value,
                            DBNull.Value,
                            DBNull.Value
                             );
                }
                _detail1.Value = _tem1;
                _detail1.TypeName = "[Cooperation].[TaskCheckLists]";

                var _detail2 = new SqlParameter("@TaskOfUsers", SqlDbType.Structured);
                var _tem2 = new System.Data.DataTable();
                _tem2.Columns.Add("TaskOfUserGuid       ");
                _tem2.Columns.Add("TaskGuid             ");
                _tem2.Columns.Add("OrganizationGuid     ");
                _tem2.Columns.Add("DepartmentGuid       ");
                _tem2.Columns.Add("DepartmentID         ");
                _tem2.Columns.Add("EmployeeGuid         ");
                _tem2.Columns.Add("EmployeeID           ");
                _tem2.Columns.Add("EmployeeName         ");
                _tem2.Columns.Add("LoginName           ");
                _tem2.Columns.Add("IsRead               ");
                _tem2.Columns.Add("ReadDate             ");
                _tem2.Columns.Add("IsCompleted          ");
                _tem2.Columns.Add("DirectLink           ");
                _tem2.Columns.Add("PermissionID         ");
                _tem2.Columns.Add("IsOwner              ");
                _tem2.Columns.Add("IsLeader             ");
                _tem2.Columns.Add("IsAcceptTask         ");
                _tem2.Columns.Add("PercentComplete      ");
                _tem2.Columns.Add("TimeReminder         ");
                _tem2.Columns.Add("StatusCompleted      ");
                _tem2.Columns.Add("Note                 ");
                _tem2.Columns.Add("CompleteDate         ");
                _tem2.Columns.Add("IsRecallAccept       ");
                _tem2.Columns.Add("TaskLevel            ");
                _tem2.Columns.Add("Mark                 ");
                _tem2.Columns.Add("KPIScore             ");
                _tem2.Columns.Add("IsLocked             ");
                _tem2.Columns.Add("CreatedDate          ");
                _tem2.Columns.Add("CreatedBy            ");
                _tem2.Columns.Add("ModifiedBy           ");
                _tem2.Columns.Add("ModifiedDate         ");
                _tem2.Columns.Add("IsFollowed           ");
                _tem2.Columns.Add("ActualStartDate      ");
                _tem2.Columns.Add("ActualEndDate        ");
                _tem2.Columns.Add("TaskProcess          ");
                _tem2.Columns.Add("RecordGuid           ");
                _tem2.Columns.Add("DepartmentName       ");
                _tem2.Columns.Add("ProcessAccept        ");
                _tem2.Columns.Add("ProcessAcceptComment ");
                _tem2.Columns.Add("ProcessDelete        ");
                _tem2.Columns.Add("FinishDay            ");
                _tem2.Columns.Add("ProcessAcceptDate    ");
                _tem2.Columns.Add("ProcessAcceptPercent ");

                foreach (var _item in lstTaskOfUsers)
                {
                    _item.TaskOfUserGuid = Guid.NewGuid();
                    _tem2.Rows.Add(
                           _item.TaskOfUserGuid
                          , obj.TaskGuid
                          , _emp.OrganizationGuid
                          , _item.DepartmentGuid
                          , _item.DepartmentId
                          , _item.EmployeeGuid
                          , _item.EmployeeId
                          , _item.EmployeeName
                          , _item.LoginName
                          , _item.IsRead
                          , _item.ReadDate
                          , _item.IsCompleted
                          , _item.DirectLink
                          , _item.PermissionId
                          , _item.IsOwner
                          , _item.IsLeader
                          , _item.IsAcceptTask
                          , _item.PercentComplete
                          , _item.TimeReminder
                          , _item.StatusCompleted
                          , _item.Note
                          , _item.CompleteDate
                          , _item.IsRecallAccept
                          , _item.TaskLevel
                          , _item.Mark
                          , _item.Kpiscore
                          , _item.IsLocked
                          , DBNull.Value
                          , DBNull.Value
                          , DBNull.Value
                          , DBNull.Value
                          , _item.IsFollowed
                          , _item.ActualStartDate
                          , _item.ActualEndDate
                          , _item.TaskProcess
                          , _item.RecordGuid
                          , _item.DepartmentName
                          , _item.ProcessAccept
                          , _item.ProcessAcceptComment
                          , _item.ProcessDelete
                          , _item.FinishDay
                          , _item.ProcessAcceptDate
                          , _item.ProcessAcceptPercent

                            );
                }
                _detail2.Value = _tem2;
                _detail2.TypeName = "[Cooperation].[TaskOfUsers]";


                var _rs = _context.SQLCOMMANDS.FromSql(sql, TaskGuid, ParentID, ProjectGuid, ProjectID, OrganizationGuid, DepartmentGuid, DepartmentID, DepartmentName, EmployeeGuid, EmployeeID, EmployeeName, CustomerGuid, CustomerID, CustomerName, Subject, TaskNumber, TaskContent, StartDate, StartTime, EndDate, EndTime, CompleteDate, Priority, SecurityLevel, PercentComplete, TimeReminder, StatusCompleted, Description, Note, Approval, IsApproval, ApprovalStatus, IsRecall, TaskLevel, Mark, KPIScore, CategoryOfTaskGuid, IsActive, IsDraft, IsLocked, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, LoginName, IsArchive, TaskProcess, AttachCount, RecordGuid, CompaignID, ProcessDelete, FinishDay, _detail,
                    _detail1, _lstDeletefile, _detail2,
                    Error).FirstOrDefault();
                if (_rs.Error == "")
                {
                    if (files.Count > 0)
                    {
                        string sqlAttach = "[dbo].[SP_Insert_Attachment] @Database, @Scheme,@TableName,@TableAttachment";
                        var Database = new SqlParameter("@Database", SqlDbType.NVarChar, 50); CheckNullParameterStore(Database, _appSettings.DBName_HR);
                        var Scheme = new SqlParameter("@Scheme", SqlDbType.NVarChar, 50); CheckNullParameterStore(Scheme, "Cooperation");
                        var TableName = new SqlParameter("@TableName ", SqlDbType.NVarChar, 50); CheckNullParameterStore(TableName, "DocumentTaskAttachments");
                        var _detailAttach = new SqlParameter("@TableAttachment", SqlDbType.Structured);
                        /// table detail
                        var _tematach = new System.Data.DataTable();
                        _tematach.Columns.Add("AttachmentGuid");
                        _tematach.Columns.Add("ParentGuid");
                        _tematach.Columns.Add("RecordGuid");
                        _tematach.Columns.Add("OrganizationGuid");
                        _tematach.Columns.Add("DepartmentGuid");
                        _tematach.Columns.Add("ModuleID");
                        _tematach.Columns.Add("Title");
                        _tematach.Columns.Add("FileName");
                        _tematach.Columns.Add("Attachment", typeof(byte[]));
                        _tematach.Columns.Add("FileExtension");
                        _tematach.Columns.Add("FileSize");
                        _tematach.Columns.Add("UrlExternal");
                        _tematach.Columns.Add("IsPublic");
                        _tematach.Columns.Add("PrivateLevel");
                        _tematach.Columns.Add("IsFolder");
                        _tematach.Columns.Add("IsHidden");
                        _tematach.Columns.Add("IsReadonly");
                        _tematach.Columns.Add("IsSync");
                        _tematach.Columns.Add("SyncTime");
                        _tematach.Columns.Add("CloudPath");
                        _tematach.Columns.Add("CreatedDate", typeof(DateTime));
                        _tematach.Columns.Add("ModifiedDate", typeof(DateTime));
                        _tematach.Columns.Add("CreatedBy");
                        _tematach.Columns.Add("ModifiedBy");
                        for (var i = 0; i < files.Count; i++)
                        {
                            for (var j = 0; j < lstTitle.Count; j++)
                            {
                                if (i == lstTitle[j].STT)
                                {
                                    var stream = files[i].OpenReadStream();
                                    byte[] byteArr = Utilities.StreamToByteArray(stream);
                                    var name = files[i].FileName;
                                    Hrattachments _attach = new Hrattachments()
                                    {
                                        AttachmentGuid = Guid.NewGuid(),
                                        RecordGuid = obj.TaskGuid,
                                        OrganizationGuid = _emp.OrganizationGuid,
                                        Title = lstTitle[j].Title,
                                        FileName = name,
                                        CreatedDate = DateTime.Now,
                                        CreatedBy = GetCreatedBy(_context),
                                        ModifiedDate = DateTime.Now,
                                        ModifiedBy = GetModifiedBy(_context),
                                        Attachment = byteArr,
                                        ModuleId = "HR",
                                        FileSize = byteArr.Length,
                                        FileExtension = System.IO.Path.GetExtension(files[i].FileName)
                                    };
                                    _tematach.Rows.Add(_attach.AttachmentGuid,
                                        _attach.ParentGuid,
                                        _attach.RecordGuid,
                                        _attach.OrganizationGuid,
                                        _attach.DepartmentGuid,
                                        _attach.ModuleId,
                                        _attach.Title,
                                        _attach.FileName,
                                        _attach.Attachment,
                                        _attach.FileExtension,
                                        _attach.FileSize,
                                        _attach.UrlExternal,
                                        _attach.IsPublic,
                                        _attach.PrivateLevel,
                                        _attach.IsFolder,
                                        _attach.IsHidden,
                                        _attach.IsReadonly,
                                        _attach.IsSync,
                                       DBNull.Value,
                                        _attach.CloudPath,
                                        _attach.CreatedDate,
                                        _attach.ModifiedDate,
                                        _attach.CreatedBy,
                                        _attach.ModifiedBy
                                    );
                                }
                            }
                        }
                        _detailAttach.Value = _tematach;
                        _detailAttach.TypeName = "[dbo].[Attachment]";
                        var __temattach = _context.SQLCOMMANDS.FromSql(sqlAttach, Database, Scheme, TableName, _detailAttach).FirstOrDefault();
                        if (__temattach.Error != "")
                        {

                        }
                    }
                    msg.Title = "Thêm mới thành công.";
                }
                else
                {
                    return new { Title = _rs.Error, Error = true, Object = _rs.Error };
                }
            }
            catch (Exception ex)

            {
                return new { Object = ex, Title = ex.Message, Error = true };
            }
            return Json(msg);
        }
        [Area("Cooperation")]
        [HttpPost]
        public object Update()
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var _emp = GetEmployeeLogin(_context);
                var files = Request.Form.Files.ToList();
                var _Task = Request.Form["Task"];
                var _Lables = Request.Form["Lables"];
                var _TaskCheckLists = Request.Form["TaskCheckLists"];
                var lstTitlefile = Request.Form["lstTitlefile"];
                var _ListDeletefile = Request.Form["ListDeletefile"];
                Tasks obj = JsonConvert.DeserializeObject<Tasks>(_Task);
                List<TaskOfLabels> lstTaskOfLabels = JsonConvert.DeserializeObject<List<TaskOfLabels>>(_Lables);
                List<TaskCheckLists> lstTaskCheckLists = JsonConvert.DeserializeObject<List<TaskCheckLists>>(_TaskCheckLists);
                List<objTitleFile> lstTitle = JsonConvert.DeserializeObject<List<objTitleFile>>(lstTitlefile);
                List<string> lstDeletefile = JsonConvert.DeserializeObject<List<string>>(_ListDeletefile);
                var _TaskOfUsers = Request.Form["TaskOfUsers"];
                List<TaskOfUsers> lstTaskOfUsers = JsonConvert.DeserializeObject<List<TaskOfUsers>>(_TaskOfUsers);


                if (obj.StartDate.HasValue) { obj.StartDate = obj.StartDate.Value.ToLocalTime(); } else { obj.StartDate = null; }
                if (obj.EndDate.HasValue) { obj.EndDate = obj.EndDate.Value.ToLocalTime(); } else { obj.EndDate = null; }
                string sql = "[Sale].[SP_ESHR_Tasks_Update]" +
                    "@TaskGuid, @ParentID, @ProjectGuid, @ProjectID, @OrganizationGuid, @DepartmentGuid, @DepartmentID, @DepartmentName, @EmployeeGuid, @EmployeeID, @EmployeeName, @CustomerGuid, @CustomerID, @CustomerName, @Subject, @TaskNumber, @TaskContent, @StartDate, @StartTime, @EndDate, @EndTime, @CompleteDate, @Priority, @SecurityLevel, @PercentComplete, @TimeReminder, @StatusCompleted, @Description, @Note, @Approval, @IsApproval, @ApprovalStatus, @IsRecall, @TaskLevel, @Mark, @KPIScore, @CategoryOfTaskGuid, @IsActive, @IsDraft, @IsLocked, @CreatedBy, @CreatedDate, @ModifiedBy, @ModifiedDate, @LoginName, @IsArchive, @TaskProcess, @AttachCount, @RecordGuid, @CompaignID, @ProcessDelete, @FinishDay,@lstTaskOfLabels,@lstTaskCheckLists,@lstDeleteFile,@TaskOfUsers,@Error out";
                var TaskGuid = SqlPara("@TaskGuid ", obj.TaskGuid, SqlDbType.UniqueIdentifier);
                var ParentID = SqlPara("@ParentID            ", obj.ParentId, SqlDbType.UniqueIdentifier);
                var ProjectGuid = SqlPara("@ProjectGuid         ", obj.ProjectGuid, SqlDbType.UniqueIdentifier);
                var ProjectID = SqlPara("@ProjectID           ", obj.ProjectId, SqlDbType.NVarChar);
                var OrganizationGuid = SqlPara("@OrganizationGuid    ", _emp.OrganizationGuid, SqlDbType.UniqueIdentifier);
                var DepartmentGuid = SqlPara("@DepartmentGuid      ", _emp.DepartmentGuid, SqlDbType.UniqueIdentifier);
                var DepartmentID = SqlPara("@DepartmentID        ", _emp.DepartmentId, SqlDbType.NVarChar);
                var DepartmentName = SqlPara("@DepartmentName      ", _emp.DepartmentName, SqlDbType.NVarChar);
                var EmployeeGuid = SqlPara("@EmployeeGuid        ", _emp.EmployeeGuid, SqlDbType.UniqueIdentifier);
                var EmployeeID = SqlPara("@EmployeeID          ", _emp.EmployeeId, SqlDbType.NVarChar);
                var EmployeeName = SqlPara("@EmployeeName        ", _emp.FullName, SqlDbType.NVarChar);
                var CustomerGuid = SqlPara("@CustomerGuid        ", obj.CustomerGuid, SqlDbType.UniqueIdentifier);
                var CustomerID = SqlPara("@CustomerID          ", obj.CustomerId, SqlDbType.NVarChar);
                var CustomerName = SqlPara("@CustomerName        ", obj.CustomerName, SqlDbType.NVarChar);
                var Subject = SqlPara("@Subject             ", obj.Subject, SqlDbType.NVarChar);
                var TaskNumber = SqlPara("@TaskNumber          ", obj.TaskNumber, SqlDbType.NVarChar);
                var TaskContent = SqlPara("@TaskContent         ", obj.TaskContent, SqlDbType.NVarChar);
                var StartDate = SqlPara("@StartDate           ", obj.StartDate, SqlDbType.Date);
                var StartTime = SqlPara("@StartTime           ", obj.StartTime, SqlDbType.Time);
                var EndDate = SqlPara("@EndDate             ", obj.EndDate, SqlDbType.Date);
                var EndTime = SqlPara("@EndTime             ", obj.EndTime, SqlDbType.Time);
                var CompleteDate = SqlPara("@CompleteDate        ", obj.CompleteDate, SqlDbType.Date);
                var Priority = SqlPara("@Priority            ", obj.Priority, SqlDbType.NVarChar);
                var SecurityLevel = SqlPara("@SecurityLevel       ", obj.SecurityLevel, SqlDbType.NVarChar);
                var PercentComplete = SqlPara("@PercentComplete     ", obj.PercentComplete, SqlDbType.Decimal);
                var TimeReminder = SqlPara("@TimeReminder        ", obj.TimeReminder, SqlDbType.DateTime);
                var StatusCompleted = SqlPara("@StatusCompleted     ", obj.StatusCompleted, SqlDbType.NVarChar);
                var Description = SqlPara("@Description         ", obj.Description, SqlDbType.NVarChar);
                var Note = SqlPara("@Note                ", obj.Note, SqlDbType.NVarChar);
                var Approval = SqlPara("@Approval            ", obj.Approval, SqlDbType.NVarChar);
                var IsApproval = SqlPara("@IsApproval          ", obj.IsApproval, SqlDbType.NVarChar);
                var ApprovalStatus = SqlPara("@ApprovalStatus      ", obj.ApprovalStatus, SqlDbType.NVarChar);
                var IsRecall = SqlPara("@IsRecall            ", obj.IsRecall, SqlDbType.Bit);
                var TaskLevel = SqlPara("@TaskLevel           ", obj.TaskLevel, SqlDbType.NVarChar);
                var Mark = SqlPara("@Mark                ", obj.Mark, SqlDbType.Decimal);
                var KPIScore = SqlPara("@KPIScore            ", obj.Kpiscore, SqlDbType.Decimal);
                var CategoryOfTaskGuid = SqlPara("@CategoryOfTaskGuid", obj.CategoryOfTaskGuid, SqlDbType.UniqueIdentifier);
                var IsActive = SqlPara("@IsActive            ", obj.IsActive, SqlDbType.Int);
                var IsDraft = SqlPara("@IsDraft             ", obj.IsDraft, SqlDbType.Bit);
                var IsLocked = SqlPara("@IsLocked            ", obj.IsLocked, SqlDbType.Bit);
                obj.CreatedBy = GetCreatedBy(_context); obj.CreatedDate = DateTime.Now;
                obj.ModifiedBy = GetCreatedBy(_context); obj.ModifiedDate = DateTime.Now;
                var CreatedBy = SqlPara("@CreatedBy           ", obj.CreatedBy, SqlDbType.NVarChar);
                var CreatedDate = SqlPara("@CreatedDate         ", obj.CreatedDate, SqlDbType.DateTime);
                var ModifiedBy = SqlPara("@ModifiedBy          ", obj.ModifiedBy, SqlDbType.NVarChar);
                var ModifiedDate = SqlPara("@ModifiedDate        ", obj.ModifiedDate, SqlDbType.DateTime);
                var LoginName = SqlPara("@LoginName           ", _emp.LoginName, SqlDbType.NVarChar);
                var IsArchive = SqlPara("@IsArchive           ", obj.IsArchive, SqlDbType.Bit);
                var TaskProcess = SqlPara("@TaskProcess         ", obj.TaskProcess, SqlDbType.UniqueIdentifier);
                var AttachCount = SqlPara("@AttachCount         ", obj.AttachCount, SqlDbType.Int);
                var RecordGuid = SqlPara("@RecordGuid          ", obj.RecordGuid, SqlDbType.UniqueIdentifier);
                var CompaignID = SqlPara("@CompaignID          ", obj.CompaignId, SqlDbType.NVarChar);
                var ProcessDelete = SqlPara("@ProcessDelete       ", obj.ProcessDelete, SqlDbType.Bit);
                var FinishDay = SqlPara("@FinishDay           ", obj.FinishDay, SqlDbType.DateTime);
                var Error = new SqlParameter("@Error", SqlDbType.NVarChar, 4000) { Direction = System.Data.ParameterDirection.Output };
                var _lstDeletefile = new SqlParameter("@lstDeleteFile", lstDeletefile.Count >= 1 ? string.Join(",", lstDeletefile) : "");

                var _detail = new SqlParameter("@lstTaskOfLabels", SqlDbType.Structured);
                var _tem = new System.Data.DataTable();
                _tem.Columns.Add("RowGuid");
                _tem.Columns.Add("TaskGuid");
                _tem.Columns.Add("LabelId");

                foreach (var _item in lstTaskOfLabels)
                {
                    _item.RowGuid = Guid.NewGuid();
                    _tem.Rows.Add(_item.RowGuid,
                            obj.TaskGuid,
                            _item.LabelId
                             );
                }
                _detail.Value = _tem;
                _detail.TypeName = "[Cooperation].[TaskOfLabels]";
                var _detail1 = new SqlParameter("@lstTaskCheckLists", SqlDbType.Structured);
                var _tem1 = new System.Data.DataTable();
                _tem1.Columns.Add("CheckListGuid");
                _tem1.Columns.Add("TaskGuid");
                _tem1.Columns.Add("CheckListTitle");
                _tem1.Columns.Add("OrderId");
                _tem1.Columns.Add("Status");
                _tem1.Columns.Add("CreatedDate");
                _tem1.Columns.Add("CreatedBy");
                _tem1.Columns.Add("ModifiedBy");
                _tem1.Columns.Add("ModifiedDate");
                var orderId = 0;
                foreach (var _item in lstTaskCheckLists)
                {
                    _item.CheckListGuid = Guid.NewGuid();
                    orderId = orderId + 1;
                    _tem1.Rows.Add(_item.CheckListGuid,
                            obj.TaskGuid,
                            _item.CheckListTitle,
                            orderId,
                            _item.Status,
                            DBNull.Value,
                            DBNull.Value,
                            DBNull.Value,
                            DBNull.Value
                             );
                }
                _detail1.Value = _tem1;
                _detail1.TypeName = "[Cooperation].[TaskCheckLists]";

                var _detail2 = new SqlParameter("@TaskOfUsers", SqlDbType.Structured);
                var _tem2 = new System.Data.DataTable();
                _tem2.Columns.Add("TaskOfUserGuid       ");
                _tem2.Columns.Add("TaskGuid             ");
                _tem2.Columns.Add("OrganizationGuid     ");
                _tem2.Columns.Add("DepartmentGuid       ");
                _tem2.Columns.Add("DepartmentID         ");
                _tem2.Columns.Add("EmployeeGuid         ");
                _tem2.Columns.Add("EmployeeID           ");
                _tem2.Columns.Add("EmployeeName         ");
                _tem2.Columns.Add("LoginName           ");
                _tem2.Columns.Add("IsRead               ");
                _tem2.Columns.Add("ReadDate             ");
                _tem2.Columns.Add("IsCompleted          ");
                _tem2.Columns.Add("DirectLink           ");
                _tem2.Columns.Add("PermissionID         ");
                _tem2.Columns.Add("IsOwner              ");
                _tem2.Columns.Add("IsLeader             ");
                _tem2.Columns.Add("IsAcceptTask         ");
                _tem2.Columns.Add("PercentComplete      ");
                _tem2.Columns.Add("TimeReminder         ");
                _tem2.Columns.Add("StatusCompleted      ");
                _tem2.Columns.Add("Note                 ");
                _tem2.Columns.Add("CompleteDate         ");
                _tem2.Columns.Add("IsRecallAccept       ");
                _tem2.Columns.Add("TaskLevel            ");
                _tem2.Columns.Add("Mark                 ");
                _tem2.Columns.Add("KPIScore             ");
                _tem2.Columns.Add("IsLocked             ");
                _tem2.Columns.Add("CreatedDate          ");
                _tem2.Columns.Add("CreatedBy            ");
                _tem2.Columns.Add("ModifiedBy           ");
                _tem2.Columns.Add("ModifiedDate         ");
                _tem2.Columns.Add("IsFollowed           ");
                _tem2.Columns.Add("ActualStartDate      ");
                _tem2.Columns.Add("ActualEndDate        ");
                _tem2.Columns.Add("TaskProcess          ");
                _tem2.Columns.Add("RecordGuid           ");
                _tem2.Columns.Add("DepartmentName       ");
                _tem2.Columns.Add("ProcessAccept        ");
                _tem2.Columns.Add("ProcessAcceptComment ");
                _tem2.Columns.Add("ProcessDelete        ");
                _tem2.Columns.Add("FinishDay            ");
                _tem2.Columns.Add("ProcessAcceptDate    ");
                _tem2.Columns.Add("ProcessAcceptPercent ");

                foreach (var _item in lstTaskOfUsers)
                {
                    _item.TaskOfUserGuid = Guid.NewGuid();
                    _tem2.Rows.Add(
                           _item.TaskOfUserGuid
                          , obj.TaskGuid
                          , _emp.OrganizationGuid
                          , _item.DepartmentGuid
                          , _item.DepartmentId
                          , _item.EmployeeGuid
                          , _item.EmployeeId
                          , _item.EmployeeName
                          , _item.LoginName
                          , _item.IsRead
                          , _item.ReadDate
                          , _item.IsCompleted
                          , _item.DirectLink
                          , _item.PermissionId
                          , _item.IsOwner
                          , _item.IsLeader
                          , _item.IsAcceptTask
                          , _item.PercentComplete
                          , _item.TimeReminder
                          , _item.StatusCompleted
                          , _item.Note
                          , _item.CompleteDate
                          , _item.IsRecallAccept
                          , _item.TaskLevel
                          , _item.Mark
                          , _item.Kpiscore
                          , _item.IsLocked
                          , DBNull.Value
                          , DBNull.Value
                          , DBNull.Value
                          , DBNull.Value
                          , _item.IsFollowed
                          , _item.ActualStartDate
                          , _item.ActualEndDate
                          , _item.TaskProcess
                          , _item.RecordGuid
                          , _item.DepartmentName
                          , _item.ProcessAccept
                          , _item.ProcessAcceptComment
                          , _item.ProcessDelete
                          , _item.FinishDay
                          , _item.ProcessAcceptDate
                          , _item.ProcessAcceptPercent

                            );
                }
                _detail2.Value = _tem2;
                _detail2.TypeName = "[Cooperation].[TaskOfUsers]";


                var _rs = _context.SQLCOMMANDS.FromSql(sql, TaskGuid, ParentID, ProjectGuid, ProjectID, OrganizationGuid, DepartmentGuid, DepartmentID, DepartmentName, EmployeeGuid, EmployeeID, EmployeeName, CustomerGuid, CustomerID, CustomerName, Subject, TaskNumber, TaskContent, StartDate, StartTime, EndDate, EndTime, CompleteDate, Priority, SecurityLevel, PercentComplete, TimeReminder, StatusCompleted, Description, Note, Approval, IsApproval, ApprovalStatus, IsRecall, TaskLevel, Mark, KPIScore, CategoryOfTaskGuid, IsActive, IsDraft, IsLocked, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, LoginName, IsArchive, TaskProcess, AttachCount, RecordGuid, CompaignID, ProcessDelete, FinishDay, _detail,
                    _detail1, _lstDeletefile, _detail2,
                    Error).FirstOrDefault();
                if (_rs.Error == "")
                {
                    if (files.Count > 0)
                    {
                        string sqlAttach = "[dbo].[SP_Insert_Attachment] @Database, @Scheme,@TableName,@TableAttachment";
                        var Database = new SqlParameter("@Database", SqlDbType.NVarChar, 50); CheckNullParameterStore(Database, _appSettings.DBName_HR);
                        var Scheme = new SqlParameter("@Scheme", SqlDbType.NVarChar, 50); CheckNullParameterStore(Scheme, "Cooperation");
                        var TableName = new SqlParameter("@TableName ", SqlDbType.NVarChar, 50); CheckNullParameterStore(TableName, "DocumentTaskAttachments");
                        var _detailAttach = new SqlParameter("@TableAttachment", SqlDbType.Structured);
                        /// table detail
                        var _tematach = new System.Data.DataTable();
                        _tematach.Columns.Add("AttachmentGuid");
                        _tematach.Columns.Add("ParentGuid");
                        _tematach.Columns.Add("RecordGuid");
                        _tematach.Columns.Add("OrganizationGuid");
                        _tematach.Columns.Add("DepartmentGuid");
                        _tematach.Columns.Add("ModuleID");
                        _tematach.Columns.Add("Title");
                        _tematach.Columns.Add("FileName");
                        _tematach.Columns.Add("Attachment", typeof(byte[]));
                        _tematach.Columns.Add("FileExtension");
                        _tematach.Columns.Add("FileSize");
                        _tematach.Columns.Add("UrlExternal");
                        _tematach.Columns.Add("IsPublic");
                        _tematach.Columns.Add("PrivateLevel");
                        _tematach.Columns.Add("IsFolder");
                        _tematach.Columns.Add("IsHidden");
                        _tematach.Columns.Add("IsReadonly");
                        _tematach.Columns.Add("IsSync");
                        _tematach.Columns.Add("SyncTime");
                        _tematach.Columns.Add("CloudPath");
                        _tematach.Columns.Add("CreatedDate", typeof(DateTime));
                        _tematach.Columns.Add("ModifiedDate", typeof(DateTime));
                        _tematach.Columns.Add("CreatedBy");
                        _tematach.Columns.Add("ModifiedBy");
                        for (var i = 0; i < files.Count; i++)
                        {
                            for (var j = 0; j < lstTitle.Count; j++)
                            {
                                if (i == lstTitle[j].STT)
                                {
                                    var stream = files[i].OpenReadStream();
                                    byte[] byteArr = Utilities.StreamToByteArray(stream);
                                    var name = files[i].FileName;
                                    Hrattachments _attach = new Hrattachments()
                                    {
                                        AttachmentGuid = Guid.NewGuid(),
                                        RecordGuid = obj.TaskGuid,
                                        OrganizationGuid = _emp.OrganizationGuid,
                                        Title = lstTitle[j].Title,
                                        FileName = name,
                                        CreatedDate = DateTime.Now,
                                        CreatedBy = GetCreatedBy(_context),
                                        ModifiedDate = DateTime.Now,
                                        ModifiedBy = GetModifiedBy(_context),
                                        Attachment = byteArr,
                                        ModuleId = "HR",
                                        FileSize = byteArr.Length,
                                        FileExtension = System.IO.Path.GetExtension(files[i].FileName)
                                    };
                                    _tematach.Rows.Add(_attach.AttachmentGuid,
                                        _attach.ParentGuid,
                                        _attach.RecordGuid,
                                        _attach.OrganizationGuid,
                                        _attach.DepartmentGuid,
                                        _attach.ModuleId,
                                        _attach.Title,
                                        _attach.FileName,
                                        _attach.Attachment,
                                        _attach.FileExtension,
                                        _attach.FileSize,
                                        _attach.UrlExternal,
                                        _attach.IsPublic,
                                        _attach.PrivateLevel,
                                        _attach.IsFolder,
                                        _attach.IsHidden,
                                        _attach.IsReadonly,
                                        _attach.IsSync,
                                       DBNull.Value,
                                        _attach.CloudPath,
                                        _attach.CreatedDate,
                                        _attach.ModifiedDate,
                                        _attach.CreatedBy,
                                        _attach.ModifiedBy
                                    );
                                }
                            }
                        }
                        _detailAttach.Value = _tematach;
                        _detailAttach.TypeName = "[dbo].[Attachment]";
                        var __temattach = _context.SQLCOMMANDS.FromSql(sqlAttach, Database, Scheme, TableName, _detailAttach).FirstOrDefault();
                        if (__temattach.Error != "")
                        {

                        }
                    }
                    msg.Title = "Cập nhật thành công.";
                }
                else
                {
                    return new { Title = _rs.Error, Error = true, Object = _rs.Error };
                }
            }
            catch (Exception ex)

            {
                return new { Object = ex, Title = ex.Message, Error = true };
            }
            return Json(msg);
        }
        /// <summary>
        /// Xóa bản ghi
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        [Area("Cooperation")]
        public object Delete([FromBody] TempSub obj)
        {
            try
            {
                var _tGuid = new Guid(obj.IdS[0]);
                string sql = "[Sale].[SP_ESHR_Tasks_Delete] @TaskGuid";
                var TaskGuid = SqlPara("@TaskGuid ", _tGuid, SqlDbType.UniqueIdentifier);
                var _rs = _context.SQLCOMMANDS.FromSql(sql, TaskGuid).FirstOrDefault();
                if (_rs.Error == "")
                {
                    return new { Title = "Xóa công việc thành công.", Error = false };
                }
                else
                {
                    return new { Title = _rs.Error, Error = true, Object = _rs.Error };
                }
            }
            catch (Exception ex)
            {
                return new { Title = ex.Message, Error = true, Object = ex };
            }
        }

        //Tải file đính kèm
        [Area("Cooperation")]
        [HttpGet]
        public object Download(Guid? Id)
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var username = GetEmployeeLogin(_context);
                string sql = "[dbo].[SP_Download_Attachment] @Database, @Scheme, @TableName, @OrganizationGuid, @AttachmentGuid, @Error";
                var _Database = new SqlParameter("@Database", SqlDbType.NVarChar); _Database.Value = _appSettings.DBName_HR;
                var _Scheme = new SqlParameter("@Scheme", SqlDbType.VarChar); _Scheme.Value = "Cooperation";
                var _TableName = new SqlParameter("@TableName", SqlDbType.VarChar); _TableName.Value = "DocumentTaskAttachments";
                var _OrganizationGuid = new SqlParameter("@OrganizationGuid", SqlDbType.UniqueIdentifier); _OrganizationGuid.Value = username.OrganizationGuid;
                var _AttachmentGuid = new SqlParameter("@AttachmentGuid", SqlDbType.UniqueIdentifier); _AttachmentGuid.Value = Id ?? (Object)DBNull.Value;
                var _Error = new SqlParameter("@Error", SqlDbType.NVarChar); _Error.Value = "";
                var rs = _context.AttachmentsDownloadView.FromSql(sql, _Database, _Scheme, _TableName, _OrganizationGuid, _AttachmentGuid, _Error).SingleOrDefault();
                if (rs != null)
                {
                    byte[] fileBytes = rs.Attachment;
                    var fileName = rs.FileName;
                    return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);

                }
                else return null;
            }
            catch (Exception ex)
            {
                msg.Title = "Có lỗi khi lấy dữ liệu.";
                return null;
            }
        }

        ///// <summary>
        ///// Danh sách danh mục loại công việc
        ///// </summary>
        ///// <returns></returns>
        [HttpGet]
        [Area("Cooperation")]
        public object CateOfTaskList()
        {
            try
            {
                var _emp = GetEmployeeLogin(_context);
                var o = _context.CategoryOfTasks
                                .Where(x =>
                                    (
                                        (
                                            (
                                                x.DepartmentGuid == _emp.DepartmentGuid
                                                ||
                                                x.DepartmentGuid == null
                                                ||
                                                x.IsPublic == true
                                            )
                                            &&
                                            x.IsActive == true
                                        )
                                    )
                                    &&
                                        x.OrganizationGuid == _emp.OrganizationGuid
                                )
                                .Select(x => new
                                {
                                    x.Title,
                                    x.CategoryOfTaskGuid,
                                    x.TypeStep,
                                    TaskStepBySteps = x.TaskStepBySteps.Count(),
                                    x.IsDefault,
                                    x.IsPublic
                                })
                                .ToList();
                return new JMessage() { Error = false, Title = "Lấy dữ liệu thành công.", Object = o };
            }
            catch (Exception ex)
            {
                return new JMessage() { Error = true, Title = ex.Message, Object = ex };
            }
        }


        //#endregion
        //#region Category Of Task
        //public class TreeView
        //{
        //    public string Title { get; set; }
        //    public Guid Id { get; set; }
        //    public bool HasChild { get; set; }
        //    public bool? IsDefault { get; set; }
        //}
        public class TreeView_GetTreeDataCOT
        {
            public string Title { get; set; }
            public Guid Id { get; set; }
            public Guid? ParentId { get; set; }
            public bool HasChild { get; set; }
            public bool? IsDefault { get; set; }
            public int? TaskStepBySteps { get; set; }
            public string TypeStep { get; set; }
        }
        ///// <summary>
        ///// Lấy danh sách danh mục công việc dưới dạng phân cấp
        ///// </summary>
        ///// <param name="id"></param>
        ///// <returns></returns>
        [HttpPost]
        [Area("Cooperation")]
        public List<TreeView_GetTreeDataCOT> GetTreeDataCOT(Guid? id)
        {
            var _emp = GetEmployeeLogin(_context);
            var data = _context.CategoryOfTasks
                            .Select(x => new TreeView_GetTreeDataCOT()
                            {
                                Id = x.CategoryOfTaskGuid,
                                Title = x.Title,
                                HasChild = false,
                                ParentId = x.ParentId,
                                IsDefault = x.IsDefault,
                                TaskStepBySteps = x.TaskStepBySteps
                                    .Count(),
                                TypeStep = x.TypeStep,
                            }
                            )
                            .ToList();
            var dataOrder = GetSubTreeData(data, null, new List<TreeView_GetTreeDataCOT>(), "");
            return dataOrder;
        }
        private List<TreeView_GetTreeDataCOT> GetSubTreeData(List<TreeView_GetTreeDataCOT> data, Guid? parentid, List<TreeView_GetTreeDataCOT> lstCategories, string tab)
        {
            tab += "- ";
            var contents = parentid == null
                ? data.Where(x => x.ParentId == null).ToList()
                : data.Where(x => x.ParentId == parentid).ToList();
            foreach (var item in contents)
            {
                var category = new TreeView_GetTreeDataCOT
                {
                    Id = item.Id,
                    Title = tab + item.Title,
                    HasChild = data.Any(x => x.ParentId == item.Id),
                    IsDefault = item.IsDefault,
                    TaskStepBySteps = item.TaskStepBySteps,
                    TypeStep = item.TypeStep,
                };
                lstCategories.Add(category);
                if (category.HasChild) GetSubTreeData(data, item.Id, lstCategories, tab);
            }
            return lstCategories;
        }
        ///// <summary>
        ///// Thêm mới danh mục công việc
        ///// </summary>
        ///// <param name="obj"></param>
        ///// <returns></returns>
        [HttpPost]
        [Area("Cooperation")]
        public object InsertCategoryOfTaskCOT([FromBody] CategoryOfTasks obj)
        {
            var msg = new JMessage() { Error = false };
            try
            {
                obj.CreatedDate = DateTime.Now;
                obj.ModifiedDate = DateTime.Now;
                var _emp = GetEmployeeLogin(_context);
                obj.CreatedBy = _emp.LoginName + "#" + _emp.FullName;
                obj.ModifiedBy = _emp.LoginName + "#" + _emp.FullName;
                obj.OrganizationGuid = _emp.OrganizationGuid;
                _context.CategoryOfTasks.Add(obj);
                _context.SaveChanges();
                msg.Title = "Thêm mới thành công";
            }
            catch (Exception ex)
            {
                msg.Error = true;
                msg.Title = "Có lỗi khi thêm mới";
                msg.Object = ex.ToString();
            }
            return msg;
        }


        ///// <summary>
        ///// Lấy thông tin danh sách nhãn công việc
        ///// </summary>
        ///// <param name="obj"></param>
        ///// <returns></returns>
        [Area("Cooperation")]
        [HttpPost]
        public object GetLabels([FromBody] TempSub obj)
        {
            try
            {
                var _emp = GetEmployeeLogin(_context);

                string _sql = "exec [Cooperation].[SP_ESTM_TASK_GetLabels] " +
                    "@OrganizationGuid";
                var _OrganizationGuid = new SqlParameter("@OrganizationGuid", SqlDbType.UniqueIdentifier);
                CheckNullParameterStore(_OrganizationGuid, _emp.OrganizationGuid);
                var _rs = _context.TaskLabelCustoms.FromSql(_sql,
                    _OrganizationGuid
                    ).ToList();
                return new JMessage() { Error = false, Title = "Lấy dữ liệu thành công.", Object = _rs };
            }
            catch (Exception ex)
            {
                return new JMessage() { Error = true, Title = ex.Message, Object = ex.ToString() };
            }
        }
        public class TaskCustomUpdateprocess
        {
            public Guid? ParentId { get; set; }
            public Guid TaskOfUserGuid { get; set; }
            public bool? IsConpleted { get; set; }
            public Guid TaskGuid { get; set; }
            public byte? PercentComplete { get; set; }
            public DateTime? TimeReminder { get; set; }
            public string StatusCompleted { get; set; }
            public string Note { get; set; }
            public DateTime? CompleteDate { get; set; }
            /// <summary>
            /// 1 update to Task
            /// 2 update to TaskOfUser
            /// </summary>
            public int? StatusType { get; set; }
        }
        /// <summary>
        /// Cập nhật tiến độ cho bản ghi
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Area("Cooperation")]
        public object UpdateProcess()
        {
            _context.Database.BeginTransaction();
            var _msg = new JMessage() { Error = false };
            try
            {
                var files = Request.Form.Files;
                var objtemp = Request.Form["update"];
                TaskCustomUpdateprocess o = JsonConvert.DeserializeObject<TaskCustomUpdateprocess>(objtemp);
                var _emp = GetEmployeeLogin(_context);
                var _oTask = _context.Tasks
                                .Where(x => x.TaskGuid == o.TaskGuid)
                                .Select(x => new
                                {
                                    x.Subject,
                                    x.TaskGuid,
                                    x.EmployeeGuid,
                                    x.EmployeeId,
                                    x.CategoryOfTaskGu
                                        .Mark,
                                    x.CategoryOfTaskGu
                                        .TaskLevel,
                                    IsOwner = x.TaskOfUsers
                                        .Where(c => c.LoginName.ToUpper() == _emp.LoginName.ToUpper())
                                        .Select(c => c.IsOwner)
                                        .FirstOrDefault(),
                                    PermissionId = x.TaskOfUsers
                                        .Where(c => c.LoginName.ToUpper() == _emp.LoginName.ToUpper())
                                        .Select(c => c.Permission.PermissionId)
                                        .FirstOrDefault(),
                                    TaskOfUser = x.TaskOfUsers.Select(c => c.LoginName).ToList(),
                                })
                                .FirstOrDefault();
                if (_oTask.IsOwner == "A")
                {
                    var _rs = _context.Tasks
                                    .Where(x => x.TaskGuid == o.TaskGuid)
                                    .FirstOrDefault();
                    if (o.TimeReminder != null)
                        o.TimeReminder = Convert.ToDateTime(o.TimeReminder).ToLocalTime();
                    _context.Attach(_rs).State = EntityState.Modified;
                    _rs.Note = o.Note;
                    _rs.PercentComplete = o.PercentComplete;
                    _rs.StatusCompleted = o.StatusCompleted;
                    _rs.TimeReminder = o.TimeReminder;
                    _rs.CompleteDate = o.CompleteDate;
                    _rs.ModifiedBy = _emp.LoginName + "#" + _emp.FullName;
                    _rs.ModifiedDate = DateTime.Now;
                    _rs.FinishDay = DateTime.Now;
                    _context.SaveChanges();
                    var __rs = _context.TaskOfUsers
                                   .Where(x => x.TaskGuid == o.TaskGuid &&
                                               x.LoginName.ToUpper() == _emp.LoginName.ToUpper()
                                   )
                                   .ToList();
                    if (o.TimeReminder != null)
                        o.TimeReminder = Convert.ToDateTime(o.TimeReminder).ToLocalTime();
                    if (__rs != null)
                    {
                        foreach (var item in __rs)
                        {
                            _context.Attach(item);
                            _context.Entry(item).Property(x => x.Note).IsModified = true;
                            _context.Entry(item).Property(x => x.PercentComplete).IsModified = true;
                            _context.Entry(item).Property(x => x.StatusCompleted).IsModified = true;
                            _context.Entry(item).Property(x => x.TimeReminder).IsModified = true;
                            _context.Entry(item).Property(x => x.CompleteDate).IsModified = true;
                            _context.Entry(item).Property(x => x.ModifiedBy).IsModified = true;
                            _context.Entry(item).Property(x => x.ModifiedDate).IsModified = true;
                            item.Note = o.Note;
                            item.PercentComplete = o.PercentComplete;
                            item.StatusCompleted = o.StatusCompleted;
                            if (item.StatusCompleted == "C")
                            {
                                _context.Entry(item).Property(x => x.TaskLevel).IsModified = true;
                                _context.Entry(item).Property(x => x.Mark).IsModified = true;
                                item.TaskLevel = _oTask.TaskLevel;
                                item.Mark = _oTask.Mark;
                            }
                            item.TimeReminder = o.TimeReminder;
                            item.CompleteDate = o.CompleteDate;
                            item.ModifiedBy = _emp.LoginName + "#" + _emp.FullName;
                            item.ModifiedDate = DateTime.Now;
                            item.FinishDay = DateTime.Now;
                            _context.SaveChanges();
                        }
                    }
                }
                else
                {
                    var rs = _context.TaskOfUsers
                                .Where(x => x.TaskGuid == o.TaskGuid &&
                                            x.EmployeeGuid == _emp.EmployeeGuid
                                )
                                .ToList();
                    if (o.TimeReminder != null)
                        o.TimeReminder = Convert.ToDateTime(o.TimeReminder).ToLocalTime();
                    foreach (var item in rs)
                    {
                        _context.Attach(item);
                        _context.Entry(item).Property(x => x.Note).IsModified = true;
                        _context.Entry(item).Property(x => x.PercentComplete).IsModified = true;
                        _context.Entry(item).Property(x => x.StatusCompleted).IsModified = true;
                        _context.Entry(item).Property(x => x.TimeReminder).IsModified = true;
                        _context.Entry(item).Property(x => x.CompleteDate).IsModified = true;
                        _context.Entry(item).Property(x => x.ModifiedBy).IsModified = true;
                        _context.Entry(item).Property(x => x.ModifiedDate).IsModified = true;
                        if (item.StatusCompleted == "C")
                        {
                            _context.Entry(item).Property(x => x.TaskLevel).IsModified = true;
                            _context.Entry(item).Property(x => x.Mark).IsModified = true;
                            item.TaskLevel = _oTask.TaskLevel;
                            item.Mark = _oTask.Mark;
                        }
                        item.Note = o.Note;
                        item.PercentComplete = o.PercentComplete;
                        item.StatusCompleted = o.StatusCompleted;
                        item.TimeReminder = o.TimeReminder;
                        item.CompleteDate = o.CompleteDate;
                        item.ModifiedBy = _emp.LoginName + "#" + _emp.FullName;
                        item.ModifiedDate = DateTime.Now;
                        item.FinishDay = DateTime.Now;
                        _context.SaveChanges();
                    }
                }
                if (files.Count > 0)
                {
                    foreach (var file in files)
                    {
                        var stream = file.OpenReadStream();
                        byte[] byteArr = Utilities.StreamToByteArray(stream);
                        var name = file.FileName;
                        TaskAttachments otemp = new TaskAttachments()
                        {
                            RecordGuid = o.TaskGuid,
                            FileName = name,
                            Title = name,
                            CreatedDate = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd hh:mm")),
                            ModifiedDate = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd hh:mm")),
                            CreatedBy = _emp.LoginName + "#" + _emp.FullName,
                            ModifiedBy = _emp.LoginName + "#" + _emp.FullName,
                            Attachment = byteArr,
                        };
                        _context.TaskAttachments.Add(otemp);
                        _context.SaveChanges();
                    }
                }
                CreateLogTaskActivities(_context, "-1", "", "Cập nhật tiến độ công việc " + o.PercentComplete + "%.", o.TaskGuid, _emp);
                CreateLogComment(_context, _emp, _emp.FullName + "vừa cập nhật " + o.PercentComplete + "% hoàn thành. " + ((o.Note != null && o.Note.Trim() != "") ? "Với nội dung: " + o.Note : ""));
                _context.Database.CommitTransaction();
                _msg = new JMessage() { Error = false, Title = "Cập nhật tiến độ thành công." };

            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                _msg = new JMessage() { Error = true, Title = "Cập nhật tiến độ không thành công.", Object = ex.Message };
            }
            return _msg;
        }
        [HttpPost]
        [Area("Cooperation")]
        public object UpdateProcessSuccess([FromBody] TempSub obj)
        {
            JMessage _msg = new JMessage() { Error = false, Title = "Cập nhật công việc thành công" };
            try
            {
                var _emp = GetEmployeeLogin(_context);
                Guid _guid = new Guid(obj.IdS[0]);
                var _rs = _context.TaskOfUsers
                                .Where(x => x.TaskGuid == _guid
                                    && x.LoginName == _emp.LoginName)
                                .ToList();
                var _oTask = _context.Tasks
                              .Where(x => x.TaskGuid == _guid)
                              .Select(x => new
                              {
                                  x.Subject,
                                  x.TaskGuid,
                                  x.EmployeeGuid,
                                  x.EmployeeId,
                                  x.CategoryOfTaskGu
                                      .Mark,
                                  x.CategoryOfTaskGu
                                      .TaskLevel,
                                  IsOwner = x.TaskOfUsers
                                        .Where(c => c.LoginName.ToUpper() == _emp.LoginName.ToUpper())
                                        .Select(c => c.IsOwner)
                                        .FirstOrDefault(),
                                  PermissionId = x.TaskOfUsers
                                        .Where(c => c.LoginName.ToUpper() == _emp.LoginName.ToUpper())
                                        .Select(c => c.Permission.PermissionId)
                                        .FirstOrDefault(),
                                  TaskOfUser = x.TaskOfUsers.Select(c => c.LoginName).ToList(),
                              })
                              .FirstOrDefault();
                foreach (var item in _rs)
                {
                    item.ModifiedBy = _emp.LoginName + "#" + _emp.FullName;
                    item.ModifiedDate = DateTime.Now;
                    _context.Attach(item);
                    _context.Entry(item).Property(x => x.IsCompleted).IsModified = true;
                    _context.Entry(item).Property(x => x.PercentComplete).IsModified = true;
                    _context.Entry(item).Property(x => x.StatusCompleted).IsModified = true;
                    _context.Entry(item).Property(x => x.TaskLevel).IsModified = true;
                    _context.Entry(item).Property(x => x.Mark).IsModified = true;
                    _context.Entry(item).Property(x => x.TaskLevel).IsModified = true;
                    _context.Entry(item).Property(x => x.Mark).IsModified = true;
                    item.TaskLevel = _oTask.TaskLevel;
                    item.Mark = _oTask.Mark;
                    item.IsCompleted = "Y";
                    item.PercentComplete = 100;
                    item.StatusCompleted = "C";
                    item.FinishDay = DateTime.Now;
                    _context.SaveChanges();
                }
                if (_oTask.IsOwner == "A")
                {
                    var _Task = _context.Tasks
                            .Where(x => x.TaskGuid == _guid)
                            .FirstOrDefault();
                    _context.Attach(_Task).State = EntityState.Modified;
                    _Task.PercentComplete = 100;
                    _Task.StatusCompleted = "C";
                    _Task.ModifiedBy = _emp.LoginName + "#" + _emp.FullName;
                    _Task.ModifiedDate = DateTime.Now;
                    _Task.FinishDay = DateTime.Now;
                    _context.Entry(_Task).Property(x => x.PercentComplete).IsModified = true;
                    _context.Entry(_Task).Property(x => x.StatusCompleted).IsModified = true;
                    _context.Entry(_Task).Property(x => x.ModifiedBy).IsModified = true;
                    _context.Entry(_Task).Property(x => x.ModifiedDate).IsModified = true;
                    _context.Entry(_Task).Property(x => x.FinishDay).IsModified = true;
                    _context.SaveChanges();

                }
                CreateLogTaskActivities(_context, "-1", "", "Cập nhật tiến độ công việc hoàn thành", _oTask.TaskGuid, _emp);

            }
            catch (Exception ex)
            {
                _msg = new JMessage() { Error = true, Title = "Cập nhật tiến độ không thành công", Object = ex.ToString() };
            }
            return _msg;
        }
        /// <summary>
        /// Đưa ra danh sách ô tích công việc
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        [Area("Cooperation")]
        public object LoadCheckList([FromBody] TempSub obj)
        {
            JMessage _rs = new JMessage();
            try
            {
                var _guid = new Guid(obj.IdS[0]);
                var _emp = GetEmployeeLogin(_context);
                string _sqlComment = "exec [Cooperation].[SP_TM_Get_GetLoadCheckList] " +
                 "@OrganizationGuid," +
                 "@LoginName," +
                 "@RecordGuid," +
                 "@Error out";

                var _OrganizationGuid = new SqlParameter("@OrganizationGuid", SqlDbType.UniqueIdentifier);
                CheckNullParameterStore(_OrganizationGuid, _emp.OrganizationGuid);

                var _LoginName = new SqlParameter("@LoginName", SqlDbType.NVarChar);
                CheckNullParameterStore(_LoginName, _emp.LoginName);

                var _RecordGuid = new SqlParameter("@RecordGuid", SqlDbType.UniqueIdentifier);
                CheckNullParameterStore(_RecordGuid, _guid);

                var _Error = new SqlParameter("@Error", SqlDbType.NVarChar) { Direction = ParameterDirection.Output };
                CheckNullParameterStore(_Error, "");

                var _d = _context.LoadCheckLists.FromSql(_sqlComment,
                        _OrganizationGuid,
                        _LoginName,
                        _RecordGuid,
                        _Error)
                    .OrderBy(x => x.OrderId)
                    .ToList();

                _rs.Error = false;
                _rs.Title = "Lấy danh sách tin nhắn thành công";
                _rs.Object = _d;
            }
            catch (Exception ex)
            {
                _rs.Error = true;
                _rs.Title = "Lấy danh sách tin nhắn không thành công";
                _rs.Object = ex.ToString();
            }
            return _rs;
        }
        public class CustomChecklist
        {
            public string CheckListTitle { get; set; }
            public bool? Status { get; set; }
            public Guid? TaskGuid { get; set; }
        }
        /// <summary>
        /// Thêm ô tích cho công việc
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        [Area("Cooperation")]
        public object InsertCheckList([FromBody] CustomChecklist obj)
        {
            try
            {
                var _c = _context.TaskCheckLists.Where(x => x.TaskGuid == obj.TaskGuid).Count();
                var _emp = GetEmployeeLogin(_context);
                var _obj = new TaskCheckLists()
                {
                    CheckListGuid = Guid.NewGuid(),
                    CheckListTitle = obj.CheckListTitle,
                    Status = obj.Status,
                    TaskGuid = obj.TaskGuid,
                    OrderId = _c + 1,
                };
                _obj.CreatedBy = _emp.LoginName + "#" + _emp.FullName;
                _obj.CreatedDate = DateTime.Now;
                _context.TaskCheckLists.Add(_obj);
                _context.SaveChanges();
                CreateLogTaskActivities(_context, "-1", "", "Thêm checklist công việc.", (Guid)obj.TaskGuid, _emp);
                return new JMessage() { Error = false, Title = "Thêm mới check list thành công" };
            }
            catch (Exception ex)
            {
                return new JMessage() { Error = true, Title = "Có lỗi trong quá trình thêm mới.", Object = ex.ToString() };
            }
        }
        /// <summary>
        /// Cập nhật các bản ghi tích chọn
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        [Area("Cooperation")]
        public object UpdateCheckList([FromBody] TempSub obj)
        {
            try
            {
                var _guid = new Guid(obj.IdS[0]);
                var _emp = GetEmployeeLogin(_context);
                var _rs = _context.TaskCheckLists
                                .Where(x => x.CheckListGuid == _guid)
                                .FirstOrDefault();
                _context.Attach(_rs);
                _context.Entry(_rs).Property(x => x.Status).IsModified = true;
                _context.Entry(_rs).Property(x => x.ModifiedBy).IsModified = true;
                _context.Entry(_rs).Property(x => x.ModifiedDate).IsModified = true;
                _rs.Status = ((obj.IdI[0] == 0) ? false : true);
                _rs.ModifiedBy = _emp.LoginName + "#" + _emp.FullName;
                _rs.ModifiedDate = DateTime.Now;
                _context.SaveChanges();
                string _s = ((obj.IdI[0] == 0) ? "Bỏ tích chọn " : "Tích chọn ") + "checklist công việc [" + _rs.CheckListTitle + "].";
                CreateLogTaskActivities(_context, "-1", "", _s, (Guid)_rs.TaskGuid, _emp);
                return new JMessage() { Error = false, Title = "Cập nhật check list thành công", };
            }
            catch (Exception ex)
            {
                return new JMessage() { Error = true, Title = "Có lỗi trong quá trình cập nhật.", Object = ex.ToString() };
            }
        }
        public class Active_ProcessDelete_Custom
        {
            public Guid TaskOfUserGuid { get; set; }
            public string ProcessDelete { get; set; }
        }
        /// <summary>
        /// Update accept proccess success by user created task
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [Area("Cooperation")]
        [HttpPost]
        public object Active_ProcessDelete([FromBody] Active_ProcessDelete_Custom obj)
        {
            JMessage _rs = new JMessage() { Error = false, Title = "Cập nhật dữ liệu thành công" };
            try
            {
                var _emp = GetEmployeeLogin(_context);
                var _d = _context.TaskOfUsers
                        .Where(x => x.TaskOfUserGuid == obj.TaskOfUserGuid)
                        .FirstOrDefault();
                _d.ProcessDelete = obj.ProcessDelete;
                _context.SaveChanges();
                var _tms = _context.TaskOfUsers
                        .Where(x => x.TaskGuid == _d.TaskGuid)
                        .Select(x => new
                        {
                            x.TaskGuid,
                            x.ProcessDelete
                        })
                        .ToList();
                CreateLogTaskActivities(_context, "-1", "", "Chấp nhận xóa công việc.", (Guid)_d.TaskGuid, _emp);
                if (_tms.Where(x => x.ProcessDelete == "Y").Count() == _tms.Count())
                {
                    var _tm = _context.Tasks
                            .Where(x => x.TaskGuid == _d.TaskGuid)
                            .FirstOrDefault();
                    _context.Remove(_tm);
                    _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                _rs.Error = true;
                _rs.Object = ex.ToString();
                _rs.Title = "Cập nhật không thành công";
            }
            return _rs;
        }
        /// <summary>
        /// Thêm vào theo dõi công việc
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        [Area("Cooperation")]
        public object UpdateFollow([FromBody] TempSub obj)
        {
            try
            {
                var _emp = GetEmployeeLogin(_context);
                Guid _guid = new Guid(obj.IdS[0]);
                var _rs = _context.TaskOfUsers
                        .Where(x => x.TaskGuid == _guid && x.EmployeeGuid == _emp.EmployeeGuid)
                        .FirstOrDefault();
                if (_rs.IsFollowed == true)
                {
                    _rs.IsFollowed = false;
                }
                else
                {
                    _rs.IsFollowed = true;
                }
                _context.Entry(_rs);
                _context.Entry(_rs).Property(x => x.IsFollowed).IsModified = true;
                _context.SaveChanges();
                CreateLogTaskActivities(_context, "-1", "", "Cập nhật theo dõi công việc.", _rs.TaskGuid, _emp);
                return new JMessage() { Error = false, Title = "Cập nhật theo dõi thành công" };
            }
            catch (Exception ex)
            {
                return new JMessage() { Error = true, Title = "Có lỗi trong quá trình thêm theo dõi", Object = ex.ToString() };
            }
        }
        #endregion
    }
}
