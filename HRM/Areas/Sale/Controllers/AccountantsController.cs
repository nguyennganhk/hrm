﻿using FTU.Utils.HelperNet;
using HRM.Controllers;
using HRM.Models;
using HRM.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;
using System.Data;
using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Newtonsoft.Json;
using Grpc.Core;
using DocumentFormat.OpenXml.Drawing;
using ES_MODEL.HR.Custom.Wf;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using DocumentFormat.OpenXml.Drawing.Charts;
using ES_MODEL.Task;
using HRM.Filters;
using ES_MODEL.HR;
using Aspose.Pdf;
using DocumentFormat.OpenXml.Bibliography;
using DocumentFormat.OpenXml.Wordprocessing;
using Microsoft.CodeAnalysis;
using DocumentFormat.OpenXml.VariantTypes;
using static Aspose.Pdf.Operator;
using System.Threading;
using HRM.Models.Custom.Hr;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Math;
using System.Web;
using DocumentFormat.OpenXml.Spreadsheet;
using Stimulsoft.Report.Dashboard;
using DocumentFormat.OpenXml.Drawing.Spreadsheet;
using static Stimulsoft.Report.StiOptions.Wpf;
using Stimulsoft.System.Web.UI.WebControls;
using Items = HRM.Models.Items;
using DocumentFormat.OpenXml.ExtendedProperties;
using DataTable = System.Data.DataTable;

namespace HRM.Areas.Sale.Controllers
{
    [Authorize]
    public class AccountantsController : BaseController
    {
        public IConfigurationRoot Configuration { get; }
        private readonly HRMDBContext _context;
        private readonly IHostingEnvironment _environment;
        private readonly AppSettings _appSettings;

        public AccountantsController(IOptions<AppSettings> appSettings, HRMDBContext context, IHostingEnvironment environment)
        {
            _context = context;
            _environment = environment;
            _appSettings = appSettings.Value;
        }
        [Area("Sale")]
        [ActionAuthorize(EAction.ACCESS)]
        public IActionResult Index()
        {
            return View();
        }
        public class JTableCustomer : JTableModel
        {
            public string Keyword { set; get; }
            public string IsBussiness { set; get; }
            public string Status { set; get; }
            public string Types { set; get; }
            public string IsProvince { set; get; }
            public string ProvinceId { set; get; }
            public string TerritoryId { set; get; }
            public Guid? RowGuid { set; get; }
            public string WorkFlowsContent { set; get; }
            public bool? IsAccountant { set; get; }

        }
        public class approveObj
        {
            public string Types { set; get; }
            public Guid? RowGuid { set; get; }
            public string WorkFlowsContent { set; get; }
            public bool? IsAccountant { set; get; }

        }
        [HttpGet]
        public object GetPicfile(Guid? id)
        {
            try
            {
                var Photo = _context.AccountantsComments_DOC.Where(x => x.AttachmentGuid == id).Select(x => x.Attachment).FirstOrDefault();
                byte[] ImageTemp = Photo;

                FileContentResult hd = null;
                hd = File(ImageTemp, "image/jpeg");
                int gh = ImageTemp.Count();
                if (gh > 0)
                {
                    return hd;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }
        [HttpPost]
        public object JTable([FromBody] JTableSearch jTable)
        {
            var msg = new JMessage() { Error = false };
            var _emp = GetEmployeeLogin(_context);
            try
            {
                int intBeginFor = (jTable.CurrentPage - 1) * jTable.Length;
                string sql = "[Sale].[SP_ESHR_Accountants_JTable] " +
                   "@Keyword, " +
                   "@lstMonth," +
                   "@Years," +
                   "@StartDate," +
                    "@EndDate," +
                   "@Type," +
                    "@LoginName," +
                   "@Permisstion," +
                   "@Sort, " +
                   "@Skip, " +
                   "@Take," +
                   "@Count out";
                var Keyword = SqlPara("@Keyword ", jTable.Keyword, SqlDbType.NVarChar);
                var lstMonth = new SqlParameter("@lstMonth", jTable.lstString.Count >= 1 ? string.Join(",", jTable.lstString) : "");
                var Years = SqlPara("@Years ", jTable.Years, SqlDbType.NVarChar);
                var StartDate = SqlPara("@StartDate ", jTable.StartDate, SqlDbType.Date);
                var EndDate = SqlPara("@EndDate ", jTable.EndDate, SqlDbType.Date);
                var Type = SqlPara("@Type ", jTable.Type, SqlDbType.VarChar);
                var LoginName = SqlPara("@LoginName ", _emp.LoginName, SqlDbType.NVarChar);
                var Permisstion = SqlPara("@Permisstion ", jTable.Permisstion, SqlDbType.VarChar);
                var Sort = SqlPara("@Sort", jTable.QueryOrderBy, SqlDbType.VarChar);
                var Skip = SqlPara("@Skip", intBeginFor, SqlDbType.Int);
                var Take = SqlPara("@Take ", jTable.Length, SqlDbType.Int);
                var Count = new SqlParameter("@Count", SqlDbType.Int) { Direction = System.Data.ParameterDirection.Output };
                var data = _context.AccountantsViews.FromSql(sql, Keyword, lstMonth, Years, StartDate, EndDate, Type, LoginName, Permisstion, Sort, Skip, Take, Count).ToList();
                var jdata = JTableHelper.JObjectTable(data, jTable.Draw, (int)Count.Value);
                return Json(jdata);
            }
            catch (Exception ex)
            {
                return new JMessage() { Error = true, Title = ex.Message, Object = ex };
            }
        }
        [Area("Sale")]
        public object FactoryType(string q)
        {
            try
            {
                var lstItem = new List<Object_Jexcel>();
                var _obj = new Object_Jexcel() { id = "EXW", name = "EXW" };
                var _obj1 = new Object_Jexcel() { id = "FOB", name = "FOB" };
                var _obj2 = new Object_Jexcel() { id = "CNF", name = "CNF" };
                var _obj3 = new Object_Jexcel() { id = "CIF", name = "CIF" };
                lstItem.Add(_obj); lstItem.Add(_obj1); lstItem.Add(_obj2); lstItem.Add(_obj3);
                return Json(lstItem);
            }
            catch (Exception ex)
            {
                return Json(new JMessage { Title = "Lấy dữ liệu không thành công", Object = ex, Error = true });
            }
        }

        [HttpPost]
        public object Submit()
        {
            var msg = new JMessage() { Error = false };
            var _emp = GetEmployeeLogin(_context);
            try
            {
                var files = Request.Form.Files.ToList();
                var submit = Request.Form["submit"];
                var detail = Request.Form["detail"];
                var detailDelete = Request.Form["detailDelete"];
                Accountants obj = JsonConvert.DeserializeObject<Accountants>(submit);
                List<AccountantsDetails> lstdetail = JsonConvert.DeserializeObject<List<AccountantsDetails>>(detail);
                List<Guid> ListdetailDelete = JsonConvert.DeserializeObject<List<Guid>>(detailDelete);

                string sql = "[Sale].[SP_ESHR_Accountants_Insert] @RowGuid,@OrganizationGuid,@CodeID,@StatusAcc,@DateSX,@EmployeeRequire,@EmployeeID,@EmployeeName,@Note,@CreatedDate ,@ModifiedDate,@CreatedBy ,@ModifiedBy,@lstDeleteDetail,@AccountantsDetails,@Error out";
                if (obj.DateSX.HasValue)
                {
                    obj.DateSX = Convert.ToDateTime(obj.DateSX).ToLocalTime();
                }
                else
                {
                    obj.DateSX = null;
                }

                var RowGuid = SqlPara("@RowGuid ", obj.RowGuid, SqlDbType.UniqueIdentifier);
                var OrganizationGuid = SqlPara("@OrganizationGuid ", _emp.OrganizationGuid, SqlDbType.UniqueIdentifier);
                var CodeID = SqlPara("@CodeID ", obj.CodeID, SqlDbType.NVarChar);
                var DateSX = SqlPara("@DateSX ", obj.DateSX, SqlDbType.Date);
                var StatusAcc = SqlPara("@StatusAcc ", obj.StatusAcc, SqlDbType.NVarChar);
                var EmployeeRequire = SqlPara("@EmployeeRequire", obj.EmployeeRequire, SqlDbType.NVarChar);
                var EmployeeID = SqlPara("@EmployeeID ", obj.EmployeeID, SqlDbType.NVarChar);
                var EmployeeName = SqlPara("@EmployeeName ", obj.EmployeeName, SqlDbType.NVarChar);

                obj.CreatedDate = DateTime.Now;
                obj.ModifiedDate = DateTime.Now;
                obj.CreatedBy = GetCreatedBy(_context);
                obj.ModifiedBy = GetCreatedBy(_context);
                var CreatedDate = SqlPara("@CreatedDate ", obj.CreatedDate, SqlDbType.DateTime);
                var ModifiedDate = SqlPara("@ModifiedDate ", obj.ModifiedDate, SqlDbType.DateTime);
                var CreatedBy = SqlPara("@CreatedBy ", obj.CreatedBy, SqlDbType.NVarChar);
                var ModifiedBy = SqlPara("@ModifiedBy ", obj.ModifiedBy, SqlDbType.NVarChar);
                var Note = SqlPara("@Note ", obj.Note, SqlDbType.NVarChar);
                var _lstDeleteDetail = new SqlParameter("@lstDeleteDetail", ListdetailDelete.Count >= 1 ? string.Join(",", ListdetailDelete) : "");

                var _detail = new SqlParameter("@AccountantsDetails", SqlDbType.Structured);
                var _tem = new System.Data.DataTable();
                _tem.Columns.Add("RowGuid      ");
                _tem.Columns.Add("RecordGuid   ");
                _tem.Columns.Add("ItemName     ");
                _tem.Columns.Add("NumberOrder     ");
                _tem.Columns.Add("UnitID       ");
                _tem.Columns.Add("UnitName     ");
                _tem.Columns.Add("Quantity     ");
                _tem.Columns.Add("UnitPrice    ");
                _tem.Columns.Add("AmountOc     ");
                _tem.Columns.Add("StatusOrder   ");
                _tem.Columns.Add("Method  ");
                _tem.Columns.Add("Note  ");
                _tem.Columns.Add("CreatedDate  ");
                _tem.Columns.Add("ModifiedDate ");
                _tem.Columns.Add("CreatedBy    ");
                _tem.Columns.Add("ModifiedBy   ");
                _tem.Columns.Add("SortOrder    ");

                foreach (var _item in lstdetail)
                {
                    _tem.Rows.Add(
                            _item.RowGuid
                            , _item.RecordGuid
                            , _item.ItemName
                             , _item.NumberOrder
                            , _item.UnitID
                            , _item.UnitName
                            , _item.Quantity
                            , _item.UnitPrice
                            , _item.AmountOc
                            , _item.StatusOrder
                            , _item.Method
                            , _item.Note
                            , DBNull.Value
                            , DBNull.Value
                            , DBNull.Value
                            , DBNull.Value
                            , _item.SortOrder
                             );
                }
                _detail.Value = _tem;
                _detail.TypeName = "[Sale].[AccountantsDetails]";

                var Error = new SqlParameter("@Error", SqlDbType.NVarChar, 4000) { Direction = System.Data.ParameterDirection.Output };
                var rs = _context.SQLCOMMANDS.FromSql(sql, RowGuid, OrganizationGuid, CodeID, StatusAcc, DateSX, EmployeeRequire, EmployeeID, EmployeeName, Note, CreatedDate, ModifiedDate, CreatedBy, ModifiedBy, _lstDeleteDetail, _detail, Error).FirstOrDefault();
                if (rs.Error == "")
                {
                    return new JMessage() { Error = false, Title = "Thêm thành công" };
                }
                else
                {
                    return new JMessage() { Error = true, Title = rs.Error };
                }
            }
            catch (Exception ex)
            {
                return new JMessage() { Error = true, Title = ex.Message, Object = ex };
            }
        }

        [HttpPost]
        public object Delete(Guid Id)
        {
            var msg = new JMessage() { Error = false };
            try
            {
                string sql = "[Sale].[SP_ESHR_Accountants_Delete] @RowGuid,@Error out";
                var RowGuid = new SqlParameter("@RowGuid", SqlDbType.UniqueIdentifier); CheckNullParameterStore(RowGuid, Id);
                var Error = new SqlParameter("@Error", SqlDbType.NVarChar, 1000) { Direction = System.Data.ParameterDirection.Output };
                var rs = _context.SQLCOMMANDS.FromSql(sql, RowGuid, Error).FirstOrDefault();
                if (rs.Error == "")
                {
                    return new JMessage() { Error = false, Title = "Xóa thành công" };
                }
                else
                {
                    return new JMessage() { Error = true, Title = rs.Error };
                }
            }
            catch (Exception ex)
            {
                return new JMessage() { Error = true, Title = ex.Message, Object = ex };
            }
        }
        [HttpPost]
        public object GetItem(Guid Id)
        {
            var msg = new JMessage() { Error = false };
            try
            {
                string sql = "[Sale].[SP_ESHR_Accountants_GetItem] @RowGuid";
                var RowGuid = new SqlParameter("@RowGuid", SqlDbType.UniqueIdentifier); CheckNullParameterStore(RowGuid, Id);
                var rs = _context.Object_Table5.FromSql(sql, RowGuid).FirstOrDefault();
                return rs;
            }
            catch (Exception ex)
            {
                return new JMessage() { Error = true, Title = ex.Message, Object = ex };
            }
        }
        public class objTitleFile
        {
            public int? STT { get; set; }
            public string Title { get; set; }
        }
        public class ObjAttach
        {
            public Guid RecordGuid { get; set; }
            public int? STT { get; set; }

        }
        [HttpPost]
        //[ActionAuthorize(EAction.EDIT)]
        public JsonResult UpdateFile()
        {
            var msg = new JMessage() { Error = false, Title = "Cập nhật thành công." };
            try
            {
                var lstTitlefile = Request.Form["lstTitlefile"];
                List<objTitleFile> lstTitle = JsonConvert.DeserializeObject<List<objTitleFile>>(lstTitlefile);
                var DeleteAttach = Request.Form["DeleteAttach"];
                var _emp = GetEmployeeLogin(_context);
                var files = Request.Form.Files.ToList();
                var _obj = Request.Form["model"];
                ObjAttach obj = JsonConvert.DeserializeObject<ObjAttach>(_obj);
                List<Guid> deleteFileattach = JsonConvert.DeserializeObject<List<Guid>>(DeleteAttach);

                if (deleteFileattach.Count > 0)
                {
                    foreach (var id in deleteFileattach)
                    {
                        string sqlAttach = "[HR].[SP_Delete_Attachment] @database, @scheme,@TableName,@OrganizationGuid,@AttachmentGuid,@Error out";
                        var database = new SqlParameter("@database ", SqlDbType.NVarChar, 250); CheckNullParameterStore(database, _appSettings.DBName_HR);
                        var scheme = new SqlParameter("@scheme ", SqlDbType.NVarChar, 250); CheckNullParameterStore(scheme, "Sale");
                        var OrganizationGuid = new SqlParameter("@OrganizationGuid ", SqlDbType.NVarChar); CheckNullParameterStore(OrganizationGuid, _emp.OrganizationGuid.ToString());
                        var TableName = new SqlParameter("@TableName ", SqlDbType.NVarChar, 50); CheckNullParameterStore(TableName, "Accountants_DOC");
                        var AttachGuid = new SqlParameter("@AttachmentGuid ", SqlDbType.NVarChar); CheckNullParameterStore(AttachGuid, id.ToString());
                        var Error = new SqlParameter("@Error ", SqlDbType.NVarChar) { Direction = ParameterDirection.Output }; CheckNullParameterStore(Error, "");
                        var temattach = _context.SQLCOMMANDS.FromSql(sqlAttach, database, scheme, TableName, OrganizationGuid, AttachGuid, Error).FirstOrDefault();
                        if (Error.Value.ToString() == "")
                        {
                            msg.Title = "Xóa đính kèm thành công.";
                        }
                        else
                        {
                            msg.Title = "Xóa đính kèm không thành công."; msg.Error = true;
                        }
                    }
                }
                if (files.Count > 0)
                {
                    string sqlAttach = "[Hr].[SP_Insert_Attachment_for_Calendar] @database, @scheme,@TableName,@TableAttachment";
                    var database = new SqlParameter("@database ", SqlDbType.NVarChar, 250); CheckNullParameterStore(database, _appSettings.DBName_HR);
                    var scheme = new SqlParameter("@scheme ", SqlDbType.NVarChar, 250); CheckNullParameterStore(scheme, "Sale");
                    var TableName = new SqlParameter("@TableName ", SqlDbType.NVarChar, 50); CheckNullParameterStore(TableName, "Accountants_DOC");
                    var _detailAttach = new SqlParameter("@TableAttachment", SqlDbType.Structured);
                    /// table detail
                    var _tematach = new DataTable();
                    _tematach.Columns.Add("AttachmentGuid");
                    _tematach.Columns.Add("ParentGuid");
                    _tematach.Columns.Add("RecordGuid");
                    _tematach.Columns.Add("OrganizationGuid");
                    _tematach.Columns.Add("DepartmentGuid");
                    _tematach.Columns.Add("ModuleID");
                    _tematach.Columns.Add("Title");
                    _tematach.Columns.Add("FileName");
                    _tematach.Columns.Add("Attachment", typeof(byte[]));
                    _tematach.Columns.Add("FileExtension");
                    _tematach.Columns.Add("FileSize");
                    _tematach.Columns.Add("UrlExternal");
                    _tematach.Columns.Add("IsPublic");
                    _tematach.Columns.Add("PrivateLevel");
                    _tematach.Columns.Add("IsFolder");
                    _tematach.Columns.Add("IsHidden");
                    _tematach.Columns.Add("IsReadonly");
                    _tematach.Columns.Add("IsSync");
                    _tematach.Columns.Add("SyncTime");
                    _tematach.Columns.Add("CloudPath");
                    _tematach.Columns.Add("CreatedDate");
                    _tematach.Columns.Add("ModifiedDate");
                    _tematach.Columns.Add("CreatedBy");
                    _tematach.Columns.Add("ModifiedBy");
                    for (var i = 0; i < files.Count; i++)
                    {
                        for (var j = 0; j < lstTitle.Count; j++)
                        {
                            if (i == lstTitle[j].STT)
                            {
                                var stream = files[i].OpenReadStream();
                                byte[] byteArr = Utilities.StreamToByteArray(stream);
                                var name = files[i].FileName;

                                ContractAttachments _attach = new ContractAttachments()
                                {
                                    AttachmentGuid = Guid.NewGuid(),
                                    RecordGuid = obj.RecordGuid,
                                    OrganizationGuid = _emp.OrganizationGuid,
                                    Title = lstTitle[j].Title,
                                    FileName = name,
                                    CreatedDate = DateTime.Now,
                                    CreatedBy = GetCreatedBy(_context),
                                    ModifiedDate = DateTime.Now,
                                    ModifiedBy = GetModifiedBy(_context),
                                    Attachment = byteArr,
                                    ModuleId = "HR",
                                    FileSize = byteArr.Length,
                                    FileExtension = System.IO.Path.GetExtension(files[i].FileName)
                                };
                                _tematach.Rows.Add(_attach.AttachmentGuid,
                                    _attach.ParentGuid,
                                    _attach.RecordGuid,
                                    _attach.OrganizationGuid,
                                    _attach.DepartmentGuid,
                                    _attach.ModuleId,
                                    _attach.Title,
                                    _attach.FileName,
                                    _attach.Attachment,
                                    _attach.FileExtension,
                                    _attach.FileSize,
                                    _attach.UrlExternal,
                                    _attach.IsPublic,
                                    _attach.PrivateLevel,
                                    _attach.IsFolder,
                                    _attach.IsHidden,
                                    _attach.IsReadonly,
                                    _attach.IsSync,
                                   DBNull.Value,
                                    _attach.CloudPath,
                                    DBNull.Value, DBNull.Value, _attach.CreatedBy, _attach.ModifiedBy);
                            }
                        }
                    }
                    _detailAttach.Value = _tematach;
                    _detailAttach.TypeName = "[dbo].[Attachment]";
                    var temattach = _context.SQLCOMMANDS.FromSql(sqlAttach, database, scheme, TableName, _detailAttach).FirstOrDefault();
                    if (temattach.Error == "")
                    {
                        msg.Title = "Thêm mới thành công.";
                    }
                    else
                    {
                        msg.Title = "Thêm mới không thành công."; msg.Error = true;
                    }
                }
            }
            catch (SqlException ex)
            {
                msg.Title = "Có lỗi khi đính kèm";
                msg.Object = ex;
                msg.Error = true;
            }
            return Json(msg);
        }
        [HttpGet]
        public FileResult DownloadItemAttachment(Guid? Id)
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var username = GetEmployeeLogin(_context);
                var gui = new Guid();
                string sql = "[dbo].[SP_Download_Attachment] @Database,@Scheme, @TableName, @OrganizationGuid, @AttachmentGuid, @Error";
                var Database = new SqlParameter("@Database", SqlDbType.VarChar); Database.Value = _appSettings.DBName_HR;
                var Scheme = new SqlParameter("@Scheme", SqlDbType.VarChar); Scheme.Value = "Sale";
                var TableName = new SqlParameter("@TableName", SqlDbType.VarChar); TableName.Value = "Accountants_DOC";
                var OrganizationGuid = new SqlParameter("@OrganizationGuid", SqlDbType.UniqueIdentifier); OrganizationGuid.Value = username.OrganizationGuid;
                var AttachmentGuid = new SqlParameter("@AttachmentGuid", SqlDbType.UniqueIdentifier); AttachmentGuid.Value = Id == gui ? (Object)DBNull.Value : Id;
                var Error = new SqlParameter("@Error", SqlDbType.NVarChar); Error.Value = "";
                var rs = _context.AttachmentsDownloadView_.FromSql(sql, Database, Scheme, TableName, OrganizationGuid, AttachmentGuid, Error).SingleOrDefault();
                if (rs != null)
                {
                    byte[] fileBytes = rs.Attachment;
                    var fileName = rs.FileName;
                    return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);

                }
                else return null;
            }
            catch (Exception ex)
            {
                msg.Title = "Có lỗi khi lấy dữ liệu.";
                return null;
            }
        }
        [HttpPost]
        //  [Filters.ActionAuthorize(EAction.OPEN)]
        public JsonResult GetItemAttachment([FromBody] ContractAttachments obj)
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var username = GetEmployeeLogin(_context);
                string sql = "[HR].[SP_GET_Attachment_Calendar] @Database, @Scheme, @TableName, @OrganizationGuid, @RecordGuid, @Error";
                var database = new SqlParameter("@Database ", SqlDbType.NVarChar); CheckNullParameterStore(database, _appSettings.DBName_HR);
                var Scheme = new SqlParameter("@Scheme", SqlDbType.VarChar); Scheme.Value = "Sale";
                var TableName = new SqlParameter("@TableName", SqlDbType.VarChar); TableName.Value = "Accountants_DOC";
                var OrganizationGuid = SqlPara("@OrganizationGuid", username.OrganizationGuid.ToString(), SqlDbType.NVarChar);
                var RecordGuid = SqlPara("@RecordGuid", obj.RecordGuid.ToString(), SqlDbType.NVarChar);
                var Error = new SqlParameter("@Error", SqlDbType.NVarChar); Error.Value = "";
                var rs = _context.AttachmentsView.FromSql(sql, database, Scheme, TableName, OrganizationGuid, RecordGuid, Error).ToList();
                return Json(rs);
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Title = ex.Message, Object = ex });
            }
        }
        [HttpPost]
        public JMessage InsertComments()
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var userlogin = GetEmployeeLogin(_context);
                var files = Request.Form.Files.ToList();
                var _obj = Request.Form["insert"];
                ProductionManagerComments obj = JsonConvert.DeserializeObject<ProductionManagerComments>(_obj);
                obj.CommentGuid = Guid.NewGuid();
                obj.OrganizationGuid = userlogin.OrganizationGuid;
                obj.EmployeeGuid = userlogin.EmployeeGuid;
                obj.EmployeeId = userlogin.EmployeeId;
                obj.EmployeeName = userlogin.FullName;
                obj.DepartmentGuid = userlogin.DepartmentGuid;
                obj.DepartmentId = userlogin.DepartmentId;
                obj.DepartmentName = userlogin.DepartmentName;
                obj.AvatarUrl = "";
                obj.CreatedDate = DateTime.Now;
                obj.CreatedBy = GetCreatedBy(_context);
                obj.ModifiedDate = DateTime.Now;
                obj.ModifiedBy = GetModifiedBy(_context);
                string sql = "[Hr].[SP_Insert_Comments_Hr] @Scheme, @TableName, @CommentGuid,@ParentGuid,@RecordGuid,@OrganizationGuid,@Comment,@EmployeeGuid,@EmployeeID,@EmployeeName,@LoginName,@DepartmentGuid,@DepartmentID,@DepartmentName,@AvatarUrl,@CreatedDate,@CreatedBy,@ModifiedDate,@ModifiedBy,@Error";
                var _Scheme = new SqlParameter("@Scheme", SqlDbType.VarChar);
                CheckNullParameterStore(_Scheme, "Sale");
                var _TableName = new SqlParameter("@TableName", SqlDbType.NVarChar);
                CheckNullParameterStore(_TableName, "AccountantsComments");
                var _CommentGuid = new SqlParameter("@CommentGuid", SqlDbType.UniqueIdentifier);
                CheckNullParameterStore(_CommentGuid, obj.CommentGuid);
                var _ParentGuid = new SqlParameter("@ParentGuid", SqlDbType.UniqueIdentifier);
                CheckNullParameterStore(_ParentGuid, obj.ParentGuid);
                var _RecordGuid = new SqlParameter("@RecordGuid", SqlDbType.UniqueIdentifier);
                CheckNullParameterStore(_RecordGuid, obj.RecordGuid);
                var _OrganizationGuid = new SqlParameter("@OrganizationGuid", SqlDbType.UniqueIdentifier);
                CheckNullParameterStore(_OrganizationGuid, obj.OrganizationGuid);
                var _Comment = new SqlParameter("@Comment", SqlDbType.NVarChar);
                CheckNullParameterStore(_Comment, obj.Comment);
                var _EmployeeGuid = new SqlParameter("@EmployeeGuid", SqlDbType.UniqueIdentifier);
                CheckNullParameterStore(_EmployeeGuid, obj.EmployeeGuid);
                var _EmployeeID = new SqlParameter("@EmployeeID", SqlDbType.VarChar, 20);
                CheckNullParameterStore(_EmployeeID, obj.EmployeeId);
                var _EmployeeName = new SqlParameter("@EmployeeName", SqlDbType.NVarChar, 50);
                CheckNullParameterStore(_EmployeeName, obj.EmployeeName);
                var _LoginName = new SqlParameter("@LoginName", SqlDbType.NVarChar, 50);
                CheckNullParameterStore(_LoginName, userlogin.LoginName);
                var _DepartmentGuid = new SqlParameter("@DepartmentGuid", SqlDbType.UniqueIdentifier);
                CheckNullParameterStore(_DepartmentGuid, obj.DepartmentGuid);
                var _DepartmentID = new SqlParameter("@DepartmentID", SqlDbType.VarChar, 20);
                CheckNullParameterStore(_DepartmentID, obj.DepartmentId);
                var _DepartmentName = new SqlParameter("@DepartmentName", SqlDbType.NVarChar, 50);
                CheckNullParameterStore(_DepartmentName, obj.DepartmentName);
                var _AvatarUrl = new SqlParameter("@AvatarUrl", SqlDbType.NVarChar, 255);
                CheckNullParameterStore(_AvatarUrl, obj.AvatarUrl);
                var _CreatedDate = new SqlParameter("@CreatedDate", SqlDbType.DateTime);
                CheckNullParameterStore(_CreatedDate, obj.CreatedDate);
                var _CreatedBy = new SqlParameter("@CreatedBy", SqlDbType.NVarChar, 250);
                CheckNullParameterStore(_CreatedBy, obj.CreatedBy);
                var _ModifiedDate = new SqlParameter("@ModifiedDate", SqlDbType.DateTime);
                CheckNullParameterStore(_ModifiedDate, obj.ModifiedDate);
                var _ModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.NVarChar, 250);
                CheckNullParameterStore(_ModifiedBy, obj.ModifiedBy);
                var _Error = new SqlParameter("@Error", SqlDbType.NVarChar); _Error.Value = "";
                var rs = _context.SQLCOMMANDS.FromSql(sql, _Scheme, _TableName, _CommentGuid, _ParentGuid, _RecordGuid, _OrganizationGuid, _Comment, _EmployeeGuid, _EmployeeID, _EmployeeName, _LoginName, _DepartmentGuid, _DepartmentID, _DepartmentName, _AvatarUrl, _CreatedDate, _CreatedBy, _ModifiedDate, _ModifiedBy, _Error).FirstOrDefault();

                if (files.Count > 0)
                {

                    string sqlAttach = "[dbo].[SP_Insert_Attachment] @Database, @Scheme,@TableName,@TableAttachment";
                    var Database = new SqlParameter("@Database", SqlDbType.NVarChar, 50); CheckNullParameterStore(Database, _appSettings.DBName_HR);
                    var scheme = new SqlParameter("@scheme ", SqlDbType.NVarChar, 250); CheckNullParameterStore(scheme, "Sale");
                    var TableName = new SqlParameter("@TableName ", SqlDbType.NVarChar, 50); CheckNullParameterStore(TableName, "AccountantsComments_DOC");
                    var _detailAttach = new SqlParameter("@TableAttachment", SqlDbType.Structured);

                    /// table detail
                    var _tematach = new System.Data.DataTable();
                    _tematach.Columns.Add("AttachmentGuid");
                    _tematach.Columns.Add("ParentGuid");
                    _tematach.Columns.Add("RecordGuid");
                    _tematach.Columns.Add("OrganizationGuid");
                    _tematach.Columns.Add("DepartmentGuid");
                    _tematach.Columns.Add("ModuleID");
                    _tematach.Columns.Add("Title");
                    _tematach.Columns.Add("FileName");
                    _tematach.Columns.Add("Attachment", typeof(byte[]));
                    _tematach.Columns.Add("FileExtension");
                    _tematach.Columns.Add("FileSize");
                    _tematach.Columns.Add("UrlExternal");
                    _tematach.Columns.Add("IsPublic");
                    _tematach.Columns.Add("PrivateLevel");
                    _tematach.Columns.Add("IsFolder");
                    _tematach.Columns.Add("IsHidden");
                    _tematach.Columns.Add("IsReadonly");
                    _tematach.Columns.Add("IsSync");
                    _tematach.Columns.Add("SyncTime");
                    _tematach.Columns.Add("CloudPath");

                    _tematach.Columns.Add("CreatedDate", typeof(DateTime));
                    _tematach.Columns.Add("ModifiedDate", typeof(DateTime));
                    _tematach.Columns.Add("CreatedBy");
                    _tematach.Columns.Add("ModifiedBy");

                    foreach (var file in files)
                    {
                        var stream = file.OpenReadStream();
                        byte[] byteArr = Utilities.StreamToByteArray(stream);
                        var name = file.FileName;
                        Hrattachments _attach = new Hrattachments()
                        {
                            AttachmentGuid = Guid.NewGuid(),

                            RecordGuid = obj.CommentGuid,
                            OrganizationGuid = userlogin.OrganizationGuid,
                            Title = name,
                            FileName = name,
                            CreatedDate = DateTime.Now,
                            CreatedBy = GetCreatedBy(_context),
                            ModifiedDate = DateTime.Now,
                            ModifiedBy = GetModifiedBy(_context),
                            Attachment = byteArr,
                            ModuleId = "ES_HR",
                            FileSize = byteArr.Length,
                            FileExtension = System.IO.Path.GetExtension(name)
                        };
                        _tematach.Rows.Add(_attach.AttachmentGuid,
                              _attach.ParentGuid,
                              _attach.RecordGuid,
                              _attach.OrganizationGuid,
                              _attach.DepartmentGuid,
                              _attach.ModuleId,
                              _attach.Title,
                              _attach.FileName,
                              _attach.Attachment,
                              _attach.FileExtension,
                              _attach.FileSize,
                              _attach.UrlExternal,
                              _attach.IsPublic,
                              _attach.PrivateLevel,
                              _attach.IsFolder,
                              _attach.IsHidden,
                              _attach.IsReadonly,

                              _attach.IsSync,
                             DBNull.Value,
                              _attach.CloudPath,
                              DateTime.Now, DateTime.Now, _attach.CreatedBy, _attach.ModifiedBy);
                    }

                    _detailAttach.Value = _tematach;
                    _detailAttach.TypeName = "[dbo].[Attachment]";
                    var __temattach = _context.SQLCOMMANDS.FromSql(sqlAttach, Database, scheme, TableName, _detailAttach).FirstOrDefault();

                    if (__temattach.Error == "")
                    {
                        var mesage = __temattach.Error;
                    }
                    else
                    {
                        return new JMessage() { Error = true, Title = "Thêm file không thành công", Object = __temattach };
                    }

                }
                if (rs.Error == "")
                {

                    return new JMessage() { Error = false, Title = "Thêm ý kiến thành công." };
                }
                else
                {
                    return new JMessage() { Error = true, Title = rs.Error };
                }
            }
            catch (SqlException ex)
            {
                return new JMessage() { Error = true, Object = ex, Title = ex.Message };
            }
        }
        [HttpGet]
        [Area("Hr")]
        public FileResult DownloadfileCommentAttach(Guid? Id)
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var username = GetEmployeeLogin(_context);
                var gui = new Guid();
                string sql = "[dbo].[SP_Download_Attachment] @Database,@Scheme, @TableName, @OrganizationGuid, @AttachmentGuid, @Error";
                var Database = new SqlParameter("@Database", SqlDbType.VarChar); Database.Value = _appSettings.DBName_HR;
                var Scheme = new SqlParameter("@Scheme", SqlDbType.VarChar); Scheme.Value = "Sale";
                var TableName = new SqlParameter("@TableName", SqlDbType.VarChar); TableName.Value = "AccountantsComments_DOC";
                var OrganizationGuid = new SqlParameter("@OrganizationGuid", SqlDbType.UniqueIdentifier); OrganizationGuid.Value = username.OrganizationGuid;
                var AttachmentGuid = new SqlParameter("@AttachmentGuid", SqlDbType.UniqueIdentifier); AttachmentGuid.Value = Id == gui ? (Object)DBNull.Value : Id;
                var Error = new SqlParameter("@Error", SqlDbType.NVarChar); Error.Value = "";
                var rs = _context.AttachmentsDownloadView_.FromSql(sql, Database, Scheme, TableName, OrganizationGuid, AttachmentGuid, Error).SingleOrDefault();
                if (rs != null)
                {
                    byte[] fileBytes = rs.Attachment;
                    var fileName = rs.FileName;
                    return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);

                }
                else return null;
            }
            catch (Exception ex)
            {
                msg.Title = "Có lỗi khi lấy dữ liệu.";
                msg.Object = ex;
                return null;

            }
        }
        [Area("Sale")]
        public object StatusOrder(string q)
        {
            try
            {
                var lstItem = new List<Object_Jexcel>();
                var _obj = new Object_Jexcel() { id = "D", name = "Đã duyệt" };
                var _obj1 = new Object_Jexcel() { id = "C", name = "Chưa duyệt" };

                lstItem.Add(_obj); lstItem.Add(_obj1);
                return Json(lstItem);
            }
            catch (Exception ex)
            {
                return Json(new JMessage { Title = "Lấy dữ liệu không thành công", Object = ex, Error = true });
            }
        }
        public class CustomPage
        {
            public int Page { set; get; }
            public int ItemPage { set; get; }
            public int? Status { set; get; }
            public Guid? RecordGuid { set; get; }
        }
        /// Danh sách Comment
        [HttpPost]
        public object GetAllCommentPage([FromBody] CustomPage obj)
        {
            try
            {
                var _emp = GetEmployeeLogin(_context);

                var intBeginFor = (obj.Page - 1) * obj.ItemPage;
                string sql = "[Hr].[SP_GET_Comment] @DBName,@Scheme,@TableName,@DBNameDocument,@SchemeAt,@TableNameAt,@OrganizationGuid,@RecordGuid,@Skip,@Take,@Error";

                var DBName = new SqlParameter("@DBName", SqlDbType.NVarChar); CheckNullParameterStore(DBName, _appSettings.DBName_HR);
                var Scheme = new SqlParameter("@Scheme", SqlDbType.NVarChar); CheckNullParameterStore(Scheme, "Sale");
                var TableName = new SqlParameter("@TableName", SqlDbType.NVarChar); CheckNullParameterStore(TableName, "AccountantsComments");
                var DBNameDocument = new SqlParameter("@DBNameDocument", SqlDbType.NVarChar); CheckNullParameterStore(DBNameDocument, _appSettings.DBName_HR);
                var SchemeAt = new SqlParameter("@SchemeAt", SqlDbType.NVarChar); CheckNullParameterStore(SchemeAt, "Sale");
                var TableNameAt = new SqlParameter("@TableNameAt", SqlDbType.NVarChar); CheckNullParameterStore(TableNameAt, "AccountantsComments_DOC");
                var OrganizationGuid = new SqlParameter("@OrganizationGuid", SqlDbType.NVarChar); CheckNullParameterStore(OrganizationGuid, _emp.OrganizationGuid.ToString());
                var RecordGuid = new SqlParameter("@RecordGuid", SqlDbType.NVarChar); CheckNullParameterStore(RecordGuid, obj.RecordGuid.ToString());
                var Skip = new SqlParameter("@Skip", SqlDbType.NVarChar); Skip.Value = intBeginFor;
                var Take = new SqlParameter("@Take", SqlDbType.NVarChar); Take.Value = obj.ItemPage;
                var Error = new SqlParameter("@Error", SqlDbType.Int) { Direction = System.Data.ParameterDirection.Output };
                var rs = _context.CommentView.FromSql(sql, DBName, Scheme, TableName, DBNameDocument, SchemeAt, TableNameAt, OrganizationGuid, RecordGuid, Skip, Take, Error).ToList();

                return new JMessage() { Error = false, Title = "Success", Object = rs };

            }
            catch (Exception ex)
            {
                return new JMessage() { Error = true, Title = "Có lỗi khi lấy dữ liệu", Object = ex };
            }
        }
        [HttpPost]
        public object JTable_AccountantsDebt([FromBody] JTableSearch jTablePara)
        {
            var emp = GetEmployeeLogin(_context);
            if (jTablePara.lstString.Count > 0)
            {
                var count = _context.AccountantsDebt.Where(a => jTablePara.lstString.Contains(a.DateVB.Value.Month.ToString())
                                && a.DateVB.Value.Year.ToString() == jTablePara.Years).Count();
                int intBeginFor = (jTablePara.CurrentPage - 1) * jTablePara.Length;
                var balance = _context.AccountantsDebt.Where(a => jTablePara.lstString.Contains(a.DateVB.Value.Month.ToString())
                                && a.DateVB.Value.Year.ToString() == jTablePara.Years).Select(a => new { a.RowGuid, a.CodeID, a.Title, a.DateVBString, a.UnitID, a.UnitName, a.Quantity, a.StatusOrder, a.EmployeeID, a.EmployeeName }).OrderUsingSortExpression(jTablePara.QueryOrderBy).Skip(intBeginFor).Take(jTablePara.Length).ToList();
                var jdata = JTableHelper.JObjectTable(balance, jTablePara.Draw, count, "RowGuid", "CodeID", "Title", "DateVBString", "UnitID", "UnitName", "Quantity", "StatusOrder", "EmployeeID", "EmployeeName");
                return Json(jdata);
            }
            else
            {
                var count = _context.AccountantsDebt.Where(a => a.DateVB.Value.Year.ToString() == jTablePara.Years).Count();
                int intBeginFor = (jTablePara.CurrentPage - 1) * jTablePara.Length;
                var balance = _context.AccountantsDebt.Where(a => a.DateVB.Value.Year.ToString() == jTablePara.Years).Select(a => new { a.RowGuid, a.CodeID, a.Title, a.DateVBString, a.UnitID, a.UnitName, a.Quantity, a.StatusOrder, a.EmployeeID, a.EmployeeName }).OrderUsingSortExpression(jTablePara.QueryOrderBy).Skip(intBeginFor).Take(jTablePara.Length).ToList();
                var jdata = JTableHelper.JObjectTable(balance, jTablePara.Draw, count, "RowGuid", "CodeID", "Title", "DateVBString", "UnitID", "UnitName", "Quantity", "StatusOrder", "EmployeeID", "EmployeeName");
                return Json(jdata);
            }
        }
        [HttpPost]
        public object JTable_AccountantsManagement([FromBody] JTableSearch jTablePara)
        {
            var emp = GetEmployeeLogin(_context);

            if (jTablePara.lstString.Count > 0)
            {
                var count = _context.AccountantsManagement.Where(a => jTablePara.lstString.Contains(a.DatePS.Value.Month.ToString())
                                && a.DatePS.Value.Year.ToString() == jTablePara.Years).Count();

                int intBeginFor = (jTablePara.CurrentPage - 1) * jTablePara.Length;
                var balance = _context.AccountantsManagement.Where(a => jTablePara.lstString.Contains(a.DatePS.Value.Month.ToString())
                                && a.DatePS.Value.Year.ToString() == jTablePara.Years)
                    .Select(a => new { a.RowGuid, a.CodeID, a.Title, a.DatePSString, a.Description, a.UnitID, a.UnitName, a.Quantity, a.UnitPrice, a.AmountOc, a.EmployeeID, a.EmployeeName, a.EmployeeApprove, a.StatusOrder, a.Method })
                    .OrderUsingSortExpression(jTablePara.QueryOrderBy).Skip(intBeginFor).Take(jTablePara.Length).ToList();
                var jdata = JTableHelper.JObjectTable(balance, jTablePara.Draw, count, "RowGuid", "CodeID", "Title", "DatePSString", "Description", "UnitID", "UnitName", "Quantity", "UnitPrice", "AmountOc", "EmployeeID", "EmployeeName", "EmployeeApprove", "StatusOrder", "Method");
                return Json(jdata);
            }
            else
            {
                var count = _context.AccountantsManagement.Where(a =>
                                 a.DatePS.Value.Year.ToString() == jTablePara.Years).Count();

                int intBeginFor = (jTablePara.CurrentPage - 1) * jTablePara.Length;
                var balance = _context.AccountantsManagement.Where(a =>
                                 a.DatePS.Value.Year.ToString() == jTablePara.Years).Select(a => new { a.RowGuid, a.CodeID, a.Title, a.DatePSString, a.Description, a.UnitID, a.UnitName, a.Quantity, a.UnitPrice, a.AmountOc, a.EmployeeID, a.EmployeeName, a.EmployeeApprove, a.StatusOrder, a.Method })
                    .OrderUsingSortExpression(jTablePara.QueryOrderBy).Skip(intBeginFor).Take(jTablePara.Length).ToList();
                var jdata = JTableHelper.JObjectTable(balance, jTablePara.Draw, count, "RowGuid", "CodeID", "Title", "DatePSString", "Description", "UnitID", "UnitName", "Quantity", "UnitPrice", "AmountOc", "EmployeeID", "EmployeeName", "EmployeeApprove", "StatusOrder", "Method");
                return Json(jdata);
            }



        }
        [HttpPost]
        public object JTable_AccountantsOthers([FromBody] JTableSearch jTablePara)
        {
            var emp = GetEmployeeLogin(_context);
            var count = (from a in _context.AccountantsOthers
                         select new { a.RowGuid }).Count();

            int intBeginFor = (jTablePara.CurrentPage - 1) * jTablePara.Length;
            var balance = (from a in _context.AccountantsOthers
                           select new { a.RowGuid, a.CodeID, a.Title, a.OrderID,a.DatePSString, a.UnitID, a.UnitName, a.Quantity, a.UnitPrice, a.AmountOc, a.EmployeeID, a.EmployeeName, a.EmployeeApprove }).OrderUsingSortExpression(jTablePara.QueryOrderBy).Skip(intBeginFor).Take(jTablePara.Length).ToList();
            var jdata = JTableHelper.JObjectTable(balance, jTablePara.Draw, count, "RowGuid", "CodeID", "DatePSString", "Title", "OrderID", "UnitID", "UnitName", "Quantity", "UnitPrice", "AmountOc", "EmployeeID", "EmployeeName", "EmployeeApprove");
            return Json(jdata);
        }

        [HttpPost]
        public JsonResult Submit_AccountantsOthers()
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var _emp = GetEmployeeLogin(_context);
                var files = Request.Form.Files.ToList();
                var submit = Request.Form["submit"];
                AccountantsOthers obj = JsonConvert.DeserializeObject<AccountantsOthers>(submit);
                string sql = "[Sale].[SP_ESSM_AccountantsOthers_Insert] @RowGuid,@OrganizationGuid,@CodeID,@OrderID,@Title,@UnitID,@UnitName,@Quantity,@UnitPrice,@AmountOc,@EmployeeID,@EmployeeApprove,@EmployeeName,@Note,@CreatedDate,@ModifiedDate,@CreatedBy,@ModifiedBy,@DatePS";
                if (obj.DatePS.HasValue) { obj.DatePS = obj.DatePS.Value.ToLocalTime(); } else { obj.DatePS = null; }

                obj.CreatedDate = DateTime.Now;
                obj.ModifiedDate = DateTime.Now;
                obj.CreatedBy = GetCreatedBy(_context);
                obj.ModifiedBy = GetCreatedBy(_context);
                var RowGuid = SqlPara("@RowGuid                     ", obj.RowGuid, SqlDbType.UniqueIdentifier);
                var OrganizationGuid = SqlPara("@OrganizationGuid            ", obj.OrganizationGuid, SqlDbType.UniqueIdentifier);
                var CodeID = SqlPara("@CodeID                      ", obj.CodeID, SqlDbType.NVarChar);
                var OrderID = SqlPara("@OrderID                      ", obj.OrderID, SqlDbType.NVarChar);
                var Title = SqlPara("@Title                      ", obj.Title, SqlDbType.NVarChar);
                var UnitID = SqlPara("@UnitID                       ", obj.UnitID, SqlDbType.NVarChar);
                var UnitName = SqlPara("@UnitName                  ", obj.UnitName, SqlDbType.NVarChar);
                var Quantity = SqlPara("@Quantity                ", obj.Quantity, SqlDbType.Decimal);
                var UnitPrice = SqlPara("@UnitPrice       ", obj.UnitPrice, SqlDbType.Decimal);
                var AmountOc = SqlPara("@AmountOc                  ", obj.AmountOc, SqlDbType.Decimal);
                var EmployeeID = SqlPara("@EmployeeID             ", obj.EmployeeID, SqlDbType.NVarChar);
                var EmployeeApprove = SqlPara("@EmployeeApprove             ", obj.EmployeeApprove, SqlDbType.NVarChar);
                var EmployeeName = SqlPara("@EmployeeName             ", obj.EmployeeName, SqlDbType.NVarChar);
                var Note = SqlPara("@Note             ", obj.Note, SqlDbType.NVarChar);
                var CreatedDate = SqlPara("@CreatedDate                 ", obj.CreatedDate, SqlDbType.DateTime);
                var ModifiedDate = SqlPara("@ModifiedDate                ", obj.ModifiedDate, SqlDbType.DateTime);
                var CreatedBy = SqlPara("@CreatedBy                   ", obj.CreatedBy, SqlDbType.NVarChar);
                var ModifiedBy = SqlPara("@ModifiedBy                  ", obj.ModifiedBy, SqlDbType.NVarChar);
                var DatePS = SqlPara("@DatePS                  ", obj.DatePS, SqlDbType.Date);

                var _rs = _context.SQLCOMMANDS.FromSql(sql, RowGuid, OrganizationGuid, CodeID, OrderID, Title, UnitID, UnitName, Quantity, UnitPrice, AmountOc, EmployeeID, EmployeeApprove, EmployeeName, Note, CreatedDate, ModifiedDate, CreatedBy, ModifiedBy, DatePS).FirstOrDefault();
                if (_rs.Error == "")
                {
                    msg.Title = "Thêm mới thành công";
                }
                else
                {
                    return Json(new JMessage() { Error = true, Title = _rs.Error, Object = _rs.Error });
                }
            }
            catch (Exception ex)
            {
                msg.Error = true;
                msg.Object = ex;
                msg.Title = ex.Message;
            }
            return Json(msg);
        }

        [HttpPost]
        public JsonResult GetItem_AccountantsOthers(Guid Id)
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var _emp = GetEmployeeLogin(_context);
                var rs = _context.AccountantsOthers.Where(x => x.RowGuid == Id).FirstOrDefault();
                return Json(rs);
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = ex.Message });
            }
        }
        [HttpPost]
        public object Delete_AccountantsOthers(Guid Id)
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var _emp = GetEmployeeLogin(_context);
                var _ob = _context.Accountants_DOC.Where(x => x.RecordGuid == Id).ToList();
                foreach (var item in _ob)
                {
                    _context.Accountants_DOC.Remove(item);
                    _context.SaveChanges();
                }
                var _obj = _context.AccountantsOthers.Where(x => x.RowGuid == Id).FirstOrDefault();
                _context.AccountantsOthers.Remove(_obj);
                _context.SaveChanges();
                return Json(new JMessage() { Error = false, Title = "Xóa thành công" });
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = ex.Message });
            }
        }

        [HttpPost]
        public object getOrderAll()
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var rs = _context.ProductDeparments.Select(x => new { value = x.OrderID, text = x.OrderID }).ToList();
                return Json(rs);
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = ex.Message });
            }
        }


        [HttpPost]
        public JsonResult Submit_AccountantsManagement()
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var _emp = GetEmployeeLogin(_context);
                var files = Request.Form.Files.ToList();
                var submit = Request.Form["submit"];
                AccountantsManagement obj = JsonConvert.DeserializeObject<AccountantsManagement>(submit);
                string sql = "[Sale].[SP_ESSM_AccountantsManagement_Insert] @RowGuid,@OrganizationGuid,@CodeID,@DatePS,@Title,@Description,@UnitID,@UnitName,@Quantity,@UnitPrice,@AmountOc,@EmployeeID,@EmployeeApprove,@EmployeeName,@StatusOrder,@Method,@Note,@CreatedDate,@ModifiedDate,@CreatedBy,@ModifiedBy";
                if (obj.DatePS.HasValue) { obj.DatePS = obj.DatePS.Value.ToLocalTime(); } else { obj.DatePS = null; }

                obj.CreatedDate = DateTime.Now;
                obj.ModifiedDate = DateTime.Now;
                obj.CreatedBy = GetCreatedBy(_context);
                obj.ModifiedBy = GetCreatedBy(_context);
                var RowGuid = SqlPara("@RowGuid                     ", obj.RowGuid, SqlDbType.UniqueIdentifier);
                var OrganizationGuid = SqlPara("@OrganizationGuid            ", obj.OrganizationGuid, SqlDbType.UniqueIdentifier);
                var CodeID = SqlPara("@CodeID                      ", obj.CodeID, SqlDbType.NVarChar);
                var DatePS = SqlPara("@DatePS                      ", obj.DatePS, SqlDbType.Date);
                var Title = SqlPara("@Title                      ", obj.Title, SqlDbType.NVarChar);
                var Description = SqlPara("@Description                  ", obj.Description, SqlDbType.NVarChar);

                var UnitID = SqlPara("@UnitID                       ", obj.UnitID, SqlDbType.NVarChar);
                var UnitName = SqlPara("@UnitName                  ", obj.UnitName, SqlDbType.NVarChar);
                var Quantity = SqlPara("@Quantity                ", obj.Quantity, SqlDbType.Decimal);
                var UnitPrice = SqlPara("@UnitPrice       ", obj.UnitPrice, SqlDbType.Decimal);
                var AmountOc = SqlPara("@AmountOc                  ", obj.AmountOc, SqlDbType.Decimal);
                var EmployeeID = SqlPara("@EmployeeID             ", obj.EmployeeID, SqlDbType.NVarChar);
                var EmployeeApprove = SqlPara("@EmployeeApprove             ", obj.EmployeeApprove, SqlDbType.NVarChar);
                var EmployeeName = SqlPara("@EmployeeName             ", obj.EmployeeName, SqlDbType.NVarChar);
                var StatusOrder = SqlPara("@StatusOrder             ", obj.StatusOrder, SqlDbType.NVarChar);
                var Method = SqlPara("@Method             ", obj.Method, SqlDbType.NVarChar);

                var Note = SqlPara("@Note             ", obj.Note, SqlDbType.NVarChar);
                var CreatedDate = SqlPara("@CreatedDate                 ", obj.CreatedDate, SqlDbType.DateTime);
                var ModifiedDate = SqlPara("@ModifiedDate                ", obj.ModifiedDate, SqlDbType.DateTime);
                var CreatedBy = SqlPara("@CreatedBy                   ", obj.CreatedBy, SqlDbType.NVarChar);
                var ModifiedBy = SqlPara("@ModifiedBy                  ", obj.ModifiedBy, SqlDbType.NVarChar);
                var _rs = _context.SQLCOMMANDS.FromSql(sql, RowGuid, OrganizationGuid, CodeID, DatePS, Title, Description, UnitID, UnitName, Quantity, UnitPrice, AmountOc, EmployeeID, EmployeeApprove, EmployeeName, StatusOrder, Method, Note, CreatedDate, ModifiedDate, CreatedBy, ModifiedBy).FirstOrDefault();
                if (_rs.Error == "")
                {
                    msg.Title = "Thêm mới thành công";
                }
                else
                {
                    return Json(new JMessage() { Error = true, Title = _rs.Error, Object = _rs.Error });
                }
            }
            catch (Exception ex)
            {
                msg.Error = true;
                msg.Object = ex;
                msg.Title = ex.Message;
            }
            return Json(msg);
        }

        [HttpPost]
        public JsonResult GetItem_AccountantsManagement(Guid Id)
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var _emp = GetEmployeeLogin(_context);
                var rs = _context.AccountantsManagement.Where(x => x.RowGuid == Id).FirstOrDefault();
                return Json(rs);
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = ex.Message });
            }
        }
        [HttpPost]
        public object Delete_AccountantsManagement(Guid Id)
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var _emp = GetEmployeeLogin(_context);
                var _ob = _context.Accountants_DOC.Where(x => x.RecordGuid == Id).ToList();
                foreach (var item in _ob)
                {
                    _context.Accountants_DOC.Remove(item);
                    _context.SaveChanges();
                }
                var _obj = _context.AccountantsManagement.Where(x => x.RowGuid == Id).FirstOrDefault();
                _context.AccountantsManagement.Remove(_obj);
                _context.SaveChanges();
                return Json(new JMessage() { Error = false, Title = "Xóa thành công" });
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult Submit_AccountantsDebt()
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var _emp = GetEmployeeLogin(_context);
                var files = Request.Form.Files.ToList();
                var submit = Request.Form["submit"];
                AccountantsDebt obj = JsonConvert.DeserializeObject<AccountantsDebt>(submit);
                string sql = "[Sale].[SP_ESSM_AccountantsDebt_Insert] @RowGuid,@OrganizationGuid,@CodeID,@DateVB,@Title,@UnitID,@UnitName,@Quantity,@StatusOrder,@EmployeeID,@EmployeeName,@Note,@CreatedDate,@ModifiedDate,@CreatedBy,@ModifiedBy";
                obj.CreatedDate = DateTime.Now;
                obj.ModifiedDate = DateTime.Now;
                obj.CreatedBy = GetCreatedBy(_context);
                obj.ModifiedBy = GetCreatedBy(_context);
                var RowGuid = SqlPara("@RowGuid                     ", obj.RowGuid, SqlDbType.UniqueIdentifier);
                var OrganizationGuid = SqlPara("@OrganizationGuid            ", obj.OrganizationGuid, SqlDbType.UniqueIdentifier);
                var CodeID = SqlPara("@CodeID                      ", obj.CodeID, SqlDbType.NVarChar);
                var DateVB = SqlPara("@DateVB                      ", obj.DateVB, SqlDbType.Date);
                var Title = SqlPara("@Title                      ", obj.Title, SqlDbType.NVarChar);

                var UnitID = SqlPara("@UnitID                       ", obj.UnitID, SqlDbType.NVarChar);
                var UnitName = SqlPara("@UnitName                  ", obj.UnitName, SqlDbType.NVarChar);
                var Quantity = SqlPara("@Quantity                ", obj.Quantity, SqlDbType.Decimal);
                var StatusOrder = SqlPara("@StatusOrder                  ", obj.StatusOrder, SqlDbType.NVarChar);
                var EmployeeID = SqlPara("@EmployeeID             ", obj.EmployeeID, SqlDbType.NVarChar);
                var EmployeeName = SqlPara("@EmployeeName             ", obj.EmployeeName, SqlDbType.NVarChar);
                var Note = SqlPara("@Note             ", obj.Note, SqlDbType.NVarChar);
                var CreatedDate = SqlPara("@CreatedDate                 ", obj.CreatedDate, SqlDbType.DateTime);
                var ModifiedDate = SqlPara("@ModifiedDate                ", obj.ModifiedDate, SqlDbType.DateTime);
                var CreatedBy = SqlPara("@CreatedBy                   ", obj.CreatedBy, SqlDbType.NVarChar);
                var ModifiedBy = SqlPara("@ModifiedBy                  ", obj.ModifiedBy, SqlDbType.NVarChar);
                var _rs = _context.SQLCOMMANDS.FromSql(sql, RowGuid, OrganizationGuid, CodeID, DateVB, Title, UnitID, UnitName, Quantity, StatusOrder, EmployeeID, EmployeeName, Note, CreatedDate, ModifiedDate, CreatedBy, ModifiedBy).FirstOrDefault();
                if (_rs.Error == "")
                {
                    msg.Title = "Thêm mới thành công";
                }
                else
                {
                    return Json(new JMessage() { Error = true, Title = _rs.Error, Object = _rs.Error });
                }
            }
            catch (Exception ex)
            {
                msg.Error = true;
                msg.Object = ex;
                msg.Title = ex.Message;
            }
            return Json(msg);
        }

        [HttpPost]
        public JsonResult GetItem_AccountantsDebt(Guid Id)
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var _emp = GetEmployeeLogin(_context);
                var rs = _context.AccountantsDebt.Where(x => x.RowGuid == Id).FirstOrDefault();
                return Json(rs);
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = ex.Message });
            }
        }
        [HttpPost]
        public object Delete_AccountantsDebt(Guid Id)
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var _emp = GetEmployeeLogin(_context);
                var _ob = _context.Accountants_DOC.Where(x => x.RecordGuid == Id).ToList();
                foreach (var item in _ob)
                {
                    _context.Accountants_DOC.Remove(item);
                    _context.SaveChanges();
                }
                var _obj = _context.AccountantsDebt.Where(x => x.RowGuid == Id).FirstOrDefault();
                _context.AccountantsDebt.Remove(_obj);
                _context.SaveChanges();
                return Json(new JMessage() { Error = false, Title = "Xóa thành công" });
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = ex.Message });
            }
        }

    }
}
