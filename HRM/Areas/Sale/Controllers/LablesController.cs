﻿using FTU.Utils.HelperNet;
using HRM.Controllers;
using HRM.Models;
using HRM.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;
using System.Data;
using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Newtonsoft.Json;
using Grpc.Core;
using DocumentFormat.OpenXml.Drawing;
using ES_MODEL.HR.Custom.Wf;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using DocumentFormat.OpenXml.Drawing.Charts;
using ES_MODEL.Task;
using HRM.Filters;
using ES_MODEL.HR;
using Aspose.Pdf;
using DocumentFormat.OpenXml.Bibliography;
using DocumentFormat.OpenXml.Wordprocessing;
using Microsoft.CodeAnalysis;
using DocumentFormat.OpenXml.VariantTypes;
using static Aspose.Pdf.Operator;
using System.Threading;
using HRM.Models.Custom.Hr;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Math;
using System.Web;

namespace HRM.Areas.Sale.Controllers
{
    [Authorize]
    public class LablesController : BaseController
    {
        private readonly HRMDBContext db;
        private readonly IHostingEnvironment environment;
        private readonly AppSettings _appSettings;
        public LablesController(HRMDBContext _db, IHostingEnvironment _environment, IOptions<AppSettings> appSettings)
        {
            db = _db;
            environment = _environment;
            _appSettings = appSettings.Value;
        }
        [Area("Sale")]
        [ActionAuthorize(EAction.ACCESS)]
        public IActionResult Index()
        {
            return View();
        }
        //GetById
        [HttpPost]
        public JsonResult GetById(int? Id)
        {

            var msg = new JMessage() { Error = false };
            try
            {
                var _emp = GetEmployeeLogin(db);
                string sql = "exec [Cooperation].[SP_ESTM_GetLables_ById] @LableID";
                var _LableID = new SqlParameter("@LableID", SqlDbType.Int); _LableID.Value = Id;
                var rs = db.Lables.FromSql(sql, _LableID).FirstOrDefault();
                return Json(rs);
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = ex.Message });
            }
        }
        public class DataJtable : JTableModel
        {
            public bool? IsActive { get; set; }
        }
        //Jtable
        [HttpPost]
        public object JTableLables([FromBody] DataJtable jTablePara)
        {
            try
            {
                var _emp =  GetEmployeeLogin(db);
                int intBeginFor = (jTablePara.CurrentPage - 1) * jTablePara.Length;
                string sql = "exec [Cooperation].[SP_ESTM_Lables_viewJTable]" +
                "@Keyword, " +
                 "@OrganizationGuid, " +
                "@Sort, " +
                "@Skip, " +
                "@Take," +
                "@output out ";
                var Keyword = new SqlParameter("@Keyword", SqlDbType.NVarChar, 255); Keyword.Value = jTablePara.search.value;
                var OrganizationGuid = new SqlParameter("@OrganizationGuid", SqlDbType.UniqueIdentifier); OrganizationGuid.Value = _emp.OrganizationGuid;
                var Sort = new SqlParameter("@Sort", SqlDbType.VarChar); Sort.Value = jTablePara.QueryOrderBy;
                var Skip = new SqlParameter("@Skip", SqlDbType.Int); Skip.Value = intBeginFor;
                var Take = new SqlParameter("@Take", SqlDbType.Int); Take.Value = jTablePara.Length;
                var output = new SqlParameter("@output", SqlDbType.Int) { Direction = System.Data.ParameterDirection.Output };
                var data = db.ViewJtableLable.FromSql(sql, Keyword, OrganizationGuid, Sort, Skip, Take, output)
                    .Select(a => new
                    {
                        a.LableId,
                        a.LableName,
                        a.Description,
                        a.OrderId,
                        a.Class
                    }).ToList();
                var jdata = JTableHelper.JObjectTable(data, jTablePara.Draw, (int)output.Value);
                return Json(jdata);
            }
            catch (Exception ex)
            {
                return Json(ex);
            }
        }
        //insert Store
        [HttpPost]
        public object Submit()
        {
            var _emp =  GetEmployeeLogin(db);
            var _obj = Request.Form["submit"];
            Lables obj = JsonConvert.DeserializeObject<Lables>(_obj);
            try
            {
                string sql = "exec [Cooperation].[SP_ESTM_Insert_Lables] " +
                    "@LableName," +
                    "@Description , " +
                    "@OrganizationGuid," +
                    "@OrderID," +
                    "@CreatedDate," +
                    "@ModifiedDate," +
                    "@CreatedBy," +
                    "@ModifiedBy," +
                     "@Class," +
                    "@ReturnCode OUT";
                var _LableName = new SqlParameter("@LableName", SqlDbType.NVarChar); CheckNullParameterStore(_LableName, obj.LableName);
                var _Description = new SqlParameter("@Description", SqlDbType.NVarChar); CheckNullParameterStore(_Description, obj.Description);
                var _OrganizationGuid = new SqlParameter("@OrganizationGuid", SqlDbType.UniqueIdentifier); CheckNullParameterStore(_OrganizationGuid, _emp.OrganizationGuid);
                var _OrderID = new SqlParameter("@OrderID", SqlDbType.Int); CheckNullParameterStore(_OrderID, obj.OrderId);
                var _CreatedDate = new SqlParameter("@CreatedDate", SqlDbType.DateTime); _CreatedDate.Value = DateTime.Now;
                var _ModifiedDate = new SqlParameter("@ModifiedDate", SqlDbType.DateTime); _ModifiedDate.Value = DateTime.Now;
                var _CreatedBy = new SqlParameter("@CreatedBy", SqlDbType.NVarChar); _CreatedBy.Value = _emp.LoginName + "#" + _emp.FullName;
                var _ModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.NVarChar); _ModifiedBy.Value = _emp.LoginName + "#" + _emp.FullName;
                var _Class = new SqlParameter("@Class", SqlDbType.NVarChar); CheckNullParameterStore(_Class, obj.Class);
                var _ReturnCode = new SqlParameter("@ReturnCode", SqlDbType.NVarChar, 1000) { Direction = System.Data.ParameterDirection.Output }; _ReturnCode.Value = "";
                var rs = db.Database.ExecuteSqlCommand(
                    sql,
                    _LableName,
                    _Description,
                    _OrganizationGuid,
                    _OrderID,
                    _CreatedDate,
                    _ModifiedDate,
                    _CreatedBy,
                    _ModifiedBy,
                    _Class,
                    _ReturnCode);
                if (_ReturnCode.Value.ToString() == "success")
                {
                    return new { Title = "Thêm mới thành công", Error = false };
                }
            }
            catch (Exception ex)
            {
                return new { Title = "Thêm mới không thành công", Error = true, ex.Message };
            }

            return null;
        }
        //Update Store
        [HttpPost]
        public object Edit()
        {
            try
            {
                var _emp =  GetEmployeeLogin(db);
                var _obj = Request.Form["edit"];
                Lables obj = JsonConvert.DeserializeObject<Lables>(_obj);
                string sql = "exec [Cooperation].[SP_ESTM_Update_Lables]" +
                    "@LableID," +
                    "@LableName," +
                    "@Description , " +
                    "@OrganizationGuid," +
                    "@OrderID," +
                    "@ModifiedDate," +
                    "@ModifiedBy," +
                    "@Class," +
                    "@ReturnCode OUT";
                var _LableID = new SqlParameter("@LableID", SqlDbType.Int); CheckNullParameterStore(_LableID, obj.LableId);
                var _LableName = new SqlParameter("@LableName", SqlDbType.NVarChar); CheckNullParameterStore(_LableName, obj.LableName);
                var _Description = new SqlParameter("@Description", SqlDbType.NVarChar); CheckNullParameterStore(_Description, obj.Description);
                var _OrganizationGuid = new SqlParameter("@OrganizationGuid", SqlDbType.UniqueIdentifier); CheckNullParameterStore(_OrganizationGuid, _emp.OrganizationGuid);
                var _OrderID = new SqlParameter("@OrderID", SqlDbType.Int); CheckNullParameterStore(_OrderID, obj.OrderId);
                var _ModifiedDate = new SqlParameter("@ModifiedDate", SqlDbType.DateTime); _ModifiedDate.Value = DateTime.Now;
                var _ModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.NVarChar); _ModifiedBy.Value = _emp.LoginName + "#" + _emp.FullName;
                var _Class = new SqlParameter("@Class", SqlDbType.NVarChar); CheckNullParameterStore(_Class, obj.Class);
                var _ReturnCode = new SqlParameter("@ReturnCode", SqlDbType.NVarChar, 1000) { Direction = System.Data.ParameterDirection.Output }; _ReturnCode.Value = "";
                var rs = db.Database.ExecuteSqlCommand(
                    sql,
                    _LableID,
                    _LableName,
                    _Description,
                    _OrganizationGuid,
                    _OrderID,
                    _ModifiedDate,
                    _ModifiedBy,
                    _Class,
                    _ReturnCode);
                if (_ReturnCode.Value.ToString() == "success")
                {
                    return new { Title = "Cập nhật thành công", Error = false };
                }
            }
            catch (Exception ex)
            {
                return new { Title = "Cập nhật không thành công", Error = true, ex.Message };
            }

            return null;
        }
        //Delete Store
        [HttpPost]
        public object Delete(int id)
        {
            try
            {
                var _LableID = new SqlParameter("@LableID", id);
                var _ReturnCode = new SqlParameter("@ReturnCode", SqlDbType.NVarChar, 1000) { Direction = System.Data.ParameterDirection.Output };
                string sql = "exec  [Cooperation].[SP_ESTM_Delete_Lables] @LableID, @ReturnCode OUT";
                var rs = db.Database.ExecuteSqlCommand(sql, _LableID, _ReturnCode);
                if (_ReturnCode.Value.ToString() == "success")
                {
                    return new { Title = " Xóa thành công", Error = false };
                }
                else
                {
                    return new { Title = "Bạn không thể xóa khi bản ghi đang được sử dụng", Error = true };
                };
            }
            catch (Exception ex)
            {
                return new { Title = "Xóa không thành công.", Error = true };
            }
        }
        [HttpPost]
        public object IsDeletedItems([FromBody] List<int?> listGuid)
        {
            try
            {
                var _emp = GetEmployeeLogin(db);
                string sql = "exec [Cooperation].[SP_ESTM_DeleteItems_Lables] @lstDeleteDetail";
                var _lstDeleteDetail = new SqlParameter("@lstDeleteDetail", SqlDbType.NVarChar, 1000); _lstDeleteDetail.Value = listGuid.Count >= 1 ? string.Join(",", listGuid) : "";
                var _tem = db.SQLCOMMANDS.FromSql(sql, _lstDeleteDetail).FirstOrDefault();

                if (_tem.Error == "")
                {
                    return new JMessage() { Error = false, Title = "Xóa thành công." };
                }
                else
                {
                    return new JMessage() { Error = true, Title = _tem.Error, Object = _tem };
                }
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Object = ex, Error = true, Title = "Có lỗi khi xóa." });
            }
        }
    }
}
