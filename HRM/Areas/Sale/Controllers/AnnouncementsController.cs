﻿using FTU.Utils.HelperNet;
using HRM.Controllers;
using HRM.Models;
using HRM.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;
using System.Data;
using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Newtonsoft.Json;
using Grpc.Core;
using DocumentFormat.OpenXml.Drawing;
using ES_MODEL.HR.Custom.Wf;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using DocumentFormat.OpenXml.Drawing.Charts;
using ES_MODEL.HR;
using HRM.Filters;

namespace HRM.Areas.Sale.Controllers
{
    [Authorize]
    public class AnnouncementsController : BaseController
    {
        public IConfigurationRoot Configuration { get; }
        private readonly HRMDBContext _context;
        private readonly IHostingEnvironment _environment;
        private readonly AppSettings _appSettings;

        public AnnouncementsController(IOptions<AppSettings> appSettings, HRMDBContext context, IHostingEnvironment environment)
        {
            _context = context;
            _environment = environment;
            _appSettings = appSettings.Value;
        }
        [Area("Sale")]
        [ActionAuthorize(EAction.ACCESS)]
        public IActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public object GetPicfile(Guid? id)
        {
            try
            {
                var Photo = _context.AnnouncementAttachmentComments.Where(x => x.AttachmentGuid == id).Select(x => x.Attachment).FirstOrDefault();
                byte[] ImageTemp = Photo;

                FileContentResult hd = null;
                hd = File(ImageTemp, "image/jpeg");
                int gh = ImageTemp.Count();
                if (gh > 0)
                {
                    return hd;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }
        public class DataJtable : JTableModel
        {
            public string IsPublic { get; set; }
            public string Status { get; set; }
            public string Ispriority { get; set; }
            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }
            public Guid? DepartmentGuid { get; set; }
        }
        [HttpPost]
        public object JTable([FromBody] DataJtable jTablePara)
        {
            try
            {
                {
                    var s = GetEmployeeLogin(_context);

                    if (jTablePara.StartDate != null)
                    {
                        jTablePara.StartDate = jTablePara.StartDate.Value.ToLocalTime();
                    }
                    else
                    {
                        jTablePara.StartDate = null;
                    }
                    if (jTablePara.EndDate != null)
                    {
                        jTablePara.EndDate = jTablePara.EndDate.Value.ToLocalTime();
                    }
                    else
                    {
                        jTablePara.EndDate = null;
                    }
                    int intBeginFor = (jTablePara.CurrentPage - 1) * jTablePara.Length;
                    string sql = "[dbo].[SP_ESVCM_Announcements_ListAllEMP]" +
                    "@Keyword, " +
                    "@IsPublic, " +
                    "@Status, " +
                    "@OrganizationGuid, " +
                    "@EmployeeGuid, " +
                    "@StartDate, " +
                    "@EndDate, " +
                    "@DepartmentGuid, " +
                    "@Ispriority, " +
                    "@LoginName, " +
                    "@Sort, " +
                    "@Skip, " +
                    "@Take," +
                    "@Count out ";
                    var _Keyword = new SqlParameter("@Keyword", SqlDbType.NVarChar); CheckNullParameterStore(_Keyword, jTablePara.search.value);
                    var _IsPublic = new SqlParameter("@IsPublic", SqlDbType.VarChar); CheckNullParameterStore(_IsPublic, jTablePara.IsPublic);
                    var _Status = new SqlParameter("@Status", SqlDbType.VarChar); CheckNullParameterStore(_Status, jTablePara.Status);
                    var _OrganizationGuid = new SqlParameter("@OrganizationGuid", SqlDbType.UniqueIdentifier); CheckNullParameterStore(_OrganizationGuid, s.OrganizationGuid);
                    var _EmployeeGuid = new SqlParameter("@EmployeeGuid", SqlDbType.UniqueIdentifier); CheckNullParameterStore(_EmployeeGuid, s.EmployeeGuid);
                    var StartDate = new SqlParameter("@StartDate", SqlDbType.Date); CheckNullParameterStore(StartDate, jTablePara.StartDate);
                    var EndDate = new SqlParameter("@EndDate", SqlDbType.Date); CheckNullParameterStore(EndDate, jTablePara.EndDate);
                    var DepartmentGuid = new SqlParameter("@DepartmentGuid", SqlDbType.UniqueIdentifier); CheckNullParameterStore(DepartmentGuid, jTablePara.DepartmentGuid);
                    var Ispriority = new SqlParameter("@Ispriority", SqlDbType.VarChar); CheckNullParameterStore(Ispriority, jTablePara.Ispriority);
                    var _LoginName = new SqlParameter("@LoginName", SqlDbType.VarChar); CheckNullParameterStore(_LoginName, s.LoginName);
                    var _Sort = new SqlParameter("@Sort", SqlDbType.VarChar); CheckNullParameterStore(_Sort, jTablePara.QueryOrderBy);
                    var _Skip = new SqlParameter("@Skip", SqlDbType.Int); CheckNullParameterStore(_Skip, intBeginFor);
                    var _Take = new SqlParameter("@Take", SqlDbType.Int); CheckNullParameterStore(_Take, jTablePara.Length);
                    var _output = new SqlParameter("@Count", SqlDbType.Int) { Direction = System.Data.ParameterDirection.Output };
                    var data = _context.AnnouncementsJtableView.FromSql(sql,
                        _Keyword,
                        _IsPublic,
                        _Status,
                        _OrganizationGuid,
                        _EmployeeGuid,
                        StartDate,
                        EndDate,
                        DepartmentGuid,
                        Ispriority,
                        _LoginName,
                        _Sort,
                        _Skip,
                        _Take,
                        _output).ToList();
                    var jdata = JTableHelper.JObjectTable(data, jTablePara.Draw, (int)_output.Value);
                    return Json(jdata);
                }

            }
            catch (Exception ex)
            {
                return Json(ex);
            }
        }
        [HttpGet]
        public object GetDepartmentAll()
        {
            var _emp = GetEmployeeLogin(_context);
            var a = _context.Departments.Where(x => x.OrganizationGuid == _emp.OrganizationGuid)
            .Select(x => new
            {
                id = x.DepartmentGuid,
                name = x.DepartmentName
            }).ToList();
            return Json(a);
        }
        [HttpGet]
        public object GetEmpAnnoucement()
        {
            var userlogin = GetEmployeeLogin(_context);
            var data = _context.Employees.Where(x => x.OrganizationGuid == userlogin.OrganizationGuid).Select(x => new { x.EmployeeGuid, x.FullName, x.EmployeeId }).ToList();

            return data;
        }
        [HttpPost]
        public JsonResult GetBylistEmp(Guid Id)
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var _emp = GetEmployeeLogin(_context);
                string sql = "exec [HR].[SP_ESHR_GetbyListEmpAnnoucementGuid] @AnnouncementGuid";
                var _AnnouncementGuid = new SqlParameter("@AnnouncementGuid", SqlDbType.UniqueIdentifier); _AnnouncementGuid.Value = Id;
                var rs = _context.ListEmpAnnoucementGetByid.FromSql(sql, _AnnouncementGuid).ToList();
                return Json(rs);
            }
            catch (Exception ex)
            {
                msg.Title = "Có lỗi khi lấy dữ liệu.";
                return Json(new JMessage() { Error = true, Object = ex, Title = msg.Title });
            }
        }

        [HttpPost]
        public object GetDepartment()
        {
            try
            {
                var s = GetEmployeeLogin(_context);
                var _rs = _context.Departments.Where(x => x.OrganizationGuid == s.OrganizationGuid).Select(x => new
                {
                    value = x.DepartmentGuid,
                    text = x.DepartmentId + "-" + x.DepartmentName
                }).ToList();
                return Json(_rs);
            }
            catch (Exception)
            {

                throw;
            }
        }
        [HttpPost]
        public object GetLogin()
        {
            var s = GetEmployeeLogin(_context);
            return s;
        }
        public class objTitleFile
        {
            public int? STT { get; set; }
            public string Title { get; set; }
        }
        [HttpPost]
        public object Submit()
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var s = GetEmployeeLogin(_context);
                var files = Request.Form.Files.ToList();
                var _obj = Request.Form["submit"];
                var lstTitlefile = Request.Form["lstTitlefile"];
                var _data = Request.Form["submitListEmp"];
                Announcements obj = JsonConvert.DeserializeObject<Announcements>(_obj);
                List<objTitleFile> lstTitle = JsonConvert.DeserializeObject<List<objTitleFile>>(lstTitlefile);
                List<ListEmpAnnouncement> listdata = JsonConvert.DeserializeObject<List<ListEmpAnnouncement>>(_data);
                if (obj.DueDate.HasValue) { obj.DueDate = obj.DueDate.Value.ToLocalTime(); } else { obj.DueDate = null; }
                if (obj.StartDate.HasValue) { obj.StartDate = obj.StartDate.Value.ToLocalTime(); } else { obj.StartDate = null; }
                if (obj.EndDate.HasValue) { obj.EndDate = obj.EndDate.Value.ToLocalTime(); } else { obj.EndDate = null; }



                var d = _context.Departments.Where(x => x.DepartmentGuid == obj.DepartmentGuid).SingleOrDefault();
                string sql = "[dbo].[SP_ESVCM_Announcements_Insert]" +
                    "@AnnouncementGuid, " +
                    "@OrganizationGuid, " +
                    "@DepartmentGuid, " +
                    "@DepartmentID, " +
                    "@DepartmentName, " +
                    "@EmployeeGuid, " +
                    "@Title, " +
                    "@Description, " +
                    "@Contents, " +
                    "@ExternalUrl, " +
                    "@IsPublic, " +
                    "@IsActive, " +
                    "@StartDate, " +
                    "@EndDate, " +
                     "@DueDate, " +
                    "@Status, " +
                    "@Ispriority, " +
                    "@CreatedDate, " +
                    "@CreatedBy, " +
                    "@ModifiedDate, " +
                    "@ModifiedBy,@ListEmpAnnouncement";
                var AnnouncementGuid = new SqlParameter("@AnnouncementGuid", System.Data.SqlDbType.UniqueIdentifier);
                var _AnnouncementGuid = obj.AnnouncementGuid = Guid.NewGuid();
                CheckNullParameterStore(AnnouncementGuid, _AnnouncementGuid);
                var _OrganizationGuid = new SqlParameter("@OrganizationGuid", System.Data.SqlDbType.UniqueIdentifier); CheckNullParameterStore(_OrganizationGuid, s.OrganizationGuid);
                var _DepartmentGuid = new SqlParameter("@DepartmentGuid", System.Data.SqlDbType.UniqueIdentifier); CheckNullParameterStore(_DepartmentGuid, obj.DepartmentGuid);
                var _DepartmentID = new SqlParameter("@DepartmentID", System.Data.SqlDbType.VarChar); CheckNullParameterStore(_DepartmentID, d.DepartmentId);
                var _DepartmentName = new SqlParameter("@DepartmentName", System.Data.SqlDbType.NVarChar); CheckNullParameterStore(_DepartmentName, d.DepartmentName);
                var _EmployeeGuid = new SqlParameter("@EmployeeGuid", System.Data.SqlDbType.UniqueIdentifier); CheckNullParameterStore(_EmployeeGuid, s.EmployeeGuid);
                var _Title = new SqlParameter("@Title", System.Data.SqlDbType.NVarChar); CheckNullParameterStore(_Title, obj.Title);
                var _Description = new SqlParameter("@Description", System.Data.SqlDbType.NVarChar); CheckNullParameterStore(_Description, obj.Description);
                var _Contents = new SqlParameter("@Contents", System.Data.SqlDbType.NVarChar); CheckNullParameterStore(_Contents, obj.Contents);
                var _ExternalUrl = new SqlParameter("@ExternalUrl", System.Data.SqlDbType.NVarChar); CheckNullParameterStore(_ExternalUrl, obj.ExternalUrl);
                var _IsPublic = new SqlParameter("@IsPublic", System.Data.SqlDbType.VarChar); CheckNullParameterStore(_IsPublic, obj.IsPublic);
                var _IsActive = new SqlParameter("@IsActive", System.Data.SqlDbType.VarChar); CheckNullParameterStore(_IsActive, obj.IsActive);
                var _StartDate = new SqlParameter("@StartDate", SqlDbType.DateTime); CheckNullParameterStore(_StartDate, obj.StartDate);
                var _EndDate = new SqlParameter("@EndDate", SqlDbType.DateTime); CheckNullParameterStore(_EndDate, obj.EndDate);
                var DueDate = new SqlParameter("@DueDate", SqlDbType.DateTime); CheckNullParameterStore(DueDate, obj.DueDate);

                var _Status = new SqlParameter("@Status", System.Data.SqlDbType.VarChar); CheckNullParameterStore(_Status, obj.Status);
                var _Ispriority = new SqlParameter("@Ispriority", System.Data.SqlDbType.VarChar); CheckNullParameterStore(_Ispriority, obj.Ispriority);
                var _CreatedDate = new SqlParameter("@CreatedDate", System.Data.SqlDbType.DateTime); _CreatedDate.Value = DateTime.Now;
                var _CreatedBy = new SqlParameter("@CreatedBy", System.Data.SqlDbType.NVarChar); _CreatedBy.Value = GetCreatedBy(_context);
                var _ModifiedDate = new SqlParameter("@ModifiedDate", System.Data.SqlDbType.DateTime); _ModifiedDate.Value = DateTime.Now;
                var _ModifiedBy = new SqlParameter("@ModifiedBy", System.Data.SqlDbType.NVarChar); _ModifiedBy.Value = GetModifiedBy(_context);
                var LstEmpAnnouncement = listdata.Select(x => x.EmployeeGuid).ToList();
                var ListEmpAnnouncement = new SqlParameter("@ListEmpAnnouncement", LstEmpAnnouncement.Count >= 1 ? string.Join(",", LstEmpAnnouncement) : "");

                var _rs = _context.SQLCOMMANDS.FromSql(sql, AnnouncementGuid, _OrganizationGuid, _DepartmentGuid, _DepartmentID, _DepartmentName, _EmployeeGuid, _Title, _Description, _Contents, _ExternalUrl, _IsPublic, _IsActive, _StartDate, _EndDate, DueDate, _Status, _Ispriority, _CreatedDate, _CreatedBy, _ModifiedDate, _ModifiedBy, ListEmpAnnouncement).FirstOrDefault();             

                if (_rs.Error == "")
                {
                    if (files.Count > 0)
                    {
                        string sqlAttach = "[dbo].[SP_Insert_Attachment] @Database, @Scheme,@TableName,@TableAttachment";
                        var Database = new SqlParameter("@Database", SqlDbType.NVarChar, 50); CheckNullParameterStore(Database, _appSettings.DBName_HR);
                        var Scheme = new SqlParameter("@Scheme", SqlDbType.NVarChar, 50); CheckNullParameterStore(Scheme, "Hr");
                        var TableName = new SqlParameter("@TableName ", SqlDbType.NVarChar, 50); CheckNullParameterStore(TableName, "AnnouncementAttachments");
                        var _detailAttach = new SqlParameter("@TableAttachment", SqlDbType.Structured);
                        /// table detail
                        var _tematach = new System.Data.DataTable();
                        _tematach.Columns.Add("AttachmentGuid");
                        _tematach.Columns.Add("ParentGuid");
                        _tematach.Columns.Add("RecordGuid");
                        _tematach.Columns.Add("OrganizationGuid");
                        _tematach.Columns.Add("DepartmentGuid");
                        _tematach.Columns.Add("ModuleID");
                        _tematach.Columns.Add("Title");
                        _tematach.Columns.Add("FileName");
                        _tematach.Columns.Add("Attachment", typeof(byte[]));
                        _tematach.Columns.Add("FileExtension");
                        _tematach.Columns.Add("FileSize");
                        _tematach.Columns.Add("UrlExternal");
                        _tematach.Columns.Add("IsPublic");
                        _tematach.Columns.Add("PrivateLevel");
                        _tematach.Columns.Add("IsFolder");
                        _tematach.Columns.Add("IsHidden");
                        _tematach.Columns.Add("IsReadonly");
                        _tematach.Columns.Add("IsSync");
                        _tematach.Columns.Add("SyncTime");
                        _tematach.Columns.Add("CloudPath");
                        _tematach.Columns.Add("CreatedDate", typeof(DateTime));
                        _tematach.Columns.Add("ModifiedDate", typeof(DateTime));
                        _tematach.Columns.Add("CreatedBy");
                        _tematach.Columns.Add("ModifiedBy");
                        for (var i = 0; i < files.Count; i++)
                        {
                            for (var j = 0; j < lstTitle.Count; j++)
                            {
                                if (i == lstTitle[j].STT)
                                {
                                    var stream = files[i].OpenReadStream();
                                    byte[] byteArr = Utilities.StreamToByteArray(stream);
                                    var name = files[i].FileName;
                                    Hrattachments _attach = new Hrattachments()
                                    {
                                        AttachmentGuid = Guid.NewGuid(),
                                        RecordGuid = obj.AnnouncementGuid,
                                        OrganizationGuid = s.OrganizationGuid,
                                        Title = lstTitle[j].Title,
                                        FileName = name,
                                        CreatedDate = DateTime.Now,
                                        CreatedBy = GetCreatedBy(_context),
                                        ModifiedDate = DateTime.Now,
                                        ModifiedBy = GetModifiedBy(_context),
                                        Attachment = byteArr,
                                        ModuleId = "HR",
                                        FileSize = byteArr.Length,
                                        FileExtension = System.IO.Path.GetExtension(files[i].FileName)
                                    };
                                    _tematach.Rows.Add(_attach.AttachmentGuid,
                                        _attach.ParentGuid,
                                        _attach.RecordGuid,
                                        _attach.OrganizationGuid,
                                        _attach.DepartmentGuid,
                                        _attach.ModuleId,
                                        _attach.Title,
                                        _attach.FileName,
                                        _attach.Attachment,
                                        _attach.FileExtension,
                                        _attach.FileSize,
                                        _attach.UrlExternal,
                                        _attach.IsPublic,
                                        _attach.PrivateLevel,
                                        _attach.IsFolder,
                                        _attach.IsHidden,
                                        _attach.IsReadonly,
                                        _attach.IsSync,
                                       DBNull.Value,
                                        _attach.CloudPath,
                                        _attach.CreatedDate,
                                        _attach.ModifiedDate,
                                        _attach.CreatedBy,
                                        _attach.ModifiedBy
                                    );
                                }
                            }
                        }
                        _detailAttach.Value = _tematach;
                        _detailAttach.TypeName = "[dbo].[Attachment]";
                        var __temattach = _context.SQLCOMMANDS.FromSql(sqlAttach, Database, Scheme, TableName, _detailAttach).FirstOrDefault();
                        if (__temattach.Error != "")
                        {
                            if (msg.Title == null)
                            {
                                msg.Title = "đính kèm";
                            }
                            else
                            {
                                msg.Title = msg.Title + ",đính kèm";
                            }
                        }

                    }
                    if (msg.Title != null)
                    {
                        msg.Title = "Thêm mới thành công. Nhưng có lỗi trong quá trình đính kèm.";
                    }
                    else
                    {
                        msg.Title = "Thêm mới thành công.";                      
                    }
                }
                else
                {
                    return new { Title = _rs.Error, Error = true, Object = _rs.Error };
                }
            }
            catch (Exception ex)

            {
                return new { Object = ex, Title = "Thêm mới không thành công", Error = true };
            }
            return Json(msg);
        }
        [HttpPost]
        public object ListEmpAnnouncement_Update([FromBody] List<string> listdata)
        {
            try
            {
                var _emp = GetEmployeeLogin(_context);
                string sql = "[Hr].[SP_ESHR_ListEmpAnnouncement_Update] @lstAnnouncementGuid,@EmployeeGuid,@Error ";
                var lstAnnouncementGuid = new SqlParameter("@lstAnnouncementGuid", listdata.Count >= 1 ? string.Join(",", listdata) : "");
                var EmployeeGuid = SqlPara("@EmployeeGuid", _emp.EmployeeGuid, SqlDbType.UniqueIdentifier);
                var _Error = new SqlParameter("@Error", SqlDbType.NVarChar); _Error.Value = "";
                var __tem = _context.SQLCOMMANDS.FromSql(sql, lstAnnouncementGuid, EmployeeGuid, _Error).FirstOrDefault();

                if (__tem.Error == "")
                {
                    return Json(new JMessage() { Error = false, Title = "Cập nhật thành công." });
                }
                else
                {
                    return Json(new JMessage() { Error = true, Title = __tem.Error });
                }
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = ex.Message });
            }
        }
        [HttpPost]
        public JsonResult GetAttachments([FromBody] Hrattachments obj)
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var username = GetEmployeeLogin(_context);
                string sql = "[dbo].[SP_GET_Attachment] @Database, @Scheme, @TableName, @OrganizationGuid, @VoucherGuid, @Error";
                var _Database = new SqlParameter("@Database", SqlDbType.NVarChar); _Database.Value = _appSettings.DBName_HR;
                var _Scheme = new SqlParameter("@Scheme", SqlDbType.NVarChar); _Scheme.Value = "HR";
                var _TableName = new SqlParameter("@TableName", SqlDbType.NVarChar); _TableName.Value = "AnnouncementAttachments";
                var _OrganizationGuid = new SqlParameter("@OrganizationGuid", SqlDbType.UniqueIdentifier); _OrganizationGuid.Value = username.OrganizationGuid;
                var _VoucherGuid = new SqlParameter("@VoucherGuid", SqlDbType.NVarChar); CheckNullParameterStore(_VoucherGuid, obj.RecordGuid.ToString());
                var _Error = new SqlParameter("@Error", SqlDbType.NVarChar); _Error.Value = "";
                var rs = _context.OpenAttachmentsView.FromSql(sql, _Database, _Scheme, _TableName, _OrganizationGuid, _VoucherGuid, _Error).ToList();
                return Json(rs);
            }
            catch (Exception ex)
            {
                msg.Title = "Có lỗi khi lấy dữ liệu.";
                return Json(new JMessage() { Error = true, Title = msg.Title });
            }
        }

        public FileResult Download(Guid Id)
        {

            var msg = new JMessage() { Error = false };
            try
            {
                var username = GetEmployeeLogin(_context);
                string sql = "[dbo].[SP_Download_Attachment] @database, @Scheme, @TableName, @OrganizationGuid, @AttachmentGuid, @Error";
                var database = new SqlParameter("@database ", SqlDbType.NVarChar, 250); CheckNullParameterStore(database, _appSettings.DBName_HR);
                var Scheme = new SqlParameter("@Scheme", SqlDbType.VarChar); Scheme.Value = "HR";
                var TableName = new SqlParameter("@TableName", SqlDbType.VarChar); TableName.Value = "AnnouncementAttachments";
                var OrganizationGuid = new SqlParameter("@OrganizationGuid", SqlDbType.UniqueIdentifier); OrganizationGuid.Value = username.OrganizationGuid;
                var AttachmentGuid = new SqlParameter("@AttachmentGuid", SqlDbType.UniqueIdentifier); CheckNullParameterStore(AttachmentGuid, Id);
                var Error = new SqlParameter("@Error", SqlDbType.NVarChar); Error.Value = "";
                var rs = _context.AttachmentsDownloadView.FromSql(sql, database, Scheme, TableName, OrganizationGuid, AttachmentGuid, Error).FirstOrDefault();
                return File(rs.Attachment, System.Net.Mime.MediaTypeNames.Application.Octet, rs.FileName);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        [HttpPost]
        public JsonResult GetById(Guid? id)
        {

            var msg = new JMessage() { Error = false };
            try
            {
                var _emp = GetEmployeeLogin(_context);
                string sql = "[dbo].[SP_ESVCM_DocumentBooks_ViewItem] @AnnouncementGuid";
                var _AnnouncementGuid = new SqlParameter("@AnnouncementGuid ", SqlDbType.UniqueIdentifier); CheckNullParameterStore(_AnnouncementGuid, id);
                var rs = _context.Announcements.FromSql(sql, _AnnouncementGuid).SingleOrDefault();
                return Json(rs);
            }
            catch (Exception ex)
            {
                msg.Title = "Có lỗi khi lấy dữ liệu.";
                return Json(new JMessage() { Error = true, Object = ex, Title = msg.Title });
            }
        }
        [HttpPost]
        public object Edit()
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var s = GetEmployeeLogin(_context);
                var files = Request.Form.Files.ToList();
                var _obj = Request.Form["edit"];
                var lstTitlefile = Request.Form["lstTitlefile"];
                var DeleteAttach = Request.Form["DeleteAttach"];

                Announcements obj = JsonConvert.DeserializeObject<Announcements>(_obj);
                var _data = Request.Form["submitListEmp"];
                List<objTitleFile> lstTitle = JsonConvert.DeserializeObject<List<objTitleFile>>(lstTitlefile);
                List<ListEmpAnnouncement> listdata = JsonConvert.DeserializeObject<List<ListEmpAnnouncement>>(_data);
                List<Guid> deleteFileattach = JsonConvert.DeserializeObject<List<Guid>>(DeleteAttach);
                var d = _context.Departments.Where(x => x.DepartmentGuid == obj.DepartmentGuid).SingleOrDefault();
                if (obj.DueDate.HasValue) { obj.DueDate = obj.DueDate.Value.ToLocalTime(); } else { obj.DueDate = null; }
                if (obj.StartDate.HasValue) { obj.StartDate = obj.StartDate.Value.ToLocalTime(); } else { obj.StartDate = null; }
                if (obj.EndDate.HasValue) { obj.EndDate = obj.EndDate.Value.ToLocalTime(); } else { obj.EndDate = null; }
                string sql = "[dbo].[SP_ESVCM_Announcements_Update]" +
                    "@AnnouncementGuid, " +
                    "@OrganizationGuid, " +
                    "@DepartmentGuid, " +
                    "@DepartmentID, " +
                    "@DepartmentName, " +
                    "@EmployeeGuid, " +
                    "@Title, " +
                    "@Description, " +
                    "@Contents, " +
                    "@ExternalUrl, " +
                    "@IsPublic, " +
                    "@IsActive, " +
                    "@StartDate, " +
                    "@EndDate, " +
                      "@DueDate, " +
                    "@Status, " +
                     "@Ispriority, " +
                    "@ModifiedDate, " +
                    "@ModifiedBy,@ListEmpAnnouncement";
                var _AnnouncementGuid = new SqlParameter("@AnnouncementGuid", System.Data.SqlDbType.UniqueIdentifier); CheckNullParameterStore(_AnnouncementGuid, obj.AnnouncementGuid);
                var _OrganizationGuid = new SqlParameter("@OrganizationGuid", System.Data.SqlDbType.UniqueIdentifier); CheckNullParameterStore(_OrganizationGuid, s.OrganizationGuid);
                var _DepartmentGuid = new SqlParameter("@DepartmentGuid", System.Data.SqlDbType.UniqueIdentifier); CheckNullParameterStore(_DepartmentGuid, obj.DepartmentGuid);
                var _DepartmentID = new SqlParameter("@DepartmentID", System.Data.SqlDbType.VarChar); CheckNullParameterStore(_DepartmentID, d.DepartmentId);
                var _DepartmentName = new SqlParameter("@DepartmentName", System.Data.SqlDbType.NVarChar); CheckNullParameterStore(_DepartmentName, d.DepartmentName);
                var _EmployeeGuid = new SqlParameter("@EmployeeGuid", System.Data.SqlDbType.UniqueIdentifier); CheckNullParameterStore(_EmployeeGuid, s.EmployeeGuid);
                var _Title = new SqlParameter("@Title", System.Data.SqlDbType.NVarChar); CheckNullParameterStore(_Title, obj.Title);
                var _Description = new SqlParameter("@Description", System.Data.SqlDbType.NVarChar); CheckNullParameterStore(_Description, obj.Description);
                var _Contents = new SqlParameter("@Contents", System.Data.SqlDbType.NVarChar); CheckNullParameterStore(_Contents, obj.Contents);
                var _ExternalUrl = new SqlParameter("@ExternalUrl", System.Data.SqlDbType.NVarChar); CheckNullParameterStore(_ExternalUrl, obj.ExternalUrl);
                var _IsPublic = new SqlParameter("@IsPublic", System.Data.SqlDbType.VarChar); CheckNullParameterStore(_IsPublic, obj.IsPublic);
                var _IsActive = new SqlParameter("@IsActive", System.Data.SqlDbType.VarChar); CheckNullParameterStore(_IsActive, obj.IsActive);
                var _StartDate = new SqlParameter("@StartDate", SqlDbType.DateTime); CheckNullParameterStore(_StartDate, obj.StartDate);
                var _EndDate = new SqlParameter("@EndDate", SqlDbType.DateTime); CheckNullParameterStore(_EndDate, obj.EndDate);
                var DueDate = new SqlParameter("@DueDate", SqlDbType.DateTime); CheckNullParameterStore(DueDate, obj.DueDate);

                var _Status = new SqlParameter("@Status", System.Data.SqlDbType.VarChar); CheckNullParameterStore(_Status, obj.Status);
                var _Ispriority = new SqlParameter("@Ispriority", System.Data.SqlDbType.VarChar); CheckNullParameterStore(_Ispriority, obj.Ispriority);
                var _ModifiedDate = new SqlParameter("@ModifiedDate", System.Data.SqlDbType.DateTime); _ModifiedDate.Value = DateTime.Now;
                var _ModifiedBy = new SqlParameter("@ModifiedBy", System.Data.SqlDbType.NVarChar); _ModifiedBy.Value = GetModifiedBy(_context);
                var LstEmpAnnouncement = listdata.Select(x => x.EmployeeGuid).ToList();
                var ListEmpAnnouncement = new SqlParameter("@ListEmpAnnouncement", LstEmpAnnouncement.Count >= 1 ? string.Join(",", LstEmpAnnouncement) : "");

                var _rs = _context.SQLCOMMANDS.FromSql(sql, _AnnouncementGuid, _OrganizationGuid, _DepartmentGuid, _DepartmentID, _DepartmentName, _EmployeeGuid, _Title, _Description, _Contents, _ExternalUrl, _IsPublic, _IsActive, _StartDate, _EndDate, DueDate, _Status, _Ispriority, _ModifiedDate, _ModifiedBy, ListEmpAnnouncement).FirstOrDefault();

                var list = _context.ListEmpAnnouncement.Where(x => x.AnnouncementGuid == obj.AnnouncementGuid).ToList();
                _context.RemoveRange(list);
               
                if (deleteFileattach.Count > 0)
                {
                    foreach (var _iF in deleteFileattach)
                    {
                        string d_sql = "[dbo].[SP_DELETE_Attachment] @Database, @scheme, @TableName, @OrganizationGuid, @RowGuid, @Error";
                        var d_Database = new SqlParameter("@Database", SqlDbType.NVarChar, 50); CheckNullParameterStore(d_Database, _appSettings.DBName_HR);
                        var d_Scheme = new SqlParameter("@scheme", SqlDbType.VarChar); d_Scheme.Value = "Hr";
                        var d_TableName = new SqlParameter("@TableName", SqlDbType.VarChar); d_TableName.Value = "AnnouncementAttachments";
                        var d_OrganizationGuid = new SqlParameter("@OrganizationGuid", SqlDbType.UniqueIdentifier); d_OrganizationGuid.Value = s.OrganizationGuid;
                        var d_AttachmentGuid = new SqlParameter("@RowGuid", SqlDbType.NVarChar, 50); d_AttachmentGuid.Value = _iF.ToString();
                        var d_Error = new SqlParameter("@Error", SqlDbType.NVarChar); d_Error.Value = "";
                        var d_rs = _context.SQLCOMMANDS.FromSql(d_sql, d_Database, d_Scheme, d_TableName, d_OrganizationGuid, d_AttachmentGuid, d_Error).SingleOrDefault();
                    }
                }
                if (files.Count > 0)
                {
                    string sqlAttach = "[dbo].[SP_Insert_Attachment] @Database, @Scheme,@TableName,@TableAttachment";
                    var Database = new SqlParameter("@Database", SqlDbType.NVarChar, 50); CheckNullParameterStore(Database, _appSettings.DBName_HR);
                    var Scheme = new SqlParameter("@Scheme", SqlDbType.NVarChar, 50); CheckNullParameterStore(Scheme, "Hr");
                    var TableName = new SqlParameter("@TableName ", SqlDbType.NVarChar, 50); CheckNullParameterStore(TableName, "AnnouncementAttachments");
                    var _detailAttach = new SqlParameter("@TableAttachment", SqlDbType.Structured);
                    /// table detail
                    var _tematach = new System.Data.DataTable();
                    _tematach.Columns.Add("AttachmentGuid");
                    _tematach.Columns.Add("ParentGuid");
                    _tematach.Columns.Add("RecordGuid");
                    _tematach.Columns.Add("OrganizationGuid");
                    _tematach.Columns.Add("DepartmentGuid");
                    _tematach.Columns.Add("ModuleID");
                    _tematach.Columns.Add("Title");
                    _tematach.Columns.Add("FileName");
                    _tematach.Columns.Add("Attachment", typeof(byte[]));
                    _tematach.Columns.Add("FileExtension");
                    _tematach.Columns.Add("FileSize");
                    _tematach.Columns.Add("UrlExternal");
                    _tematach.Columns.Add("IsPublic");
                    _tematach.Columns.Add("PrivateLevel");
                    _tematach.Columns.Add("IsFolder");
                    _tematach.Columns.Add("IsHidden");
                    _tematach.Columns.Add("IsReadonly");
                    _tematach.Columns.Add("IsSync");
                    _tematach.Columns.Add("SyncTime");
                    _tematach.Columns.Add("CloudPath");
                    _tematach.Columns.Add("CreatedDate", typeof(DateTime));
                    _tematach.Columns.Add("ModifiedDate", typeof(DateTime));
                    _tematach.Columns.Add("CreatedBy");
                    _tematach.Columns.Add("ModifiedBy");
                    for (var i = 0; i < files.Count; i++)
                    {
                        for (var j = 0; j < lstTitle.Count; j++)
                        {
                            if (i == lstTitle[j].STT)
                            {
                                var stream = files[i].OpenReadStream();
                                byte[] byteArr = Utilities.StreamToByteArray(stream);
                                var name = files[i].FileName;
                                Hrattachments _attach = new Hrattachments()
                                {
                                    AttachmentGuid = Guid.NewGuid(),
                                    RecordGuid = obj.AnnouncementGuid,
                                    OrganizationGuid = s.OrganizationGuid,
                                    Title = lstTitle[j].Title,
                                    FileName = name,
                                    CreatedDate = DateTime.Now,
                                    CreatedBy = GetCreatedBy(_context),
                                    ModifiedDate = DateTime.Now,
                                    ModifiedBy = GetModifiedBy(_context),
                                    Attachment = byteArr,
                                    ModuleId = "HR",
                                    FileSize = byteArr.Length,
                                    FileExtension = System.IO.Path.GetExtension(files[i].FileName)
                                };
                                _tematach.Rows.Add(_attach.AttachmentGuid,
                                    _attach.ParentGuid,
                                    _attach.RecordGuid,
                                    _attach.OrganizationGuid,
                                    _attach.DepartmentGuid,
                                    _attach.ModuleId,
                                    _attach.Title,
                                    _attach.FileName,
                                    _attach.Attachment,
                                    _attach.FileExtension,
                                    _attach.FileSize,
                                    _attach.UrlExternal,
                                    _attach.IsPublic,
                                    _attach.PrivateLevel,
                                    _attach.IsFolder,
                                    _attach.IsHidden,
                                    _attach.IsReadonly,
                                    _attach.IsSync,
                                   DBNull.Value,
                                    _attach.CloudPath,
                                    _attach.CreatedDate,
                                    _attach.ModifiedDate,
                                    _attach.CreatedBy,
                                    _attach.ModifiedBy
                                );
                            }
                        }
                    }
                    _detailAttach.Value = _tematach;
                    _detailAttach.TypeName = "[dbo].[Attachment]";
                    var __temattach = _context.SQLCOMMANDS.FromSql(sqlAttach, Database, Scheme, TableName, _detailAttach).FirstOrDefault();
                    if (__temattach.Error == "")
                    {
                        return new { Title = "Cập nhật thành công", Error = false };
                    }
                    else
                    {
                        return new { Title = "Cập nhật có file không thành công ", Error = true, Object = __temattach.Error };
                    }
                }
                else
                {
                    if (_rs.Error == "")
                    {
                        return new { Title = "Cập nhật thành công", Error = false };
                    }
                    else
                    {
                        return new { Title = _rs.Error, Error = true };
                    }

                }
            }
            catch (Exception ex)

            {
                return new { Object = ex, Title = "Cập nhật không thành công", Error = true };
            }

        }
        [HttpPost]
        public object Remove(Guid? id)
        {
            try
            {
                var _emp = GetEmployeeLogin(_context);
                string sql = "[dbo].[SP_ESVCM_Announcements_Delete] @Database,@DBNameDocument,@Scheme,@TableName,@TableNameAttach,@TableNameComment,@TableNameCommentAttach,@OrganizationGuid,@RowGuid, @Error";
                var Database = new SqlParameter("@Database", SqlDbType.NVarChar); CheckNullParameterStore(Database, _appSettings.DBName_HR);
                var DBNameDocument = new SqlParameter("@DBNameDocument", SqlDbType.NVarChar);
                CheckNullParameterStore(DBNameDocument, _appSettings.DBName_HR);
                var Scheme = new SqlParameter("@Scheme", SqlDbType.NVarChar); CheckNullParameterStore(Scheme, "Hr");
                var TableName = new SqlParameter("@TableName", SqlDbType.NVarChar);
                CheckNullParameterStore(TableName, "Announcements");
                var TableNameAttach = new SqlParameter("@TableNameAttach", SqlDbType.NVarChar);
                CheckNullParameterStore(TableNameAttach, "AnnouncementAttachments");
                var TableNameComment = new SqlParameter("@TableNameComment", SqlDbType.NVarChar);
                CheckNullParameterStore(TableNameComment, "AnnouncementsComments");
                var TableNameCommentAttach = new SqlParameter("@TableNameCommentAttach", SqlDbType.NVarChar);
                CheckNullParameterStore(TableNameCommentAttach, "AnnouncementAttachmentComments");
                var OrganizationGuid = new SqlParameter("@OrganizationGuid", SqlDbType.UniqueIdentifier);
                CheckNullParameterStore(OrganizationGuid, _emp.OrganizationGuid);
                var RowGuid = new SqlParameter("@RowGuid ", SqlDbType.UniqueIdentifier); CheckNullParameterStore(RowGuid, id);
                var Error = new SqlParameter("@Error ", SqlDbType.NVarChar); CheckNullParameterStore(Error, "");
                var _rs = _context.SQLCOMMANDS.FromSql(sql, Database, DBNameDocument, Scheme, TableName, TableNameAttach, TableNameComment, TableNameCommentAttach, OrganizationGuid, RowGuid, Error).FirstOrDefault();
                if (_rs.Error == "")
                {
                    return new JMessage() { Error = false, Title = "Xóa thành công." };
                }
                else
                {
                    return new { Title = "Đã có bảng khác sử dụng dữ liệu bảng này", Error = true, Object = _rs.Error };
                };
            }
            catch (Exception ex)
            {
                return new { Object = ex, Title = "Xóa không thành công.", Error = true };
            }
        }
        [HttpPost]
        public object RemoveItems([FromBody] List<Guid?> Items)
        {
            var msg = new JMessage() { Error = false };
            try
            {
                string sql = "[HR].[SP_ESVCM_Announcements_DeleteItem] @lstDeleteDetail";
                var _lstDeleteDetail = new SqlParameter("@lstDeleteDetail", SqlDbType.NVarChar); _lstDeleteDetail.Value = Items.Count >= 1 ? string.Join(",", Items) : "";
                var __tem = _context.SQLCOMMANDS.FromSql(sql, _lstDeleteDetail).FirstOrDefault();
                if (__tem.Error == "")
                {
                    return new JMessage() { Error = false, Title = "Xóa thành công." };
                }
                else
                {
                    return new JMessage() { Error = true, Title = __tem.Error, Object = __tem };
                }
            }
            catch (Exception ex)
            {
                msg.Title = "Có lỗi khi xóa dữ liệu.";
                return Json(new JMessage() { Error = true, Object = ex, Title = msg.Title });
            }
        }
        [HttpPost]
        public JMessage InsertComments()
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var userlogin = GetEmployeeLogin(_context);
                var files = Request.Form.Files.ToList();
                var _obj = Request.Form["insert"];
                AnnouncementsComments obj = JsonConvert.DeserializeObject<AnnouncementsComments>(_obj);
                obj.CommentGuid = Guid.NewGuid();
                obj.OrganizationGuid = userlogin.OrganizationGuid;
                obj.EmployeeGuid = userlogin.EmployeeGuid;
                obj.EmployeeId = userlogin.EmployeeId;
                obj.EmployeeName = userlogin.FullName;
                obj.DepartmentGuid = userlogin.DepartmentGuid;
                obj.DepartmentId = userlogin.DepartmentId;
                obj.DepartmentName = userlogin.DepartmentName;
                obj.AvatarUrl = "";
                obj.CreatedDate = DateTime.Now;
                obj.CreatedBy = GetCreatedBy(_context);
                obj.ModifiedDate = DateTime.Now;
                obj.ModifiedBy = GetModifiedBy(_context);
                string sql = "[Hr].[SP_Insert_Comments_Hr] @Scheme, @TableName, @CommentGuid,@ParentGuid,@RecordGuid,@OrganizationGuid,@Comment,@EmployeeGuid,@EmployeeID,@EmployeeName,@LoginName,@DepartmentGuid,@DepartmentID,@DepartmentName,@AvatarUrl,@CreatedDate,@CreatedBy,@ModifiedDate,@ModifiedBy,@Error";
                var _Scheme = new SqlParameter("@Scheme", SqlDbType.VarChar);
                CheckNullParameterStore(_Scheme, "Hr");
                var _TableName = new SqlParameter("@TableName", SqlDbType.NVarChar);
                CheckNullParameterStore(_TableName, "AnnouncementsComments");
                var _CommentGuid = new SqlParameter("@CommentGuid", SqlDbType.UniqueIdentifier);
                CheckNullParameterStore(_CommentGuid, obj.CommentGuid);
                var _ParentGuid = new SqlParameter("@ParentGuid", SqlDbType.UniqueIdentifier);
                CheckNullParameterStore(_ParentGuid, obj.ParentGuid);
                var _RecordGuid = new SqlParameter("@RecordGuid", SqlDbType.UniqueIdentifier);
                CheckNullParameterStore(_RecordGuid, obj.RecordGuid);
                var _OrganizationGuid = new SqlParameter("@OrganizationGuid", SqlDbType.UniqueIdentifier);
                CheckNullParameterStore(_OrganizationGuid, obj.OrganizationGuid);
                var _Comment = new SqlParameter("@Comment", SqlDbType.NChar);
                CheckNullParameterStore(_Comment, obj.Comment);
                var _EmployeeGuid = new SqlParameter("@EmployeeGuid", SqlDbType.UniqueIdentifier);
                CheckNullParameterStore(_EmployeeGuid, obj.EmployeeGuid);
                var _EmployeeID = new SqlParameter("@EmployeeID", SqlDbType.VarChar, 20);
                CheckNullParameterStore(_EmployeeID, obj.EmployeeId);
                var _EmployeeName = new SqlParameter("@EmployeeName", SqlDbType.NVarChar, 50);
                CheckNullParameterStore(_EmployeeName, obj.EmployeeName);
                var _LoginName = new SqlParameter("@LoginName", SqlDbType.NVarChar, 50);
                CheckNullParameterStore(_LoginName, userlogin.LoginName);
                var _DepartmentGuid = new SqlParameter("@DepartmentGuid", SqlDbType.UniqueIdentifier);
                CheckNullParameterStore(_DepartmentGuid, obj.DepartmentGuid);
                var _DepartmentID = new SqlParameter("@DepartmentID", SqlDbType.VarChar, 20);
                CheckNullParameterStore(_DepartmentID, obj.DepartmentId);
                var _DepartmentName = new SqlParameter("@DepartmentName", SqlDbType.NVarChar, 50);
                CheckNullParameterStore(_DepartmentName, obj.DepartmentName);
                var _AvatarUrl = new SqlParameter("@AvatarUrl", SqlDbType.NVarChar, 255);
                CheckNullParameterStore(_AvatarUrl, obj.AvatarUrl);
                var _CreatedDate = new SqlParameter("@CreatedDate", SqlDbType.DateTime);
                CheckNullParameterStore(_CreatedDate, obj.CreatedDate);
                var _CreatedBy = new SqlParameter("@CreatedBy", SqlDbType.NVarChar, 250);
                CheckNullParameterStore(_CreatedBy, obj.CreatedBy);
                var _ModifiedDate = new SqlParameter("@ModifiedDate", SqlDbType.DateTime);
                CheckNullParameterStore(_ModifiedDate, obj.ModifiedDate);
                var _ModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.NVarChar, 250);
                CheckNullParameterStore(_ModifiedBy, obj.ModifiedBy);
                var _Error = new SqlParameter("@Error", SqlDbType.NVarChar); _Error.Value = "";
                var rs = _context.SQLCOMMANDS.FromSql(sql, _Scheme, _TableName, _CommentGuid, _ParentGuid, _RecordGuid, _OrganizationGuid, _Comment, _EmployeeGuid, _EmployeeID, _EmployeeName, _LoginName, _DepartmentGuid, _DepartmentID, _DepartmentName, _AvatarUrl, _CreatedDate, _CreatedBy, _ModifiedDate, _ModifiedBy, _Error).FirstOrDefault();
                if (files.Count > 0)
                {

                    string sqlAttach = "[dbo].[SP_Insert_Attachment] @Database, @Scheme,@TableName,@TableAttachment";
                    var Database = new SqlParameter("@Database", SqlDbType.NVarChar, 50); CheckNullParameterStore(Database, _appSettings.DBName_HR);
                    var scheme = new SqlParameter("@scheme ", SqlDbType.NVarChar, 250); CheckNullParameterStore(scheme, "Hr");
                    var TableName = new SqlParameter("@TableName ", SqlDbType.NVarChar, 50); CheckNullParameterStore(TableName, "AnnouncementAttachmentComments");
                    var _detailAttach = new SqlParameter("@TableAttachment", SqlDbType.Structured);

                    /// table detail
                    var _tematach = new System.Data.DataTable();
                    _tematach.Columns.Add("AttachmentGuid");
                    _tematach.Columns.Add("ParentGuid");
                    _tematach.Columns.Add("RecordGuid");
                    _tematach.Columns.Add("OrganizationGuid");
                    _tematach.Columns.Add("DepartmentGuid");
                    _tematach.Columns.Add("ModuleID");
                    _tematach.Columns.Add("Title");
                    _tematach.Columns.Add("FileName");
                    _tematach.Columns.Add("Attachment", typeof(byte[]));
                    _tematach.Columns.Add("FileExtension");
                    _tematach.Columns.Add("FileSize");
                    _tematach.Columns.Add("UrlExternal");
                    _tematach.Columns.Add("IsPublic");
                    _tematach.Columns.Add("PrivateLevel");
                    _tematach.Columns.Add("IsFolder");
                    _tematach.Columns.Add("IsHidden");
                    _tematach.Columns.Add("IsReadonly");
                    _tematach.Columns.Add("IsSync");
                    _tematach.Columns.Add("SyncTime");
                    _tematach.Columns.Add("CloudPath");

                    _tematach.Columns.Add("CreatedDate", typeof(DateTime));
                    _tematach.Columns.Add("ModifiedDate", typeof(DateTime));
                    _tematach.Columns.Add("CreatedBy");
                    _tematach.Columns.Add("ModifiedBy");

                    foreach (var file in files)
                    {
                        var stream = file.OpenReadStream();
                        byte[] byteArr = Utilities.StreamToByteArray(stream);
                        var name = file.FileName;
                        Hrattachments _attach = new Hrattachments()
                        {
                            AttachmentGuid = Guid.NewGuid(),

                            RecordGuid = obj.CommentGuid,
                            OrganizationGuid = userlogin.OrganizationGuid,
                            Title = name,
                            FileName = name,
                            CreatedDate = DateTime.Now,
                            CreatedBy = GetCreatedBy(_context),
                            ModifiedDate = DateTime.Now,
                            ModifiedBy = GetModifiedBy(_context),
                            Attachment = byteArr,
                            ModuleId = "ES_SM",
                            FileSize = byteArr.Length,
                            FileExtension = System.IO.Path.GetExtension(name)
                        };
                        _tematach.Rows.Add(_attach.AttachmentGuid,
                              _attach.ParentGuid,
                              _attach.RecordGuid,
                              _attach.OrganizationGuid,
                              _attach.DepartmentGuid,
                              _attach.ModuleId,
                              _attach.Title,
                              _attach.FileName,
                              _attach.Attachment,
                              _attach.FileExtension,
                              _attach.FileSize,
                              _attach.UrlExternal,
                              _attach.IsPublic,
                              _attach.PrivateLevel,
                              _attach.IsFolder,
                              _attach.IsHidden,
                              _attach.IsReadonly,

                              _attach.IsSync,
                             DBNull.Value,
                              _attach.CloudPath,
                              DateTime.Now, DateTime.Now, _attach.CreatedBy, _attach.ModifiedBy);
                    }

                    _detailAttach.Value = _tematach;
                    _detailAttach.TypeName = "[dbo].[Attachment]";
                    var __temattach = _context.SQLCOMMANDS.FromSql(sqlAttach, Database, scheme, TableName, _detailAttach).FirstOrDefault();

                    if (__temattach.Error == "")
                    {
                        var mesage = __temattach.Error;
                    }
                    else
                    {
                        return new JMessage() { Error = true, Title = "Thêm file không thành công", Object = __temattach };
                    }

                }
                if (rs.Error == "")
                {

                    return new JMessage() { Error = false, Title = "Thêm ý kiến thành công." };
                }
                else
                {
                    return new JMessage() { Error = true, Title = rs.Error };
                }
            }
            catch (SqlException ex)
            {
                return new JMessage() { Error = true, Object = ex, Title = ex.Message };
            }
        }
        [HttpGet]
        [Area("Hr")]
        public FileResult DownloadfileCommentAttach(Guid? Id)
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var username = GetEmployeeLogin(_context);
                var gui = new Guid();
                string sql = "[dbo].[SP_Download_Attachment] @Database,@Scheme, @TableName, @OrganizationGuid, @AttachmentGuid, @Error";
                var Database = new SqlParameter("@Database", SqlDbType.VarChar); Database.Value = _appSettings.DBName_HR;
                var Scheme = new SqlParameter("@Scheme", SqlDbType.VarChar); Scheme.Value = "Hr";
                var TableName = new SqlParameter("@TableName", SqlDbType.VarChar); TableName.Value = "AnnouncementAttachmentComments";
                var OrganizationGuid = new SqlParameter("@OrganizationGuid", SqlDbType.UniqueIdentifier); OrganizationGuid.Value = username.OrganizationGuid;
                var AttachmentGuid = new SqlParameter("@AttachmentGuid", SqlDbType.UniqueIdentifier); AttachmentGuid.Value = Id == gui ? (Object)DBNull.Value : Id;
                var Error = new SqlParameter("@Error", SqlDbType.NVarChar); Error.Value = "";
                var rs = _context.AttachmentsDownloadView_.FromSql(sql, Database, Scheme, TableName, OrganizationGuid, AttachmentGuid, Error).SingleOrDefault();
                if (rs != null)
                {
                    byte[] fileBytes = rs.Attachment;
                    var fileName = rs.FileName;
                    return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);

                }
                else return null;
            }
            catch (Exception ex)
            {
                msg.Title = "Có lỗi khi lấy dữ liệu.";

                return null;

            }
        }
        public class CustomPage
        {
            public int Page { set; get; }
            public int ItemPage { set; get; }
            public int? Status { set; get; }
            public Guid? RecordGuid { set; get; }

        }
        [HttpPost]
        public object GetAllCommentPage([FromBody] CustomPage obj)
        {
            try
            {
                var _emp = GetEmployeeLogin(_context);

                var intBeginFor = (obj.Page - 1) * obj.ItemPage;
                string sql = "[Hr].[SP_GET_Comment] @DBName,@Scheme,@TableName,@DBNameDocument,@SchemeAt,@TableNameAt,@OrganizationGuid,@RecordGuid,@Skip,@Take,@Error";

                var DBName = new SqlParameter("@DBName", SqlDbType.NVarChar); CheckNullParameterStore(DBName, _appSettings.DBName_HR);
                var Scheme = new SqlParameter("@Scheme", SqlDbType.NVarChar); CheckNullParameterStore(Scheme, "Hr");
                var TableName = new SqlParameter("@TableName", SqlDbType.NVarChar); CheckNullParameterStore(TableName, "AnnouncementsComments");
                var DBNameDocument = new SqlParameter("@DBNameDocument", SqlDbType.NVarChar); CheckNullParameterStore(DBNameDocument, _appSettings.DBName_HR);
                var SchemeAt = new SqlParameter("@SchemeAt", SqlDbType.NVarChar); CheckNullParameterStore(SchemeAt, "Hr");
                var TableNameAt = new SqlParameter("@TableNameAt", SqlDbType.NVarChar); CheckNullParameterStore(TableNameAt, "AnnouncementAttachmentComments");
                var OrganizationGuid = new SqlParameter("@OrganizationGuid", SqlDbType.NVarChar); CheckNullParameterStore(OrganizationGuid, _emp.OrganizationGuid.ToString());
                var RecordGuid = new SqlParameter("@RecordGuid", SqlDbType.NVarChar); CheckNullParameterStore(RecordGuid, obj.RecordGuid.ToString());
                var Skip = new SqlParameter("@Skip", SqlDbType.NVarChar); Skip.Value = intBeginFor;
                var Take = new SqlParameter("@Take", SqlDbType.NVarChar); Take.Value = obj.ItemPage;
                var Error = new SqlParameter("@Error", SqlDbType.Int) { Direction = System.Data.ParameterDirection.Output };
                var rs = _context.CommentView.FromSql(sql, DBName, Scheme, TableName, DBNameDocument, SchemeAt, TableNameAt, OrganizationGuid, RecordGuid, Skip, Take, Error).ToList();

                return new JMessage() { Error = false, Title = "Success", Object = rs };

            }
            catch (Exception ex)
            {
                return new JMessage() { Error = true, Title = "Có lỗi khi lấy dữ liệu", Object = ex };
            }
        }




    }
}
