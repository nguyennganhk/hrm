﻿using FTU.Utils.HelperNet;
using HRM.Controllers;
using HRM.Models;
using HRM.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;
using System.Data;
using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Newtonsoft.Json;
using Grpc.Core;
using DocumentFormat.OpenXml.Drawing;
using ES_MODEL.HR.Custom.Wf;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using DocumentFormat.OpenXml.Drawing.Charts;
using ES_MODEL.Task;
using HRM.Filters;
using ES_MODEL.HR;
using Aspose.Pdf;
using DocumentFormat.OpenXml.Bibliography;
using DocumentFormat.OpenXml.Wordprocessing;
using Microsoft.CodeAnalysis;
using DocumentFormat.OpenXml.VariantTypes;
using static Aspose.Pdf.Operator;
using System.Threading;
using HRM.Models.Custom.Hr;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Math;
using System.Web;
using DocumentFormat.OpenXml.Spreadsheet;
using Stimulsoft.Report.Dashboard;
using DocumentFormat.OpenXml.Drawing.Spreadsheet;
using static Stimulsoft.Report.StiOptions.Wpf;
using Stimulsoft.System.Web.UI.WebControls;
using Items = HRM.Models.Items;
using DocumentFormat.OpenXml.ExtendedProperties;

namespace HRM.Areas.Sale.Controllers
{
    [Authorize]
    public class ProductionMaterialsController : BaseController
    {
        public IConfigurationRoot Configuration { get; }
        private readonly HRMDBContext _context;
        private readonly IHostingEnvironment _environment;
        private readonly AppSettings _appSettings;

        public ProductionMaterialsController(IOptions<AppSettings> appSettings, HRMDBContext context, IHostingEnvironment environment)
        {
            _context = context;
            _environment = environment;
            _appSettings = appSettings.Value;
        }
        [Area("Sale")]
        [ActionAuthorize(EAction.ACCESS)]
        public IActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public JsonResult GetUnit()
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var _emp = GetEmployeeLogin(_context);
                string sql = "[Sale].[SP_ESHR_Unit_GetAll] @OrganizationGuid";
                var OrganizationGuid = SqlPara("@OrganizationGuid", _emp.OrganizationGuid, SqlDbType.UniqueIdentifier);
                var rs = _context.Object_Combobox.FromSql(sql, OrganizationGuid).ToList();
                return Json(rs);
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = ex.Message });
            }
        }
        [HttpPost]
        public JsonResult GetProductionManager()
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var _emp = GetEmployeeLogin(_context);
                string sql = "[Sale].[SP_ESHR_ProductionManager_GetAll] @OrganizationGuid";
                var OrganizationGuid = SqlPara("@OrganizationGuid", _emp.OrganizationGuid, SqlDbType.UniqueIdentifier);
                var rs = _context.Object_Combobox.FromSql(sql, OrganizationGuid).ToList();
                return Json(rs);
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = ex.Message });
            }
        }
        [HttpPost]
        public object getItemjexcel([FromBody] JTableCustomer obj)
        {
            var msg = new JMessage() { Error = false };
            var _emp = GetEmployeeLogin(_context);
            try
            {
                string sql = "[Sale].[SP_ESHR_Items_jexcel] @Search,@OrganizationGuid";
                var _Search = new SqlParameter("@Search", SqlDbType.NVarChar); CheckNullParameterStore(_Search, obj.Keyword);
                var OrganizationGuid = SqlPara("@OrganizationGuid", _emp.OrganizationGuid, SqlDbType.UniqueIdentifier);
                var rs = _context.Object_Jexcel.FromSql(sql, _Search, OrganizationGuid).ToList();
                return Json(rs);
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = ex.Message });
            }
        }
        [HttpPost]
        public object getSupplierjexcel([FromBody] JTableCustomer obj)
        {
            var msg = new JMessage() { Error = false };
            var _emp = GetEmployeeLogin(_context);
            try
            {
                string sql = "[Sale].[SP_ESHR_Supplier_jexcel] @Search,@OrganizationGuid";
                var _Search = new SqlParameter("@Search", SqlDbType.NVarChar); CheckNullParameterStore(_Search, obj.Keyword);
                var OrganizationGuid = SqlPara("@OrganizationGuid", _emp.OrganizationGuid, SqlDbType.UniqueIdentifier);
                var rs = _context.Object_Jexcel.FromSql(sql, _Search, OrganizationGuid).ToList();
                return Json(rs);
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = ex.Message });
            }
        }
        [HttpPost]
        public object getItem_by_ItemId([FromBody] TempSub _obj)
        {
            var msg = new JMessage() { Error = false };
            var _emp = GetEmployeeLogin(_context);
            try
            {
                var Keyword = _obj.IdS[0];
                string sql = "[Sale].[SP_ESHR_Items_by_ItemID] @Search,@OrganizationGuid";
                var _Search = new SqlParameter("@Search", SqlDbType.NVarChar); CheckNullParameterStore(_Search, Keyword);
                var OrganizationGuid = SqlPara("@OrganizationGuid", _emp.OrganizationGuid, SqlDbType.UniqueIdentifier);

                var rs = _context.Items.FromSql(sql, _Search, OrganizationGuid).FirstOrDefault();
                return Json(rs);
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = ex.Message });
            }
        }
        [HttpPost]
        public object getSupplier_by_SupplierID([FromBody] TempSub _obj)
        {
            var msg = new JMessage() { Error = false };
            var _emp = GetEmployeeLogin(_context);
            try
            {
                var Keyword = _obj.IdS[0];
                var rs = _context.Customers.Where(x => x.CustomerId == Keyword).FirstOrDefault();
                return Json(rs);
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = ex.Message });
            }
        }
        [HttpPost]
        public object getUnitJexcel([FromBody] JTableCustomer obj)
        {
            var msg = new JMessage() { Error = false };
            var _emp = GetEmployeeLogin(_context);
            try
            {
                string sql = "[Sale].[SP_ESHR_Unit_jexcel] @Search,@OrganizationGuid";
                var _Search = new SqlParameter("@Search", SqlDbType.NVarChar); CheckNullParameterStore(_Search, obj.Keyword);
                var OrganizationGuid = SqlPara("@OrganizationGuid", _emp.OrganizationGuid, SqlDbType.UniqueIdentifier);
                var rs = _context.Object_Jexcel.FromSql(sql, _Search, OrganizationGuid).ToList();
                return Json(rs);
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = ex.Message });
            }
        }
        [HttpPost]
        public object getUnit_by_ItemId([FromBody] TempSub _obj)
        {
            var msg = new JMessage() { Error = false };
            var _emp = GetEmployeeLogin(_context);
            try
            {
                var Keyword = _obj.IdS[0];
                string sql = "[Sale].[SP_ESHR_Unit_jexcel_by_ItemId] @Search,@OrganizationGuid";
                var _Search = new SqlParameter("@Search", SqlDbType.NVarChar); CheckNullParameterStore(_Search, Keyword);
                var OrganizationGuid = SqlPara("@OrganizationGuid", _emp.OrganizationGuid, SqlDbType.UniqueIdentifier);

                var rs = _context.Units.FromSql(sql, _Search, OrganizationGuid).FirstOrDefault();
                return Json(rs);
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = ex.Message });
            }
        }

        [HttpPost]
        public object GetItemsByItemlistId()
        {
            try
            {
                var _emp = GetEmployeeLogin(_context);
                var tempRecrui = Request.Form["Insert"];
                var Detail = Request.Form["Detail"];
                ProductionMaterials obj = JsonConvert.DeserializeObject<ProductionMaterials>(tempRecrui);
                List<string> detail = JsonConvert.DeserializeObject<List<string>>(Detail);

                string sql = "[Sale].[SP_ESAM_GetItems_By_listItemId] @OrganizationGuid,@ItemId";
                var _OrganizationGuid = new SqlParameter("@OrganizationGuid", SqlDbType.UniqueIdentifier); _OrganizationGuid.Value = _emp.OrganizationGuid;
                var _ItemId = new SqlParameter("@ItemId", detail.Count >= 1 ? string.Join(",", detail) : "");
                var rs = _context.Items.FromSql(sql, _OrganizationGuid, _ItemId).ToList();
                return Json(rs);
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = "Lấy dữ liệu không thành công" });
            }
        }


        public class JTableCustomer : JTableModel
        {
            public string Keyword { set; get; }
            public string IsBussiness { set; get; }
            public string Status { set; get; }
            public string Types { set; get; }
            public string IsProvince { set; get; }
            public string ProvinceId { set; get; }
            public string TerritoryId { set; get; }
        }
        [HttpPost]
        public object JTable([FromBody] JTableCustomer jTable)
        {
            try
            {
                var _emp = GetEmployeeLogin(_context);
                int intBeginFor = (jTable.CurrentPage - 1) * jTable.Length;
                string sql = "[Sale].[SP_ESSM_ProductionMaterials_viewJTable] " +
                   "@Keyword, " +
                   "@Status, " +
                   "@OrganizationGuid, " +
                   "@Skip, " +
                   "@Take," +
                   "@Count out";
                var _Keyword = SqlPara("@Keyword", jTable.Keyword, SqlDbType.NVarChar);
                var _Status = SqlPara("@Status", jTable.Status, SqlDbType.NVarChar);
                var _OrganizationGuid = SqlPara("@OrganizationGuid", _emp.OrganizationGuid, SqlDbType.UniqueIdentifier);
                var _Skip = SqlPara("@Skip", intBeginFor, SqlDbType.Int);
                var _Take = SqlPara("@Take", jTable.Length, SqlDbType.Int);
                var _output = new SqlParameter("@Count", SqlDbType.Int) { Direction = System.Data.ParameterDirection.Output };

                var data = _context.ProductionMaterialsViews.FromSql(
                            sql,
                            _Keyword,
                            _Status,
                            _OrganizationGuid,
                            _Skip,
                            _Take,
                            _output
                ).ToList();
                var jdata = JTableHelper.JObjectTable(data, jTable.Draw, (int)_output.Value);
                return Json(jdata);
            }
            catch (Exception ex)
            {
                return new JMessage() { Error = true, Title = ex.Message, Object = ex };
            }
        }
        [HttpPost]
        public JsonResult GetItem(Guid Id)
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var _emp = GetEmployeeLogin(_context);
                string sql = "[Sale].[SP_ESSM_ProductionMaterials_GetItem] @RowGuid";
                var RowGuid = SqlPara("@RowGuid", Id, SqlDbType.UniqueIdentifier);
                var rs = _context.Object_Table.FromSql(sql, RowGuid).FirstOrDefault();
                return Json(rs);
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = ex.Message });
            }
        }

        public class ObjItems : ProductionMaterials
        {
            public string Temps { set; get; }
        }
        [HttpPost]
        public JsonResult Submit()
        {

            var msg = new JMessage() { Error = false };
            try
            {
                var _emp = GetEmployeeLogin(_context);
                var files = Request.Form.Files.ToList();
                var submit = Request.Form["submit"];
                var detail = Request.Form["detail"];
                ObjItems obj = JsonConvert.DeserializeObject<ObjItems>(submit);
                List<ProductionMaterialDetails> lstdetail = JsonConvert.DeserializeObject<List<ProductionMaterialDetails>>(detail);
                string sql = "[Sale].[SP_ESSM_ProductionMaterials_Insert] @RowGuid,@OrganizationGuid,@CodeID,@DateSX,@Title,@EmployeeID,@EmployeeName,@ProductionManagerGuid,@TotalMoney,@Status,@IsApprove,@IsAccountant,@IsManager,@ConditionValue,@WorkFlowGuid,@WorkFlowsContent,@WorkFlowCurrent,@CreatedDate,@ModifiedDate,@CreatedBy,@ModifiedBy" +
               ",@Temps,@ProductionMaterialDetails";
                obj.CreatedDate = DateTime.Now;
                obj.ModifiedDate = DateTime.Now;
                obj.CreatedBy = GetCreatedBy(_context);
                obj.ModifiedBy = GetCreatedBy(_context);
                var RowGuid = SqlPara("@RowGuid                     ", obj.RowGuid, SqlDbType.UniqueIdentifier);
                var OrganizationGuid = SqlPara("@OrganizationGuid            ", obj.OrganizationGuid, SqlDbType.UniqueIdentifier);
                var CodeID = SqlPara("@CodeID                      ", obj.CodeID, SqlDbType.NVarChar);
                var DateSX = SqlPara("@DateSX                      ", obj.DateSX, SqlDbType.Date);
                var Title = SqlPara("@Title                       ", obj.Title, SqlDbType.NVarChar);
                var EmployeeID = SqlPara("@EmployeeID                  ", obj.EmployeeID, SqlDbType.NVarChar);
                var EmployeeName = SqlPara("@EmployeeName                ", obj.EmployeeName, SqlDbType.NVarChar);
                var ProductionManagerGuid = SqlPara("@ProductionManagerGuid       ", obj.ProductionManagerGuid, SqlDbType.UniqueIdentifier);
                var TotalMoney = SqlPara("@TotalMoney                  ", obj.TotalMoney, SqlDbType.Decimal);
                var Status = SqlPara("@Status                      ", obj.Status, SqlDbType.NVarChar);
                var IsApprove = SqlPara("@IsApprove                   ", obj.IsApprove, SqlDbType.NVarChar);
                var IsAccountant = SqlPara("@IsAccountant                ", obj.IsAccountant, SqlDbType.Bit);
                var IsManager = SqlPara("@IsManager                   ", obj.IsManager, SqlDbType.Bit);
                var ConditionValue = SqlPara("@ConditionValue              ", obj.ConditionValue, SqlDbType.NVarChar);
                var WorkFlowGuid = SqlPara("@WorkFlowGuid                ", obj.WorkFlowGuid, SqlDbType.UniqueIdentifier);
                var WorkFlowsContent = SqlPara("@WorkFlowsContent            ", obj.WorkFlowsContent, SqlDbType.NVarChar);
                var WorkFlowCurrent = SqlPara("@WorkFlowCurrent             ", obj.WorkFlowCurrent, SqlDbType.UniqueIdentifier);
                var CreatedDate = SqlPara("@CreatedDate                 ", obj.CreatedDate, SqlDbType.DateTime);
                var ModifiedDate = SqlPara("@ModifiedDate                ", obj.ModifiedDate, SqlDbType.DateTime);
                var CreatedBy = SqlPara("@CreatedBy                   ", obj.CreatedBy, SqlDbType.NVarChar);
                var ModifiedBy = SqlPara("@ModifiedBy                  ", obj.ModifiedBy, SqlDbType.NVarChar);

                var Temps = SqlPara("@Temps", obj.Temps, SqlDbType.NVarChar);
                var _detail = new SqlParameter("@ProductionMaterialDetails", SqlDbType.Structured);
                var _tem = new System.Data.DataTable();
                _tem.Columns.Add("RowGuid      ");
                _tem.Columns.Add("RecordGuid   ");
                _tem.Columns.Add("ItemID       ");
                _tem.Columns.Add("ItemName     ");
                _tem.Columns.Add("Title        ");
                _tem.Columns.Add("UnitID       ");
                _tem.Columns.Add("UnitName     ");
                _tem.Columns.Add("Quantity     ");
                _tem.Columns.Add("UnitPrice    ");
                _tem.Columns.Add("AmountOc     ");
                _tem.Columns.Add("VATPercent   ");
                _tem.Columns.Add("VATAmountOC  ");
                _tem.Columns.Add("AmountNoVAT  ");
                _tem.Columns.Add("AmountVAT    ");
                _tem.Columns.Add("Note         ");
                _tem.Columns.Add("Packings         ");
                _tem.Columns.Add("ProductSpecificate         ");
                _tem.Columns.Add("SupplierID         ");
                _tem.Columns.Add("SupplierName         ");
                _tem.Columns.Add("ImportDate         ",typeof(DateTime));
                _tem.Columns.Add("PaymentsType         ");
                _tem.Columns.Add("PlaceOfDelivery         ");
                _tem.Columns.Add("CreatedDate  ");
                _tem.Columns.Add("ModifiedDate ");
                _tem.Columns.Add("CreatedBy    ");
                _tem.Columns.Add("ModifiedBy   ");
                _tem.Columns.Add("SortOrder    ");

                foreach (var _item in lstdetail)
                {
                    if (_item.ImportDate != null)
                    {
                        _item.ImportDate = _item.ImportDate.Value.ToLocalTime();
                    }
                    else
                    {
                        _item.ImportDate = null;
                    }
                    _tem.Rows.Add(
                            _item.RowGuid
                            , _item.RecordGuid
                            , _item.ItemID
                            , _item.ItemName
                            , _item.Title
                            , _item.UnitID
                            , _item.UnitName
                            , _item.Quantity
                            , _item.UnitPrice
                            , _item.AmountOc
                            , _item.VATPercent
                            , _item.VATAmountOC
                            , _item.AmountNoVAT
                            , _item.AmountVAT
                            , _item.Note
                            , _item.Packings
                            , _item.ProductSpecificate
                            , _item.SupplierID
                            , _item.SupplierName
                            , _item.ImportDate
                            , _item.PaymentsType
                            , _item.PlaceOfDelivery
                            , _item.CreatedDate
                            , _item.ModifiedDate
                            , _item.CreatedBy
                            , _item.ModifiedBy
                            , _item.SortOrder
                             );
                }
                _detail.Value = _tem;
                _detail.TypeName = "[Sale].[ProductionMaterialDetails]";
                var _rs = _context.SQLCOMMANDS.FromSql(sql, RowGuid, OrganizationGuid, CodeID, DateSX, Title, EmployeeID, EmployeeName, ProductionManagerGuid, TotalMoney, Status, IsApprove, IsAccountant, IsManager, ConditionValue, WorkFlowGuid, WorkFlowsContent, WorkFlowCurrent, CreatedDate, ModifiedDate, CreatedBy, ModifiedBy, Temps, _detail).FirstOrDefault();
                if (_rs.Error == "")
                {
                    if (obj.Temps == "1")
                    {
                        msg.Title = "Thêm mới thành công";
                    }
                    else
                    {
                        msg.Title = "Cập nhật thành công";
                    }
                }
                else
                {
                    return Json(new JMessage() { Error = true, Title = _rs.Error, Object = _rs.Error });
                }
            }
            catch (Exception ex)
            {
                msg.Error = true;
                msg.Object = ex;
                msg.Title = ex.Message;
            }
            return Json(msg);
        }

        [HttpPost]
        public object Delete(Guid? Id)
        {
            var msg = new JMessage() { Error = false };
            try
            {
                string sql = "[Sale].[SP_ESSM_ProductionMaterials_Delete] @RowGuid";
                var RowGuid = SqlPara("@RowGuid", Id, SqlDbType.UniqueIdentifier);
                var __tem = _context.SQLCOMMANDS.FromSql(sql, RowGuid).FirstOrDefault();
                if (__tem.Error == "")
                {
                    return new JMessage() { Error = false, Title = "Xóa thành công." };
                }
                else
                {
                    return new JMessage() { Error = true, Title = __tem.Error, Object = __tem };
                }
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = ex.Message });
            }
        }
        [HttpPost]
        //[ActionAuthorize(EAction.LISTVIEW)]
        public object JTable_Customers([FromBody] JTableCustomer jTablePara)
        {
            var emp = GetEmployeeLogin(_context);
            var count = (from a in _context.Customers
                         where a.IsProvider == true
                         select new { a.CustomerGuid }).Count();

            int intBeginFor = (jTablePara.CurrentPage - 1) * jTablePara.Length;
            var balance = (from a in _context.Customers
                           where a.IsProvider == true
                           select new { a.CustomerGuid, a.CustomerId, a.CustomerName, a.Address, a.WorkPlace, a.ActivityFieldName, a.BusinessTypeName, a.LeadName, a.Method, a.Note }).OrderUsingSortExpression(jTablePara.QueryOrderBy).Skip(intBeginFor).Take(jTablePara.Length).ToList();
            var jdata = JTableHelper.JObjectTable(balance, jTablePara.Draw, count, "CustomerGuid", "CustomerId", "CustomerName", "Address", "WorkPlace", "ActivityFieldName", "BusinessTypeName", "LeadName", "Method", "Note");
            return Json(jdata);
        }
    }

}
