﻿using FTU.Utils.HelperNet;
using HRM.Controllers;
using HRM.Models;
using HRM.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;
using System.Data;
using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Newtonsoft.Json;
using Grpc.Core;
using DocumentFormat.OpenXml.Drawing;
using ES_MODEL.HR.Custom.Wf;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using DocumentFormat.OpenXml.Drawing.Charts;
using ES_MODEL.Task;
using HRM.Filters;
using ES_MODEL.HR;
using Aspose.Pdf;
using DocumentFormat.OpenXml.Bibliography;
using DocumentFormat.OpenXml.Wordprocessing;
using Microsoft.CodeAnalysis;
using DocumentFormat.OpenXml.VariantTypes;
using static Aspose.Pdf.Operator;
using System.Threading;
using HRM.Models.Custom.Hr;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Math;
using System.Web;
using DocumentFormat.OpenXml.Spreadsheet;
using Stimulsoft.Report.Dashboard;
using DocumentFormat.OpenXml.Drawing.Spreadsheet;
using static Stimulsoft.Report.StiOptions.Wpf;
using Stimulsoft.System.Web.UI.WebControls;
using Items = HRM.Models.Items;

namespace HRM.Areas.Sale.Controllers
{
    [Authorize]
    public class ItemsController : BaseController
    {
        public IConfigurationRoot Configuration { get; }
        private readonly HRMDBContext _context;
        private readonly IHostingEnvironment _environment;
        private readonly AppSettings _appSettings;

        public ItemsController(IOptions<AppSettings> appSettings, HRMDBContext context, IHostingEnvironment environment)
        {
            _context = context;
            _environment = environment;
            _appSettings = appSettings.Value;
        }
        [Area("Sale")]
        [ActionAuthorize(EAction.ACCESS)]
        public IActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public JsonResult GetUnit()
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var _emp = GetEmployeeLogin(_context);
                string sql = "[Sale].[SP_ESHR_Unit_GetAll] @OrganizationGuid";
                var OrganizationGuid = SqlPara("@OrganizationGuid", _emp.OrganizationGuid, SqlDbType.UniqueIdentifier);
                var rs = _context.Object_Combobox.FromSql(sql, OrganizationGuid).ToList();
                return Json(rs);
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = ex.Message });
            }
        }

        public class JTableCustomer : JTableModel
        {
            public string Keyword { set; get; }
            public string IsBussiness { set; get; }
            public string Status { set; get; }
            public string Type { set; get; }
            public string IsProvince { set; get; }
            public string ProvinceId { set; get; }
            public string TerritoryId { set; get; }
        }
        [HttpPost]
        public object JTable([FromBody] JTableCustomer jTable)
        {
            try
            {
                var _emp = GetEmployeeLogin(_context);
                int intBeginFor = (jTable.CurrentPage - 1) * jTable.Length;
                string sql = "[Sale].[SP_ESSM_Items_viewJTable] " +
                   "@Keyword, " +
                   "@Status, " +
                   "@OrganizationGuid, " +
                   "@Skip, " +
                   "@Take," +
                   "@Count out";
                var _Keyword = SqlPara("@Keyword", jTable.Keyword, SqlDbType.NVarChar);
                var _Status = SqlPara("@Status", jTable.Status, SqlDbType.NVarChar);
                var _OrganizationGuid = SqlPara("@OrganizationGuid", _emp.OrganizationGuid, SqlDbType.UniqueIdentifier);
                var _Skip = SqlPara("@Skip", intBeginFor, SqlDbType.Int);
                var _Take = SqlPara("@Take", jTable.Length, SqlDbType.Int);
                var _output = new SqlParameter("@Count", SqlDbType.Int) { Direction = System.Data.ParameterDirection.Output };

                var data = _context.Items.FromSql(
                            sql,
                            _Keyword,
                            _Status,
                            _OrganizationGuid,
                            _Skip,
                            _Take,
                            _output
                ).ToList();
                var jdata = JTableHelper.JObjectTable(data, jTable.Draw, (int)_output.Value);
                return Json(jdata);
            }
            catch (Exception ex)
            {
                return new JMessage() { Error = true, Title = ex.Message, Object = ex };
            }
        }
        [HttpPost]
        public JsonResult GetItem(Guid Id)
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var _emp = GetEmployeeLogin(_context);
                string sql = "[Sale].[SP_ESHR_Items_GetItem] @RowGuid";
                var RowGuid = SqlPara("@RowGuid", Id, SqlDbType.UniqueIdentifier);
                var rs = _context.Object_Table.FromSql(sql, RowGuid).FirstOrDefault();
                return Json(rs);
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = ex.Message });
            }
        }

        public class ObjItems : Items
        {
            public string Temps { set; get; }
        }
        [HttpPost]
        public JsonResult Submit()
        {

            var msg = new JMessage() { Error = false };
            try
            {
                var _emp = GetEmployeeLogin(_context);
                var files = Request.Form.Files.ToList();
                var submit = Request.Form["submit"];
                ObjItems obj = JsonConvert.DeserializeObject<ObjItems>(submit);

                string sql = "[Sale].[SP_ESSM_Items_Insert]" +
               "@RowGuid" +
               ",@ItemID" +
               ",@ItemName" +
               ",@ItemProviderID" +
               ",@UnitID" +
               ",@UnitName" +
               ",@Quantity" +
               ",@QuantityExist" +
                ",@Origin" +
                ",@Characteristic" +
                ",@HarvestSeason" +
                 ",@Preserve" +
                 ",@EmployeeName" +
                ",@Note" +
               ",@CreatedDate" +
               ",@CreatedBy" +
               ",@ModifiedDate" +
               ",@ModifiedBy" +
               ",@Temps";
                obj.CreatedDate = DateTime.Now;
                obj.ModifiedDate = DateTime.Now;
                obj.CreatedBy = GetCreatedBy(_context);
                obj.ModifiedBy = GetCreatedBy(_context);
                var RowGuid = SqlPara("@RowGuid", obj.RowGuid, SqlDbType.UniqueIdentifier);
                var ItemID = SqlPara("@ItemID        ", obj.ItemID, SqlDbType.NVarChar);
                var ItemName = SqlPara("@ItemName      ", obj.ItemName, SqlDbType.NVarChar);
                var ItemProviderID = SqlPara("@ItemProviderID", obj.ItemProviderID, SqlDbType.NVarChar);
                var UnitID = SqlPara("@UnitID        ", obj.UnitID, SqlDbType.NVarChar);
                var UnitName = SqlPara("@UnitName      ", obj.UnitName, SqlDbType.NVarChar);
                var Quantity = SqlPara("@Quantity      ", obj.Quantity, SqlDbType.Decimal);
                var QuantityExist = SqlPara("@QuantityExist ", obj.QuantityExist, SqlDbType.Decimal);
                var Origin = SqlPara("@Origin      ", obj.Origin, SqlDbType.NVarChar);
                var Characteristic = SqlPara("@Characteristic      ", obj.Characteristic, SqlDbType.NVarChar);
                var HarvestSeason = SqlPara("@HarvestSeason      ", obj.HarvestSeason, SqlDbType.NVarChar);
                var Preserve = SqlPara("@Preserve      ", obj.Preserve, SqlDbType.NVarChar);
                var EmployeeName = SqlPara("@EmployeeName      ", obj.EmployeeName, SqlDbType.NVarChar);
                var Note = SqlPara("@Note      ", obj.Note, SqlDbType.NVarChar);
                var CreatedDate = SqlPara("@CreatedDate   ", obj.CreatedDate, SqlDbType.DateTime);
                var CreatedBy = SqlPara("@CreatedBy     ", obj.CreatedBy, SqlDbType.NVarChar);
                var ModifiedDate = SqlPara("@ModifiedDate  ", obj.ModifiedDate, SqlDbType.DateTime);
                var ModifiedBy = SqlPara("@ModifiedBy    ", obj.ModifiedBy, SqlDbType.NVarChar);
                var Temps = SqlPara("@Temps", obj.Temps, SqlDbType.NVarChar);

                var _rs = _context.SQLCOMMANDS.FromSql(sql, RowGuid, ItemID, ItemName, ItemProviderID, UnitID, UnitName, Quantity, QuantityExist, Origin, Characteristic, HarvestSeason, Preserve, EmployeeName, Note, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, Temps).FirstOrDefault();
                if (_rs.Error == "")
                {
                    if (obj.Temps == "1")
                    {
                        msg.Title = "Thêm mới thành công";
                    }
                    else
                    {
                        msg.Title = "Cập nhật thành công";
                    }
                }
                else
                {
                    return Json(new JMessage() { Error = true, Title = _rs.Error, Object = _rs.Error });
                }
            }
            catch(Exception ex)
            {
                msg.Error = true;
                msg.Object = ex;
                msg.Title = ex.Message;
            }
            return Json(msg);
        }

        [HttpPost]
        public object Delete(Guid? Id)
        {
            var msg = new JMessage() { Error = false };
            try
            {
                string sql = "[Sale].[SP_ESSM_Items_Delete] @RowGuid";
                var RowGuid = SqlPara("@RowGuid", Id, SqlDbType.UniqueIdentifier);
                var __tem = _context.SQLCOMMANDS.FromSql(sql, RowGuid).FirstOrDefault();
                if (__tem.Error == "")
                {
                    return new JMessage() { Error = false, Title = "Xóa thành công." };
                }
                else
                {
                    return new JMessage() { Error = true, Title = __tem.Error, Object = __tem };
                }
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = ex.Message });
            }
        }
    }
}
