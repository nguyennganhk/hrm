﻿using FTU.Utils.HelperNet;
using HRM.Controllers;
using HRM.Models;
using HRM.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;
using System.Data;
using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Newtonsoft.Json;
using Grpc.Core;
using DocumentFormat.OpenXml.Drawing;
using ES_MODEL.HR.Custom.Wf;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using DocumentFormat.OpenXml.Drawing.Charts;
using ES_MODEL.Task;
using HRM.Filters;
using ES_MODEL.HR;
using Aspose.Pdf;
using DocumentFormat.OpenXml.Bibliography;
using DocumentFormat.OpenXml.Wordprocessing;
using Microsoft.CodeAnalysis;
using DocumentFormat.OpenXml.VariantTypes;
using static Aspose.Pdf.Operator;
using System.Threading;
using HRM.Models.Custom.Hr;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Math;
using System.Web;
using DocumentFormat.OpenXml.Spreadsheet;
using Stimulsoft.Report.Dashboard;
using DocumentFormat.OpenXml.Drawing.Spreadsheet;
using static Stimulsoft.Report.StiOptions.Wpf;
using Stimulsoft.System.Web.UI.WebControls;
using Items = HRM.Models.Items;
using DocumentFormat.OpenXml.ExtendedProperties;
using DataTable = System.Data.DataTable;

namespace HRM.Areas.Sale.Controllers
{
    [Authorize]
    public class LogisticsController : BaseController
    {
        public IConfigurationRoot Configuration { get; }
        private readonly HRMDBContext _context;
        private readonly IHostingEnvironment _environment;
        private readonly AppSettings _appSettings;

        public LogisticsController(IOptions<AppSettings> appSettings, HRMDBContext context, IHostingEnvironment environment)
        {
            _context = context;
            _environment = environment;
            _appSettings = appSettings.Value;
        }
        [Area("Sale")]
        [ActionAuthorize(EAction.ACCESS)]
        public IActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public JsonResult GetUnit()
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var _emp = GetEmployeeLogin(_context);
                string sql = "[Sale].[SP_ESHR_Unit_GetAll] @OrganizationGuid";
                var OrganizationGuid = SqlPara("@OrganizationGuid", _emp.OrganizationGuid, SqlDbType.UniqueIdentifier);
                var rs = _context.Object_Combobox.FromSql(sql, OrganizationGuid).ToList();
                return Json(rs);
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = ex.Message });
            }
        }
        [HttpPost]
        public JsonResult GetProductionManager()
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var _emp = GetEmployeeLogin(_context);
                string sql = "[Sale].[SP_ESHR_ProductionManager_GetAll] @OrganizationGuid";
                var OrganizationGuid = SqlPara("@OrganizationGuid", _emp.OrganizationGuid, SqlDbType.UniqueIdentifier);
                var rs = _context.Object_Combobox.FromSql(sql, OrganizationGuid).ToList();
                return Json(rs);
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = ex.Message });
            }
        }
        [HttpPost]
        public object getItemjexcel([FromBody] JTableCustomer obj)
        {
            var msg = new JMessage() { Error = false };
            var _emp = GetEmployeeLogin(_context);
            try
            {
                string sql = "[Sale].[SP_ESHR_Items_jexcel] @Search,@OrganizationGuid";
                var _Search = new SqlParameter("@Search", SqlDbType.NVarChar); CheckNullParameterStore(_Search, obj.Keyword);
                var OrganizationGuid = SqlPara("@OrganizationGuid", _emp.OrganizationGuid, SqlDbType.UniqueIdentifier);
                var rs = _context.Object_Jexcel.FromSql(sql, _Search, OrganizationGuid).ToList();
                return Json(rs);
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = ex.Message });
            }
        }
        [HttpPost]
        public object getItem_by_ItemId([FromBody] TempSub _obj)
        {
            var msg = new JMessage() { Error = false };
            var _emp = GetEmployeeLogin(_context);
            try
            {
                var Keyword = _obj.IdS[0];
                string sql = "[Sale].[SP_ESHR_Items_by_ItemID] @Search,@OrganizationGuid";
                var _Search = new SqlParameter("@Search", SqlDbType.NVarChar); CheckNullParameterStore(_Search, Keyword);
                var OrganizationGuid = SqlPara("@OrganizationGuid", _emp.OrganizationGuid, SqlDbType.UniqueIdentifier);

                var rs = _context.Items.FromSql(sql, _Search, OrganizationGuid).FirstOrDefault();
                return Json(rs);
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = ex.Message });
            }
        }
        [HttpPost]
        public object getUnitJexcel([FromBody] JTableCustomer obj)
        {
            var msg = new JMessage() { Error = false };
            var _emp = GetEmployeeLogin(_context);
            try
            {
                string sql = "[Sale].[SP_ESHR_Unit_jexcel] @Search,@OrganizationGuid";
                var _Search = new SqlParameter("@Search", SqlDbType.NVarChar); CheckNullParameterStore(_Search, obj.Keyword);
                var OrganizationGuid = SqlPara("@OrganizationGuid", _emp.OrganizationGuid, SqlDbType.UniqueIdentifier);
                var rs = _context.Object_Jexcel.FromSql(sql, _Search, OrganizationGuid).ToList();
                return Json(rs);
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = ex.Message });
            }
        }
        [HttpPost]
        public object getUnit_by_ItemId([FromBody] TempSub _obj)
        {
            var msg = new JMessage() { Error = false };
            var _emp = GetEmployeeLogin(_context);
            try
            {
                var Keyword = _obj.IdS[0];
                string sql = "[Sale].[SP_ESHR_Unit_jexcel_by_ItemId] @Search,@OrganizationGuid";
                var _Search = new SqlParameter("@Search", SqlDbType.NVarChar); CheckNullParameterStore(_Search, Keyword);
                var OrganizationGuid = SqlPara("@OrganizationGuid", _emp.OrganizationGuid, SqlDbType.UniqueIdentifier);

                var rs = _context.Units.FromSql(sql, _Search, OrganizationGuid).FirstOrDefault();
                return Json(rs);
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = ex.Message });
            }
        }

        [HttpPost]
        public object GetItemsByItemlistId()
        {
            try
            {
                var _emp = GetEmployeeLogin(_context);
                var tempRecrui = Request.Form["Insert"];
                var Detail = Request.Form["Detail"];
                ProductionMaterials obj = JsonConvert.DeserializeObject<ProductionMaterials>(tempRecrui);
                List<string> detail = JsonConvert.DeserializeObject<List<string>>(Detail);

                string sql = "[Sale].[SP_ESAM_GetItems_By_listItemId] @OrganizationGuid,@ItemId";
                var _OrganizationGuid = new SqlParameter("@OrganizationGuid", SqlDbType.UniqueIdentifier); _OrganizationGuid.Value = _emp.OrganizationGuid;
                var _ItemId = new SqlParameter("@ItemId", detail.Count >= 1 ? string.Join(",", detail) : "");
                var rs = _context.Items.FromSql(sql, _OrganizationGuid, _ItemId).ToList();
                return Json(rs);
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = "Lấy dữ liệu không thành công" });
            }
        }


        public class JTableCustomer : JTableModel
        {
            public string Keyword { set; get; }
            public string IsBussiness { set; get; }
            public string Status { set; get; }
            public string Types { set; get; }
            public string IsProvince { set; get; }
            public string ProvinceId { set; get; }
            public string TerritoryId { set; get; }
        }
        [HttpPost]
        public object JTable([FromBody] JTableCustomer jTable)
        {
            try
            {
                var _emp = GetEmployeeLogin(_context);
                int intBeginFor = (jTable.CurrentPage - 1) * jTable.Length;
                string sql = "[Sale].[SP_ESSM_Logistics_viewJTable] " +
                   "@Keyword, " +
                   "@Status, " +
                   "@OrganizationGuid, " +
                   "@Skip, " +
                   "@Take," +
                   "@Count out";
                var _Keyword = SqlPara("@Keyword", jTable.Keyword, SqlDbType.NVarChar);
                var _Status = SqlPara("@Status", jTable.Status, SqlDbType.NVarChar);
                var _OrganizationGuid = SqlPara("@OrganizationGuid", _emp.OrganizationGuid, SqlDbType.UniqueIdentifier);
                var _Skip = SqlPara("@Skip", intBeginFor, SqlDbType.Int);
                var _Take = SqlPara("@Take", jTable.Length, SqlDbType.Int);
                var _output = new SqlParameter("@Count", SqlDbType.Int) { Direction = System.Data.ParameterDirection.Output };

                var data = _context.ProductionMaterialsViews.FromSql(
                            sql,
                            _Keyword,
                            _Status,
                            _OrganizationGuid,
                            _Skip,
                            _Take,
                            _output
                ).ToList();
                var jdata = JTableHelper.JObjectTable(data, jTable.Draw, (int)_output.Value);
                return Json(jdata);
            }
            catch (Exception ex)
            {
                return new JMessage() { Error = true, Title = ex.Message, Object = ex };
            }
        }
        [HttpPost]
        public JsonResult GetItem(Guid Id)
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var _emp = GetEmployeeLogin(_context);
                string sql = "[Sale].[SP_ESSM_Logistics_GetItem] @RowGuid";
                var RowGuid = SqlPara("@RowGuid", Id, SqlDbType.UniqueIdentifier);
                var rs = _context.Object_Table.FromSql(sql, RowGuid).FirstOrDefault();
                return Json(rs);
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = ex.Message });
            }
        }

        public class ObjItems : Logistics
        {
            public string Temps { set; get; }
        }
        [HttpPost]
        public JsonResult Submit()
        {

            var msg = new JMessage() { Error = false };
            try
            {
                var _emp = GetEmployeeLogin(_context);
                var files = Request.Form.Files.ToList();
                var submit = Request.Form["submit"];
                var detail = Request.Form["detail"];
                ObjItems obj = JsonConvert.DeserializeObject<ObjItems>(submit);
                List<LogisticsDetails> lstdetail = JsonConvert.DeserializeObject<List<LogisticsDetails>>(detail);
                string sql = "[Sale].[SP_ESSM_Logistics_Insert] @RowGuid,@OrganizationGuid,@CodeID,@DateSX,@Title,@EmployeeID,@EmployeeName,@ProductionManagerGuid,@TotalMoney,@Status,@IsApprove,@IsAccountant,@IsManager,@ConditionValue,@WorkFlowGuid,@WorkFlowsContent,@WorkFlowCurrent,@CreatedDate,@ModifiedDate,@CreatedBy,@ModifiedBy" +
               ",@Temps,@LogisticsDetails";
                obj.CreatedDate = DateTime.Now;
                obj.ModifiedDate = DateTime.Now;
                obj.CreatedBy = GetCreatedBy(_context);
                obj.ModifiedBy = GetCreatedBy(_context);
                var RowGuid = SqlPara("@RowGuid                     ", obj.RowGuid, SqlDbType.UniqueIdentifier);
                var OrganizationGuid = SqlPara("@OrganizationGuid            ", obj.OrganizationGuid, SqlDbType.UniqueIdentifier);
                var CodeID = SqlPara("@CodeID                      ", obj.CodeID, SqlDbType.NVarChar);
                var DateSX = SqlPara("@DateSX                      ", obj.DateSX, SqlDbType.DateTime);
                var Title = SqlPara("@Title                       ", obj.Title, SqlDbType.NVarChar);
                var EmployeeID = SqlPara("@EmployeeID                  ", obj.EmployeeID, SqlDbType.NVarChar);
                var EmployeeName = SqlPara("@EmployeeName                ", obj.EmployeeName, SqlDbType.NVarChar);
                var ProductionManagerGuid = SqlPara("@ProductionManagerGuid       ", obj.ProductionManagerGuid, SqlDbType.UniqueIdentifier);
                var TotalMoney = SqlPara("@TotalMoney                  ", obj.TotalMoney, SqlDbType.Decimal);
                var Status = SqlPara("@Status                      ", obj.Status, SqlDbType.NVarChar);
                var IsApprove = SqlPara("@IsApprove                   ", obj.IsApprove, SqlDbType.NVarChar);
                var IsAccountant = SqlPara("@IsAccountant                ", obj.IsAccountant, SqlDbType.Bit);
                var IsManager = SqlPara("@IsManager                   ", obj.IsManager, SqlDbType.Bit);
                var ConditionValue = SqlPara("@ConditionValue              ", obj.ConditionValue, SqlDbType.NVarChar);
                var WorkFlowGuid = SqlPara("@WorkFlowGuid                ", obj.WorkFlowGuid, SqlDbType.UniqueIdentifier);
                var WorkFlowsContent = SqlPara("@WorkFlowsContent            ", obj.WorkFlowsContent, SqlDbType.NVarChar);
                var WorkFlowCurrent = SqlPara("@WorkFlowCurrent             ", obj.WorkFlowCurrent, SqlDbType.UniqueIdentifier);
                var CreatedDate = SqlPara("@CreatedDate                 ", obj.CreatedDate, SqlDbType.DateTime);
                var ModifiedDate = SqlPara("@ModifiedDate                ", obj.ModifiedDate, SqlDbType.DateTime);
                var CreatedBy = SqlPara("@CreatedBy                   ", obj.CreatedBy, SqlDbType.NVarChar);
                var ModifiedBy = SqlPara("@ModifiedBy                  ", obj.ModifiedBy, SqlDbType.NVarChar);

                var Temps = SqlPara("@Temps", obj.Temps, SqlDbType.NVarChar);
                var _detail = new SqlParameter("@LogisticsDetails", SqlDbType.Structured);
                var _tem = new System.Data.DataTable();
                _tem.Columns.Add("RowGuid      ");
                _tem.Columns.Add("RecordGuid   ");
                _tem.Columns.Add("ItemID       ");
                _tem.Columns.Add("ItemName     ");
                _tem.Columns.Add("Title        ");
                _tem.Columns.Add("UnitID       ");
                _tem.Columns.Add("UnitName     ");
                _tem.Columns.Add("Quantity     ");
                _tem.Columns.Add("UnitPrice    ");
                _tem.Columns.Add("AmountOc     ");
                _tem.Columns.Add("VATPercent   ");
                _tem.Columns.Add("VATAmountOC  ");
                _tem.Columns.Add("AmountNoVAT  ");
                _tem.Columns.Add("AmountVAT    ");
                _tem.Columns.Add("Note         ");
                _tem.Columns.Add("CreatedDate  ");
                _tem.Columns.Add("ModifiedDate ");
                _tem.Columns.Add("CreatedBy    ");
                _tem.Columns.Add("ModifiedBy   ");
                _tem.Columns.Add("SortOrder    ");
                _tem.Columns.Add("Supplier   ");
                _tem.Columns.Add("ETDETA   ");
                _tem.Columns.Add("BookingNumber   ");
                _tem.Columns.Add("BillNumber   ");
                foreach (var _item in lstdetail)
                {
                    _tem.Rows.Add(
                            _item.RowGuid
                            , _item.RecordGuid
                            , _item.ItemID
                            , _item.ItemName
                            , _item.Title
                            , _item.UnitID
                            , _item.UnitName
                            , _item.Quantity
                            , _item.UnitPrice
                            , _item.AmountOc
                            , _item.VATPercent
                            , _item.VATAmountOC
                            , _item.AmountNoVAT
                            , _item.AmountVAT
                            , _item.Note
                            , _item.CreatedDate
                            , _item.ModifiedDate
                            , _item.CreatedBy
                            , _item.ModifiedBy
                            , _item.SortOrder
                              , _item.Supplier
                                , _item.ETDETA
                                , _item.BookingNumber
                                 , _item.BillNumber
                             );
                }
                _detail.Value = _tem;
                _detail.TypeName = "[Sale].[LogisticsDetails]";
                var _rs = _context.SQLCOMMANDS.FromSql(sql, RowGuid, OrganizationGuid, CodeID, DateSX, Title, EmployeeID, EmployeeName, ProductionManagerGuid, TotalMoney, Status, IsApprove, IsAccountant, IsManager, ConditionValue, WorkFlowGuid, WorkFlowsContent, WorkFlowCurrent, CreatedDate, ModifiedDate, CreatedBy, ModifiedBy, Temps, _detail).FirstOrDefault();
                if (_rs.Error == "")
                {
                    if (obj.Temps == "1")
                    {
                        msg.Title = "Thêm mới thành công";
                    }
                    else
                    {
                        msg.Title = "Cập nhật thành công";
                    }
                }
                else
                {
                    return Json(new JMessage() { Error = true, Title = _rs.Error, Object = _rs.Error });
                }
            }
            catch (Exception ex)
            {
                msg.Error = true;
                msg.Object = ex;
                msg.Title = ex.Message;
            }
            return Json(msg);
        }

        [HttpPost]
        public object Delete(Guid? Id)
        {
            var msg = new JMessage() { Error = false };
            try
            {
                string sql = "[Sale].[SP_ESSM_Logistics_Delete] @RowGuid";
                var RowGuid = SqlPara("@RowGuid", Id, SqlDbType.UniqueIdentifier);
                var __tem = _context.SQLCOMMANDS.FromSql(sql, RowGuid).FirstOrDefault();
                if (__tem.Error == "")
                {
                    return new JMessage() { Error = false, Title = "Xóa thành công." };
                }
                else
                {
                    return new JMessage() { Error = true, Title = __tem.Error, Object = __tem };
                }
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = ex.Message });
            }
        }
        public class objTitleFile
        {
            public int? STT { get; set; }
            public string Title { get; set; }
        }
        public class ObjAttach
        {
            public Guid RecordGuid { get; set; }
            public int? STT { get; set; }

        }
        [HttpPost]
        //[ActionAuthorize(EAction.EDIT)]
        public JsonResult UpdateFile()
        {
            var msg = new JMessage() { Error = false, Title = "Cập nhật thành công." };
            try
            {
                var lstTitlefile = Request.Form["lstTitlefile"];
                List<objTitleFile> lstTitle = JsonConvert.DeserializeObject<List<objTitleFile>>(lstTitlefile);
                var DeleteAttach = Request.Form["DeleteAttach"];
                var _emp = GetEmployeeLogin(_context);
                var files = Request.Form.Files.ToList();
                var _obj = Request.Form["model"];
                ObjAttach obj = JsonConvert.DeserializeObject<ObjAttach>(_obj);
                List<Guid> deleteFileattach = JsonConvert.DeserializeObject<List<Guid>>(DeleteAttach);

                if (deleteFileattach.Count > 0)
                {
                    foreach (var id in deleteFileattach)
                    {
                        string sqlAttach = "[HR].[SP_Delete_Attachment] @database, @scheme,@TableName,@OrganizationGuid,@AttachmentGuid,@Error out";
                        var database = new SqlParameter("@database ", SqlDbType.NVarChar, 250); CheckNullParameterStore(database, _appSettings.DBName_HR);
                        var scheme = new SqlParameter("@scheme ", SqlDbType.NVarChar, 250); CheckNullParameterStore(scheme, "Sale");
                        var OrganizationGuid = new SqlParameter("@OrganizationGuid ", SqlDbType.NVarChar); CheckNullParameterStore(OrganizationGuid, _emp.OrganizationGuid.ToString());
                        var TableName = new SqlParameter("@TableName ", SqlDbType.NVarChar, 50); CheckNullParameterStore(TableName, "ProductionManager_DOC");
                        var AttachGuid = new SqlParameter("@AttachmentGuid ", SqlDbType.NVarChar); CheckNullParameterStore(AttachGuid, id.ToString());
                        var Error = new SqlParameter("@Error ", SqlDbType.NVarChar) { Direction = ParameterDirection.Output }; CheckNullParameterStore(Error, "");
                        var temattach = _context.SQLCOMMANDS.FromSql(sqlAttach, database, scheme, TableName, OrganizationGuid, AttachGuid, Error).FirstOrDefault();
                        if (Error.Value.ToString() == "")
                        {
                            msg.Title = "Xóa đính kèm thành công.";
                        }
                        else
                        {
                            msg.Title = "Xóa đính kèm không thành công."; msg.Error = true;
                        }
                    }
                }
                if (files.Count > 0)
                {
                    string sqlAttach = "[Hr].[SP_Insert_Attachment_for_Calendar] @database, @scheme,@TableName,@TableAttachment";
                    var database = new SqlParameter("@database ", SqlDbType.NVarChar, 250); CheckNullParameterStore(database, _appSettings.DBName_HR);
                    var scheme = new SqlParameter("@scheme ", SqlDbType.NVarChar, 250); CheckNullParameterStore(scheme, "Sale");
                    var TableName = new SqlParameter("@TableName ", SqlDbType.NVarChar, 50); CheckNullParameterStore(TableName, "ProductionManager_DOC");
                    var _detailAttach = new SqlParameter("@TableAttachment", SqlDbType.Structured);
                    /// table detail
                    var _tematach = new DataTable();
                    _tematach.Columns.Add("AttachmentGuid");
                    _tematach.Columns.Add("ParentGuid");
                    _tematach.Columns.Add("RecordGuid");
                    _tematach.Columns.Add("OrganizationGuid");
                    _tematach.Columns.Add("DepartmentGuid");
                    _tematach.Columns.Add("ModuleID");
                    _tematach.Columns.Add("Title");
                    _tematach.Columns.Add("FileName");
                    _tematach.Columns.Add("Attachment", typeof(byte[]));
                    _tematach.Columns.Add("FileExtension");
                    _tematach.Columns.Add("FileSize");
                    _tematach.Columns.Add("UrlExternal");
                    _tematach.Columns.Add("IsPublic");
                    _tematach.Columns.Add("PrivateLevel");
                    _tematach.Columns.Add("IsFolder");
                    _tematach.Columns.Add("IsHidden");
                    _tematach.Columns.Add("IsReadonly");
                    _tematach.Columns.Add("IsSync");
                    _tematach.Columns.Add("SyncTime");
                    _tematach.Columns.Add("CloudPath");
                    _tematach.Columns.Add("CreatedDate");
                    _tematach.Columns.Add("ModifiedDate");
                    _tematach.Columns.Add("CreatedBy");
                    _tematach.Columns.Add("ModifiedBy");
                    for (var i = 0; i < files.Count; i++)
                    {
                        for (var j = 0; j < lstTitle.Count; j++)
                        {
                            if (i == lstTitle[j].STT)
                            {
                                var stream = files[i].OpenReadStream();
                                byte[] byteArr = Utilities.StreamToByteArray(stream);
                                var name = files[i].FileName;

                                ContractAttachments _attach = new ContractAttachments()
                                {
                                    AttachmentGuid = Guid.NewGuid(),
                                    RecordGuid = obj.RecordGuid,
                                    OrganizationGuid = _emp.OrganizationGuid,
                                    Title = lstTitle[j].Title,
                                    FileName = name,
                                    CreatedDate = DateTime.Now,
                                    CreatedBy = GetCreatedBy(_context),
                                    ModifiedDate = DateTime.Now,
                                    ModifiedBy = GetModifiedBy(_context),
                                    Attachment = byteArr,
                                    ModuleId = "HR",
                                    FileSize = byteArr.Length,
                                    FileExtension = System.IO.Path.GetExtension(files[i].FileName)
                                };
                                _tematach.Rows.Add(_attach.AttachmentGuid,
                                    _attach.ParentGuid,
                                    _attach.RecordGuid,
                                    _attach.OrganizationGuid,
                                    _attach.DepartmentGuid,
                                    _attach.ModuleId,
                                    _attach.Title,
                                    _attach.FileName,
                                    _attach.Attachment,
                                    _attach.FileExtension,
                                    _attach.FileSize,
                                    _attach.UrlExternal,
                                    _attach.IsPublic,
                                    _attach.PrivateLevel,
                                    _attach.IsFolder,
                                    _attach.IsHidden,
                                    _attach.IsReadonly,
                                    _attach.IsSync,
                                   DBNull.Value,
                                    _attach.CloudPath,
                                    DBNull.Value, DBNull.Value, _attach.CreatedBy, _attach.ModifiedBy);
                            }
                        }
                    }
                    _detailAttach.Value = _tematach;
                    _detailAttach.TypeName = "[dbo].[Attachment]";
                    var temattach = _context.SQLCOMMANDS.FromSql(sqlAttach, database, scheme, TableName, _detailAttach).FirstOrDefault();
                    if (temattach.Error == "")
                    {
                        msg.Title = "Thêm mới thành công.";
                    }
                    else
                    {
                        msg.Title = "Thêm mới không thành công."; msg.Error = true;
                    }
                }
            }
            catch (SqlException ex)
            {
                msg.Title = "Có lỗi khi đính kèm";
                msg.Object = ex;
                msg.Error = true;
            }
            return Json(msg);
        }
        [HttpGet]
        public FileResult DownloadItemAttachment(Guid? Id)
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var username = GetEmployeeLogin(_context);
                var gui = new Guid();
                string sql = "[dbo].[SP_Download_Attachment] @Database,@Scheme, @TableName, @OrganizationGuid, @AttachmentGuid, @Error";
                var Database = new SqlParameter("@Database", SqlDbType.VarChar); Database.Value = _appSettings.DBName_HR;
                var Scheme = new SqlParameter("@Scheme", SqlDbType.VarChar); Scheme.Value = "Sale";
                var TableName = new SqlParameter("@TableName", SqlDbType.VarChar); TableName.Value = "ProductionManager_DOC";
                var OrganizationGuid = new SqlParameter("@OrganizationGuid", SqlDbType.UniqueIdentifier); OrganizationGuid.Value = username.OrganizationGuid;
                var AttachmentGuid = new SqlParameter("@AttachmentGuid", SqlDbType.UniqueIdentifier); AttachmentGuid.Value = Id == gui ? (Object)DBNull.Value : Id;
                var Error = new SqlParameter("@Error", SqlDbType.NVarChar); Error.Value = "";
                var rs = _context.AttachmentsDownloadView_.FromSql(sql, Database, Scheme, TableName, OrganizationGuid, AttachmentGuid, Error).SingleOrDefault();
                if (rs != null)
                {
                    byte[] fileBytes = rs.Attachment;
                    var fileName = rs.FileName;
                    return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);

                }
                else return null;
            }
            catch (Exception ex)
            {
                msg.Title = "Có lỗi khi lấy dữ liệu.";
                msg.Object = ex;
                return null;
            }
        }
        [HttpPost]
        //  [Filters.ActionAuthorize(EAction.OPEN)]
        public JsonResult GetItemAttachment([FromBody] ContractAttachments obj)
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var username = GetEmployeeLogin(_context);
                string sql = "[HR].[SP_GET_Attachment_Calendar] @Database, @Scheme, @TableName, @OrganizationGuid, @RecordGuid, @Error";
                var database = new SqlParameter("@Database ", SqlDbType.NVarChar); CheckNullParameterStore(database, _appSettings.DBName_HR);
                var Scheme = new SqlParameter("@Scheme", SqlDbType.VarChar); Scheme.Value = "Sale";
                var TableName = new SqlParameter("@TableName", SqlDbType.VarChar); TableName.Value = "ProductionManager_DOC";
                var OrganizationGuid = SqlPara("@OrganizationGuid", username.OrganizationGuid.ToString(), SqlDbType.NVarChar);
                var RecordGuid = SqlPara("@RecordGuid", obj.RecordGuid.ToString(), SqlDbType.NVarChar);
                var Error = new SqlParameter("@Error", SqlDbType.NVarChar); Error.Value = "";
                var rs = _context.AttachmentsView.FromSql(sql, database, Scheme, TableName, OrganizationGuid, RecordGuid, Error).ToList();
                return Json(rs);
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Title = ex.Message, Object = ex });
            }
        }
        [HttpPost]
        public object JTable_Customers([FromBody] JTableSearch jTablePara)
        {
            var emp = GetEmployeeLogin(_context);
            var count = (from a in _context.Customers
                         where a.IsLeads == true
                         select new { a.CustomerGuid }).Count();

            int intBeginFor = (jTablePara.CurrentPage - 1) * jTablePara.Length;
            var balance = (from a in _context.Customers
                           where a.IsLeads == true
                           select new { a.CustomerGuid, a.CustomerId, a.CustomerName, a.Address, a.WorkPlace, a.ActivityFieldName, a.BusinessTypeName, a.LeadName, a.Method, a.Note }).OrderUsingSortExpression(jTablePara.QueryOrderBy).Skip(intBeginFor).Take(jTablePara.Length).ToList();
            var jdata = JTableHelper.JObjectTable(balance, jTablePara.Draw, count, "CustomerGuid", "CustomerId", "CustomerName", "Address", "WorkPlace", "ActivityFieldName", "BusinessTypeName", "LeadName", "Method", "Note");
            return Json(jdata);
        }
        [HttpPost]
        public object JTable_ExportDocuments([FromBody] JTableSearch jTablePara)
        {
            var emp = GetEmployeeLogin(_context);
            var count = (from a in _context.ExportDocuments
                         select new { a.RowGuid }).Count();

            int intBeginFor = (jTablePara.CurrentPage - 1) * jTablePara.Length;
            var balance = (from a in _context.ExportDocuments
                           select new { a.RowGuid, a.CodeID, a.Title, a.ExportMarket, a.ExportForm, a.ExportFile, a.Agencies, a.EmployeeName, a.Note }).OrderUsingSortExpression(jTablePara.QueryOrderBy).Skip(intBeginFor).Take(jTablePara.Length).ToList();
            var jdata = JTableHelper.JObjectTable(balance, jTablePara.Draw, count, "RowGuid", "CodeID", "Title", "ExportMarket", "ExportForm", "ExportFile", "Agencies", "EmployeeName", "Note");
            return Json(jdata);
        }
        [HttpPost]
        public JsonResult Submit_ExportDocuments()
        {

            var msg = new JMessage() { Error = false };
            try
            {
                var _emp = GetEmployeeLogin(_context);
                var files = Request.Form.Files.ToList();
                var submit = Request.Form["submit"];
                ExportDocuments obj = JsonConvert.DeserializeObject<ExportDocuments>(submit);
                string sql = "[Sale].[SP_ESSM_ExportDocuments_Insert] @RowGuid,@OrganizationGuid,@CodeID,@Title,@ExportMarket,@ExportForm,@ExportFile,@Agencies,@EmployeeName,@Note,@CreatedDate,@ModifiedDate,@CreatedBy,@ModifiedBy";
                obj.CreatedDate = DateTime.Now;
                obj.ModifiedDate = DateTime.Now;
                obj.CreatedBy = GetCreatedBy(_context);
                obj.ModifiedBy = GetCreatedBy(_context);
                var RowGuid = SqlPara("@RowGuid                     ", obj.RowGuid, SqlDbType.UniqueIdentifier);
                var OrganizationGuid = SqlPara("@OrganizationGuid            ", obj.OrganizationGuid, SqlDbType.UniqueIdentifier);
                var CodeID = SqlPara("@CodeID                      ", obj.CodeID, SqlDbType.NVarChar);
                var Title = SqlPara("@Title                      ", obj.Title, SqlDbType.NVarChar);
                var ExportMarket = SqlPara("@ExportMarket                       ", obj.ExportMarket, SqlDbType.NVarChar);
                var ExportForm = SqlPara("@ExportForm                  ", obj.ExportForm, SqlDbType.NVarChar);
                var ExportFile = SqlPara("@ExportFile                ", obj.ExportFile, SqlDbType.NVarChar);
                var Agencies = SqlPara("@Agencies       ", obj.Agencies, SqlDbType.NVarChar);
                var EmployeeName = SqlPara("@EmployeeName                  ", obj.EmployeeName, SqlDbType.NVarChar);

                var Note = SqlPara("@Note             ", obj.Note, SqlDbType.NVarChar);
                var CreatedDate = SqlPara("@CreatedDate                 ", obj.CreatedDate, SqlDbType.DateTime);
                var ModifiedDate = SqlPara("@ModifiedDate                ", obj.ModifiedDate, SqlDbType.DateTime);
                var CreatedBy = SqlPara("@CreatedBy                   ", obj.CreatedBy, SqlDbType.NVarChar);
                var ModifiedBy = SqlPara("@ModifiedBy                  ", obj.ModifiedBy, SqlDbType.NVarChar);


                var _rs = _context.SQLCOMMANDS.FromSql(sql, RowGuid, OrganizationGuid, CodeID, Title, ExportMarket, ExportForm, ExportFile, Agencies, EmployeeName, Note, CreatedDate, ModifiedDate, CreatedBy, ModifiedBy).FirstOrDefault();
                if (_rs.Error == "")
                {
                    msg.Title = "Thêm mới thành công";
                }
                else
                {
                    return Json(new JMessage() { Error = true, Title = _rs.Error, Object = _rs.Error });
                }
            }
            catch (Exception ex)
            {
                msg.Error = true;
                msg.Object = ex;
                msg.Title = ex.Message;
            }
            return Json(msg);
        }
        [Area("Sale")]
        public object BookingNumber(string q)
        {
            try
            {
                var lstItem = new List<Object_Jexcel>();
                var _obj = new Object_Jexcel() { id = "C", name = "Chuyển khoản" };
                var _obj1 = new Object_Jexcel() { id = "T", name = "Tiền mặt" };

                lstItem.Add(_obj); lstItem.Add(_obj1);
                return Json(lstItem);
            }
            catch (Exception ex)
            {
                return Json(new JMessage { Title = "Lấy dữ liệu không thành công", Object = ex, Error = true });
            }
        }
        [HttpPost]
        public JsonResult GetItem_ExportDocuments(Guid Id)
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var _emp = GetEmployeeLogin(_context);
                var rs = _context.ExportDocuments.Where(x => x.RowGuid == Id).FirstOrDefault();
                return Json(rs);
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = ex.Message });
            }
        }
        [HttpPost]
        public object Delete_ExportDocuments(Guid Id)
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var _emp = GetEmployeeLogin(_context);
                var _obj = _context.ExportDocuments.Where(x => x.RowGuid == Id).FirstOrDefault();
                _context.ExportDocuments.Remove(_obj);
                _context.SaveChanges();
                return Json(new JMessage() { Error = false, Title = "Xóa thành công" });
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = ex.Message });
            }
        }
    }

}
