﻿using FTU.Utils.HelperNet;
using HRM.Controllers;
using HRM.Models;
using HRM.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;
using System.Data;
using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Newtonsoft.Json;
using Grpc.Core;
using DocumentFormat.OpenXml.Drawing;
using ES_MODEL.HR.Custom.Wf;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using DocumentFormat.OpenXml.Drawing.Charts;
using ES_MODEL.Task;
using HRM.Filters;
using ES_MODEL.HR;
using Aspose.Pdf;
using DocumentFormat.OpenXml.Bibliography;
using DocumentFormat.OpenXml.Wordprocessing;
using Microsoft.CodeAnalysis;
using DocumentFormat.OpenXml.VariantTypes;
using static Aspose.Pdf.Operator;
using System.Threading;
using HRM.Models.Custom.Hr;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Math;
using System.Web;
using DocumentFormat.OpenXml.Drawing.Spreadsheet;
using DocumentFormat.OpenXml.ExtendedProperties;
using OfficeOpenXml.FormulaParsing.Excel.Functions.DateTime;
using static Stimulsoft.Base.Drawing.Win32;

namespace HRM.Areas.Sale.Controllers
{
    [Authorize]
    public class RatingTasksController : BaseController
    {
        private readonly HRMDBContext _context;
        private readonly IHostingEnvironment environment;
        private readonly AppSettings _appSettings;
        public RatingTasksController(HRMDBContext _db, IHostingEnvironment _environment, IOptions<AppSettings> appSettings)
        {
            _context = _db;
            environment = _environment;
            _appSettings = appSettings.Value;
        }

        [Area("Sale")]
        [ActionAuthorize(EAction.ACCESS)]
        public IActionResult Index()
        {
            return View();
        }
        public class DataJtable : JTableModel
        {
            public string Keyword { get; set; }
            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }
            public Guid? DepartmentGuid { get; set; }
            public string StatusCompleted { get; set; }
        }
        [HttpPost]
        public object JTable([FromBody] DataJtable jTablePara)
        {
            try
            {
                {
                    var s = GetEmployeeLogin(_context);
                    if (jTablePara.StartDate != null)
                    {
                        jTablePara.StartDate = jTablePara.StartDate.Value.ToLocalTime();
                    }
                    else
                    {
                        jTablePara.StartDate = null;
                    }
                    if (jTablePara.EndDate != null)
                    {
                        jTablePara.EndDate = jTablePara.EndDate.Value.ToLocalTime();
                    }
                    else
                    {
                        jTablePara.EndDate = null;
                    }
                    int intBeginFor = (jTablePara.CurrentPage - 1) * jTablePara.Length;
                    string sql = "[dbo].[SP_ESHR_RatingTasks_JTable]" +
                    "@Keyword, " +
                    "@StartDate, " +
                    "@EndDate, " +
                     "@StatusCompleted, " +
                    "@Sort, " +
                    "@Skip, " +
                    "@Take," +
                    "@Count out ";
                    var _Keyword = new SqlParameter("@Keyword", SqlDbType.NVarChar); CheckNullParameterStore(_Keyword, jTablePara.Keyword);
                    var StartDate = new SqlParameter("@StartDate", SqlDbType.Date); CheckNullParameterStore(StartDate, jTablePara.StartDate);
                    var EndDate = new SqlParameter("@EndDate", SqlDbType.Date); CheckNullParameterStore(EndDate, jTablePara.EndDate);
                    var StatusCompleted = new SqlParameter("@StatusCompleted", SqlDbType.VarChar); CheckNullParameterStore(StatusCompleted, jTablePara.StatusCompleted);
                    var _Sort = new SqlParameter("@Sort", SqlDbType.VarChar); CheckNullParameterStore(_Sort, jTablePara.QueryOrderBy);
                    var _Skip = new SqlParameter("@Skip", SqlDbType.Int); CheckNullParameterStore(_Skip, intBeginFor);
                    var _Take = new SqlParameter("@Take", SqlDbType.Int); CheckNullParameterStore(_Take, jTablePara.Length);
                    var _output = new SqlParameter("@Count", SqlDbType.Int) { Direction = System.Data.ParameterDirection.Output };
                    var data = _context.RatingTasksView.FromSql(sql,
                        _Keyword,
                        StartDate,
                        EndDate,
                        StatusCompleted,
                        _Sort,
                        _Skip,
                        _Take,
                        _output).ToList();
                    var jdata = JTableHelper.JObjectTable(data, jTablePara.Draw, (int)_output.Value);
                    return Json(jdata);
                }

            }
            catch (Exception ex)
            {
                return Json(ex);
            }
        }

        [HttpPost]
        public object GetBieuDo([FromBody] DataJtable jTablePara)
        {
            try
            {
                {
                    var s = GetEmployeeLogin(_context);
                    if (jTablePara.StartDate != null)
                    {
                        jTablePara.StartDate = jTablePara.StartDate.Value.ToLocalTime();
                    }
                    else
                    {
                        jTablePara.StartDate = null;
                    }
                    if (jTablePara.EndDate != null)
                    {
                        jTablePara.EndDate = jTablePara.EndDate.Value.ToLocalTime();
                    }
                    else
                    {
                        jTablePara.EndDate = null;
                    }
                    string sql = "[dbo].[SP_ESHR_RatingTasks_BieuDo]" +
                    "@Keyword, " +
                    "@StartDate, " +
                    "@EndDate, " +
                     "@StatusCompleted";
                    var _Keyword = new SqlParameter("@Keyword", SqlDbType.NVarChar); CheckNullParameterStore(_Keyword, jTablePara.Keyword);
                    var StartDate = new SqlParameter("@StartDate", SqlDbType.Date); CheckNullParameterStore(StartDate, jTablePara.StartDate);
                    var EndDate = new SqlParameter("@EndDate", SqlDbType.Date); CheckNullParameterStore(EndDate, jTablePara.EndDate);
                    var StatusCompleted = new SqlParameter("@StatusCompleted", SqlDbType.VarChar); CheckNullParameterStore(StatusCompleted, jTablePara.StatusCompleted);
                    var data = _context.RatingTasks_BieuDo.FromSql(sql,
                        _Keyword,
                        StartDate,
                        EndDate,
                        StatusCompleted
                        ).ToList();
                    return Json(data);
                }
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Title = ex.Message, Object = ex });
            }
        }

        [HttpPost]
        public JsonResult GetById(Guid? Id)
        {

            var msg = new JMessage() { Error = false };
            try
            {
                var rs = _context.Tasks.Where(x => x.TaskGuid == Id).FirstOrDefault();
                return Json(rs);
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Object = ex, Title = ex.Message });
            }
        }
        [HttpPost]
        public object Update()
        {
            try
            {
                var _emp = GetEmployeeLogin(_context);
                var _obj = Request.Form["edit"];
                Tasks obj = JsonConvert.DeserializeObject<Tasks>(_obj);
                string sql = "exec [Cooperation].[SP_ESHR_RatingTasks_Update]" +
                    "@TaskGuid," +
                    "@Approval";
                var TaskGuid = SqlPara("@TaskGuid", obj.TaskGuid, SqlDbType.UniqueIdentifier);
                var Approval = SqlPara("@Approval", obj.Approval, SqlDbType.VarChar);
                var _rs = _context.SQLCOMMANDS.FromSql(sql, TaskGuid, Approval).FirstOrDefault();
                if (_rs.Error == "")
                {
                    return Json(new JMessage() { Error = false, Title = "Cập nhật thành công" });
                }
                else
                {
                    return Json(new JMessage() { Error = true, Title = _rs.Error, Object = _rs.Error });
                }
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Title = ex.Message, Object = ex });
            }
        }

    }
}
