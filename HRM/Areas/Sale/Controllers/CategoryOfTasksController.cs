﻿using FTU.Utils.HelperNet;
using HRM.Controllers;
using HRM.Models;
using HRM.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;
using System.Data;
using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Newtonsoft.Json;
using Grpc.Core;
using DocumentFormat.OpenXml.Drawing;
using ES_MODEL.HR.Custom.Wf;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using DocumentFormat.OpenXml.Drawing.Charts;
using ES_MODEL.Task;
using HRM.Filters;
using ES_MODEL.HR;
using Aspose.Pdf;
using DocumentFormat.OpenXml.Bibliography;
using DocumentFormat.OpenXml.Wordprocessing;
using Microsoft.CodeAnalysis;
using DocumentFormat.OpenXml.VariantTypes;
using static Aspose.Pdf.Operator;
using System.Threading;
using HRM.Models.Custom.Hr;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Math;
using System.Web;

namespace HRM.Areas.Sale.Controllers
{
    /// <summary>
    /// Danh mục công việc
    /// </summary>
    [Authorize]
    public class CategoryOfTasksController : BaseController
    {
        private readonly HRMDBContext _context;
        private readonly AppSettings _appSettings;
        public CategoryOfTasksController(IOptions<AppSettings> appSettings, HRMDBContext context)
        {
            _context = context;
            _appSettings = appSettings.Value;
        }
        [Area("Sale")]
        [ActionAuthorize(EAction.ACCESS)]
        public IActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// Cập nhật trang thái cho các danh mục con
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public object UpdateIsActive([FromBody] CategoryOfTasks obj)
        {
            var msg = new JMessage() { Error = false };
            try
            {
                List<CategoryOfTasks> listdata = new List<CategoryOfTasks>();
                if (obj.CategoryOfTaskGuid != null)
                {

                    var data = _context.CategoryOfTasks
                                    .OrderBy(x => x.CreatedDate)
                                    .Where(x => x.ParentId == obj.CategoryOfTaskGuid)
                                    .ToList();
                    foreach (var item in data)
                    {
                        listdata.Add(item);
                    }
                    var data1 = GetLitData(data);
                    foreach (var item in data1)
                    {
                        listdata.Add(item);
                    }
                }
                if (listdata.Count > 0)
                {
                    foreach (var i in listdata.ToList())
                    {
                        var objj = _context.CategoryOfTasks
                                        .First(m => m.CategoryOfTaskGuid == i.CategoryOfTaskGuid);
                        objj.IsActive = obj.IsActive;
                        _context.CategoryOfTasks.Update(objj);
                        _context.Entry(objj).Property(x => x.IsActive).IsModified = false;
                        _context.SaveChanges();
                    }
                }
                msg.Title = "Cập nhật thành công";

            }
            catch (Exception ex)
            {
                msg.Error = true;
                msg.Title = "Có lỗi khi cập nhật";
            }
            return msg;

        }
        /// <summary>
        /// Xóa các danh mục con
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public object DeleteIsActive([FromBody] CategoryOfTasks obj)
        {
            var msg = new JMessage() { Error = false };
            try
            {
                List<CategoryOfTasks> listdata = new List<CategoryOfTasks>();

                if (obj.CategoryOfTaskGuid != null)
                {

                    var data = _context.CategoryOfTasks.OrderBy(x => x.CategoryOfTaskGuid)
                                    .Where(x => x.ParentId == obj.CategoryOfTaskGuid)
                                    .ToList();
                    foreach (var item in data)
                    {
                        listdata.Add(item);
                    }
                    var data1 = GetLitData(data);
                    foreach (var item in data1)
                    {
                        listdata.Add(item);
                    }
                }
                if (listdata.Count > 0)
                {
                    foreach (var i in listdata.ToList())
                    {
                        var objj = _context.CategoryOfTasks
                            .First(m => m.CategoryOfTaskGuid == i.CategoryOfTaskGuid);
                        _context.CategoryOfTasks.Remove(objj);
                        _context.Entry(objj).Property(x => x.CategoryOfTaskGuid).IsModified = false;
                        _context.SaveChanges();
                    }
                }
                msg.Title = "Xoá thành công";

            }
            catch (Exception ex)
            {
                msg.Error = true;
                msg.Title = "Có lỗi khi xóa";
            }
            return msg;

        }
        List<CategoryOfTasks> list = new List<CategoryOfTasks>();
        private List<CategoryOfTasks> GetLitData(List<CategoryOfTasks> data)
        {
            foreach (var item in data)
            {
                var data1 = _context.CategoryOfTasks.OrderBy(x => x.CategoryOfTaskGuid)
                                    .Where(x => x.ParentId == item.CategoryOfTaskGuid)
                                    .ToList();
                foreach (var item1 in data1)
                {
                    list.Add(item1);
                }
                if (data1.Count > 0)
                {
                    GetLitData(data1);
                }
            }
            return list;
        }
        public class GetAllCustom
        {
            public Guid? CategoryOfTaskGuid { get; set; }
            public string Keyword { get; set; }
        }
        /// <summary>
        /// Lấy danh sách tất cả các danh mục công việc
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public object GetAll([FromBody] GetAllCustom obj)
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var _emp = GetEmployeeLogin(_context);
                List<CategoryOfTasks> listdata = new List<CategoryOfTasks>();
                var rs = _context.CategoryOfTasks
                                .Where(x => x.Title.Contains(obj.Keyword)
                                    && x.OrganizationGuid == _emp.OrganizationGuid
                                )
                                .OrderBy(x => x.CreatedDate)
                                .ToList();
                return rs;
            }
            catch (Exception ex)
            {
                msg.Error = true;
                msg.Title = ex.Message;
            }
            return msg;
        }
        List<CategoryOfTasks> lists = new List<CategoryOfTasks>();
        private List<CategoryOfTasks> GetLitDatas(List<CategoryOfTasks> data)
        {
            foreach (var item in data)
            {
                var data1 = _context.CategoryOfTasks.OrderBy(x => x.CreatedDate)
                                .Where(x => x.ParentId == item.CategoryOfTaskGuid)
                                .ToList();
                foreach (var item1 in data1)
                {
                    lists.Add(item1);
                }
                if (data1.Count > 0)
                {
                    GetLitDatas(data1);
                }
            }
            return lists;
        }
        /// <summary>
        /// Thêm mới danh mục công việc
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public object Insert([FromBody] CategoryOfTasks obj)
        {
            var msg = new JMessage() { Error = false };
            try
            {
                obj.CreatedDate = DateTime.Now;
                obj.ModifiedDate = DateTime.Now;
                var _emp = GetEmployeeLogin(_context);
                obj.CreatedBy = _emp.LoginName + "#" + _emp.FullName;
                obj.OrganizationGuid = _emp.OrganizationGuid;
                obj.ModifiedBy = _emp.LoginName + "#" + _emp.FullName;
                obj.LoginName = _emp.LoginName;
                if (obj.CategoryOfProcess != null && obj.CategoryOfProcess.Count() > 0)
                {
                    foreach (var item in obj.CategoryOfProcess)
                    {
                        item.TaskProcessGu.CreatedBy = _emp.LoginName + "#" + _emp.FullName;
                        item.TaskProcessGu.ModifiedBy = _emp.LoginName + "#" + _emp.FullName;
                        item.TaskProcessGu.CreatedDate = DateTime.Now;
                        item.TaskProcessGu.ModifiedDate = DateTime.Now;
                        item.TaskProcessGu.OrganizationGuid = _emp.OrganizationGuid;
                    }
                }
                if (obj.CategoryShare != null && obj.CategoryShare.Count() > 0)
                {
                    foreach (var _item in obj.CategoryShare)
                    {
                        _item.OrganizationGuid = _emp.OrganizationGuid;
                    }
                }
                if (obj.TaskStepBySteps != null)
                {
                    foreach (var item in obj.TaskStepBySteps)
                    {
                        item.OrganizationGuid = _emp.OrganizationGuid;
                        item.CreatedBy = _emp.LoginName + "#" + _emp.FullName;
                        item.ModifiedBy = _emp.LoginName + "#" + _emp.FullName;
                    }
                }
                _context.CategoryOfTasks.Add(obj);
                var a = _context.SaveChanges();
                msg.Title = "Thêm mới thành công";
            }
            catch (Exception ex)
            {
                msg.Error = true;
                msg.Title = "Có lỗi khi thêm mới";
            }
            return Json(msg);
        }
        /// <summary>
        /// Cập nhật danh mục công việc
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public object Update([FromBody] CategoryOfTasks obj)
        {
            _context.Database.BeginTransaction();
            try
            {
                var _CateShare = _context.CategoryShare
                        .Where(x => x.CategoryOfTaskGuid == obj.CategoryOfTaskGuid)
                        .ToList();
                _context.RemoveRange(_CateShare);
                _context.SaveChanges();

                var _CateTaskStep = _context.TaskStepBySteps
                        .Where(x => x.CategoryOfTaskGuid == obj.CategoryOfTaskGuid)
                        .ToList();
                _context.RemoveRange(_CateTaskStep);
                _context.SaveChanges();

                var _emp = GetEmployeeLogin(_context);
                var _processOld = _context.CategoryOfProcess
                        .Where(x => x.CategoryOfTaskGuid == obj.CategoryOfTaskGuid)
                        .ToList();
                if (obj.CategoryShare != null && obj.CategoryShare.Count() > 0)
                {
                    foreach (var _item in obj.CategoryShare)
                    {
                        _item.OrganizationGuid = _emp.OrganizationGuid;
                    }
                }

                _context.CategoryOfProcess.RemoveRange(_processOld);
                _context.SaveChanges();

                _context.Attach(obj).State = EntityState.Modified;
                obj.ModifiedBy = _emp.LoginName + "#" + _emp.FullName;
                obj.ModifiedDate = DateTime.Now;
                _context.Entry(obj).Property(x => x.CategoryOfTaskGuid).IsModified = false;
                _context.Entry(obj).Property(x => x.OrganizationGuid).IsModified = false;
                _context.Entry(obj).Property(x => x.CreatedBy).IsModified = false;
                _context.Entry(obj).Property(x => x.CreatedDate).IsModified = false;
                if (obj.TaskStepBySteps != null)
                {
                    foreach (var item in obj.TaskStepBySteps)
                    {
                        item.OrganizationGuid = _emp.OrganizationGuid;
                        item.CreatedBy = _emp.LoginName + "#" + _emp.FullName;
                        item.ModifiedBy = _emp.LoginName + "#" + _emp.FullName;
                    }
                }
                var a = _context.SaveChanges();
                _context.Database.CommitTransaction();
                return new JMessage() { Error = false, Title = "Cập nhật thành công.", };
            }
            catch (Exception ex)
            {
                _context.Database.RollbackTransaction();
                return new JMessage() { Error = true, Title = "Cập nhật không thành công.", Object = ex.ToString() };
            }
        }
        /// <summary>
        /// Xóa danh mục công việc
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public object Delete([FromBody] TempSub obj)
        {
            var msg = new JMessage() { Error = false, Title = "Xóa bản ghi thành công" };
            try
            {
                Guid _guid = new Guid(obj.IdS[0]);
                var _CateTaskStep = _context.TaskStepBySteps
                     .Where(x => x.CategoryOfTaskGuid == _guid
                        && x.TaskStepStatus.Count() > 0
                     )
                     .ToList();
                if (_CateTaskStep != null && _CateTaskStep.Count() > 0)
                {
                    msg.Error = true;
                    msg.Title = "Bản ghi đang được sử dụng tại công việc, không thể xóa";
                }
                else
                {
                    var _count_task = _context.CategoryOfTasks
                        .Where(x => x.CategoryOfTaskGuid == _guid)
                        .AsNoTracking()
                            .Select(x => x.Tasks.Count)
                            .FirstOrDefault();
                    if (_count_task > 0)
                    {
                        msg.Title = "Bản ghi đang được sử dụng tại công việc, không thể xóa";
                        msg.Error = true;
                    }
                    else
                    {
                        var _count_Category = _context.CategoryOfTasks
                             .Where(x => x.CategoryOfTaskGuid == _guid)
                            .AsNoTracking()
                           .Select(x => x.InverseParent.Count)
                           .FirstOrDefault();
                        if (_count_Category > 0)
                        {
                            msg.Title = "Bản ghi đang được sử dụng bởi nhóm công việc khác, không thể xóa";
                            msg.Error = true;
                        }
                        else
                        {
                            var _obj = _context.CategoryOfTasks.First(m => m.CategoryOfTaskGuid == _guid);
                            _context.CategoryOfTasks.Remove(_obj);
                            _context.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                msg.Error = true;
                msg.Title = "Xóa bản ghi không thành công";
                msg.Object = ex.ToString();
            }
            return msg;
        }
        /// <summary>
        /// Lấy chi tiết danh mục công việc
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public object GetItem([FromBody] TempSub obj)
        {
            try
            {
                Guid _guid = new Guid(obj.IdS[0]);
                var _rs = _context.CategoryOfTasks
                                .Where(x => x.CategoryOfTaskGuid == _guid)
                                .Select(x => new
                                {
                                    x.Title,
                                    x.CategoryOfTaskGuid,
                                    x.ParentId,
                                    ParentIdName = ((x.Parent != null) ? x.Parent.Title : ""),
                                    x.DepartmentGuid,
                                    x.DepartmentId,
                                    x.DepartmentName,
                                    x.EmployeeGuid,
                                    x.EmployeeId,
                                    x.EmployeeName,
                                    x.IsActive,
                                    x.IsPublic,
                                    x.TaskLevel,
                                    x.Alias,
                                    x.CreatedBy,
                                    x.CreatedDate,
                                    x.ModifiedBy,
                                    x.ModifiedDate,
                                    x.Mark,
                                    x.Class,
                                    x.LoginName,
                                    x.IsDefault,
                                    x.OrderId,
                                    x.ClassText,
                                    CategoryOfProcess = x.CategoryOfProcess
                                        .Select(c => new
                                        {
                                            c.TaskProcessGuid,
                                            c.TaskProcessGu.Title,
                                            c.TaskProcessGu.Class,
                                            c.OrderId
                                        })
                                        .OrderBy(c => c.OrderId)
                                        .ToList(),
                                    DataCategoryShares = x.CategoryShare
                                        .Select(c => new
                                        {
                                            c.DepartmentId,
                                            c.DepartmentName
                                        }).ToList(),
                                    x.TypeStep,
                                    x.Type,
                                    TaskStepBySteps = x.TaskStepBySteps
                                        .Select(c => new
                                        {
                                            c.RecordGuid,
                                            c.Sort,
                                            c.Guid,
                                            c.Code,
                                            c.Name,
                                            c.ColorBackground,
                                            c.ColorText
                                        })
                                        .OrderBy(c => c.Sort)
                                        .ToList(),
                                })
                                .FirstOrDefault();
                return _rs;
            }
            catch (Exception ex)
            {
                return new JMessage() { Error = true, Title = "Lấy thông tin không thành công.", Object = ex.ToString() };
            }
        }
        /// <summary>
        /// Lấy nhân viên theo mã nhân viên
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public object GetEmployeeItem([FromBody] TempSub obj)
        {
            try
            {
                var _d = _context.Employees.Where(x => x.EmployeeId == obj.IdS[0]).FirstOrDefault();
                return new JMessage() { Error = false, Title = "Lấy dữ liệu thành công.", Object = _d };
            }
            catch (Exception ex)
            {
                return new JMessage() { Error = true, Title = "Có lỗi xảy ra trong quá trình lấy dữ liệu.", Object = ex };
            }
        }
        /// <summary>
        /// Lấy danh sách phòng ban
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public object GetDepart()
        {
            try
            {
                var _d = _context.Departments.Where(x => x.IsActive == true).ToList();
                return new JMessage() { Error = false, Title = "Lấy dữ liệu thành công.", Object = _d };
            }
            catch (Exception ex)
            {
                return new JMessage() { Error = true, Title = ex.Message, Object = ex };
            }
        }
        /// <summary>
        /// Lấy phòng ban theo mã
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        [HttpPost]
        public object GetDepartItem([FromBody] TempSub guid)
        {
            try
            {
                var _d = _context.Departments.Where(x => x.DepartmentGuid.ToString() == guid.IdS[0]).FirstOrDefault();
                return new JMessage() { Error = false, Title = "Lấy dữ liệu thành công.", Object = _d };
            }
            catch (Exception ex)
            {
                return new JMessage() { Error = true, Title = "Có lỗi xảy ra trong quá trình lấy dữ liệu.", Object = ex };
            }
        }
        /// <summary>
        /// Danh sách nhân viên được lấy theo bộ phận
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        [HttpPost]
        public object GetEmployee([FromBody] TempSub o)
        {
            try
            {
                var _d = _context.Employees.Where(x => x.DepartmentGuid.ToString() == o.IdS[0]).ToList();
                return new JMessage() { Error = false, Title = "Lấy dữ liệu thành công.", Object = _d };
            }
            catch (Exception ex)
            {
                return new JMessage() { Error = true, Title = ex.Message, Object = ex };
            }
        }
        public class TreeView
        {
            public string Title { get; set; }
            public Guid Id { get; set; }
            public bool HasChild { get; set; }
        }
        /// <summary>
        /// Lấy danh sách danh mục công việc dưới dạng phân cấp
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<List<TreeView>> GetTreeData(Guid? id)
        {
            var _emp = GetEmployeeLogin(_context);
            var data = _context.CategoryOfTasks
                            .Where(x => (x.DepartmentGuid == _emp.DepartmentGuid
                                ||
                                x.IsPublic == true)
                                && x.OrganizationGuid == _emp.OrganizationGuid
                            )
                            .OrderBy(x => x.CreatedDate)
                            .Where(x => ((x.CategoryOfTaskGuid != id && x.ParentId != id)
                                        || id == null)
                                        && x.IsActive == true
                                        && x.OrganizationGuid == _emp.OrganizationGuid
                                        //&& x.DepartmentId.ToUpper() == _emp.DepartmentId.ToUpper()
                            )
                            .ToList();
            var dataOrder = GetSubTreeData(data, null, new List<TreeView>(), "");
            return dataOrder;
        }
        private List<TreeView> GetSubTreeData(List<CategoryOfTasks> data, Guid? parentid, List<TreeView> lstCategories, string tab)
        {
            tab += "- ";
            var contents = parentid == null
                ? data.Where(x => x.ParentId == null).ToList()
                : data.Where(x => x.ParentId == parentid).ToList();
            foreach (var item in contents)
            {
                var category = new TreeView
                {
                    Id = item.CategoryOfTaskGuid,
                    Title = tab + item.Title,
                    HasChild = data.Any(x => x.ParentId == item.CategoryOfTaskGuid)
                };
                lstCategories.Add(category);
                if (category.HasChild) GetSubTreeData(data, item.CategoryOfTaskGuid, lstCategories, tab);
            }
            return lstCategories;
        }
        /// <summary>
        /// Lấy danh mục theo mã danh mục cha
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public object GetByParent(Guid? id)
        {
            var temp = Convert.ToInt32(id);
            return Json(_context.CategoryOfTasks.Where(x => (x.ParentId == id) || (temp == 0 && x.ParentId == null))
                .OrderBy(x => x.CreatedDate)
                .ToList());
        }
        /// <summary>
        /// Cập nhật trạng thái khi thay đổi trạng thái danh mục cha
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public object CheckStatus(string id)
        {
            Guid _guid = new Guid(id);
            var temp = Convert.ToInt32(id);
            return Json(_context.CategoryOfTasks.Where(x => x.CategoryOfTaskGuid == _guid && x.IsActive == false)
                .OrderBy(x => x.CategoryOfTaskGuid)
                .ToList());
        }
    }
}
