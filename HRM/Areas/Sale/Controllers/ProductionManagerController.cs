﻿using FTU.Utils.HelperNet;
using HRM.Controllers;
using HRM.Models;
using HRM.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;
using System.Data;
using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Newtonsoft.Json;
using Grpc.Core;
using DocumentFormat.OpenXml.Drawing;
using ES_MODEL.HR.Custom.Wf;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using DocumentFormat.OpenXml.Drawing.Charts;
using ES_MODEL.HR;
using HRM.Models.Custom.Hr;
using DataTable = System.Data.DataTable;
using OfficeOpenXml.Style.XmlAccess;

namespace HRM.Areas.Sale.Controllers
{
    [Authorize]
    public class ProductionManagerController : BaseController
    {
        public IConfigurationRoot Configuration { get; }
        private readonly HRMDBContext _context;
        private readonly IHostingEnvironment _environment;
        private readonly AppSettings _appSettings;

        public ProductionManagerController(IOptions<AppSettings> appSettings, HRMDBContext context, IHostingEnvironment environment)
        {
            _context = context;
            _environment = environment;
            _appSettings = appSettings.Value;
        }
        [Area("Sale")]
        public IActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public object GetPicfile(Guid? id)
        {
            try
            {
                var Photo = _context.ProductionManagerComments_DOC.Where(x => x.AttachmentGuid == id).Select(x => x.Attachment).FirstOrDefault();
                byte[] ImageTemp = Photo;

                FileContentResult hd = null;
                hd = File(ImageTemp, "image/jpeg");
                int gh = ImageTemp.Count();
                if (gh > 0)
                {
                    return hd;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }
        [HttpPost]
        public object getCustomer_GetAll()
        {
            try
            {
                var _emp = GetEmployeeLogin(_context);
                string sql = "[Sale].[SP_ESHR_Customer_GetAll] @OrganizationGuid,@IsProvider,@IsCustomer";
                var OrganizationGuid = SqlPara("@OrganizationGuid", _emp.OrganizationGuid, SqlDbType.UniqueIdentifier);
                var IsProvider = SqlPara("@IsProvider", false, SqlDbType.Bit);
                var IsCustomer = SqlPara("@IsCustomer", true, SqlDbType.Bit);
                var rs = _context.Object_Combobox.FromSql(sql, OrganizationGuid, IsProvider, IsCustomer).ToList();
                return rs;
            }
            catch (Exception ex)
            {
                return new JMessage() { Error = true, Title = ex.Message, Object = ex };
            }
        }

        [HttpPost]
        public object JTable([FromBody] JTableSearch jTable)
        {
            var msg = new JMessage() { Error = false };
            var _emp = GetEmployeeLogin(_context);
            try
            {
                int intBeginFor = (jTable.CurrentPage - 1) * jTable.Length;
                string sql = "[Sale].[SP_ESHR_ProductionManager_JTable] " +
                   "@Keyword, " +
                   "@lstDepartmentId," +
                   "@StartDate," +
                    "@EndDate," +
                   "@Type," +
                    "@LoginName," +
                   "@Permisstion," +
                   "@Sort, " +
                   "@Skip, " +
                   "@Take," +
                   "@Count out";
                var Keyword = SqlPara("@Keyword ", jTable.Keyword, SqlDbType.NVarChar);
                var lstDepartmentId = new SqlParameter("@lstDepartmentId", jTable.lstString.Count >= 1 ? string.Join(",", jTable.lstString) : "");
                var StartDate = SqlPara("@StartDate ", jTable.StartDate, SqlDbType.Date);
                var EndDate = SqlPara("@EndDate ", jTable.EndDate, SqlDbType.Date);
                var Type = SqlPara("@Type ", jTable.Type, SqlDbType.VarChar);
                var LoginName = SqlPara("@LoginName ", _emp.LoginName, SqlDbType.NVarChar);
                //var Permisstion = 0;
                //var ListPermission = System.Web.WebContext.CheckPermission(ControllerContext.RouteData.Values["controller"].ToString());
                //var per = ListPermission.Contains(EAction.FULLCONTROL.ToString());
                //if (per == true)
                //{
                //    Permisstion = 1;
                //}
                var Permisstion = SqlPara("@Permisstion ", jTable.Permisstion, SqlDbType.VarChar);
                var Sort = SqlPara("@Sort", jTable.QueryOrderBy, SqlDbType.VarChar);
                var Skip = SqlPara("@Skip", intBeginFor, SqlDbType.Int);
                var Take = SqlPara("@Take ", jTable.Length, SqlDbType.Int);
                var Count = new SqlParameter("@Count", SqlDbType.Int) { Direction = System.Data.ParameterDirection.Output };
                var data = _context.ProductionManagerViews.FromSql(sql, Keyword, lstDepartmentId, StartDate, EndDate, Type, LoginName, Permisstion, Sort, Skip, Take, Count).ToList();
                var jdata = JTableHelper.JObjectTable(data, jTable.Draw, (int)Count.Value);
                return Json(jdata);
            }
            catch (Exception ex)
            {
                return new JMessage() { Error = true, Title = ex.Message, Object = ex };
            }
        }
        [Area("Sale")]
        public object FactoryType(string q)
        {
            try
            {
                var lstItem = new List<Object_Jexcel>();
                var _obj = new Object_Jexcel() { id = "EXW", name = "EXW" };
                var _obj1 = new Object_Jexcel() { id = "FOB", name = "FOB" };
                var _obj2 = new Object_Jexcel() { id = "CNF", name = "CNF" };
                var _obj3 = new Object_Jexcel() { id = "CIF", name = "CIF" };
                lstItem.Add(_obj); lstItem.Add(_obj1); lstItem.Add(_obj2); lstItem.Add(_obj3);
                return Json(lstItem);
            }
            catch (Exception ex)
            {
                return Json(new JMessage { Title = "Lấy dữ liệu không thành công", Object = ex, Error = true });
            }
        }

        [HttpPost]
        public object Submit()
        {
            var msg = new JMessage() { Error = false };
            var _emp = GetEmployeeLogin(_context);
            try
            {
                var files = Request.Form.Files.ToList();
                var submit = Request.Form["submit"];
                var detail = Request.Form["detail"];
                var detailDelete = Request.Form["detailDelete"];
                ProductionManager obj = JsonConvert.DeserializeObject<ProductionManager>(submit);
                List<ProductionManagerDetails> lstdetail = JsonConvert.DeserializeObject<List<ProductionManagerDetails>>(detail);
                List<Guid> ListdetailDelete = JsonConvert.DeserializeObject<List<Guid>>(detailDelete);

                string sql = "[Sale].[SP_ESHR_ProductionManager_Insert] @RowGuid,@OrganizationGuid,@CodeID,@DateSX,@EffectiveDate,@Title,@EmployeeID,@EmployeeName,@CustomerID,@CustomerName,@TotalMoney,@Status ,@IsApprove, @ConditionValue,@WorkFlowGuid,@WorkFlowsContent,@WorkFlowCurrent,@CreatedDate ,@ModifiedDate,@CreatedBy ,@ModifiedBy,@Note,@lstDeleteDetail,@ProductionManagerDetails,@Error out";
                if (obj.DateSX.HasValue)
                {
                    obj.DateSX = Convert.ToDateTime(obj.DateSX).ToLocalTime();
                }
                else
                {
                    obj.DateSX = null;
                }
                if (obj.EffectiveDate.HasValue)
                {
                    obj.EffectiveDate = Convert.ToDateTime(obj.EffectiveDate).ToLocalTime();
                }
                else
                {
                    obj.EffectiveDate = null;
                }
                var RowGuid = SqlPara("@RowGuid ", obj.RowGuid, SqlDbType.UniqueIdentifier);
                var OrganizationGuid = SqlPara("@OrganizationGuid ", _emp.OrganizationGuid, SqlDbType.UniqueIdentifier);
                var CodeID = SqlPara("@CodeID ", obj.CodeID, SqlDbType.NVarChar);
                var DateSX = SqlPara("@DateSX ", obj.DateSX, SqlDbType.Date);
                var EffectiveDate = SqlPara("@EffectiveDate ", obj.EffectiveDate, SqlDbType.Date);
                var Title = SqlPara("@Title ", obj.Title, SqlDbType.NVarChar);
                var EmployeeID = SqlPara("@EmployeeID ", obj.EmployeeID, SqlDbType.NVarChar);
                var EmployeeName = SqlPara("@EmployeeName ", obj.EmployeeName, SqlDbType.NVarChar);
                var CustomerID = SqlPara("@CustomerID", obj.CustomerID, SqlDbType.NVarChar);
                var CustomerName = SqlPara("@CustomerName ", obj.CustomerName, SqlDbType.NVarChar);
                var TotalMoney = SqlPara("@TotalMoney ", obj.TotalMoney, SqlDbType.Decimal);
                var Status = SqlPara("@Status ", obj.Status, SqlDbType.NVarChar);
                var IsApprove = SqlPara("@IsApprove ", obj.IsApprove, SqlDbType.NVarChar);
                var ConditionValue = SqlPara("@ConditionValue ", obj.ConditionValue, SqlDbType.NVarChar);
                var WorkFlowGuid = SqlPara("@WorkFlowGuid ", obj.WorkFlowGuid, SqlDbType.UniqueIdentifier);
                var WorkFlowsContent = SqlPara("@WorkFlowsContent ", obj.WorkFlowsContent, SqlDbType.NVarChar);
                var WorkFlowCurrent = SqlPara("@WorkFlowCurrent ", obj.WorkFlowCurrent, SqlDbType.UniqueIdentifier);
                obj.CreatedDate = DateTime.Now;
                obj.ModifiedDate = DateTime.Now;
                obj.CreatedBy = GetCreatedBy(_context);
                obj.ModifiedBy = GetCreatedBy(_context);
                var CreatedDate = SqlPara("@CreatedDate ", obj.CreatedDate, SqlDbType.DateTime);
                var ModifiedDate = SqlPara("@ModifiedDate ", obj.ModifiedDate, SqlDbType.DateTime);
                var CreatedBy = SqlPara("@CreatedBy ", obj.CreatedBy, SqlDbType.NVarChar);
                var ModifiedBy = SqlPara("@ModifiedBy ", obj.ModifiedBy, SqlDbType.NVarChar);
                var Note = SqlPara("@Note ", obj.Note, SqlDbType.NVarChar);
                var _lstDeleteDetail = new SqlParameter("@lstDeleteDetail", ListdetailDelete.Count >= 1 ? string.Join(",", ListdetailDelete) : "");

                var _detail = new SqlParameter("@ProductionManagerDetails", SqlDbType.Structured);
                var _tem = new System.Data.DataTable();
                _tem.Columns.Add("RowGuid      ");
                _tem.Columns.Add("RecordGuid   ");
                _tem.Columns.Add("ItemID       ");
                _tem.Columns.Add("ItemName     ");
                _tem.Columns.Add("Title     ");
                _tem.Columns.Add("Packing        ");
                _tem.Columns.Add("ProductSpecifications     ");
                _tem.Columns.Add("FactoryType     ");
                _tem.Columns.Add("UnitID       ");
                _tem.Columns.Add("UnitName     ");
                _tem.Columns.Add("Quantity     ");
                _tem.Columns.Add("UnitPrice    ");
                _tem.Columns.Add("ExchangeRate    ");
                _tem.Columns.Add("AmountOc     ");
                _tem.Columns.Add("VATPercent   ");
                _tem.Columns.Add("VATAmountOC  ");
                _tem.Columns.Add("AmountNoVAT  ");
                _tem.Columns.Add("AmountVAT    ");
                _tem.Columns.Add("Note         ");
                _tem.Columns.Add("CreatedDate  ");
                _tem.Columns.Add("ModifiedDate ");
                _tem.Columns.Add("CreatedBy    ");
                _tem.Columns.Add("ModifiedBy   ");
                _tem.Columns.Add("SortOrder    ");

                foreach (var _item in lstdetail)
                {
                    _tem.Rows.Add(
                            _item.RowGuid
                            , _item.RecordGuid
                            , _item.ItemID
                            , _item.ItemName
                             , _item.Title
                            , _item.Packing
                             , _item.ProductSpecifications
                              , _item.FactoryType
                            , _item.UnitID
                            , _item.UnitName
                            , _item.Quantity
                            , _item.UnitPrice
                            , _item.ExchangeRate
                            , _item.AmountOc
                            , _item.VATPercent
                            , _item.VATAmountOC
                            , _item.AmountNoVAT
                            , _item.AmountVAT
                            , _item.Note
                            , DBNull.Value
                            , DBNull.Value
                            , DBNull.Value
                            , DBNull.Value
                            , _item.SortOrder
                             );
                }
                _detail.Value = _tem;
                _detail.TypeName = "[Sale].[ProductionManagerDetails]";

                var Error = new SqlParameter("@Error", SqlDbType.NVarChar, 4000) { Direction = System.Data.ParameterDirection.Output };
                var rs = _context.SQLCOMMANDS.FromSql(sql, RowGuid, OrganizationGuid, CodeID, DateSX, EffectiveDate, Title, EmployeeID, EmployeeName, CustomerID, CustomerName, TotalMoney, Status, IsApprove, ConditionValue, WorkFlowGuid, WorkFlowsContent, WorkFlowCurrent, CreatedDate, ModifiedDate, CreatedBy, ModifiedBy, Note, _lstDeleteDetail, _detail, Error).FirstOrDefault();
                if (rs.Error == "")
                {
                    return new JMessage() { Error = false, Title = "Thêm thành công" };
                }
                else
                {
                    return new JMessage() { Error = true, Title = rs.Error };
                }
            }
            catch (Exception ex)
            {
                return new JMessage() { Error = true, Title = ex.Message, Object = ex };
            }
        }

        [HttpPost]
        public object Delete([FromBody] TempSub obj)
        {
            var msg = new JMessage() { Error = false };
            try
            {
                string sql = "[Sale].[SP_ESHR_ProductionManager_Delete] @RowGuid,@Error out";
                var Id = new Guid(obj.IdS[0]);
                var RowGuid = new SqlParameter("@RowGuid", SqlDbType.UniqueIdentifier); CheckNullParameterStore(RowGuid, Id);
                var Error = new SqlParameter("@Error", SqlDbType.NVarChar, 1000) { Direction = System.Data.ParameterDirection.Output };
                var rs = _context.SQLCOMMANDS.FromSql(sql, RowGuid, Error).FirstOrDefault();
                if (rs.Error == "")
                {
                    return new JMessage() { Error = false, Title = "Xóa thành công" };
                }
                else
                {
                    return new JMessage() { Error = true, Title = rs.Error };
                }
            }
            catch (Exception ex)
            {
                return new JMessage() { Error = true, Title = ex.Message, Object = ex };
            }
        }
        [HttpPost]
        public object GetItem([FromBody] TempSub obj)
        {
            var msg = new JMessage() { Error = false };
            try
            {
                string sql = "[Sale].[SP_ESHR_ProductionManager_GetItem] @RowGuid";
                Guid? guid = new Guid(obj.IdS[0]);
                var RowGuid = new SqlParameter("@RowGuid", SqlDbType.UniqueIdentifier); CheckNullParameterStore(RowGuid, guid);
                var rs = _context.Object_Table5.FromSql(sql, RowGuid).FirstOrDefault();
                return rs;
            }
            catch (Exception ex)
            {
                return new JMessage() { Error = true, Title = ex.Message, Object = ex };
            }
        }
        [HttpPost]
        public JMessage InsertComments()
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var userlogin = GetEmployeeLogin(_context);
                var files = Request.Form.Files.ToList();
                var _obj = Request.Form["insert"];
                ProductionManagerComments obj = JsonConvert.DeserializeObject<ProductionManagerComments>(_obj);
                obj.CommentGuid = Guid.NewGuid();
                obj.OrganizationGuid = userlogin.OrganizationGuid;
                obj.EmployeeGuid = userlogin.EmployeeGuid;
                obj.EmployeeId = userlogin.EmployeeId;
                obj.EmployeeName = userlogin.FullName;
                obj.DepartmentGuid = userlogin.DepartmentGuid;
                obj.DepartmentId = userlogin.DepartmentId;
                obj.DepartmentName = userlogin.DepartmentName;
                obj.AvatarUrl = "";
                obj.CreatedDate = DateTime.Now;
                obj.CreatedBy = GetCreatedBy(_context);
                obj.ModifiedDate = DateTime.Now;
                obj.ModifiedBy = GetModifiedBy(_context);
                string sql = "[Hr].[SP_Insert_Comments_Hr] @Scheme, @TableName, @CommentGuid,@ParentGuid,@RecordGuid,@OrganizationGuid,@Comment,@EmployeeGuid,@EmployeeID,@EmployeeName,@LoginName,@DepartmentGuid,@DepartmentID,@DepartmentName,@AvatarUrl,@CreatedDate,@CreatedBy,@ModifiedDate,@ModifiedBy,@Error";
                var _Scheme = new SqlParameter("@Scheme", SqlDbType.VarChar);
                CheckNullParameterStore(_Scheme, "Sale");
                var _TableName = new SqlParameter("@TableName", SqlDbType.NVarChar);
                CheckNullParameterStore(_TableName, "ProductionManagerComments");
                var _CommentGuid = new SqlParameter("@CommentGuid", SqlDbType.UniqueIdentifier);
                CheckNullParameterStore(_CommentGuid, obj.CommentGuid);
                var _ParentGuid = new SqlParameter("@ParentGuid", SqlDbType.UniqueIdentifier);
                CheckNullParameterStore(_ParentGuid, obj.ParentGuid);
                var _RecordGuid = new SqlParameter("@RecordGuid", SqlDbType.UniqueIdentifier);
                CheckNullParameterStore(_RecordGuid, obj.RecordGuid);
                var _OrganizationGuid = new SqlParameter("@OrganizationGuid", SqlDbType.UniqueIdentifier);
                CheckNullParameterStore(_OrganizationGuid, obj.OrganizationGuid);
                var _Comment = new SqlParameter("@Comment", SqlDbType.NVarChar);
                CheckNullParameterStore(_Comment, obj.Comment);
                var _EmployeeGuid = new SqlParameter("@EmployeeGuid", SqlDbType.UniqueIdentifier);
                CheckNullParameterStore(_EmployeeGuid, obj.EmployeeGuid);
                var _EmployeeID = new SqlParameter("@EmployeeID", SqlDbType.VarChar, 20);
                CheckNullParameterStore(_EmployeeID, obj.EmployeeId);
                var _EmployeeName = new SqlParameter("@EmployeeName", SqlDbType.NVarChar, 50);
                CheckNullParameterStore(_EmployeeName, obj.EmployeeName);
                var _LoginName = new SqlParameter("@LoginName", SqlDbType.NVarChar, 50);
                CheckNullParameterStore(_LoginName, userlogin.LoginName);
                var _DepartmentGuid = new SqlParameter("@DepartmentGuid", SqlDbType.UniqueIdentifier);
                CheckNullParameterStore(_DepartmentGuid, obj.DepartmentGuid);
                var _DepartmentID = new SqlParameter("@DepartmentID", SqlDbType.VarChar, 20);
                CheckNullParameterStore(_DepartmentID, obj.DepartmentId);
                var _DepartmentName = new SqlParameter("@DepartmentName", SqlDbType.NVarChar, 50);
                CheckNullParameterStore(_DepartmentName, obj.DepartmentName);
                var _AvatarUrl = new SqlParameter("@AvatarUrl", SqlDbType.NVarChar, 255);
                CheckNullParameterStore(_AvatarUrl, obj.AvatarUrl);
                var _CreatedDate = new SqlParameter("@CreatedDate", SqlDbType.DateTime);
                CheckNullParameterStore(_CreatedDate, obj.CreatedDate);
                var _CreatedBy = new SqlParameter("@CreatedBy", SqlDbType.NVarChar, 250);
                CheckNullParameterStore(_CreatedBy, obj.CreatedBy);
                var _ModifiedDate = new SqlParameter("@ModifiedDate", SqlDbType.DateTime);
                CheckNullParameterStore(_ModifiedDate, obj.ModifiedDate);
                var _ModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.NVarChar, 250);
                CheckNullParameterStore(_ModifiedBy, obj.ModifiedBy);
                var _Error = new SqlParameter("@Error", SqlDbType.NVarChar); _Error.Value = "";
                var rs = _context.SQLCOMMANDS.FromSql(sql, _Scheme, _TableName, _CommentGuid, _ParentGuid, _RecordGuid, _OrganizationGuid, _Comment, _EmployeeGuid, _EmployeeID, _EmployeeName, _LoginName, _DepartmentGuid, _DepartmentID, _DepartmentName, _AvatarUrl, _CreatedDate, _CreatedBy, _ModifiedDate, _ModifiedBy, _Error).FirstOrDefault();

                if (files.Count > 0)
                {

                    string sqlAttach = "[dbo].[SP_Insert_Attachment] @Database, @Scheme,@TableName,@TableAttachment";
                    var Database = new SqlParameter("@Database", SqlDbType.NVarChar, 50); CheckNullParameterStore(Database, _appSettings.DBName_HR);
                    var scheme = new SqlParameter("@scheme ", SqlDbType.NVarChar, 250); CheckNullParameterStore(scheme, "Sale");
                    var TableName = new SqlParameter("@TableName ", SqlDbType.NVarChar, 50); CheckNullParameterStore(TableName, "ProductionManagerComments_DOC");
                    var _detailAttach = new SqlParameter("@TableAttachment", SqlDbType.Structured);

                    /// table detail
                    var _tematach = new System.Data.DataTable();
                    _tematach.Columns.Add("AttachmentGuid");
                    _tematach.Columns.Add("ParentGuid");
                    _tematach.Columns.Add("RecordGuid");
                    _tematach.Columns.Add("OrganizationGuid");
                    _tematach.Columns.Add("DepartmentGuid");
                    _tematach.Columns.Add("ModuleID");
                    _tematach.Columns.Add("Title");
                    _tematach.Columns.Add("FileName");
                    _tematach.Columns.Add("Attachment", typeof(byte[]));
                    _tematach.Columns.Add("FileExtension");
                    _tematach.Columns.Add("FileSize");
                    _tematach.Columns.Add("UrlExternal");
                    _tematach.Columns.Add("IsPublic");
                    _tematach.Columns.Add("PrivateLevel");
                    _tematach.Columns.Add("IsFolder");
                    _tematach.Columns.Add("IsHidden");
                    _tematach.Columns.Add("IsReadonly");
                    _tematach.Columns.Add("IsSync");
                    _tematach.Columns.Add("SyncTime");
                    _tematach.Columns.Add("CloudPath");

                    _tematach.Columns.Add("CreatedDate", typeof(DateTime));
                    _tematach.Columns.Add("ModifiedDate", typeof(DateTime));
                    _tematach.Columns.Add("CreatedBy");
                    _tematach.Columns.Add("ModifiedBy");

                    foreach (var file in files)
                    {
                        var stream = file.OpenReadStream();
                        byte[] byteArr = Utilities.StreamToByteArray(stream);
                        var name = file.FileName;
                        Hrattachments _attach = new Hrattachments()
                        {
                            AttachmentGuid = Guid.NewGuid(),

                            RecordGuid = obj.CommentGuid,
                            OrganizationGuid = userlogin.OrganizationGuid,
                            Title = name,
                            FileName = name,
                            CreatedDate = DateTime.Now,
                            CreatedBy = GetCreatedBy(_context),
                            ModifiedDate = DateTime.Now,
                            ModifiedBy = GetModifiedBy(_context),
                            Attachment = byteArr,
                            ModuleId = "ES_HR",
                            FileSize = byteArr.Length,
                            FileExtension = System.IO.Path.GetExtension(name)
                        };
                        _tematach.Rows.Add(_attach.AttachmentGuid,
                              _attach.ParentGuid,
                              _attach.RecordGuid,
                              _attach.OrganizationGuid,
                              _attach.DepartmentGuid,
                              _attach.ModuleId,
                              _attach.Title,
                              _attach.FileName,
                              _attach.Attachment,
                              _attach.FileExtension,
                              _attach.FileSize,
                              _attach.UrlExternal,
                              _attach.IsPublic,
                              _attach.PrivateLevel,
                              _attach.IsFolder,
                              _attach.IsHidden,
                              _attach.IsReadonly,

                              _attach.IsSync,
                             DBNull.Value,
                              _attach.CloudPath,
                              DateTime.Now, DateTime.Now, _attach.CreatedBy, _attach.ModifiedBy);
                    }

                    _detailAttach.Value = _tematach;
                    _detailAttach.TypeName = "[dbo].[Attachment]";
                    var __temattach = _context.SQLCOMMANDS.FromSql(sqlAttach, Database, scheme, TableName, _detailAttach).FirstOrDefault();

                    if (__temattach.Error == "")
                    {
                        var mesage = __temattach.Error;
                    }
                    else
                    {
                        return new JMessage() { Error = true, Title = "Thêm file không thành công", Object = __temattach };
                    }

                }
                if (rs.Error == "")
                {

                    return new JMessage() { Error = false, Title = "Thêm ý kiến thành công." };
                }
                else
                {
                    return new JMessage() { Error = true, Title = rs.Error };
                }
            }
            catch (SqlException ex)
            {
                return new JMessage() { Error = true, Object = ex, Title = ex.Message };
            }
        }
        [HttpGet]
        [Area("Hr")]
        public FileResult DownloadfileCommentAttach(Guid? Id)
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var username = GetEmployeeLogin(_context);
                var gui = new Guid();
                string sql = "[dbo].[SP_Download_Attachment] @Database,@Scheme, @TableName, @OrganizationGuid, @AttachmentGuid, @Error";
                var Database = new SqlParameter("@Database", SqlDbType.VarChar); Database.Value = _appSettings.DBName_HR;
                var Scheme = new SqlParameter("@Scheme", SqlDbType.VarChar); Scheme.Value = "Sale";
                var TableName = new SqlParameter("@TableName", SqlDbType.VarChar); TableName.Value = "ProductionManagerComments_DOC";
                var OrganizationGuid = new SqlParameter("@OrganizationGuid", SqlDbType.UniqueIdentifier); OrganizationGuid.Value = username.OrganizationGuid;
                var AttachmentGuid = new SqlParameter("@AttachmentGuid", SqlDbType.UniqueIdentifier); AttachmentGuid.Value = Id == gui ? (Object)DBNull.Value : Id;
                var Error = new SqlParameter("@Error", SqlDbType.NVarChar); Error.Value = "";
                var rs = _context.AttachmentsDownloadView_.FromSql(sql, Database, Scheme, TableName, OrganizationGuid, AttachmentGuid, Error).SingleOrDefault();
                if (rs != null)
                {
                    byte[] fileBytes = rs.Attachment;
                    var fileName = rs.FileName;
                    return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);

                }
                else return null;
            }
            catch (Exception ex)
            {
                msg.Title = "Có lỗi khi lấy dữ liệu.";
                msg.Object = ex;
                return null;

            }
        }
        public class CustomPage
        {
            public int Page { set; get; }
            public int ItemPage { set; get; }
            public int? Status { set; get; }
            public Guid? RecordGuid { set; get; }
        }
        /// Danh sách Comment
        [HttpPost]
        public object GetAllCommentPage([FromBody] CustomPage obj)
        {
            try
            {
                var _emp = GetEmployeeLogin(_context);

                var intBeginFor = (obj.Page - 1) * obj.ItemPage;
                string sql = "[Hr].[SP_GET_Comment] @DBName,@Scheme,@TableName,@DBNameDocument,@SchemeAt,@TableNameAt,@OrganizationGuid,@RecordGuid,@Skip,@Take,@Error";

                var DBName = new SqlParameter("@DBName", SqlDbType.NVarChar); CheckNullParameterStore(DBName, _appSettings.DBName_HR);
                var Scheme = new SqlParameter("@Scheme", SqlDbType.NVarChar); CheckNullParameterStore(Scheme, "Sale");
                var TableName = new SqlParameter("@TableName", SqlDbType.NVarChar); CheckNullParameterStore(TableName, "ProductionManagerComments");
                var DBNameDocument = new SqlParameter("@DBNameDocument", SqlDbType.NVarChar); CheckNullParameterStore(DBNameDocument, _appSettings.DBName_HR);
                var SchemeAt = new SqlParameter("@SchemeAt", SqlDbType.NVarChar); CheckNullParameterStore(SchemeAt, "Sale");
                var TableNameAt = new SqlParameter("@TableNameAt", SqlDbType.NVarChar); CheckNullParameterStore(TableNameAt, "ProductionManagerComments_DOC");
                var OrganizationGuid = new SqlParameter("@OrganizationGuid", SqlDbType.NVarChar); CheckNullParameterStore(OrganizationGuid, _emp.OrganizationGuid.ToString());
                var RecordGuid = new SqlParameter("@RecordGuid", SqlDbType.NVarChar); CheckNullParameterStore(RecordGuid, obj.RecordGuid.ToString());
                var Skip = new SqlParameter("@Skip", SqlDbType.NVarChar); Skip.Value = intBeginFor;
                var Take = new SqlParameter("@Take", SqlDbType.NVarChar); Take.Value = obj.ItemPage;
                var Error = new SqlParameter("@Error", SqlDbType.Int) { Direction = System.Data.ParameterDirection.Output };
                var rs = _context.CommentView.FromSql(sql, DBName, Scheme, TableName, DBNameDocument, SchemeAt, TableNameAt, OrganizationGuid, RecordGuid, Skip, Take, Error).ToList();

                return new JMessage() { Error = false, Title = "Success", Object = rs };

            }
            catch (Exception ex)
            {
                return new JMessage() { Error = true, Title = "Có lỗi khi lấy dữ liệu", Object = ex };
            }
        }

        // làm việc với quy trình
        [HttpPost]
        public object GetItems(Guid? Id)
        {
            var msg = new JMessage() { Error = false };
            try
            {
                string sql = "[HR].[SP_HR_ProductionManager_GetItemWorkFlows] @Id";
                var _Id = new SqlParameter("@Id", SqlDbType.UniqueIdentifier); CheckNullParameterStore(_Id, Id);
                var rs = _context.Object_Table.FromSql(sql, _Id).FirstOrDefault();
                var data = JsonConvert.DeserializeObject<List<ProductionManager>>(rs.Table1)[0];
                var wf = JsonConvert.DeserializeObject<List<Workflows>>(rs.Table2)[0];
                var activities = JsonConvert.DeserializeObject<List<Activities>>(rs.Table3)[0];
                return Json(new { item = data, item1 = wf, wf = activities });
            }
            catch (Exception ex)
            {
                return Json(new { Error = true, Object = ex, Title = "Lấy dữ liệu không thành công." });
            }
        }
        public class ObjPost
        {
            public Guid? WorkFlowGuid { get; set; }
            public string Uid { get; set; }
        }
        [HttpPost]
        public object GetProcessWF(Guid? Id)
        {
            try
            {
                var WorkFlowGuid = _context.ProductionManager.Where(x => x.RowGuid == Id).Select(x => x.WorkFlowGuid).FirstOrDefault();
                var dataWF = new ObjPost();
                dataWF.WorkFlowGuid = WorkFlowGuid;
                dataWF.Uid = "";
                var UidStep = ""; var lstStep = new List<String>();
                var UidStartStep = _context.Activities.Where(x => x.WorkflowGuid == dataWF.WorkFlowGuid && x.StartStep == true).FirstOrDefault();
                var daAactivity = new List<Activities>();
                daAactivity.Add(UidStartStep);
                lstStep.Add(UidStartStep.ActivityUid);
                var activity = _context.Activities.Where(x => x.ActivityUid == UidStartStep.ActivityUid && x.WorkflowGuid == dataWF.WorkFlowGuid && x.ConditionGuidOfStep != null).FirstOrDefault();
                var condition = _context.Conditions.FirstOrDefault(x => x.WorkflowGuid == dataWF.WorkFlowGuid && x.ConditionGuid == activity.ConditionGuidOfStep);
                string[] next = condition.StepContinue.Split(',');
                foreach (var item in next)
                {
                    var iAUpdate = _context.Activities.Where(x => x.WorkflowGuid == dataWF.WorkFlowGuid && x.ActivityUid == item).FirstOrDefault();
                    if (iAUpdate != null)
                    {
                        daAactivity.Add(iAUpdate);
                        UidStep = item;
                        lstStep.Add(item);
                    }
                }
                var lstUid = _context.Activities.Where(x => x.WorkflowGuid == dataWF.WorkFlowGuid && x.StartStep != true && !lstStep.Contains(x.ActivityUid)).Select(x => x.ActivityUid).Distinct().ToList();

                foreach (var item in lstUid)
                {
                    if (UidStep != "" && UidStep != null)
                    {
                        var activityStep = _context.Activities.Where(x => x.ActivityUid == UidStep && x.WorkflowGuid == dataWF.WorkFlowGuid && x.ConditionGuidOfStep != null).FirstOrDefault();
                        if (activityStep != null)
                        {
                            var conditionStep = _context.Conditions.FirstOrDefault(x => x.WorkflowGuid == dataWF.WorkFlowGuid && x.ConditionGuid == activityStep.ConditionGuidOfStep);
                            string[] nextStep = conditionStep.StepContinue.Split(',');
                            foreach (var itemStep in nextStep)
                            {
                                var iAUpdate = _context.Activities.Where(x => x.WorkflowGuid == dataWF.WorkFlowGuid && x.ActivityUid == itemStep && !lstStep.Contains(x.ActivityUid)).FirstOrDefault();
                                if (iAUpdate != null)
                                {
                                    daAactivity.Add(iAUpdate);
                                    UidStep = item;
                                    lstStep.Add(item);
                                }
                            }
                        }
                    }
                }
                var lstshowMapWF = new List<showMapWF>();
                foreach (var item in daAactivity)
                {
                    var map = new showMapWF(); map.id = item.ActivityUid; map.text = item.ActivityName;
                    var stt = _context.ProductionManagerProcess.Where(x => x.RecordGuid == Id && x.ActionName == item.ActivityUid && x.JobTitleName == "W").Count();
                    if (stt > 0)
                    {
                        map.status = "1";
                    }
                    else
                    {
                        map.status = "0";
                    }
                    lstshowMapWF.Add(map);
                }
                return Json(new { item = lstshowMapWF });
            }
            catch (Exception ex)
            {
                return Json(new { Error = true, Object = ex, Title = "Lấy dữ liệu không thành công." });
            }
        }

        public object Approve(ProductionManagerProcess _obj)
        {
            var userlogin = GetEmployeeLogin(_context);
            try
            {
                string sql = "[HR].[SP_ESHR_Workflows_Approve]" +
                    "@Database," +
                    "@scheme," +
                    "@TableName," +
                    "@TableProcessName," +
                    "@RowGuid," +
                    "@LoginName," +
                    "@Comment";

                var Database = new SqlParameter("@Database", SqlDbType.NVarChar);
                CheckNullParameterStore(Database, _appSettings.DBName_HR);

                var scheme = new SqlParameter("@scheme", SqlDbType.NVarChar);
                CheckNullParameterStore(scheme, "Sale");

                var TableName = new SqlParameter("@TableName", SqlDbType.NVarChar);
                CheckNullParameterStore(TableName, "ProductionManager");

                var TableProcessName = new SqlParameter("@TableProcessName", SqlDbType.NVarChar);
                CheckNullParameterStore(TableProcessName, "ProductionManagerProcess");

                var RowGuid = new SqlParameter("@RowGuid", SqlDbType.UniqueIdentifier);
                CheckNullParameterStore(RowGuid, _obj.RowGuid);

                var LoginName = new SqlParameter("@LoginName", SqlDbType.NVarChar);
                CheckNullParameterStore(LoginName, userlogin.LoginName);

                var Comment = new SqlParameter("@Comment", SqlDbType.NVarChar);
                CheckNullParameterStore(Comment, _obj.Comment);

                var rs = _context.SQLCOMMANDMessage.FromSql(
                    sql,
                    Database,
                    scheme,
                    TableName,
                    TableProcessName,
                    RowGuid,
                    LoginName,
                    Comment
                    ).FirstOrDefault();
                if (rs.Error == "")
                {
                    var ob = _context.ProductionManager.Where(x => x.RowGuid == _obj.RowGuid).FirstOrDefault();
                    ProductionManagerComments obj = new ProductionManagerComments();
                    obj.CommentGuid = Guid.NewGuid();
                    obj.Comment = _obj.Comment;
                    obj.OrganizationGuid = userlogin.OrganizationGuid;
                    obj.EmployeeGuid = userlogin.EmployeeGuid;
                    obj.EmployeeId = userlogin.EmployeeId;
                    obj.EmployeeName = userlogin.FullName;
                    obj.DepartmentGuid = userlogin.DepartmentGuid;
                    obj.DepartmentId = userlogin.DepartmentId;
                    obj.DepartmentName = userlogin.DepartmentName;
                    obj.AvatarUrl = "";
                    obj.CreatedDate = DateTime.Now;
                    obj.CreatedBy = GetCreatedBy(_context);
                    obj.ModifiedDate = DateTime.Now;
                    obj.ModifiedBy = GetModifiedBy(_context);
                    string _sql = "[Hr].[SP_Insert_Comments_Hr] @Scheme, @TableName, @CommentGuid,@ParentGuid,@RecordGuid,@OrganizationGuid,@Comment,@EmployeeGuid,@EmployeeID,@EmployeeName,@LoginName,@DepartmentGuid,@DepartmentID,@DepartmentName,@AvatarUrl,@CreatedDate,@CreatedBy,@ModifiedDate,@ModifiedBy,@Error";
                    var _Scheme = new SqlParameter("@Scheme", SqlDbType.VarChar);
                    CheckNullParameterStore(_Scheme, "Sale");
                    var _TableName = new SqlParameter("@TableName", SqlDbType.NVarChar);
                    CheckNullParameterStore(_TableName, "ProductionManagerComments");
                    var _CommentGuid = new SqlParameter("@CommentGuid", SqlDbType.UniqueIdentifier);
                    CheckNullParameterStore(_CommentGuid, obj.CommentGuid);
                    var _ParentGuid = new SqlParameter("@ParentGuid", SqlDbType.UniqueIdentifier);
                    CheckNullParameterStore(_ParentGuid, obj.ParentGuid);
                    var _RecordGuid = new SqlParameter("@RecordGuid", SqlDbType.UniqueIdentifier);
                    CheckNullParameterStore(_RecordGuid, ob.RowGuid);
                    var _OrganizationGuid = new SqlParameter("@OrganizationGuid", SqlDbType.UniqueIdentifier);
                    CheckNullParameterStore(_OrganizationGuid, obj.OrganizationGuid);
                    var _Comment = new SqlParameter("@Comment", SqlDbType.NVarChar);
                    CheckNullParameterStore(_Comment, obj.Comment);
                    var _EmployeeGuid = new SqlParameter("@EmployeeGuid", SqlDbType.UniqueIdentifier);
                    CheckNullParameterStore(_EmployeeGuid, obj.EmployeeGuid);
                    var _EmployeeID = new SqlParameter("@EmployeeID", SqlDbType.VarChar, 20);
                    CheckNullParameterStore(_EmployeeID, obj.EmployeeId);
                    var _EmployeeName = new SqlParameter("@EmployeeName", SqlDbType.NVarChar, 50);
                    CheckNullParameterStore(_EmployeeName, obj.EmployeeName);
                    var _LoginName = new SqlParameter("@LoginName", SqlDbType.NVarChar, 50);
                    CheckNullParameterStore(_LoginName, userlogin.LoginName);
                    var _DepartmentGuid = new SqlParameter("@DepartmentGuid", SqlDbType.UniqueIdentifier);
                    CheckNullParameterStore(_DepartmentGuid, obj.DepartmentGuid);
                    var _DepartmentID = new SqlParameter("@DepartmentID", SqlDbType.VarChar, 20);
                    CheckNullParameterStore(_DepartmentID, obj.DepartmentId);
                    var _DepartmentName = new SqlParameter("@DepartmentName", SqlDbType.NVarChar, 50);
                    CheckNullParameterStore(_DepartmentName, obj.DepartmentName);
                    var _AvatarUrl = new SqlParameter("@AvatarUrl", SqlDbType.NVarChar, 255);
                    CheckNullParameterStore(_AvatarUrl, obj.AvatarUrl);
                    var _CreatedDate = new SqlParameter("@CreatedDate", SqlDbType.DateTime);
                    CheckNullParameterStore(_CreatedDate, obj.CreatedDate);
                    var _CreatedBy = new SqlParameter("@CreatedBy", SqlDbType.NVarChar, 250);
                    CheckNullParameterStore(_CreatedBy, obj.CreatedBy);
                    var _ModifiedDate = new SqlParameter("@ModifiedDate", SqlDbType.DateTime);
                    CheckNullParameterStore(_ModifiedDate, obj.ModifiedDate);
                    var _ModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.NVarChar, 250);
                    CheckNullParameterStore(_ModifiedBy, obj.ModifiedBy);
                    var _Error = new SqlParameter("@Error", SqlDbType.NVarChar); _Error.Value = "";
                    var _rscm = _context.SQLCOMMANDS.FromSql(_sql, _Scheme, _TableName, _CommentGuid, _ParentGuid, _RecordGuid, _OrganizationGuid, _Comment, _EmployeeGuid, _EmployeeID, _EmployeeName, _LoginName, _DepartmentGuid, _DepartmentID, _DepartmentName, _AvatarUrl, _CreatedDate, _CreatedBy, _ModifiedDate, _ModifiedBy, _Error).FirstOrDefault();

                    if (rs.Status == "1")
                    {
                        ob.IsApprove = "2";
                    }
                    else
                    {
                        ob.IsApprove = "1";
                    }
                    _context.ProductionManager.Update(ob);
                    _context.Entry(ob).State = EntityState.Modified;
                    _context.Entry(ob).Property(x => x.RowGuid).IsModified = false;
                    _context.Entry(ob).Property(x => x.IsApprove).IsModified = true;
                    _context.SaveChanges();
                    return new { Title = "Trình lên thành công.", Error = false };
                }
            }
            catch (Exception ex)
            {
                return new { Title = ex.Message, Error = true };
            }
            return null;
        }
        public object NotApprove(ProductionManagerProcess _obj)
        {
            var userlogin = GetEmployeeLogin(_context);
            try
            {
                string sql = "[HR].[SP_ESHR_Workflows_NotApprove]" +
                    "@Database," +
                    "@scheme," +
                    "@TableName," +
                    "@TableProcessName," +
                    "@RowGuid," +
                    "@LoginName," +
                    "@Comment";

                var _Database = new SqlParameter("@Database", SqlDbType.NVarChar);
                CheckNullParameterStore(_Database, _appSettings.DBName_HR);

                var _scheme = new SqlParameter("@scheme", SqlDbType.NVarChar);
                CheckNullParameterStore(_scheme, "Sale");

                var _TableName = new SqlParameter("@TableName", SqlDbType.NVarChar);
                CheckNullParameterStore(_TableName, "ProductionManager");

                var _TableProcessName = new SqlParameter("@TableProcessName", SqlDbType.NVarChar);
                CheckNullParameterStore(_TableProcessName, "ProductionManagerProcess");

                var _RowGuid = new SqlParameter("@RowGuid", SqlDbType.UniqueIdentifier);
                CheckNullParameterStore(_RowGuid, _obj.RowGuid);

                var _LoginName = new SqlParameter("@LoginName", SqlDbType.NVarChar);
                CheckNullParameterStore(_LoginName, userlogin.LoginName);

                var _Comment = new SqlParameter("@Comment", SqlDbType.NVarChar);
                CheckNullParameterStore(_Comment, _obj.Comment);

                var rs = _context.SQLCOMMANDMessage.FromSql(
                    sql,
                    _Database,
                    _scheme,
                    _TableName,
                    _TableProcessName,
                    _RowGuid,
                    _LoginName,
                    _Comment
                    ).FirstOrDefault();

                if (rs.Error == "")
                {
                    var obj = _context.ProductionManager.Where(x => x.RowGuid == _obj.RowGuid).FirstOrDefault();
                    obj.IsApprove = "3";
                    _context.ProductionManager.Update(obj);
                    _context.Entry(obj).State = EntityState.Modified;
                    _context.Entry(obj).Property(x => x.RowGuid).IsModified = false;
                    _context.Entry(obj).Property(x => x.IsApprove).IsModified = true;
                    _context.SaveChanges();
                    return new { Title = "Trả lại thành công.", Error = false };
                }
            }
            catch (Exception ex)
            {
                return new { Title = ex.Message, Error = true };
            }
            return null;
        }
        [HttpPost]
        public object CheckLogin([FromBody] ProductionManager applyOutsides)
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var emp = GetEmployeeLogin(_context);
                var process = _context.ProductionManagerProcess.Where(x => x.LoginName == emp.LoginName && x.RecordGuid == applyOutsides.RowGuid && x.JobTitleName == "W").FirstOrDefault();
                var activityLogin = _context.Activities.Select(x => new
                {
                    x.ActivityGuid,
                    x.ConditionGuid,
                    x.ConditionGuidOfStep,
                    x.WorkflowGuid,
                    x.ActivityName,
                    x.IsActive,
                    x.ParentId,
                    x.Location,
                    x.StartStep,
                    x.IsValid,
                    x.ActivityUid
                }).Where(x => x.StartStep == true && x.WorkflowGuid == applyOutsides.WorkFlowGuid && x.ConditionGuidOfStep != null).FirstOrDefault();
                if (process != null)
                {
                    if (process.ActionName == activityLogin.ActivityUid)
                    {
                        msg.Object = 1;
                    }
                    else
                    {
                        msg.Object = 0;
                    }
                }
                else
                {
                    msg.Object = 0;
                }
            }
            catch (Exception ex)
            {
                msg.Error = true;
                msg.Object = ex;
                msg.Title = "Không trả lại được nữa.";
            }
            return Json(msg);
        }
        public class objTitleFile
        {
            public int? STT { get; set; }
            public string Title { get; set; }
        }
        public class ObjAttach
        {
            public Guid RecordGuid { get; set; }
            public int? STT { get; set; }

        }
        [HttpPost]
        //[ActionAuthorize(EAction.EDIT)]
        public JsonResult UpdateFile()
        {
            var msg = new JMessage() { Error = false, Title = "Cập nhật thành công." };
            try
            {
                var lstTitlefile = Request.Form["lstTitlefile"];
                List<objTitleFile> lstTitle = JsonConvert.DeserializeObject<List<objTitleFile>>(lstTitlefile);
                var DeleteAttach = Request.Form["DeleteAttach"];
                var _emp = GetEmployeeLogin(_context);
                var files = Request.Form.Files.ToList();
                var _obj = Request.Form["model"];
                ObjAttach obj = JsonConvert.DeserializeObject<ObjAttach>(_obj);
                List<Guid> deleteFileattach = JsonConvert.DeserializeObject<List<Guid>>(DeleteAttach);

                if (deleteFileattach.Count > 0)
                {
                    foreach (var id in deleteFileattach)
                    {
                        string sqlAttach = "[HR].[SP_Delete_Attachment] @database, @scheme,@TableName,@OrganizationGuid,@AttachmentGuid,@Error out";
                        var database = new SqlParameter("@database ", SqlDbType.NVarChar, 250); CheckNullParameterStore(database, _appSettings.DBName_HR);
                        var scheme = new SqlParameter("@scheme ", SqlDbType.NVarChar, 250); CheckNullParameterStore(scheme, "Sale");
                        var OrganizationGuid = new SqlParameter("@OrganizationGuid ", SqlDbType.NVarChar); CheckNullParameterStore(OrganizationGuid, _emp.OrganizationGuid.ToString());
                        var TableName = new SqlParameter("@TableName ", SqlDbType.NVarChar, 50); CheckNullParameterStore(TableName, "ProductionManager_DOC");
                        var AttachGuid = new SqlParameter("@AttachmentGuid ", SqlDbType.NVarChar); CheckNullParameterStore(AttachGuid, id.ToString());
                        var Error = new SqlParameter("@Error ", SqlDbType.NVarChar) { Direction = ParameterDirection.Output }; CheckNullParameterStore(Error, "");
                        var temattach = _context.SQLCOMMANDS.FromSql(sqlAttach, database, scheme, TableName, OrganizationGuid, AttachGuid, Error).FirstOrDefault();
                        if (Error.Value.ToString() == "")
                        {
                            msg.Title = "Xóa đính kèm thành công.";
                        }
                        else
                        {
                            msg.Title = "Xóa đính kèm không thành công."; msg.Error = true;
                        }
                    }
                }
                if (files.Count > 0)
                {
                    string sqlAttach = "[Hr].[SP_Insert_Attachment_for_Calendar] @database, @scheme,@TableName,@TableAttachment";
                    var database = new SqlParameter("@database ", SqlDbType.NVarChar, 250); CheckNullParameterStore(database, _appSettings.DBName_HR);
                    var scheme = new SqlParameter("@scheme ", SqlDbType.NVarChar, 250); CheckNullParameterStore(scheme, "Sale");
                    var TableName = new SqlParameter("@TableName ", SqlDbType.NVarChar, 50); CheckNullParameterStore(TableName, "ProductionManager_DOC");
                    var _detailAttach = new SqlParameter("@TableAttachment", SqlDbType.Structured);
                    /// table detail
                    var _tematach = new DataTable();
                    _tematach.Columns.Add("AttachmentGuid");
                    _tematach.Columns.Add("ParentGuid");
                    _tematach.Columns.Add("RecordGuid");
                    _tematach.Columns.Add("OrganizationGuid");
                    _tematach.Columns.Add("DepartmentGuid");
                    _tematach.Columns.Add("ModuleID");
                    _tematach.Columns.Add("Title");
                    _tematach.Columns.Add("FileName");
                    _tematach.Columns.Add("Attachment", typeof(byte[]));
                    _tematach.Columns.Add("FileExtension");
                    _tematach.Columns.Add("FileSize");
                    _tematach.Columns.Add("UrlExternal");
                    _tematach.Columns.Add("IsPublic");
                    _tematach.Columns.Add("PrivateLevel");
                    _tematach.Columns.Add("IsFolder");
                    _tematach.Columns.Add("IsHidden");
                    _tematach.Columns.Add("IsReadonly");
                    _tematach.Columns.Add("IsSync");
                    _tematach.Columns.Add("SyncTime");
                    _tematach.Columns.Add("CloudPath");
                    _tematach.Columns.Add("CreatedDate");
                    _tematach.Columns.Add("ModifiedDate");
                    _tematach.Columns.Add("CreatedBy");
                    _tematach.Columns.Add("ModifiedBy");
                    for (var i = 0; i < files.Count; i++)
                    {
                        for (var j = 0; j < lstTitle.Count; j++)
                        {
                            if (i == lstTitle[j].STT)
                            {
                                var stream = files[i].OpenReadStream();
                                byte[] byteArr = Utilities.StreamToByteArray(stream);
                                var name = files[i].FileName;

                                ContractAttachments _attach = new ContractAttachments()
                                {
                                    AttachmentGuid = Guid.NewGuid(),
                                    RecordGuid = obj.RecordGuid,
                                    OrganizationGuid = _emp.OrganizationGuid,
                                    Title = lstTitle[j].Title,
                                    FileName = name,
                                    CreatedDate = DateTime.Now,
                                    CreatedBy = GetCreatedBy(_context),
                                    ModifiedDate = DateTime.Now,
                                    ModifiedBy = GetModifiedBy(_context),
                                    Attachment = byteArr,
                                    ModuleId = "HR",
                                    FileSize = byteArr.Length,
                                    FileExtension = System.IO.Path.GetExtension(files[i].FileName)
                                };
                                _tematach.Rows.Add(_attach.AttachmentGuid,
                                    _attach.ParentGuid,
                                    _attach.RecordGuid,
                                    _attach.OrganizationGuid,
                                    _attach.DepartmentGuid,
                                    _attach.ModuleId,
                                    _attach.Title,
                                    _attach.FileName,
                                    _attach.Attachment,
                                    _attach.FileExtension,
                                    _attach.FileSize,
                                    _attach.UrlExternal,
                                    _attach.IsPublic,
                                    _attach.PrivateLevel,
                                    _attach.IsFolder,
                                    _attach.IsHidden,
                                    _attach.IsReadonly,
                                    _attach.IsSync,
                                   DBNull.Value,
                                    _attach.CloudPath,
                                    DBNull.Value, DBNull.Value, _attach.CreatedBy, _attach.ModifiedBy);
                            }
                        }
                    }
                    _detailAttach.Value = _tematach;
                    _detailAttach.TypeName = "[dbo].[Attachment]";
                    var temattach = _context.SQLCOMMANDS.FromSql(sqlAttach, database, scheme, TableName, _detailAttach).FirstOrDefault();
                    if (temattach.Error == "")
                    {
                        msg.Title = "Thêm mới thành công.";
                    }
                    else
                    {
                        msg.Title = "Thêm mới không thành công."; msg.Error = true;
                    }
                }
            }
            catch (SqlException ex)
            {
                msg.Title = "Có lỗi khi đính kèm";
                msg.Object = ex;
                msg.Error = true;
            }
            return Json(msg);
        }
        [HttpGet]
        public FileResult DownloadItemAttachment(Guid? Id)
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var username = GetEmployeeLogin(_context);
                var gui = new Guid();
                string sql = "[dbo].[SP_Download_Attachment] @Database,@Scheme, @TableName, @OrganizationGuid, @AttachmentGuid, @Error";
                var Database = new SqlParameter("@Database", SqlDbType.VarChar); Database.Value = _appSettings.DBName_HR;
                var Scheme = new SqlParameter("@Scheme", SqlDbType.VarChar); Scheme.Value = "Sale";
                var TableName = new SqlParameter("@TableName", SqlDbType.VarChar); TableName.Value = "ProductionManager_DOC";
                var OrganizationGuid = new SqlParameter("@OrganizationGuid", SqlDbType.UniqueIdentifier); OrganizationGuid.Value = username.OrganizationGuid;
                var AttachmentGuid = new SqlParameter("@AttachmentGuid", SqlDbType.UniqueIdentifier); AttachmentGuid.Value = Id == gui ? (Object)DBNull.Value : Id;
                var Error = new SqlParameter("@Error", SqlDbType.NVarChar); Error.Value = "";
                var rs = _context.AttachmentsDownloadView_.FromSql(sql, Database, Scheme, TableName, OrganizationGuid, AttachmentGuid, Error).SingleOrDefault();
                if (rs != null)
                {
                    byte[] fileBytes = rs.Attachment;
                    var fileName = rs.FileName;
                    return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);

                }
                else return null;
            }
            catch (Exception ex)
            {
                msg.Title = "Có lỗi khi lấy dữ liệu.";
                msg.Object = ex;
                return null;
            }
        }
        [HttpPost]
        //  [Filters.ActionAuthorize(EAction.OPEN)]
        public JsonResult GetItemAttachment([FromBody] ContractAttachments obj)
        {
            var msg = new JMessage() { Error = false };
            try
            {
                var username = GetEmployeeLogin(_context);
                string sql = "[HR].[SP_GET_Attachment_Calendar] @Database, @Scheme, @TableName, @OrganizationGuid, @RecordGuid, @Error";
                var database = new SqlParameter("@Database ", SqlDbType.NVarChar); CheckNullParameterStore(database, _appSettings.DBName_HR);
                var Scheme = new SqlParameter("@Scheme", SqlDbType.VarChar); Scheme.Value = "Sale";
                var TableName = new SqlParameter("@TableName", SqlDbType.VarChar); TableName.Value = "ProductionManager_DOC";
                var OrganizationGuid = SqlPara("@OrganizationGuid", username.OrganizationGuid.ToString(), SqlDbType.NVarChar);
                var RecordGuid = SqlPara("@RecordGuid", obj.RecordGuid.ToString(), SqlDbType.NVarChar);
                var Error = new SqlParameter("@Error", SqlDbType.NVarChar); Error.Value = "";
                var rs = _context.AttachmentsView.FromSql(sql, database, Scheme, TableName, OrganizationGuid, RecordGuid, Error).ToList();
                return Json(rs);
            }
            catch (Exception ex)
            {
                return Json(new JMessage() { Error = true, Title = ex.Message, Object = ex });
            }
        }
        [HttpPost]
        //[ActionAuthorize(EAction.LISTVIEW)]
        public object JTable_Customers([FromBody] JTableSearch jTablePara)
        {
            var emp = GetEmployeeLogin(_context);
            var count = (from a in _context.Customers
                         where a.IsCustomer == true
                         select new { a.CustomerGuid }).Count();

            int intBeginFor = (jTablePara.CurrentPage - 1) * jTablePara.Length;
            var balance = (from a in _context.Customers
                           where a.IsCustomer == true
                           select new { a.CustomerGuid, a.CustomerId, a.CustomerName, a.Address, a.WorkPlace, a.ActivityFieldName, a.BusinessTypeName, a.LeadName, a.Method, a.Note }).OrderUsingSortExpression(jTablePara.QueryOrderBy).Skip(intBeginFor).Take(jTablePara.Length).ToList();
            var jdata = JTableHelper.JObjectTable(balance, jTablePara.Draw, count, "CustomerGuid", "CustomerId", "CustomerName", "Address", "WorkPlace", "ActivityFieldName", "BusinessTypeName", "LeadName", "Method", "Note");
            return Json(jdata);
        }
    }
}

