﻿var ctxfolder = "/views/EIM/Resource";
app.factory('dataservice', function ($http) {
    var headers = {
        "Content-Type": "application/json;odata=verbose",
        "Accept": "application/json;odata=verbose",
    }
    return {
        insert: function (data, callback) {
            $http.post('/Resource/Insert', data).success(callback);
        },
        Update: function (data, callback) {
            $http.post('/Resource/Update', data).success(callback);
        },
        deleteItems: function (data, callback) {
            $http.post('/Resource/DeleteItems', data).success(callback);
        },
        delete: function (data, callback) {
            $http.post('/Resource/Delete', data).success(callback);
        },
        getItem: function (data, callback) {
            $http.post('/Resource/GetItem', data).success(callback);
        },
        getResource: function (callback) {
            $http.get('/Resource/GetResource').success(callback);
        },
        GetPrivilege: function (data, callback) {
            $http.post('/Resource/GetPrivilege', data).success(callback);
        },
        GetESUserPrivileges: function (data, callback) {
            $http.post('/Resource/GetESUserPrivileges', data).success(callback);
        },
        GetESResAttributes: function (data, callback) {
            $http.post('/Resource/GetESResAttributes', data).success(callback);
        },

        GetEmployees: function (callback) {
            $http.get('/Resource/GetEmployees').success(callback);
        },
        GetESActions: function (callback) {
            $http.get('/Resource/GetESActions').success(callback);
        },
        deleteEmployees: function (data, callback) {
            $http.post('/Resource/deleteEmployees', data).success(callback);
        },
        deleteActions: function (data, callback) {
            $http.post('/Resource/deleteActions', data).success(callback);
        },
        getQuyen: function (data, callback) {
            $http.post('/Resource/getQuyen', data).success(callback);
        },
        UpdateAttribute: function (data, callback) {
            $http.post('/Resource/UpdateAttribute', data).success(callback);
        },
        GetParrent: function (callback) {
            $http.get('/Resource/GetParrent').success(callback);
        },
    }
});
app.filter('fdate', [
    '$filter', function ($filter) {
        return function (input, f) {
            if (input && input.toString().indexOf('Date') > -1) {
                return moment(input).format(f);
            } if (input && input.toString().indexOf('T') > -1) {
                return moment(input).format(f);
            } else return input;
        };
    }
]);
app.directive('format', ['$filter', function ($filter) {
    return {
        require: '?ngModel',
        link: function (scope, elem, attrs, ctrl) {
            if (!ctrl) return;
            ctrl.$formatters.unshift(function (a) {
                return $filter(attrs.format)(ctrl.$modelValue)
            });

            ctrl.$parsers.unshift(function (viewValue) {
                var plainNumber = viewValue.replace(/[^\d|\-+|\.+]/g, '');
                elem.val($filter(attrs.format)(plainNumber));
                return plainNumber;
            });
        }
    };
}]);
app.filter('html', ['$sce', function ($sce) {
    return function (text) {
        return $sce.trustAsHtml(text);
    };
}])
var spassAresSlug = function (number, isview) {
    try {
        // khai bao bien su dung       
        var numbers = number; var length = numbers.length; if (isview) {
            // khi dang la hien thi        
            if (numbers.indexOf(".") != -1) {
                length = numbers.indexOf(".");
                numbers = number.replace(".", ",");
            }
        } else {
            // khi dang la input textbox           
            if (numbers.indexOf(",") != -1) length = numbers.indexOf(",");
        }
        // loai bo nhung ky tu khong can thiet       
        // check so 0         
        var isTrue = true; while (isTrue) {
            if (numbers[0] == "0") numbers = numbers.substr(1, numbers.length);
            else isTrue = false;
        };
        // check dau phay      
        numbers = numbers.split(",");
        var string = "";
        for (var i = 0; i < numbers.length; i++) {
            if (i == 0) string += numbers[i];
            else if (i == 1) string += "," + numbers[i];
            else string += numbers[i];
        } numbers = string;
        // dung chung cho 2 truong hop       
        var no = 3, index = length - no;
        while (index > 0) { numbers = numbers.substr(0, index) + '.' + numbers.substr(index); index -= no; };
        return numbers == "" ? 0 : numbers;
    } catch (ex) { }
    return numbers == "" ? 0 : numbers;
}

app.directive('formatsub', ['$filter', function ($filter) {
    return {
        require: '?ngModel', link: function (scope, elem, attrs, ctrl) {
            if (!ctrl) return; ctrl.$formatters.unshift(function (viewValue) {
                try {
                    plainNumber = viewValue.replace('.', ','); elem.val(spassAresSlug(plainNumber));
                    return spassAresSlug(plainNumber);
                } catch (ex) { return undefined; }
            }); ctrl.$parsers.unshift(function (viewValue) {
                plainNumber = viewValue.replace(/[^-0-9,]/g, '');
                elem.val(spassAresSlug(plainNumber)); return plainNumber.replace(/[,]/g, '.');
            });
        }
    };
}]);

app.controller('Ctrl_HR_Resource', function ($scope, $rootScope, $compile, dataservice) {
    $rootScope.go = function (path) {
        $location.path(path); return false;
    };
    $rootScope.LoginName = $("#loginName").val();
    dataservice.GetESActions(function (rs) {
        if (rs != null) {
            $rootScope.ESActions = [];
            angular.forEach(rs, function (value, key) {
                $rootScope.ESActions.push({ value: value.Id, text: value.Id + "-" + value.Title });
            })
        }
    });
    dataservice.GetParrent(function (rs) {
        $rootScope.ListParentId = [];
        $rootScope.ListParentId.push({ value: null, text: "Bỏ chọn" });
        if (rs != null) {
            angular.forEach(rs, function (value, key) {
                $rootScope.ListParentId.push({ value: value.id, text: value.text });
            })
        }
    });

    dataservice.GetEmployees(function (rs) {
        if (rs != null) {
            $rootScope.Employees = [];
            angular.forEach(rs, function (value, key) {
                $rootScope.Employees.push({ value: value.LoginName, text: value.LoginName + " - "+ value.FullName });
            })
        }
    });
    $rootScope.ListIsActive = [
        {
            value: null,
            text: 'Bỏ chọn'
        },
        {
            value: '1',
            text: 'Sử dụng'
        }, {
            value: '0',
            text: 'Không sử dụng'
        }
    ];

    $rootScope.validationOptions = {
        rules: {
            Code: {
                required: true,
                maxlength: 250
            },
            Title: {
                required: true,
                maxlength: 250
            },

        },
        messages: {
            Code: {
                required: "Yêu cầu nhập mã đường dẫn",
                maxlength: "Mã đường dẫn không vượt quá 250 ký tự."
            },
            Title: {
                required: "Yêu cầu nhập Tên đường dẫn",
                maxlength: "Tên đường dẫn không vượt quá 250 ký tự."
            },

        }
    }
    $rootScope.StatusData = [{
        value: 1,
        text: 'Sử dụng'
    }, {
        value: 0,
        text: 'Không sử dụng'
    }];
    $rootScope.CreateDate = function (date) {
        if (date !== null && date !== undefined) {
            var newdate = new Date(date);
            var month = newdate.getMonth() + 1;
            var day = newdate.getDate();
            var year = newdate.getFullYear();
            var hh = newdate.getHours();
            var mm = newdate.getMinutes();
            if (month < 10)
                month = "0" + month;
            if (day < 10)
                day = "0" + day;
            if (mm < 10)
                mm = "0" + mm;
            return todayDate = day + "/" + month + "/" + year;
        } else {
            return null;
        }
    }
    $rootScope.ConvertStringTonumber = function (str) {
        if (str !== null && str !== "" && str !== undefined && str > 0) {
            var x = parseFloat(str);
            return x;
        } else {
            return "";
        }

    }
    $rootScope.addPeriod1 = function (nStr) {
        if (nStr !== null && nStr !== "" && nStr !== undefined) {
            nStr = Math.round(parseFloat(nStr) * 100) / 100;

            nStr += "";
            if (nStr.indexOf(",") >= 0) {
                var x = nStr.split(",");
            } else {
                var x = nStr.split(".");
            }
            var x1 = x[0];
            var x2 = x.length > 1 && parseInt(x[1]) > 0 ? "," + x[1] : "";
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, "$1" + "." + "$2");
            }
            var Total = x1 + x2;
            return Total;

        }
        else {
            return 0;
        }
    };
    $rootScope.GenderData = [
        {
            value: "M",
            text: 'Nam'
        }, {
            value: "F",
            text: 'Nữ'
        }];
    $rootScope.StatusOfWorkData = [
        {
            value: 'OM',
            text: 'Nhân viên chính thức'

        }, {
            value: 'PE',
            text: 'Nhân viên thử việc'

        },
        {
            value: 'PT',
            text: 'Nhân viên thời vụ'

        },

    ];

    $rootScope.FilterResultData = [
        {
            value: null,
            text: 'Bỏ chọn'
        },
        {
            value: 'F',
            text: 'Chưa lọc'
        }, {
            value: 'Y',
            text: 'Đạt'
        }, {
            value: 'N',
            text: 'Không đạt'
        }
    ];

    $rootScope.CreateDate = function (date) {
        if (date !== null && date !== undefined) {
            var newdate = new Date(date);
            var month = newdate.getMonth() + 1;
            var day = newdate.getDate();
            var year = newdate.getFullYear();
            var hh = newdate.getHours();
            var mm = newdate.getMinutes();
            if (month < 10)
                month = "0" + month;
            if (day < 10)
                day = "0" + day;
            if (mm < 10)
                mm = "0" + mm;
            return todayDate = day + "/" + month + "/" + year;
        } else {
            return null;
        }
    };
    $rootScope.convertDateSearch = function (datetime) {
        if (datetime !== null && datetime !== "" && datetime !== undefined) {
            var Str = datetime.toString();
            if (Str.indexOf("/Date") >= 0) {

                var newdate = new Date(parseInt(datetime.substr(6)));
                var month = newdate.getMonth() + 1;
                var day = newdate.getDate();
                var year = newdate.getFullYear();
                var hh = newdate.getHours();
                var mm = newdate.getMinutes();
                if (month < 10)
                    month = "0" + month;
                if (day < 10)
                    day = "0" + day;
                return month + "/" + day + "/" + year;
            } else {
                var value = datetime.split('/');

                return value[2] + '-' + value[1] + '-' + value[0];
            }
        }
        return null;
    };
    $rootScope.DateCurrent = function () {
        var newdate = new Date();
        var month = newdate.getMonth() + 1;
        var day = newdate.getDate();
        var year = newdate.getFullYear();
        var hh = newdate.getHours();
        var mm = newdate.getMinutes();

        if (month < 10)
            month = "0" + month;
        if (day < 10)
            day = "0" + day;
        if (mm < 10)
            mm = "0" + mm;
        return month + "/" + day + "/" + year;
    };
    //0 11/11/2017
    //1 11/11/2017 22:22
    //2 123123123
    //3 type date
    //4 type data year month day
    $rootScope.ConDate = function (data, number) {
        try {
            if (data == null || data == "") {
                return '';
            }
            if (data !== null && data != "" && data != undefined) {
                try {
                    if (data.indexOf("SA") != -1 || data.indexOf("CH") != -1) {
                        if (data.indexOf("SA") != -1) { }
                        if (data.indexOf("CH") != -1) { }
                    }

                    if (data.indexOf('Date') != -1) {
                        data = data.substring(data.indexOf("Date") + 5);
                        data = parseInt(data);
                    }
                }
                catch (ex) { }
                var newdate = new Date(data);
                if (number == 3) {
                    return newdate;
                }
                if (number == 2) {
                    return "/Date(" + newdate.getTime() + ")/";
                }
                var month = newdate.getMonth() + 1;
                var day = newdate.getDate();
                var year = newdate.getFullYear();
                var hh = newdate.getHours();
                var mm = newdate.getMinutes();
                if (month < 10)
                    month = "0" + month;
                if (day < 10)
                    day = "0" + day;
                if (mm < 10)
                    mm = "0" + mm;
                if (number == 0) {
                    return todayDate = day + "/" + month + "/" + year;
                }
                if (number == 1) {
                    return todayDate = day + "/" + month + "/" + year + " " + hh + ":" + mm;
                }
                if (number == 4) {
                    return new Date(year, month - 1, day);
                }
            } else {
                return '';
            }
        } catch (ex) {
            return "";
        }
    }
    //định dạng số xóa dấu chấm phẩy
    $rootScope.addPeriod = function (nStr) {
        if (nStr !== null && nStr !== "" && nStr !== undefined) {
            nStr = Math.round(parseFloat(nStr) * 100) / 100;

            nStr += "";
            if (nStr.indexOf(",") >= 0) {
                var x = nStr.split(",");
            } else {
                var x = nStr.split(".");
            }
            var x1 = x[0];
            var x2 = x.length > 1 && parseInt(x[1]) > 0 ? "," + x[1] : "";
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, "$1" + "." + "$2");
            }
            var Total = x1 + x2;
            return Total;

        }
        else {
            return 0;
        }
    };

});

app.config(function ($routeProvider, $validatorProvider) {
    $routeProvider
        .when('/', {
            templateUrl: ctxfolder + '/index.html',
            controller: 'index'
        })
        .when('/edit/:id', {
            templateUrl: ctxfolder + '/edit.html',
            controller: 'edit'
        }).when('/open/:id', {
            templateUrl: ctxfolder + '/open.html',
            controller: 'open'
        })
        .when('/add/', {
            templateUrl: ctxfolder + '/add.html',
            controller: 'add'
        })
    $validatorProvider.setDefaults({
        errorElement: 'span',
        errorClass: 'help-block',
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        success: function (label) {
            label.closest('.form-group').removeClass('has-error');
        }
    });
});
app.controller('index', function ($http, $scope, $timeout, $rootScope, $compile, $confirm, $ngConfirm, $uibModal, DTOptionsBuilder, DTColumnBuilder, DTInstances, dataservice, keyhandle, utility) {

    var vm = $scope;
    $scope.model = { ESActionsId: [], EmployeesId: null,AccountName :"" };

    // Hiển thị đơn vị tổ chức
    $scope.jstreeData = [];
    $scope.ignoreChanges = false;
    $scope.DepartmentID = null;
    $scope.lstDepartmentID = [];

    $scope.reloadTree = function () {
        $scope.model.reSelected = [];
        $scope.jstreeData = [];
        dataservice.getResource(function (rs) {
            if (rs.Error) {
                App.notifyDanger("Có lỗi khi lấy dữ liệu.");
            } else {
                $scope.jstreeData = initJsTreeData(rs);

            }
            $scope.treeConfig.version++;
        });
    }
    $scope.reloadTree();
    $scope.applyModelChanges = function () {
        return !$scope.ignoreChanges;
    };
    $scope.treeConfig = {
        core: {
            error: function (error) {
                $log.error('treeCtrl: error from js tree - ' + angular.toJson(error));
            },
            check_callback: true, worker: true
        },
        "checkbox": {
            "three_state": true,
            "whole_node": false,
            "keep_selected_style": true,
            "cascade": "undetermined"
        },
        plugins: ["checkbox", 'types', 'state'],
        version: 1,
        types: {
            valid_children: ["selected"],
            types: {
                "selected": {
                    "select_node": false
                }
            }
        }
    };
    $scope.applyModelChanges = function () {
        return !$scope.ignoreChanges;
    };
    $scope.readyCB = function () {
        $timeout(function () {
            $scope.ignoreChanges = false;
        });
    }
    $scope.lstOrganizationGuid = [];
    $scope.changeNode = function (event, data) {
        var items = $scope.treeInstance.jstree(true).get_selected();

        if (items.length > 0) {
            $scope.DepartmentID = items[0];
            $scope.lstDepartmentID = [];
            $scope.lstDepartmentID = items;

            $scope.getData();
        } else {
            $scope.lstDepartmentID = [];
            $scope.getData();
        }
    }
    $scope.choose = function (item) {
        item.Active = !item.Active;
    }
    $scope.ESActionsConfig = {
        placeholder: 'Chọn',
        selectAll: true,
        search: true,
        searchText: 'Tìm kiếm...',
        showTitle: 'true',
        csvSepChar: ',',
        captionFormat: '{0} lựa chọn',
        captionFormatAllSelected: '{0} lựa chọn',
        noMatch: 'Không có dữ liệu "{0}"',
        okCancelInMulti: true,
        locale: ['Đồng ý', 'Hủy', 'Chọn tất cả']
    };

    $scope.ESActionsChange = function (data) {
        $scope.model.ESActionsId = data !== "null" ? data : "";
    }
    //Trạng thái

    $scope.EmployeesConfig = {
        placeholder: 'Chọn',
        selectAll: true,
        search: true,
        searchText: 'Tìm kiếm...',
        showTitle: 'true',
        csvSepChar: ',',
        captionFormat: '{0} lựa chọn',
        captionFormatAllSelected: '{0} lựa chọn',
        noMatch: 'Không có dữ liệu "{0}"',
        okCancelInMulti: true,
        locale: ['Đồng ý', 'Hủy', 'Chọn tất cả']
    };
    $scope.EmployeesChange = function (data) {
        $scope.model.EmployeesId = data !== "null" ? data : "";
    };

    $scope.reloadALL = function () {
        $scope.staticParam.Search = "";
        $scope.staticParam.IsActive = '1';
        $scope.selectAll = false;
        reloadData(true);
    };
    $rootScope.reload = function () {
        reloadData(true);
    };
    $scope.lstESActionsId = [];
    $scope.getData = function () {

        dataservice.GetPrivilege($scope.lstDepartmentID, function (rs) {
            if (rs != null) {
                $scope.lstPrivilegeId = []; $scope.lstESActionsId = [];
                angular.forEach(rs.Privilege, function (value, key) {
                    $scope.lstESActionsId.push(value);
                })

                $scope.ListEmployeesId = [];
                angular.forEach(rs.ESUserPrivileges, function (value, key) {
                    $scope.ListEmployeesId.push(value);
                })

                $scope.ListESResAttributes = [];
                angular.forEach(rs.Attribute, function (value, key) {
                    $scope.ListESResAttributes.push(value);
                })

                if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                    $scope.$apply();
                }
            }
        });
    }
    $scope.add_Employees = function () {
        if ($scope.model.EmployeesId === null || $scope.model.EmployeesId === undefined || $scope.model.EmployeesId === "") {
            App.notifyDanger("Yêu cầu chọn người truy cập");
            return;
        }
        if ($scope.model.ESActionsId.length === 0) {
            App.notifyDanger("Yêu cầu chọn quyền truy cập");
            return;
        }
        var ActionsId = [];
        for (var i = 0; i < $scope.model.ESActionsId.length; i++) {
            if ($scope.model.ESActionsId[i] !== "null" && $scope.model.ESActionsId[i] !== null) {
                ActionsId.push({ ActionId: $scope.model.ESActionsId[i] });
            }
        }
        var UserId = [];
        for (var i = 0; i < $scope.model.EmployeesId.length; i++) {
            if ($scope.model.EmployeesId[i] !== "null" && $scope.model.EmployeesId[i] !== null) {
                UserId.push($scope.model.EmployeesId[i]);
            }
        }
        var obj = {
            lstId: $scope.lstDepartmentID,
            Privilege: ActionsId,
            ESUserPrivileges: UserId
        }
        var fd = new FormData();
        App.blockUI({
            target: "#contentMain",
            boxed: true,
            message: 'Đang tải...'
        });
        fd.append('Insert', JSON.stringify(obj));
        $.ajax({
            url: '/Resource/InsertUserId',
            data: fd,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (rs) {
                if (rs.Error) {
                    App.notifyDanger(rs.Title);
                    App.unblockUI("#contentMain");
                    return;
                }
                App.notifyInfo(rs.Title);
                App.unblockUI("#contentMain");
                $scope.getData();
            },
            error: function (rs) {

                App.notifyInfo(rs.Title);
            }
        });

    }
    $scope.updateESResAttributes = function (data, index) {
        dataservice.UpdateAttribute(data, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
                return;
            } else {
                App.notifyInfo(rs.Title);
            }
        });

    }

    $scope.addSetting = function () {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/add.html',
            controller: 'add',
            backdrop: 'static',
            keyboard: false,
            size: '80'
        });
        modalInstance.result.then(function (d) {
            $scope.reloadTree();
        }, function () {
        });
    }
    $scope.editSetting = function () {
        if ($scope.lstDepartmentID.length === 0 || $scope.lstDepartmentID === undefined || $scope.lstDepartmentID === "") {
            App.notifyDanger("Chưa có đường dẫn nào được chọn");
            return;
        }

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/edit.html',
            controller: 'edit',
            backdrop: 'static',
            keyboard: false,
            size: '80',
            resolve: {
                para: function () {
                    return $scope.lstDepartmentID;
                }
            }
        });
        modalInstance.result.then(function (d) {
            $scope.reloadTree();
        }, function () {
        });
    }

    $scope.deleteEmployees = function (data, index) {

        $confirm({ text: 'Bạn có chắc chắn xóa ' + data + ' không ?', title: 'Xác nhận', ok: 'Đồng ý', cancel: ' Đóng' })
            .then(function () {
                App.blockUI({
                    target: "#contentMain",
                    boxed: true,
                    message: 'Đang tải...'
                });
                dataservice.deleteEmployees({ IdI: $scope.lstDepartmentID, Ids: [data] }, function (result) {
                    if (result.Error) {
                        App.notifyDanger(result.Title);
                        App.unblockUI("#contentMain");

                    } else {
                        App.notifyInfo(result.Title);
                        $scope.getData();
                    }
                    App.unblockUI("#contentMain");
                });
            });
    }
    $scope.deleteActions = function (data, index) {

        $confirm({ text: 'Bạn có chắc chắn xóa ' + data.ActionId + ' - ' + $scope.model.AccountName + ' không ?', title: 'Xác nhận', ok: 'Đồng ý', cancel: ' Đóng' })
            .then(function () {
                App.blockUI({
                    target: "#contentMain",
                    boxed: true,
                    message: 'Đang tải...'
                });
                dataservice.deleteActions({ IdI: [data.Id], IdS: [$scope.model.AccountName] }, function (result) {
                    if (result.Error) {
                        App.notifyDanger(result.Title);
                    } else {
                        App.notifyInfo(result.Title);
                        $scope.getData();
                    }
                    App.unblockUI("#contentMain");
                });
            });

    }
    $scope.getQuyen = function (data) {
        $scope.model.AccountName = data;
        dataservice.getQuyen({ IdI: $scope.lstDepartmentID, Ids: [data] }, function (rs) {
            if (rs !== null) {
                $scope.lstESActionsId = [];
                angular.forEach(rs, function (value, key) {
                    $scope.lstESActionsId.push(value);
                })
            }
        });
    }
    $scope.delete = function () {
        if ($scope.lstDepartmentID.length === 0 || $scope.lstDepartmentID === undefined || $scope.lstDepartmentID === "") {
            App.notifyDanger("Chưa có đường dẫn nào được chọn");
            return;
        }
        dataservice.getItem($scope.lstDepartmentID, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {
                $confirm({ text: 'Bạn có chắc chắn xóa ' + rs.Title + ' không ?', title: 'Xác nhận', ok: 'Đồng ý', cancel: ' Đóng' })
                    .then(function () {
                        App.blockUI({
                            target: "#contentMain",
                            boxed: true,
                            message: 'Đang tải...'
                        });
                        dataservice.delete({ IdI: $scope.lstDepartmentID }, function (result) {
                            if (result.Error) {
                                App.notifyDanger(result.Title);
                            } else {
                                App.notifyInfo(result.Title);
                                $scope.reloadTree();
                                $scope.getData();
                            }
                            App.unblockUI("#contentMain");
                        });
                    });
            }
        })
    }


});

app.controller('add', function ($scope, $rootScope, $compile, $uibModal, $confirm, $uibModalInstance, FileUploader, dataservice) {
    $scope.Title = "Thêm đường dẫn";
    $scope.model = { ParentId: null, GroupResourceId: "ES-HR" };
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    }
    var uploader = $scope.uploader = new FileUploader({
        // url: App.baseService() + '/upload'
    });
    uploader.filters.push({
        name: "customFilter",
        fn: function (item /*{File|FileLikeObject}*/, options) {
            return this.queue.length < 10;
        }
    });
    uploader.onSuccessItem = function (fileItem, response, status, headers) {
        $scope.jmodel.FileAttachments.push(response);
    };
    $scope.jmodelPer = {};
    uploader.onCompleteAll = function () {
        $scope.commit();
    };

    //Trạng thái
    $scope.StatusConfig = {
        placeholder: 'Chọn cấp trên',
        okCancelInMulti: true,
        selected: ['true']
    };
    $scope.ChangeIsActive = function (data) {
        $scope.model.ParentId = data !== "null" ? data : "";
    }

    $scope.submit = function () {
        if ($scope.addform.validate()) {
            if ($scope.model.ParentId === "" || $scope.model.ParentId === "null" || $scope.model.ParentId === undefined) {
                $scope.model.ParentId = null;
            }
            dataservice.insert($scope.model, function (rs) {
                if (rs.Error) {
                    App.notifyDanger(rs.Title);
                    return;
                } else {
                    App.notifyInfo(rs.Title);
                    $uibModalInstance.close();
                }
            });

        }
    }
});
function initJsTreeData(data) {
    if (!data) {
        return null;
    }
    var jsData = [];
    data.forEach(function (item, index) {
        //if (item.ParentId === null) {
        var treeItem = {
            id: item.Id,
            parent: '#',
            text: item.Title,
            state: {
                opened: true
            }
        };
        jsData.push(treeItem);
        //subJsTreeData(jsData, data, item.Id);
        //}
    });
    return jsData;
}
function subJsTreeData(jsData, data, parentid) {
    data.forEach(function (item, index) {
        if (item.ParentId && item.ParentId === parentid) {
            var treeItem = {
                id: item.Id,
                parent: item.ParentId,
                text: item.Title,
                state: {
                    "opened": true
                }
            };
            jsData.push(treeItem);
            subJsTreeData(jsData, data, item.Id);
        }
    });

    return jsData;
}
app.controller('edit', function ($scope, $rootScope, $compile, $uibModal, $confirm, $uibModalInstance, FileUploader, dataservice, para) {
    $scope.Title = "Sửa đường dẫn";
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    }
    $scope.StatusConfig = {
        placeholder: 'Chọn trạng thái',
        okCancelInMulti: true,
        selected: ['true']
    };
    $scope.ChangeIsActive = function (data) {
        $scope.model.ParentId = data !== "null" ? data : "";
    }

    $scope.initData = function () {
        dataservice.getItem(para, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
                return;
            } else {
                $scope.model = rs;

            }
        });
    }
    $scope.initData();
    $scope.submit = function () {
        if ($scope.addform.validate()) {
            if ($scope.model.ParentId === "" || $scope.model.ParentId === "null" || $scope.model.ParentId === undefined) {
                $scope.model.ParentId = null;
            }
            dataservice.Update($scope.model, function (rs) {
                if (rs.Error) {
                    App.notifyDanger(rs.Title);
                    return;
                } else {
                    App.notifyInfo(rs.Title);
                    $uibModalInstance.close();
                }
            });
        }
    }

});
app.controller('open', function ($scope, $rootScope, $compile, $uibModal, $confirm, $uibModalInstance, dataservice, para) {
    $scope.Title = "Xem đường dẫn";
    $scope.model = {};
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    }

    $scope.loadData = function () {
        dataservice.getItem({ IdI: [para] }, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {
                $scope.model = rs;
                if ($scope.model.IsActive === true) {
                    $scope.model.IsActive = "Sử dụng";
                } else { $scope.model.IsActive = "Không sử dụng"; }

            }
        });
    }
    $scope.loadData();
}); 
