﻿function loadCountDocument() {
    try {
        var _data = [];
        $.ajax({
            type: "POST",
            url: "/Home/CountMenuDocuments",
            contentType: false,
            processData: false,
            data: JSON.stringify(_data),
            success: function (rs) {
                if (rs.Error) {
                    console.log(rs);
                } else {
                    for (var i = 0; i < rs.Object.length; i++) {
                        var _html = '<span ';
                        //_html += 'style="color: ' + _liStatus[i].color + ';"';
                        _html += 'style="color:white;background-color: var(--ES-notstart) !important;" class="badge sidebar-section-nav__badge ';
                        _html += 'notstart';
                        _html += '">';
                        _html += rs.Object[i].Value + '</span>';
                        $('[link-code=' + rs.Object[i].Name + ']')
                            .append(_html);
                    }
                }
            }
        })
    } catch (ex) {
        console.log('Error count document.')
    }
}
//load xong giao diện thì thực hiện đếm số bản ghi
$(window).ready(function () {
    loadCountDocument();
})