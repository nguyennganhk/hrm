﻿var ctxfolder = "/views/HR/Employees";
//var app = angular.module('App_ESHRM_Employees', ['ngSanitize', 'angularjs-datetime-picker', "ui.bootstrap", "ngRoute", "ngValidate", "datatables", "datatables.bootstrap", 'datatables.colvis', "ui.bootstrap.contextMenu", 'datatables.colreorder', 'angular-confirm', "ui.select", "ngJsTree", "treeGrid", 'summernote', 'angularFileUpload']);
app.factory('dataservice', function ($http) {
    var headers = {
        "Content-Type": "application/json;odata=verbose",
        "Accept": "application/json;odata=verbose",
    }
    return {
        //=========================Start Employees (nhân viên)  =====================
        insert: function (data, callback) {
            $http.post('/Employees/insert', data).success(callback);
        },
        GetNumberAuto: function (data, callback) {
            $http.post('/Employees/GetNumberAuto', data).success(callback);
        },
        update: function (data, callback) {
            $http.post('/Employees/update', data).success(callback);
        },
        delete: function (data, callback) {
            $http.post('/Employees/IsDeleted_Employees?id=' + data).success(callback);
        },
        deletes: function (data, callback) {
            $http.post('/Employees/deleteItems_Employees', data).success(callback);
        },
        getitem: function (data, callback) {
            $http.post('/Employees/GetItem?id=' + data).success(callback);
        },
        GetItemOpen: function (data, callback) {
            $http.post('/Employees/GetItemOpen?id=' + data).success(callback);
        },
        getItemByID: function (data, callback) {
            $http.post('/Employees/GetItemByID', data).success(callback);
        },
        getall: function (callback) {
            $http.get('/Employees/getall').success(callback);
        },
        getdepartment: function (callback) {
            $http.get('/Employees/GetDepartment').success(callback);
        },
        geteducation: function (callback) {
            $http.get('/Employees/geteducation').success(callback);
        },
        getshift: function (callback) {
            $http.get('/Employees/GetShift').success(callback);
        },
        getjobTitle: function (callback) {
            $http.get('/Employees/GetJobTitle').success(callback);
        },
        getnationality: function (callback) {
            $http.get('/Employees/GetNationality').success(callback);
        },
        getprovince: function (callback) {
            $http.get('/Employees/GetProvince').success(callback);
        },
        getdistrict: function (callback) {
            $http.get('/Employees/GetDistrict').success(callback);
        },
        getCandidate: function (callback) {
            $http.get('/Employees/GetCandidate').success(callback);
        },
        getEthnic: function (callback) {
            $http.get('/Employees/GetEthnics').success(callback);
        },
        getBank: function (callback) {
            $http.get('/Employees/GetBanks').success(callback);
        },
        getLanguages: function (callback) {
            $http.get('/Employees/GetLanguages').success(callback);
        },
        getGetPic: function (data, callback) {
            $http.get('/Employees/GetPic?id=' + data).success(callback);
        },
        getEmployeesLogin: function (data, callback) {
            $http.post('/Employees/GetEmployees_By_LoginName', data).success(callback);
        },
        Read: function (data, callback) {
            $http.post('/Employees/Read', data).success(callback);
        },
        GetItemTrainingRequirement: function (data, callback) {
            $http.post('/Employees/GetItemTrainingRequirement/', data).success(callback);
        },
        GetItem_TrainingRequestDetailsIndex: function (data, callback) {
            $http.post('/Employees/GetItem_TrainingRequestDetailsIndex/', data).success(callback);
        },

        PrintHDLDCHINHTHUC: function (data, callback) {
            $http.post('/Employees/PrintHDLD/', data).success(callback);
        },
        PrintHDLDTHUVIEC: function (data, callback) {
            $http.post('/Employees/PrintHDLDTHUVIEC/', data).success(callback);
        },
        PrintHDLDHocnghe: function (data, callback) {
            $http.post('/Employees/PrintHDLDHocnghe/', data).success(callback);
        },

        PrintHDLDTHOIVU: function (data, callback) {
            $http.post('/Employees/PrintHDLDTHOIVU/', data).success(callback);
        },
        printExcel: function (data, callback) {
            $http.post('/Employees/PrintExcel', data).success(callback);
        },
        GetAttachmentTrainingRequirement: function (data, callback) {
            $http.post('/Employees/GetAttachmentTrainingRequirement', data).success(callback);
        },
        gettreedataadd: function (data, callback) {
            $http.post('/Employees/GetTreeDataAdd/' + data).success(callback);
        },
        getTreeByID: function (data, callback) {
            $http.post('/Employees/Approved?id=' + data).success(callback);
        },
        getListSalariesbyEmployees: function (data, callback) {
            $http.post('/Employees/ListSalariesbyEmployees', data).success(callback);
        },
        getListInsurancesbyEmployees: function (data, callback) {
            $http.post('/Employees/ListInsurancesbyEmployees', data).success(callback);
        },
        getListContractsbyEmployees: function (data, callback) {
            $http.post('/Employees/ListContractsbyEmployees', data).success(callback);
        },
        getListHistoriesbyEmployees: function (data, callback) {
            $http.post('/Employees/ListHistoriesbyEmployees', data).success(callback);
        },
        getListRewardDisciplinesbyEmployees: function (data, callback) {
            $http.post('/Employees/ListRewardDisciplinesbyEmployees', data).success(callback);
        },
        getListCertificatebyEmployees: function (data, callback) {
            $http.post('/Employees/ListCertificatebyEmployees', data).success(callback);
        },
        getListTrainingRequirementsbyEmployees: function (data, callback) {
            $http.post('/Employees/ListTrainingRequirementsbyEmployees', data).success(callback);
        },
        getListReasonLeaves: function (callback) {
            $http.post('/Employees/getListReasonLeaves').success(callback);
        },
        ImportSalaries: function (data, callback) {
            $http.post('/Employees/ImportSalaries', data).success(callback);
        },
        GetProfestionals: function (callback) {
            $http.get('/Employees/GetProfestionals').success(callback);
        },
        GetSpecializes: function (callback) {
            $http.get('/Employees/GetSpecializes').success(callback);
        },
        GetConcurrently: function (data, callback) {
            $http.post('/Employees/GetConcurrently?id=' + data).success(callback);
        },
        GetSalaryTier: function (callback) {
            $http.get('/Employees/GetSalaryTier').success(callback);
        },
        GetShiftOfEmployees: function (data, sscallback) {
            $http.post('/Employees/GetShiftOfEmployees?id=' + data).success(callback);
        },
        GetRank: function (callback) {
            $http.get('/Employees/GetRank').success(callback);
        },
        GetManager: function (callback) {
            $http.get('/Employees/GetManager').success(callback);
        },
        //lấy đơn vị tổ chức Organizations
        getOrganizations: function (callback) {
            $http.get('/Employees/GetOrganizations').success(callback);
        },
        //Export Employee
        exportExcel: function (data, callback) {
            $http.post('/Employees/ExportDSNhanvien/', data).success(callback);
        },
       

        //========================End Sync Employee HRM to Employee SALE ======================
    }
});
app.filter('fdate', [
    '$filter', function ($filter) {
        return function (input, f) {
            if (input && input.toString().indexOf('Date') > -1) {
                return moment(input).format(f);
            } if (input && input.toString().indexOf('T') > -1) {
                return moment(input).format(f);
            } else return input;
        };
    }
]);
app.directive('ngFiles', ['$parse', function ($parse) {
    function fn_link(scope, element, attrs) {
        var onChange = $parse(attrs.ngFiles);
        element.on('change', function (event) {
            onChange(scope, { $files: event.target.files });
        });
    };
    return { link: fn_link }
}])

var spassAresSlug = function (number, isview) {
    try {
        // khai bao bien su dung       
        var numbers = number; var length = numbers.length; if (isview) {
            // khi dang la hien thi        
            if (numbers.indexOf(".") != -1) {
                length = numbers.indexOf(".");
                numbers = number.replace(".", ",");
            }
        } else {
            // khi dang la input textbox           
            if (numbers.indexOf(",") != -1) length = numbers.indexOf(",");
        }
        // loai bo nhung ky tu khong can thiet       
        // check so 0         
        var isTrue = true; while (isTrue) {
            if (numbers[0] == "0") numbers = numbers.substr(1, numbers.length);
            else isTrue = false;
        };
        // check dau phay      
        numbers = numbers.split(",");
        var string = "";
        for (var i = 0; i < numbers.length; i++) {
            if (i == 0) string += numbers[i];
            else if (i == 1) string += "," + numbers[i];
            else string += numbers[i];
        } numbers = string;
        // dung chung cho 2 truong hop       
        var no = 3, index = length - no;
        while (index > 0) { numbers = numbers.substr(0, index) + '.' + numbers.substr(index); index -= no; };
        return numbers === "" ? 0 : numbers;
    } catch (ex) { }
    return numbers === "" ? 0 : numbers;
}
app.directive('formatsub', ['$filter', function ($filter) {
    return {
        require: '?ngModel', link: function (scope, elem, attrs, ctrl) {
            if (!ctrl) return; ctrl.$formatters.unshift(function (viewValue) {
                try {
                    plainNumber = viewValue.replace('.', ','); elem.val(spassAresSlug(plainNumber));
                    return spassAresSlug(plainNumber);
                } catch (ex) { return undefined; }
            }); ctrl.$parsers.unshift(function (viewValue) {
                plainNumber = viewValue.replace(/[^-0-9,]/g, '');
                elem.val(spassAresSlug(plainNumber)); return plainNumber.replace(/[,]/g, '.');
            });
        }
    };
}]);
app.filter('html', ['$sce', function ($sce) {
    return function (text) {
        return $sce.trustAsHtml(text);
    };
}]);
app.controller('Ctrl_ES_HR_Employees', function ($scope, $rootScope, $sce, $compile, $uibModal, dataservice) {
    $rootScope.go = function (path) {
        $location.path(path); return false;
    };
    $rootScope.loginName = $("#loginName").val();
    dataservice.getall(function (rs) {
        if (rs.length > 0) {
            $rootScope.EmployeeData = rs;
        }
    })
    $rootScope.HomeNumber = "";
    $rootScope.CommuneWard = "";
    $rootScope.DistrictID = "";
    //các hàm chuyển đổi định dạng /Date(785548800000)/
    $rootScope.convertToJSONDate = function (strDate) {
        if (strDate !== null && strDate !== "") {
            var Str = strDate.toString();
            if (Str.indexOf("/Date") >= 0) {

                return Str;
            } else {
                var newDate = new Date(strDate);
                return '/Date(' + newDate.getTime() + ')/';
            }
        }

    }
    $rootScope.validationOptions = {
        rules: {
            EmployeeId: {
                required: true,
                maxlength: 20
            },
            FullName: {
                required: true,
                maxlength: 60
            }, Title: {
                maxlength: 8
            },
            Identification: {
                maxlength: 12
            },
            AppliedDate: {
                required: true

            }
        },
        messages: {
            EmployeeId: {
                required: "Yêu cầu nhập mã nhân viên.",
                maxlength: "Mã nhân viên không vượt quá 20 ký tự."
            },
            FullName: {
                required: "Yêu cầu nhập họ tên nhân viên.",
                maxlength: "Họ tên nhân viên không vượt quá 60 ký tự."
            },
            Title: {
                maxlength: "Xưng danh không được vượt quá 8 kí tự."
            },
            Identification: {
                maxlength: "CMT không được vượt quá 12 kí tự."
            },
            AppliedDate: {
                required: "Yêu cầu nhập ngày áp dụng."
            }


        }
    }
    $rootScope.StatusData = [{
        Value: 1,
        Name: 'Hoạt động'
    }, {
        Value: 0,
        Name: 'Ngừng hoạt động'
    }];

    $rootScope.JIsActiveData = [

        {
            value: 1,
            text: 'Đang kiêm nhiệm'
        }, {
            value: 0,
            text: 'Ngừng kiêm nhiệm'
        }];
    $rootScope.GenderData = [
        {
            value: "M",
            text: 'Nam'
        }, {
            value: "F",
            text: 'Nữ'
        }];
    $rootScope.SalariedFlagData = [
        {
            value: null,
            text: 'Bỏ chọn'
        },
        {
            value: "M",
            text: 'Trả lương theo tháng'
        }, {
            value: "H",
            text: 'Trả lương theo giờ'
        }, {
            value: "T",
            text: 'Trả lương theo tuần'
        }, {
            value: "K",
            text: 'Làm khoán'
        }, {
            value: "O",
            text: 'Khác'
        }
    ];
    $rootScope.SalaryMethodData = [{
        value: null,
        text: 'Bỏ chọn'
    },
    {
        value: "CK",
        text: 'Chuyển khoản'
    }, {
        value: "TM",
        text: 'Tiền mặt'
    }, {
        value: "KH",
        text: 'Khác'
    }
    ];

    $rootScope.UniformData = [
        {
            value: null,
            text: 'Bỏ chọn'
        },
        {
            value: "N",
            text: 'Chưa bàn giao'
        }, {
            value: "Y",
            text: 'Đã bàn giao'
        }];

    $rootScope.StatusOfWorkData = [
        {
            value: 'OM',
            text: 'Nhân viên chính thức'

        }, {
            value: 'PE',
            text: 'Nhân viên thử việc'

        },
        {
            value: 'PT',
            text: 'Nhân viên thời vụ'

        },
        {
            value: 'ML',
            text: 'Nghỉ thai sản'

        },
        {
            value: 'OL',
            text: 'Nghỉ khác'

        },
        {
            value: 'LJ',
            text: 'Nhân viên nghỉ việc'

        }
       
    ];
    $rootScope.StatusOfWorkDataHistory = [
        {
            value: 'HT',
            text: 'Quá trình công tác'

        },
        {
            value: 'HC',
            text: 'Chuyển phòng ban'

        },
        {
            value: 'OM',
            text: 'Nhân viên chính thức'

        }, {
            value: 'PE',
            text: 'Nhân viên thử việc'

        },
        {
            value: 'PT',
            text: 'Nhân viên thời vụ'

        },
        {
            value: 'ML',
            text: 'Nghỉ thai sản'

        },
        {
            value: 'OL',
            text: 'Nghỉ khác'

        },
        {
            value: 'LJ',
            text: 'Nhân viên nghỉ việc'

        }

    ];
    $rootScope.StatusHDData = [
        {
            value: null,
            text: 'Bỏ chọn'
        },
        {
            value: '0',
            text: 'Hết hạn hợp đồng'
        }, {
            value: '1',
            text: 'Hết hạn thử việc'
        }
    ];
    $rootScope.MaritalStatusData = [
        {
            value: null,
            text: 'Bỏ chọn'
        },
        {
            value: 'S',
            text: 'Chưa kết hôn'
        }, {
            value: 'M',
            text: 'Đã kết hôn'
        }
    ];

    $rootScope.ContractTypeData = [
        {
            value: null,
            text: 'Bỏ chọn'
        },
        {
            value: 1,
            text: 'Không có thời hạn'
        }, {
            Value: 0,
            text: 'Có thời hạn'
        }];
    $rootScope.GenitiveIdData = [
        {
            value: null,
            text: 'Bỏ chọn'
        },
        {
            value: 'OSK',
            text: 'OSK'
        }, {
            value: '3Q',
            text: '3Q'
        }, {
            value: 'BCT',
            text: 'BCT'
        },
        {
            value: 'Fitek',
            text: 'Fitek'
        }

    ];



    $rootScope.fName = function (name) {
        try {
            if (name.length <= 0) {
                return { first: "", mid: "", last: "" };
            } else {
                var xtemp = name.split(" ");
                var itemp = xtemp.length;
                if (itemp == 1) {
                    return { first: xtemp[0], mid: "", last: "" };
                }
                if (itemp == 2) {
                    return { first: xtemp[0], mid: "", last: xtemp[1] };
                }
                var midName = "";
                for (var i = 1; i < itemp - 1; i++) {
                    if (i == itemp - 2) {
                        midName += xtemp[i];
                    } else {
                        midName += xtemp[i] + " ";
                    }

                }

                return { first: xtemp[0], mid: midName, last: xtemp[itemp - 1] };
            }
        } catch (ex) {
            return { first: "", mid: "", last: "" };
        }
    }
    $rootScope.fHo = function (ho, hodem, ten) {
        try {
            var ho1 = "";
            var hodem1 = "";
            var ten1 = "";
            if (ho !== "" && ho !== null && ho !== undefined) {
                ho1 = ho;
            }
            if (hodem !== "" && hodem !== null && hodem !== undefined) {
                hodem1 = hodem;
            }
            if (ten !== "" && ten !== null && ten !== undefined) {
                ten1 = ten;
            }

            if (ho !== "" && ho !== null && ho !== undefined && hodem !== "" && hodem !== null && hodem !== undefined && ten !== "" && ten !== null && ten !== undefined) {
                return ho1 + " " + hodem1 + " " + ten1;
            } else if (ho !== "" && ho !== null && ho !== undefined && hodem !== "" && hodem !== null && hodem !== undefined) {
                return ho1 + " " + hodem1;
            } else if (ho !== "" && ho !== null && ho !== undefined && ten !== "" && ten !== null && ten !== undefined) {
                return ho1 + " " + ten1;
            }
            else if (ten !== "" && ten !== null && ten !== undefined && hodem !== "" && hodem !== null && hodem !== undefined) {
                return hodem1 + " " + ten1;
            } else if (ho !== "" && ho !== null && ho !== undefined) {
                return ho1;
            }
            else if (hodem !== "" && hodem !== null && hodem !== undefined) {
                return hodem1;
            }
            else if (ten !== "" && ten !== null && ten !== undefined) {
                return ten1;
            }
            return "";

        } catch (ex) {
            return "";
        }
    }
    $rootScope.convertDateNow = function (datetime) {
        try {
            if (datetime !== null) {
                var newdate = new Date(datetime);
                var month = newdate.getMonth() + 1;
                var day = newdate.getDate();
                var year = newdate.getFullYear();
                var hh = newdate.getHours();
                var mm = newdate.getMinutes();
                if (month < 10)
                    month = "0" + month;
                if (day < 10)
                    day = "0" + day;
                return month + "/" + day + "/" + year;
            }
        }
        catch (ex) {
            return "";
        }
        return "";
    };
    $rootScope.convertDateSearch = function (datetime) {
        if (datetime !== null && datetime !== "" && datetime !== undefined) {
            var Str = datetime.toString();
            if (Str.indexOf("/Date") >= 0) {

                var newdate = new Date(parseInt(datetime.substr(6)));
                var month = newdate.getMonth() + 1;
                var day = newdate.getDate();
                var year = newdate.getFullYear();
                var hh = newdate.getHours();
                var mm = newdate.getMinutes();
                if (month < 10)
                    month = "0" + month;
                if (day < 10)
                    day = "0" + day;
                return month + "/" + day + "/" + year;
            } else {
                var value = datetime.split('/');

                return value[2] + '-' + value[1] + '-' + value[0];
            }
        }
        return null;
    };
    $rootScope.tinymceOptions = {
        selector: "textarea",
        plugins: [
            "advlist autolink lists link  charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern imagetools codesample toc"
        ],
        skin: "lightgray", language: "vi_VN",
        theme: "modern", height: 200,
        toolbar1: "undo redo | styleselect sizeselect fontselect fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent ",
        toolbar2: "print preview insert link | forecolor backcolor emoticons | codesample",
        image_advtab: true,
        fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
        file_browser_callback: function (field_name, url, type, win) {
            console.log(type + "|" + win);
            var connector = "/_FM/Index.aspx?f=L1B1Ymxpc2hpbmdJbWFnZXM=";
            var enableAutoTypeSelection = true;
            if (enableAutoTypeSelection && type) {
                connector += "&Type=" + type;
            }
            connector += "&Field=" + field_name;
            App.openPage(connector, "File Manager", 1150, 600);
        },
        relative_urls: false,
        remove_script_host: true,
        convert_urls: true
    };
    //0 11/11/2017
    //1 11/11/2017 22:22
    //2 123123123
    //3 type date
    //4 type data year month day
    $rootScope.ConDate = function (data, number) {
        try {
            if (data == null || data == "") {
                return '';
            }
            if (data !== null && data != "" && data != undefined) {
                try {
                    if (data.indexOf("SA") != -1 || data.indexOf("CH") != -1) {
                        if (data.indexOf("SA") != -1) { }
                        if (data.indexOf("CH") != -1) { }
                    }

                    if (data.indexOf('Date') != -1) {
                        data = data.substring(data.indexOf("Date") + 5);
                        data = parseInt(data);
                    }
                }
                catch (ex) { }
                var newdate = new Date(data);
                if (number == 3) {
                    return newdate;
                }
                if (number == 2) {
                    return "/Date(" + newdate.getTime() + ")/";
                }
                var month = newdate.getMonth() + 1;
                var day = newdate.getDate();
                var year = newdate.getFullYear();
                var hh = newdate.getHours();
                var mm = newdate.getMinutes();
                if (month < 10)
                    month = "0" + month;
                if (day < 10)
                    day = "0" + day;
                if (mm < 10)
                    mm = "0" + mm;
                if (number == 0) {
                    return todayDate = day + "/" + month + "/" + year;
                }
                if (number == 1) {
                    return todayDate = day + "/" + month + "/" + year + " " + hh + ":" + mm;
                }
                if (number == 4) {
                    return new Date(year, month - 1, day);
                }
            } else {
                return '';
            }
        } catch (ex) {
            return "";
        }
    }
    $rootScope.addPeriod = function (nStr) {
        if (nStr !== null && nStr !== "" && nStr !== undefined) {
            nStr = Math.round(parseFloat(nStr) * 100) / 100;

            nStr += "";
            if (nStr.indexOf(",") >= 0) {
                var x = nStr.split(",");
            } else {
                var x = nStr.split(".");
            }
            var x1 = x[0];
            var x2 = x.length > 1 && parseInt(x[1]) > 0 ? "," + x[1] : "";
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, "$1" + "." + "$2");
            }
            var Total = x1 + x2;
            return Total;

        }
        else {
            return 0;
        }
    };
    $rootScope.EditDate = function (datetime) {
        if (datetime !== null && datetime !== undefined) {
            var Str = datetime.toString();
            if (Str.indexOf("/Date") >= 0) {

                var newdate = new Date(parseInt(datetime.substr(6)));
                var month = newdate.getMonth() + 1;
                var day = newdate.getDate();
                var year = newdate.getFullYear();
                var hh = newdate.getHours();
                var mm = newdate.getMinutes();
                if (month < 10)
                    month = "0" + month;
                if (day < 10)
                    day = "0" + day;
                return year + "/" + month + "/" + day;
            } else {
                var value = datetime.split("/");

                return value[2] + "/" + value[1] + "/" + value[0];
            }
        }
        return null;
    };
    $rootScope.faddress = function (ho, hodem, ten) {
        try {
            var ho1 = "";
            var hodem1 = "";
            var ten1 = "";
            if (ho !== "" && ho !== null && ho !== undefined) {
                ho1 = ho;
            }
            if (hodem !== "" && hodem !== null && hodem !== undefined) {
                hodem1 = hodem;
            }
            if (ten !== "" && ten !== null && ten !== undefined) {
                ten1 = ten;
            }

            if (ho !== "" && ho !== null && ho !== undefined && hodem !== "" && hodem !== null && hodem !== undefined && ten !== "" && ten !== null && ten !== undefined) {
                return ho1 + " " + hodem1 + " " + ten1;
            } else if (ho !== "" && ho !== null && ho !== undefined && hodem !== "" && hodem !== null && hodem !== undefined) {
                return ho1 + " " + hodem1;
            } else if (ho !== "" && ho !== null && ho !== undefined && ten !== "" && ten !== null && ten !== undefined) {
                return ho1 + " " + ten1;
            }
            else if (ten !== "" && ten !== null && ten !== undefined && hodem !== "" && hodem !== null && hodem !== undefined) {
                return hodem1 + " " + ten1;
            } else if (ho !== "" && ho !== null && ho !== undefined) {
                return ho1;
            }
            else if (hodem !== "" && hodem !== null && hodem !== undefined) {
                return hodem1;
            }
            else if (ten !== "" && ten !== null && ten !== undefined) {
                return ten1;
            }
            return "";

        } catch (ex) {
            return "";
        }
    }
    $rootScope.ConvertTodayDate = function (date) {
        if (date !== null && date !== undefined) {
            var newdate = new Date(date);
            var month = newdate.getMonth() + 1;
            var day = newdate.getDate();
            var year = newdate.getFullYear();
            var hh = newdate.getHours();
            var mm = newdate.getMinutes();
            if (month < 10)
                month = "0" + month;
            if (day < 10)
                day = "0" + day;
            if (mm < 10)
                mm = "0" + mm;
            return todayDate = day + "/" + month + "/" + year;
        } else {
            return null;
        }
    }
    $rootScope.ReadDate = function (date) {
        if (date !== null && date !== undefined) {
            var newdate = new Date(date);
            var month = newdate.getMonth() + 1;
            var day = newdate.getDate();
            var year = newdate.getFullYear();
            var hh = newdate.getHours();
            var mm = newdate.getMinutes();
            var ss = newdate.getSeconds();
            if (month < 10)
                month = "0" + month;
            if (day < 10)
                day = "0" + day;
            if (hh < 10)
                hh = "0" + hh;
            if (mm < 10)
                mm = "0" + mm;
            if (ss < 10)
                ss = "0" + ss;
            return day + "/" + month + "/" + year + " " + hh + ":" + mm + ":" + ss;
        } else {
            return null;
        }
    }
    $rootScope.convertToDate = function (datetime) {
        if (datetime !== null && datetime !== undefined && datetime != "") {
            var Str = datetime.toString();
            if (Str.indexOf("/Date") >= 0) {

                var newdate = new Date(parseInt(datetime.substr(6)));
                var month = newdate.getMonth() + 1;
                var day = newdate.getDate();
                var year = newdate.getFullYear();
                var hh = newdate.getHours();
                var mm = newdate.getMinutes();
                if (month < 10)
                    month = "0" + month;
                if (day < 10)
                    day = "0" + day;
                return year + "/" + month + "/" + day;
            } else {
                var value = datetime.split('/');

                return value[2] + '/' + value[1] + '/' + value[0];
            }
        }
        return null;
    };
    $rootScope.convertDate = function (datetime) {
        try {
            if (datetime !== null) {
                var newdate = new Date(datetime);
                var month = newdate.getMonth() + 1;
                var day = newdate.getDate();
                var year = newdate.getFullYear();
                var hh = newdate.getHours();
                var mm = newdate.getMinutes();
                if (month < 10)
                    month = "0" + month;
                if (day < 10)
                    day = "0" + day;
                return month + "/" + day + "/" + year;
            }
        }
        catch (ex) {
            return "";
        }
        return "";
    };
    $rootScope.convertDateToString = function (date, type) {
        if (date != null) {
            var time = new Date(date);
            if (type == 'Date') {
                return (time.getDate() > 9 ? time.getDate() : '0' + time.getDate()) + '/' + ((time.getMonth() + 1) > 9 ? ('0' + (time.getMonth() + 1)) : ('0' + (time.getMonth() + 1))) + '/' + time.getFullYear();
            } if (type == 'Date HH:mm') {
                return (time.getDate() > 9 ? time.getDate() : '0' + time.getDate()) + '/' + ((time.getMonth() + 1) > 9 ? ('0' + (time.getMonth() + 1)) : ('0' + (time.getMonth() + 1))) + '/' + time.getFullYear() + ' ' + (time.getUTCHours() > 9 ? time.getUTCHours() : '0' + time.getUTCHours()) + ':' + (time.getUTCMinutes() > 9 ? time.getUTCMinutes() : '0' + time.getUTCMinutes());
            }
            else {
                return "/Date(" + time.getTime() + ")/";
            }
        }
        return date;
    }
    $rootScope.DateCurrent = function () {
        var newdate = new Date();
        var month = newdate.getMonth() + 1;
        var day = newdate.getDate();
        var year = newdate.getFullYear();
        var hh = newdate.getHours();
        var mm = newdate.getMinutes();

        if (month < 10)
            month = "0" + month;
        if (day < 10)
            day = "0" + day;
        if (mm < 10)
            mm = "0" + mm;
        return month + "/" + day + "/" + year;
    };
    $rootScope.loadData = function () {
        dataservice.getdepartment(function (rs1) {
            $rootScope.li1 = rs1;
        });
        dataservice.geteducation(function (rs) {
            $rootScope.li2 = [];
            $rootScope.li2.push({ value: null, text: "Bỏ chọn" });
            angular.forEach(rs, function (value, key) {
                $rootScope.li2.push({ value: value.value, text: value.text });
            })

        });
        dataservice.getshift(function (rs3) {
            $rootScope.li3 = [];
            $rootScope.li3 = rs3;
        });
        dataservice.getjobTitle(function (rs) {
            $rootScope.li4 = [];
            $rootScope.li4.push({ value: null, text: "Bỏ chọn" });
            angular.forEach(rs, function (value, key) {
                $rootScope.li4.push({ value: value.value, text: value.text });
            })

        });

        dataservice.getLanguages(function (rs8) {
            $rootScope.li8 = rs8;
        });
        dataservice.gettreedataadd(null, function (result) {
            $rootScope.treeData = result;
        });
        dataservice.getListReasonLeaves(function (rs) {
            $rootScope.listReasonLeaves = [];
            $rootScope.listReasonLeaves.push({ value: null, text: "Bỏ chọn" });
            angular.forEach(rs, function (value, key) {
                $rootScope.listReasonLeaves.push({ value: value.value, text: value.text });
            })
        });

        dataservice.getEthnic(function (rs) {
            $rootScope.listEthnic = [];
            $rootScope.listEthnic.push({ value: null, text: "Bỏ chọn" });
            angular.forEach(rs, function (value, key) {
                $rootScope.listEthnic.push({ value: value.value, text: value.text });
            })
        });
        dataservice.getBank(function (rs) {
            $rootScope.listBank = [];
            $rootScope.listBank.push({ value: null, text: "Bỏ chọn" });
            angular.forEach(rs, function (value, key) {
                $rootScope.listBank.push({ value: value.value, text: value.text });
            })
        });
        dataservice.GetProfestionals(function (rs) {
            $rootScope.listProfestionals = [];
            $rootScope.listProfestionals.push({ value: null, text: "Bỏ chọn" });
            angular.forEach(rs, function (value, key) {
                $rootScope.listProfestionals.push({ value: value.value, text: value.text });
            })
        });
        dataservice.GetSpecializes(function (rs) {
            $rootScope.listSpecializes = [];
            $rootScope.listSpecializes.push({ value: null, text: "Bỏ chọn" });
            angular.forEach(rs, function (value, key) {
                $rootScope.listSpecializes.push({ value: value.value, text: value.text });
            })
        });
        dataservice.GetSalaryTier(function (rs) {
            $rootScope.SalaryTierIdData = [];
            $rootScope.SalaryTierIdData.push({ value: null, text: "Bỏ chọn" });
            angular.forEach(rs, function (value, key) {
                $rootScope.SalaryTierIdData.push({ value: value.value, text: value.text });
            })
        });
        dataservice.GetRank(function (rs) {
            $rootScope.RankData = [];
            $rootScope.RankData.push({ value: null, text: "Bỏ chọn" });
            angular.forEach(rs, function (value, key) {
                $rootScope.RankData.push({ value: value.value, text: value.text });
            })
        });
        dataservice.GetManager(function (rs) {
            $rootScope.ManagerData = [];
            $rootScope.ManagerData.push({ value: null, text: "Bỏ chọn" });
            angular.forEach(rs, function (value, key) {
                $rootScope.ManagerData.push({ value: value.value, text: value.text });
            })
        });
        // lấy đơn vị tổ chức
        dataservice.getOrganizations(function (rs) {
            $rootScope.ListOrganizations = [];
            $rootScope.ListOrganizations.push({ value: null, text: "Bỏ chọn" });
            angular.forEach(rs, function (value, key) {
                $rootScope.ListOrganizations.push({ value: value.OrganizationId, text: value.OrganizationName });
            })
        });


    }
    $rootScope.loadData();
    $rootScope.ListAddPositionsConcurrently = [];
});
app.config(function ($routeProvider, $validatorProvider) {
    $routeProvider
        .when('/', {
            templateUrl: ctxfolder + '/index.html',
            controller: 'index'
        })
        .when('/edit/:id', {
            templateUrl: ctxfolder + '/edit.html',
            controller: 'edit'
        })
        .when('/add/', {
            templateUrl: ctxfolder + '/add.html',
            controller: 'add'
        })
        .when('/address/', {
            templateUrl: ctxfolder + '/address.html',
            controller: 'address'
        })
    $validatorProvider.setDefaults({
        errorElement: 'span',
        errorClass: 'help-block',
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        success: function (label) {
            label.closest('.form-group').removeClass('has-error');
        }
    });
});
app.controller('index', function ($http, $scope, $timeout, $rootScope, $compile, $confirm, $ngConfirm, $uibModal, DTOptionsBuilder, DTColumnBuilder, DTInstances, dataservice, keyhandle) {
    $scope.model = {};
    $rootScope.FULLCONTROL = App.Permissions.FULLCONTROL;
    $rootScope.OPEN = App.Permissions.OPEN;
    $rootScope.NameLogin = document.getElementById("urlStatic").value;
    // Hiển thị đơn vị tổ chức
    $scope.jstreeData = [];
    $scope.ignoreChanges = false;
    $scope.DepartmentID = null;
    $scope.lstDepartmentID = [];
    $scope.reloadTree = function () {
        $scope.model.reSelected = [];
        $scope.jstreeData = [];
        dataservice.getdepartment(function (rs) {
            if (rs.Error) {
                App.notifyDanger("Có lỗi khi lấy dữ liệu.");
            } else {
                $scope.jstreeData = initJsTreeData(rs);
            }
            $scope.treeConfig.version++;
        });
    }
    $scope.reloadTree();
    $scope.applyModelChanges = function () {
        return !$scope.ignoreChanges;
    };
    $scope.treeConfig = {
        core: {
            error: function (error) {
                $log.error('treeCtrl: error from js tree - ' + angular.toJson(error));
            },
            check_callback: true, worker: true
        },
        "checkbox": {
            "three_state": true,
            "whole_node": false,
            "keep_selected_style": true,
            "cascade": "undetermined"
        },
        plugins: ["checkbox", 'types', 'state'],
        version: 1,
        types: {
            valid_children: ["selected"],
            types: {
                "selected": {
                    "select_node": false
                }
            }
        }
    };
    $scope.applyModelChanges = function () {
        return !$scope.ignoreChanges;
    };
    $scope.readyCB = function () {
        $timeout(function () {
            $scope.ignoreChanges = false;
        });
    }
    $scope.lstOrganizationGuid = [];
    $scope.changeNode = function (event, data) {
        var items = $scope.treeInstance.jstree(true).get_selected();

        if (items.length > 0) {
            $scope.DepartmentID = items[0];
            $scope.lstDepartmentID = [];
            $scope.lstDepartmentID = items;
            $rootScope.reloadEmployee();
        } else {
            $scope.lstDepartmentID = [];
            $rootScope.reloadEmployee();
        }
    }
    $scope.choose = function (item) {
        item.Active = !item.Active;
    }
    // Hiển thị danh sách nhân viên
    var vm = $scope;
    var ctrl = $scope;
    ctrl.nextRow = function (d) {
        if (ctrl.ElementRowCheck === -1) {
            if (ctrl.allRow.length > 1) {
                ctrl.ElementRowCheck = ctrl.allRow[0].row;
                $('td', ctrl.ElementRowCheck).addClass("es-iSeleted");
                ctrl.iSelected = ctrl.allRow[0].data;
                ctrl.open_Detail(ctrl.iSelected.QuotationNo, ctrl.iSelected.CurrencyRate);

            }
        } else
            for (var i = 0; i < ctrl.allRow.length; i++) {
                if (ctrl.allRow[i].row === ctrl.ElementRowCheck) {
                    if (i === ctrl.allRow.length - 1) break;
                    $('td', ctrl.ElementRowCheck).removeClass('es-iSeleted');
                    ctrl.ElementRowCheck = ctrl.allRow[i + 1].row;
                    $('td', ctrl.ElementRowCheck).addClass("es-iSeleted");
                    ctrl.iSelected = ctrl.allRow[i + 1].data;
                    ctrl.open_Detail(ctrl.iSelected.QuotationNo, ctrl.iSelected.CurrencyRate);
                    break;
                }
            }
    }
    ctrl.upRow = function (d) {
        if (ctrl.ElementRowCheck === -1) {
            if (ctrl.allRow.length > 1) {
                ctrl.ElementRowCheck = ctrl.allRow[ctrl.allRow.length - 1].row;
                $('td', ctrl.ElementRowCheck).addClass("es-iSeleted");
                ctrl.iSelected = ctrl.allRow[ctrl.allRow.length - 1].row;
                ctrl.open_Detail(ctrl.iSelected.QuotationNo, ctrl.iSelected.CurrencyRate);
            }
        } else
            for (var i = 0; i < ctrl.allRow.length; i++) {
                if (ctrl.allRow[i].row === ctrl.ElementRowCheck) {
                    if (i === 0) break;
                    $('td', ctrl.ElementRowCheck).removeClass('es-iSeleted');
                    ctrl.ElementRowCheck = ctrl.allRow[i - 1].row;
                    $('td', ctrl.ElementRowCheck).addClass("es-iSeleted");
                    ctrl.iSelected = ctrl.allRow[i - 1].data;
                    ctrl.open_Detail(ctrl.iSelected.QuotationNo, ctrl.iSelected.CurrencyRate);
                    break;
                }
            }
    }
    // export excel
    $scope.exportExcel = function () {
        dataservice.exportExcel({ Keyword: $scope.staticParam.Keyword, lstDepartmentID: $scope.lstDepartmentID, StartDate: $rootScope.convertDateSearch($scope.staticParam.startdate), EndDate: $rootScope.convertDateSearch($scope.staticParam.endate), Status: $scope.staticParam.StatusOfWork }, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
                return;
            }
            else {
                window.location.href = rs.Title;
            }
        });
    };
    keyhandle.eventCtrl(40, ctrl.nextRow);
    keyhandle.eventCtrl(38, ctrl.upRow);
    ctrl.ElementRowCheck = -1;
    ctrl.allRow = [];
    ctrl.iSelected = {};
    ctrl.dataScrollPage = [];
    var date = new Date();
    var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
    var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
    $scope.staticParam = {
        totalItems: 0,
        currentPage: 1,
        pageSize: 20,
        maxSize: 5,
        Keyword: "",
        JobTitleId: null,
        lstDepartmentID: null,
        StatusOfWork: ["OM"],
        EmployeeGuid: null,
        EmployeeId: null,
        FullName: "",
        StatusHD: ""
    };
    $scope.txtComment = "";
    $scope.dataload = [];
    $scope.TotalRow = 0;
    $scope.CheckReload = false;
    $scope.lstInt = [];
    $rootScope.TongGT = 0;
    $scope.selected = [];
    $scope.selectAll = false;
    $scope.toggleAll = toggleAll;
    $scope.toggleOne = toggleOne;
    // var titleHtml = '<label class="mt-checkbox"><input type="checkbox" ng-model="selectAll" ng-click="toggleAll(selectAll, selected)"/><span></span></label>';

    vm.dtOptions = DTOptionsBuilder.newOptions()
        .withOption('serverSide', true)
        .withOption('order', [2, 'asc'])
        .withDataProp('data')
        .withOption('pageLength', 20)
        .withOption('initComplete', function (settings, json) {
            //chỉ dùng khi có footer
            //ẩn scroll-x body
            $('.dataTables_scrollBody').css({
                'overflow-y': 'scroll',
                'overflow-x': 'hidden',
                'border': 'none'
            });
            //hiền scroll footer
            $('#tblData_wrapper .dataTables_scrollFoot').css('overflow', 'auto');
            //cuộn toàn bảng theo footer
            $('#tblData_wrapper .dataTables_scrollFoot').on('scroll', function () {
                $('#tblData_wrapper .dataTables_scrollBody').scrollLeft($(this).scrollLeft());
            });

            //thêm sự kiện scroll phân trang
            $('#tblData').parent().attr("onscroll", "angular.element(this).scope().LoadScroll(this)");
        })
        .withOption('searching', false)
        .withOption('createdRow', function (row, data, dataIndex) {
            const contextScope = $scope.$new(true);
            contextScope.data = data;
            $scope.data = data;
            var obj = {
                STT: data._STT.toString(),
                Id: data.EmployeeGuid
            }
            $scope.lstInt.push(obj);
            $compile(row)($scope);
            if (data.CandidateGuid !== null && data.CandidateGuid !== undefined && data.CandidateGuid !== "" && data.CandidateGuid !== "00000000-0000-0000-0000-000000000000") {
                contextScope.contextMenu = $scope.contextMenuCandidate;
            } else {
                contextScope.contextMenu = $scope.contextMenu;
            }
            $compile(angular.element(row).find('input'))($scope);
            $compile(angular.element(row).attr('context-menu', 'contextMenu'))(contextScope);
        });
    vm.dtOptions.withOption('ajax', function (data, callback, settings) {
        data.lstDepartmentID = $scope.lstDepartmentID;
        data.Keyword = $scope.staticParam.Keyword;
        data.Status = $scope.staticParam.StatusOfWork;
        data.JobTitleId = $scope.staticParam.JobTitleId !== 'null' ? $scope.staticParam.JobTitleId : null;
        data.StartDate = $rootScope.convertDateSearch($scope.staticParam.startdate);
        data.EndDate = $rootScope.convertDateSearch($scope.staticParam.endate);
        $scope.staticParam.currentPage = $scope.dtInstance.DataTable.page() + 1;
        if ($scope.CheckReload) {
            data.Start = 0;
        }
        App.blockUI({
            target: "#table_load",
            boxed: true,
            message: 'Đang tải...'
        });
        $http.post('/Employees/JTable', data)
            .success(function (res) {

                if (data.start === 0) {
                    $scope.dataload = [];
                    $scope.TotalRow = res.recordsTotal;
                    angular.forEach(res.data, function (val, key) {
                        $scope.dataload.push(val);
                    });
                    callback({
                        data: $scope.dataload,
                        recordsTotal: res.recordsTotal,
                        recordsFiltered: res.recordsFiltered
                    });
                }
                else if (res.recordsTotal > $scope.dataload.length) {
                    angular.forEach(res.data, function (val, key) {
                        $scope.dataload.push(val);
                    });
                    $scope.TotalRow = res.recordsTotal;
                    callback({
                        data: $scope.dataload,
                        recordsTotal: res.recordsTotal,
                        recordsFiltered: res.recordsFiltered
                    });
                }

                App.unblockUI("#table_load");

            });
    });
    //scroll theo tỷ lệ màn hình
    vm.dtOptions.withOption('scrollY', '68vh')
        .withOption('scrollX', '100%') //mặc định 100%
        .withOption('scrollCollapse', true) //xóa khoảng trắng khi dữ liệu trong bảng ít
        .withOption('rowCallback', rowCallback) //sự kiên click vào 1 dòng
        .withOption('scroller', {
            loadingIndicator: true
        }).withFixedColumns({
            leftColumns: 0 //fix bên trái 2 cột
            //rightColumns: 1 //fix bên phải 1 cột
        });


    vm.dtColumns = [];

    vm.dtColumns.push(DTColumnBuilder.newColumn('_STT').withTitle('STT').notSortable().withOption('sClass', 'tcenter').withOption('sWidth', '5px').renderWith(function (data, type, full, meta) {
        return meta.row + 1;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('EmployeeID').withTitle('Ảnh').notSortable().withOption('sWidth', '40px').renderWith(function (data, type, full, meta) {
        if (full.PhotoTitle !== null && full.PhotoTitle !== "") {
            return '<img width="30px" height = "35px" src="' + "/Employees/GetPicThumb/" + full.EmployeeGuid + '" />';

        } else {
            if (full.Gender === "M") {
                return '<img width="30px" height = "35px" src="/images/male.jpeg" />';
            } else {
                return '<img width="30px" height = "35px" src="/images/female.jpeg" />';
            }
        }

    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('EmployeeID').withTitle('Mã NV').withTitleDescription('Mã nhân viên').withOption('sWidth', '60px').renderWith(function (data, type, full, meta) {
        return data;

    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('FullName').withTitle('Họ tên').withOption('sWidth', '70px').renderWith(function (data, type, full, meta) {
        //return '<a href="" ng-click="XemnhansuNhanh(\'' + full.EmployeeGuid + '\',\'' + full.EmployeeID + '\')" title="Xem thông tin nhân viên">' + data + '</a>';
        return data;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('StartDate').withTitle('Ngày vào').withOption('sWidth', '60px').renderWith(function (data, type, full, meta) {
        if (data !== null && data !== undefined) {
            return full.StartDateString;
        } else {
            return "";
        }
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('HomeMobile').withTitle('Số ĐT').withTitleDescription('Số điện thoại').withOption('sWidth', '60px').renderWith(function (data, type, full, meta) {
        return full.Phone;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('DepartmentID').withTitle('Phòng ban').withTitleDescription('Phòng ban').withOption('sWidth', '70px').renderWith(function (data, type, full, meta) {
        return full.DepartmentName;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('JobTitleID').withTitle('Chức vụ').withOption('sWidth', '60px').renderWith(function (data, type, full, meta) {
        return full.JobTitlesName;
    }));
    //vm.dtColumns.push(DTColumnBuilder.newColumn('ProfestionaName').withTitle('Công việc').withOption('sWidth', '70px').renderWith(function (data, type, full, meta) {
    //    return data;
    //}));


    vm.reloadData = reloadData;
    vm.dtInstance = {};
    function reloadData(resetPaging) {
        $scope.staticParam.currentPage = resetPaging;
        vm.dtInstance.reloadData(callback, resetPaging);

    }
    function callback(json) {

    }
    function toggleAll(selectAll, selectedItems) {
        for (var id in selectedItems) {
            if (selectedItems.hasOwnProperty(id)) {
                selectedItems[id] = selectAll;
            }
        }
    }
    function toggleOne(selectedItems) {
        for (var id in selectedItems) {
            if (selectedItems.hasOwnProperty(id)) {
                if (!selectedItems[id]) {
                    vm.selectAll = false;
                    return;
                }
            }
        }
        vm.selectAll = true;
    }
    $scope.total = 0;
    $scope.check = false;
    $scope.LoadScroll = function (obj) {
        var total = obj.offsetHeight + obj.scrollTop;
        if (obj.offsetHeight + obj.scrollTop >= obj.scrollHeight && $scope.check && $scope.total <= total) {
            $scope.check = false;
            try {
                $scope.CheckReload = false;
                vm.dtInstance.DataTable.page('next').draw('page');
            }
            catch (ex) {
            }
        }
        if (obj.offsetHeight + obj.scrollTop >= obj.scrollHeight) {
            $scope.total = obj.offsetHeight + obj.scrollTop;
        }
        else {
            $scope.check = true;
        }

    };


    $('#minize-menuleft').click(function () {
        vm.dtInstance.DataTable.draw();
    });
    function rowCallback(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        if (iDisplayIndexFull === 0) ctrl.allRow = [];
        $('td', nRow).unbind('click');
        $('td', nRow).bind('click', function () {
            if (vm.ElementRowCheck === nRow) return;
            $('td', nRow).addClass("es-iSeleted");
            $('td', vm.ElementRowCheck).removeClass('es-iSeleted');
            vm.ElementRowCheck = nRow;
            ctrl.ElementRowCheck = nRow;
            ctrl.iSelected = aData;
            vm.staticParam.EmployeeGuid = aData.EmployeeGuid;
            vm.staticParam.EmployeeId = aData.EmployeeID;
            vm.staticParam.FullName = aData.FullName;

        });
        $('td', nRow).unbind('dblclick');
        $('td', nRow).bind('dblclick', function () {
            $scope.$apply(function () {
                $scope.XemnhansuNhanh(aData.EmployeeGuid, aData.EmployeeID);
            });
        });
        ctrl.allRow.push({ row: nRow, data: aData });
        return nRow;
    }

    $rootScope.reloadEmployee = function () {
        $scope.lstInt = [];
        $scope.dataload = [];
        reloadData(true);
    };
    $scope.initDate = {
        reloadData: {}
    }
    $scope.reloadALL = function () {
        $scope.staticParam.SearchWards = "";
        $scope.staticParam.CurrencyRate = 0;
        $scope.staticParam.OrderStatus = 0;
        $scope.staticParam.ObjectId = "null";
        $scope.staticParam.StatusOfWork = ['OM'];
        $scope.staticParam.startdate = null;
        $scope.staticParam.endate = null;
        $scope.initDate.reloadData.reloadDefault();
        $scope.lstInt = [];
        $scope.dataload = [];
        reloadData(true);
        $('#tblData').parent().removeAttr("onscroll");

    };
    $scope.StatusOfWorkDataChange = function (rs) {
        $scope.staticParam.StatusOfWork = rs;
        $scope.dataload = [];
        reloadData(true);

    }
    $scope.StatusHDDataChange = function (rs) {
        $scope.staticParam.StatusHD = rs;
        $scope.dataload = [];
        reloadData(true);

    }
    $scope.StatusOfWorkDataConfig = {
        placeholder: 'Chọn tình trạng',
        selectAll: true,
        search: true,
        searchText: 'Tìm kiếm...',
        showTitle: 'true',
        csvSepChar: ',',
        captionFormat: '{0} lựa chọn',
        captionFormatAllSelected: '{0} lựa chọn',
        noMatch: 'Không có dữ liệu "{0}"',
        okCancelInMulti: true,
        locale: ['Đồng ý', 'Hủy', 'Chọn tất cả']
    };
    $scope.StatusHDDataConfig = {
        placeholder: 'Chọn tình trạng',
        selectAll: true,
        search: true,
        searchText: 'Tìm kiếm...',
        showTitle: 'true',
        csvSepChar: ',',
        captionFormat: '{0} lựa chọn',
        captionFormatAllSelected: '{0} lựa chọn',
        noMatch: 'Không có dữ liệu "{0}"',
        okCancelInMulti: true,
        locale: ['Đồng ý', 'Hủy', 'Chọn tất cả']
    };
    $scope.$watch('staticParam.startdate', function (newvalue, oldvalue) {
        if (newvalue !== undefined && newvalue !== oldvalue) {
            $rootScope.reloadEmployee();
        }
    });
    $scope.$watch('staticParam.endate', function (newvalue, oldvalue) {
        if (newvalue !== undefined && newvalue !== oldvalue) {
            $rootScope.reloadEmployee();
        }
    });
    $scope.search = function () {
        $rootScope.reloadEmployee();

    };
    $scope.ObjectIdConfig = { search: true };
    $scope.ListObjectKHChange = function (rs) {
        $scope.staticParam.ObjectId = rs !== "null" ? rs : null;
        $rootScope.reloadEmployee();
    }
    $scope.changeOptionDate = function (d) {
        if (d.length > 0) {
            $scope.staticParam.startdate = $rootScope.ConDate(d[0], 2);
            $scope.staticParam.endate = $rootScope.ConDate(d[1], 2);
        } else {
            $scope.staticParam.startdate = null;
            $scope.staticParam.endate = null;
        }
    }
    $scope.dhm = function (ms) {
        days = Math.floor(ms / (24 * 60 * 60 * 1000));
        daysms = ms % (24 * 60 * 60 * 1000);
        hours = Math.floor((daysms) / (60 * 60 * 1000));
        hoursms = ms % (60 * 60 * 1000);
        minutes = Math.floor((hoursms) / (60 * 1000));
        minutesms = ms % (60 * 1000);
        sec = Math.floor((minutesms) / (1000));
        return days;
    }
    $scope.add_Employees = function () {
        if ($scope.DepartmentID !== undefined && $scope.DepartmentID !== "" && $scope.DepartmentID !== null) {
            $rootScope.Address1 = "";
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: ctxfolder + '/add.html',
                controller: 'add',
                backdrop: 'static',
                keyboard: false,
                size: '100',
                resolve: {
                    para: function () {
                        return $scope.DepartmentID;
                    }

                }
            });
            modalInstance.result.then(function (d) {
                $rootScope.reloadEmployee();
            }, function () {
            });
        } else {
            App.notifyDanger("Yêu cầu chọn phòng ban trước khi thêm!");
            return;
        }
    }
    $scope.ImportLuong = function () {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'ImportLuong.html',
            controller: 'ImportLuong',
            backdrop: 'static',
            keyboard: false,
            size: 'max',
            resolve: {
                para: function () {
                    return "";
                },

            }
        });
        modalInstance.result.then(function (d) {

        }, function () {
        });
    }


    $scope.contextMenuCandidate = [
        [function ($itemScope) {
            return '<i class="mdi mdi-eye"></i> Xem';
        }, function ($itemScope, $event, model) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: ctxfolder + '/view.html',
                controller: 'view',
                backdrop: 'static',
                keyboard: false,
                size: '100',

                resolve: {
                    para: function () {
                        return $itemScope.data.EmployeeGuid;
                    },
                    para1: function () {
                        return $itemScope.data.EmployeeID;
                    }
                }
            });

        }, function ($itemScope, $event, model) {
            return App.Permissions.OPEN;
        }],
        [function ($itemScope) {
            return '<i class="mdi mdi-table-edit"></i> Sửa';
        }, function ($itemScope, $event, model) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: ctxfolder + '/edit.html',
                controller: 'edit',
                backdrop: 'static',
                keyboard: false,
                size: '100',
                resolve: {
                    para: function () {
                        return $itemScope.data.EmployeeGuid;
                    }
                }
            });
            modalInstance.result.then(function (d) {
                $rootScope.reloadEmployee();
            }, function () {
            });
        }, function ($itemScope, $event, model) {
            return App.Permissions.FULLCONTROL;
        }],
        [function ($itemScope) {
            return '<i class="mdi mdi-delete"></i> Xóa';
        }, function ($itemScope, $event, model) {
            ESConfirm([{ 'value': 1, 'title': 'Xác nhận', 'type': 'info' }, { 'value': 2, 'title': 'Đóng', 'type': 'danger' }], { 'description': '[' + $itemScope.data.FullName + '] ?', 'class': 'eswarning' }, function (rs) {
                if (rs === '1') {
                    App.blockUI({
                        target: "#contentMain",
                        boxed: true,
                        message: 'Đang tải...'
                    });
                    dataservice.delete($itemScope.data.EmployeeGuid, function (result) {
                        if (result.Error) {
                            App.notifyDanger(result.Title);
                        } else {
                            App.notifyInfo(result.Title);
                            $rootScope.reloadEmployee();
                        }
                        App.unblockUI("#contentMain");
                    });
                }
            });
        }, function ($itemScope, $event, model) {
            return App.Permissions.FULLCONTROL;
        }]

    ];
    $scope.contextMenu = [
        [function ($itemScope) {
            return '<i class="mdi mdi-eye"></i> Xem';
        }, function ($itemScope, $event, model) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: ctxfolder + '/view.html',
                controller: 'view',
                backdrop: 'static',
                keyboard: false,
                size: '100',

                resolve: {
                    para: function () {
                        return $itemScope.data.EmployeeGuid;
                    },
                    para1: function () {
                        return $itemScope.data.EmployeeID;
                    }
                }
            });

        }, function ($itemScope, $event, model) {
            return App.Permissions.OPEN;
        }],
        [function ($itemScope) {
            return '<i class="mdi mdi-table-edit"></i> Sửa';
        }, function ($itemScope, $event, model) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: ctxfolder + '/edit.html',
                controller: 'edit',
                backdrop: 'static',
                keyboard: false,
                size: '100',
                resolve: {
                    para: function () {
                        return $itemScope.data.EmployeeGuid;
                    }
                }
            });
            modalInstance.result.then(function (d) {
                $rootScope.reloadEmployee();
            }, function () {
            });
        }, function ($itemScope, $event, model) {
            return App.Permissions.FULLCONTROL;
        }],

        [function ($itemScope) {
            return '<i class="mdi mdi-delete"></i> Xóa';
        }, function ($itemScope, $event, model) {
            ESConfirm([{ 'value': 1, 'title': 'Xác nhận', 'type': 'info' }, { 'value': 2, 'title': 'Đóng', 'type': 'danger' }], { 'description': '[' + $itemScope.data.FullName + '] ?', 'class': 'eswarning' }, function (rs) {
                if (rs === '1') {
                    App.blockUI({
                        target: "#contentMain",
                        boxed: true,
                        message: 'Đang tải...'
                    });
                    dataservice.delete($itemScope.data.EmployeeGuid, function (result) {
                        if (result.Error) {
                            App.notifyDanger(result.Title);
                        } else {
                            App.notifyInfo(result.Title);
                            $rootScope.reloadEmployee();
                        }
                        App.unblockUI("#contentMain");
                    });
                }
            });
        }, function ($itemScope, $event, model) {
            return App.Permissions.FULLCONTROL;
        }]

    ];
    //#endregion
    $scope.search = function () {

        $rootScope.reloadEmployee();
    }

    $scope.searchtxt = function () {
        //$scope.DepartmentID = "";
        //$scope.lstDepartmentID = [];

        $scope.Search.JobTitleID = "";
        $scope.Search.StartDate = null;
        $scope.Search.EndDate = null;
        $rootScope.reloadEmployee();
    }
    $scope.Refresh = function () {

        $scope.Search.Title = "";
        //$scope.DepartmentID = "";
        //$scope.lstDepartmentID = [];
        $scope.StatusOfWork = "EM";
        $scope.Search.JobTitleID = "";
        $scope.Search.StartDate = null;
        $scope.Search.EndDate = null;
        $rootScope.reloadEmployee();
    }

    $scope.searchDateEmployee = function () {

        $scope.Search.Title = "";
        $scope.Search.JobTitleID = "";
        $rootScope.reloadEmployee();
    }
    $scope.ChangeJobTitleID = function () {

        $scope.Search.Title = "";

        $scope.Search.StartDate = null;
        $scope.Search.EndDate = null;
        $rootScope.reloadEmployee();
    }
    $scope.XemnhansuNhanh = function (Id, EmployeeID) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/view.html',
            controller: 'view',
            backdrop: 'static',
            keyboard: false,
            size: '100',
            resolve: {
                para: function () {
                    return Id;
                },
                para1: function () {
                    return EmployeeID;
                }
            }
        });
    }
    $scope.Suanhansunhanh = function () {
        if (vm.staticParam.EmployeeGuid === null || vm.staticParam.EmployeeGuid === "" || vm.staticParam.EmployeeGuid === undefined) {
            App.notifyDanger("Yêu cầu chọn nhân viên !");
            return;
        }
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/edit.html',
            controller: 'edit',
            backdrop: 'static',
            keyboard: false,
            size: '100',
            resolve: {
                para: function () {
                    return vm.staticParam.EmployeeGuid;
                }
            }
        });
        modalInstance.result.then(function (d) {
            $rootScope.reloadEmployee();
        }, function () {
            return App.Permissions.FULLCONTROL;
        });

    }
    $scope.Xemnhansu = function () {
        if (!App.Permissions.OPEN) return;

        if (vm.staticParam.EmployeeGuid === null || vm.staticParam.EmployeeGuid === "" || vm.staticParam.EmployeeGuid === undefined) {
            App.notifyDanger("Yêu cầu chọn nhân viên !");
            return;
        }
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/view.html',
            controller: 'view',
            backdrop: 'static',
            keyboard: false,
            size: '100',

            resolve: {
                para: function () {
                    return vm.staticParam.EmployeeGuid;
                },
                para1: function () {
                    return vm.staticParam.EmployeeId;
                }
            }
        });
        modalInstance.result.then(function (d) {
            $rootScope.reloadEmployee();
        }, function () {
        });

    }
    $scope.Suanhansu = function () {
        if (!App.Permissions.FULLCONTROL) return;
        if (vm.staticParam.EmployeeGuid === null || vm.staticParam.EmployeeGuid === "" || vm.staticParam.EmployeeGuid === undefined) {
            App.notifyDanger("Yêu cầu chọn nhân viên !");
            return;
        }
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/edit.html',
            controller: 'edit',
            backdrop: 'static',
            keyboard: false,
            size: '100',
            resolve: {
                para: function () {
                    return vm.staticParam.EmployeeGuid;
                }
            }
        });
        modalInstance.result.then(function (d) {
            $rootScope.reloadEmployee();
        }, function () {

        });

    }
    $scope.Xemluong = function () {
        if (!App.Permissions.FULLCONTROL) return;
        if (vm.staticParam.EmployeeGuid === null || vm.staticParam.EmployeeGuid === "" || vm.staticParam.EmployeeGuid === undefined) {
            App.notifyDanger("Yêu cầu chọn nhân viên !");
            return;
        }
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/Salaries_index.html',
            controller: 'Salaries_index',
            backdrop: 'static',
            keyboard: false,
            size: '100',
            resolve: {
                para: function () {
                    return vm.staticParam.EmployeeGuid;
                },
                para1: function () {
                    return vm.staticParam.EmployeeId;
                },
                TitleIndex: function () {
                    return "Thông tin lương : " + vm.staticParam.FullName;
                }
            }
        });
        modalInstance.result.then(function (d) {
            $rootScope.reloadEmployee();
        }, function () {
        });

    }
    $scope.Xembaohiem = function () {
        if (!App.Permissions.FULLCONTROL) return;
        if (vm.staticParam.EmployeeGuid === null || vm.staticParam.EmployeeGuid === "" || vm.staticParam.EmployeeGuid === undefined) {
            App.notifyDanger("Yêu cầu chọn nhân viên !");
            return;
        }
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/Insurances_index.html',
            controller: 'Insurances_index',
            backdrop: 'static',
            keyboard: false,
            size: '100',
            resolve: {
                para: function () {
                    return vm.staticParam.EmployeeGuid;
                },
                para1: function () {
                    return vm.staticParam.EmployeeId;
                },
                TitleIndex: function () {
                    return "Thông tin bảo hiểm : " + vm.staticParam.FullName;
                }
            }
        });
        modalInstance.result.then(function (d) {
            $rootScope.reloadEmployee();
        }, function () {
        });

    }
    $scope.Xemhopdong = function () {
        if (!App.Permissions.FULLCONTROL) return;
        if (vm.staticParam.EmployeeGuid === null || vm.staticParam.EmployeeGuid === "" || vm.staticParam.EmployeeGuid === undefined) {
            App.notifyDanger("Yêu cầu chọn nhân viên !");
            return;
        }
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/Contracts_index.html',
            controller: 'Contracts_index',
            backdrop: 'static',
            keyboard: false,
            size: '100',
            resolve: {
                para: function () {
                    return vm.staticParam.EmployeeGuid;
                },
                para1: function () {
                    return vm.staticParam.EmployeeId;
                },
                TitleIndex: function () {
                    return "Thông tin hợp đồng : " + vm.staticParam.FullName;
                }
            }
        });
        modalInstance.result.then(function (d) {
            $rootScope.reloadEmployee();
        }, function () {
        });

    }
    $scope.Xemquatrinhcongtac = function () {
        if (!App.Permissions.FULLCONTROL) return;
        if (vm.staticParam.EmployeeGuid === null || vm.staticParam.EmployeeGuid === "" || vm.staticParam.EmployeeGuid === undefined) {
            App.notifyDanger("Yêu cầu chọn nhân viên !");
            return;
        }
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/Histories_index.html',
            controller: 'Histories_index',
            backdrop: 'static',
            keyboard: false,
            size: '100',
            resolve: {
                para: function () {
                    return vm.staticParam.EmployeeGuid;
                },
                para1: function () {
                    return vm.staticParam.EmployeeId;
                },
                TitleIndex: function () {
                    return "Thông tin quá trình công tác : " + vm.staticParam.FullName;
                }
            }
        });
        modalInstance.result.then(function (d) {
            $rootScope.reloadEmployee();
        }, function () {
        });

    }
    $scope.Xemthiduakhenthuong = function () {
        if (!App.Permissions.FULLCONTROL) return;
        if (vm.staticParam.EmployeeGuid === null || vm.staticParam.EmployeeGuid === "" || vm.staticParam.EmployeeGuid === undefined) {
            App.notifyDanger("Yêu cầu chọn nhân viên !");
            return;
        }
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/RewardDisciplines_index.html',
            controller: 'RewardDisciplines_index',
            backdrop: 'static',
            keyboard: false,
            size: '100',
            resolve: {
                para: function () {
                    return vm.staticParam.EmployeeGuid;
                },
                para1: function () {
                    return vm.staticParam.EmployeeId;
                },
                TitleIndex: function () {
                    return "Thông tin khen thưởng, kỷ luật : " + vm.staticParam.FullName;
                }
            }
        });
        modalInstance.result.then(function (d) {
            $rootScope.reloadEmployee();
        }, function () {
        });

    }
    $scope.Xemchucchichungnhan = function () {
        if (!App.Permissions.FULLCONTROL) return;
        if (vm.staticParam.EmployeeGuid === null || vm.staticParam.EmployeeGuid === "" || vm.staticParam.EmployeeGuid === undefined) {
            App.notifyDanger("Yêu cầu chọn nhân viên !");
            return;
        }
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/Certificate_index.html',
            controller: 'Certificate_index',
            backdrop: 'static',
            keyboard: false,
            size: '100',
            resolve: {
                para: function () {
                    return vm.staticParam.EmployeeGuid;
                },
                para1: function () {
                    return vm.staticParam.EmployeeId;
                },
                TitleIndex: function () {
                    return "Thông tin chứng chỉ, chứng nhận : " + vm.staticParam.FullName;
                }
            }
        });
        modalInstance.result.then(function (d) {
            $rootScope.reloadEmployee();
        }, function () {
        });

    }
    $scope.Xemnquatrinhdaotao = function () {
        if (!App.Permissions.FULLCONTROL) return;
        if (vm.staticParam.EmployeeGuid === null || vm.staticParam.EmployeeGuid === "" || vm.staticParam.EmployeeGuid === undefined) {
            App.notifyDanger("Yêu cầu chọn nhân viên !");
            return;
        }
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/TrainingRequirements_index.html',
            controller: 'TrainingRequirements_index',
            backdrop: 'static',
            keyboard: false,
            size: '100',
            resolve: {
                para: function () {
                    return vm.staticParam.EmployeeGuid;
                },
                para1: function () {
                    return vm.staticParam.EmployeeId;
                }
                ,
                TitleIndex: function () {
                    return "Thông tin quá trình đào tạo : " + vm.staticParam.FullName;
                }

            }
        });
        modalInstance.result.then(function (d) {
            $rootScope.reloadEmployee();
        }, function () {
        });

    }
    $scope.Xoanhansu = function () {
        if (!App.Permissions.FULLCONTROL) return;
        if (vm.staticParam.EmployeeGuid === null || vm.staticParam.EmployeeGuid === "" || vm.staticParam.EmployeeGuid === undefined) {
            App.notifyDanger("Yêu cầu chọn nhân viên !");
            return;
        }

        ESConfirm([{ 'value': 1, 'title': 'Xác nhận', 'type': 'info' }, { 'value': 2, 'title': 'Đóng', 'type': 'danger' }], { 'description': 'nhân viên này không ?', 'class': 'eswarning' }, function (rs) {
            if (rs === '1') {
                App.blockUI({
                    target: "#contentMain",
                    boxed: true,
                    message: 'Đang tải...'
                });
                dataservice.delete(vm.staticParam.EmployeeGuid, function (result) {
                    if (result.Error) {
                        App.notifyDanger(result.Title);
                    } else {
                        App.notifyInfo(result.Title);
                        $rootScope.reloadEmployee();
                    }
                    App.unblockUI("#contentMain");
                });
            }
        });

    }
    $scope.open_Candidates = function (temp) {
        if (temp !== "" && temp !== undefined && temp !== null) {
            window.open("/HR/Candidates/Index#/?" + "OPEN#" + temp, "_parent");
        } else {
            App.notifyDanger("Không có nhân viên nào được chọn");
            return;
        }
    };
    $scope.model = {};
    $scope.DateCreate = $rootScope.convertDateNow(Date.now());
    $scope.DateCreate !== null ? $scope.model.DateBG = "/Date(" + new Date($scope.DateCreate).getTime() + ")/" : undefined;

    $scope.printClick = function () {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/report_AllEmployee.html',
            controller: 'report_AllEmployee',
            backdrop: 'static',
            size: '80',
            resolve: {
                Keyword: function () {
                    return $scope.staticParam.Keyword;
                },
                JobTitleId: function () {
                    return $scope.staticParam.JobTitleId !== 'null' ? $scope.staticParam.JobTitleId : null;
                },
                lstDepartmentID: function () {
                    return $scope.lstDepartmentID;
                },
                StartDate: function () {
                    return $scope.staticParam.startdate;
                }, EndDate: function () {
                    return $scope.staticParam.endate;
                },
                Status: function () {
                    return $scope.staticParam.StatusOfWork;
                }
            }
        })
    }
    $scope.UpdatePhoto = function () {
        dataservice.UpdatePhoto(function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
                return;
            }
            App.notifyInfo(rs.Title);
        })
    }
});
function initJsTreeData(data) {
    if (!data) {
        return null;
    }
    var jsData = [];
    data.forEach(function (item, index) {
        if (item.ParentId === null) {
            var treeItem = {
                id: item.DepartmentGuid,
                parent: '#',
                text: item.DepartmentName,
                state: {
                    opened: true
                }
            };
            jsData.push(treeItem);
            subJsTreeData(jsData, data, item.DepartmentGuid);
        }
    });
    return jsData;
}
function subJsTreeData(jsData, data, parentid) {
    data.forEach(function (item, index) {
        if (item.ParentId && item.ParentId === parentid) {
            var treeItem = {
                id: item.DepartmentGuid,
                parent: item.ParentId,
                text: item.DepartmentName,
                state: {
                    "opened": true
                }
            };
            jsData.push(treeItem);
            subJsTreeData(jsData, data, item.DepartmentGuid);
        }
    });

    return jsData;
}
// thêm mới nhân viên
app.controller('add', function ($scope, $rootScope, $compile, $uibModal, $confirm, $uibModalInstance, FileUploader, dataservice, para) {
    $scope.model = {
        IsActive: '1',
        LoginName: "",
        Kiemnhiem: false,
        MaritalStatus: 'S',
        EducationId: 5,
        JobTitleId: 244,
        DepartmentGuid: para,
        ProfestionalD: null,
        SpecializesId: null,
        ShiftID: 72,
        EthnicId: 1,
        CandidateId: null,
        SalaryMethod: 'M',
        ManagerId: null,
        StatusOfWork: 'OM',
        BankCode: null,
        SalaryPayBy: 'M',
        Gender: 'M',
        Uniform: null,
        Language1: [],
        imageLink: "/images/male.jpeg",
        GenitiveId: 'ESVN',
        SalaryTierId: null,
        ShiftId1: [72],
        ReasonLeaveId: null,
        OrganizationId: null,
        BankCode: 'VPBank',
        StartDate: $rootScope.convertToJSONDate(new Date())
        //EndDateWork : null
    };
    $scope.modelauto = {};
    //$scope.reloadnumberauto = function () {
    //    dataservice.GetNumberAuto({ AliasId: "QTDSCT_NV" }, function (rs) {
    //        $scope.modelauto.Value = rs.Value;
    //        $scope.modelauto.IsEdit = rs.IsEdit;
    //        $scope.model.EmployeeId = rs.Value;
    //    });
    //};
    //$scope.reloadnumberauto();
    $scope.jmodel = {};
    $scope.Language1Config = {
        placeholder: 'Chọn ngoại ngữ',
        search: true,
        searchText: 'Tìm kiếm...',
        showTitle: 'true',
        csvSepChar: ',',
        captionFormat: '{0} lựa chọn',
        captionFormatAllSelected: '{0} lựa chọn',
        noMatch: 'Không có dữ liệu "{0}"'
    }
    $scope.BankCodeDataConfig = {
        placeholder: 'Chọn tài khoản ngân hàng',
        search: true,
        searchText: 'Tìm kiếm...',
        showTitle: 'true',
        csvSepChar: ',',
        captionFormat: '{0} lựa chọn',
        captionFormatAllSelected: '{0} lựa chọn',
        noMatch: 'Không có dữ liệu "{0}"'
    }
    $scope.EducationIDConfig = {
        placeholder: 'Chọn trình độ học vấn',
        search: true,
        searchText: 'Tìm kiếm...',
        showTitle: 'true',
        csvSepChar: ',',
        captionFormat: '{0} lựa chọn',
        captionFormatAllSelected: '{0} lựa chọn',
        noMatch: 'Không có dữ liệu "{0}"'
    }
    $scope.ProfestionalDConfig = {
        placeholder: 'Chọn trình độ chuyên môn',
        search: true,
        searchText: 'Tìm kiếm...',
        showTitle: 'true',
        csvSepChar: ',',
        captionFormat: '{0} lựa chọn',
        captionFormatAllSelected: '{0} lựa chọn',
        noMatch: 'Không có dữ liệu "{0}"'
    }
    $scope.ListPBConfig = {
        placeholder: 'Chọn phòng ban, bộ phận chức danh',
        search: true,
        searchText: 'Tìm kiếm...',
        showTitle: 'true',
        csvSepChar: ',',
        captionFormat: '{0} lựa chọn',
        captionFormatAllSelected: '{0} lựa chọn',
        noMatch: 'Không có dữ liệu "{0}"'
    }
    $scope.SalaryMethodConfig = {
        placeholder: 'Chọn hình thức trả lương',
        search: true,
        searchText: 'Tìm kiếm...',
        showTitle: 'true',
        csvSepChar: ',',
        captionFormat: '{0} lựa chọn',
        captionFormatAllSelected: '{0} lựa chọn',
        noMatch: 'Không có dữ liệu "{0}"'
    }
    $scope.JobTitleIDConfig = {
        placeholder: 'Chọn chức danh',
        search: true,
        searchText: 'Tìm kiếm...',
        showTitle: 'true',
        csvSepChar: ',',
        captionFormat: '{0} lựa chọn',
        captionFormatAllSelected: '{0} lựa chọn',
        noMatch: 'Không có dữ liệu "{0}"'
    }
    $scope.SalaryMethodDataConfig = {
        placeholder: 'Chọn chức danh',
        search: true,
        searchText: 'Tìm kiếm...',
        showTitle: 'true',
        csvSepChar: ',',
        captionFormat: '{0} lựa chọn',
        captionFormatAllSelected: '{0} lựa chọn',
        noMatch: 'Không có dữ liệu "{0}"'
    }

    $scope.ShiftId1Config = {
        placeholder: 'Chọn ca làm việc',
        selectAll: true,
        search: true,
        searchText: 'Tìm kiếm...',
        showTitle: 'true',
        csvSepChar: ',',
        captionFormat: '{0} lựa chọn',
        captionFormatAllSelected: '{0} lựa chọn',
        noMatch: 'Không có dữ liệu "{0}"',
        okCancelInMulti: true,
        locale: ['Đồng ý', 'Hủy', 'Chọn tất cả']
    };

    $scope.RankDataConfig = {
        search: true
    }
    $scope.ManagerIdConfig = {
        search: true
    }
    $scope.listSpecializesConfig = {
        search: true
    }
    $scope.GenitiveIdConfig = {
        search: true
    }
    $scope.SalaryTierIdConfig = {
        search: true
    }
    $scope.ReasonLeaveIDConfig = {
        search: true
    }
    $rootScope.ListAddPositionsConcurrently = [];
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    }
    $scope.loadDataCombobox = function () {
        dataservice.getnationality(function (rs5) {
            $rootScope.li5 = rs5;
            $rootScope.lstCountry = rs5;
        });
        dataservice.getprovince(function (rs6) {
            $rootScope.li6 = rs6;
            $rootScope.lstProvince = rs6;
        });
        dataservice.getdistrict(function (rs7) {
            $rootScope.li7 = rs7;
            $rootScope.lstDistrict = rs7;
        });
        dataservice.gettreedataadd(null, function (rs) {
            $rootScope.ListPB = [];
            angular.forEach(rs, function (value, key) {
                $rootScope.ListPB.push({ DepartmentId: value.DepartmentId, value: value.DepartmentGuid, text: value.Title, DepartmentName: value.DepartmentName });
            })
            $scope.ListPBChange($scope.model.DepartmentGuid);
        });

    }
    $scope.loadDataCombobox();

    dataservice.getEmployeesLogin({ LoginName: $rootScope.loginName }, function (response) {
        $rootScope.jemployees = response;
        if (response.length > 0) {
            $scope.model.LoginID = response[0].EmployeeID;

            $scope.FullName = response[0].FullName;
            //$scope.model.DepartmentName = response[0].DepartmentName;
        } else {
            $scope.FullName = null;
        }
    });
    $rootScope.HomeNumber = "";
    $rootScope.CommuneWard = "";
    $rootScope.DistrictID = "";
    $rootScope.ProvinceID = "";
    $rootScope.CountryID = "";
    $scope.dongAddress = function () {
        $rootScope.hidenAdress = "add";
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/address.html',
            controller: 'address',
            backdrop: 'static',
            windowClass: 'app-modal-window',
            resolve: {
                para1: function () {
                    return $rootScope.HomeNumber;
                },
                para2: function () {
                    return $rootScope.CommuneWard;
                },
                para3: function () {
                    return $rootScope.DistrictID;
                },
                para4: function () {
                    return $rootScope.ProvinceID;
                },
                para5: function () {
                    return $rootScope.CountryID;
                }
            }
        });
        modalInstance.result.then(function (d) {
            $rootScope.reloadEmployee();
        }, function () {
        });
    }

    $scope.Kiemnhiem = function (item) {
        if (item === false) {
            $scope.model.Kiemnhiem = true;


        } else {
            $scope.model.Kiemnhiem = false;
        }
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'addKiemnghiem.html',
            controller: 'addKiemnghiem',
            backdrop: 'static',
            size: '90',
            resolve: {
                para: function () {
                    return $scope.model.DepartmentGuid;
                }

            }
        });
        modalInstance.result.then(function (d) {
            $rootScope.reloadEmployee();
        }, function () {
        });

    }
    $scope.hoten = function (st) {
        if (st === true) {
            $scope.stahoten = true;
        } else {
            $scope.stahoten = false;
        }
        $scope.model.FullName = "";
        $scope.model.FullName = $rootScope.fHo($scope.model.FirstName, $scope.model.MiddleName, $scope.model.LastName);

    }

    $scope.detailName = function () {

        if ($scope.model.FullName === null && $scope.model.FullName === "") {
            $scope.model.FirstName = "";
            $scope.model.MiddleName = "";
            $scope.model.LastName = "";
        } else {
            $scope.arrName = $rootScope.fName($scope.model.FullName);
            $scope.model.FirstName = $scope.arrName.first;
            $scope.model.MiddleName = $scope.arrName.mid;
            $scope.model.LastName = $scope.arrName.last;
        }
    };
    $scope.StatusOfWorkDataChange = function (data) {
        $scope.model.StatusOfWork = data !== "null" ? data : "";
    }
    $scope.li4Change = function (data) {
        $scope.model.JobTitleId = data !== "null" ? parseInt(data) : "";
    }
    $scope.li2Change = function (data) {
        $scope.model.EducationId = data !== "null" ? parseInt(data) : "";
    }
    $scope.listReasonLeavesChange = function (data) {
        $scope.model.ReasonLeaveId = data !== "null" ? parseInt(data) : "";
    }
    $scope.EthnicDataChange = function (data) {
        $scope.model.EthnicId = data !== "null" ? parseInt(data) : "";
    }
    $scope.ProfestionalDataChange = function (data) {
        $scope.model.ProfestionalD = data !== "null" ? parseInt(data) : "";
    }
    $scope.SalariedFlagDataChange = function (data) {
        $scope.model.SalaryPayBy = data !== "null" ? data : "";
    }
    $scope.RankDataChange = function (data) {
        $scope.model.RankId = data !== "null" ? data : "";
    }
    $scope.ManagerDataChange = function (data) {
        $scope.model.ManagerId = data !== "null" ? data.toUpperCase() : "";
    }

    $scope.GenderDataChange = function (data) {
        $scope.model.Gender = data;
        if ($scope.model.Gender === "M") {
            $scope.model.imageLink = "/images/male.jpeg";
        } else {
            $scope.model.imageLink = "/images/female.jpeg";
        }
        if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
            $scope.$apply();
        }
    }
    $scope.MaritalStatusDataChange = function (data) {
        $scope.model.MaritalStatus = data !== "null" ? data : "";
    }
    $scope.CandidateChange = function (data) {
        $scope.model.CandidateId = data !== "null" ? data : "";
    }
    $scope.BankCodeDataChange = function (data) {
        $scope.model.BankCode = data !== "null" ? data : "";
    }
    $scope.ProfestionalDataChange = function (data) {
        $scope.model.ProfestionalD = data !== "null" ? data : "";
    }
    $scope.listSpecializesDataChange = function (data) {
        $scope.model.SpecializesId = data !== "null" ? data : "";
    }
    $scope.GenitiveIdDataChange = function (data) {
        $scope.model.OrganizationId = data !== "null" ? data : "";
    }
    $scope.SalaryTierIdDataChange = function (data) {
        $scope.model.SalaryTierId = data !== "null" ? data : "";
    }
    $scope.UniformDataChange = function (data) {
        $scope.model.Uniform = data !== "null" ? data : "";
    }
    $scope.SalaryMethodDataChange = function (data) {
        $scope.model.SalaryMethod = data !== "null" ? data : "";
    }
    $scope.ListPBChange = function (data) {
        $scope.model.DepartmentGuid = data;
        angular.forEach($rootScope.ListPB, function (value, key) {
            if (value.value === $scope.model.DepartmentGuid) {
                $scope.model.DepartmentID = value.DepartmentGuid;
            }
        })
    }

    $scope.newImage = function () {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#image').attr('src', e.target.result);
        }
        reader.readAsDataURL($("#loadImage").prop('files')[0]);
        $scope.changeImage = true;
    }
    var reg1 = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;

    $scope.submit = function () {
        $rootScope.listDetail = [];
        var testmail = true;
        var testmail2 = true;
        var sta = false;

        if ($scope.model.Identification !== undefined && $scope.model.Identification !== "" && isNaN($scope.model.Identification)) {
            App.notifyDanger("Số chứng minh thư không đúng");
            return;
        }
        if ($scope.model.HomePhone !== undefined && $scope.model.HomePhone !== "" && isNaN($scope.model.HomePhone)) {
            App.notifyDanger("Số điện thoại cố định cá nhân, nhà riêng không đúng");
            return;
        }
        if ($scope.model.HomeMobile !== undefined && $scope.model.HomeMobile !== "" && isNaN($scope.model.HomeMobile)) {
            App.notifyDanger("Số điện thoại di động cá nhân, nhà riêng không đúng");
            return;
        }

        if ($scope.model.WorkPhone !== undefined && $scope.model.WorkPhone !== "" && isNaN($scope.model.WorkPhone)) {
            App.notifyDanger("Số điện thoại cố định cơ quan không đúng");
            return;
        }
        if ($scope.model.WorkMobile !== undefined && $scope.model.WorkMobile !== "" && isNaN($scope.model.WorkMobile)) {
            App.notifyDanger("Số điện thoại di động cơ quan không đúng");
            return;
        }
        if ($scope.model.EndDateWork < $scope.model.StartDate) {
            App.notifyDanger("Ngày nghỉ việc không được nhỏ hơn ngày bắt đầu làm việc");
            return;
        }
        if ($scope.model.LoginName !== "" && $scope.model.LoginName !== null && $scope.model.LoginName !== undefined) {
            angular.forEach($rootScope.EmployeeData, function (value, key) {
                if (value.LoginName === $scope.model.LoginName) {
                    sta = true;
                    App.notifyDanger("Tài khoản đăng nhập đã tồn tại, xin kiểm tra lại !!");
                    return;
                }
            });
        }
        if (!sta) {

            if ($scope.model.WorkEmail !== "" && $scope.model.WorkEmail !== null && $scope.model.WorkEmail !== undefined) {
                testmail = reg1.test($scope.model.WorkEmail);
            }
            if ($scope.model.HomeEmail !== "" && $scope.model.HomeEmail !== null && $scope.model.HomeEmail !== undefined) {
                testmail2 = reg1.test($scope.model.HomeEmail);
            }
            if (testmail === true && testmail2 === true) {
                if ($scope.addform.validate()) {

                    $scope.model.HomeNumber = $rootScope.HomeNumber;
                    $scope.model.WardName = $rootScope.CommuneWard;
                    $scope.model.DistrictID = $rootScope.DistrictID;
                    $scope.model.ProvinceID = $rootScope.ProvinceID;
                    $scope.model.CountryID = $rootScope.CountryID;
                    $scope.model.PermanentAddress = $rootScope.Address1;
                    angular.forEach($rootScope.listEthnic, function (value, key) {
                        if (value.value === $scope.model.EthnicID) {
                            $scope.model.EthnicName = value.text;
                        }
                    });
                    angular.forEach($rootScope.lstDistrict, function (value, key) {
                        if (value.value === $scope.model.DistrictID) {
                            $scope.model.DistrictName = value.text;
                        }
                    });
                    angular.forEach($rootScope.li6, function (value, key) {
                        if (value.value === $scope.model.ProvinceID) {
                            $scope.model.ProvinceName = value.text;
                        }
                    });
                    angular.forEach($rootScope.li5, function (value, key) {
                        if (value.value === $scope.model.CountryID) {
                            $scope.model.CountryName = value.text;
                        }
                    });

                    if ($rootScope.ListAddPositionsConcurrently.length > 0) {
                        for (var j = 0; j < $rootScope.ListAddPositionsConcurrently.length; j++) {
                            var obj = {
                                DepartmentGuid: $rootScope.ListAddPositionsConcurrently[j].DepartmentGuid,
                                DepartmentName: $rootScope.ListAddPositionsConcurrently[j].DepartmentName,
                                JobTitleID: $rootScope.ListAddPositionsConcurrently[j].JobTitleID,
                                StartDate: $rootScope.ListAddPositionsConcurrently[j].StartDate,
                                EndDate: $rootScope.ListAddPositionsConcurrently[j].EndDate,
                                IsActive: $rootScope.ListAddPositionsConcurrently[j].IsActive
                            }
                            $rootScope.listDetail.push(obj);
                        }
                    }
                    $scope.model.JobTitleId = $scope.model.JobTitleId !== "" ? parseInt($scope.model.JobTitleId) : null;
                    $scope.model.EducationId = $scope.model.EducationId !== "" ? parseInt($scope.model.EducationId) : null;
                    $scope.model.EthnicId = $scope.model.EthnicId !== "" ? parseInt($scope.model.EthnicId) : null;
                    $scope.model.ProfestionalD = $scope.model.ProfestionalD !== "" ? parseInt($scope.model.ProfestionalD) : null;
                    $scope.model.ReasonLeaveId = $scope.model.ReasonLeaveId !== "" ? parseInt($scope.model.ReasonLeaveId) : null;
                    $scope.model.IsActive = true;
                    var ShiftId1 = [];
                    if ($scope.model.ShiftId1 !== null && $scope.model.ShiftId1 !== undefined) {
                        for (var i = 0; i < $scope.model.ShiftId1.length; i++) {
                            ShiftId1.push({ ShiftID: parseInt($scope.model.ShiftId1[i]) });
                        }
                    }


                    var imageTemp = $("#loadImage").prop('files')[0];
                    var fd = new FormData();
                    fd.append('file', imageTemp);
                    fd.append('Insert', JSON.stringify($scope.model));
                    fd.append('PositionsConcurrently', JSON.stringify($rootScope.listDetail));
                    fd.append('ShiftOfEmployees', JSON.stringify(ShiftId1));
                    fd.append('auto', JSON.stringify($scope.modelauto));
                    $.ajax({
                        url: '/Employees/Insert_Employees',
                        data: fd,
                        processData: false,
                        contentType: false,
                        type: 'POST',
                        success: function (rs) {
                            if (rs.Title === "TRUNGMA") {
                                ESConfirm([{ 'value': 1, 'title': 'Xác nhận', 'type': 'info' }, { 'value': 2, 'title': 'Đóng', 'type': 'danger' }], { 'description': 'Mã nhân viên <' + $scope.model.EmployeeId + ' > đã tồn tại bạn có muốn chương <br> trình tự động tăng mã nhân viên không?', 'class': 'eswarning_v2' }, function (rs) {
                                    if (rs === '1') {
                                        $scope.modelauto.IsNumberAuto = true;
                                        var fdr = new FormData();
                                        fdr.append('file', imageTemp);
                                        fdr.append('Insert', JSON.stringify($scope.model));
                                        fdr.append('PositionsConcurrently', JSON.stringify($rootScope.listDetail));
                                        fdr.append('ShiftOfEmployees', JSON.stringify(ShiftId1));
                                        fdr.append('auto', JSON.stringify($scope.modelauto));
                                        $.ajax({
                                            type: "POST",
                                            url: '/Employees/Insert_Employees',
                                            contentType: false,
                                            processData: false,
                                            data: fdr,
                                            success: function (rs) {
                                                if (rs.Error === true) {
                                                    App.notifyDanger(rs.Title);
                                                } else {
                                                    App.notifyInfo(rs.Title);
                                                    $rootScope.reloadEmployee();
                                                    $uibModalInstance.close();
                                                }
                                                App.unblockUI(".modal-content");
                                            },
                                            error: function (rs) {
                                                App.notifyDanger(rs.Title);
                                                App.unblockUI(".modal-content");
                                            }
                                        });
                                    }
                                    else {
                                        $scope.modelauto.IsNumberAuto = false;
                                    }
                                }, function () {
                                });
                            }
                            else {
                                App.notifyInfo(rs.Title);
                                $rootScope.reloadEmployee();
                                $uibModalInstance.close();
                            }
                        },
                        error: function (rs) {
                            App.notifyInfo(rs.Title);
                        }
                    });
                }
            }
        }
    }
});
//Sửa nhân viên
app.controller('edit', function ($scope, $rootScope, $compile, $uibModal, $confirm, $uibModalInstance, FileUploader, dataservice, para) {
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    }
    $scope.dlgTitle = "Sửa thông tin nhân viên";
    $scope.model = { ManagerId: null };
    $scope.Language1Config = {
        placeholder: 'Chọn ngoại ngữ',
        search: true,
        searchText: 'Tìm kiếm...',
        showTitle: 'true',
        csvSepChar: ',',
        captionFormat: '{0} lựa chọn',
        captionFormatAllSelected: '{0} lựa chọn',
        noMatch: 'Không có dữ liệu "{0}"'
    }
    $scope.BankCodeDataConfig = {
        placeholder: 'Chọn tài khoản ngân hàng',
        search: true,
        searchText: 'Tìm kiếm...',
        showTitle: 'true',
        csvSepChar: ',',
        captionFormat: '{0} lựa chọn',
        captionFormatAllSelected: '{0} lựa chọn',
        noMatch: 'Không có dữ liệu "{0}"'
    }
    $scope.EducationIDConfig = {
        placeholder: 'Chọn trình độ học vấn',
        search: true,
        searchText: 'Tìm kiếm...',
        showTitle: 'true',
        csvSepChar: ',',
        captionFormat: '{0} lựa chọn',
        captionFormatAllSelected: '{0} lựa chọn',
        noMatch: 'Không có dữ liệu "{0}"'
    }
    $scope.ProfestionalDConfig = {
        placeholder: 'Chọn trình độ chuyên môn',
        search: true,
        searchText: 'Tìm kiếm...',
        showTitle: 'true',
        csvSepChar: ',',
        captionFormat: '{0} lựa chọn',
        captionFormatAllSelected: '{0} lựa chọn',
        noMatch: 'Không có dữ liệu "{0}"'
    }
    $scope.ListPBConfig = {
        placeholder: 'Chọn phòng ban, bộ phận chức danh',
        search: true,
        searchText: 'Tìm kiếm...',
        showTitle: 'true',
        csvSepChar: ',',
        captionFormat: '{0} lựa chọn',
        captionFormatAllSelected: '{0} lựa chọn',
        noMatch: 'Không có dữ liệu "{0}"'
    }
    $scope.SalaryMethodConfig = {
        placeholder: 'Chọn hình thức trả lương',
        search: true,
        searchText: 'Tìm kiếm...',
        showTitle: 'true',
        csvSepChar: ',',
        captionFormat: '{0} lựa chọn',
        captionFormatAllSelected: '{0} lựa chọn',
        noMatch: 'Không có dữ liệu "{0}"'
    }
    $scope.JobTitleIDConfig = {
        placeholder: 'Chọn chức danh',
        search: true,
        searchText: 'Tìm kiếm...',
        showTitle: 'true',
        csvSepChar: ',',
        captionFormat: '{0} lựa chọn',
        captionFormatAllSelected: '{0} lựa chọn',
        noMatch: 'Không có dữ liệu "{0}"'
    }
    $scope.SalaryMethodDataConfig = {
        placeholder: 'Chọn chức danh',
        search: true,
        searchText: 'Tìm kiếm...',
        showTitle: 'true',
        csvSepChar: ',',
        captionFormat: '{0} lựa chọn',
        captionFormatAllSelected: '{0} lựa chọn',
        noMatch: 'Không có dữ liệu "{0}"'
    }
    $scope.ShiftId1Config = {
        placeholder: 'Chọn ca làm việc',
        selectAll: true,
        search: true,
        searchText: 'Tìm kiếm...',
        showTitle: 'true',
        csvSepChar: ',',
        captionFormat: '{0} lựa chọn',
        captionFormatAllSelected: '{0} lựa chọn',
        noMatch: 'Không có dữ liệu "{0}"',
        okCancelInMulti: true,
        locale: ['Đồng ý', 'Hủy', 'Chọn tất cả']
    };
    $scope.ManagerIdConfig = {
        search: true
    }
    $scope.ReasonLeaveIDConfig = {
        search: true
    }
    $scope.RankDataConfig = {
        search: true
    }
    $scope.listSpecializesConfig = {
        search: true
    }
    $scope.GenitiveIdConfig = {
        search: true
    }
    $scope.SalaryTierIdConfig = {
        search: true
    }
    $scope.listReasonLeavesChange = function (data) {
        $scope.model.ReasonLeaveId = data !== "null" ? parseInt(data) : "";
    }
    $rootScope.listDetail = [];
    $rootScope.ListDetailDelete = [];
    $scope.loadDataCombobox = function () {
        dataservice.getnationality(function (rs5) {
            $rootScope.li5 = rs5;
            $rootScope.lstCountry = rs5;
        });
        dataservice.getprovince(function (rs6) {
            $rootScope.li6 = rs6;
            $rootScope.lstProvince = rs6;
        });
        dataservice.getdistrict(function (rs7) {
            $rootScope.li7 = rs7;
            $rootScope.lstDistrict = rs7;
        });
        dataservice.gettreedataadd(null, function (rs) {
            $rootScope.ListPB = [];
            angular.forEach(rs, function (value, key) {
                $rootScope.ListPB.push({ DepartmentId: value.DepartmentId, value: value.DepartmentGuid, text: value.Title, DepartmentName: value.DepartmentName });
            })

        });
    }
    $scope.loadDataCombobox();
    $scope.dongAddress = function () {
        $rootScope.hidenAdress = "edit";
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/address.html',
            controller: 'address',
            backdrop: 'static',
            windowClass: 'app-modal-window',
            resolve: {
                para1: function () {
                    return $rootScope.HomeNumber;
                },
                para2: function () {
                    return $rootScope.CommuneWard;
                },
                para3: function () {
                    return $rootScope.DistrictID;
                },
                para4: function () {
                    return $rootScope.ProvinceID;
                },
                para5: function () {
                    return $rootScope.CountryID;
                }

            }
        });
        modalInstance.result.then(function (d) {
            $rootScope.reloadEmployee();
        }, function () {
        });

    }
    $scope.detailName = function () {

        if ($scope.model.FullName == null && $scope.model.FullName == "") {
            $scope.model.FirstName = "";
            $scope.model.MiddleName = "";
            $scope.model.LastName = "";
        } else {
            $scope.arrName = $rootScope.fName($scope.model.FullName);
            $scope.model.FirstName = $scope.arrName.first;
            $scope.model.MiddleName = $scope.arrName.mid;
            $scope.model.LastName = $scope.arrName.last;
        }
    };
    $rootScope.ListPositionsConcurrently = [];
    $scope.initData = function () {
        dataservice.getitem(para, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {

                $scope.model = rs;
                if ($scope.model.ImagePath !== null && $scope.model.ImagePath !== "") {
                    $scope.model.imageLink = "/employees/GetPic/" + rs.EmployeeGuid;
                } else {
                    if ($scope.model.Gender === "M") {
                        $scope.model.imageLink = "/images/male.jpeg";
                    } else {
                        $scope.model.imageLink = "/images/female.jpeg";
                    }
                }

                $rootScope.HomeNumber = $scope.model.HomeNumber;
                $rootScope.CommuneWard = $scope.model.WardName;
                $rootScope.DistrictID = $scope.model.DistrictId;
                $rootScope.ProvinceID = $scope.model.ProvinceId;
                $rootScope.CountryID = $scope.model.CountryId;
                $rootScope.Address1 = $scope.model.PermanentAddress;
                //$scope.model.Language1 = JSON.parse(rs.Language);
                $scope.model.IssueIddate !== null ? $scope.model.IssueIddate = "/Date(" + new Date($scope.model.IssueIddate).getTime() + ")/" : undefined;
                $scope.model.BirthDate !== null ? $scope.model.BirthDate = "/Date(" + new Date($scope.model.BirthDate).getTime() + ")/" : undefined;
                $scope.model.HireDate !== null ? $scope.model.HireDate = "/Date(" + new Date($scope.model.HireDate).getTime() + ")/" : undefined;
                $scope.model.EndDateWork !== null ? $scope.model.EndDateWork = "/Date(" + new Date($scope.model.EndDateWork).getTime() + ")/" : undefined;
                $scope.model.StartDate !== null ? $scope.model.StartDate = "/Date(" + new Date($scope.model.StartDate).getTime() + ")/" : undefined;
                $scope.model.EndDate !== null ? $scope.model.EndDate = "/Date(" + new Date($scope.model.EndDate).getTime() + ")/" : undefined;
                if ($scope.model.SalariedFlag === true) {
                    $scope.model.SalariedFlag = 1;
                } else { $scope.model.SalariedFlag = 0; }
                if ($scope.model.StatusOfWork.indexOf("OM") >= 0) {
                    $scope.model.StatusOfWork = "OM";
                } else if ($scope.model.StatusOfWork.indexOf("PE") >= 0) {
                    $scope.model.StatusOfWork = "PE";
                }
                else if ($scope.model.StatusOfWork.indexOf("PT") >= 0) {
                    $scope.model.StatusOfWork = "PT";
                }
                else if ($scope.model.StatusOfWork.indexOf("ML") >= 0) {
                    $scope.model.StatusOfWork = "ML";
                }
                else if ($scope.model.StatusOfWork.indexOf("OL") >= 0) {
                    $scope.model.StatusOfWork = "OL";
                }
                else if ($scope.model.StatusOfWork.indexOf("LJ") >= 0) {
                    $scope.model.StatusOfWork = "LJ";
                }
                if ($scope.model.ManagerId !== "" && $scope.model.ManagerId !== null) {
                    $scope.model.ManagerId = $scope.model.ManagerId.toUpperCase();
                }

                if ($scope.model.IsActive === true) {
                    $scope.model.IsActive = "true";
                } else { $scope.model.IsActive = "false"; }



            }
        });
    }
    $scope.initData();
    $scope.Kiemnhiem = function (item) {
        if (item === false) {
            $scope.model.Kiemnhiem = true;
        }
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'editKiemnghiem.html',
            controller: 'editKiemnghiem',
            backdrop: true,
            size: '80',
            resolve: {
                para: function () {
                    return $scope.model.DepartmentGuid;
                }

            }
        });
        modalInstance.result.then(function (d) {

        }, function () {
        });

    }
    $scope.StatusOfWorkDataChange = function (data) {
        $scope.model.StatusOfWork = data !== "null" ? data : "";
        if ($scope.model.StatusOfWork === "ML" || $scope.model.StatusOfWork === "OL" || $scope.model.StatusOfWork === "LJ") {
            $scope.Click_Histories(1);
        }

    }
    $scope.li4Change = function (data) {
        $scope.model.JobTitleId = data !== "null" ? parseInt(data) : "";
    }
    $scope.li2Change = function (data) {
        $scope.model.EducationId = data !== "null" ? parseInt(data) : "";
    }

    $scope.EthnicDataChange = function (data) {
        $scope.model.EthnicId = data !== "null" ? parseInt(data) : "";
    }
    $scope.ProfestionalDataChange = function (data) {
        $scope.model.ProfestionalD = data !== "null" ? parseInt(data) : "";
    }
    $scope.listSpecializesDataChange = function (data) {
        $scope.model.SpecializesId = data !== "null" ? data : "";
    }
    $scope.GenitiveIdDataChange = function (data) {
        $scope.model.GenitiveId = data !== "null" ? data : "";
    }
    $scope.SalaryTierIdDataChange = function (data) {
        $scope.model.SalaryTierId = data !== "null" ? data : "";
    }

    $scope.SalariedFlagDataChange = function (data) {
        $scope.model.SalaryPayBy = data !== "null" ? data : null;
    }
    $scope.RankDataChange = function (data) {
        $scope.model.RankId = data !== "null" ? data : "";
    }
    $scope.ManagerDataChange = function (data) {
        $scope.model.ManagerId = data !== "null" ? data.toUpperCase() : "";
    }
    $scope.GenderDataChange = function (data) {
        $scope.model.Gender = data;
        if ($scope.model.Gender === "M") {
            $scope.model.imageLink = "/images/male.jpeg";
        } else {
            $scope.model.imageLink = "/images/female.jpeg";
        }
        if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
            $scope.$apply();
        }
    }
    $scope.MaritalStatusDataChange = function (data) {
        $scope.model.MaritalStatus = data !== "null" ? data : "";
    }
    $scope.CandidateChange = function (data) {
        $scope.model.CandidateId = data !== "null" ? data : "";
    }
    $scope.BankCodeDataChange = function (data) {
        $scope.model.BankCode = data !== "null" ? data : "";
    }
    $scope.ProfestionalDataChange = function (data) {
        $scope.model.ProfestionalD = data !== "null" ? data : "";
    }
    $scope.UniformDataChange = function (data) {
        $scope.model.Uniform = data !== "null" ? data : "";
    }
    $scope.SalaryMethodDataChange = function (data) {
        $scope.model.SalaryMethod = data !== "null" ? data : "";
    }
    $scope.ListPBChange = function (data) {
        $scope.model.DepartmentGuid = data;
        angular.forEach($rootScope.ListPB, function (value, key) {
            if (value.value === $scope.model.DepartmentGuid) {
                $scope.model.DepartmentId = value.DepartmentId;
            }
        })
    }
    $scope.GenderDataChange = function (data) {
        $scope.model.Gender = data;
        if ($scope.model.ImagePath === null || $scope.model.ImagePath === "" || $scope.model.ImagePath === undefined) {
            if ($scope.model.Gender === "M") {
                $scope.model.imageLink = "/images/male.jpeg";
            } else {
                $scope.model.imageLink = "/images/female.jpeg";
            }
        }
        if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
            $scope.$apply();
        }
    }
    $scope.Click_Histories = function (type) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/Histories_add.html',
            controller: 'Histories_add',
            backdrop: 'static',
            keyboard: false,
            size: '100',
            resolve: {
                para: function () {
                    return $scope.model.EmployeeGuid;
                },
                para1: function () {
                    return $scope.model.EmployeeId;
                },
                para2: function () {
                    return $scope.model;
                },
                para3: function () {
                    return type;
                },
                TitleIndex: function () {
                    return "Thông tin quá trình công tác : " + $scope.model.FullName;
                }
            }
        });
    }
    $scope.newImage = function () {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#image').attr('src', e.target.result);
        }
        reader.readAsDataURL($("#loadImage").prop('files')[0]);
        $scope.changeImage = true;
    }
    var reg1 = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;

    $scope.submit = function () {
        var testmail = true;
        var testmail2 = true;
        var sta = false;
        if ($scope.model.Identification !== undefined && $scope.model.Identification !== "" && isNaN($scope.model.Identification)) {
            App.notifyDanger("Số chứng minh thư không đúng");
            return;
        }
        if ($scope.model.HomePhone !== undefined && $scope.model.HomePhone !== "" && isNaN($scope.model.HomePhone)) {
            App.notifyDanger("Số điện thoại cố định cá nhân, nhà riêng không đúng");
            return;
        }
        if ($scope.model.HomeMobile !== undefined && $scope.model.HomeMobile !== "" && isNaN($scope.model.HomeMobile)) {
            App.notifyDanger("Số điện thoại di động cá nhân, nhà riêng không đúng");
            return;
        }

        if ($scope.model.WorkPhone !== undefined && $scope.model.WorkPhone !== "" && isNaN($scope.model.WorkPhone)) {
            App.notifyDanger("Số điện thoại cố định cơ quan không đúng");
            return;
        }
        if ($scope.model.WorkMobile !== undefined && $scope.model.WorkMobile !== "" && isNaN($scope.model.WorkMobile)) {
            App.notifyDanger("Số điện thoại di động cơ quan không đúng");
            return;
        }
        if ($scope.model.EndDateWork < $scope.model.StartDate) {
            App.notifyDanger("Ngày nghỉ việc không được nhỏ hơn ngày bắt đầu làm việc");
            return;
        }
        $scope.model.ReasonLeaveId = $scope.model.ReasonLeaveId !== "" ? parseInt($scope.model.ReasonLeaveId) : null;
        if ($scope.model.LoginName !== "" && $scope.model.LoginName !== null && $scope.model.LoginName !== undefined) {
            dataservice.getall(function (rs) {
                $scope.EmployeeDataUpdate = rs;
                for (var i = 0; i < $scope.EmployeeDataUpdate.length; i++) {
                    if ($scope.EmployeeDataUpdate[i].LoginName === $scope.model.LoginName && $scope.EmployeeDataUpdate[i].EmployeeGuid !== $scope.model.EmployeeGuid) {
                        sta = true;
                        App.notifyDanger("TK đăng nhập đã tồn tại, xin kiểm tra lại");
                        return;
                    }
                } if (!sta) {
                    if ($scope.editform.validate()) {

                        if ($scope.model.WorkEmail !== null && $scope.model.WorkEmail !== "" && $scope.model.WorkEmail !== undefined) {
                            testmail = reg1.test($scope.model.WorkEmail);
                        }
                        if ($scope.model.HomeEmail !== null && $scope.model.HomeEmail !== "" && $scope.model.HomeEmail !== undefined) {
                            testmail2 = reg1.test($scope.model.HomeEmail);
                        }
                        if (testmail === true && testmail2 === true) {
                            $scope.model.PermanentAddress = $rootScope.Address1;
                            $scope.model.HomeNumber = $rootScope.HomeNumber;
                            $scope.model.WardName = $rootScope.CommuneWard;
                            $scope.model.DistrictID = $rootScope.DistrictID;
                            $scope.model.ProvinceID = $rootScope.ProvinceID;
                            $scope.model.CountryID = $rootScope.CountryID;
                            angular.forEach($rootScope.lstDistrict, function (value, key) {
                                if (value.value === $scope.model.DistrictID) {
                                    $scope.model.DistrictName = value.text;
                                }
                            });
                            angular.forEach($rootScope.li6, function (value, key) {
                                if (value.value === $scope.model.ProvinceID) {
                                    $scope.model.ProvinceName = value.text;
                                }
                            });
                            angular.forEach($rootScope.li5, function (value, key) {
                                if (value.value === $scope.model.CountryID) {
                                    $scope.model.CountryName = value.text;
                                }
                            });
                            var imageTemp = $("#loadImage").prop('files')[0];

                            $scope.model.JobTitleId = $scope.model.JobTitleId !== "" ? parseInt($scope.model.JobTitleId) : null;
                            $scope.model.EducationId = $scope.model.EducationId !== "" ? parseInt($scope.model.EducationId) : null;

                            $scope.model.EthnicId = $scope.model.EthnicId !== "" ? parseInt($scope.model.EthnicId) : null;
                            $scope.model.ProfestionalD = $scope.model.ProfestionalD !== "" ? parseInt($scope.model.ProfestionalD) : null;
                            var ShiftId1 = [];
                            if ($scope.model.ShiftId1 !== null && $scope.model.ShiftId1 !== undefined) {
                                for (var i = 0; i < $scope.model.ShiftId1.length; i++) {
                                    ShiftId1.push({ ShiftID: parseInt($scope.model.ShiftId1[i]) });
                                }
                            }
                            var fd = new FormData();
                            fd.append('file', imageTemp);
                            fd.append('Insert', JSON.stringify($scope.model));
                            fd.append('ListDetailDelete', JSON.stringify($rootScope.ListDetailDelete));
                            fd.append('PositionsConcurrently', JSON.stringify($rootScope.ListPositionsConcurrently));


                            fd.append('ShiftOfEmployees', JSON.stringify(ShiftId1));
                            $.ajax({
                                url: '/employees/Update_Employees',
                                data: fd,
                                processData: false,
                                contentType: false,
                                type: 'POST',
                                success: function (rs) {
                                    if (rs.Error) {
                                        App.notifyDanger(rs.Title);
                                        return;
                                    }
                                    App.notifyInfo(rs.Title);
                                    $rootScope.reloadEmployee();
                                    $uibModalInstance.close();
                                },
                                error: function (rs) {

                                    App.notifyInfo(rs.Title);
                                }
                            });
                        } else {
                            App.notifyDanger("Địa chỉ email không hợp lệ");
                            return;
                        }
                    }
                }

            });
        } else {
            if ($scope.editform.validate()) {

                if ($scope.model.WorkEmail !== null && $scope.model.WorkEmail !== "" && $scope.model.WorkEmail !== undefined) {
                    testmail = reg1.test($scope.model.WorkEmail);
                }
                if ($scope.model.HomeEmail !== null && $scope.model.HomeEmail !== "" && $scope.model.HomeEmail !== undefined) {
                    testmail2 = reg1.test($scope.model.HomeEmail);
                }
                if (testmail === true && testmail2 === true) {
                    $scope.model.PermanentAddress = $rootScope.Address1;
                    $scope.model.HomeNumber = $rootScope.HomeNumber;
                    $scope.model.WardName = $rootScope.CommuneWard;
                    $scope.model.DistrictID = $rootScope.DistrictID;
                    $scope.model.ProvinceID = $rootScope.ProvinceID;
                    $scope.model.CountryID = $rootScope.CountryID;
                    angular.forEach($rootScope.lstDistrict, function (value, key) {
                        if (value.value === $scope.model.DistrictID) {
                            $scope.model.DistrictName = value.text;
                        }
                    });
                    angular.forEach($rootScope.li6, function (value, key) {
                        if (value.value === $scope.model.ProvinceID) {
                            $scope.model.ProvinceName = value.text;
                        }
                    });
                    angular.forEach($rootScope.li5, function (value, key) {
                        if (value.value === $scope.model.CountryID) {
                            $scope.model.CountryName = value.text;
                        }
                    });
                    var ShiftId1 = [];
                    if ($scope.model.ShiftId1 !== null && $scope.model.ShiftId1 !== undefined) {
                        for (var i = 0; i < $scope.model.ShiftId1.length; i++) {
                            ShiftId1.push({ ShiftID: parseInt($scope.model.ShiftId1[i]) });
                        }
                    }
                    var imageTemp = $("#loadImage").prop('files')[0];

                    var fd = new FormData();
                    fd.append('file', imageTemp);
                    fd.append('Insert', JSON.stringify($scope.model));
                    fd.append('ListDetailDelete', JSON.stringify($rootScope.ListDetailDelete));
                    fd.append('PositionsConcurrently', JSON.stringify($rootScope.ListPositionsConcurrently));
                    fd.append('ShiftOfEmployees', JSON.stringify(ShiftId1));
                    $.ajax({
                        url: '/employees/Update_Employees',
                        data: fd,
                        processData: false,
                        contentType: false,
                        type: 'POST',
                        success: function (rs) {
                            if (rs.Error) {
                                App.notifyDanger(rs.Title);
                                return;
                            }
                            App.notifyInfo(rs.Title);
                            $rootScope.reloadEmployee();
                            $uibModalInstance.close();
                        },
                        error: function (rs) {

                            App.notifyInfo(rs.Title);
                        }
                    });
                } else {
                    App.notifyDanger("Địa chỉ email không hợp lệ");
                    return;
                }
            }
        }

    }

});
// xem nhân viên
app.controller('view', function ($http, $scope, $rootScope, $timeout, $compile, $uibModal, $confirm, $uibModalInstance, DTOptionsBuilder, DTColumnBuilder, DTInstances, keyhandle, FileUploader, dataservice, para, para1) {
    $scope.cancel_end = function () {
        $('#tblData_Salaries').parent().removeAttr("onscroll");
        $('#tblData_Insurances').parent().removeAttr("onscroll");
        $('#tblData_Contracts').parent().removeAttr("onscroll");
        $('#tblData_Histories').parent().removeAttr("onscroll");
        $('#tblData_Certificates').parent().removeAttr("onscroll");
        $('#tblData_Reward').parent().removeAttr("onscroll");
        $('#tblData_TrainingRequirement').parent().removeAttr("onscroll");
        $uibModalInstance.dismiss('cancel');

        //thêm sự kiện scroll phân trang      
    }
    $scope.model = {};
    $scope.dlgTitle = "Xem thông tin nhân viên";
    $scope.loadDataCombobox = function () {
        dataservice.getnationality(function (rs5) {
            $rootScope.li5 = rs5;
            $rootScope.lstCountry = rs5;
        });
        dataservice.getprovince(function (rs6) {
            $rootScope.li6 = rs6;
            $rootScope.lstProvince = rs6;
        });
        dataservice.getdistrict(function (rs7) {
            $rootScope.li7 = rs7;
            $rootScope.lstDistrict = rs7;
        });

    }

    var uploader = $scope.uploader = new FileUploader({
        // url: App.baseService() + '/upload'
    });
    uploader.filters.push({
        name: 'customFilter',
        fn: function (item /*{File|FileLikeObject}*/, options) {
            return this.queue.length < 10;
        }
    });
    uploader.onSuccessItem = function (fileItem, response, status, headers) {
        $scope.model.FileAttachments.push(response);
    };
    uploader.onCompleteAll = function () {
        $scope.commit();
    };
    $scope.dongAddress = function () {
        $rootScope.hidenAdress = "view";
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/address.html',
            controller: 'address',
            backdrop: true,
            windowClass: 'app-modal-window',
            resolve: {
                para1: function () {
                    return $rootScope.HomeNumber;
                },
                para2: function () {
                    return $rootScope.CommuneWard;
                },
                para3: function () {
                    return $rootScope.DistrictID;
                },
                para4: function () {
                    return $rootScope.ProvinceID;
                },
                para5: function () {
                    return $rootScope.CountryID;
                }
            }
        });
        modalInstance.result.then(function (d) {
            $rootScope.reloadEmployee();
        }, function () {
        });
    }
    $scope.Kiemnhiem = function (item) {
        if (item == false) {
            $scope.model.Kiemnhiem = true;
        }
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'openKiemnghiem.html',
            controller: 'openKiemnghiem',
            backdrop: true,
            size: '80',
            resolve: {
                para: function () {
                    return 5;
                }

            }
        });
        modalInstance.result.then(function (d) {

        }, function () {
        });

    }
    $scope.ShiftId1Config = {
        placeholder: 'Chọn ca làm việc',
        selectAll: true,
        search: true,
        searchText: 'Tìm kiếm...',
        showTitle: 'true',
        csvSepChar: ',',
        captionFormat: '{0} lựa chọn',
        captionFormatAllSelected: '{0} lựa chọn',
        noMatch: 'Không có dữ liệu "{0}"',
        okCancelInMulti: true,
        locale: ['Đồng ý', 'Hủy', 'Chọn tất cả']
    };
    $scope.initData = function () {
        dataservice.GetItemOpen(para, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {

                $scope.model = rs;
                $scope.model.StatusOfWorkOpen = $scope.model.StatusOfWork;
                if ($scope.model.ImagePath !== null && $scope.model.ImagePath !== "") {
                    $scope.model.imageLink = "/employees/GetPic/" + rs.EmployeeGuid;
                } else {
                    if ($scope.model.Gender === "M") {
                        $scope.model.imageLink = "/images/male.jpeg";
                    } else {
                        $scope.model.imageLink = "/images/female.jpeg";
                    }
                }

                $rootScope.HomeNumber = $scope.model.HomeNumber;
                $rootScope.CommuneWard = $scope.model.WardName;
                $rootScope.DistrictID = $scope.model.DistrictName;
                $rootScope.ProvinceID = $scope.model.ProvinceName;
                $rootScope.CountryID = $scope.model.CountryName;
                $rootScope.Address1 = $scope.model.PermanentAddress;
                //$scope.model.Language1 = JSON.parse(rs.Language);
                $scope.model.IssueIddate !== null ? $scope.model.IssueIddate = "/Date(" + new Date($scope.model.IssueIddate).getTime() + ")/" : undefined;
                $scope.model.BirthDate !== null ? $scope.model.BirthDate = "/Date(" + new Date($scope.model.BirthDate).getTime() + ")/" : undefined;
                $scope.model.HireDate !== null ? $scope.model.HireDate = "/Date(" + new Date($scope.model.HireDate).getTime() + ")/" : undefined;
                $scope.model.EndDateWork !== null ? $scope.model.EndDateWork = "/Date(" + new Date($scope.model.EndDateWork).getTime() + ")/" : undefined;
                $scope.model.StartDate !== null ? $scope.model.StartDate = "/Date(" + new Date($scope.model.StartDate).getTime() + ")/" : undefined;
                $scope.model.EndDate !== null ? $scope.model.EndDate = "/Date(" + new Date($scope.model.EndDate).getTime() + ")/" : undefined;
                if ($scope.model.SalariedFlag === true) {
                    $scope.model.SalariedFlag = 1;
                } else { $scope.model.SalariedFlag = 0; }
                if ($scope.model.StatusOfWork.indexOf("OM") >= 0) {
                    $scope.model.StatusOfWork = "Nhân viên chính thức";
                } else if ($scope.model.StatusOfWork.indexOf("PE") >= 0) {
                    $scope.model.StatusOfWork = "Nhân viên thử việc";
                }
                else if ($scope.model.StatusOfWork.indexOf("PT") >= 0) {
                    $scope.model.StatusOfWork = "Nhân viên thời vụ";
                }
                else if ($scope.model.StatusOfWork.indexOf("ML") >= 0) {
                    $scope.model.StatusOfWork = "Nghỉ thai sản";
                }
                else if ($scope.model.StatusOfWork.indexOf("OL") >= 0) {
                    $scope.model.StatusOfWork = "Nghỉ khác";
                }
                else if ($scope.model.StatusOfWork.indexOf("LJ") >= 0) {
                    $scope.model.StatusOfWork = "Nhân viên nghỉ việc";
                }
                if ($scope.model.Gender === "M") {
                    $scope.model.Gender = "Nam";
                } else if ($scope.model.Gender === "F") {
                    $scope.model.Gender = "Nữ";
                } else {
                    $scope.model.Gender = "";
                }
                if ($scope.model.MaritalStatus === "S") {
                    $scope.model.MaritalStatus = "Chưa kết hôn";
                } else if ($scope.model.MaritalStatus === "M") {
                    $scope.model.MaritalStatus = "Đã kết hôn";
                } else {
                    $scope.model.MaritalStatus = "";
                }
                if ($scope.model.Uniform === "Y") {
                    $scope.model.Uniform = "Đã bàn giao";
                } else if ($scope.model.Uniform === "N") {
                    $scope.model.Uniform = "Chưa bàn giao";
                } else {
                    $scope.model.Uniform = "";
                }

                angular.forEach($rootScope.SalaryTierIdData, function (value, key) {
                    if (value.value === $scope.model.SalaryTierId && value.value !== null) {
                        $scope.model.SalaryTierId = value.text;
                    }
                })
                angular.forEach($rootScope.listReasonLeaves, function (value, key) {
                    if (parseInt(value.value) === $scope.model.ReasonLeaveId && value.value !== null) {
                        $scope.model.ReasonLeaveId = value.text;
                    }
                })

                if ($scope.model.SalaryPayBy === "M") {
                    $scope.model.SalaryPayBy = "Trả lương theo tháng";
                } else if ($scope.model.SalaryPayBy === "H") {
                    $scope.model.SalaryPayBy = "Trả lương theo giờ";
                }
                else if ($scope.model.SalaryPayBy === "T") {
                    $scope.model.SalaryPayBy = "Trả lương theo tuần";
                }
                else if ($scope.model.SalaryPayBy === "K") {
                    $scope.model.SalaryPayBy = "Làm khoán";
                }
                else if ($scope.model.SalaryPayBy === "O") {
                    $scope.model.SalaryPayBy = "Khác";
                }
                else {
                    $scope.model.SalaryPayBy = "";
                }
                if ($scope.model.SalaryMethod === "CK") {
                    $scope.model.SalaryMethod = "Chuyển khoản";
                } else if ($scope.model.SalaryMethod === "TM") {
                    $scope.model.SalaryMethod = "Tiền mặt";
                }
                else if ($scope.model.SalaryMethod === "KH") {
                    $scope.model.SalaryMethod = "Khác";
                }
                else {
                    $scope.model.SalaryMethod = "";
                }

                dataservice.GetShiftOfEmployees($scope.model.EmployeeGuid, function (rs) {
                    if (rs.Error) {
                        App.notifyDanger(rs.Title);
                    } else {
                        if (rs.length > 0) {
                            $scope.model.ShiftId1 = rs;
                        } else {
                            $scope.model.ShiftId1 = [];
                        }
                        if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                            $scope.$apply();
                        }
                    }
                })

                if (rs.CreatedBy !== null && rs.CreatedBy !== undefined) {
                    if (rs.CreatedBy.includes('#')) {
                        $scope.model.CreatedBy = $scope.model.CreatedBy.split('#')[1];
                    }
                }
                if (rs.ModifiedBy !== null && rs.ModifiedBy !== undefined) {
                    if (rs.ModifiedBy.includes('#')) {
                        $scope.model.ModifiedBy = $scope.model.ModifiedBy.split('#')[1];
                    }
                }

            }
        });
    }
    $scope.initData();
    $scope.flagStatus = 0;
    $scope.OpenTable = function (data) {
        $scope.flagStatus = data;
    }

    //Xem đính kèm
    $scope.OpenFile = function () {
        if ($scope.flagStatus === 1) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: ctxfolder + '/openAttchment.html',
                controller: 'openattchmentFile',
                backdrop: 'static',
                size: '60',
                resolve: {
                    para: function () {
                        return $scope.bmodel.InsuranceGuid;
                    },
                    para1: function () {
                        return $scope.flagStatus;
                    }
                }
            });
        } else if ($scope.flagStatus === 2) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: ctxfolder + '/openAttchment.html',
                controller: 'openattchmentFile',
                backdrop: 'static',
                size: '60',
                resolve: {
                    para: function () {
                        return $scope.hmodel.ContractGuid;
                    },
                    para1: function () {
                        return $scope.flagStatus;
                    }
                }
            });

        }
        else if ($scope.flagStatus === 3) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: ctxfolder + '/openAttchment.html',
                controller: 'openattchmentFile',
                backdrop: 'static',
                size: '60',
                resolve: {
                    para: function () {

                        return $scope.kmodel.RewardGuid;

                    },
                    para1: function () {
                        return $scope.flagStatus;
                    }
                }
            });

        } else if ($scope.flagStatus === 4) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: ctxfolder + '/openAttchment.html',
                controller: 'openattchmentFile',
                backdrop: 'static',
                size: '60',
                resolve: {
                    para: function () {
                        return $scope.cmodel.CertificateGuid;
                    },
                    para1: function () {
                        return $scope.flagStatus;
                    }
                }
            });
        }
        else if ($scope.flagStatus === 7) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: ctxfolder + '/openAttchment.html',
                controller: 'openattchmentFile',
                backdrop: 'static',
                size: '60',
                resolve: {
                    para: function () {
                        return $scope.dmodel.TrainingRequestDetailGuid;
                    },
                    para1: function () {
                        return $scope.flagStatus;
                    }
                }
            });
        }
    }

   


});

app.controller('address', function ($scope, $rootScope, $compile, $uibModal, $confirm, $uibModalInstance, FileUploader, dataservice, para1, para2, para3, para4, para5) {
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    }

    $scope.ab = {
        HomeNumber: "",
        CommuneWard: "",
        DistrictID: "",
        ProvinceID: "",
        NationalityID: "",
        CountryID: "VN",
        DistrictIDName: "",
        ProvinceIDName: "",
        CountryIDName: ""
    }
    $scope.lstDistrictConfig = {
        search: true
    };
    $scope.li6Config = {
        search: true
    }
    $scope.li5Config = {
        search: true
    }

    if ($rootScope.hidenAdress === "view") {
        $scope.hidden = true;
        $scope.dlgTitle = "Xem địa chỉ";
    } else {
        $scope.hidden = false;
        $scope.dlgTitle = "Thêm địa chỉ";
    }
    if (para1 !== "" && para1 !== null && para1 !== undefined) {
        $scope.ab.HomeNumber = para1;
    }
    if (para2 !== "" && para2 !== null && para2 !== undefined) {
        $scope.ab.CommuneWard = para2;
    }
    if (para3 !== "" && para3 !== null && para3 !== undefined) {
        $scope.ab.DistrictID = para3;
        $rootScope.DistrictIDName = para3;
        angular.forEach($rootScope.li7, function (value, key) {
            if (value.value === $scope.ab.DistrictID) {
                $rootScope.DistrictIDName = value.text;
            }

        });
    }
    if (para4 !== "" && para4 !== null && para4 !== undefined) {
        $scope.ab.ProvinceID = para4;
        $rootScope.ProvinceIDName = para4;
        angular.forEach($rootScope.li6, function (value, key) {
            if (value.value === $scope.ab.ProvinceID) {
                $rootScope.ProvinceIDName = value.text;
            }

        });
    }
    if (para5 !== "" && para5 !== null && para5 !== undefined) {
        $scope.ab.CountryID = para5;
        $rootScope.CountryIDName = para5;
        angular.forEach($rootScope.li5, function (value, key) {
            if (value.value === $scope.ab.CountryID) {
                $rootScope.CountryIDName = value.text;
            }

        });
    }

    $scope.hoten = function (st) {
        if (st === true) {
            $scope.stahoten = true;
        } else {
            $scope.stahoten = false;
        }
        $scope.model.FullName = "";
        $scope.model.FullName = $rootScope.fHo($scope.model.FirstName, $scope.model.MiddleName, $scope.model.LastName);
    }

    $scope.OnchangeProvince = function (item) {
        $rootScope.lstDistrict = [];
        for (var a = 0; a < $rootScope.li7.length; a++) {
            if ($rootScope.li7[a].valueMore === item) {
                var obj = {
                    value: $rootScope.li7[a].value,
                    text: $rootScope.li7[a].text
                }
                $rootScope.lstDistrict.push(obj);
            }
        }
        if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
            $scope.$apply();
        }
    }
    $scope.OnchangeDistrict = function (item) {
        //$rootScope.lstProvince = [];
        //angular.forEach($rootScope.li7, function (value, key) {
        //    if (value.value === item) {
        //        var item1 = value.valueMore;
        //        angular.forEach($rootScope.li6, function (value1, key) {
        //            if (value1.value === item1) {
        //                $rootScope.lstProvince.push(value1);
        //            }
        //        });
        //    }
        //});
        //if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
        //    $scope.$apply();
        //}
    }
    $scope.OnchangeCountry = function (item) {
        $rootScope.lstProvince = [];
        angular.forEach($rootScope.li6, function (value, key) {
            if (value.valueMore === item) {

                $rootScope.lstProvince.push(value);
            }
        });
        if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
            $scope.$apply();
        }

    }
    $scope.submit = function () {

        $rootScope.HomeNumber = $scope.ab.HomeNumber;
        $rootScope.CommuneWard = $scope.ab.CommuneWard;
        $rootScope.DistrictID = $scope.ab.DistrictID;
        $rootScope.ProvinceID = $scope.ab.ProvinceID;
        $rootScope.CountryID = $scope.ab.CountryID;

        $rootScope.DistrictIDName = "";
        $rootScope.ProvinceIDName = "";
        $rootScope.CountryIDName = "";
        angular.forEach($rootScope.li7, function (value, key) {
            if (value.value === $scope.ab.DistrictID) {
                $rootScope.DistrictIDName = value.text;
            }

        });
        angular.forEach($rootScope.li6, function (value, key) {
            if (value.value === $scope.ab.ProvinceID) {
                $rootScope.ProvinceIDName = value.text;
            }

        });
        angular.forEach($rootScope.li5, function (value, key) {
            if (value.value === $scope.ab.CountryID) {
                $rootScope.CountryIDName = value.text;
            }

        });
        var sonha = ""; var phuongxa = ""; var quanhuyen = ""; var tinhtp = ""; var quocgia = "";
        if ($rootScope.HomeNumber !== "") { sonha = $rootScope.HomeNumber + ", "; }
        if ($rootScope.CommuneWard !== "") { phuongxa = $rootScope.CommuneWard + ", "; }
        if ($rootScope.DistrictIDName !== "") { quanhuyen = $rootScope.DistrictIDName + ", "; }
        if ($rootScope.ProvinceIDName !== "") { tinhtp = $rootScope.ProvinceIDName + ", "; }
        if ($rootScope.CountryIDName !== "") { quocgia = $rootScope.CountryIDName; }

        $rootScope.Address1 = sonha + phuongxa + quanhuyen + tinhtp + quocgia;
        $uibModalInstance.dismiss('cancel');


    };




});
