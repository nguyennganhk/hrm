﻿var ctxfolder = "/views/HR/Contracts";
app.factory('dataservice', function ($http) {
    var headers = {
        "Content-Type": "application/json;odata=verbose",
        "Accept": "application/json;odata=verbose",
    }
    return {
        insert: function (data, callback) {
            $http.post('/Contracts/Insert', data).success(callback);
        },
        Update: function (data, callback) {
            $http.post('/Contracts/Update', data).success(callback);
        },
        deleteItems: function (data, callback) {
            $http.post('/Contracts/DeleteItems', data).success(callback);
        },
        delete: function (data, callback) {
            $http.post('/Contracts/Delete', data).success(callback);
        },
        getItem: function (data, callback) {
            $http.post('/Contracts/GetItem', data).success(callback);
        },
        
    }
});
app.filter('fdate', [
    '$filter', function ($filter) {
        return function (input, f) {
            if (input && input.toString().indexOf('Date') > -1) {
                return moment(input).format(f);
            } if (input && input.toString().indexOf('T') > -1) {
                return moment(input).format(f);
            } else return input;
        };
    }
]);
app.directive('format', ['$filter', function ($filter) {
    return {
        require: '?ngModel',
        link: function (scope, elem, attrs, ctrl) {
            if (!ctrl) return;
            ctrl.$formatters.unshift(function (a) {
                return $filter(attrs.format)(ctrl.$modelValue)
            });

            ctrl.$parsers.unshift(function (viewValue) {
                var plainNumber = viewValue.replace(/[^\d|\-+|\.+]/g, '');
                elem.val($filter(attrs.format)(plainNumber));
                return plainNumber;
            });
        }
    };
}]);
app.filter('html', ['$sce', function ($sce) {
    return function (text) {
        return $sce.trustAsHtml(text);
    };
}])
var spassAresSlug = function (number, isview) {
    try {
        // khai bao bien su dung       
        var numbers = number; var length = numbers.length; if (isview) {
            // khi dang la hien thi        
            if (numbers.indexOf(".") != -1) {
                length = numbers.indexOf(".");
                numbers = number.replace(".", ",");
            }
        } else {
            // khi dang la input textbox           
            if (numbers.indexOf(",") != -1) length = numbers.indexOf(",");
        }
        // loai bo nhung ky tu khong can thiet       
        // check so 0         
        var isTrue = true; while (isTrue) {
            if (numbers[0] == "0") numbers = numbers.substr(1, numbers.length);
            else isTrue = false;
        };
        // check dau phay      
        numbers = numbers.split(",");
        var string = "";
        for (var i = 0; i < numbers.length; i++) {
            if (i == 0) string += numbers[i];
            else if (i == 1) string += "," + numbers[i];
            else string += numbers[i];
        } numbers = string;
        // dung chung cho 2 truong hop       
        var no = 3, index = length - no;
        while (index > 0) { numbers = numbers.substr(0, index) + '.' + numbers.substr(index); index -= no; };
        return numbers == "" ? 0 : numbers;
    } catch (ex) { }
    return numbers == "" ? 0 : numbers;
}

app.directive('formatsub', ['$filter', function ($filter) {
    return {
        require: '?ngModel', link: function (scope, elem, attrs, ctrl) {
            if (!ctrl) return; ctrl.$formatters.unshift(function (viewValue) {
                try {
                    plainNumber = viewValue.replace('.', ','); elem.val(spassAresSlug(plainNumber));
                    return spassAresSlug(plainNumber);
                } catch (ex) { return undefined; }
            }); ctrl.$parsers.unshift(function (viewValue) {
                plainNumber = viewValue.replace(/[^-0-9,]/g, '');
                elem.val(spassAresSlug(plainNumber)); return plainNumber.replace(/[,]/g, '.');
            });
        }
    };
}]);

app.controller('CtrlContracts', function ($scope, $rootScope, $compile, dataservice) {
    $rootScope.go = function (path) {
        $location.path(path); return false;
    };
    $rootScope.LoginName = $("#loginName").val();
    
    $rootScope.ListIsActive = [  
        {
            value: null,
            text: 'Bỏ chọn'
        },
        {
            value: '1',
            text: 'Sử dụng'
        }, {
            value: '0',
            text: 'Không sử dụng'
        }
    ];
    
    $rootScope.validationOptions = {
        rules: {
            Title: {
                required: true,
                maxlength: 20
            },
            ContractDate: {
                required: true
            }

        },
        messages: {
            Title: {
                required: "Yêu cầu nhập tiêu đề",
                maxlength: "Tiêu đề không vượt quá 250 ký tự."
            },
            ContractDate: {
                required: "Yêu cầu nhập ngày hợp đồng"
            }

        }
    }
    $rootScope.StatusData = [{
        value: 1,
        text: 'Sử dụng'
    }, {
        value: 0,
        text: 'Không sử dụng'
    }];
    $rootScope.CreateDate = function (date) {
        if (date !== null && date !== undefined) {
            var newdate = new Date(date);
            var month = newdate.getMonth() + 1;
            var day = newdate.getDate();
            var year = newdate.getFullYear();
            var hh = newdate.getHours();
            var mm = newdate.getMinutes();
            if (month < 10)
                month = "0" + month;
            if (day < 10)
                day = "0" + day;
            if (mm < 10)
                mm = "0" + mm;
            return todayDate = day + "/" + month + "/" + year;
        } else {
            return null;
        }
    }
    $rootScope.ConvertStringTonumber = function (str) {
        if (str !== null && str !== "" && str !== undefined && str > 0) {
            var x = parseFloat(str);
            return x;
        } else {
            return "";
        }

    }
    $rootScope.addPeriod1 = function (nStr) {
        if (nStr !== null && nStr !== "" && nStr !== undefined) {
            nStr = Math.round(parseFloat(nStr) * 100) / 100;

            nStr += "";
            if (nStr.indexOf(",") >= 0) {
                var x = nStr.split(",");
            } else {
                var x = nStr.split(".");
            }
            var x1 = x[0];
            var x2 = x.length > 1 && parseInt(x[1]) > 0 ? "," + x[1] : "";
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, "$1" + "." + "$2");
            }
            var Total = x1 + x2;
            return Total;

        }
        else {
            return 0;
        }
    };
    $rootScope.GenderData = [
        {
            value: "M",
            text: 'Nam'
        }, {
            value: "F",
            text: 'Nữ'
        }];
    $rootScope.StatusOfWorkData = [
        {
            value: 'OM',
            text: 'Nhân viên chính thức'

        }, {
            value: 'PE',
            text: 'Nhân viên thử việc'

        },
        {
            value: 'PT',
            text: 'Nhân viên thời vụ'

        },
        {
            value: 'ML',
            text: 'Nghỉ thai sản'

        },
        {
            value: 'OL',
            text: 'Nghỉ khác'

        },
        {
            value: 'LJ',
            text: 'Nhân viên nghỉ việc'

        },
        {
            value: 'EC',
            text: 'Hết hạn hợp đồng'

        }
    ];

    $rootScope.FilterResultData = [
        {
            value: null,
            text: 'Bỏ chọn'
        },
        {
            value: 'F',
            text: 'Chưa lọc'
        }, {
            value: 'Y',
            text: 'Đạt'
        }, {
            value: 'N',
            text: 'Không đạt'
        }
    ];

    $rootScope.CreateDate = function (date) {
        if (date !== null && date !== undefined) {
            var newdate = new Date(date);
            var month = newdate.getMonth() + 1;
            var day = newdate.getDate();
            var year = newdate.getFullYear();
            var hh = newdate.getHours();
            var mm = newdate.getMinutes();
            if (month < 10)
                month = "0" + month;
            if (day < 10)
                day = "0" + day;
            if (mm < 10)
                mm = "0" + mm;
            return todayDate = day + "/" + month + "/" + year;
        } else {
            return null;
        }
    };
    $rootScope.convertDateSearch = function (datetime) {
        if (datetime !== null && datetime !== "" && datetime !== undefined) {
            var Str = datetime.toString();
            if (Str.indexOf("/Date") >= 0) {

                var newdate = new Date(parseInt(datetime.substr(6)));
                var month = newdate.getMonth() + 1;
                var day = newdate.getDate();
                var year = newdate.getFullYear();
                var hh = newdate.getHours();
                var mm = newdate.getMinutes();
                if (month < 10)
                    month = "0" + month;
                if (day < 10)
                    day = "0" + day;
                return month + "/" + day + "/" + year;
            } else {
                var value = datetime.split('/');

                return value[2] + '-' + value[1] + '-' + value[0];
            }
        }
        return null;
    };
    $rootScope.DateCurrent = function () {
        var newdate = new Date();
        var month = newdate.getMonth() + 1;
        var day = newdate.getDate();
        var year = newdate.getFullYear();
        var hh = newdate.getHours();
        var mm = newdate.getMinutes();

        if (month < 10)
            month = "0" + month;
        if (day < 10)
            day = "0" + day;
        if (mm < 10)
            mm = "0" + mm;
        return month + "/" + day + "/" + year;
    };
    //0 11/11/2017
    //1 11/11/2017 22:22
    //2 123123123
    //3 type date
    //4 type data year month day
    $rootScope.ConDate = function (data, number) {
        try {
            if (data == null || data == "") {
                return '';
            }
            if (data !== null && data != "" && data != undefined) {
                try {
                    if (data.indexOf("SA") != -1 || data.indexOf("CH") != -1) {
                        if (data.indexOf("SA") != -1) { }
                        if (data.indexOf("CH") != -1) { }
                    }

                    if (data.indexOf('Date') != -1) {
                        data = data.substring(data.indexOf("Date") + 5);
                        data = parseInt(data);
                    }
                }
                catch (ex) { }
                var newdate = new Date(data);
                if (number == 3) {
                    return newdate;
                }
                if (number == 2) {
                    return "/Date(" + newdate.getTime() + ")/";
                }
                var month = newdate.getMonth() + 1;
                var day = newdate.getDate();
                var year = newdate.getFullYear();
                var hh = newdate.getHours();
                var mm = newdate.getMinutes();
                if (month < 10)
                    month = "0" + month;
                if (day < 10)
                    day = "0" + day;
                if (mm < 10)
                    mm = "0" + mm;
                if (number == 0) {
                    return todayDate = day + "/" + month + "/" + year;
                }
                if (number == 1) {
                    return todayDate = day + "/" + month + "/" + year + " " + hh + ":" + mm;
                }
                if (number == 4) {
                    return new Date(year, month - 1, day);
                }
            } else {
                return '';
            }
        } catch (ex) {
            return "";
        }
    }
    //định dạng số xóa dấu chấm phẩy
    $rootScope.addPeriod = function (nStr) {
        if (nStr !== null && nStr !== "" && nStr !== undefined) {
            nStr = Math.round(parseFloat(nStr) * 100) / 100;

            nStr += "";
            if (nStr.indexOf(",") >= 0) {
                var x = nStr.split(",");
            } else {
                var x = nStr.split(".");
            }
            var x1 = x[0];
            var x2 = x.length > 1 && parseInt(x[1]) > 0 ? "," + x[1] : "";
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, "$1" + "." + "$2");
            }
            var Total = x1 + x2;
            return Total;

        }
        else {
            return 0;
        }
    };

});

app.config(function ($routeProvider, $validatorProvider) {
    $routeProvider
        .when('/', {
            templateUrl: ctxfolder + '/index.html',
            controller: 'index'
        })
        .when('/edit/:id', {
            templateUrl: ctxfolder + '/edit.html',
            controller: 'edit'
        }).when('/open/:id', {
            templateUrl: ctxfolder + '/open.html',
            controller: 'open'
        })
        .when('/add/', {
            templateUrl: ctxfolder + '/add.html',
            controller: 'add'
        })
    $validatorProvider.setDefaults({
        errorElement: 'span',
        errorClass: 'help-block',
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        success: function (label) {
            label.closest('.form-group').removeClass('has-error');
        }
    });
});
app.controller('index', function ($http, $scope, $rootScope, $compile, $confirm, $uibModal, DTOptionsBuilder, DTColumnBuilder, DTInstances, dataservice, keyhandle, utility) {

    var vm = $scope;
    $scope.selected = [];
    $scope.selectAll = false;
    $scope.toggleAll = toggleAll;
    $scope.toggleOne = toggleOne;
    $scope.staticParam = {
        IsActive: "1",
        Search: "",
        recordsTotal:0
    };
    $scope.TotalRow = 0;
    var titleHtml = '<label class="mt-checkbox"><input type="checkbox" ng-model="selectAll" ng-click="toggleAll(selectAll, selected)"/><span></span></label>';
    vm.dtOptions = DTOptionsBuilder.newOptions()
        .withPaginationType('full_numbers').withDOM("t<'table-scrollable't>ip")
        .withDataProp('data').withDisplayLength(10)
        .withOption('order', [3, 'desc'])
        .withOption('serverSide', true)
        .withOption('stateLoadParams', function (settings, data) {
            data.search.search = '';
        })
        .withOption('headerCallback', function (header) {
            if (!$scope.headerCompiled) {
                $scope.headerCompiled = true;
                $compile(angular.element(header).contents())($scope);
            }
        })
        .withOption('rowCallback', rowCallback)
        .withOption('initComplete', function (settings, json) {
        })
        .withOption('createdRow', function (row, data, dataIndex) {
            const contextScope = $scope.$new(true);
            contextScope.data = data;
            $scope.data = data;
            $compile(row)($scope);
            contextScope.contextMenu = $scope.contextMenu;
            $compile(angular.element(row).find('input'))($scope);
            $compile(angular.element(row).attr('context-menu', 'contextMenu'))(contextScope);
        });
    vm.dtOptions.withOption('ajax', {
        url: "/Contracts/jtable",
        beforeSend: function (jqXHR, settings) {
            App.blockUI({
                target: "#contentMain",
                boxed: true,
                message: 'Đang tải...'
            });
        },
        type: 'POST',
        data: function (d) {
            d.search.value = $scope.staticParam.Search;
            var IsActive = null;
            if ($scope.staticParam.IsActive === "1") {
                IsActive = true;
            } else if ($scope.staticParam.IsActive === "0") {
                IsActive = false;
            }
            d.IsActive = IsActive;
            $scope.staticParam.currentPage = $scope.dtInstance.DataTable.page() + 1;          
        },
        complete: function (res) {
            $scope.TotalRow =  res.responseJSON.recordsTotal;
            App.unblockUI("#contentMain");
        }
    }).withOption('serverSide', true);
    vm.dtColumns = [];
    vm.dtColumns.push(DTColumnBuilder.newColumn("Id").withTitle(titleHtml).notSortable()
        .renderWith(function (data, type, full, meta) {
            $scope.selected[data] = false;
            var dm = "selected['" + data + "']";
            return '<label class="mt-checkbox"><input type="checkbox" ng-model="' + dm + '" ng-click="toggleOne(selected)"/><span></span></label>';
        }).withOption('sWidth', '5px').withOption('sClass', 'tcenter'));
    vm.dtColumns.push(DTColumnBuilder.newColumn('_STT').withTitle('STT').notSortable().withOption('sWidth', '5px').renderWith(function (data, type, full, meta) {
        return ($scope.staticParam.currentPage - 1) * 10 + data;
    }).withOption('sClass', 'tcenter'));

    vm.dtColumns.push(DTColumnBuilder.newColumn('Title').withTitle('Tiêu đề').withOption('sWidth', '300px').renderWith(function (data, type, full, meta) {
        var open = 'open("' + full.Id + '")';
        return "<a href='' ng-click='" + open + "' >" + data + "</a>";
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('Salary').withTitle('Tiền').withOption('sWidth', '100px').renderWith(function (data, type, full, meta) {
        return $rootScope.addPeriod(data);
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('ContractDate').withTitle('Ngày').withOption('sWidth', '100px').renderWith(function (data, type, full, meta) {
        return full.ContractDateString;
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('Salary').withTitle('Tiền').withOption('sWidth', '100px').renderWith(function (data, type, full, meta) {
        return $rootScope.addPeriod(data);
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('ContractDate').withTitle('Ngày').withOption('sWidth', '100px').renderWith(function (data, type, full, meta) {
        return full.ContractDateString;
    }));
    
  
    vm.dtColumns.push(DTColumnBuilder.newColumn('ContractDate').withTitle('Ngày').withOption('sWidth', '100px').renderWith(function (data, type, full, meta) {
        return full.ContractDateString;

    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('Salary').withTitle('Tiền').withOption('sWidth', '100px').renderWith(function (data, type, full, meta) {
        return $rootScope.addPeriod(data);
    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('ContractDate').withTitle('Ngày').withOption('sWidth', '100px').renderWith(function (data, type, full, meta) {
        return full.ContractDateString;

    }));
    vm.dtColumns.push(DTColumnBuilder.newColumn('Id').notSortable().withTitle('Điều khiển').withOption('sWidth', '50px').withOption('sClass', 'tcenter').renderWith(function (data, type, full, meta) {
        var open = "open('" + data + "')";
        var edit = "edit('" + data + "')";
        var delete1 = "delete('" + data + "')";
        return '<div class="table__cell-actions-wrap"><a ng-click=' + open + ' class="table__cell-actions-item table__cell-actions-icon"> <span class="mdi mdi-eye" title="Xem chi tiết"></span> </a>' +
            '<a ng-click=' + edit + ' class="table__cell-actions-item table__cell-actions-icon"><span class="mdi mdi-table-edit" title="Sửa"></span> </a>' +
            '<a ng-click=' + delete1 + ' class="table__cell-actions-item table__cell-actions-icon"><span class="mdi mdi-delete" title="Xóa"></span> </a></div>';
    }));

    vm.reloadData = reloadData;
    vm.dtInstance = {};
    function reloadData(resetPaging) {
        $scope.staticParam.currentPage = resetPaging;
        vm.dtInstance.reloadData(callback, resetPaging);
    }
    function callback(json) {

    }
    function toggleAll(selectAll, selectedItems) {
        for (var id in selectedItems) {
            if (selectedItems.hasOwnProperty(id)) {
                selectedItems[id] = selectAll;
            }
        }
    }
    function toggleOne(selectedItems) {
        for (var id in selectedItems) {
            if (selectedItems.hasOwnProperty(id)) {
                if (!selectedItems[id]) {
                    vm.selectAll = false;
                    return;
                }
            }
        }
        vm.selectAll = true;
    }
    function rowCallback(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        $('td', nRow).unbind('dblclick');
        $('td', nRow).bind('dblclick', function () {
            $scope.$apply(function () {
                $scope.open(aData.Id);
            });
        });
        return nRow;
    }
    $scope.reload = function () {
        reloadData(true);

    };
    //Trạng thái
    $scope.StatusConfig = {
        placeholder: 'Chọn trạng thái',
        okCancelInMulti: true,
        selected: ['true']
    };
    $scope.IsActiveChange = function (data) {
        $scope.staticParam.IsActive = data !=="null" ? data : "";
        $scope.reload();
    };
    $scope.reloadALL = function () {
        $scope.staticParam.Search = "";
        $scope.staticParam.IsActive = '1';
        $scope.selectAll = false;
        reloadData(true);
    };
    $rootScope.reload = function () {
        reloadData(true);
    };
    $scope.add = function () {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/add.html',
            controller: 'add',
            backdrop: true,
            size: '80'
        });
        modalInstance.result.then(function (d) {
            $scope.reload();
        }, function () {
        });
    }
    $scope.edit = function (Id) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/edit.html',
            controller: 'edit',
            backdrop: true,
            size: '80',
            resolve: {
                para: function () {
                    return Id;
                }
            }
        });
        modalInstance.result.then(function (d) {
            $scope.reload();
        }, function () {
        });
    }
    $scope.open = function (Id) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/open.html',
            controller: 'open',
            backdrop: true,
            size: '80',
            resolve: {
                para: function () {
                    return Id;
                }
            }
        });
        modalInstance.result.then(function (d) {
            $scope.reload();
        }, function () {
        });
    }
    $scope.delete = function (Id) {
        dataservice.getItem({ IdI: [Id] }, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {
                $confirm({ text: 'Bạn có chắc chắn xóa ' + rs.Title + ' không ?', title: 'Xác nhận', ok: 'Đồng ý', cancel: ' Đóng' })
                    .then(function () {
                        App.blockUI({
                            target: "#contentMain",
                            boxed: true,
                            message: 'Đang tải...'
                        });
                        dataservice.delete({ IdI: [Id] }, function (result) {
                            if (result.Error) {
                                App.notifyDanger(result.Title);
                            } else {
                                App.notifyInfo(result.Title);
                                $scope.reload();
                            }
                            App.unblockUI("#contentMain");
                        });
                    });
            }
        })
    }
    $scope.deleteChecked = function () {
        var deleteItems = [];
        for (var id in $scope.selected) {
            if ($scope.selected.hasOwnProperty(id)) {
                if ($scope.selected[id]) {
                    deleteItems.push(id);
                }
            }
        }
        if (deleteItems.length > 0) {
            $confirm({ text: 'Bạn có chắc chắn muốn xóa các khoản mục đã chọn?', title: 'Xác nhận', ok: 'Đồng ý', cancel: ' Đóng' })
                .then(function () {
                    App.blockUI({
                        target: "#contentMain",
                        boxed: true,
                        message: 'Đang tải...'
                    });
                    dataservice.deleteItems(deleteItems, function (result) {
                        if (result.Error) {
                            App.notifyDanger(result.Title);
                        } else {
                            App.notifyInfo(result.Title);
                            $scope.reload();
                        }
                        App.unblockUI("#contentMain");
                    });

                });
        } else {
            App.notifyDanger("Không có khoản mục nào được chọn");
        }
    }
    
    $scope.contextMenu = [
        [function ($itemScope) {
            return '<i class="mdi mdi-eye"></i> Xem';
        }, function ($itemScope, $event, model) {
            $scope.open($itemScope.data.Id);
        }, function ($itemScope, $event, model) {
                return App.Permissions.OPEN;
        }],
        [function ($itemScope) {
            return '<i class="mdi mdi-table-edit"></i> Sửa';
        }, function ($itemScope, $event, model) {
            $scope.edit($itemScope.data.Id);
        }, function ($itemScope, $event, model) {
                return App.Permissions.EDIT;
        }],
        [function ($itemScope) {
            return '<i class="mdi mdi-delete"></i> Xóa';
        }, function ($itemScope, $event, model) {
            $scope.delete($itemScope.data.Id);
        }, function ($itemScope, $event, model) {
                return App.Permissions.DELETE;
        }]
    ];



});

app.controller('add', function ($scope, $rootScope, $compile, $uibModal, $confirm, $uibModalInstance, FileUploader, dataservice) {
    $scope.Title = "Thêm hợp đồng";
    $scope.model = { ContractType: 2, IsActive: "1" };
    $scope.modeljexcel = { Jexcel: {} };
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    }
    $scope.conjexcel = function (number) {
        if (number > 0) {
            return number.toString().replaceAll('[.]', ',');
        } else return 0;

    }
    $scope.model.FileAttachments = [];
    var uploader = $scope.uploader = new FileUploader({
        // url: App.baseService() + '/upload'
    });
    uploader.filters.push({
        name: "customFilter",
        fn: function (item /*{File|FileLikeObject}*/, options) {
            return this.queue.length < 10;
        }
    });
    uploader.onSuccessItem = function (fileItem, response, status, headers) {
        $scope.jmodel.FileAttachments.push(response);
    };
    $scope.jmodelPer = {};
    uploader.onCompleteAll = function () {
        $scope.commit();
    };
    $scope.rowchange = function (obj, row, cell, columnName) {
        var rowindex = parseInt(row); // dòng muốn thay đổi        
        var ExchangeRate = 0;
        if ($scope.model.ExchangeRate !== undefined && $scope.model.ExchangeRate !== null && $scope.model.ExchangeRate !== "") {
            ExchangeRate = parseFloat($scope.model.ExchangeRate);
        } else {
            ExchangeRate = 0;
        }

        $scope.ListData = $scope.modeljexcel.Jexcel.jexcel('getData');
        if (columnName === "ProductId") { //nếu bằng cell hiện tại bằng cell muốn thay đổi thì đổi giá trị               
            var dv = ""; var ItemId = ""; var ItemName = ""; var UnitConvertGuid = ""; var UnitId = ""; var UnitTitle = "";
            dataservice.getItems($scope.ListData[rowindex].ProductId, function (rs) {
                if (rs !== null) {
                    dv = rs.OrganizationGuid;
                    ItemId = rs.ItemId; ItemName = rs.ItemName;
                    UnitConvertGuid = rs.UnitGuid; UnitId = rs.UnitId;
                    UnitTitle = rs.UnitName;
                }
                $scope.modeljexcel.Jexcel.jexcel("setValueByName", "ProductName", row, ItemName);
                $scope.modeljexcel.Jexcel.jexcel('setValueByName', "UnitId", row, UnitId);

            });
        }
        // Thay đổi cột mã hàng      
        if (columnName === "Quantity" || columnName === "UnitPrice") {
            // tính thành tiền
            var ValuaCelH = parseFloat($scope.ListData[rowindex].Quantity) * parseFloat($scope.ListData[rowindex].UnitPrice); ValuaCelH = ValuaCelH > 0 ? ValuaCelH : 0; $scope.modeljexcel.Jexcel.jexcel("setValueByName", "AmountOc", row, $scope.conjexcel(ValuaCelH));
            // tính thành tiền quy đổi
            var ValuaCelI = parseFloat($scope.ListData[rowindex].Quantity) * parseFloat($scope.ListData[rowindex].UnitPrice) * ExchangeRate; ValuaCelI = ValuaCelI > 0 ? ValuaCelI : 0; $scope.modeljexcel.Jexcel.jexcel("setValueByName", "Amount", row, $scope.conjexcel(ValuaCelI));
            // tính tiền chiết khấu
            var ValuaCelN = ValuaCelH * $scope.ListData[rowindex].DiscountPercent / 100; ValuaCelN = ValuaCelN > 0 ? ValuaCelN : 0; $scope.modeljexcel.Jexcel.jexcel("setValueByName", "DiscountOc", row, $scope.conjexcel(ValuaCelN));
            // tính tiền chiết khấu quy đổi
            var ValuaCelO = ValuaCelN * ExchangeRate; ValuaCelO = ValuaCelO > 0 ? ValuaCelO : 0; $scope.modeljexcel.Jexcel.jexcel("setValueByName", "Discount", row, $scope.conjexcel(ValuaCelO));
            // tính tiền thuế
            var ValuaCelK = (ValuaCelH - parseFloat(ValuaCelN)) * parseFloat($scope.ListData[rowindex].Vatpercent) / 100; ValuaCelK = ValuaCelK > 0 ? ValuaCelK : 0; $scope.modeljexcel.Jexcel.jexcel("setValueByName", "VatamountOc", row, $scope.conjexcel(ValuaCelK));
            // tính tiền thuế quy đổi
            var ValuaCelL = ValuaCelK * ExchangeRate; ValuaCelL = ValuaCelL > 0 ? ValuaCelL : 0; $scope.modeljexcel.Jexcel.jexcel("setValueByName", "Vatamount", row, $scope.conjexcel(ValuaCelL));

            // tính tổng tiền thanh toán
            var ValuaCelP4 = ValuaCelH + ValuaCelK - ValuaCelN; ValuaCelP4 = ValuaCelP4 > 0 ? ValuaCelP4 : 0; $scope.modeljexcel.Jexcel.jexcel("setValueByName", "TongtienTT", row, $scope.conjexcel(ValuaCelP4));
            // tính tổng tiền thanh toán quy đổi
            var ValuaCelQ16 = (ValuaCelH + ValuaCelK - ValuaCelN) * ExchangeRate; ValuaCelQ16 = ValuaCelQ16 > 0 ? ValuaCelQ16 : 0; $scope.modeljexcel.Jexcel.jexcel("setValueByName", "TongtienTTQD", row, $scope.conjexcel(ValuaCelQ16));
        }
        if (columnName === "Vatpercent") {
            $scope.col9 = parseFloat($scope.ListData[rowindex].Vatpercent);
            $scope.col7 = parseFloat($scope.ListData[rowindex].AmountOc);
            $scope.col12 = parseFloat($scope.ListData[rowindex].DiscountPercent);
            $scope.col13 = parseFloat($scope.ListData[rowindex].DiscountOc);
            $scope.ValuaCelK = ($scope.col7 - $scope.col13) * $scope.col9 / 100; $scope.ValuaCelK = $scope.ValuaCelK > 0 ? $scope.ValuaCelK : 0; $scope.modeljexcel.Jexcel.jexcel("setValueByName", "VatamountOc", row, $scope.conjexcel($scope.ValuaCelK));
            // tính tiền thuế quy đổi
            $scope.ValuaCelL = $scope.ValuaCelK * ExchangeRate; $scope.ValuaCelL = $scope.ValuaCelL > 0 ? $scope.ValuaCelL : 0; $scope.modeljexcel.Jexcel.jexcel("setValueByName", "Vatamount", row, $scope.conjexcel($scope.ValuaCelL));
            // tính tiền chiết khấu
            $scope.ValuaCelN = $scope.col7 * $scope.col12 / 100; $scope.ValuaCelN = $scope.ValuaCelN > 0 ? $scope.ValuaCelN : 0; $scope.modeljexcel.Jexcel.jexcel("setValueByName", "DiscountOc", row, $scope.conjexcel($scope.ValuaCelN));
            // tính tiền chiết khấu quy đổi
            $scope.ValuaCelO = $scope.ValuaCelN * ExchangeRate; $scope.ValuaCelO = $scope.ValuaCelO > 0 ? $scope.ValuaCelO : 0; $scope.modeljexcel.Jexcel.jexcel("setValueByName", "Discount", row, $scope.conjexcel($scope.ValuaCelN));
            // tính tổng tiền thanh toán
            $scope.ValuaCelP4 = $scope.col7 + $scope.ValuaCelK - $scope.col13; $scope.ValuaCelP4 = $scope.ValuaCelP4 > 0 ? $scope.ValuaCelP4 : 0; $scope.modeljexcel.Jexcel.jexcel("setValueByName", "TongtienTT", row, $scope.conjexcel($scope.ValuaCelP4));
            // tính tổng tiền thanh toán quy đổi
            $scope.ValuaCelQ16 = ($scope.col7 + $scope.ValuaCelK - $scope.col13) * ExchangeRate; $scope.ValuaCelQ16 = $scope.ValuaCelQ16 > 0 ? $scope.ValuaCelQ16 : 0; $scope.modeljexcel.Jexcel.jexcel("setValueByName", "TongtienTTQD", row, $scope.conjexcel($scope.ValuaCelQ16));
        }
        if (columnName === "DiscountOc") {
            $scope.ListData = $('#myht').jexcel('getData');
            $scope.col7 = parseFloat($scope.ListData[rowindex].AmountOc);
            $scope.col9 = parseFloat($scope.ListData[rowindex].Vatpercent);
            $scope.col10 = parseFloat($scope.ListData[rowindex].VatamountOc);
            $scope.col13 = parseFloat($scope.ListData[rowindex].DiscountOc);
            $scope.ValuaCelK = ($scope.col7 - $scope.col13) * $scope.col9 / 100; $scope.ValuaCelK = $scope.ValuaCelK > 0 ? $scope.ValuaCelK : 0; $scope.modeljexcel.Jexcel.jexcel("setValueByName", "DiscountOc", row, $scope.conjexcel($scope.ValuaCelK));
            // tính tiền thuế quy đổi
            $scope.ValuaCelL = $scope.ValuaCelK * ExchangeRate; $scope.ValuaCelL = $scope.ValuaCelL > 0 ? $scope.ValuaCelL : 0; $scope.modeljexcel.Jexcel.jexcel("setValueByName", "Discount", row, $scope.conjexcel($scope.ValuaCelL));

            // tính % chiết khấu
            $scope.ValuaCelM13 = $scope.col13 * 100 / $scope.col7; $scope.ValuaCelM13 = $scope.ValuaCelM13 > 0 ? $scope.ValuaCelM13 : 0; $scope.modeljexcel.Jexcel.jexcel("setValueByName", "DiscountPercent", row, $scope.conjexcel($scope.ValuaCelM13));
            // tính tiền chiết khấu quy đổi
            $scope.ValuaCelO12 = $scope.col13 * ExchangeRate; $scope.ValuaCelO12 = $scope.ValuaCelO12 > 0 ? $scope.ValuaCelO12 : 0; $scope.modeljexcel.Jexcel.jexcel("setValueByName", "Discount", row, $scope.conjexcel($scope.ValuaCelO12));
            // tính tổng tiền thanh toán
            $scope.ValuaCelP13 = $scope.col7 + $scope.ValuaCelK - $scope.col13; $scope.ValuaCelP13 = $scope.ValuaCelP13 > 0 ? $scope.ValuaCelP13 : 0; $scope.modeljexcel.Jexcel.jexcel("setValueByName", "TongtienTT", row, $scope.conjexcel($scope.ValuaCelP13));
            // tính tổng tiền thanh toán quy đổi
            $scope.ValuaCelQ16 = $scope.ValuaCelP13 * ExchangeRate; $scope.ValuaCelQ16 = $scope.ValuaCelQ16 > 0 ? $scope.ValuaCelQ16 : 0; $scope.modeljexcel.Jexcel.jexcel("setValueByName", "TongtienTTQD", row, $scope.conjexcel($scope.ValuaCelQ16));
        }
        if (columnName === "DiscountPercent") {
            $scope.col7 = parseFloat($scope.ListData[rowindex].AmountOc);
            $scope.col9 = parseFloat($scope.ListData[rowindex].Vatpercent);
            $scope.col10 = parseFloat($scope.ListData[rowindex].VatamountOc);
            $scope.col12 = parseFloat($scope.ListData[rowindex].DiscountPercent);
            // tính tiền chiết khấu
            var ValuaCelN12 = $scope.col12 * $scope.col7 / 100; ValuaCelN12 = ValuaCelN12 > 0 ? ValuaCelN12 : 0; $scope.modeljexcel.Jexcel.jexcel("setValueByName", "DiscountOc", row, $scope.conjexcel(ValuaCelN12));
            // tính tiền chiết khấu quy đổi
            var ValuaCelO12 = ValuaCelN12 * ExchangeRate; ValuaCelO12 = ValuaCelO12 > 0 ? ValuaCelO12 : 0; $scope.modeljexcel.Jexcel.jexcel("setValueByName", "Discount", row, $scope.conjexcel(ValuaCelO12));
            $scope.ListData = $('#myht').jexcel('getData');
            $scope.col13 = parseFloat($scope.ListData[rowindex].DiscountOc);
            // tính tiền thuế 
            $scope.ValuaCelK = ($scope.col7 - $scope.col13) * $scope.col9 / 100; $scope.ValuaCelK = $scope.ValuaCelK > 0 ? $scope.ValuaCelK : 0; $scope.modeljexcel.Jexcel.jexcel("setValueByName", "VatamountOc", row, $scope.conjexcel($scope.ValuaCelK));
            // tính tiền thuế quy đổi
            $scope.ValuaCelL = $scope.ValuaCelK * ExchangeRate; $scope.ValuaCelL = $scope.ValuaCelL > 0 ? $scope.ValuaCelL : 0; $scope.modeljexcel.Jexcel.jexcel("setValueByName", "Vatamount", row, $scope.conjexcel($scope.ValuaCelL));
            // tính tổng tiền thanh toán
            var ValuaCelP12 = $scope.col7 + $scope.ValuaCelK - ValuaCelN12; ValuaCelP12 = ValuaCelP12 > 0 ? ValuaCelP12 : 0; $scope.modeljexcel.Jexcel.jexcel("setValueByName", "TongtienTT", row, $scope.conjexcel(ValuaCelP12));
            // tính tổng tiền thanh toán quy đổi
            $scope.ValuaCelQ16 = ValuaCelP12 * ExchangeRate; $scope.ValuaCelQ16 = $scope.ValuaCelQ16 > 0 ? $scope.ValuaCelQ16 : 0; $scope.modeljexcel.Jexcel.jexcel("setValueByName", "TongtienTTQD", row, $scope.conjexcel($scope.ValuaCelQ16));
        }
        $rootScope.totalcol4 = 0;
        $rootScope.totalcol7 = 0;
        $rootScope.totalcol10 = 0;
        $rootScope.totalcol13 = 0;
        $rootScope.totalcol15 = 0;
        $scope.ListData = $scope.modeljexcel.Jexcel.jexcel('getData');
        angular.forEach($scope.ListData, function (val, key) {
            if (val.Quantity !== null && val.Quantity !== "" && val.Quantity !== undefined) {
                $rootScope.totalcol4 += parseFloat(val.Quantity);
            }
            if (val.AmountOc !== null && val.AmountOc !== "" && val.AmountOc !== undefined) {
                $rootScope.totalcol7 += parseFloat(val.AmountOc);
            }
            if (val.VatamountOc !== null && val.VatamountOc !== "" && val.VatamountOc !== undefined) {
                $rootScope.totalcol10 += parseFloat(val.VatamountOc);
            }
            if (val.DiscountOc !== null && val.DiscountOc !== "" && val.DiscountOc !== undefined) {
                $rootScope.totalcol13 += parseFloat(val.DiscountOc);
            }
            if (val.TongtienTT !== null && val.TongtienTT !== "" && val.TongtienTT !== undefined) {
                $rootScope.totalcol15 += parseFloat(val.TongtienTT);
            }
        });
        $('#colfoot-4').html($rootScope.addPeriod($rootScope.totalcol4));
        $('#totalcol7').html($rootScope.addPeriod($rootScope.totalcol7 * ExchangeRate));
        $('#colfoot-7').html($rootScope.addPeriod($rootScope.totalcol7));
        $('#colfoot-10').html($rootScope.addPeriod($rootScope.totalcol10));
        $('#totalcol10').html($rootScope.addPeriod($rootScope.totalcol10 * ExchangeRate));
        $('#colfoot-13').html($rootScope.addPeriod($rootScope.totalcol13));
        $('#totalcol13').html($rootScope.addPeriod($rootScope.totalcol13 * ExchangeRate));
        $('#colfoot-15').html($rootScope.addPeriod($rootScope.totalcol15));
        $('#totalcol15').html($rootScope.addPeriod($rootScope.totalcol15 * ExchangeRate));

    }
    $scope.deletees = function (element, item) {
        $scope.ListData = $('#myht').jexcel('getData');
        if ($scope.ListData.length > 1) {
            $('#myht').jexcel('deleteRow', item);
            $rootScope.totalcol4 = 0;
            $rootScope.totalcol7 = 0;
            $rootScope.totalcol10 = 0;
            $rootScope.totalcol13 = 0;
            $rootScope.totalcol15 = 0;

            angular.forEach($scope.ListData, function (val, key) {
                if (val.Quantity !== null && val.Quantity !== "" && val.Quantity !== undefined) {
                    $rootScope.totalcol4 += parseFloat(val.Quantity);
                }
                if (val.AmountOc !== null && val.AmountOc !== "" && val.AmountOc !== undefined) {
                    $rootScope.totalcol7 += parseFloat(val.AmountOc);
                }
                if (val.VatamountOc !== null && val.VatamountOc !== "" && val.VatamountOc !== undefined) {
                    $rootScope.totalcol10 += parseFloat(val.VatamountOc);
                }
                if (val.DiscountOc !== null && val.DiscountOc !== "" && val.DiscountOc !== undefined) {
                    $rootScope.totalcol13 += parseFloat(val.DiscountOc);
                }
                if (val.TongtienTT !== null && val.TongtienTT !== "" && val.TongtienTT !== undefined) {
                    $rootScope.totalcol15 += parseFloat(val.TongtienTT);
                }
            });
            $('#colfoot-4').html($rootScope.addPeriod($rootScope.totalcol4));
            $('#totalcol7').html($rootScope.addPeriod($rootScope.totalcol7 * parseFloat($scope.model.ExchangeRate)));
            $('#colfoot-7').html($rootScope.addPeriod($rootScope.totalcol7));
            $('#colfoot-10').html($rootScope.addPeriod($rootScope.totalcol10));
            $('#totalcol10').html($rootScope.addPeriod($rootScope.totalcol10 * parseFloat($scope.model.ExchangeRate)));
            $('#colfoot-13').html($rootScope.addPeriod($rootScope.totalcol13));
            $('#totalcol13').html($rootScope.addPeriod($rootScope.totalcol13 * parseFloat($scope.model.ExchangeRate)));
            $('#colfoot-15').html($rootScope.addPeriod($rootScope.totalcol15));
            $('#totalcol15').html($rootScope.addPeriod($rootScope.totalcol15 * parseFloat($scope.model.ExchangeRate)));
            App.notifySuccess('xóa thành công');

        }
        else {
            App.notifyDanger('Không xóa được dòng cuối cùng này !');
            return;
        }
    };
    $scope.deleterowhotkey = function (element, rowselected) {
        $scope.ListData = $('#myht').jexcel('getData');
        if ($scope.ListData.length > 1) {
            $('#myht').jexcel('deleteSelection', true, rowselected);
            $rootScope.totalcol4 = 0;
            $rootScope.totalcol7 = 0;
            $rootScope.totalcol10 = 0;
            $rootScope.totalcol13 = 0;
            $rootScope.totalcol15 = 0;

            angular.forEach($scope.ListData, function (val, key) {
                if (val.Quantity !== null && val.Quantity !== "" && val.Quantity !== undefined) {
                    $rootScope.totalcol4 += parseFloat(val.Quantity);
                }
                if (val.AmountOc !== null && val.AmountOc !== "" && val.AmountOc !== undefined) {
                    $rootScope.totalcol7 += parseFloat(val.AmountOc);
                }
                if (val.VatamountOc !== null && val.VatamountOc !== "" && val.VatamountOc !== undefined) {
                    $rootScope.totalcol10 += parseFloat(val.VatamountOc);
                }
                if (val.DiscountOc !== null && val.DiscountOc !== "" && val.DiscountOc !== undefined) {
                    $rootScope.totalcol13 += parseFloat(val.DiscountOc);
                }
                if (val.TongtienTT !== null && val.TongtienTT !== "" && val.TongtienTT !== undefined) {
                    $rootScope.totalcol15 += parseFloat(val.TongtienTT);
                }
            });
            $('#colfoot-4').html($rootScope.addPeriod($rootScope.totalcol4));
            $('#totalcol7').html($rootScope.addPeriod($rootScope.totalcol7 * parseFloat($scope.model.ExchangeRate)));
            $('#colfoot-7').html($rootScope.addPeriod($rootScope.totalcol7));
            $('#colfoot-10').html($rootScope.addPeriod($rootScope.totalcol10));
            $('#totalcol10').html($rootScope.addPeriod($rootScope.totalcol10 * parseFloat($scope.model.ExchangeRate)));
            $('#colfoot-13').html($rootScope.addPeriod($rootScope.totalcol13));
            $('#totalcol13').html($rootScope.addPeriod($rootScope.totalcol13 * parseFloat($scope.model.ExchangeRate)));
            $('#colfoot-15').html($rootScope.addPeriod($rootScope.totalcol15));
            $('#totalcol15').html($rootScope.addPeriod($rootScope.totalcol15 * parseFloat($scope.model.ExchangeRate)));
            App.notifySuccess('xóa thành công');

        }
        else {
            App.notifyDanger('Không xóa được dòng cuối cùng này !');
            return;
        }
    };
    $scope.Attchment123 = function (obj, row) {
        var datarow = $('#myhtread').jexcel('getData')[parseInt(obj.data.row)];
        var SortOrder = $('#myhtread').jexcel('getData')[parseInt(obj.data.row)].SortOrder;
        if (datarow.ItemIdbyProvider === "" && datarow.ProductId === "") {
            App.notifyDanger("Dòng thứ " + (parseInt(obj.data.row) + 1) + ": Yêu cầu chọn mã sản phẩm.");
            return;
        }
        var data = $scope.uploader.queue.filter(x => x.SortOrder === SortOrder);
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/addAttchment.html',
            controller: 'attchment',
            backdrop: 'static',
            size: '80',
            resolve: {
                para: function () {
                    return datarow;
                },
                data: function () {
                    return data;
                },
                para1: function () {
                    return $scope.openAttach;
                }
            }
        });
        modalInstance.result.then(function (data) {
            $scope.ListDeleteFile = data.Delete;
            for (var i = 0; i < $scope.uploader.queue.length; i++) {
                if ($scope.uploader.queue[i].SortOrder === SortOrder) {
                    $scope.uploader.queue.splice(i, 1);
                    i--;
                }
            }
            angular.forEach(data.File, function (value, key) {
                value.SortOrder = SortOrder;
                $scope.uploader.queue.push(value);
            });
        }, function () {
        });
    }
    $scope.jxcel = {
        deleterowconfirm: $scope.deletees,
        deleterowhotkey: $scope.deleterowhotkey,
        dataObject: true,
        reloadData: {},
        data: [['', '', '']],
        colHeaders: ['id', 'Mã hàng', 'Tên hàng'],
        colTitles: ['', 'Mã hàng', 'Tên hàng'],
        colFooters: ['', 'Tổng', ''],
        columnShow: [false, true, true],
        columns: [
            {
                name: 'Id',
                type: 'text'
            },
            
            { name: 'ProductName', type: 'text' }, // tên hàng
            //{
            //    name: 'UnitId', type: 'autocomplete', // đơn vị tính
            //    url: '/Sale/Quotations/GetUnitsAllJExcel'
            //},
            {
                name: 'Quantity', // số lượng
                type: 'numeric', formatNumber: 'VN-NUMERIC'
            },
            {
                name: 'QuantityDG', // số lượng đã giao
                type: 'numeric', formatNumber: 'VN-NUMERIC', readOnly: true
            },
          
        ],
        initDataRows: ['', '', ''],
        colWidths: [100, 100, 250],
        allrowevent: true,
        tableOverflow: true,
        csvHeaders: true,
        tableHeight: '200px',
       
        changeCell: $scope.rowchange,
        contextMenuOption: [
            {
                title: 'Đính kèm',
                event: {
                    name: 'click',
                    callback: $scope.Attchment123
                },
                element: "<i style='font-size: 17px;' class='mdi mdi-attachment'></i>"
            }
        ]
    };

    //Trạng thái
    $scope.StatusConfig = {
        placeholder: 'Chọn trạng thái',
        okCancelInMulti: true,
        selected: ['true']
    };
    $scope.ChangeIsActive = function (data) {
        $scope.model.IsActive = data !== "null" ? data : "";
    }

    $scope.submit = function () {
        if ($scope.addform.validate()) {
            $scope.model.IsActive = $scope.model.IsActive == "1" || $scope.model.IsActive == true ? true : false;
            dataservice.insert($scope.model, function (rs) {
                if (rs.Error) {
                    App.notifyDanger(rs.Title);
                    return;
                } else {
                    App.notifyInfo(rs.Title);
                    $uibModalInstance.close();
                }
            });

        }
    }
});
app.controller('edit', function ($scope, $rootScope, $compile, $uibModal, $confirm, $uibModalInstance, FileUploader, dataservice, para) {
    $scope.Title = "Sửa hợp đồng";
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    }
    $scope.StatusConfig = {
        placeholder: 'Chọn trạng thái',
        okCancelInMulti: true,
        selected: ['true']
    };
    $scope.ChangeIsActive = function (data) {
        $scope.model.IsActive = data !== "null" ? data : "";
    }

    $scope.initData = function () {
        dataservice.getItem({ IdI: [para] }, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
                return;
            } else {
                $scope.model = rs;
                $scope.model.ContractDate !== null ? $scope.model.ContractDate = "/Date(" + new Date($scope.model.ContractDate).getTime() + ")/" : undefined;
                if ($scope.model.IsActive === true) {
                    $scope.model.IsActive = "1";
                } else { $scope.model.IsActive = "0"; }
            }
        });
    }
    $scope.initData();
    $scope.submit = function () {
        if ($scope.addform.validate()) {
            $scope.model.IsActive = $scope.model.IsActive == "1" || $scope.model.IsActive == true ? true : false;
            dataservice.Update($scope.model, function (rs) {
                if (rs.Error) {
                    App.notifyDanger(rs.Title);
                    return;
                } else {
                    App.notifyInfo(rs.Title);
                    $uibModalInstance.close();
                }
            });
        }
    }

});
app.controller('open', function ($scope, $rootScope, $compile, $uibModal, $confirm, $uibModalInstance, dataservice, para) {
    $scope.Title = "Xem hợp đồng";
    $scope.model = {};
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    }

    $scope.loadData = function () {
        dataservice.getItem({ IdI: [para] }, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {
                $scope.model = rs;
                $scope.model.ContractDate !== null ? $scope.model.ContractDate = "/Date(" + new Date($scope.model.ContractDate).getTime() + ")/" : undefined;
                if ($scope.model.IsActive === true) {
                    $scope.model.IsActive = "Sử dụng";
                } else { $scope.model.IsActive = "Không sử dụng"; }

            }
        });
    }
    $scope.loadData();
}); 
