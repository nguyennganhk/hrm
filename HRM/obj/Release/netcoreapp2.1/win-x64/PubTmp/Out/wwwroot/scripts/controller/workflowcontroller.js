﻿var app = angular.module('e-app', ["ui.bootstrap", "ngValidate", "ui.select", "ng-aimodu"]);
app.factory('dataservice', function ($http) {
    var headers = {
        "Content-Type": "SAVouchers/json;odata=verbose",
        "Accept": "SAVouchers/json;odata=verbose"
    };
    return {
        insert: function (data, callback) {
            $http.post('/WorkFlow/insert', data).success(callback);
        },
        getTable: function (callback) {
            $http.get('/WorkFlow/GetTable').success(callback);
        }
    };
});
app.controller('workflowcontroller', ['$scope', '$parse', '$interpolate', '$compile', '$rootScope',
    function ($scope, $parse, $interpolate, $compile, $rootScope, dataservice) {
        $.validator.addMethod("alphanumerics", function (value, element) {
            var data = /[(!@#$%^&*\)]+$/i.test(value);
            return this.optional(element) || data === false ? true : false;
        }, "Letters, numbers, and underscores only please");
        $.validator.addMethod("Number", function (value, element) {
            return this.optional(element) || parseInt(value.split('.').join('')) >= 0 ? true : false;
        }, "Letters, numbers, and underscores only please");
        $.validator.addMethod("valueNotEquals", function (value, element, arg) {
            // I use element.value instead value here, value parameter was always null
            return arg !== element.value;
        }, "Value must not equal arg.");
        $scope.validationOptions = {
            //ignore: [],
            rules: {
                WorkflowId: {
                    required: true
                },
                WorkflowName: {
                    required: true
                },
                TableGuid: {
                    valueNotEquals: "null"
                }
            },
            messages: {
                WorkflowId: {
                    required: 'Yêu cầu nhập mã quy trình.'
                },
                WorkflowName: {
                    required: 'Yêu cầu nhập tên quy trình.'
                },
                TableGuid: {
                    valueNotEquals: 'Yêu cầu chọn quy trình.'
                }
            }
        };
        $scope.model = { Conditions: [], Activities: [] };
        $scope.model1 = {};
        $scope.model2 = {};
        $scope.initData = function () {
            //Bảng
            $scope.TableConfig = {
                placeholder: '',
                okCancelInMulti: false,
                selected: []
            };
            $scope.ListTable = [{ value: null, text: 'chọn' }];
            $.ajax({
                type: 'get',
                url: '/WorkFlow/GetTable',
                success: function (rs) {
                    if (rs.Error) {
                        toastr["error"](rs.Title);
                    } else {
                        angular.forEach(rs, function (val, key) {
                            $scope.ListTable.push(val);
                        });
                        $scope.$apply();
                    }
                }
            });
            $scope.ChangeTable = function (data) {
                $scope.model.TableGuid = data;
            };
        };
        $scope.initData();

        angular.element(document).ready(function () {
            var url = window.location.href;
            var check = url.indexOf('?');
            if (check !== -1) {
                var url2 = window.location.href;
                var url1 = url2.split('=');
                $.ajax({
                    type: 'post',
                    url: '/Home/LoadWorkFlow',
                    data: { id: url1[1] },
                    success: function (rss) {
                        var rs = JSON.parse(rss);
                        editor.parse(JSON.parse(rs.WorkFlowData));
                        $scope.model = rs;
                        $scope.$apply();
                    }
                });
            }

            $("#myModalFormPoperty").modal({ backdrop: 'static' }, 'show');
            var editor = new dhx.DiagramEditor(document.body, {
                select: true
            });
            var a = '[{"id":"u1555905737914","type":"start","text":"Bắt đầu","x":300,"y":-10,"fill":"#EEF1F6","stroke":"#B8C6D6","extraLinesStroke":"#B8C6D6","width":140,"height":90,"strokeWidth":1,"fontColor":"#4C4C4C","fontSize":14,"textAlign":"center","lineHeight":14,"fontStyle":"normal","textVerticalAlign":"center","angle":0,"strokeType":"line"},{"id":"u1555905737915","type":"rectangle","text":"Leader","x":300,"y":140,"fill":"#EEF1F6","stroke":"#B8C6D6","extraLinesStroke":"#B8C6D6","width":140,"height":90,"strokeWidth":1,"fontColor":"#4C4C4C","fontSize":14,"textAlign":"center","lineHeight":14,"fontStyle":"normal","textVerticalAlign":"center","angle":0,"strokeType":"line"},{"id":"u1555905737916","type":"decision","text":"Duyệt","x":300,"y":310,"fill":"#EEF1F6","stroke":"#B8C6D6","extraLinesStroke":"#B8C6D6","width":140,"height":90,"strokeWidth":1,"fontColor":"#4C4C4C","fontSize":14,"textAlign":"center","lineHeight":14,"fontStyle":"normal","textVerticalAlign":"center","angle":0,"strokeType":"line"},{"id":"u1555905737917","type":"rectangle","text":"HCNS","x":300,"y":480,"fill":"#EEF1F6","stroke":"#B8C6D6","extraLinesStroke":"#B8C6D6","width":140,"height":90,"strokeWidth":1,"fontColor":"#4C4C4C","fontSize":14,"textAlign":"center","lineHeight":14,"fontStyle":"normal","textVerticalAlign":"center","angle":0,"strokeType":"line"},{"id":"u1555905737922","type":"decision","text":"Duyệt","x":300,"y":650,"fill":"#EEF1F6","stroke":"#B8C6D6","extraLinesStroke":"#B8C6D6","width":140,"height":90,"strokeWidth":1,"fontColor":"#4C4C4C","fontSize":14,"textAlign":"center","lineHeight":14,"fontStyle":"normal","textVerticalAlign":"center","angle":0,"strokeType":"line"},{"id":"u1555905737927","type":"rectangle","text":"Giám đốc","x":300,"y":820,"fill":"#EEF1F6","stroke":"#B8C6D6","extraLinesStroke":"#B8C6D6","width":140,"height":90,"strokeWidth":1,"fontColor":"#4C4C4C","fontSize":14,"textAlign":"center","lineHeight":14,"fontStyle":"normal","textVerticalAlign":"center","angle":0,"strokeType":"line"},{"id":"u1555905737932","type":"decision","text":"Duyệt","x":300,"y":990,"fill":"#EEF1F6","stroke":"#B8C6D6","extraLinesStroke":"#B8C6D6","width":140,"height":90,"strokeWidth":1,"fontColor":"#4C4C4C","fontSize":14,"textAlign":"center","lineHeight":14,"fontStyle":"normal","textVerticalAlign":"center","angle":0,"strokeType":"line"},{"id":"u1555905737953","type":"end","text":"Kết thúc","x":300,"y":1170,"fill":"#EEF1F6","stroke":"#B8C6D6","extraLinesStroke":"#B8C6D6","width":140,"height":90,"strokeWidth":1,"fontColor":"#4C4C4C","fontSize":14,"textAlign":"center","lineHeight":14,"fontStyle":"normal","textVerticalAlign":"center","angle":0,"strokeType":"line"},{"id":"u1555905737966","type":"line","points":[{"x":370,"y":1080},{"x":370,"y":1170}],"stroke":"#2196F3","connectType":"elbow","strokeWidth":2,"cornersRadius":0,"width":0,"height":90,"x":370,"y":1080,"from":"u1555905737932","to":"u1555905737953","fromSide":"bottom","toSide":"top","strokeType":"line","backArrow":"","forwardArrow":"filled","callBacks":"next"},{"id":"u1555905738296","type":"line","points":[{"x":440,"y":1035},{"x":450,"y":1035},{"x":450,"y":525},{"x":440,"y":525}],"stroke":"#2196F3","connectType":"elbow","strokeWidth":2,"cornersRadius":0,"width":0,"height":510,"x":440,"y":1035,"from":"u1555905737932","to":"u1555905737917","fromSide":"right","toSide":"right","strokeType":"line","backArrow":"","forwardArrow":"filled","callBacks":"previous"},{"id":"u1555905740041","type":"line","points":[{"x":370,"y":910},{"x":370,"y":990}],"stroke":"#2196F3","connectType":"elbow","strokeWidth":2,"cornersRadius":0,"width":0,"height":80,"x":370,"y":910,"from":"u1555905737927","to":"u1555905737932","fromSide":"bottom","toSide":"top","strokeType":"line","backArrow":"","forwardArrow":"filled","callBacks":"next"},{"id":"u1555905740531","type":"line","points":[{"x":370,"y":740},{"x":370,"y":820}],"stroke":"#2196F3","connectType":"elbow","strokeWidth":2,"cornersRadius":0,"width":0,"height":80,"x":370,"y":740,"from":"u1555905737922","to":"u1555905737927","fromSide":"bottom","toSide":"top","strokeType":"line","backArrow":"","forwardArrow":"filled","callBacks":"next"},{"id":"u1555905740969","type":"line","points":[{"x":370,"y":570},{"x":370,"y":650}],"stroke":"#2196F3","connectType":"elbow","strokeWidth":2,"cornersRadius":0,"width":0,"height":80,"x":370,"y":570,"from":"u1555905737917","to":"u1555905737922","fromSide":"bottom","toSide":"top","strokeType":"line","backArrow":"","forwardArrow":"filled","callBacks":"next"},{"id":"u1555905743998","type":"line","points":[{"x":300,"y":695},{"x":290,"y":695},{"x":290,"y":185},{"x":300,"y":185}],"stroke":"#2196F3","connectType":"elbow","strokeWidth":2,"cornersRadius":0,"width":0,"height":510,"x":300,"y":695,"from":"u1555905737922","to":"u1555905737915","fromSide":"left","toSide":"left","strokeType":"line","backArrow":"","forwardArrow":"filled","callBacks":"previous"},{"id":"u1555905745209","type":"line","points":[{"x":370,"y":400},{"x":370,"y":480}],"stroke":"#2196F3","connectType":"elbow","strokeWidth":2,"cornersRadius":0,"width":0,"height":80,"x":370,"y":400,"from":"u1555905737916","to":"u1555905737917","fromSide":"bottom","toSide":"top","strokeType":"line","backArrow":"","forwardArrow":"filled","callBacks":"next"},{"id":"u1555905745602","type":"line","points":[{"x":370,"y":230},{"x":370,"y":310}],"stroke":"#2196F3","connectType":"elbow","strokeWidth":2,"cornersRadius":0,"width":0,"height":80,"x":370,"y":230,"from":"u1555905737915","to":"u1555905737916","fromSide":"bottom","toSide":"top","strokeType":"line","backArrow":"","forwardArrow":"filled","callBacks":"next"},{"id":"u1555905746003","type":"line","points":[{"x":370,"y":80},{"x":370,"y":140}],"stroke":"#2196F3","connectType":"elbow","strokeWidth":2,"cornersRadius":0,"width":0,"height":60,"x":370,"y":80,"from":"u1555905737914","to":"u1555905737915","fromSide":"bottom","toSide":"top","strokeType":"line","backArrow":"","forwardArrow":"filled","callBacks":"next"}]';
            editor.parse(JSON.parse(a));
            editor.events.on("ApplyButton", function () {
                $scope.WorkFlowData = JSON.stringify(editor.serialize("Json"));
                var fd = new FormData();
                fd.append('data', JSON.stringify($scope.model));
                fd.append('flowjson', $scope.WorkFlowData);
                if (check === -1) {
                    if ($scope.model.WorkFlowCode !== null && $scope.model.WorkFlowCode !== ""
                        && $scope.model.WorkFlowName !== null && $scope.model.WorkFlowName !== "") {
                        $.ajax({
                            type: 'post',
                            url: '/WorkFlow/InsertWorkFlow',
                            processData: false,
                            contentType: false,
                            data: fd,
                            success: function (rs) {
                                if (rs.Error) {
                                    toastr["error"]("Thêm mới không thành công!");
                                } else {
                                    toastr["success"]("Thêm mới thành công!");
                                    //window.location.href = '/Home/WorkFlow?id=' + rs.id;
                                    $scope.$apply();
                                }
                            }
                        });
                        console.log($scope.model);
                    }
                    else if ($scope.model.WorkFlowCode !== null || $scope.model.WorkFlowCode !== "") {
                        toastr["error"]("Mã quy trình không được bỏ trống!");
                    }
                    else if ($scope.model.WorkFlowName !== null || $scope.model.WorkFlowName !== "") {
                        toastr["error"]("Tên quy trình không được bỏ trống!");
                    }
                }
                else {
                    if ($scope.model.WorkFlowCode !== null && $scope.model.WorkFlowCode !== ""
                        && $scope.model.WorkFlowName !== null && $scope.model.WorkFlowName !== "") {
                        $.ajax({
                            type: 'post',
                            url: '/Home/UpdateWorkFlow',
                            processData: false,
                            contentType: false,
                            data: fd,
                            success: function (rs) {
                                if (rs.error) {
                                    toastr["error"]("Sửa không thành công!");
                                } else {
                                    toastr["success"]("Sửa thành công!");
                                    $scope.$apply();
                                }
                            }
                        });
                    }
                    else if ($scope.model.WorkFlowCode !== null || $scope.model.WorkFlowCode !== "") {
                        toastr["error"]("Mã quy trình không được bỏ trống!");
                    }
                    else if ($scope.model.WorkFlowName !== null || $scope.model.WorkFlowName !== "") {
                        toastr["error"]("Tên quy trình không được bỏ trống!");
                    }
                }
            });
            editor.events.on("ShapeResize", function (id) {
                console.log('An item double-clicked');
            });

            setTimeout(function () {
                $(document).trigger('afterready');
            }, 1);
            $scope.Settings = [];

            $(document).on("click", ".dhx_diagram_connector", function (e) {
                return false;
            });

            $(document).on('click', '#vn_add', function () {
                $("#myModalFormPoperty").modal({ backdrop: 'static' }, 'show');
            });
            $scope.count = 1;

            $(document).on('dblclick', '.dhx_diagram_flow_item', function () {
                var countConditonData = 1;
                var countConditionTrue = 1;
                var countConditionFalse = 1;
                $scope.Step = [];
                $('#addCondition').children('.row:not(:first)').remove();
                $scope.model2 = {};
                //Cột
                $scope.ColumneConfig = {
                    placeholder: '',
                    okCancelInMulti: false,
                    selected: []
                };
                $scope.ListColumn = [{ value: null, text: 'chọn' }];
                $.ajax({
                    type: 'post',
                    async: false,
                    url: '/WorkFlow/GetColumn',
                    data: { Id: $scope.model.TableGuid },
                    success: function (rs) {
                        if (rs.Error) {
                            toastr["error"](rs.Title);
                        } else {
                            angular.forEach(rs, function (val, key) {
                                $scope.ListColumn.push(val);
                            });
                            $scope.$apply();
                        }
                    }
                });
                $scope.ChangeColumn = function (data) {
                    $scope.model2.ColumnGuid = data;
                    $scope.ConditionDataJson = $scope.ListColumn.find(x => x.value === $scope.model2.ColumnGuid);
                    var a = JSON.parse($scope.ConditionDataJson.ConditionOfColumn);
                    $scope.ListConditionValue = [{ value: null, text: 'chọn' }];
                    angular.forEach(a, function (val, key) {
                        $scope.ListConditionValue.push(val);
                    });
                    $scope.model2.Condition = 'null';
                };
                //Điều kiện toán tử
                $scope.ConditionConfig = {
                    placeholder: '',
                    okCancelInMulti: false,
                    selected: []
                };

                $scope.ListCondition = [
                    { value: null, text: 'chọn' },
                    { value: 1, text: '> (Lớn hơn)' },
                    { value: 2, text: '< (Nhỏ hơn)' },
                    { value: 3, text: '>= (Lớn hơn hoặc bằng)' },
                    { value: 4, text: '<= (Nhỏ hơn hoặc bằng)' },
                    { value: 5, text: '= (Bằng)' },
                    { value: 6, text: '<> (Khác)' },
                    { value: 7, text: 'Beteen (Trong khoảng)' }];

                $scope.ChangeCondition = function (data) {
                    $scope.model2.Condition = data;
                };
                //giá trị điều kiện
                $scope.ConditionValueConfig = {
                    placeholder: '',
                    okCancelInMulti: false,
                    selected: []
                };
                $scope.ListConditionValue = [];
                $scope.ChangeConditionValue = function (data) {
                    $scope.model2.ConditionValue = data;
                };
                //Điều kiện
                $scope.AndOrConfig = {
                    placeholder: '',
                    okCancelInMulti: false,
                    selected: []
                };
                $scope.ListAndOr = [
                    { value: null, text: 'chọn' },
                    { value: 'AND', text: 'AND' },
                    { value: 'OR', text: 'OR' }
                ];
                //add conditon
                $scope.AddCondition = function () {
                    angular.element(document.getElementById('addCondition')).append($compile('<div class="row"><div class="col-md-1"><select ng-model="model2.AndOr' + countConditonData + '" ng-smselect se-data="ListAndOr" se-change="ChangeAndOr' + countConditonData + '(data)" se-init="AndOrConfig"></select></div>' +
                        '<div class="col-md-4"> <select ng-model="model2.ColumnGuid' + countConditonData + '" ng-smselect se-data="ListColumn" se-change="ChangeColumn' + countConditonData + '(data)" se-init="ColumneConfig"></select></div > ' +
                        '<div class="col-md-3"><select ng-model="model2.Condition' + countConditonData + '" ng-smselect se-data="ListCondition" se-change="ChangeCondition' + countConditonData + '(data)" se-init="ConditionConfig"></select></div>' +
                        '<div class="col-md-4"><select ng-model="model2.ConditionValue' + countConditonData + '" ng-smselect se-data="ListConditionValue" se-change="ChangeConditionValue' + countConditonData + '(data)" se-init="ConditionValueConfig"></select> </div></div >')($scope));
                    countConditonData++;
                };

                var fieldp1 = 'Condition' + countConditonData;
                var field2 = 'ChangeColumn' + countConditonData;
                $scope[field2] = function (data) {
                    var fieldp2 = 'ColumnGuid' + countConditonData;
                    $scope.model2[fieldp2] = data;
                    $scope.ConditionDataJson = $scope.ListColumn.find(x => x.value === $scope.model2[fieldp2]);
                    var a = JSON.parse($scope.ConditionDataJson.ConditionOfColumn);
                    $scope.ListConditionValue = a;
                    $scope.model2[fieldp1] = 'null';
                };

                var field1 = 'ChangeCondition' + countConditonData;
                $scope[field1] = function (data) {
                    $scope.model2[fieldp1] = data;
                };

                var field3 = 'ChangeConditionValue' + countConditonData;
                $scope[field3] = function (data) {
                    var fieldp3 = 'ConditionValue' + countConditonData;
                    $scope.model2[fieldp3] = data;
                };
                var field4 = 'ChangeAndOr' + countConditonData;
                $scope[field4] = function (data) {
                    var fieldp4 = 'ChangeAndOr' + countConditonData;
                    $scope.model2[fieldp4] = data;
                };
                //End add condition

                var mail = "model2.Activity == 'mail'";
                var pd = "model2.Activity == 'pd'";
                var pdF = "model2.ActivityF == 'pd'";
                var mailF = "model2.ActivityF == 'mail'";
                //Add Activity True
                $scope.AddActivityTrue = function () {
                    angular.element(document.getElementById('condionTrue')).after($compile('<div class="row"><div class="col-md-4"><select ng-model="model2.Activity' + countConditionTrue + '" ng-smselect se-data="ListActivity" se-change="ChangeActivity' + countConditionTrue + '(data)" se-init="ActivityConfig"></select>' +
                        '<div ng-show="' + mail + '"><select ng-model="model2.Approver' + countConditionTrue + '" ng-smselect se-data="ListApprover" multiple se-change="ChangeApprover' + countConditionTrue + '(data)" se-init="ApproverConfig"></select> </div></div >' +
                        '<div class="col-md-4" ng-show="' + pd + '"><select ng-model="model2.ApproveType' + countConditionTrue + '" ng-smselect se-data="ListApproveType" se-change="ChangeApproveType' + countConditionTrue + '(data)" se-init="ApproveTypeConfig"></select></div>' +
                        '<div class="col-md-4" ng-show="' + pd + '" > <select ng-model="model2.Approver' + countConditionTrue + '" ng-smselect se-data="ListApprover" multiple se-change="ChangeApprover' + countConditionTrue + '(data)" se-init="ApproverConfig" ng-disabled="ChoseApproveType"></select></div >' +
                        '<div class="col-md-8" ng-show="' + mail + '"><textarea class="form-control" rows="3" ng-model="model2.ContentEmail' + countConditionTrue + '" placeholder="Nhập nội dung mẫu"></textarea></div></div>')($scope));
                    countConditionTrue++;
                };

                var fieldT = 'ChangeApprover' + countConditionTrue;
                $scope[fieldT] = function (data) {
                    var fielTdp2 = 'Approver' + countConditionTrue;
                    $scope.model2[fielTdp2] = data;
                };
                var fieldTT = 'ChangeApproveType' + countConditionTrue;
                $scope[fieldTT] = function (data) {
                    var fieldTTdp2 = 'ApproveType' + countConditionTrue;
                    $scope.model2[fieldTTdp2] = data;
                };
                var fieldAT = 'ChangeActivity' + countConditionTrue;
                $scope[fieldAT] = function (data) {
                    var fieldATdp2 = 'Activity' + countConditionTrue;
                    $scope.model2[fieldATdp2] = data;
                };

                //End add Activity True
                //Add Activity false
                $scope.AddActivityFalse = function () {
                    angular.element(document.getElementById('condionFalse')).after($compile('<div class="row"><div class="col-md-4"><select ng-model="model2.ActivityF' + countConditionFalse + '" ng-smselect se-data="ListActivity" se-change="ChangeActivityF' + countConditionTrue + '(data)" se-init="ActivityConfig"></select>' +
                        '<div ng-show="' + mailF + '"><select ng-model="model2.ApproverF' + countConditionFalse + '" ng-smselect se-data="ListApprover" multiple se-change="ChangeApproverF' + countConditionFalse + '(data)" se-init="ApproverConfig"></select></div></div>' +
                        '<div class="col-md-4" ng-show="' + pdF + '"> <select ng-model="model2.ApproveTypeF' + countConditionFalse + '" ng-smselect se-data="ListApproveType" se-change="ChangeApproveTypeF' + countConditionFalse + '(data)" se-init="ApproveTypeConfig"></select></div>' +
                        '<div class="col-md-4" ng-show="' + pdF + '"><select ng-model="model2.ApproverF' + countConditionFalse + '" ng-smselect se-data="ListApprover" multiple se-change="ChangeApproverF' + countConditionFalse + '(data)" se-init="ApproverConfig" ng-disabled="ChoseApproveType"></select></div>' +
                        '<div class="col-md-8" ng-show="' + mailF + '"> <textarea class="form-control" rows="3" ng-model="model2.ContentEmailF' + countConditionFalse + '" placeholder="Nhập nội dung mẫu"></textarea></div></div>')($scope));
                    countConditionFalse++;
                };
                var fieldF = 'ChangeApprover' + countConditionFalse;
                $scope[fieldF] = function (data) {
                    var fielFdp2 = 'Approver' + countConditionFalse;
                    $scope.model2[fielFdp2] = data;
                };
                var fieldFT = 'ChangeApproveType' + countConditionFalse;
                $scope[fieldFT] = function (data) {
                    var fieldFTdp2 = 'ApproveType' + countConditionFalse;
                    $scope.model2[fieldFTdp2] = data;
                };
                var fieldAF = 'ChangeActivityF' + countConditionTrue;
                $scope[fieldAT] = function (data) {
                    var fieldAFdp2 = 'ActivityF' + countConditionTrue;
                    $scope.model2[fieldAFdp2] = data;
                };
                //End Activity

                //Hành động
                $scope.model2.Activity = 'pd';
                $scope.model2.ActivityF = 'mail';
                $scope.ActivityConfig = {
                    placeholder: '',
                    okCancelInMulti: false,
                    selected: []
                };
                $scope.ListActivity = [
                    { value: null, text: 'chọn' },
                    { value: 'pd', text: 'Phê duyệt' },
                    { value: 'mail', text: 'Gửi Email' }
                ];

                $scope.ChangeActivity = function (data) {
                    $scope.model2.Activity = data;
                };
                $scope.ChangeActivityF = function (data) {
                    $scope.model2.ActivityF = data;
                };
                //Loại duyệt
                $scope.model2.ApproveType = 'null';
                if ($scope.model2.ApproveType === 'null') {
                    $scope.ChoseApproveType = true;
                }
                $scope.ApproveTypeConfig = {
                    placeholder: '',
                    okCancelInMulti: false,
                    selected: ['null']
                };
                $scope.ListApproveType = [
                    { value: null, text: 'chọn' },
                    { value: '1', text: 'Chỉ định người duyệt' },
                    { value: '2', text: 'Người dùng tự nhập' }
                ];

                $scope.ChangeApproveType = function (data) {
                    $scope.model2.ApproveType = data;
                    if (data === '2' || data === 2 || data === 'null' || data === null) {
                        $scope.model2.Approver = [];
                        $scope.ChoseApproveType = true;
                        $scope.$apply();
                    }
                    if (data === '1' || data === 1) {
                        $scope.model2.Approver = [];
                        $scope.ChoseApproveType = false;
                        $scope.$apply();
                    }
                };
                //Người duyệt
                $scope.ApproverConfig = {
                    placeholder: '',
                    okCancelInMulti: true,
                    selectAll: true,
                    selected: []
                };
                $scope.ListApprover = [
                    { value: 'thuynm', text: 'Nguyễn Minh Thúy' },
                    { value: 'vuchi', text: 'Vũ Chi' },
                    { value: 'admin', text: 'Nguyễn Trung Việt' },
                    { value: 'hanhlt', text: 'Lê Thị Hạnh' },   
                    { value: 'duongvp', text: 'Võ Phi Dương' },
                    { value: 'myvd', text: 'Võ Diễm My' }
                ];

                $scope.ChangeApprover = function (data) {
                    $scope.model2.Approver = data;
                };
                $scope.ChangeApproverF = function (data) {
                    $scope.model2.ApproverF = data;
                };
                var jsonData = editor.serialize("Json");
                if ($(this).children('path').attr('d') === 'M 0 45 L 70 0 L 140 45 L 70 90 Z') {
                    var uidCodition = $(this).attr('dhx_id');
                    //if ($scope.model.Conditions.find(x => x.uidCodition === uidCodition) === undefined) {
                   
                    var lstActionInCondition = jsonData.filter(x => x.from === uidCodition);
                    $scope.model2.StepContinue = '';
                    $scope.model2.StepCallback = '';

                    angular.forEach(lstActionInCondition, function (val, key) {
                        if (val.callBacks === 'previous')
                            $scope.model2.StepCallback += val.to + ',';
                        if (val.callBacks === 'next')
                            $scope.model2.StepContinue += val.to + ',';
                    });
                    $('#myModalTransition').modal({ backdrop: 'static' }, 'show');

                    $scope.SaveTransition = function () {
                        $scope.Conditions = {};
                        var itemData = [];
                        itemData.push({ ColumnGuid: $scope.model2.ColumnGuid, Condition: $scope.model2.Condition, ConditionValue: $scope.model2.ConditionValue, AndOr: $scope.model2.AndOr });
                        for (var i = 1; i < countConditonData; i++) {
                            var coulumn = 'ColumnGuid' + i;
                            var condition = 'Condition' + i;
                            var conditionValue = 'ConditionValue' + i;
                            var andOr = 'AndOr' + i;
                            itemData.push({ ColumnGuid: $scope.model2[coulumn], Condition: $scope.model2[condition], ConditionValue: $scope.model2[conditionValue], AndOr: $scope.model2[andOr] });
                        }
                        $scope.Conditions.ConditionData = JSON.stringify(itemData);
                        $scope.Conditions.ConditionTrue = JSON.stringify({ Activity: $scope.model2.Activity, ApproveType: $scope.model2.ApproveType, Approver: $scope.model2.Approver, StepContinue: $scope.model2.StepContinue });
                        $scope.Conditions.ConditionFalse = JSON.stringify({ Activity: $scope.model2.Activity1, Approver: $scope.model2.Approver1, ContentEmail: $scope.model2.ContentEmail, StepCallback: $scope.model2.StepCallback });
                        $scope.Conditions.uidCodition = uidCodition;
                        $scope.Conditions.StepContinue = $scope.model2.StepContinue;
                        $scope.Conditions.StepCallback = $scope.model2.StepCallback;
                        $scope.model.Conditions.push($scope.Conditions);
                        $('#myModalTransition').modal('hide');
                        console.log($scope.model);

                    };
                    //}
                    $scope.$apply();
                }
                else if ($(this).children('path').attr('d') === 'M 0,0 L 0,90 L 140,90 L 140,0 Z') {
                    $('#myModalActivity').modal('show');
                    var uidActivity = $(this).attr('dhx_id');
                    var uid = $scope.model.Activities.find(x => x.uidActivity === uidActivity);
                    $scope.stepStart = false;
                    var item = jsonData.filter(x => x.to === uidActivity);
                    var item1 = jsonData.filter(x => x.from === uidActivity);
                    if (item !== undefined) {
                        angular.forEach(item, function (val, key) {
                            var checkStart = jsonData.find(x => x.id === val.from);
                            if (checkStart !== undefined && checkStart.type === 'start')
                                $scope.stepStart = true;
                        });
                        angular.forEach(item1, function (val, key) {
                            var checkStart = jsonData.find(x => x.id === val.to);
                            if (checkStart !== undefined && checkStart.type === 'decision')
                                $scope.conditionStep = checkStart.id;
                        });
                    }
                    if (uid !== null && uid !== undefined) {
                        $scope.model2.ActivityName = uid.ActivityName;
                        $scope.$apply();
                    }
                    else {
                        $scope.model2.ActivityName = "";
                        $scope.$apply();
                    }

                    $scope.SaveActivity = function () {
                        if (uid !== null && uid !== undefined) {
                            $scope.model.Activities.ActivityName = uid.ActivityName;
                        } else {
                            $scope.Activity = {};
                            $scope.Activity = { ActivityName: $scope.model2.ActivityName, uidActivity: uidActivity, stepStart: $scope.stepStart, conditionStep: $scope.conditionStep };
                            $scope.model.Activities.push($scope.Activity);

                        }
                        $('#myModalActivity').modal('hide');
                    };
                }
            });
            $scope.Locaticon = function (item) {
                $scope.model.ActivityState = angular.uppercase(xoa_dau(item));
            };

        });
        $(document).bind('afterready', function () {
            var html = '<div class="vn_custom_block"><div id="custom"><span class="btn btn-default">Thêm thuộc tính</span></div></div>';
            var target = angular.element('.dhx_state_block');
            $(target).after(html);
        });
        $(document).on('click', '#custom', function () {
            $("#myModalFormPoperty").modal({ backdrop: 'static' }, 'show');
        });
        $scope.SaveProperty = function () {
            if ($scope.propertyform.validate()) {
                $("#myModalFormPoperty").modal('hide');
            }
        };
    }
]);

