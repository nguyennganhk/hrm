﻿var ctxfolder = "/views/HR/Departments";
app.factory('dataservice', function ($http) {
    var headers = {
        "Content-Type": "application/json;odata=verbose",
        "Accept": "application/json;odata=verbose"
    };
    return {
        getDepartmentsCombo: function (data, callback) {
            $http.get('/Departments/GetDepartments?guid=' + data).success(callback);
        },
        getOrganizations: function (callback) {
            $http.get('/Departments/GetOrganizations').success(callback);
        },
        getAll: function (data, callback) {
            $http.post('/Departments/JTable', data).success(callback);
        },
        getItem: function (data, callback) {
            $http.post('/Departments/GetById/' + data).success(callback);
        },
        getParent: function (data, callback) {
            $http.post('/Departments/GetParent/' + data).success(callback);
        },
        delete: function (data, callback) {
            $http.post('/Departments/Remove?id=' + data).success(callback);
        },
        checkPermission: function (data, callback) {
            $http.get('/Departments/checkPermission?id=' + data).success(callback);
        },
        getbyparent: function (data, callback) {
            $http.post('/Departments/getbyparent?id=' + data).success(callback);
        },
        resort: function (data, callback) {
            $http.post('/Departments/resort', data).success(callback);
        },
        getparent: function (data, callback) {
            $http.post('/Departments/GetParents?id=' + data).success(callback);
        },
        getLogin: function (callback) {
            $http.post('/Departments/getLogin').success(callback);
        },
        exportExcel: function (data, callback) {
            $http.post('/Departments/exportExcel/', data).success(callback);
        },
        getEmp: function (callback) {
            $http.get('/Departments/GetManager').success(callback);
        }
        
    };
});
app.filter('fdate', [
    '$filter', function ($filter) {
        return function (input, f) {
            if (input && input.toString().indexOf('Date') > -1) {
                return moment(input).format(f);
            } if (input && input.toString().indexOf('T') > -1) {
                return moment(input).format(f);
            } else return input;
        };
    }
]);
app.directive('format', ['$filter', function ($filter) {
    return {
        require: '?ngModel',
        link: function (scope, elem, attrs, ctrl) {
            if (!ctrl) return;
            ctrl.$formatters.unshift(function (a) {
                return $filter(attrs.format)(ctrl.$modelValue);
            });
            ctrl.$parsers.unshift(function (viewValue) {
                var plainNumber = viewValue.replace(/[^\d|\-+|\.+]/g, '');
                elem.val($filter(attrs.format)(plainNumber));
                return plainNumber;
            });
        }
    };
}]);
app.filter('fstatus', [
    '$filter', function ($filter) {
        return function (s) {
            if (s === false) return "Không hoạt động";
            if (s === true) return "Hoạt động";
            return "";
        };
    }
]);
app.filter('fsta', [
    '$filter', function ($filter) {
        return function (s) {
            if (s === 'GT') return "Gián tiếp";
            if (s === 'TT') return "Trực tiếp";
            if (s === 'Khac') return "Khác";
            return "";
        };
    }
]);
app.controller('Ctrl_HR_Departments', function ($scope, $rootScope, $compile, $uibModal, dataservice) {
    $rootScope.go = function (path) {
        $location.path(path); return false;
    };
   
    dataservice.getEmp(function (rs) {
        $rootScope.ListEmployees = [];
        $rootScope.ListEmployees.push({ value: null, text: "Bỏ chọn" })
        angular.forEach(rs, function (value, key) {
            $rootScope.ListEmployees.push({ value: value.EmployeeGuid, text: "[" + value.EmployeeId + "] " + value.FullName });
        })
    });
    jQuery_V.validator.addMethod("Email", function (value, element) {
        var data = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/i.test(value);
        return this.optional(element) || data ? true : false;
    }, "Letters, numbers, and underscores only please");
    $rootScope.validationOptions = {
        rules: {
            Email: {
                Email:true
            },
            DepartmentName: {
                required: true,
                maxlength: 50
            },
            DepartmentId: {
                required: true,
                maxlength: 20
            },
            Description: {
                maxlength: 255
            }
            ,
            Address: {
                maxlength: 255
            },
            Phone: {
                maxlength: 50,
                min: 0
            },
            Fax: {
                maxlength: 50,
                min: 0
            },
            OrderId: {
                min: 0
            }
        },
        messages: {
            DepartmentName: {
                required: "Yêu cầu nhập tên phòng ban.",
                maxlength: "Tên phòng ban không vượt quá 50 ký tự."
            },
            DepartmentId: {
                required: "Yêu cầu nhập mã phòng ban.",
                maxlength: "Mã phòng ban không vượt quá 20 ký tự."
            },
            Description: {
                maxlength: 'Mô tả không vượt quá 255 ký tự.'
            },
            Address: {
                maxlength: 'Địa chỉ không vượt quá 255 ký tự.'
            },
            Email: {
                Email: 'Yêu cầu nhập đúng định dạng email.'
            },
            Phone: {
                maxlength: 'Số điện thoại không vượt quá 50 ký tự.',
                min: 'Yêu cầu nhập số nguyên dương.'
            },
            Fax: {
                maxlength: 'Số fax không vượt quá 50 ký tự.',
                min: 'Yêu cầu nhập số nguyên dương.'
            },
            OrderId: {
                min: 'Yêu cầu nhập số nguyên dương.'
            }
        }
    };
    $rootScope.ListIsActive = [{
        value: 1,
        text: 'Hoạt động'
    }, {
        value: 0,
        text: 'Không hoạt động'
    }];
    $rootScope.IsActive = [{
        value: 1,
        text: 'Hoạt động'
    }, {
        value: 0,
        text: 'Không hoạt động'
        }];
    $rootScope.Khoi = [{
        value: "GT",
        text: 'Gián tiếp'
    }, {
        value: "TT",
        text: 'Trực tiếp'
        },
         {
            value: "Khac",
            text: 'Khac'
        }
    ];
    
    $rootScope.StatusConfig = {
        placeholder: 'Chọn ...',
        search: true
    };
    $rootScope.IsActiveConfig = {
        placeholder: 'Chọn trạng thái...',
        selected: ['1'],
        search: true
    };


    //Config
    $rootScope.DepartmentsComboConfig = {
        placeholder: 'Chọn phòng ban...',
        search: true,
        firstload: false
    };
    //Thêm phần tinymceOptions
    $rootScope.tinymceOptions = {
        selector: 'textarea',
        plugins: [
            'advlist autolink lists link charmap print preview hr anchor pagebreak image',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
        ],
        skin: 'lightgray', language: 'vi_VN',
        theme: 'modern', height: 70,
        toolbar1: 'codesample | bold italic sizeselect fontselect fontsizeselect | hr alignleft aligncenter alignright alignjustify | bullist numlist outdent indent',
        fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
        //toolbar2: 'print preview insert link image media | forecolor backcolor emoticons | codesample',
        image_advtab: true,

        file_browser_callback: function (field_name, url, type, win) {
            var connector = "/FileManager/Folder/File";
            var enableAutoTypeSelection = true;
            if (enableAutoTypeSelection && type) {
                connector += "&Type=" + type;
            }
            connector += "&Field=" + field_name;
            App.openPage(connector, "File Manager", 1150, 600);
        },
        relative_urls: false,
        remove_script_host: true,
        convert_urls: true
    };
    $rootScope.ConvertTodayDate = function (date) {
        if (date !== null && date !== "") {
            var newdate = new Date(date);
            var month = newdate.getMonth() + 1;
            var day = newdate.getDate();
            var year = newdate.getFullYear();
            var hh = newdate.getHours();
            var mm = newdate.getMinutes();
            if (month < 10)
                month = "0" + month;
            if (day < 10)
                day = "0" + day;
            if (mm < 10)
                mm = "0" + mm;
            return todayDate = day + "/" + month + "/" + year;
        } else {
            return null;
        }
    };
    $rootScope.ConvertDateView = function (date) {
        if (date !== null && date !== "") {
            var newdate = new Date(date);
            var month = newdate.getMonth() + 1;
            var day = newdate.getDate();
            var year = newdate.getFullYear();
            var hh = newdate.getHours();
            var mm = newdate.getMinutes();
            if (month < 10)
                month = "0" + month;
            if (day < 10)
                day = "0" + day;
            if (mm < 10)
                mm = "0" + mm;
            return todayDate = day + "/" + month + "/" + year + ' ' + hh + ':' + mm;
        } else {
            return null;
        }
    };
    $rootScope.convertDate = function (datetime) {
        if (datetime !== null && datetime !== undefined && datetime !== "") {
            var value = datetime.split(' ');
            return value[0];
        } else {
            return "";
        }
    };
    $rootScope.convertToDate = function (datetime) {
        if (datetime !== null && datetime !== "") {
            var Str = datetime.toString();
            if (Str.indexOf("/Date") >= 0) {

                var newdate = new Date(parseInt(datetime.substr(6)));
                var month = newdate.getMonth() + 1;
                var day = newdate.getDate();
                var year = newdate.getFullYear();
                var hh = newdate.getHours();
                var mm = newdate.getMinutes();
                if (month < 10)
                    month = "0" + month;
                if (day < 10)
                    day = "0" + day;
                return year + "-" + month + "-" + day;
            } else {
                var value = datetime.split('/');

                return value[2] + '-' + value[1] + '-' + value[0];
            }
        }
        return null;
    };
    $rootScope.ConvertNumber = function (number) {
        if (number !== null && number !== "" && number !== undefined) {
            var data = number.split('.0000');
            if (data !== undefined) {
                var f = data[0];
                return f.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            } else {
                return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            }
        }
    };
});
app.config(function ($routeProvider, $validatorProvider) {
    $routeProvider
        .when('/', {
            templateUrl: ctxfolder + '/index.html',
            controller: 'index'
        })
        .when('/edit/:id', {
            templateUrl: ctxfolder + '/edit.html',
            controller: 'edit'
        })
        .when('/add/', {
            templateUrl: ctxfolder + '/add.html',
            controller: 'add'
        })
        .when('/addDepartments/', {
            templateUrl: ctxfolder + '/addDepartments.html',
            controller: 'addDepartments'
        });
    $validatorProvider.setDefaults({
        errorElement: 'span',
        errorClass: 'help-block',
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        success: function (label) {
            label.closest('.form-group').removeClass('has-error');
        }
    });
});
app.controller('index', function ($scope, $http, $timeout, $rootScope, $compile, $confirm, $ngConfirm, $uibModal, DTOptionsBuilder, DTColumnBuilder, DTInstances, dataservice) {
    $scope.model = {};
    var ctrl = $scope;
    $rootScope.ID = undefined;
    ctrl.para = window.location.href.split('?')[1];
    $scope.staticParam = {
        IsLocked: 1,
        OrganizationGuid:[]
    };
   
    $scope.ListOrganizations = [];
    $scope.jstreeData = [];
    $scope.ignoreChanges = false;
    $scope.reloadTree = function () {
        $scope.model.reSelected = [];
        $scope.jstreeData = [];
        dataservice.getOrganizations(function (rs) {
            if (rs.Error) {
                App.notifyDanger("Có lỗi khi lấy dữ liệu.");
            } else {
                $scope.ListOrganizations = [];
                $scope.ListOrganizations = rs;
                if ($scope.ListOrganizations.length > 0) {
                    $scope.model.OrganizationGuid = $scope.ListOrganizations[0].OrganizationGuid;
                    $scope.staticParam.OrganizationGuid = [];
                    $scope.staticParam.OrganizationGuid.push($scope.ListOrganizations[0].OrganizationGuid);
                    $rootScope.LoadCombo($scope.staticParam.OrganizationGuid);
                }
                $scope.jstreeData = initJsTreeData(rs);
            }
            $scope.treeConfig.version++;
        });
    };
    $scope.reloadTree();

    $scope.applyModelChanges = function () {
        return !$scope.ignoreChanges;
    };
    $scope.treeConfig = {
        core: {
            error: function (error) {
                $log.error('treeCtrl: error from js tree - ' + angular.toJson(error));
            },
            check_callback: true, worker: true
        },
        "checkbox": {
            "three_state": true,
            "whole_node": false,
            "keep_selected_style": true,
            "cascade": "undetermined"
        },
        plugins: ['types', 'state'],
        version: 1,
        types: {
            valid_children: ["selected"],
            types: {
                "selected": {
                    "select_node": false
                }
            }
        }
    };
    $scope.applyModelChanges = function () {
        return !$scope.ignoreChanges;
    };
    function initJsTreeData(data) {
        if (!data) {
            return null;
        }
        var jsData = [];
        data.forEach(function (item, index) {
            if (item.ParentId === null) {
                var treeItem = {
                    id: item.OrganizationGuid,
                    parent: '#',
                    text: "<a href='' title='" + item.OrganizationName + "'>" + item.OrganizationName + "</a>",
                    state: {
                        opened: false
                    }
                };
                jsData.push(treeItem);
                subJsTreeData(jsData, data, item.OrganizationGuid);
            }
        });
        return jsData;
    }
    function subJsTreeData(jsData, data, parentid) {
        data.forEach(function (item, index) {
            if (item.ParentId && item.ParentId === parentid) {
                var treeItem = {
                    id: item.OrganizationGuid,
                    parent: item.ParentId,
                    text: "<a href='' title='" + item.OrganizationName + "'>" + item.OrganizationName + "</a>",
                    state: {
                        "opened": false
                    }
                };
                jsData.push(treeItem);
                subJsTreeData(jsData, data, item.OrganizationGuid);
            }
        });

        return jsData;
    }
    $scope.readyCB = function () {
        $timeout(function () {
            $scope.ignoreChanges = false;
        });
    };
    $scope.lstOrganizationGuid = [];
    $scope.changeNode = function (event, data) {
        var items = $scope.treeInstance.jstree(true).get_selected();
        if (items.length > 0) {
            $scope.staticParam.OrganizationGuid = [];
            $scope.staticParam.OrganizationGuid = items;
            $scope.staticParam.DepartmentGuid = null;
            if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
                $scope.$apply();
            }
           
            $rootScope.LoadCombo($scope.staticParam.OrganizationGuid);
            $rootScope.loadData();
        } 

    };
    //combobox tìm kiếm
    $rootScope.LoadCombo = function (data) {
        $rootScope.ListDepartmentsCombo = [];
        dataservice.getDepartmentsCombo(data, function (result) {
            $rootScope.ListDepartmentsCombo = [];
            $rootScope.ListDepartmentsCombo = result;
            var item = {
                value: '',
                text: 'Bỏ chọn'
            };
            $rootScope.ListDepartmentsCombo.unshift(item);
        });
    }
    //Table
    var vm = $scope;
    $scope.staticParam = {
        totalItems: 0,
        currentPage: 1,
        pageSize: 25,
        maxSize: 5,
        OrganizationGuid: null,
        //IsActive: 1,
        DepartmentGuid: null
    };
    //$scope.IsActiveChange = function (data) {
    //    $scope.staticParam.IsActive = parseInt(data);
    //    $rootScope.loadData();
    //};
    $scope.DepartmentChange = function (data) {
        $scope.staticParam.DepartmentGuid = data;
        $rootScope.loadData();
    };
    $scope.selected = [];
    $scope.selectAll = false;
    $rootScope.loadData = function () {
        App.blockUI({
            target: "#contentMain",
            boxed: true,
            message: 'Đang tải...'
        });
        var obj = {
            OrganizationGuid: $scope.staticParam.OrganizationGuid === "null" ? null : $scope.staticParam.OrganizationGuid,
            DepartmentGuid: $scope.staticParam.DepartmentGuid === "null" ? null : $scope.staticParam.DepartmentGuid,
            ParentId: $scope.staticParam.DepartmentGuid === "null" ? null : $scope.staticParam.DepartmentGuid
        };
        dataservice.getAll(obj, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {
                $scope.tree_data = buildJtree(rs.Object);
            }
            App.unblockUI("#contentMain");
        });
    };
    //$rootScope.loadData();
    $rootScope.reload = function () {
        $rootScope.OrganizationGuid = $scope.model.OrganizationGuid;  
        $scope.staticParam.OrganizationGuid = [];
        $scope.staticParam.OrganizationGuid.push($scope.model.OrganizationGuid);
        $scope.staticParam.DepartmentGuid = null;
        $scope.DepartmentGuid = null;
        $rootScope.loadData();
    };
   
    $scope.expanding_property = {
        field: "DepartmentName",
        width: "550px",
        displayName: "Tên phòng ban",
        sortable: true,
        filterable: true
    };
    $scope.tree_data = [];
    $scope.my_tree = tree = {};
    var data = [];
    data.push('<a style="margin-left:5px!important" class="table__cell-actions-item table__cell-actions-icon" title="Xem" ng-click="cellTemplateScope.open(row.branch)" onclick="return false"><i class="mdi mdi-eye"></i ></a >');
    data.push('<a style="margin-left:5px!important" class="table__cell-actions-item table__cell-actions-icon" title="Sửa" ng-click="cellTemplateScope.edit(row.branch)" onclick="return false"><i class="mdi mdi-table-edit"></i ></a >');
    data.push('<a style="margin-left:5px!important" class="table__cell-actions-item table__cell-actions-icon" title="Xóa" ng-click="cellTemplateScope.delete(row.branch)" onclick="return false"><i class="mdi mdi-delete"></i ></a >');

    var contextMenu = [];
    contextMenu.push("<li ><a class='dropdown-item' href='#' ng-click='cellTemplateScope.addDepartments(row.branch)' title='Thêm' onclick='return false'><i class='mdi mdi-table-edit'></i>Thêm</a></li>");
    contextMenu.push("<li ><a class='dropdown-item' href='#' ng-click='cellTemplateScope.open(row.branch)' title='Xem' onclick='return false'><i class='mdi mdi-eye'></i> Xem</a></li>");
    contextMenu.push("<li ><a class='dropdown-item' href='#' ng-click='cellTemplateScope.edit(row.branch)' title='Sửa' onclick='return false'><i class='mdi mdi-table-edit'></i> Sửa </a></li>");
    contextMenu.push("<li ><a class='dropdown-item' href='#' ng-click='cellTemplateScope.delete(row.branch)' title='Xóa ' onclick='return false'><i class='mdi mdi-delete'></i> Xóa</a></li>");
    contextMenu.push('<li ><a class="dropdown-item" Commonef="#" ng-click="cellTemplateScope.sort(\'A\',row.branch)" title="Sắp xếp" onclick="return false"><i class="mdi mdi-view-list"></i> Sắp xếp</a></li>');
    $scope.col_defs = [
        {
            field: "",
            sortable: false, width: '30px',
            displayName: "",
            colclass: "action",
            cellTemplate: "<div class='btn-group'>"
                + "<a   class='btn btn-icon-xs btn-icon-only'data-toggle='dropdown'><i class='es-icon mdi mdi-view-list subnav__heading-icon'></i></a>"
                + "<ul class='dropdown-menu  pull-right' >"
                + contextMenu.join("")
                + "</ul>"
                + "</div>",
            cellTemplateScope: {
                delete: function (data) {
                    $scope.delete(data.DepartmentGuid);
                },
                edit: function (data) {
                    $scope.edit(data.DepartmentGuid);
                },
                addDepartments: function (data) {
                    $scope.addDepartments(data.DepartmentGuid);
                },
                open: function (data) {
                    $scope.open(data.DepartmentGuid);
                },
                sort: function (id, data) {
                    $scope.sort(id, data.DepartmentGuid);
                }

            }
        },
        {
            field: "DepartmentId", displayName: "Mã phòng ban", width: "200px",
            sortable: true,
            cellTemplate: "<span>{{row.branch['DepartmentId']}}</span>"
        },
        {
            field: "Description", displayName: "Khối", width: "200px",
            sortable: true,
            cellTemplate: "<span>{{row.branch['Description'] | fsta}}</span>"
        },
        {
            field: "IsActive", displayName: "Trạng thái", width: "120px",
            sortable: true,
            cellTemplate: "<span> {{row.branch[col.field] | fstatus}} </span>"
        },
        {
            field: "", displayName: "Điều khiển", width: "90px",
            sortable: false,
            cellTemplate: "<div class='es-tcenter'>" + data.join("") + "</div>",
            cellTemplateScope: {
                delete: function (data) {
                    $scope.delete(data.DepartmentGuid);
                },
                edit: function (data) {
                    $scope.edit(data.DepartmentGuid);
                },
                addDepartments: function (data) {
                    $scope.addDepartments(data.DepartmentGuid);
                },
                open: function (data) {
                    $scope.open(data.DepartmentGuid);
                },
                sort: function (id, data) {
                    $scope.sort(id, data.DepartmentGuid);
                }

            }
        }
    ];

    //table
    $scope.my_tree_handler = function (branch) {
        $rootScope.selectedItem = branch;
    };
    function buildJtree(data) {
        var tree = [];
        if (!data || data.length === 0) return [];
        $.each(data, function (index, item) {
            if (!item.ParentId) {
                var treeObjs = item;
                var sub = subJtree(data, item.DepartmentGuid);
                if (sub.length > 0) {
                    treeObjs.children = sub;
                }
                treeObjs.expanded = true;
                tree.push(treeObjs);
            }
        });
        return tree;
    }
    function subJtree(data, parentVal) {
        var subTree = [];
        $.each(data, function (index, item) {
            if (item.ParentId && item.ParentId === parentVal) {
                var treeObjs = item;
                var sub = subJtree(data, item.DepartmentGuid);
                if (sub.length > 0) {
                    treeObjs.children = sub;
                }
                treeObjs.expanded = false;
                subTree.push(treeObjs);
            }
        });
        return subTree;
    }
    $scope.IsActiveChange = function (data) {
        $scope.staticParam.IsActive = data;
        $rootScope.loadData();
    }
    vm.reloadData = reloadData;
    vm.dtInstance = {};
    function reloadData(resetPaging) {
        $scope.staticParam.currentPage = resetPaging;
    }
    $scope.reloadAll = function () {
        $scope.selectAll = false;
        $scope.staticParam.DepartmentGuid = null;
        $scope.staticParam.OrganizationGuid = [];            
        $rootScope.loadData();
    };
    $scope.reloadAll();
    //tính năng
    $scope.sort = function (a, obj) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/sort.html',
            controller: 'sort',
            backdrop: 'static',
            size: '60',
            resolve: {
                para: function () {
                    if (obj === $scope.staticParam.OrganizationGuid) {
                        return { guid: $scope.staticParam.OrganizationGuid, text: a };

                    } else {
                        return { guid: obj, text: a };
                    }

                }
            }
        });
        modalInstance.result.then(function (d) {
            if ($rootScope.ID !== undefined) {
                $rootScope.Departments();
            } else {
                $rootScope.loadData();
            }
        }, function () {
        });
    };
    //
    $scope.addDepartments = function (temp) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/addDepartments.html',
            controller: 'addDepartments',
            backdrop: 'static',
            size: '80',
            resolve: {
                para: function () {
                    return temp;
                   
                }
            }
        });
        modalInstance.result.then(function (d) {
            //$scope.reloadTree();
        }, function () {
        });
    };
    //
    $scope.add = function () {
        if ($scope.staticParam.OrganizationGuid.length === 0) {
            App.notifyDanger("Yêu cầu chọn đơn vị, tổ chức");
            return false;
        } else {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: ctxfolder + '/add.html',
                controller: 'add',
                backdrop: 'static',
                size: '80',
                resolve: {
                    para: function () {
                        if ($scope.staticParam.OrganizationGuid.length > 0) {
                            return $scope.staticParam.OrganizationGuid[0];
                        } else {
                            return null;
                        }
                    }
                }
            });
            modalInstance.result.then(function (d) {
            }, function () {
            });
        }
    };
    //
    $scope.open = function (temp) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/open.html',
            controller: 'open',
            backdrop: 'static',
            size: '80',
            resolve: {
                para: function () {
                    return temp;
                }
            }
        });
        modalInstance.result.then(function (d) {
        }, function () {
        });

    };
    //
    $scope.edit = function (temp) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: ctxfolder + '/edit.html',
            controller: 'edit',
            backdrop: 'static',
            size: '80',
            resolve: {
                para: function () {
                    return temp;
                }
            }
        });
        modalInstance.result.then(function (d) {
        }, function () {
        });
    };
    //
    $scope.delete = function (temp) {
        dataservice.getItem(temp, function (rs1) {
            ESConfirm([{ 'value': 1, 'title': 'Xác nhận', 'type': 'info' }, { 'value': 2, 'title': 'Đóng', 'type': 'danger' }], { 'description': 'Bạn có muốn xóa ' + rs1.DepartmentName, 'class': 'eswarning_v2' }, function (rs) {
                if (rs === '1') {
                    App.blockUI({
                        target: "#contentMain",
                        boxed: true,
                        message: 'Đang tải...'
                    });
                    dataservice.delete(temp, function (result) {
                        if (result.Error) {
                            App.notifyDanger(result.Title);
                        } else {
                            App.notifyInfo(result.Title);
                            $rootScope.loadData();
                            $rootScope.LoadCombo(rs1.OrganizationGuid);
                        }
                        App.unblockUI("#contentMain");
                    });
                }
            }, function () {
            });
        });
    };
    // export excel
    $scope.exportExcel = function () {
        App.blockUI({
            target: "#contentMain",
            boxed: true,
            message: 'Đang tải dữ liệu...'
        });
        var obj = {
            OrganizationGuid: $scope.staticParam.OrganizationGuid === "null" ? null : $scope.staticParam.OrganizationGuid,
            DepartmentGuid: $scope.staticParam.DepartmentGuid === "null" ? null : $scope.staticParam.DepartmentGuid        };
        dataservice.exportExcel(obj, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
                App.unblockUI("#contentMain");
                return;
            }
            else {
                window.location.href = rs.Title;
                App.unblockUI("#contentMain");
            }
        });
    };
});
//controller
app.controller('add', function ($scope, $rootScope, $uibModalInstance, para, dataservice) {
    $scope.Title = "Thêm mới phòng ban";
    $scope.model = { Description: "GT"};
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
    $scope.model.OrganizationGuid = para;
    $scope.ChangeDepartment = function (data) {
        $scope.model.ParentId = data;
        var rs = $scope.ListDepartmentsCombo.find(x => x.value === data);
    };
    $scope.ChangeIsActive = function (data) {
        $scope.model.IsActive = parseInt(data);
    };
    $scope.ListEmpChange = function (data) {
        $scope.model.ManagerGuid = data !== "null" ? data : "";
    }
   
    $scope.loadData();
    $scope.submit = function () {
        if ($scope.addform.validate()) {
            if ($scope.model.ManagerGuid === "null" || $scope.model.ManagerGuid === "" || $scope.model.ManagerGuid === null || $scope.model.ManagerGuid === undefined) {
                $scope.model.ManagerGuid = null;
            }
            var fd = new FormData();
            fd.append('insurances', JSON.stringify($scope.model));
            fd.append('submit', JSON.stringify($scope.model));
            $.ajax({
                type: "POST",
                url: "/Departments/Submit",
                contentType: false,
                processData: false,
                data: fd,
                success: function (rs) {
                    if (rs.Error) {
                        App.notifyDanger(rs.Title);
                    } else {
                        App.notifyInfo(rs.Title);
                        $uibModalInstance.close();
                        $rootScope.loadData();
                        $rootScope.LoadCombo(rs.Object);

                    }
                }
            });
        }

    };
});
//
app.controller('edit', function ($scope, $rootScope, $uibModalInstance, dataservice, para) {
    $scope.model = {};
    $scope.Title = "Sửa phòng ban";
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
    $scope.ChangeDepartment = function (data) {
        $scope.model.ParentId = data;
        var rs = $scope.ListDepartmentsCombo.find(x => x.value === data);
    };
    $scope.ChangeIsActive = function (data) {
        $scope.model.IsActive = parseInt(data);
    };
    $scope.ListEmpChange = function (data) {
        $scope.model.EmployeeGuid = data !== "null" ? data : "";
    }
   
    $scope.initData = function () {
        dataservice.getItem(para, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {
                $scope.model = rs;
                $scope.model.id = rs.DepartmentId;
                $scope.model.Phone = parseInt($scope.model.Phone);
                $scope.model.Fax = parseInt($scope.model.Fax);
                if ($scope.model.IsActive === true) {
                    $scope.model.IsActive = 1;
                } else { $scope.model.IsActive = 0; }
            }
        });
    };
    $scope.initData();
    $scope.update = function () {
        if ($scope.editform.validate()) {
            if ($scope.model.ManagerGuid === "null" || $scope.model.ManagerGuid === "" || $scope.model.ManagerGuid === null || $scope.model.ManagerGuid === undefined) {
                $scope.model.ManagerGuid = null;
            }
            var fd = new FormData();
            fd.append('insurances', JSON.stringify($scope.model));
            fd.append('edit', JSON.stringify($scope.model));
            $.ajax({
                type: "POST",
                url: "/Departments/Edit",
                contentType: false,
                processData: false,
                data: fd,
                success: function (rs) {
                    if (rs.Error) {
                        App.notifyDanger(rs.Title);
                    } else {
                        App.notifyInfo(rs.Title);
                        $uibModalInstance.close();
                        $rootScope.loadData();
                        $rootScope.LoadCombo(rs.Object);

                    }
                }
            });
        }

    };
});
//
app.controller('open', function ($scope, $uibModalInstance, dataservice, para) {
    $scope.Title = "Xem phòng ban";
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
    $scope.initData = function () {
        dataservice.getItem(para, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {
                $scope.model = rs;
                if ($scope.model.IsActive === true) {
                    $scope.model.IsActive = "Sử dụng";
                } else { $scope.model.IsActive = "Không sử dụng"; }
                if ($scope.model.Description === "GT") {
                    $scope.model.Description = "Gián tiếp";
                }
                else if ($scope.model.Description === "TT") {
                    $scope.model.Description = "Trực tiếp";
                }
                else if ($scope.model.Description === "Khac") {
                    $scope.model.Description = "Khác";
                }
                else { $scope.model.Description = ""; }
             
                if (rs.CreatedBy !== null) {
                    $scope.model.CreatedBy = $scope.model.CreatedBy.split('#')[1];
                }
                if (rs.ModifiedBy !== null) {
                    $scope.model.ModifiedBy = $scope.model.ModifiedBy.split('#')[1];
                }
            }
        });
    };
    $scope.initData();
});
//
app.controller('addDepartments', function ($scope, $rootScope, $uibModalInstance, dataservice, para) {
    $scope.Title = "Thêm phòng ban";
    $scope.model = {};
    $scope.model.ParentId = null;
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
    $scope.ChangeIsActive = function (data) {
        $scope.model.IsActive = parseInt(data);
    };
    $scope.ListEmpChange = function (data) {
        $scope.model.ManagerGuid = data !== "null" ? data : "";
    }
    dataservice.getEmp(function (rs) {
        $rootScope.ListEmployees = [];
        $rootScope.ListEmployees.push({ value: null, text: "---Mời bạn chọn---" })
        angular.forEach(rs, function (value, key) {
            $rootScope.ListEmployees.push({ value: value.EmployeeGuid, text: "[" + value.EmployeeId + "] " + value.FullName });
        })
    });
    $scope.initData = function () {
        dataservice.getParent(para, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {
                $scope.model.ParentId = rs.DepartmentGuid;
                $scope.model.OrganizationGuid = rs.OrganizationGuid;
            }
        });
    };
    $scope.initData();
    $scope.insert = function () {
        if ($scope.addform.validate()) {
            var fd = new FormData();
            fd.append('insurances', JSON.stringify($scope.model));
            fd.append('submit', JSON.stringify($scope.model));
            $.ajax({
                type: "POST",
                url: "/Departments/Submit",
                contentType: false,
                processData: false,
                data: fd,
                success: function (rs) {
                    if (rs.Error) {
                        App.notifyDanger(rs.Title);
                    } else {
                        App.notifyInfo(rs.Title);
                        $uibModalInstance.close();
                        $rootScope.loadData();
                        $rootScope.LoadCombo(rs.Object);

                    }
                }
            });
        }
    };
});
//
app.controller('sort', function ($scope, $rootScope, $uibModalInstance, dataservice, para) {
    $scope.Title = "Sắp xếp thứ tự";
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
    $scope.initData = function () {
        if (para.text !== 'A') {
            dataservice.getbyparent(para.guid, function (rs) {
                $scope.model = rs;
            });
        } else {
            dataservice.getparent(para.guid, function (rs) {
                $scope.model = rs;
            });
        }
    };
    $scope.initData();
    $scope.resort = function (item, index) {
        $scope.model.splice(index, 1);
        $scope.model.splice(item.OrderId - 1, 0, item);
        $.each($scope.model, function (index, item) {
            item.OrderId = index + 1;
        });
    };
    $scope.submit = function () {
        dataservice.resort($scope.model, function (rs) {
            if (rs.Error) {
                App.notifyDanger(rs.Title);
            } else {
                App.notifyInfo(rs.Title);
                $uibModalInstance.close();
                $rootScope.loadData();
                $rootScope.LoadCombo();
            }
        });
    };
});
