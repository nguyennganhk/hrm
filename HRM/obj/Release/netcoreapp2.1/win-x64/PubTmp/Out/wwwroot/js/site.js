﻿App.activeNav = function () {
    var pathname = window.location.pathname.split('/');
    var area = pathname[1];
    var ac = 0;
    var $elements = $('ul.nav').find('li>a');
    $.each($elements, function (key, value) {
        var a = document.createElement('a');
        a.href = value;
        if (a.pathname.split('/')[1].toUpperCase() == area.toUpperCase()) {
            $('ul.nav > li.' + key + '').addClass("open");
            ac++;
        }
    });
}
App.Permissions = {};
function loadPermissions() {
    var body = document.getElementsByTagName('body')[0];
    var arrPerms = body.getAttribute('data-permission').split(',');
    if (_.contains(arrPerms, 'ACCESS')) {
        App.Permissions.ACCESS = true;
    }
    if (_.contains(arrPerms, 'LISTVIEW')) {
        App.Permissions.LISTVIEW = true;
    }
    if (_.contains(arrPerms, 'OPEN')) {
        App.Permissions.OPEN = true;
    }
    if (_.contains(arrPerms, 'ADD')) {
        App.Permissions.ADD = true;
    }
    if (_.contains(arrPerms, 'EDIT')) {
        App.Permissions.EDIT = true;
    }
    if (_.contains(arrPerms, 'DELETE')) {
        App.Permissions.DELETE = true;
    }
    if (_.contains(arrPerms, 'SORT')) {
        App.Permissions.SORT = true;
    }
    if (_.contains(arrPerms, 'EXPORT')) {
        App.Permissions.EXPORT = true;
    }
    if (_.contains(arrPerms, 'IMPORT')) {
        App.Permissions.IMPORT = true;
    }
    if (_.contains(arrPerms, 'DOWNLOAD')) {
        App.Permissions.DOWNLOAD = true;
    }
    if (_.contains(arrPerms, 'APPROVE')) {
        App.Permissions.APPROVE = true;
    }
    if (_.contains(arrPerms, 'REJECT')) {
        App.Permissions.REJECT = true;
    }
    if (_.contains(arrPerms, 'FORWARD')) {
        App.Permissions.FORWARD = true;
    }
    if (_.contains(arrPerms, 'SET_PASSWORD')) {
        App.Permissions.SET_PASSWORD = true;
    }
    if (_.contains(arrPerms, 'ADD_FOLDER')) {
        App.Permissions.ADD_FOLDER = true;
    }
    if (_.contains(arrPerms, 'EDIT_FOLDER')) {
        App.Permissions.EDIT_FOLDER = true;
    }
    if (_.contains(arrPerms, 'DELETE_FOLDER')) {
        App.Permissions.DELETE_FOLDER = true;
    }
    if (_.contains(arrPerms, 'LOCKUP')) {
        App.Permissions.LOCKUP = true;
    }
    if (_.contains(arrPerms, 'UNLOCK')) {
        App.Permissions.UNLOCK = true;
    }
    if (_.contains(arrPerms, 'COMMENT')) {
        App.Permissions.COMMENT = true;
    }
    if (_.contains(arrPerms, 'ADD_OTHER')) {
        App.Permissions.ADD_OTHER = true;
    }
    if (_.contains(arrPerms, 'EDIT_OTHER')) {
        App.Permissions.EDIT_OTHER = true;
    }
    if (_.contains(arrPerms, 'DELETE_OTHER')) {
        App.Permissions.DELETE_OTHER = true;
    }
    if (_.contains(arrPerms, 'FULLCONTROL')) {
        App.Permissions.FULLCONTROL = true;
    }
}
loadPermissions();
App.jFilter = function (response) {

}

$(document).ready(function () {
    App.activeNav();
});
$(document).ready(function () {
    //set chiều cao cho nav bên phải
    
    //$(".tab-content").height($(document).height() - $('#myTab').height() - $('.settings-panel__header').height() - 25);
    //$('.settings-panel-control').click(function () {
    //    $(".tab-content").height($(document).height() - $('#myTab').height() - $('.settings-panel__header').height() - 25);
    //});
    var heighDT = $(window).height() - $('.navbar-light').height() - $('.subnav').height() - $('.widget-tabs__header').height() - 230;
    var dt = heighDT + "px";
    var widthDT = $(window).width() - $('.simplebar-content').width() - 20;
    var wt = widthDT + "px";
    $('#home-tab').click(function () {
        $('#social-profile-activities').removeClass('hide');
        $('#social-profile-followers').addClass('hide');
        $('#social-profile-following').addClass('hide');
    });
    $('#profile-tab').click(function () {
        $('#social-profile-activities').addClass('hide');
        $('#social-profile-followers').removeClass('hide');
        $('#social-profile-following').addClass('hide');
    });
    $('#contact-tab').click(function () {
        $('#social-profile-activities').addClass('hide');
        $('#social-profile-followers').addClass('hide');
        $('#social-profile-following').removeClass('hide');
    });
});