#pragma checksum "E:\Project_2022\Project_Web\HRM2020\hrm\HRM\Views\Shared\_settingsPanel.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "08d54ee65e442414fcf67cb76566845a4757755d"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Shared__settingsPanel), @"mvc.1.0.view", @"/Views/Shared/_settingsPanel.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Shared/_settingsPanel.cshtml", typeof(AspNetCore.Views_Shared__settingsPanel))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "E:\Project_2022\Project_Web\HRM2020\hrm\HRM\Views\_ViewImports.cshtml"
using HRM;

#line default
#line hidden
#line 2 "E:\Project_2022\Project_Web\HRM2020\hrm\HRM\Views\_ViewImports.cshtml"
using HRM.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"08d54ee65e442414fcf67cb76566845a4757755d", @"/Views/Shared/_settingsPanel.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"f3f2f153c5daf588fa72175de51ed48d3760702c", @"/Views/_ViewImports.cshtml")]
    public class Views_Shared__settingsPanel : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 1 "E:\Project_2022\Project_Web\HRM2020\hrm\HRM\Views\Shared\_settingsPanel.cshtml"
  
    ViewData["Title"] = "_settingsPanel";
    Layout = null;

#line default
#line hidden
            BeginContext(70, 1783, true);
            WriteLiteral(@"
<div class=""settings-panel"">
    <div class=""settings-panel__header"">
        <span class=""settings-panel__close ua-icon-modal-close""></span>
        <h5 class=""settings-panel__heading"">Tiêu đề hộp thoại</h5>
    </div>
    <div class=""settings-panel__body"">
        <div class=""m-social-profile__tabs"">
            <ul class=""nav nav-tabs"" id=""myTab"" role=""tablist"" style=""margin-left:20px"">
                <li class=""nav-item m-social-profile__tab-item"">
                    <a class=""nav-link m-social-profile__tab-link active"" id=""home-tab"" data-toggle=""tab"" href=""#social-profile-activities"" role=""tab"" aria-controls=""home"" aria-selected=""true"">Các hoạt động</a>
                </li>
                <li class=""nav-item m-social-profile__tab-item"">
                    <a class=""nav-link m-social-profile__tab-link"" id=""profile-tab"" data-toggle=""tab"" href=""#social-profile-followers"" role=""tab"" aria-controls=""profile"" aria-selected=""false"">Người theo dõi</a>
                </li>
                <li");
            WriteLiteral(@" class=""nav-item m-social-profile__tab-item"">
                    <a class=""nav-link m-social-profile__tab-link"" id=""contact-tab"" data-toggle=""tab"" href=""#social-profile-following"" role=""tab"" aria-controls=""contact"" aria-selected=""false"">Đang theo dõi</a>
                </li>
            </ul>
            <div class=""tab-content"" data-simplebar=""init"">
                <div class=""tab-pane fade show active"" id=""social-profile-activities"">
                    <div class=""m-social-profile__activities"">
                        <div class=""m-social-profile__activity-item"">
                            <div class=""m-social-profile__activity-item-header"">
                                <img src=""http://static.essolution.net/img/users/user-15.png""");
            EndContext();
            BeginWriteAttribute("alt", " alt=\"", 1853, "\"", 1859, 0);
            EndWriteAttribute();
            BeginContext(1860, 3696, true);
            WriteLiteral(@" class=""m-social-profile__activity-item-avatar rounded-circle"" width=""56"" height=""56"">
                                <div class=""m-social-profile__activity-item-info"">
                                    <div>
                                        <a href=""#"" class=""link-info"">Francesca S.</a> <span class=""m-social-profile__activity-item-event"">created a new event</span>
                                    </div>
                                    <div>
                                        <span class=""m-social-profile__activity-item-date"">
                                            <span class=""ua-icon-datepicker m-social-profile__activity-item-icon""></span> <span>09:12 pm</span>
                                        </span>
                                        <span class=""m-social-profile__activity-item-location"">
                                            <span class=""ua-icon-map m-social-profile__activity-item-icon""></span> <span>Las Vegas</span>
                                ");
            WriteLiteral(@"        </span>
                                    </div>
                                </div>
                                <div class=""m-social-profile__activity-item-controls"">
                                    <a href=""#"" class=""m-social-profile__activity-item-reply m-social-profile__activity-item-control""><span class=""ua-icon-reply icon""></span> Reply</a>
                                    <div class=""dropdown no-arrow m-social-profile__activity-item-dropdown m-social-profile__activity-item-control"">
                                        <span class=""dropdown-toggle ua-icon-dots-vertical"" data-toggle=""dropdown""></span>
                                        <div class=""dropdown-menu dropdown-menu-right"">
                                            <a class=""dropdown-item"" href=""#"">I don’t want to see this</a>
                                            <a class=""dropdown-item"" href=""#"">Unfollow Antonius</a>
                                            <a class=""dropdown-item"" href=""#""");
            WriteLiteral(@">Get Notification</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class=""m-social-profile__activity-item-content"">
                                <div class=""m-social-profile__activity-item-document"">
                                    <span class=""ua-icon-social-document m-social-profile__activity-item-document-icon""></span>
                                    <div class=""m-social-profile__activity-item-document-desc"">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ullamcorper ligula augue, eget eleifend nisl varius eu. Proin semper tellus magna, vel venenatis justo mattis eu. Duis maximus urna a scelerisque rhoncus.
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ullamcorper ligula augue, eget eleifend nisl varius eu. Proin semper tel");
            WriteLiteral(@"lus magna, vel venenatis justo mattis eu. Duis maximus urna a scelerisque rhoncus.
                                    </div>
                                    <a href=""#"" class=""btn btn-outline-secondary m-social-profile__activity-item-document-read"">Read now</a>
                                </div>
                            </div>
                        </div>
                        <div class=""m-social-profile__activity-item"">
                            <div class=""m-social-profile__activity-item-header"">
                                <img src=""http://static.essolution.net/img/users/user-15.png""");
            EndContext();
            BeginWriteAttribute("alt", " alt=\"", 5556, "\"", 5562, 0);
            EndWriteAttribute();
            BeginContext(5563, 2368, true);
            WriteLiteral(@" class=""m-social-profile__activity-item-avatar rounded-circle"" width=""56"" height=""56"">
                                <div class=""m-social-profile__activity-item-info"">
                                    <div>
                                        <a href=""#"" class=""link-info"">Antonius</a> <span class=""m-social-profile__activity-item-event"">started following</span>
                                        <a href=""#"" class=""link-info"">Jack Bay</a> <span class=""color-gray"">2m ago</span>
                                    </div>
                                    <div>
                                        <span class=""m-social-profile__activity-item-date"">
                                            <span class=""ua-icon-datepicker m-social-profile__activity-item-icon""></span> <span>Today 08:30 am - 02.05.2024</span>
                                        </span>
                                    </div>
                                </div>
                                <div class=""m-so");
            WriteLiteral(@"cial-profile__activity-item-controls"">
                                    <a href=""#"" class=""m-social-profile__activity-item-reply m-social-profile__activity-item-control""><span class=""ua-icon-reply icon""></span> Reply</a>
                                    <div class=""dropdown no-arrow m-social-profile__activity-item-dropdown m-social-profile__activity-item-control"">
                                        <span class=""dropdown-toggle ua-icon-dots-vertical"" data-toggle=""dropdown""></span>
                                        <div class=""dropdown-menu dropdown-menu-right"">
                                            <a class=""dropdown-item"" href=""#"">I don’t want to see this</a>
                                            <a class=""dropdown-item"" href=""#"">Unfollow Antonius</a>
                                            <a class=""dropdown-item"" href=""#"">Get Notification</a>
                                        </div>
                                    </div>
                                </");
            WriteLiteral(@"div>
                            </div>
                        </div>
                        <div class=""m-social-profile__activity-item"">
                            <div class=""m-social-profile__activity-item-header"">
                                <img src=""http://static.essolution.net/img/users/user-15.png""");
            EndContext();
            BeginWriteAttribute("alt", " alt=\"", 7931, "\"", 7937, 0);
            EndWriteAttribute();
            BeginContext(7938, 3542, true);
            WriteLiteral(@" class=""m-social-profile__activity-item-avatar rounded-circle"" width=""56"" height=""56"">
                                <div class=""m-social-profile__activity-item-info"">
                                    <div>
                                        <a href=""#"" class=""link-info"">Francesca S.</a> <span class=""m-social-profile__activity-item-event"">created a new event</span>
                                    </div>
                                    <div>
                                        <span class=""m-social-profile__activity-item-date"">
                                            <span class=""ua-icon-datepicker m-social-profile__activity-item-icon""></span> <span>09:12 pm</span>
                                        </span>
                                        <span class=""m-social-profile__activity-item-location"">
                                            <span class=""ua-icon-map m-social-profile__activity-item-icon""></span> <span>Las Vegas</span>
                                ");
            WriteLiteral(@"        </span>
                                    </div>
                                </div>
                                <div class=""m-social-profile__activity-item-controls"">
                                    <a href=""#"" class=""m-social-profile__activity-item-reply m-social-profile__activity-item-control""><span class=""ua-icon-reply icon""></span> Reply</a>
                                    <div class=""dropdown no-arrow m-social-profile__activity-item-dropdown m-social-profile__activity-item-control"">
                                        <span class=""dropdown-toggle ua-icon-dots-vertical"" data-toggle=""dropdown""></span>
                                        <div class=""dropdown-menu dropdown-menu-right"">
                                            <a class=""dropdown-item"" href=""#"">I don’t want to see this</a>
                                            <a class=""dropdown-item"" href=""#"">Unfollow Antonius</a>
                                            <a class=""dropdown-item"" href=""#""");
            WriteLiteral(@">Get Notification</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class=""m-social-profile__activity-item-content"">
                                <div class=""m-social-profile__activity-item-blockquote"">
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                                        nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                    </p>
                                    <p>
                                        Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
                      ");
            WriteLiteral(@"                  culpa qui officia deserunt mollit anim id est laborum.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class=""m-social-profile__activity-item"">
                            <div class=""m-social-profile__activity-item-header"">
                                <img src=""http://static.essolution.net/img/users/user-15.png""");
            EndContext();
            BeginWriteAttribute("alt", " alt=\"", 11480, "\"", 11486, 0);
            EndWriteAttribute();
            BeginContext(11487, 2460, true);
            WriteLiteral(@" class=""m-social-profile__activity-item-avatar rounded-circle"" width=""56"" height=""56"">
                                <div class=""m-social-profile__activity-item-info"">
                                    <div>
                                        <a href=""#"" class=""link-info"">Francesca S.</a> <span class=""m-social-profile__activity-item-event"">has uploaded photo</span>
                                        <a href=""#"" class=""link-info"">Jack Bay</a> <span class=""color-gray"">yesterday</span>
                                    </div>
                                    <div>
                                        <span class=""m-social-profile__activity-item-date"">
                                            <span class=""ua-icon-datepicker m-social-profile__activity-item-icon""></span> <span>Today 08:30 am - 02.05.2024</span>
                                        </span>
                                    </div>
                                </div>
                                <div cla");
            WriteLiteral(@"ss=""m-social-profile__activity-item-controls"">
                                    <a href=""#"" class=""m-social-profile__activity-item-reply m-social-profile__activity-item-control""><span class=""ua-icon-reply icon""></span> Reply</a>
                                    <div class=""dropdown no-arrow m-social-profile__activity-item-dropdown m-social-profile__activity-item-control"">
                                        <span class=""dropdown-toggle ua-icon-dots-vertical"" data-toggle=""dropdown""></span>
                                        <div class=""dropdown-menu dropdown-menu-right"">
                                            <a class=""dropdown-item"" href=""#"">I don’t want to see this</a>
                                            <a class=""dropdown-item"" href=""#"">Unfollow Antonius</a>
                                            <a class=""dropdown-item"" href=""#"">Get Notification</a>
                                        </div>
                                    </div>
                          ");
            WriteLiteral(@"      </div>
                            </div>
                            <div class=""m-social-profile__activity-item-content"">
                                <div class=""m-social-profile__activity-item-images"">
                                    <div class=""m-social-profile__activity-item-image"">
                                        <img src=""http://static.essolution.net/img/social-profile/9.png""");
            EndContext();
            BeginWriteAttribute("alt", " alt=\"", 13947, "\"", 13953, 0);
            EndWriteAttribute();
            BeginContext(13954, 712, true);
            WriteLiteral(@">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class=""m-social-profile__load-more-block"">
                            <a href=""#"" class=""btn btn-outline-secondary icon-left mr-3"">
                                Load More Activities <span class=""btn-icon ua-icon-update""></span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<span class=""settings-panel-control"">
    <span class=""settings-panel-control__icon ua-icon-settings""></span>
</span>");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
