﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using ES_MODEL.Task;
using HRM.Models;
using HRM.Models.Custom.Hr;
using HRM.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
namespace HRM.Controllers
{
    [Authorize]
    public class BaseController : Controller
    {
        #region
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p">Biến truyền vào</param>
        /// <param name="d">giá trị truyền vào</param>
        public void CheckNullParameterStore(SqlParameter p, dynamic d)
        {
            if (d == null)
                p.Value = DBNull.Value;
            else
                p.Value = d;
        }
        #endregion
        #region user
        public EmployeesViewLogin GetEmployeeLogin(HRMDBContext _context)
        {
            try
            {
                string sql = "[HR].[SP_HRM_GetEmployeesByLoginName] @LoginName";
                var _LoginName = new SqlParameter("@LoginName", SqlDbType.NVarChar, 50);CheckNullParameterStore(_LoginName, User.Identity.Name);
                var rs = _context.EmployeesViewLogin.FromSql(sql, _LoginName).Take(1).SingleOrDefault();
                return rs;
            }
            catch (Exception ex)
            {
                return new EmployeesViewLogin();
            }
        }
        [HttpPost]
        public string GetCreatedBy(HRMDBContext _context)
        {
            try
            {
                string sql = "[HR].[SP_HRM_GetEmployeesByLoginName] @LoginName";
                var _LoginName = new SqlParameter("@LoginName", SqlDbType.NVarChar, 50); CheckNullParameterStore(_LoginName, User.Identity.Name);
                var rs = _context.EmployeesViewLogin.FromSql(sql, _LoginName).Take(1).SingleOrDefault();
                if (rs != null)
                {
                    return (rs.LoginName + "#" + rs.FullName);

                }
                return "#";
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        public string GetModifiedBy(HRMDBContext _context)
        {
            try
            {
                string sql = "[HR].[SP_HRM_GetEmployeesByLoginName] @LoginName";
                var _LoginName = new SqlParameter("@LoginName", SqlDbType.NVarChar, 50); CheckNullParameterStore(_LoginName, User.Identity.Name);
                var rs = _context.EmployeesViewLogin.FromSql(sql, _LoginName).Take(1).SingleOrDefault();
                if (rs != null)
                {
                    return (rs.LoginName + "#" + rs.FullName);

                }
                return "#";
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        #endregion
        public SqlParameter SqlPara(string Column, dynamic d, SqlDbType sqltype)
        {
            var p = new SqlParameter(Column, sqltype);
            if (d == null)
                p.Value = DBNull.Value;
            else
                p.Value = d;

            return p;
        }
        /// <summary>
        /// Tạo ghi sự kiện hoạt động của công việc
        /// </summary>
        /// <param name="id">Số hoạt động tự đặt</param>
        /// <param name="s">Subject</param>
        /// <param name="ac">ActivityContent</param>
        /// <param name="guid">Mã guid của công việc</param>
        /// <param name="_emp">Đối tượng thực hiện</param>
        public void CreateLogTaskActivities(HRMDBContext db, string id, string s, string ac, Guid guid, EmployeesViewLogin emp)
        {
            try
            {
                var _oTaskOfUser = db.TaskOfUsers
                                .Where(x => x.TaskGuid == guid
                                        && x.EmployeeGuid == emp.EmployeeGuid
                                )
                                .Select(x => new
                                {
                                    x.TaskOfUserGuid,
                                    x.TaskGu.OrganizationGuid,
                                })
                                .FirstOrDefault();
                TaskActivities o = new TaskActivities();
                o.OrganizationGuid = _oTaskOfUser.OrganizationGuid;
                o.TaskActivityGuid = Guid.NewGuid();
                o.ActivityId = id;
                o.TaskGuid = guid;
                o.TaskOfUserGuid = _oTaskOfUser.TaskOfUserGuid;
                o.Subject = s;
                o.ActivityContent = ac;
                o.CreatedBy = GetCreatedBy(db);
                o.CreatedDate = DateTime.Now;
                o.EmployeeGuid = emp.EmployeeGuid;
                o.EmployeeId = emp.EmployeeId;
                o.EmployeeName = emp.FullName;
                o.LoginName = emp.LoginName;
                o.Type = "";
                db.TaskActivities.Add(o);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                //throw new ArgumentNullException(ex.ToString());
            }
        }
        /// <summary>
        /// Thêm nội dung bình luận
        /// </summary>
        /// <param name="db">Context databse connect to table</param>
        /// <param name="_emp">Infor user login</param>
        /// <param name="comment">Comment insert to table</param>
        [HttpPost]
        [Area("Cooperation")]
        // [ActionAuthorize(EAction.COMMENT)]
        public void CreateLogComment(HRMDBContext db, EmployeesViewLogin _emp, string comment)
        {
            try
            {
                TaskComments obj = new TaskComments();
                obj.Comment = comment;
                obj.CreatedBy = _emp.LoginName + "#" + _emp.FullName;
                obj.CreatedDate =DateTime.Now;
                obj.EmployeeId = _emp.EmployeeId;
                obj.EmployeeName = _emp.FullName;
                obj.EmployeeGuid = _emp.EmployeeGuid;
                obj.OrganizationGuid = _emp.OrganizationGuid;
                db.TaskComments.Add(obj);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
            }
        }
        #region GetAPI 
        public async Task<string> POST(string link, object obj)
        {
            string data = "";
            HttpClient client = new HttpClient();
            var myContent = JsonConvert.SerializeObject(obj);
            var buffer = System.Text.Encoding.UTF8.GetBytes(myContent);
            var byteContent = new ByteArrayContent(buffer);
            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            HttpResponseMessage response = await client.PostAsync(link, byteContent);
            if (response.IsSuccessStatusCode)
            {
                data = await response.Content.ReadAsStringAsync();
            }
            return data;
        }
        //public async Task<string> POSTSENDNOTI(string link, object obj)
        //{
        //    string data = "";
        //    HttpClient client = new HttpClient();
        //    client.SetBearerToken(WebContext.UserAccessToken);
        //    var myContent = JsonConvert.SerializeObject(obj);
        //    var buffer = System.Text.Encoding.UTF8.GetBytes(myContent);
        //    var byteContent = new ByteArrayContent(buffer);
        //    byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

        //    HttpResponseMessage response = await client.PostAsync(link, byteContent);
        //    if (response.IsSuccessStatusCode)
        //    {
        //        data = await response.Content.ReadAsStringAsync();
        //    }
        //    return data;
        //}
        public async Task<object> POSTAPI_Timekeepings(string url, object Obj)
        {
            var client = new HttpClient();
            var json = JsonConvert.SerializeObject(Obj);
            var para = new StringContent(json, Encoding.UTF8, "application/json");
            var result = await client.PostAsync(url, para);
            var contents = await result.Content.ReadAsStringAsync();
            return contents;
        }
        //public async Task<string> GET(string link)
        //{
        //    HttpClient client = new HttpClient();
        //    client.SetBearerToken(WebContext.UserAccessToken);
        //    var result = await client.GetAsync(link);
        //    var contents = await result.Content.ReadAsStringAsync();
        //    return contents;
        //}
        // call api
        // var url = appSettings.APIHost + appSettings.APIUserInfoByLogin;
        //  var rs = await POST(url, _liGuid);
        #endregion
    }
}