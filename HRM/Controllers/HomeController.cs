﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using HRM.Utils;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.WsFederation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Rewrite.Internal.ApacheModRewrite;
using HRM.Models;
using Microsoft.AspNetCore.Hosting;
using FTU.Utils.HelperNet;
using System.Data.SqlClient;
using System.Data;
using Microsoft.EntityFrameworkCore;

namespace HRM.Controllers
{
    public class HomeController : BaseController
    {
        private UserManager<AppUser> UserMgr { get; }
        private SignInManager<AppUser> SignInMgr { get; }
        private readonly HRMDBContext _context;
        private readonly IHostingEnvironment _environment;
        public HomeController(HRMDBContext context, UserManager<AppUser> userManager, SignInManager<AppUser> signInManager, IHostingEnvironment environment)
        {
            UserMgr = userManager;
            SignInMgr = signInManager;
            _context = context;
            _environment = environment;
        }

        public IActionResult Index()
        {
          //  ViewBag.Slides = _context.GroupSanPhams.Where(x => x.Status == true).Select(x => new objSearch() { RowGuid = x.RowGuid, AnhSanPhams = _context.AnhSanPhams.Where(b =>b.Image1 !=null && _context.SanPhams.Where(s => s.GroupSanPhamGuid == x.RowGuid && s.Location == true).Select(s => s.RowGuid).FirstOrDefault() == b.SanPhamGuid).ToList(), TenNhom = x.TenNhom, MetaTitle = x.MetaTitle, Description = x.Description, StatusShowTenNhom = x.StatusShowTenNhom }).ToList();
           
            if (User.Identity.Name != "" && User.Identity.Name != null)
            {
                return RedirectToAction("Index", "Home", new { area = "Hr" });
            }
            else
            {
                return View();
            }
        }
        
        [HttpGet]
        public FileContentResult GetAvatar()
        {
            try
            {
              var user=  _context.Employees.Where(x => x.LoginName == User.Identity.Name).FirstOrDefault();
                if (user != null)
                {
                    byte[] cover = user.Photo;
                    if(cover !=null)
                    {
                        return File(cover, "image/jpg");
                    }
                    else
                    {
                        var path = _environment.WebRootPath + "/images/avatar.png";
                        var photo = System.IO.File.ReadAllBytes(path);
                        return File(photo, "image/jpg");
                    }                     
                }
                else
                {
                    var path = _environment.WebRootPath + "/images/avatar.png";
                    var photo = System.IO.File.ReadAllBytes(path);
                    return File(photo, "image/jpg");
                }
            }
            catch (Exception ex)
            {
                var path = _environment.WebRootPath + "/images/avatar.png";
                var photo = System.IO.File.ReadAllBytes(path);
                return File(photo, "image/jpg");
            }
        }
        public class ObjectUserChange
        {
            public string UserName { get; set; }
            public string Password { get; set; }
            public string RePassword { get; set; }
            public bool RememberMe { get; set; }
            public string Email { get; set; }
        }
        [HttpPost]
        public async Task<IActionResult> Index(ObjectUserChange obj)
        {
            JMessage msg = new JMessage() { Error = false };
            if (obj.UserName == null || obj.UserName == "" || obj.Password == null || obj.Password == "")
            {
                msg.Error = true;
                msg.Title = "Tài khoản, mật khẩu không được để trống.";
            }
            else
            {
                AppUser user = await UserMgr.FindByNameAsync(obj.UserName);

                bool _checlOldPass = await UserMgr.CheckPasswordAsync(user, obj.Password);
                if (user == null)
                {
                    msg.Error = true;
                    msg.Title = "Tài khoản chưa được đăng ký.";
                }
                else if (!_checlOldPass)
                {
                    msg.Error = true;
                    msg.Title = "Sai mật khẩu đang sử dụng.";
                }
                else
                {
                    var result = await SignInMgr.PasswordSignInAsync(obj.UserName, obj.Password, obj.RememberMe, false);
                    if (result.Succeeded)
                    {
                        var clarm = new List<Claim> {
                            new Claim(ClaimTypes.Name,obj.UserName)
                            };
                        var identity = new ClaimsIdentity(clarm, CookieAuthenticationDefaults.AuthenticationScheme);
                        var principal = new ClaimsPrincipal(identity);
                        var props = new AuthenticationProperties();
                        HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal, props).Wait();
                        return RedirectToAction("Index", "Home", new { area = "Hr" });

                    }
                    else
                    {
                        msg.Error = true;
                        msg.Object = result;
                        msg.Title = "Đăng nhập không thành công, Mật khẩu phải gồm chữ hoa, chữ thường, ký tự đặc biệt và số.";
                    }
                }
            }
            return View(msg);
        }
        public async Task<IActionResult> Logout()
        {
            await SignInMgr.SignOutAsync();
            return RedirectToAction("Login", "Account");
        }
        public IActionResult Register()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Register(ObjectUserChange obj)
        {
            JMessage msg = new JMessage() { Error = false };
            try
            {
                if (obj.UserName == null || obj.UserName == "")
                {
                    msg.Error = true;
                    msg.Title = "Tài khoản không được để trống.";

                }
                else if (obj.Password == null || obj.Password == "")
                {
                    msg.Error = true;
                    msg.Title = "Mật khẩu không được để trống.";

                }
                else if (obj.RePassword == null || obj.RePassword == "")
                {
                    msg.Error = true;
                    msg.Title = "Nhập lại mật khẩu không được để trống.";

                }
                else if (obj.RePassword.ToUpper() != obj.Password.ToUpper())
                {
                    msg.Error = true;
                    msg.Title = "Nhập lại mật khẩu không trùng khớp.";

                }
                else if (obj.Email == null || obj.Email == "")
                {
                    msg.Error = true;
                    msg.Title = "Email không được để trống.";
                }
                else
                {
                    AppUser Email = await UserMgr.FindByEmailAsync(obj.Email);
                    AppUser user = await UserMgr.FindByNameAsync(obj.UserName);
                    if (Email != null)
                    {
                        msg.Error = true;
                        msg.Title = "Email đã tồn tại.";
                    }
                    else if (user == null)
                    {
                        user = new AppUser();
                        user.UserName = obj.UserName;
                        user.Email = obj.Email;
                        user.LastName = "";
                        user.FirstName = "";
                        IdentityResult result = await UserMgr.CreateAsync(user, obj.Password);
                        if (result.Succeeded == true)
                        {
                            msg.Error = false;
                            msg.Title = "Tài khoản tạo thành công";
                        }
                        else
                        {
                            msg.Error = true;
                            msg.Title = "Tạo tài khoản không thành công";
                        }
                    }
                    else
                    {
                        msg.Error = true;
                        msg.Title = "Tài khoản đã tồn tại.";
                    }
                }
            }
            catch (Exception ex)
            {
                msg.Error = true;
                msg.Title = ex.Message;
            }
            return View(msg);
        }
        // Thay đổi mật khẩu

        public class ObjectPassChange
        {
            public string PasswordOld { get; set; }
            public string PasswordNew { get; set; }
            public string PasswordReNew { get; set; }
        }
        public IActionResult ChangePassword()
        {
            return View();
        }
        /// <summary>
        /// Thay đổi mật khẩu
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> ChangePassword(ObjectPassChange obj)
        {
            JMessage msg = new JMessage() { Error = false };
            try
            {
                if (obj.PasswordOld == null || obj.PasswordOld == "")
                {
                    msg.Error = true;
                    msg.Title = "Yêu cầu nhập mật khẩu cũ.";
                }
                else if (obj.PasswordNew == null || obj.PasswordNew == "")
                {
                    msg.Error = true;
                    msg.Title = "Yêu cầu nhập mật khẩu mới.";
                }
                else if (obj.PasswordReNew == null || obj.PasswordReNew == "")
                {
                    msg.Error = true;
                    msg.Title = "Yêu cầu nhập xác nhận mật khẩu.";
                }
                else if (obj.PasswordNew != obj.PasswordReNew)
                {
                    msg.Error = true;
                    msg.Title = "Mật khẩu xác nhận lại không khớp với mật khẩu mới.";
                }
                else
                {
                    AppUser user = await UserMgr.FindByNameAsync(User.Identity.Name);
                    bool _checlOldPass = await UserMgr.CheckPasswordAsync(user, obj.PasswordOld);
                    if (!_checlOldPass)
                    {
                        msg.Error = true;
                        msg.Title = "Sai mật khẩu hiện tại đang sử dụng.";
                    }
                    else
                    {
                        string code = await UserMgr.GeneratePasswordResetTokenAsync(user);
                        var result = await UserMgr.ResetPasswordAsync(user, code.Replace(" ", "+"), obj.PasswordNew);
                        if (result.Succeeded)
                        {
                            msg.Error = false;
                            msg.Object = result;
                            msg.Title = "Đặt lại mật khẩu thành công.";
                            Redirect("/Home");
                        }
                        else
                        {
                            msg.Error = true;
                            msg.Object = result;
                            msg.Title = "Không thành công, Mật khẩu phải gồm chữ hoa, chữ thường, ký tự đặc biệt và số.";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                msg.Error = true;
                msg.Object = ex;
                msg.Title = "Đặt lại mật khẩu không thành công.";
            }
            return View(msg);
        }
       

        public ActionResult RestorePassword()
        {
            return View();
        }
        public class ObjectRestorePassword
        {
            public string UserName { get; set; }
            public string UserEmail { get; set; }
        }
        [HttpPost]
        public async Task<ActionResult> RestorePassword(ObjectRestorePassword obj)
        {
            JMessage msg = new JMessage() { Error = false };
            try
            {
                if ((obj.UserName == null || obj.UserName.Trim() == "") || (obj.UserEmail == null || obj.UserEmail.Trim() == ""))
                {
                    msg.Error = true;
                    msg.Title = "Tên đăng nhập hoặc email không được để trống.";
                }
                else
                {
                    //var _count = _context.Employees
                    //                    .Where(x => x.LoginName.ToLower() == obj.UserName.ToLower() && x.WorkEmail.ToUpper() == obj.UserEmail.ToUpper())
                    //                    .Count();
                    AppUser user = new AppUser();
                    if (obj.UserName != null && obj.UserName.Trim() != "")
                    {
                        user = await UserMgr.FindByNameAsync(obj.UserName);
                        string code = await UserMgr.GeneratePasswordResetTokenAsync(user);

                        //Begin create random password
                        var chars_1 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                        var chars_2 = "abcdefghijklmnopqrstuvwxyz";
                        var chars_3 = "0123456789";
                        var stringChars = new char[9];
                        var random = new Random();
                        for (int i = 0; i < 3; i++)
                        {
                            stringChars[i] = chars_1[random.Next(chars_1.Length)];
                        }
                        for (int i = 3; i < 3; i++)
                        {
                            stringChars[i] = chars_2[random.Next(chars_2.Length)];
                        }
                        for (int i = 6; i < 3; i++)
                        {
                            stringChars[i] = chars_3[random.Next(chars_3.Length)];
                        }
                        var finalString = new String(stringChars);
                        //End create random password

                        var result = await UserMgr.ResetPasswordAsync(user, code.Replace(" ", "+"), finalString);
                        if (result.Succeeded)
                        {

                            //  Sendmail(_appSettings, new List<string> { obj.UserEmail }, _appSettings.FormatEmail, "Mật khẩu mới của bạn là: " + finalString);
                            msg.Error = false;
                            msg.Object = result;
                            msg.Title = "Khôi phục mật khẩu thành công. " + obj.UserEmail;
                        }
                        else
                        {
                            msg.Error = true;
                            msg.Object = result;
                            msg.Title = "Khôi phục mật khẩu không thành công.";
                        }
                    }
                    else
                    {
                        msg.Error = true;
                        msg.Title = "Tên đăng nhập hoặc email không đúng.";
                    }
                }
            }
            catch (Exception ex)
            {
                msg.Error = true;
                msg.Object = ex;
                msg.Title = "Khôi phục mật khẩu không thành công.";
            }
            return View(msg);
        }
        /// <summary>
        /// Xóa tài khoản
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
      
        public async Task<IActionResult> Delete()
        {
            JMessage msg = new JMessage() { Error = false };
            try
            {
                AppUser user = await UserMgr.FindByNameAsync(User.Identity.Name);              
               if (user != null)
                {
                    IdentityResult result = await UserMgr.DeleteAsync(user);
                    if (result.Succeeded)
                    {
                        await SignInMgr.SignOutAsync();
                        return RedirectToAction("Index", "Home");
                    }                      
                    else
                    {
                        msg.Error = true;
                        msg.Title = "Xóa tài khoản không thành công.";
                    }
                }
                else
                {
                    msg.Error = true;
                    msg.Title = "Không tìm thấy tài khoản.";
                }

            }
            catch (Exception ex)
            {
                msg.Object = ex;
                msg.Error = true;
                msg.Title = "Xóa tài khoản không thành công.";
            }
            return View(msg);
        }
        
       
        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        [HttpPost]
        public object CountMenuDocuments()
        {
            try
            {
                var _emp = GetEmployeeLogin(_context);
                string sql = "exec [dbo].[SP_Fitek_Home_CountMenuDocuments]" +
                "@OrganizationGuid, " +
                "@LoginName";
                var _OrganizationGuid = new SqlParameter("@OrganizationGuid", SqlDbType.UniqueIdentifier);
                CheckNullParameterStore(_OrganizationGuid, _emp.OrganizationGuid);

                var _LoginName = new SqlParameter("@LoginName", SqlDbType.NVarChar);
                CheckNullParameterStore(_LoginName, User.Identity.Name);

                var _rs = _context.CountDocuments.FromSql(sql, _OrganizationGuid,_LoginName).ToList();
                return new JMessage() { Error = false, Title = "Lấy dữ liệu thành công.", Object = _rs };
            }
            catch (Exception ex)
            {
                return new JMessage() { Error = true, Title = "Lấy dữ liệu không thành công.", Object = ex.ToString() };
            }
        }
    }
}
