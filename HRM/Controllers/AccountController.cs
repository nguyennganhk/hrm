﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HRM.Utils;
using Microsoft.AspNetCore.Authentication.WsFederation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Rewrite.Internal.ApacheModRewrite;
using HRM.Models;

namespace HRM.Controllers
{

    public class AccountController : Controller
    {
        private UserManager<AppUser> UserMgr { get; }
        private SignInManager<AppUser> SignInMgr { get; }

        public AccountController(UserManager<AppUser> userManager, SignInManager<AppUser> signInManager)
        {
            UserMgr = userManager;
            SignInMgr = signInManager;
        }
        public IActionResult Index()
        {
            return View();
        }
        public async Task<IActionResult> Logout()
        {
            await SignInMgr.SignOutAsync();
            return RedirectToAction("Login", "Account");
        }
        public class ObjectUserChange
        {
            public string UserName { get; set; }
            public string Password { get; set; }
        }
        public IActionResult Login()
        {
            if (User.Identity.Name != "" && User.Identity.Name != null)
            {
                return RedirectToAction("Index", "Home", new { area = "Hr" });
            }
            else
            {
                ViewBag.Res = "Trang ngoài";
                return View();
            }         
        }
        [HttpPost]
        public async Task<IActionResult> Login(ObjectUserChange obj)
        {
            if (obj.UserName != null && obj.Password != null)
            {
                var result = await SignInMgr.PasswordSignInAsync(obj.UserName, obj.Password, false, false);
                if (result.Succeeded)
                {
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ViewBag.Result = "Đăng nhập không thành công";
                }
            }
            else if (obj.UserName == null || obj.UserName == "")
            {
                ViewBag.Result = "Yêu cầu nhập tài khoản";
            }
            else if (obj.Password == null || obj.Password == "")
            {
                ViewBag.Result = "Yêu cầu nhập mật khẩu";
            }
            return View();
        }
        public async Task<IActionResult> Register()
        {
            try
            {
                ViewBag.Message = "Tài khoản đã tồn tại";
                AppUser user = await UserMgr.FindByNameAsync("TestUser");
                if (user == null)
                {
                    user = new AppUser();
                    user.UserName = "TestUser";
                    user.Email = "nguyennganhk@gmail.com";
                    user.LastName = "Ngun";
                    user.FirstName = "Ngn";
                    IdentityResult result = await UserMgr.CreateAsync(user, "Test123!");
                    ViewBag.Message = "Tài khoản tạo thành công";
                }
            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message;
            }
            return View();
        }
        // Thay đổi mật khẩu

        public class ObjectPassChange
        {
            public string PasswordOld { get; set; }
            public string PasswordNew { get; set; }
            public string PasswordReNew { get; set; }
        }
        public IActionResult ChangePassword()
        {
            return View();
        }
        /// <summary>
        /// Thay đổi mật khẩu
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> ChangePassword(ObjectPassChange obj)
        {
            JMessage msg = new JMessage() { Error = false };
            try
            {
                if (obj.PasswordNew != obj.PasswordReNew)
                {
                    msg.Error = true;
                    msg.Title = "Mật khẩu xác nhận lại không khớp với mật khẩu mới.";
                }
                else
                {
                    AppUser user = await UserMgr.FindByNameAsync(User.Identity.Name);
                    bool _checlOldPass = await UserMgr.CheckPasswordAsync(user, obj.PasswordOld);
                    if (!_checlOldPass)
                    {
                        msg.Error = true;
                        msg.Title = "Sai mật khẩu hiện tại đang sử dụng.";
                    }
                    else
                    {
                        string code = await UserMgr.GeneratePasswordResetTokenAsync(user);
                        var result = await UserMgr.ResetPasswordAsync(user, code.Replace(" ", "+"), obj.PasswordNew);
                        if (result.Succeeded)
                        {
                            msg.Error = false;
                            msg.Object = result;
                            msg.Title = "Đặt lại mật khẩu thành công.";
                            Redirect("/Home");
                        }
                        else
                        {
                            msg.Error = true;
                            msg.Object = result;
                            msg.Title = "Không thành công, Mật khẩu phải gồm chữ hoa, chữ thường, ký tự đặc biệt và số.";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                msg.Error = true;
                msg.Object = ex;
                msg.Title = "Đặt lại mật khẩu không thành công.";
            }
            return View(msg);
        }

    }
}
